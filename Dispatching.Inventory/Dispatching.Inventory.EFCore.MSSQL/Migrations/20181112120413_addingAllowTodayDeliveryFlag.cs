﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Dispatching.Inventory.EFCore.MSSQL.Migrations
{
    public partial class addingAllowTodayDeliveryFlag : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<bool>(
                name: "AllowTodayDelivery",
                table: "LKP_Category",
                nullable: false,
                defaultValue: false);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "AllowTodayDelivery",
                table: "LKP_Category");
        }
    }
}
