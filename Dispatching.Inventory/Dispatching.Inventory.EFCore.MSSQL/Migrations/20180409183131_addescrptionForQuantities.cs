﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Dispatching.Inventory.EFCore.MSSQL.Migrations
{
    public partial class addescrptionForQuantities : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "DescriptionAR",
                table: "LKP_Quantity",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "DescriptionEN",
                table: "LKP_Quantity",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "MaximumNo",
                table: "LKP_Item",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "MinimumNo",
                table: "LKP_Item",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<string>(
                name: "DescriptionAR",
                table: "LKP_Cutting",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "DescriptionEN",
                table: "LKP_Cutting",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "DescriptionAR",
                table: "LKP_Quantity");

            migrationBuilder.DropColumn(
                name: "DescriptionEN",
                table: "LKP_Quantity");

            migrationBuilder.DropColumn(
                name: "MaximumNo",
                table: "LKP_Item");

            migrationBuilder.DropColumn(
                name: "MinimumNo",
                table: "LKP_Item");

            migrationBuilder.DropColumn(
                name: "DescriptionAR",
                table: "LKP_Cutting");

            migrationBuilder.DropColumn(
                name: "DescriptionEN",
                table: "LKP_Cutting");
        }
    }
}
