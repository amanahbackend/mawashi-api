﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Dispatching.Inventory.EFCore.MSSQL.Migrations
{
    public partial class addingMultiCategories : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "Fk_Parent_Id",
                table: "LKP_Category",
                nullable: true);

            migrationBuilder.AddColumn<bool>(
                name: "IsParent",
                table: "LKP_Category",
                nullable: false,
                defaultValue: false);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Fk_Parent_Id",
                table: "LKP_Category");

            migrationBuilder.DropColumn(
                name: "IsParent",
                table: "LKP_Category");
        }
    }
}
