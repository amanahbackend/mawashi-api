﻿using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Dispatching.Inventory.EFCore.MSSQL.Migrations
{
    public partial class CategoryConfig : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<bool>(
                name: "AcceptMulti",
                table: "LKP_Category",
                nullable: false,
                defaultValue: false);

            migrationBuilder.CreateTable(
                name: "CategoryDeliveryOptions",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    CreatedDate = table.Column<DateTime>(nullable: false),
                    DateTypes = table.Column<int>(nullable: false),
                    DeletedDate = table.Column<DateTime>(nullable: false),
                    FK_CategoryConfig_Id = table.Column<int>(nullable: false),
                    FK_CreatedBy_Id = table.Column<string>(nullable: true),
                    FK_DeletedBy_Id = table.Column<string>(nullable: true),
                    FK_UpdatedBy_Id = table.Column<string>(nullable: true),
                    IsDeleted = table.Column<bool>(nullable: false),
                    IsSystemKey = table.Column<bool>(nullable: true),
                    NameAR = table.Column<string>(nullable: false),
                    NameEN = table.Column<string>(nullable: false),
                    UpdatedDate = table.Column<DateTime>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_CategoryDeliveryOptions", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "CategorySubOptions",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    CreatedDate = table.Column<DateTime>(nullable: false),
                    DeletedDate = table.Column<DateTime>(nullable: false),
                    FK_CategoryConfig_Id = table.Column<int>(nullable: false),
                    FK_CreatedBy_Id = table.Column<string>(nullable: true),
                    FK_DeletedBy_Id = table.Column<string>(nullable: true),
                    FK_UpdatedBy_Id = table.Column<string>(nullable: true),
                    IsDeleted = table.Column<bool>(nullable: false),
                    IsSystemKey = table.Column<bool>(nullable: true),
                    NameAR = table.Column<string>(nullable: false),
                    NameEN = table.Column<string>(nullable: false),
                    UpdatedDate = table.Column<DateTime>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_CategorySubOptions", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "LKP_CategoryConfig",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    AcceptMulti = table.Column<bool>(nullable: false),
                    CategoryId = table.Column<int>(nullable: true),
                    CreatedDate = table.Column<DateTime>(nullable: false),
                    DeletedDate = table.Column<DateTime>(nullable: false),
                    FK_Category_Id = table.Column<int>(nullable: false),
                    FK_CreatedBy_Id = table.Column<string>(nullable: true),
                    FK_DeletedBy_Id = table.Column<string>(nullable: true),
                    FK_UpdatedBy_Id = table.Column<string>(nullable: true),
                    HasOptions = table.Column<bool>(nullable: false),
                    IsDeleted = table.Column<bool>(nullable: false),
                    IsSystemKey = table.Column<bool>(nullable: true),
                    NameAR = table.Column<string>(nullable: false),
                    NameEN = table.Column<string>(nullable: false),
                    UpdatedDate = table.Column<DateTime>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_LKP_CategoryConfig", x => x.Id);
                    table.ForeignKey(
                        name: "FK_LKP_CategoryConfig_LKP_Category_CategoryId",
                        column: x => x.CategoryId,
                        principalTable: "LKP_Category",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_LKP_CategoryConfig_CategoryId",
                table: "LKP_CategoryConfig",
                column: "CategoryId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "CategoryDeliveryOptions");

            migrationBuilder.DropTable(
                name: "CategorySubOptions");

            migrationBuilder.DropTable(
                name: "LKP_CategoryConfig");

            migrationBuilder.DropColumn(
                name: "AcceptMulti",
                table: "LKP_Category");
        }
    }
}
