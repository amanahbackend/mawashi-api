﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Dispatching.Inventory.EFCore.MSSQL.Migrations
{
    public partial class CompleteConfig : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_LKP_CategoryConfig_LKP_Category_CategoryId",
                table: "LKP_CategoryConfig");

            migrationBuilder.DropIndex(
                name: "IX_LKP_CategoryConfig_CategoryId",
                table: "LKP_CategoryConfig");

            migrationBuilder.DropColumn(
                name: "CategoryId",
                table: "LKP_CategoryConfig");

            migrationBuilder.AddColumn<int>(
                name: "ConfigId",
                table: "LKP_Category",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_LKP_Category_ConfigId",
                table: "LKP_Category",
                column: "ConfigId",
                unique: true,
                filter: "[ConfigId] IS NOT NULL");

            migrationBuilder.AddForeignKey(
                name: "FK_LKP_Category_LKP_CategoryConfig_ConfigId",
                table: "LKP_Category",
                column: "ConfigId",
                principalTable: "LKP_CategoryConfig",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_LKP_Category_LKP_CategoryConfig_ConfigId",
                table: "LKP_Category");

            migrationBuilder.DropIndex(
                name: "IX_LKP_Category_ConfigId",
                table: "LKP_Category");

            migrationBuilder.DropColumn(
                name: "ConfigId",
                table: "LKP_Category");

            migrationBuilder.AddColumn<int>(
                name: "CategoryId",
                table: "LKP_CategoryConfig",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_LKP_CategoryConfig_CategoryId",
                table: "LKP_CategoryConfig",
                column: "CategoryId");

            migrationBuilder.AddForeignKey(
                name: "FK_LKP_CategoryConfig_LKP_Category_CategoryId",
                table: "LKP_CategoryConfig",
                column: "CategoryId",
                principalTable: "LKP_Category",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
