﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Dispatching.Inventory.EFCore.MSSQL.Migrations
{
    public partial class ChangeCategoryConfigrelation : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_LKP_Category_LKP_CategoryConfig_ConfigId",
                table: "LKP_Category");

            migrationBuilder.DropIndex(
                name: "IX_LKP_Category_ConfigId",
                table: "LKP_Category");

            migrationBuilder.DropColumn(
                name: "FK_Category_Id",
                table: "LKP_CategoryConfig");

            migrationBuilder.DropColumn(
                name: "ConfigId",
                table: "LKP_Category");

            migrationBuilder.DropColumn(
                name: "IsSystemKey",
                table: "CategoryDeliveryOptions");

            migrationBuilder.DropColumn(
                name: "NameAR",
                table: "CategoryDeliveryOptions");

            migrationBuilder.DropColumn(
                name: "NameEN",
                table: "CategoryDeliveryOptions");

            migrationBuilder.AddColumn<int>(
                name: "FK_CategoryConfig_Id",
                table: "LKP_Category",
                nullable: false,
                defaultValue: 0);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "FK_CategoryConfig_Id",
                table: "LKP_Category");

            migrationBuilder.AddColumn<int>(
                name: "FK_Category_Id",
                table: "LKP_CategoryConfig",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "ConfigId",
                table: "LKP_Category",
                nullable: true);

            migrationBuilder.AddColumn<bool>(
                name: "IsSystemKey",
                table: "CategoryDeliveryOptions",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "NameAR",
                table: "CategoryDeliveryOptions",
                nullable: false,
                defaultValue: "");

            migrationBuilder.AddColumn<string>(
                name: "NameEN",
                table: "CategoryDeliveryOptions",
                nullable: false,
                defaultValue: "");

            migrationBuilder.CreateIndex(
                name: "IX_LKP_Category_ConfigId",
                table: "LKP_Category",
                column: "ConfigId",
                unique: true,
                filter: "[ConfigId] IS NOT NULL");

            migrationBuilder.AddForeignKey(
                name: "FK_LKP_Category_LKP_CategoryConfig_ConfigId",
                table: "LKP_Category",
                column: "ConfigId",
                principalTable: "LKP_CategoryConfig",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
