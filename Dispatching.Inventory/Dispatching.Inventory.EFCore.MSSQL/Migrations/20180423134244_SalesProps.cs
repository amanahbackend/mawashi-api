﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Dispatching.Inventory.EFCore.MSSQL.Migrations
{
    public partial class SalesProps : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "CurrentHeadStock",
                table: "LKP_Item",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "CurrentKgsStock",
                table: "LKP_Item",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "CurrentPiecesStock",
                table: "LKP_Item",
                nullable: false,
                defaultValue: 0);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "CurrentHeadStock",
                table: "LKP_Item");

            migrationBuilder.DropColumn(
                name: "CurrentKgsStock",
                table: "LKP_Item");

            migrationBuilder.DropColumn(
                name: "CurrentPiecesStock",
                table: "LKP_Item");
        }
    }
}
