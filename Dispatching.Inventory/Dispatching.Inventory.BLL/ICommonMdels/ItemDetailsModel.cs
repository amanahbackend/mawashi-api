﻿using Dispatching.Inventory.Models.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace Dispatching.Inventory.BLL.CommonModels
{
    public interface IItemDetailsModel
    {
         int Fk_Item_Id { get; set; }
         List<Allowable_Items_Quatity> Quantities { get; set; }
         List<Allowable_Items_Cutting> Cuttings { get; set; }
    }
}
