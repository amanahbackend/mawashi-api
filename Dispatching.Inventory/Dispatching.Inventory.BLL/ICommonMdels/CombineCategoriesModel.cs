﻿using Dispatching.Inventory.Models.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace Dispatching.Inventory.BLL.CommonModels
{
    public interface ICombineCategoriesModel
    {
         bool AllowMulti { get; set; }
         int CategoryId { get; set; }
         int Count { get; set; }
         List<LKP_Category> Categories { get; set; }
    }
}
