﻿using System;
using System.Collections.Generic;
using DispatchProduct.RepositoryModule;
using Dispatching.Inventory.Models.Entities;
using System.Text;
using Utilites.ProcessingResult;

namespace Dispatching.Inventory.BLL.IManagers
{
    public interface IAllowable_Items_CuttingManager : IRepository<Allowable_Items_Cutting>
    {
        ProcessResult<List<Allowable_Items_Cutting>> GetByItemId(int itemId);
        ProcessResult<List<Allowable_Items_Cutting>> Submit(List<int> cuttingIds, int itemId);
    }
}
