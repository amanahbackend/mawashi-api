﻿using Dispatching.Inventory.Models.Entities;
using DispatchProduct.BuildingBlocks.BaseEntities.Entities;
using DispatchProduct.RepositoryModule;
using System;
using System.Collections.Generic;
using System.Text;
using Utilites.ProcessingResult;

namespace Dispatching.Inventory.BLL.IManagers
{
    public interface ILKP_CategoryConfigManager : IRepository<LKP_CategoryConfig>
    {
        ProcessResult<LKP_CategoryConfig> GetByCategoryId(int catId);
        ProcessResult<bool> UpdateAcceptMulti(bool acceptMulti, int configId);
    }
}
