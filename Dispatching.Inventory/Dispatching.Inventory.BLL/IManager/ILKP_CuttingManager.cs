﻿using Dispatching.Inventory.BLL.Managers;
using Dispatching.Inventory.Models.Entities;
using DispatchProduct.RepositoryModule;
using System.Collections.Generic;
using Utilites.ProcessingResult;

namespace Dispatching.Inventory.BLL.IManagers
{
    public interface ILKP_CuttingManager : IRepository<LKP_Cutting>
    {
        new ProcessResult<List<LKP_Cutting>> Add(List<LKP_Cutting> entityLst);
        new ProcessResult<LKP_Cutting> Add(LKP_Cutting entity);
        new ProcessResult<bool> Update(List<LKP_Cutting> entityLst);
        new ProcessResult<bool> Update(LKP_Cutting item);
        ProcessResult<List<LKP_Cutting>> GetByVatId(int vatId, bool? IsActive = null);
        ProcessResult<List<LKP_Cutting>> GetByDiscountId(int discountId, bool? IsActive = null);
        ProcessResult<bool> UpdateItemFinalPrice(int id, ItemPropertyChanged propChanged);

    }
}
