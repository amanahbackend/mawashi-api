﻿using System.ComponentModel.DataAnnotations.Schema;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;
using Dispatching.Inventory.Models.Entities;
using DispatchProduct.RepositoryModule;
using System.Linq;
using Utilites.ProcessingResult;
using Dispatching.Inventory.BLL.CommonModels;
using Dispatching.Inventory.BLL.Managers;
using CommonEnums;
using Dispatching.BuisnessCommon.Enums;

namespace Dispatching.Inventory.BLL.IManagers
{
    public interface ILKP_ItemManager : IRepository<LKP_Item>
    {
        ProcessResult<List<LKP_Item>> GetByCategoryId(int categoryTypeId, bool? IsActive = null);
        ProcessResult<bool> SetAvailableDate(AvailableDateViewModel model);
        ProcessResult<bool> SetItemActivation(int id, bool? isActive);
        ProcessResult<List<LKP_Item>> GetAll(bool? IsActive = null, SystemCategoryType type = 0);
        ProcessResult<bool> UpdateItemFinalPrice(int id, ItemPropertyChanged propChanged);
        ProcessResult<bool> UpdateHasQuantity(int itemId,bool hasQuantity);
        ProcessResult<bool> UpdateHasCuttings(int itemId, bool hasCutting);
        ProcessResult<ItemDetailsModel> GetItemDetails(int itemId);
        ProcessResult<bool> CanCombineItems(List<int> itemIds);
        ProcessResult<int> GetCategoryCount(int catId, bool? isActive = null);
        ProcessResult<bool> UpdateStock(int itemId, int count,OrderType orderType,MeasurmentType measurmentType);
        ProcessResult<bool> CheckAvailableIems();
        
    }
}
