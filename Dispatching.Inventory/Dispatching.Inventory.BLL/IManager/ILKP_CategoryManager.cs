﻿using CommonEnums;
using Dispatching.Inventory.BLL.CommonModels;
using Dispatching.Inventory.Models.Entities;
using DispatchProduct.BuildingBlocks.BaseEntities.Entities;
using DispatchProduct.RepositoryModule;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using Utilites.ProcessingResult;

namespace Dispatching.Inventory.BLL.IManagers
{
    public interface ILKP_CategoryManager : IRepository<LKP_Category>
    {
        ProcessResult<List<LKP_Category>> GetAll(bool? IsActive = null, SystemCategoryType type = 0, Expression<Func<LKP_Category, bool>> expression = null);
        ProcessResult<IQueryable<LKP_Category>> GetAllQuerable(bool? IsActive = null);
        ProcessResult<bool> SetAvailableDate(AvailableDateViewModel model);
        ProcessResult<bool> SetCategoryActivation(int id, bool? isActive);
        ProcessResult<List<LKP_Category>> GetByCategoryTypeId(int categoryTypeId, bool? IsActive = null);
        ProcessResult<LKP_Category> BindCategoryConfig(LKP_Category category);
        ProcessResult<bool> UpdateAcceptMulti(bool acceptMulti, int configId);
        ProcessResult<bool> CanCombineItems(List<int> catIds);
        ProcessResult<bool> CheckAvailableCategories();
        ProcessResult<List<LKP_Category>> GetAllParents(bool? IsActive = null, SystemCategoryType type = 0);
        ProcessResult<List<LKP_Category>> GetAllByParentId(int parentId, bool? IsActive = null, SystemCategoryType type = 0);
    }
}
