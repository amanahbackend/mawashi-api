﻿using Dispatching.Inventory.Models.Entities;
using DispatchProduct.RepositoryModule;
using System;
using System.Collections.Generic;
using System.Text;
using Utilites.ProcessingResult;

namespace Dispatching.Inventory.BLL.IManagers
{
    public interface ILKP_VATManager : IRepository<LKP_VAT>
    {
        ProcessResult<LKP_VAT> GetVat(string vatName = null);
    }
}
