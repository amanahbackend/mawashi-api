﻿using Dispatching.Inventory.Models.Entities;
using DispatchProduct.RepositoryModule;
using System;
using System.Collections.Generic;
using System.Text;
using Utilites.ProcessingResult;

namespace Dispatching.Inventory.BLL.IManagers
{
    public interface ILKP_QuantityManager : IRepository<LKP_Quantity>
    {
        ProcessResult<int> GetMaxQuantity(List<int> quanIds);
        ProcessResult<int> GetMinimumQuantity(List<int> quanIds);
    }
}
