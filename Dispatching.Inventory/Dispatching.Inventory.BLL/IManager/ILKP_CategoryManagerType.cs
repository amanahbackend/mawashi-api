﻿using Dispatching.Inventory.Models.Entities;
using DispatchProduct.BuildingBlocks.BaseEntities.Entities;
using DispatchProduct.RepositoryModule;
using System;
using System.Collections.Generic;
using System.Text;

namespace Dispatching.Inventory.BLL.IManagers
{
    public interface ILKP_CategoryTypeManager : IRepository<LKP_CategoryType>
    {
        
    }
}
