﻿using System;
using System.Collections.Generic;
using DispatchProduct.RepositoryModule;
using System.Text;
using Dispatching.Inventory.Models.Entities;
using Utilites.ProcessingResult;

namespace Dispatching.Inventory.BLL.IManagers
{
    public interface IAllowable_Items_QuatityManager : IRepository<Allowable_Items_Quatity>
    {
        ProcessResult<List<Allowable_Items_Quatity>> GetByItemId(int itemId);
        ProcessResult<List<Allowable_Items_Quatity>> Submit(List<int> quantityIds, int itemId);
    }
}
