﻿using Dispatching.Inventory.Models.Entities;
using DispatchProduct.BuildingBlocks.BaseEntities.Entities;
using DispatchProduct.RepositoryModule;
using System;
using System.Collections.Generic;
using System.Text;
using Utilites.ProcessingResult;

namespace Dispatching.Inventory.BLL.IManagers
{
    public interface ICategorySubOptionsManager : IRepository<CategorySubOptions>
    {
        ProcessResult<List<CategorySubOptions>> GetByCategoryConfigId(int catConfigId);
    }
}
