﻿using CommonEnums;
using Dispatching.Inventory.Models.Entities;
using DispatchProduct.BuildingBlocks.BaseEntities.Entities;
using DispatchProduct.RepositoryModule;
using System;
using System.Collections.Generic;
using System.Text;
using Utilites.ProcessingResult;

namespace Dispatching.Inventory.BLL.IManagers
{
    public interface ICategoryDeliveryOptionsManager : IRepository<CategoryDeliveryOptions>
    {
        ProcessResult<List<CategoryDeliveryOptions>> GetByCategoryConfigId(int catConfigId);
        ProcessResult<int> GetDateType(int catConfigId, int deliveryId);
        ProcessResult<List<EnumEntity>> GetDeliveryOptionsByConfigId(int configId);
    }
}
