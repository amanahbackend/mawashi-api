﻿using Dispatching.Inventory.Models.Entities;
using DispatchProduct.RepositoryModule;
using System;
using System.Collections.Generic;
using System.Text;
using Utilites.ProcessingResult;

namespace Dispatching.Inventory.BLL.IManagers
{
    public interface ILKP_CurrencyManager : IRepository<LKP_Currency>
    {
        ProcessResult<LKP_Currency> GetDefaultCurrency();
    }
}
