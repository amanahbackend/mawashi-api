﻿using Dispatching.Inventory.BLL.IManagers;
using Dispatching.Inventory.Models.Context;
using Dispatching.Inventory.Models.Entities;
using Dispatching.Inventory.Models.Settings;
using DispatchProduct.RepositoryModule;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Utilites.ProcessingResult;

namespace Dispatching.Inventory.BLL.Managers
{
    public class LKP_CurrencyManager : Repository<LKP_Currency>, ILKP_CurrencyManager
    {
        InventoryAppSettings settings;
        public LKP_CurrencyManager(InventoryDbContext context,IOptions<InventoryAppSettings> _settings)
            : base(context)
        {
            settings = _settings.Value;
        }
        public ProcessResult<LKP_Currency> GetDefaultCurrency()
        {
            ProcessResult<LKP_Currency> result = null;
            var data=GetAll().Data.Where(cr => cr.NameAR == settings.DefaultCurrencyAR || cr.NameEN == settings.DefaultCurrencyEN).FirstOrDefault();
            if (data != null)
            {
                result = ProcessResultHelper.Succedded(data);
            }
            else
            {
                result = ProcessResultHelper.Failed(data, new Exception("No Default Currency key in settings or not seeded in DB "));
            }
            return result;
        }
    }
}
