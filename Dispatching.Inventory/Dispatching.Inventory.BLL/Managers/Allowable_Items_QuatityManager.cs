﻿using System;
using System.Collections.Generic;
using DispatchProduct.RepositoryModule;
using System.Text;
using Dispatching.Inventory.Models.Entities;
using Dispatching.Inventory.BLL.IManagers;
using Dispatching.Inventory.Models.Context;
using Utilites.ProcessingResult;
using System.Linq;
using Microsoft.Extensions.DependencyInjection;


namespace Dispatching.Inventory.BLL.Managers
{
    public class Allowable_Items_QuatityManager : Repository<Allowable_Items_Quatity>, IAllowable_Items_QuatityManager
    {
        IServiceProvider serviceProvider;
        public Allowable_Items_QuatityManager(InventoryDbContext context, IServiceProvider _serviceProvider)
            : base(context)
        {
            serviceProvider = _serviceProvider;
        }
        public ProcessResult<List<Allowable_Items_Quatity>> GetByItemId(int itemId)
        {
            List<Allowable_Items_Quatity> data = null;
            ProcessResult<List<Allowable_Items_Quatity>> result = null;
            try
            {
                data = GetAllQuerable().Data.Where(r => r.FK_Item_Id == itemId).ToList();
                
                    foreach (var item in data)
                    {
                        var quantityRes = QuantityManager.Get(item.FK_Quatity_Id);
                        if (quantityRes.IsSucceeded)
                            item.Quantity = quantityRes.Data;
                    }
                    result = ProcessResultHelper.Succedded(data);
            }
            catch (Exception ex)
            {
                result = ProcessResultHelper.Failed(data, ex);
            }
            return result;
        }

        private ILKP_QuantityManager QuantityManager
        {
            get { return serviceProvider.GetService<ILKP_QuantityManager>(); }
        }
        private IAllowable_Items_Quatity AllowQuant
        {
            get { return serviceProvider.GetService<IAllowable_Items_Quatity>(); }
        }
        private ILKP_ItemManager LKP_ItemManager
        {
            get { return serviceProvider.GetService<ILKP_ItemManager>(); }
        }
        public override ProcessResult<Allowable_Items_Quatity> Add(Allowable_Items_Quatity entity)
        {
            var result = base.Add(entity);
            if (result.IsSucceeded)
            {
                LKP_ItemManager.UpdateHasQuantity(entity.FK_Item_Id, true);
            }
            return result;
        }
        public override ProcessResult<List<Allowable_Items_Quatity>> Add(List<Allowable_Items_Quatity> entityLst)
        {
            foreach (var item in entityLst)
            {
                var result = Add(item);
                if (!result.IsSucceeded)
                {
                    return ProcessResultHelper.MapToProcessResult<Allowable_Items_Quatity, List<Allowable_Items_Quatity>>(result);
                }
            }
            return ProcessResultHelper.Succedded(entityLst);
        }
        public override ProcessResult<bool> Delete(Allowable_Items_Quatity entity)
        {
            var result = base.Delete(entity);
            if (result.IsSucceeded)
            {
                var itemCount = GetAllQuerable().Data.Where(r => r.FK_Item_Id == entity.FK_Item_Id).Count();
                if (itemCount == 0)
                {
                    LKP_ItemManager.UpdateHasQuantity(entity.FK_Item_Id, false);
                }
            }
            else
            {
                return ProcessResultHelper.MapToProcessResult<bool, bool>(result);
            }
            return result;
        }
        public override ProcessResult<bool> Delete(List<Allowable_Items_Quatity> entitylst)
        {

            foreach (var item in entitylst)
            {
                var result = Delete(item);
                if (!result.IsSucceeded)
                {
                    return ProcessResultHelper.MapToProcessResult<bool, bool>(result);
                }
            }
            return ProcessResultHelper.Succedded(true);
        }
        public ProcessResult<List<Allowable_Items_Quatity>> Submit(List<int> quantityIds, int itemId)
        {
            ProcessResult<List<Allowable_Items_Quatity>> result = null;
            List<Allowable_Items_Quatity> data = new List<Allowable_Items_Quatity>();
            var deleteRes = DeleteByItemId(itemId);
            if (deleteRes.IsSucceeded)
            {
                foreach (var quantityId in quantityIds)
                {
                    var quantity = new Allowable_Items_Quatity();
                    quantity.FK_Item_Id = itemId;
                    quantity.FK_Quatity_Id = quantityId;
                    var itemRes = Add(quantity);
                    if (itemRes.IsSucceeded)
                    {
                        data.Add(itemRes.Data);
                    }
                    else
                    {
                        result = ProcessResultHelper.MapToProcessResult<Allowable_Items_Quatity, List<Allowable_Items_Quatity>>(itemRes);
                        return result;
                    }
                }
            }
            else
            {
                result = ProcessResultHelper.MapToProcessResult<bool, List<Allowable_Items_Quatity>>(deleteRes);
                return result;

            }
            result = ProcessResultHelper.Succedded(data);
            return result;
        }
        public ProcessResult<bool> DeleteByItemId(int itemId)
        {
            var data = GetAllQuerable().Data.Where(itm => itm.FK_Item_Id == itemId).ToList();
            if (data != null && data.Count > 0)
            {
                return Delete(data);
            }
            else
            {
                return ProcessResultHelper.Succedded(true);
            }
        }

    }
}
