﻿using System;
using System.Collections.Generic;
using System.Text;
using DispatchProduct.RepositoryModule;
using Dispatching.Inventory.Models.Entities;
using Dispatching.Inventory.BLL.IManagers;
using Dispatching.Inventory.Models.Context;
using Utilites.ProcessingResult;
using Microsoft.Extensions.DependencyInjection;
using System.Linq;
using Utilites.PaginatedItems;
using Dispatching.Inventory.BLL.CommonModels;
using CommonEnums;
using System.Linq.Expressions;

namespace Dispatching.Inventory.BLL.Managers
{
    public class LKP_CategoryManager : Repository<LKP_Category>, ILKP_CategoryManager
    {
        IProcessResultMapper processResultMapper;
        IServiceProvider serviceProvider;
        public override ProcessResult<List<LKP_Category>> Add(List<LKP_Category> entityLst)
        {
            for (int i = 0; i < entityLst.Count; i++)
            {
                var addRes = Add(entityLst[i]);
                if (addRes.IsSucceeded)
                {
                    entityLst[i] = addRes.Data;
                }
                else
                {
                    return ProcessResultHelper.MapToProcessResult<LKP_Category, List<LKP_Category>>(addRes);
                }
            }
            return ProcessResultHelper.Succedded(entityLst);
        }

        public override ProcessResult<LKP_Category> Add(LKP_Category entity)
        {
            ProcessResult<LKP_Category> result = null;
            var configRes = LKP_CategoryConfigManager.Get(entity.FK_CategoryConfig_Id);
            if (configRes.IsSucceeded && configRes.Data != null)
            {
                entity.AcceptMulti = configRes.Data.AcceptMulti;
                CheckCategoryAvailablity(entity);
                result = base.Add(entity);
            }
            else
            {
                result = ProcessResultHelper.Failed(entity, new Exception($"No valid Category Configuration entered {entity.FK_CategoryConfig_Id}"));
            }
            return result;
        }

        public override ProcessResult<bool> Update(LKP_Category entity)
        {
            ProcessResult<bool> result = null;
            var configRes = LKP_CategoryConfigManager.Get(entity.FK_CategoryConfig_Id);
            if (configRes.IsSucceeded && configRes.Data != null)
            {
                entity.AcceptMulti = configRes.Data.AcceptMulti;
                CheckCategoryAvailablity(entity);
                result = base.Update(entity);
            }
            else
            {
                result = ProcessResultHelper.Failed(false, new Exception($"No valid Category Configuration entered {entity.FK_CategoryConfig_Id}"));
            }
            return result;
        }

        public LKP_CategoryManager(IServiceProvider _serviceProvider, InventoryDbContext context, IProcessResultMapper _processResultMapper)
            : base(context)
        {
            processResultMapper = _processResultMapper;
            serviceProvider = _serviceProvider;
        }

        public ProcessResult<List<LKP_Category>> GetByCategoryTypeId(int categoryTypeId, bool? IsActive = null)
        {
            List<LKP_Category> data = null;
            ProcessResult<List<LKP_Category>> result = null;
            try
            {
                var processResult = GetAllQuerable();
                if (processResult.IsSucceeded)
                {
                    if (IsActive == null)
                    {
                        data = processResult.Data.Where(r => r.FK_CategoryType_Id == categoryTypeId).ToList();
                        BindCategoryConfig(data);
                        BindCategoryType(data);
                    }
                    else
                    {
                        data = processResult.Data.Where(r => r.FK_CategoryType_Id == categoryTypeId && r.IsActive == IsActive).ToList();
                        BindCategoryConfig(data);
                        BindCategoryType(data);
                    }
                    result = BindCategoryCount(data, IsActive);
                }
                else
                {
                    result = ProcessResultHelper.MapToProcessResult<IQueryable<LKP_Category>, List<LKP_Category>>(processResult);
                }
            }
            catch (Exception ex)
            {
                result = ProcessResultHelper.Failed(data, ex);
            }
            return result;
        }

        public ProcessResult<List<LKP_Category>> GetAll(bool? IsActive = null, SystemCategoryType type = 0,
            Expression<Func<LKP_Category, bool>> expression = null)
        {
            List<LKP_Category> data = null;
            ProcessResult<List<LKP_Category>> result = null;
            try
            {
                if (IsActive == null)
                {
                    data = expression == null ? base.GetAll().Data : base.GetAllQuerable().Data.Where(expression).ToList();
                }
                else
                {
                    if (type == 0)
                    {
                        data = expression == null ? base.GetAllQuerable().Data.Where(r => r.IsActive == IsActive).ToList() :
                            base.GetAllQuerable().Data.Where(r => r.IsActive == IsActive).Where(expression).ToList();
                    }
                    else
                    {
                        data = expression == null ? base.GetAllQuerable().Data.Where(r => r.IsActive == IsActive && r.FK_CategoryType_Id == (int)type).ToList() :
                            base.GetAllQuerable().Data.Where(r => r.IsActive == IsActive && r.FK_CategoryType_Id == (int)type).Where(expression).ToList();
                    }
                }
                BindCategoryConfig(data);
                BindCategoryType(data);
                result = BindCategoryCount(data, IsActive);

            }
            catch (Exception ex)
            {
                result = ProcessResultHelper.Failed(data, ex);
            }
            return result;
        }

        public ProcessResult<List<LKP_Category>> GetAllParents(bool? IsActive = null, SystemCategoryType type = 0)
        {
            Expression<Func<LKP_Category, bool>> isParent = (c => c.IsParent && c.Fk_Parent_Id == null);
            return GetAll(IsActive, type, isParent);            
        }

        public ProcessResult<List<LKP_Category>> GetAllByParentId(int parentId, bool? IsActive = null, SystemCategoryType type = 0)
        {
            Expression<Func<LKP_Category, bool>> isSubFromParent = (c => c.Fk_Parent_Id == parentId);
            return GetAll(IsActive, type, isSubFromParent);
        }


        public ProcessResult<bool> SetAvailableDate(AvailableDateViewModel model)
        {
            bool data = false;
            ProcessResult<bool> result = null;
            try
            {
                var processResult = Get(model.Id);
                if (processResult.IsSucceeded)
                {
                    var obj = processResult.Data;
                    if (obj != null)
                    {
                        obj.IsActive = CheckCategoryAvailablity(obj);
                        obj.AvailableDateFrom = model.DateFrom;
                        obj.AvailableDateTo = model.DateTo;
                    }
                    result = Update(obj);
                }
                else
                {
                    result = ProcessResultHelper.MapToProcessResult<LKP_Category, bool>(processResult);
                }
            }
            catch (Exception ex)
            {
                result = ProcessResultHelper.Failed(data, ex);
            }
            return result;
        }

        public ProcessResult<bool> SetCategoryActivation(int id, bool? isActive)
        {
            bool data = false;
            ProcessResult<bool> result = null;
            try
            {
                var processResult = Get(id);
                if (processResult.IsSucceeded)
                {
                    var obj = processResult.Data;
                    obj.IsActive = isActive;
                    obj.AvailableDateFrom = null;
                    obj.AvailableDateTo = null;
                    result = Update(obj);
                }
                else
                {
                    result = ProcessResultHelper.MapToProcessResult<LKP_Category, bool>(processResult);
                }
            }
            catch (Exception ex)
            {
                result = ProcessResultHelper.Failed(data, ex);
            }
            return result;
        }

        public ProcessResult<IQueryable<LKP_Category>> GetAllQuerable(bool? IsActive = null)
        {
            IQueryable<LKP_Category> data = null;
            ProcessResult<IQueryable<LKP_Category>> result = null;
            try
            {
                if (IsActive == null)
                {
                    data = base.GetAllQuerable().Data;
                }
                else
                {
                    data = base.GetAllQuerable().Data.Where(r => r.IsActive == IsActive);
                }
                result = ProcessResultHelper.Succedded(data);

            }
            catch (Exception ex)
            {
                result = ProcessResultHelper.Failed(data, ex);
            }
            return result;
        }

        public List<LKP_Category> BindCategoryConfig(List<LKP_Category> categories)
        {
            foreach (var item in categories)
            {
                BindCategoryConfig(item);
            }
            return categories;
        }

        public ProcessResult<LKP_Category> BindCategoryConfig(LKP_Category category)
        {
            ProcessResult<LKP_Category> result = null;
            var bindResult = LKP_CategoryConfigManager.GetByCategoryId(category.Id);
            if (bindResult.IsSucceeded && bindResult.Data != null)
            {
                category.Config = bindResult.Data;
                result = ProcessResultHelper.Succedded(category);
            }
            else
            {
                result = ProcessResultHelper.Failed<LKP_Category>(null, new Exception($"Category Configuration not set for this Category {category.Id}"));
            }
            return result;
        }

        public List<LKP_Category> BindCategoryType(List<LKP_Category> categories)
        {
            foreach (var item in categories)
            {
                BindCategoryType(item);
            }
            return categories;
        }

        public ProcessResult<LKP_Category> BindCategoryType(LKP_Category category)
        {
            ProcessResult<LKP_Category> result = null;
            var bindResult = CategoryTypeManager.Get(category.FK_CategoryType_Id);
            if (bindResult.IsSucceeded && bindResult.Data != null)
            {
                category.CategoryType = bindResult.Data;
                result = ProcessResultHelper.Succedded(category);
            }
            else
            {
                result = ProcessResultHelper.Failed<LKP_Category>(null, new Exception($"No Category Type with this Id {category.FK_CategoryType_Id}"));
            }
            return result;
        }

        private ILKP_CategoryConfigManager LKP_CategoryConfigManager
        {
            get { return serviceProvider.GetService<ILKP_CategoryConfigManager>(); }
        }

        private ILKP_CategoryTypeManager CategoryTypeManager
        {
            get { return serviceProvider.GetService<ILKP_CategoryTypeManager>(); }
        }

        private ILKP_ItemManager ItemManager
        {
            get { return serviceProvider.GetService<ILKP_ItemManager>(); }
        }

        private ICombineCategoriesModel CombineCategoriesModel
        {
            get { return serviceProvider.GetService<ICombineCategoriesModel>(); }
        }

        public ProcessResult<bool> UpdateAcceptMulti(bool acceptMulti, int categoryConfigId)
        {
            ProcessResult<bool> result = null;
            var category = GetAllQuerable().Data.Where(cat => cat.FK_CategoryConfig_Id == categoryConfigId).FirstOrDefault();
            if (category != null)
            {
                category.AcceptMulti = acceptMulti;
                result = Update(category);
            }
            else
            {
                result = ProcessResultHelper.Failed(false, new Exception($"No Category has Configuration Id: {categoryConfigId}"));
            }
            return result;
        }

        public ProcessResult<bool> CanCombineItems(List<int> catIds)
        {
            ProcessResult<bool> result = null;
            var categories = GetAllQuerable().Data.Where(itm => catIds.Contains(itm.Id)).ToList();
            var catGrouped = categories.GroupBy(cat => new { cat.AcceptMulti, cat.Id }).
                Select(res => new CombineCategoriesModel { AllowMulti = res.Key.AcceptMulti, CategoryId = res.Key.Id, Count = res.Count(), Categories = res.ToList() }).ToList();
            if (catGrouped.Count > 0)
            {
                var notCombineCount = catGrouped.Where(cat => cat.AllowMulti == false).Select(cat => cat.AllowMulti).ToList();
                if (notCombineCount.Count == 0)
                {
                    result = ProcessResultHelper.Succedded(true);
                }
                else if (notCombineCount.Count == 1 && catGrouped.Count == 1)
                {
                    result = ProcessResultHelper.Succedded(true);
                }
                if (notCombineCount.Count == 1 && catGrouped.Count > 1)
                {
                    result = ProcessResultHelper.Failed(false, new Exception("Can't Submit Cart with Multi Category have at least one not allowed to combine Items and others allow"));
                }
                else if (notCombineCount.Count > 1)
                {
                    result = ProcessResultHelper.Failed(false, new Exception("Can't Submit Cart with Multi Category configured to not allow combine their Items Together"));

                }
            }
            else
            {
                result = ProcessResultHelper.Failed(false, new Exception("can not find Items Categories to check submit cart feasibility"));
            }

            return result;
        }

        private ProcessResult<List<LKP_Category>> BindCategoryCount(List<LKP_Category> list, bool? isActive = null)
        {
            ProcessResult<List<LKP_Category>> result = null;
            foreach (var item in list)
            {
                var countResult = ItemManager.GetCategoryCount(item.Id, isActive);
                if (countResult.IsSucceeded)
                {
                    item.Count = countResult.Data;
                }
                else
                {
                    result = ProcessResultHelper.MapToProcessResult<int, List<LKP_Category>>(countResult);
                }
            }
            result = ProcessResultHelper.Succedded(list);
            return result;
        }

        public ProcessResult<bool> CheckAvailableCategories()
        {
            bool data = false;
            ProcessResult<bool> result = null;

            try
            {
                var processResult = GetAll();
                if (processResult.IsSucceeded)
                {
                    foreach (var item in processResult.Data)
                    {
                        if (item != null)
                        {
                            item.IsActive = CheckCategoryAvailablity(item);
                        }
                    }
                    result = Update(processResult.Data);
                }
                else
                {
                    result = ProcessResultHelper.MapToProcessResult<List<LKP_Category>, bool>(processResult);
                }
            }
            catch (Exception ex)
            {
                result = ProcessResultHelper.Failed(data, ex);
            }
            return result;
        }

        public bool? CheckCategoryAvailablity(LKP_Category item)
        {
            bool result = false;
            var tody = DateTime.Now;
            if (item.AvailableDateFrom != null && item.AvailableDateTo != null)
            {
                if (tody >= item.AvailableDateFrom && tody <= item.AvailableDateTo)
                {
                    result = true;
                }
                else
                {
                    result = false;
                }
            }
            else
            {
                return item.IsActive;
            }
            return result;
        }
    }
}
