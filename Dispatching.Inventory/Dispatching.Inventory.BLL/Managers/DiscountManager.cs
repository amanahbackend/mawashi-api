﻿using System;
using System.Collections.Generic;
using System.Text;
using DispatchProduct.RepositoryModule;
using Dispatching.Inventory.Models.Entities;
using Dispatching.Inventory.BLL.IManagers;
using Dispatching.Inventory.Models.Context;
using Utilites.ProcessingResult;
using Microsoft.Extensions.DependencyInjection;
namespace Dispatching.Inventory.BLL.Managers
{
    public class DiscountManager : Repository<Discount>, IDiscountManager
    {
        IServiceProvider serviceProvider;
        public DiscountManager(InventoryDbContext context, IServiceProvider _serviceProvider)
            : base(context)
        {
            serviceProvider = _serviceProvider;
        }
        public override ProcessResult<bool> Update(List<Discount> entityLst)
        {
            bool data = false;
            foreach (var item in entityLst)
            {
                data = Update(item).Data;
            }
            return ProcessResultHelper.Succedded(data);
        }
        public override ProcessResult<bool> Update(Discount discount)
        {
            try
            {
                var vatChangeResult = IsChanged(discount, nameof(discount.Percentage));
                var result = base.Update(discount);
                if (vatChangeResult.IsSucceeded && vatChangeResult.Data)
                {
                    result = ItemManager.UpdateItemFinalPrice(discount.Id, ItemPropertyChanged.Discount);
                    result = CuttingManager.UpdateItemFinalPrice(discount.Id, ItemPropertyChanged.Discount);

                }
                return result;
            }
            catch (Exception ex)
            {
                return ProcessResultHelper.Failed(false, ex);
            }
        }
        private ILKP_ItemManager ItemManager
        {
            get
            {
                return serviceProvider.GetService<ILKP_ItemManager>();
            }

        }
        private ILKP_CuttingManager CuttingManager
        {
            get
            {
                return serviceProvider.GetService<ILKP_CuttingManager>();
            }

        }
    }
}
