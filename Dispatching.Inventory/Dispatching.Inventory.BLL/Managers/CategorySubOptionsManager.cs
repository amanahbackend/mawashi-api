﻿using System;
using System.Collections.Generic;
using System.Text;
using DispatchProduct.RepositoryModule;
using Dispatching.Inventory.Models.Entities;
using Dispatching.Inventory.BLL.IManagers;
using Dispatching.Inventory.Models.Context;
using System.Linq;
using Utilites.ProcessingResult;

namespace Dispatching.Inventory.BLL.Managers
{
    public class CategorySubOptionsManager : Repository<CategorySubOptions>, ICategorySubOptionsManager
    {
        public CategorySubOptionsManager(InventoryDbContext context)
            : base(context)
        {

        }

        public ProcessResult<List<CategorySubOptions>> GetByCategoryConfigId(int catConfigId)
        {
            var options= GetAllQuerable().Data.Where(c => c.FK_CategoryConfig_Id == catConfigId).ToList();
            return ProcessResultHelper.Succedded(options);
        }
        
    }
}
