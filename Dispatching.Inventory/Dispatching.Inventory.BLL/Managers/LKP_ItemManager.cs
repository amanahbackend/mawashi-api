﻿using BuisnessCommon.Enums;
using CommonEnums;
using Dispatching.BuisnessCommon.Enums;
using Dispatching.Inventory.BLL.CommonModels;
using Dispatching.Inventory.BLL.IManagers;
using Dispatching.Inventory.Models.Context;
using Dispatching.Inventory.Models.Entities;
using DispatchProduct.Inventory.Entities;
using DispatchProduct.RepositoryModule;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Linq;
using Utilites.ProcessingResult;
using Utilities.ProcessingResult;

namespace Dispatching.Inventory.BLL.Managers
{
    public class LKP_ItemManager : Repository<LKP_Item>, ILKP_ItemManager
    {
        IProcessResultMapper processResultMapper;
        ILKP_CategoryManager categoryManager;
        IServiceProvider serviceProvider;

        public LKP_ItemManager(InventoryDbContext context, IProcessResultMapper _processResultMapper, ILKP_CategoryManager _categoryManager, IServiceProvider _serviceProvider)
            : base(context)
        {
            processResultMapper = _processResultMapper;
            categoryManager = _categoryManager;
            serviceProvider = _serviceProvider;
        }
        public override ProcessResult<bool> Update(List<LKP_Item> entityLst)
        {
            bool data = false;
            foreach (var item in entityLst)
            {
                data = Update(item).Data;
            }
            return ProcessResultHelper.Succedded(data);
        }
        public override ProcessResult<bool> Update(LKP_Item item)
        {
            if (item != null)
            {
                var priceChangeResult = IsChanged(item, nameof(item.Price));
                var vatChangeResult = IsChanged(item, nameof(item.FK_VAT_Id));
                var discountChangeResult = IsChanged(item, nameof(item.FK_Discount_Id));
                item.IsActive = CheckItemAvailablity(item);
                var result = base.Update(item);
                //if (priceChangeResult.Data || vatChangeResult.Data || discountChangeResult.Data)
                var finalPriceresult = UpdateItemFinalPrice(item.Id, ItemPropertyChanged.Price);
                if (finalPriceresult.IsSucceeded)
                {
                    return result;
                }
                else
                {
                    return ProcessResultHelper.MapToProcessResult<bool, bool>(finalPriceresult);
                }
            }
            else
            {
                return ProcessResultHelper.Failed(false, new Exception("Parameter Item can not be null"));
            }
        }
        public ProcessResult<bool> UpdateStock(int itemId, int count, OrderType orderType, MeasurmentType measurmentType)
        {
            ProcessResult<bool> result = null;
            var itemRes = Get(itemId);
            if (itemRes.IsSucceeded && itemRes.Data != null)
            {
                if (orderType == OrderType.Individual)
                {
                    result = UpdateIndividualStock(itemRes.Data, count);
                }
                else if (orderType == OrderType.Retail && measurmentType > 0)
                {
                    result = UpdateSalesStock(itemRes.Data, count, measurmentType);
                }
                else
                {
                    result = ProcessResultHelper.Failed(false, new Exception($"there is no valid orderType or measurmentType  to update Stocks based on"));
                }
            }
            else
            {
                result = ProcessResultHelper.Failed(false, new Exception($"there is no item with this id {itemId} to update Stock"));
            }
            return result;
        }
        private ProcessResult<bool> UpdateIndividualStock(LKP_Item model, int count)
        {
            ProcessResult<bool> result = null;
            if (model.CurrentStock >= count)
            {
                model.CurrentStock = model.CurrentStock - count;
                if (model.CurrentStock == 0)
                {
                    model.IsActive = false;
                }
                result = Update(model);
            }
            else
            {
                result = ProcessResultHelper.Failed(false, new Exception($"there is not enough quantity in CurrentStock for Individual from item with English Name: {model.NameEN} , Arabic Name: {model.NameAR}"));
            }
            return result;
        }
        private ProcessResult<bool> UpdateSalesStock(LKP_Item model, int count, MeasurmentType measurmentType)
        {
            ProcessResult<bool> result = null;
            switch (measurmentType)
            {
                case MeasurmentType.Head:
                    if (model.CurrentHeadStock >= count)
                    {
                        model.CurrentHeadStock = model.CurrentHeadStock - count;
                        if (model.CurrentHeadStock == 0)
                        {
                            model.IsActive = false;
                        }
                        result = Update(model);
                    }
                    else
                    {
                        result = ProcessResultHelper.Failed(false, new Exception($"there is not enough quantity in CurrentHeadStock:{model.CurrentHeadStock} to deduct {count} Heads. Item with English Name: {model.NameEN} , Arabic Name: {model.NameAR}"));
                    }
                    break;
                case MeasurmentType.Kgs:
                    if (model.CurrentKgsStock >= count)
                    {
                        model.CurrentKgsStock = model.CurrentKgsStock - count;
                        if (model.CurrentKgsStock == 0)
                        {
                            model.IsActive = false;
                        }
                        result = Update(model);
                    }
                    else
                    {
                        result = ProcessResultHelper.Failed(false, new Exception($"there is not enough quantity in CurrentKgsStock:{model.CurrentKgsStock} to deduct {count} Kgs. Item with English Name: {model.NameEN} , Arabic Name: {model.NameAR}"));
                    }
                    break;
                case MeasurmentType.Pieces:
                    if (model.CurrentPiecesStock >= count)
                    {
                        model.CurrentPiecesStock = model.CurrentPiecesStock - count;
                        if (model.CurrentPiecesStock == 0)
                        {
                            model.IsActive = false;
                        }
                        result = Update(model);
                    }
                    else
                    {
                        result = ProcessResultHelper.Failed(false, new Exception($"there is not enough quantity in CurrentPiecesStock:{model.CurrentPiecesStock} to deduct {count} Pieces. Item with English Name: {model.NameEN} , Arabic Name: {model.NameAR}"));
                    }
                    break;
                default:
                    result = ProcessResultHelper.Failed(false, new Exception($"there is not Valid Type for MeasurmentType to updateStock based on"));
                    break;
            }
            return result;
        }
        public override ProcessResult<List<LKP_Item>> Add(List<LKP_Item> entityLst)
        {
            for (int i = 0; i < entityLst.Count; i++)
            {
                var result = Add(entityLst[i]);
                if (result.IsSucceeded)
                {
                    entityLst[i] = result.Data;
                }
                else
                {
                    return ProcessResultHelper.MapToProcessResult<LKP_Item, List<LKP_Item>>(result);
                }
            }
            return ProcessResultHelper.Succedded(entityLst);
        }
        public override ProcessResult<LKP_Item> Add(LKP_Item entity)
        {
            if (entity != null)
            {
                entity.IsActive = CheckItemAvailablity(entity);
                var result = base.Add(entity);
                var finalPriceresult = UpdateItemFinalPrice(entity.Id, ItemPropertyChanged.Price);
                if (finalPriceresult.IsSucceeded)
                {
                    return result;
                }
                else
                {
                    return ProcessResultHelper.MapToProcessResult<bool, LKP_Item>(finalPriceresult);
                }
            }
            else
            {
                return ProcessResultHelper.Failed(entity, new Exception("Parameter Item can not be null"));
            }
        }
        public ProcessResult<bool> SetAvailableDate(AvailableDateViewModel model)
        {
            bool data = false;
            ProcessResult<bool> result = null;
            try
            {
                var processResult = Get(model.Id);
                if (processResult.IsSucceeded)
                {
                    var obj = processResult.Data;
                    if (obj != null)
                    {
                        obj.IsActive = CheckItemAvailablity(obj);
                        obj.AvailableDateFrom = model.DateFrom;
                        obj.AvailableDateTo = model.DateTo;
                    }
                    result = Update(obj);
                }
                else
                {
                    result = ProcessResultHelper.MapToProcessResult<LKP_Item, bool>(processResult);
                }
            }
            catch (Exception ex)
            {
                result = ProcessResultHelper.Failed(data, ex);
            }
            return result;
        }
        public ProcessResult<bool> SetItemActivation(int id, bool? isActive)
        {
            bool data = false;
            ProcessResult<bool> result = null;
            try
            {
                var processResult = Get(id);
                if (processResult.IsSucceeded)
                {
                    var obj = processResult.Data;
                    obj.IsActive = isActive;
                    obj.AvailableDateFrom = null;
                    obj.AvailableDateTo = null;
                    result = Update(obj);
                }
                else
                {
                    result = ProcessResultHelper.MapToProcessResult<LKP_Item, bool>(processResult);
                }
            }
            catch (Exception ex)
            {
                result = ProcessResultHelper.Failed(data, ex);
            }
            return result;
        }
        public ProcessResult<bool> CheckAvailableIems()
        {
            bool data = false;
            ProcessResult<bool> result = null;

            try
            {
                var processResult = GetAll();
                if (processResult.IsSucceeded)
                {
                    var tody = DateTime.Now;
                    foreach (var item in processResult.Data)
                    {
                        if (item != null)
                        {
                            item.IsActive = CheckItemAvailablity(item);
                        }
                    }
                    result = Update(processResult.Data);
                }
                else
                {
                    result = ProcessResultHelper.MapToProcessResult<List<LKP_Item>, bool>(processResult);
                }
            }
            catch (Exception ex)
            {
                result = ProcessResultHelper.Failed(data, ex);
            }
            return result;
        }
        public bool? CheckItemAvailablity(LKP_Item item)
        {
            bool result = false;
            var tody = DateTime.Now;
            if (item.AvailableDateFrom != null && item.AvailableDateTo != null)
            {
                if (tody >= item.AvailableDateFrom && tody <= item.AvailableDateTo)
                {
                    result = true;
                }
                else
                {
                    result = false;
                }
            }
            else
            {
                return item.IsActive;
            }
            return result;
        }
        public ProcessResult<List<LKP_Item>> GetByCategoryId(int categoryTypeId, bool? IsActive = null)
        {
            List<LKP_Item> data = null;
            ProcessResult<List<LKP_Item>> result = null;
            try
            {
                var processResult = GetAllQuerable();
                if (processResult.IsSucceeded)
                {
                    if (IsActive == null)
                    {
                        data = processResult.Data.Where(r => r.FK_Category_Id == categoryTypeId).ToList();
                    }
                    else
                    {
                        data = processResult.Data.Where(r => r.FK_Category_Id == categoryTypeId && r.IsActive == IsActive).ToList();
                    }
                    data = Sort(data);
                    result = ProcessResultHelper.Succedded(data);
                }
                else
                {
                    result = ProcessResultHelper.MapToProcessResult<IQueryable<LKP_Item>, List<LKP_Item>>(processResult);
                }
            }
            catch (Exception ex)
            {
                result = ProcessResultHelper.Failed(data, ex);
            }
            return result;
        }
        public ProcessResult<List<LKP_Item>> GetAll(bool? IsActive = null, SystemCategoryType type = 0)
        {
            List<LKP_Item> data = null;
            ProcessResult<List<LKP_Item>> result = null;
            try
            {
                if (IsActive == null)
                {
                    data = base.GetAll().Data;
                }
                else
                {
                    IQueryable<int> categoriesIds;
                    if (type == 0)
                    {
                        categoriesIds = categoryManager.GetAllQuerable(!IsActive).Data.Select(r => r.Id);
                    }
                    else
                    {
                        categoriesIds = categoryManager.GetAllQuerable(!IsActive).Data.
                            Where(c => c.FK_CategoryType_Id == (int)type).Select(r => r.Id);
                    }
                    data = base.GetAllQuerable().Data.Where(r => r.IsActive == IsActive && !categoriesIds.Contains(r.FK_Category_Id)).ToList();
                }
                result = ProcessResultHelper.Succedded(data);

            }
            catch (Exception ex)
            {
                result = ProcessResultHelper.Failed(data, ex);
            }
            return result;
        }
        public ProcessResult<List<LKP_Item>> GetByVatId(int vatId, bool? IsActive = null)
        {
            List<LKP_Item> data = null;
            ProcessResult<List<LKP_Item>> result = null;
            try
            {
                if (IsActive == null)
                {
                    data = base.GetAllQuerable().Data.Where(vat => vat.FK_VAT_Id == vatId).ToList();
                }
                else
                {
                    var categoriesIds = categoryManager.GetAllQuerable(!IsActive).Data.Select(r => r.Id);
                    data = base.GetAllQuerable().Data.Where(r => r.IsActive == IsActive && !categoriesIds.Contains(r.FK_Category_Id) && r.FK_VAT_Id == vatId).ToList();
                }
                result = ProcessResultHelper.Succedded(data);

            }
            catch (Exception ex)
            {
                result = ProcessResultHelper.Failed(data, ex);
            }
            return result;
        }
        public ProcessResult<List<LKP_Item>> GetByDiscountId(int discountId, bool? IsActive = null)
        {
            List<LKP_Item> data = null;
            ProcessResult<List<LKP_Item>> result = null;
            try
            {
                if (IsActive == null)
                {
                    data = base.GetAllQuerable().Data.Where(vat => vat.FK_Discount_Id == discountId).ToList();
                }
                else
                {
                    var categoriesIds = categoryManager.GetAllQuerable(!IsActive).Data.Select(r => r.Id);
                    data = base.GetAllQuerable().Data.Where(r => r.IsActive == IsActive && !categoriesIds.Contains(r.FK_Category_Id) && r.FK_Discount_Id == discountId).ToList();
                }
                result = ProcessResultHelper.Succedded(data);

            }
            catch (Exception ex)
            {
                result = ProcessResultHelper.Failed(data, ex);
            }
            return result;
        }

        private List<LKP_Item> Sort(List<LKP_Item> items)
        {
            var categoryId = items.FirstOrDefault().FK_Category_Id;
            var categoryResult = categoryManager.Get(categoryId);
            if (categoryResult.IsSucceeded && categoryResult.Data != null)
            {
                switch (categoryResult.Data.ItemsSortType)
                {
                    case SortType.Ascending:
                    default:
                        items = items.OrderBy(x => x.Id).ToList();
                        break;
                    case SortType.Descending:
                        items = items.OrderByDescending(x => x.Id).ToList();
                        break;
                }
            }
            return items;
        }

        public ProcessResult<bool> UpdateItemsVat(int vatId)
        {
            try
            {
                var data = true;
                var vatResult = GetVat(vatId);
                if (vatResult.IsSucceeded)
                {
                    if (vatResult.Data != null)
                    {
                        var vatPercentage = vatResult.Data.Percentage;
                        var itemResultByVatId = GetByVatId(vatId);
                        if (itemResultByVatId.IsSucceeded)
                        {
                            itemResultByVatId.Data = CalculateItemsVat(itemResultByVatId.Data, vatPercentage);
                            itemResultByVatId.Data = CalculateItemsFinalPrice(itemResultByVatId.Data);
                            var updateresult = base.Update(itemResultByVatId.Data);
                            if (updateresult.IsSucceeded)
                            {
                                data = updateresult.Data;
                                return ProcessResultHelper.Succedded(data);
                            }
                            else
                            {
                                return ProcessResultHelper.MapToProcessResult<bool, bool>(updateresult);
                            }

                        }
                        else
                        {
                            return ProcessResultHelper.MapToProcessResult<List<LKP_Item>, bool>(itemResultByVatId);
                        }
                    }
                    else
                    {
                        return ProcessResultHelper.Failed(false, new ArgumentOutOfRangeException("there is No VAT Instance with this Id"), "there is No VAT Instance with this Id", ProcessResultStatusCode.InvalidValue);
                    }
                }
                else
                {
                    return ProcessResultHelper.MapToProcessResult<LKP_VAT, bool>(vatResult);
                }
            }
            catch (Exception ex)
            {
                var result = false;
                return ProcessResultHelper.Failed(result, ex);
            }

        }
        public ProcessResult<bool> UpdateItemsDiscount(int discountId)
        {
            try
            {
                var data = true;
                var discountResult = GetDiscount(discountId);
                if (discountResult.IsSucceeded)
                {
                    if (discountResult.Data != null)
                    {
                        var discountPercentage = discountResult.Data.Percentage;
                        var itemResultBydiscountId = GetByDiscountId(discountId);
                        if (itemResultBydiscountId.IsSucceeded)
                        {
                            itemResultBydiscountId.Data = CalculateItemsDiscount(itemResultBydiscountId.Data, discountPercentage);
                            itemResultBydiscountId.Data = CalculateItemsFinalPrice(itemResultBydiscountId.Data);
                            var updateresult = base.Update(itemResultBydiscountId.Data);
                            if (updateresult.IsSucceeded)
                            {
                                data = updateresult.Data;
                                return ProcessResultHelper.Succedded(data);
                            }
                            else
                            {
                                return ProcessResultHelper.MapToProcessResult<bool, bool>(updateresult);
                            }

                        }
                        else
                        {
                            return ProcessResultHelper.MapToProcessResult<List<LKP_Item>, bool>(itemResultBydiscountId);
                        }
                    }
                    else
                    {
                        return ProcessResultHelper.Failed(false, new ArgumentOutOfRangeException("there is No discount Instance with this Id"), "there is No discount Instance with this Id", ProcessResultStatusCode.InvalidValue);
                    }
                }
                else
                {
                    return ProcessResultHelper.MapToProcessResult<Discount, bool>(discountResult);
                }
            }
            catch (Exception ex)
            {
                var result = false;
                return ProcessResultHelper.Failed(result, ex);
            }
        }
        public ProcessResult<bool> UpdateItemPrice(int itemId)
        {
            var itemresult = Get(itemId);
            if (itemresult.IsSucceeded && itemresult.Data != null)
            {
                var itemVatResult = GetVat(itemresult.Data.FK_VAT_Id);
                var itemDiscountResult = GetDiscount(itemresult.Data.FK_Discount_Id);
                if (itemVatResult.IsSucceeded && itemVatResult.Data != null)
                {
                    itemresult.Data = CalculateItemVat(itemresult.Data, itemVatResult.Data.Percentage);
                }
                if (itemDiscountResult.IsSucceeded && itemDiscountResult.Data != null)
                {
                    itemresult.Data = CalculateItemDiscount(itemresult.Data, itemDiscountResult.Data.Percentage);
                }
                itemresult.Data = CalculateItemFinalPrice(itemresult.Data);
                return base.Update(itemresult.Data);
            }
            else
            {
                return ProcessResultHelper.MapToProcessResult<LKP_Item, bool>(itemresult);
            }
        }
        public ProcessResult<bool> UpdateItemFinalPrice(int id, ItemPropertyChanged propChanged)
        {

            if (propChanged == ItemPropertyChanged.Discount)
            {
                // id here equal to discount Id
                return UpdateItemsDiscount(id);

            }
            else if (propChanged == ItemPropertyChanged.VAT)
            {
                // id here equal to Vat Id
                return UpdateItemsVat(id);


            }
            else if (propChanged == ItemPropertyChanged.Price)
            {
                // id here equal to ItemId 
                return UpdateItemPrice(id);
            }

            return ProcessResultHelper.Failed(false, new ArgumentOutOfRangeException("ItemPropertyChanged not valid value"), "ItemPropertyChanged not valid value");
        }
        private ProcessResult<LKP_VAT> GetVat(int vatId)
        {
            var vatManager = serviceProvider.GetService<ILKP_VATManager>();
            return vatManager.Get(vatId);
        }
        private ILKP_Item Item
        {
            get { return serviceProvider.GetService<ILKP_Item>(); }
        }
        public ProcessResult<ItemDetailsModel> GetItemDetails(int itemId)
        {
            ProcessResult<ItemDetailsModel> result = null;
            if (ItemDetailsModel != null)
            {
                var cuttingResult = GetCuttingByItemId(itemId);
                if (cuttingResult.IsSucceeded)
                {
                    ItemDetailsModel.Cuttings = cuttingResult.Data;
                }
                else
                {
                    result = ProcessResultHelper.MapToProcessResult<List<Allowable_Items_Cutting>, ItemDetailsModel>(cuttingResult);
                }
                var quantityResult = GetQuantitiesByItemId(itemId);
                if (quantityResult.IsSucceeded)
                {
                    ItemDetailsModel.Quantities = quantityResult.Data;
                }
                else
                {
                    result = ProcessResultHelper.MapToProcessResult<List<Allowable_Items_Quatity>, ItemDetailsModel>(quantityResult);
                }
                result = ProcessResultHelper.Succedded(ItemDetailsModel);
            }
            else
            {
                result = ProcessResultHelper.Failed<ItemDetailsModel>(null, new Exception("Can't Get Instance from IItemDetailsModel"));
            }
            if (result.IsSucceeded && result.Data != null)
            {
                result.Data.Fk_Item_Id = itemId;

            }
            return result;
        }

        private ProcessResult<List<Allowable_Items_Cutting>> GetCuttingByItemId(int itemId)
        {
            var cuttingManager = serviceProvider.GetService<IAllowable_Items_CuttingManager>();
            return cuttingManager.GetByItemId(itemId);
        }

        private ItemDetailsModel ItemDetailsModel
        {
            get { return serviceProvider.GetService<ItemDetailsModel>(); }
        }

        private ILKP_CategoryManager CategoryManager
        {
            get { return serviceProvider.GetService<ILKP_CategoryManager>(); }
        }
        private ProcessResult<List<Allowable_Items_Quatity>> GetQuantitiesByItemId(int itemId)
        {
            var quantityManager = serviceProvider.GetService<IAllowable_Items_QuatityManager>();
            return quantityManager.GetByItemId(itemId);
        }

        private ProcessResult<Discount> GetDiscount(int discountId)
        {
            var discountManager = serviceProvider.GetService<IDiscountManager>();
            return discountManager.Get(discountId);
        }
        private List<LKP_Item> CalculateItemsVat(List<LKP_Item> items, double percentage)
        {
            foreach (var item in items)
            {
                CalculateItemVat(item, percentage);
            }
            return items;
        }
        private LKP_Item CalculateItemVat(LKP_Item item, double percentage)
        {
            item.VATValue = (item.Price * percentage) / 100;
            return item;
        }
        private LKP_Item CalculateItemDiscount(LKP_Item item, double percentage)
        {
            item.DiscountValue = (item.Price * percentage) / 100;
            return item;
        }
        private List<LKP_Item> CalculateItemsDiscount(List<LKP_Item> items, double percentage)
        {
            foreach (var item in items)
            {
                CalculateItemDiscount(item, percentage);
            }
            return items;
        }
        private LKP_Item CalculateItemFinalPrice(LKP_Item item)
        {
            //item.FinalPrice = item.Price + item.VATValue - item.DiscountValue;
            item.FinalPrice = item.Price;
            return item;
        }
        private List<LKP_Item> CalculateItemsFinalPrice(List<LKP_Item> items)
        {
            foreach (var item in items)
            {
                CalculateItemFinalPrice(item);
            }
            return items;
        }
        public ProcessResult<bool> UpdateHasQuantity(int itemId, bool hasQuantity)
        {
            ProcessResult<bool> result;
            var itemResult = Get(itemId);
            if (itemResult.IsSucceeded && itemResult.Data != null)
            {
                itemResult.Data.HasQuantities = hasQuantity;
                result = Update(itemResult.Data);
            }
            else
            {
                result = ProcessResultHelper.Failed(false, new ArgumentOutOfRangeException($"Item with Id {itemId} doesn't exist"));
            }
            return result;
        }
        public ProcessResult<bool> UpdateHasCuttings(int itemId, bool hasCutting)
        {
            ProcessResult<bool> result;
            var itemResult = Get(itemId);
            if (itemResult.IsSucceeded && itemResult.Data != null)
            {
                itemResult.Data.HasCuttings = hasCutting;
                result = Update(itemResult.Data);
            }
            else
            {
                result = ProcessResultHelper.Failed(false, new ArgumentOutOfRangeException($"Item with Id {itemId} doesn't exist"));
            }
            return result;
        }
        public ProcessResult<bool> CanCombineItems(List<int> itemIds)
        {
            ProcessResult<bool> result = null;
            List<int> catIds = GetAllQuerable().Data.Where(itm => itemIds.Contains(itm.Id)).Select(itm => itm.FK_Category_Id).ToList();
            if (catIds.Count > 0)
            {
                result = CategoryManager.CanCombineItems(catIds);
            }
            else
            {
                result = ProcessResultHelper.Failed(false, new Exception("Can't find Category ids Configured for these Items"));

            }
            return result;
        }
        public ProcessResult<int> GetCategoryCount(int catId, bool? isActive = null)
        {
            int result;
            if (isActive != null)
            {
                result = GetAllQuerable().Data.Where(itm => itm.FK_Category_Id == catId && itm.IsActive == isActive).Count();
            }
            else
            {
                result = GetAllQuerable().Data.Where(itm => itm.FK_Category_Id == catId).Count();
            }
            return ProcessResultHelper.Succedded(result);
        }
    }
    public enum ItemPropertyChanged
    {
        Price = 1,
        VAT = 2,
        Discount = 3
    }
}
