﻿using System;
using System.Collections.Generic;
using System.Text;
using DispatchProduct.RepositoryModule;
using Dispatching.Inventory.Models.Entities;
using Dispatching.Inventory.BLL.IManagers;
using Dispatching.Inventory.Models.Context;
using Microsoft.Extensions.DependencyInjection;
using System.Linq;
using Utilites.ProcessingResult;
using CommonEnums;

namespace Dispatching.Inventory.BLL.Managers
{
    public class LKP_CategoryConfigManager : Repository<LKP_CategoryConfig>, ILKP_CategoryConfigManager
    {
        IServiceProvider serviceProvider;
        public LKP_CategoryConfigManager(IServiceProvider _serviceProvider, InventoryDbContext context)
            : base(context)
        {
            serviceProvider = _serviceProvider;
        }
        public ProcessResult<LKP_CategoryConfig> GetByCategoryId(int catId)
        {
            ProcessResult<LKP_CategoryConfig> result = null;
            var categoryConfigId = CategoryManager.GetAllQuerable().Data.Where(c => c.Id == catId)
                .Select(c=>c.FK_CategoryConfig_Id).FirstOrDefault();
            if(categoryConfigId>0)
            {
                var catConfig = GetAllQuerable().Data.Where(c => c.Id == categoryConfigId).FirstOrDefault();
               
                if (catConfig != null)
                {
                    var optionResult = CategorySubOptionsManager.GetByCategoryConfigId(catConfig.Id);
                    if (optionResult.IsSucceeded && optionResult.Data != null)
                    {
                        catConfig.CategorySubOptions = optionResult.Data;
                        result = ProcessResultHelper.Succedded(catConfig);
                    }
                }
                else
                {
                    result = ProcessResultHelper.Failed<LKP_CategoryConfig>(null, new Exception($"No Category configuration for This Id {catId}"));

                }
            }
            else
            {
                result = ProcessResultHelper.Failed<LKP_CategoryConfig>(null, new Exception($"No Category  for This Id {catId}"));

            }
            return result;
        }
        private ICategorySubOptionsManager CategorySubOptionsManager
        {
            get { return serviceProvider.GetService<ICategorySubOptionsManager>(); }
        }

        private ICategoryDeliveryOptionsManager CategoryDeliveryOptionsManager
        {
            get { return serviceProvider.GetService<ICategoryDeliveryOptionsManager>(); }
        }
        private ILKP_CategoryManager CategoryManager
        {
            get { return serviceProvider.GetService<ILKP_CategoryManager>(); }
        }
        public ProcessResult<List<CategoryDeliveryOptions>> GetDeliveryOptionsByTypeId(int configId)
        {
            ProcessResult<List<CategoryDeliveryOptions>> result = null;
            var deliveryResult = CategoryDeliveryOptionsManager.GetByCategoryConfigId(configId);
            if (deliveryResult.IsSucceeded && deliveryResult.Data != null)
            {
                result = deliveryResult;
            }
            return result;
        }
        public ProcessResult<List<EnumEntity>> GetDeliveryOptionsByConfigId(int configId)
        {
            ProcessResult<List<EnumEntity>> result = null;
            var devResult = GetDeliveryOptionsByTypeId(configId);
            if (devResult.IsSucceeded && devResult.Data != null)
            {
                var enumValues = devResult.Data.Select(val => val.DeliveryType).ToList();
                var data = EnumManager<DeliveryType>.GetEnumObjects(enumValues);
                result= ProcessResultHelper.Succedded(data);
            }
            else
            {
                result=ProcessResultHelper.Failed<List<EnumEntity>>(null,new Exception($"No Delivery options configured for this Catgory defined By {configId}"));
            }
            return result;
        }
        public ProcessResult<bool> UpdateAcceptMulti(bool acceptMulti, int configId)
        {
            ProcessResult<bool> result = null;
            var categoryRes = Get(configId);
            if (categoryRes.IsSucceeded && categoryRes.Data != null)
            {
                categoryRes.Data.AcceptMulti = acceptMulti;
                result = Update(categoryRes.Data);
                if (result.IsSucceeded)
                {
                    CategoryManager.UpdateAcceptMulti(acceptMulti, configId);
                }

            }
            else
            {
                result = ProcessResultHelper.Failed(false, new Exception($"No Category has Id {configId}"));
            }
            return result;
        }
        public override ProcessResult<LKP_CategoryConfig> Add(LKP_CategoryConfig entity)
        {
         
            var result= base.Add(entity);
            if (result.IsSucceeded)
            {
                CategoryManager.UpdateAcceptMulti(entity.AcceptMulti, entity.Id);
            }
            return result;
        }
        public override ProcessResult<List<LKP_CategoryConfig>> Add(List<LKP_CategoryConfig> entityLst)
        {
            return Add(entityLst);
        }

    }
}
