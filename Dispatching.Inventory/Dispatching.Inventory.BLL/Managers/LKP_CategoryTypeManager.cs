﻿using System;
using System.Collections.Generic;
using System.Text;
using DispatchProduct.RepositoryModule;
using Dispatching.Inventory.Models.Entities;
using Dispatching.Inventory.BLL.IManagers;
using Dispatching.Inventory.Models.Context;

namespace Dispatching.Inventory.BLL.Managers
{
    public class LKP_CategoryTypeManager : Repository<LKP_CategoryType>, ILKP_CategoryTypeManager
    {
        public LKP_CategoryTypeManager(InventoryDbContext context)
            : base(context)
        {

        }

    }
}
