﻿using System;
using System.Collections.Generic;
using DispatchProduct.RepositoryModule;
using Dispatching.Inventory.Models.Entities;
using System.Text;
using Dispatching.Inventory.BLL.IManagers;
using Dispatching.Inventory.Models.Context;
using Utilites.ProcessingResult;
using System.Linq;
using Microsoft.Extensions.DependencyInjection;

namespace Dispatching.Inventory.BLL.Managers
{
    public class Allowable_Items_CuttingManager : Repository<Allowable_Items_Cutting>, IAllowable_Items_CuttingManager
    {
        IServiceProvider serviceProvider;
        public Allowable_Items_CuttingManager(InventoryDbContext context, IServiceProvider _serviceProvider)
            : base(context)
        {
            serviceProvider = _serviceProvider;
        }
        public ProcessResult<List<Allowable_Items_Cutting>> GetByItemId(int itemId)
        {
            List<Allowable_Items_Cutting> data = null;
            ProcessResult<List<Allowable_Items_Cutting>> result = null;
            try
            {
                data = GetAllQuerable().Data.Where(r => r.FK_Item_Id == itemId).ToList();
                foreach (var item in data)
                {
                    var cuttingRes = CuttingManager.Get(item.FK_Cutting_Id);
                    if (cuttingRes.IsSucceeded)
                        item.Cutting = cuttingRes.Data;
                }
                result = ProcessResultHelper.Succedded(data);
            }
            catch (Exception ex)
            {
                result = ProcessResultHelper.Failed(data, ex);
            }
            return result;
        }
        private ILKP_CuttingManager CuttingManager
        {
            get { return serviceProvider.GetService<ILKP_CuttingManager>(); }
        }
        private ILKP_ItemManager LKP_ItemManager
        {
            get { return serviceProvider.GetService<ILKP_ItemManager>(); }
        }

        private IAllowable_Items_Cutting AllowCut
        {
            get { return serviceProvider.GetService<IAllowable_Items_Cutting>(); }
        }
        public override ProcessResult<Allowable_Items_Cutting> Add(Allowable_Items_Cutting entity)
        {
            var result = base.Add(entity);
            if (result.IsSucceeded)
            {
                LKP_ItemManager.UpdateHasCuttings(entity.FK_Item_Id, true);
            }
            return result;
        }
        public override ProcessResult<List<Allowable_Items_Cutting>> Add(List<Allowable_Items_Cutting> entityLst)
        {
            foreach (var item in entityLst)
            {
                var result = Add(item);
                if (!result.IsSucceeded)
                {
                    return ProcessResultHelper.MapToProcessResult<Allowable_Items_Cutting, List<Allowable_Items_Cutting>>(result);
                }
            }
            return ProcessResultHelper.Succedded(entityLst);
        }
        public override ProcessResult<bool> Delete(Allowable_Items_Cutting entity)
        {
            var result = base.Delete(entity);
            if (result.IsSucceeded)
            {
                var itemCount = GetAllQuerable().Data.Where(r => r.FK_Item_Id == entity.FK_Item_Id).Count();
                if (itemCount == 0)
                {
                    LKP_ItemManager.UpdateHasCuttings(entity.FK_Item_Id, false);
                }
            }
            else
            {
                return ProcessResultHelper.MapToProcessResult<bool, bool>(result);
            }
            return result;
        }
        public override ProcessResult<bool> Delete(List<Allowable_Items_Cutting> entitylst)
        {

            foreach (var item in entitylst)
            {
                var result = Delete(item);
                if (!result.IsSucceeded)
                {
                    return ProcessResultHelper.MapToProcessResult<bool, bool>(result);
                }
            }
            return ProcessResultHelper.Succedded(true);
        }
        public ProcessResult<List<Allowable_Items_Cutting>> Submit(List<int> cuttingIds, int itemId)
        {
            ProcessResult<List<Allowable_Items_Cutting>> result = null;
            List<Allowable_Items_Cutting> data = new List<Allowable_Items_Cutting>();
            var deleteRes = DeleteByItemId(itemId);
            if (deleteRes.IsSucceeded)
            {
                foreach (var cutId in cuttingIds)
                {
                    var cutting = new Allowable_Items_Cutting();
                    cutting.FK_Item_Id = itemId;
                    cutting.FK_Cutting_Id = cutId;
                    var itemRes = Add(cutting);
                    if (itemRes.IsSucceeded)
                    {
                        data.Add(itemRes.Data);
                    }
                    else
                    {
                        result = ProcessResultHelper.MapToProcessResult<Allowable_Items_Cutting, List<Allowable_Items_Cutting>>(itemRes);
                        return result;
                    }
                }
            }
            else
            {
                result = ProcessResultHelper.MapToProcessResult<bool, List<Allowable_Items_Cutting>>(deleteRes);
                return result;
            }
            result = ProcessResultHelper.Succedded(data);
            return result;
        }
        public ProcessResult<bool> DeleteByItemId(int itemId)
        {
            var data = GetAllQuerable().Data.Where(itm => itm.FK_Item_Id == itemId).ToList();
            if (data != null && data.Count > 0)
            {
                return Delete(data);
            }
            else
            {
                return ProcessResultHelper.Succedded(true);
            }
        }
    }
}
