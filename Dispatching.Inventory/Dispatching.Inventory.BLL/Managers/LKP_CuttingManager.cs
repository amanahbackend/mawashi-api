﻿using System;
using System.Collections.Generic;
using System.Text;
using DispatchProduct.RepositoryModule;
using Dispatching.Inventory.Models.Entities;
using Dispatching.Inventory.BLL.IManagers;
using Dispatching.Inventory.Models.Context;
using Utilites.ProcessingResult;
using System.Linq;
using Microsoft.Extensions.DependencyInjection;
using Utilities.ProcessingResult;

namespace Dispatching.Inventory.BLL.Managers
{
    public class LKP_CuttingManager : Repository<LKP_Cutting>, ILKP_CuttingManager
    {
        IServiceProvider serviceProvider;
        public LKP_CuttingManager(InventoryDbContext context, IServiceProvider _serviceProvider)
            : base(context)
        {
            serviceProvider = _serviceProvider;
        }
        public override ProcessResult<bool> Update(List<LKP_Cutting> entityLst)
        {
            bool data = false;
            foreach (var item in entityLst)
            {
                data = Update(item).Data;
            }
            return ProcessResultHelper.Succedded(data);
        }
        public override ProcessResult<bool> Update(LKP_Cutting item)
        {
            var priceChangeResult = IsChanged(item, nameof(item.Price));
            var vatChangeResult = IsChanged(item, nameof(item.FK_VAT_Id));
            var discountChangeResult = IsChanged(item, nameof(item.FK_Discount_Id));
            var result = base.Update(item);
            //if (priceChangeResult.Data || vatChangeResult.Data || discountChangeResult.Data)
            var finalPriceresult = UpdateItemFinalPrice(item.Id, ItemPropertyChanged.Price);
            if (finalPriceresult.IsSucceeded)
            {
                return result;
            }
            else
            {
                return ProcessResultHelper.MapToProcessResult<bool, bool>(finalPriceresult);
            }
        }
        public override ProcessResult<List<LKP_Cutting>> Add(List<LKP_Cutting> entityLst)
        {
            for (int i = 0; i < entityLst.Count; i++)
            {
                var result = Add(entityLst[i]);
                if (result.IsSucceeded)
                {
                    entityLst[i] = result.Data;
                }
                else
                {
                    return ProcessResultHelper.MapToProcessResult<LKP_Cutting, List<LKP_Cutting>>(result);
                }
            }
            return ProcessResultHelper.Succedded(entityLst);
        }
        public override ProcessResult<LKP_Cutting> Add(LKP_Cutting entity)
        {
            var result = base.Add(entity);
            var finalPriceresult = UpdateItemFinalPrice(entity.Id, ItemPropertyChanged.Price);
            if (finalPriceresult.IsSucceeded)
            {
                return result;
            }
            else
            {
                return ProcessResultHelper.MapToProcessResult<bool, LKP_Cutting>(finalPriceresult);
            }
        }
        public ProcessResult<List<LKP_Cutting>> GetByVatId(int vatId, bool? IsActive = null)
        {
            List<LKP_Cutting> data = null;
            ProcessResult<List<LKP_Cutting>> result = null;
            try
            {
                if (IsActive == null)
                {
                    data = base.GetAllQuerable().Data.Where(vat => vat.FK_VAT_Id == vatId).ToList();
                }
                else
                {
                    data = base.GetAllQuerable().Data.Where(r=>r.FK_VAT_Id == vatId).ToList();
                }
                result = ProcessResultHelper.Succedded(data);

            }
            catch (Exception ex)
            {
                result = ProcessResultHelper.Failed(data, ex);
            }
            return result;
        }
        public ProcessResult<List<LKP_Cutting>> GetByDiscountId(int discountId, bool? IsActive = null)
        {
            List<LKP_Cutting> data = null;
            ProcessResult<List<LKP_Cutting>> result = null;
            try
            {
                if (IsActive == null)
                {
                    data = base.GetAllQuerable().Data.Where(vat => vat.FK_Discount_Id == discountId).ToList();
                }
                else
                {
                    data = base.GetAllQuerable().Data.Where(r =>r.FK_Discount_Id == discountId).ToList();
                }
                result = ProcessResultHelper.Succedded(data);

            }
            catch (Exception ex)
            {
                result = ProcessResultHelper.Failed(data, ex);
            }
            return result;
        }
        public ProcessResult<bool> UpdateItemFinalPrice(int id, ItemPropertyChanged propChanged)
        {

            if (propChanged == ItemPropertyChanged.Discount)
            {
                // id here equal to discount Id
                return UpdateItemsDiscount(id);

            }
            else if (propChanged == ItemPropertyChanged.VAT)
            {
                // id here equal to Vat Id
                return UpdateItemsVat(id);


            }
            else if (propChanged == ItemPropertyChanged.Price)
            {
                // id here equal to ItemId 
                return UpdateItemPrice(id);
            }

            return ProcessResultHelper.Failed(false, new ArgumentOutOfRangeException("ItemPropertyChanged not valid value"), "ItemPropertyChanged not valid value");
        }
        private ProcessResult<bool> UpdateItemsVat(int vatId)
        {
            try
            {
                var data = true;
                var vatResult = GetVat(vatId);
                if (vatResult.IsSucceeded)
                {
                    if (vatResult.Data != null)
                    {
                        var vatPercentage = vatResult.Data.Percentage;
                        var itemResultByVatId = GetByVatId(vatId);
                        if (itemResultByVatId.IsSucceeded)
                        {
                            itemResultByVatId.Data = CalculateItemsVat(itemResultByVatId.Data, vatPercentage);
                            itemResultByVatId.Data = CalculateItemsFinalPrice(itemResultByVatId.Data);
                            var updateresult = base.Update(itemResultByVatId.Data);
                            if (updateresult.IsSucceeded)
                            {
                                data = updateresult.Data;
                                return ProcessResultHelper.Succedded(data);
                            }
                            else
                            {
                                return ProcessResultHelper.MapToProcessResult<bool, bool>(updateresult);
                            }

                        }
                        else
                        {
                            return ProcessResultHelper.MapToProcessResult<List<LKP_Cutting>, bool>(itemResultByVatId);
                        }
                    }
                    else
                    {
                        return ProcessResultHelper.Failed(false, new ArgumentOutOfRangeException("there is No VAT Instance with this Id"), "there is No VAT Instance with this Id", ProcessResultStatusCode.InvalidValue);
                    }
                }
                else
                {
                    return ProcessResultHelper.MapToProcessResult<LKP_VAT, bool>(vatResult);
                }
            }
            catch (Exception ex)
            {
                var result = false;
                return ProcessResultHelper.Failed(result, ex);
            }

        }
        private ProcessResult<bool> UpdateItemsDiscount(int discountId)
        {
            try
            {
                var data = true;
                var discountResult = GetDiscount(discountId);
                if (discountResult.IsSucceeded)
                {
                    if (discountResult.Data != null)
                    {
                        var discountPercentage = discountResult.Data.Percentage;
                        var itemResultBydiscountId = GetByDiscountId(discountId);
                        if (itemResultBydiscountId.IsSucceeded)
                        {
                            itemResultBydiscountId.Data = CalculateItemsDiscount(itemResultBydiscountId.Data, discountPercentage);
                            itemResultBydiscountId.Data = CalculateItemsFinalPrice(itemResultBydiscountId.Data);
                            var updateresult = base.Update(itemResultBydiscountId.Data);
                            if (updateresult.IsSucceeded)
                            {
                                data = updateresult.Data;
                                return ProcessResultHelper.Succedded(data);
                            }
                            else
                            {
                                return ProcessResultHelper.MapToProcessResult<bool, bool>(updateresult);
                            }

                        }
                        else
                        {
                            return ProcessResultHelper.MapToProcessResult<List<LKP_Cutting>, bool>(itemResultBydiscountId);
                        }
                    }
                    else
                    {
                        return ProcessResultHelper.Failed(false, new ArgumentOutOfRangeException("there is No discount Instance with this Id"), "there is No discount Instance with this Id", ProcessResultStatusCode.InvalidValue);
                    }
                }
                else
                {
                    return ProcessResultHelper.MapToProcessResult<Discount, bool>(discountResult);
                }
            }
            catch (Exception ex)
            {
                var result = false;
                return ProcessResultHelper.Failed(result, ex);
            }
        }
        private ProcessResult<bool> UpdateItemPrice(int itemId)
        {
            var itemresult = Get(itemId);

            if (itemresult.IsSucceeded && itemresult.Data != null)
            {
                //var itemVatResult = GetVat(itemresult.Data.FK_VAT_Id);
                var itemDiscountResult = GetDiscount(itemresult.Data.FK_Discount_Id);
                //if (itemVatResult.IsSucceeded && itemVatResult.Data!=null)
                //{
                //    itemresult.Data = CalculateItemVat(itemresult.Data, itemVatResult.Data.Percentage);
                //}
                if (itemDiscountResult.IsSucceeded && itemDiscountResult.Data != null)
                {
                    itemresult.Data = CalculateItemDiscount(itemresult.Data, itemDiscountResult.Data.Percentage);
                }
                itemresult.Data = CalculateItemFinalPrice(itemresult.Data);
                return base.Update(itemresult.Data);
            }
            else
            {
                return ProcessResultHelper.MapToProcessResult<LKP_Cutting, bool>(itemresult);
            }
        }
        private ProcessResult<LKP_VAT> GetVat(int vatId)
        {
            var vatManager = serviceProvider.GetService<ILKP_VATManager>();
            return vatManager.Get(vatId);
        }
        private ProcessResult<Discount> GetDiscount(int discountId)
        {
            var discountManager = serviceProvider.GetService<IDiscountManager>();
            return discountManager.Get(discountId);
        }
        private List<LKP_Cutting> CalculateItemsVat(List<LKP_Cutting> items, double percentage)
        {
            foreach (var item in items)
            {
                CalculateItemVat(item, percentage);
            }
            return items;
        }
        private LKP_Cutting CalculateItemVat(LKP_Cutting item, double percentage)
        {
            item.VATValue = (item.Price * percentage) / 100;
            return item;
        }
        private LKP_Cutting CalculateItemDiscount(LKP_Cutting item, double percentage)
        {
            item.DiscountValue = (item.Price * percentage) / 100;
            return item;
        }
        private List<LKP_Cutting> CalculateItemsDiscount(List<LKP_Cutting> items, double percentage)
        {
            foreach (var item in items)
            {
                CalculateItemDiscount(item, percentage);
            }
            return items;
        }
        private LKP_Cutting CalculateItemFinalPrice(LKP_Cutting item)
        {
           // item.FinalPrice = item.Price + item.VATValue - item.DiscountValue;
            item.FinalPrice = item.Price + item.DiscountValue;
            return item;
        }
        private List<LKP_Cutting> CalculateItemsFinalPrice(List<LKP_Cutting> items)
        {
            foreach (var item in items)
            {
                CalculateItemFinalPrice(item);
            }
            return items;
        }
    }
}
