﻿using System;
using System.Collections.Generic;
using System.Text;
using DispatchProduct.RepositoryModule;
using Dispatching.Inventory.Models.Entities;
using Dispatching.Inventory.BLL.IManagers;
using Dispatching.Inventory.Models.Context;
using Utilites.ProcessingResult;
using System.Linq;
using CommonEnums;

namespace Dispatching.Inventory.BLL.Managers
{
    public class CategoryDeliveryOptionsManager : Repository<CategoryDeliveryOptions>, ICategoryDeliveryOptionsManager
    {
        public CategoryDeliveryOptionsManager(InventoryDbContext context)
            : base(context)
        {

        }
        public ProcessResult<List<CategoryDeliveryOptions>> GetByCategoryConfigId(int catConfigId)
        {
            var options = GetAllQuerable().Data.Where(c => c.FK_CategoryConfig_Id == catConfigId).ToList();
            return ProcessResultHelper.Succedded(options);
        }
        public ProcessResult<int> GetDateType(int catConfigId,int deliveryId)
        {
            var option = GetAllQuerable().Data.Where(c => c.FK_CategoryConfig_Id == catConfigId &&c.DeliveryType== (DeliveryType)deliveryId).FirstOrDefault();
            if (option != null)
            {

                return ProcessResultHelper.Succedded((int)option.DateTypes);
            }
            else
            {
                return ProcessResultHelper.Failed(0,new Exception($"no configuration for deliveryTypeId {deliveryId} and categoryConfigId {catConfigId}"));
            }
        }

        public ProcessResult<List<EnumEntity>> GetDeliveryOptionsByConfigId(int configId)
        {
            ProcessResult<List<EnumEntity>> result = null;
            var devResult = GetByCategoryConfigId(configId);
            if (devResult.IsSucceeded && devResult.Data != null)
            {
                var enumValues = devResult.Data.Select(val => val.DeliveryType).ToList();
                var data = EnumManager<DeliveryType>.GetEnumObjects(enumValues);
                result = ProcessResultHelper.Succedded(data);
            }
            else
            {
                result = ProcessResultHelper.Failed<List<EnumEntity>>(null, new Exception($"No Delivery options configured for this Catgory defined By {configId}"));
            }
            return result;
        }
    }
}
