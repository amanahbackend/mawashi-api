﻿using Dispatching.Inventory.BLL.IManagers;
using Dispatching.Inventory.Models.Context;
using Dispatching.Inventory.Models.Entities;
using DispatchProduct.RepositoryModule;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Utilites.ProcessingResult;
using Microsoft.Extensions.DependencyInjection;
namespace Dispatching.Inventory.BLL.Managers
{
    public class LKP_QuantityManager : Repository<LKP_Quantity>, ILKP_QuantityManager
    {
        IServiceProvider serviceProvider;

        public LKP_QuantityManager(InventoryDbContext context, IServiceProvider _serviceProvider)
            : base(context)
        {
            _serviceProvider = serviceProvider;
        }
        public ProcessResult<int> GetMinimumQuantity(List<int> quanIds)
        {
            List<int> ids = new List<int>();
            var minVal=GetAllQuerable().Data.Where(r => quanIds.Contains(r.Id)).Min(r=>r.Value);
            return ProcessResultHelper.Succedded(minVal);
        }
        public ProcessResult<int> GetMaxQuantity(List<int> quanIds)
        {
            List<int> ids = new List<int>();
            var maxVal = GetAllQuerable().Data.Where(r => quanIds.Contains(r.Id)).Max(r => r.Value);
            return ProcessResultHelper.Succedded(maxVal);
        }
        private ILKP_Quantity Quantity
        {
            get { return serviceProvider.GetService<ILKP_Quantity>(); }
        }
    }
}
