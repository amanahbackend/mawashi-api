﻿using System;
using System.Collections.Generic;
using System.Text;
using DispatchProduct.RepositoryModule;
using Dispatching.Inventory.Models.Entities;
using Dispatching.Inventory.BLL.IManagers;
using Dispatching.Inventory.Models.Context;
using Utilites.ProcessingResult;
using Microsoft.Extensions.DependencyInjection;
using Dispatching.Inventory.Models.Settings;
using Microsoft.Extensions.Options;
using System.Linq;

namespace Dispatching.Inventory.BLL.Managers
{
    public class LKP_VATManager : Repository<LKP_VAT>, ILKP_VATManager
    {
        IServiceProvider serviceProvider;
        InventoryAppSettings settings;
        public LKP_VATManager(IOptions<InventoryAppSettings> _settings ,InventoryDbContext context, IServiceProvider _serviceProvider)
            : base(context)
        {
            serviceProvider = _serviceProvider;
            settings = _settings.Value;
        }
        public override ProcessResult<bool> Update(List<LKP_VAT> entityLst)
        {
            bool data = false;
            foreach (var item in entityLst)
            {
                data = Update(item).Data;
            }
            return ProcessResultHelper.Succedded(data);
        }
        public override ProcessResult<bool> Update(LKP_VAT vat)
        {
            try
            {
                var vatChangeResult = IsChanged(vat, nameof(vat.Percentage));
                var result = base.Update(vat);
                if (vatChangeResult.IsSucceeded && vatChangeResult.Data)
                {
                    result = ItemManager.UpdateItemFinalPrice(vat.Id, ItemPropertyChanged.VAT);
                    result = CuttingManager.UpdateItemFinalPrice(vat.Id, ItemPropertyChanged.VAT);
                }
                return result;
            }
            catch (Exception ex)
            {
                return ProcessResultHelper.Failed(false, ex);
            }
        }
        private ILKP_ItemManager ItemManager
        {
            get
            {
                return serviceProvider.GetService<ILKP_ItemManager>();
            }

        }
        private ILKP_CuttingManager CuttingManager
        {
            get
            {
                return serviceProvider.GetService<ILKP_CuttingManager>();
            }

        }
        public ProcessResult<LKP_VAT> GetVat(string vatName=null)
        {
            LKP_VAT data=null;
            if (string.IsNullOrEmpty(vatName))
            {
                data = GetAllQuerable().Data.Where(v => v.NameAR == settings.VatAR || v.NameEN == settings.VatEN).FirstOrDefault();
            }
            else
            {
                data = GetAllQuerable().Data.Where(v => v.NameAR == vatName || v.NameEN == vatName).FirstOrDefault();
            }
            if (data != null)
            {
                return ProcessResultHelper.Succedded(data);
            }
            else
            {
                return ProcessResultHelper.Failed(data,new Exception("No configured vat in system"));
            }
        }

    }
}
