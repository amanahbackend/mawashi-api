﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Dispatching.Inventory.BLL.CommonModels
{
    public class AvailableDateViewModel
    {
        public int Id { get; set; }
        public DateTime? DateFrom  { get; set; }
        public DateTime? DateTo { get; set; }
    }
}
