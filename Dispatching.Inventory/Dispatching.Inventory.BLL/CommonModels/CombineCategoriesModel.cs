﻿using Dispatching.Inventory.Models.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace Dispatching.Inventory.BLL.CommonModels
{
    public class CombineCategoriesModel: ICombineCategoriesModel
    {
        public bool AllowMulti { get; set; }
        public int CategoryId { get; set; }
        public int Count { get; set; }
        public List<LKP_Category> Categories { get; set; }
    }
}
