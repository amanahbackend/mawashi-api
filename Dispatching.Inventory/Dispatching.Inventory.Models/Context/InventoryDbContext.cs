﻿using Dispatching.Inventory.Models.Entities;
using Dispatching.Inventory.Models.EntityConfigurations;
using Microsoft.EntityFrameworkCore;

namespace Dispatching.Inventory.Models.Context
{
    public class InventoryDbContext : DbContext
    {
        public DbSet<Allowable_Items_Cutting> Allowable_Items_Cutting { get; set; }
        public DbSet<Allowable_Items_Quatity> Allowable_Items_Quatity { get; set; }
        public DbSet<LKP_Cutting> LKP_Cutting { get; set; }
        public DbSet<LKP_Quantity> LKP_Quantity { get; set; }
        public DbSet<LKP_Item> LKP_Item { get; set; }
        public DbSet<LKP_Category> LKP_Category { get; set; }
        public DbSet<LKP_CategoryType> LKP_CategoryType { get; set; }
        public DbSet<Discount> Discount { get; set; }
        public DbSet<LKP_VAT> LKP_VAT { get; set; }
        public DbSet<LKP_Currency> Currency { get; set; }
        public DbSet<LKP_CategoryConfig> CategoryConfig { get; set; }
        public DbSet<CategoryDeliveryOptions> CategoryDeliveryOptions { get; set; }
        public DbSet<CategorySubOptions> CategorySubOptions { get; set; }

        public InventoryDbContext(DbContextOptions<InventoryDbContext> options)
            : base(options)
        {



        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
            modelBuilder.ApplyConfiguration(new LKP_QuantityEntityTypeConfiguration());
            modelBuilder.ApplyConfiguration(new LKP_ItemEntityTypeConfiguration());
            modelBuilder.ApplyConfiguration(new LKP_CuttingEntityTypeConfiguration());
            modelBuilder.ApplyConfiguration(new LKP_CategoryEntityTypeConfiguration());
            modelBuilder.ApplyConfiguration(new Allowable_Items_QuatityEntityTypeConfiguration());
            modelBuilder.ApplyConfiguration(new Allowable_Items_CuttingEntityTypeConfiguration());
            modelBuilder.ApplyConfiguration(new LKP_CategoryTypeEntityTypeConfiguration());
            modelBuilder.ApplyConfiguration(new LKP_VATEntityTypeConfiguration());
            modelBuilder.ApplyConfiguration(new DiscountEntityTypeConfiguration());
            modelBuilder.ApplyConfiguration(new LKP_CurrencyEntityTypeConfiguration());
            modelBuilder.ApplyConfiguration(new LKP_CategoryConfigEntityTypeConfiguration());
            modelBuilder.ApplyConfiguration(new CategoryDeliveryOptionsEntityTypeConfiguration());
            modelBuilder.ApplyConfiguration(new CategorySubOptionsEntityTypeConfiguration());
        }
    }
}
