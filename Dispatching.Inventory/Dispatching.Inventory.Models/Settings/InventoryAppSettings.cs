﻿namespace Dispatching.Inventory.Models.Settings
{
    public class InventoryAppSettings
    {
        public string ItemImagesPath { get; set; }
        public string CategoryImagesPath { get; set; }
        public string APIGateway { get; set; }
        public string KWCurrencyEN { get; set; }
        public string KWCurrencyAR { get; set; }
        public string VatEN { get; set; }
        public string VatAR { get; set; }
        public double VatPercentage { get; set; }
        public string UAECurrencyEN { get; set; }
        public string UAECurrencyAR { get; set; }
        public string DefaultCurrencyEN { get; set; }
        public string DefaultCurrencyAR { get; set; }
    }
}
