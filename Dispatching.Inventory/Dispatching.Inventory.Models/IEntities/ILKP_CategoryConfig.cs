﻿using DispatchProduct.BuildingBlocks.BaseEntities.IEntities;
using System;
using System.Collections.Generic;
using System.Text;

namespace Dispatching.Inventory.Models.Entities
{
    public interface ILKP_CategoryConfig : IBaseLKPEntity
    {
         //int FK_Category_Id { get; set; }
         //LKP_Category Category { get; set; }
         bool HasOptions { get; set; }
         List<CategorySubOptions> CategorySubOptions { get; set; }
         List<CategoryDeliveryOptions> CategoryDeliveryOptions { get; set; }
         bool AcceptMulti { get; set; }

    }
}
