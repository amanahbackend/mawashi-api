﻿using DispatchProduct.BuildingBlocks.BaseEntities.IEntities;
using System;
using System.Collections.Generic;
using System.Text;

namespace Dispatching.Inventory.Models.Entities
{
    public interface IAllowable_Items_Cutting : IBaseEntity
    {
         int FK_Item_Id { get; set; }
         int FK_Cutting_Id { get; set; }
         LKP_Cutting Cutting { get; set; }
         LKP_Item Item { get; set; }
    }
}
