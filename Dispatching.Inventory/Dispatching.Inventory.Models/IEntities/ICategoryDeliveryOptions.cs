﻿using CommonEnums;
using DispatchProduct.BuildingBlocks.BaseEntities.IEntities;
using System;
using System.Collections.Generic;
using System.Text;

namespace Dispatching.Inventory.Models.Entities
{
    public interface ICategoryDeliveryOptions : IBaseEntity
    {
        LKP_CategoryConfig CategoryConfig { get; set; }
        int FK_CategoryConfig_Id { get; set; }
        DeliveryType DeliveryType { get; set; }
        DateTypes DateTypes { get; set; }

    }
}
