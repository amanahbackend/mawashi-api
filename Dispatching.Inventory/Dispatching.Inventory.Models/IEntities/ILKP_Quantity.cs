﻿using DispatchProduct.BuildingBlocks.BaseEntities.IEntities;
using System;
using System.Collections.Generic;
using System.Text;

namespace Dispatching.Inventory.Models.Entities
{
    public interface ILKP_Quantity:IBaseLKPEntity
    {
         int Value { get; set; }
         List<Allowable_Items_Quatity> AllowableItemsQuatity { get; set; }
         string DescriptionAR { get; set; }
         string DescriptionEN { get; set; }
    }
}
