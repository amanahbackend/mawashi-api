﻿using BuisnessCommon.Enums;
using DispatchProduct.BuildingBlocks.BaseEntities.IEntities;
using System;
using System.Collections.Generic;

namespace Dispatching.Inventory.Models.Entities
{
    public interface ILKP_Category : IBaseLKPEntity
    {
        string DescriptionAR { get; set; }
        string DescriptionEN { get; set; }
        string Code { get; set; }
        bool? IsActive { get; set; }
        DateTime? AvailableDateFrom { get; set; }
        DateTime? AvailableDateTo { get; set; }
        string PictureURL { get; set; }
        string PicturePath { get; set; }
        int FK_CategoryType_Id { get; set; }
        LKP_CategoryType CategoryType { get; set; }
        List<LKP_Item> Items { get; set; }
        LKP_CategoryConfig Config { get; set; }
        bool AcceptMulti { get; set; }
        int FK_CategoryConfig_Id { get; set; }
        int Count { get; set; }
        bool IsParent { get; set; }
        int? Fk_Parent_Id { get; set; }
        bool AllowTodayDelivery { get; set; }
        bool IsSponsored { get; set; }
        SortType ItemsSortType { get; set; }
    }
}
