﻿using DispatchProduct.BuildingBlocks.BaseEntities.IEntities;
using System;
using System.Collections.Generic;
using System.Text;

namespace Dispatching.Inventory.Models.Entities
{
    public interface ILKP_Cutting : IBaseLKPEntity
    {

        int Value { get; set; }
        int Price { get; set; }
        string DescriptionAR { get; set; }
        string DescriptionEN { get; set; }
        double VATValue { get; set; }
        double DiscountValue { get; set; }
        double FinalPrice { get; set; }
        int FK_VAT_Id { get; set; }
        int FK_Discount_Id { get; set; }
        List<Allowable_Items_Cutting> AllowableItemsCutting { get; set; }
        LKP_Category Category { get; set; }
        LKP_VAT VAT { get; set; }
    }
}
