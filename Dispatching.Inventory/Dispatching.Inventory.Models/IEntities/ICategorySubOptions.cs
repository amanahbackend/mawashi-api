﻿using DispatchProduct.BuildingBlocks.BaseEntities.IEntities;
using System;
using System.Collections.Generic;
using System.Text;

namespace Dispatching.Inventory.Models.Entities
{
    public interface ICategorySubOptions : IBaseLKPEntity
    {
        LKP_CategoryConfig CategoryConfig { get; set; }
        int FK_CategoryConfig_Id { get; set; }
    }
}
