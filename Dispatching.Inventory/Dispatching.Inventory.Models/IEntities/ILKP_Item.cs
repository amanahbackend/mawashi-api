﻿using System.ComponentModel.DataAnnotations.Schema;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;
using DispatchProduct.BuildingBlocks.BaseEntities.IEntities;
using Dispatching.Inventory.Models.Entities;

namespace DispatchProduct.Inventory.Entities
{
    public interface ILKP_Item : IBaseLKPEntity
    {
        string Code { get; set; }
        string DescriptionAR { get; set; }
        string DescriptionEN { get; set; }
        string Weight { get; set; }
        string Age { get; set; }
        int CurrentStock { get; set; }
        int CurrentHeadStock { get; set; }
        int CurrentKgsStock { get; set; }
        int CurrentPiecesStock { get; set; }
        double Price { get; set; }
        double VATValue { get; set; }
        double DiscountValue { get; set; }
        double FinalPrice { get; set; }
        int FK_Category_Id { get; set; }
        int FK_VAT_Id { get; set; }
        int FK_Discount_Id { get; set; }
        int FK_Currency_Id { get; set; }
        int MinimumNo { get; set; }
        int MaximumNo { get; set; }
        LKP_Category Category { get; set; }
        LKP_Currency Currency { get; set; }
        bool? IsActive { get; set; }
        bool IsInStock { get; set; }
        DateTime? AvailableDateFrom { get; set; }
        DateTime? AvailableDateTo { get; set; }
        string PictureURL { get; set; }
        string PicturePath { get; set; }
        List<Allowable_Items_Cutting> AllowableItemsCutting { get; set; }
        List<Allowable_Items_Quatity> AllowableItemsQuatity { get; set; }
        LKP_VAT VAT { get; set; }
        Discount Discount { get; set; }
    }
}
