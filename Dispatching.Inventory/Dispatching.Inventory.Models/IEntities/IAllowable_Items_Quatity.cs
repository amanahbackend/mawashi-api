﻿using DispatchProduct.BuildingBlocks.BaseEntities.Entities;
using DispatchProduct.BuildingBlocks.BaseEntities.IEntities;
using System;
using System.Collections.Generic;
using System.Text;

namespace Dispatching.Inventory.Models.Entities
{
    public interface IAllowable_Items_Quatity:IBaseEntity
    {
         int FK_Item_Id { get; set; }
         int FK_Quatity_Id { get; set; }
         LKP_Quantity Quantity { get; set; }
         LKP_Item Item { get; set; }
    }
}
