﻿using DispatchProduct.BuildingBlocks.BaseEntities.IEntities;
using System;
using System.Collections.Generic;
using System.Text;

namespace Dispatching.Inventory.Models.Entities
{
    public interface ILKP_VAT : IBaseLKPEntity
    {
        double Percentage { get; set; }
        DateTime? DateFrom { get; set; }
        DateTime? DateTo { get; set; }
    }
}
