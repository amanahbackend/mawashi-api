﻿using Dispatching.Inventory.Models.Entities;
using DispatchProduct.BuildingBlocks.BaseEntities.EntityConfigurations;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Dispatching.Inventory.Models.EntityConfigurations
{
    public class LKP_CuttingEntityTypeConfiguration : BaseLKPEntityTypeConfiguration<LKP_Cutting>,IEntityTypeConfiguration<LKP_Cutting>
    {
        public new void Configure(EntityTypeBuilder<LKP_Cutting> CategoryConfiguration)
        {
            base.Configure(CategoryConfiguration);

            CategoryConfiguration.ToTable("LKP_Cutting");
            CategoryConfiguration.Property(o => o.Id).ValueGeneratedOnAdd();
            CategoryConfiguration.Property(o => o.Value).IsRequired();
            CategoryConfiguration.Property(o => o.Price).IsRequired();
            CategoryConfiguration.Property(o => o.VATValue).IsRequired();
            CategoryConfiguration.Property(o => o.DiscountValue).IsRequired();
            CategoryConfiguration.Property(o => o.FinalPrice).IsRequired();
            CategoryConfiguration.Property(o => o.FK_VAT_Id).IsRequired();
            CategoryConfiguration.Property(o => o.FK_Discount_Id).IsRequired();
            CategoryConfiguration.Ignore(o => o.AllowableItemsCutting);
            CategoryConfiguration.Ignore(o => o.Category);
            CategoryConfiguration.Ignore(o => o.VAT);
        }
    }
}
