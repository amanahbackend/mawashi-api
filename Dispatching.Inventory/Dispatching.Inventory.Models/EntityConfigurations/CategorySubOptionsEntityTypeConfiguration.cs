﻿using Dispatching.Inventory.Models.Entities;
using DispatchProduct.BuildingBlocks.BaseEntities.EntityConfigurations;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Dispatching.Inventory.Models.EntityConfigurations
{
    public class CategorySubOptionsEntityTypeConfiguration : BaseLKPEntityTypeConfiguration<CategorySubOptions>,IEntityTypeConfiguration<CategorySubOptions>
    {
        public new void Configure(EntityTypeBuilder<CategorySubOptions> CategoryConfiguration)
        {
            base.Configure(CategoryConfiguration);
            CategoryConfiguration.Property(o => o.Id).ValueGeneratedOnAdd();
            CategoryConfiguration.ToTable("CategorySubOptions");
            CategoryConfiguration.Ignore(o => o.CategoryConfig);

        }
    }
}
