﻿using Dispatching.Inventory.Models.Entities;
using DispatchProduct.BuildingBlocks.BaseEntities.EntityConfigurations;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Dispatching.Inventory.Models.EntityConfigurations
{
    public class DiscountEntityTypeConfiguration : BaseEntityTypeConfiguration<Discount>,IEntityTypeConfiguration<Discount>
    {
        public new void Configure(EntityTypeBuilder<Discount> CategoryConfiguration)
        {
            base.Configure(CategoryConfiguration);
            CategoryConfiguration.Property(o => o.Percentage).IsRequired();
            CategoryConfiguration.Property(o => o.DateFrom).IsRequired(false);
            CategoryConfiguration.Property(o => o.DateTo).IsRequired(false);
            CategoryConfiguration.ToTable("Discount");
        
    }
    }
}
