﻿using Dispatching.Inventory.Models.Entities;
using DispatchProduct.BuildingBlocks.BaseEntities.EntityConfigurations;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Dispatching.Inventory.Models.EntityConfigurations
{
    public class Allowable_Items_CuttingEntityTypeConfiguration : BaseEntityTypeConfiguration<Allowable_Items_Cutting>,IEntityTypeConfiguration<Allowable_Items_Cutting>
    {
        public new void  Configure(EntityTypeBuilder<Allowable_Items_Cutting> CategoryConfiguration)
        {
            base.Configure(CategoryConfiguration);

            CategoryConfiguration.ToTable("Allowable_Items_Cutting");
            CategoryConfiguration.Property(o => o.Id).ValueGeneratedOnAdd();
            CategoryConfiguration.Property(o => o.FK_Item_Id).IsRequired();
            CategoryConfiguration.Property(o => o.FK_Cutting_Id).IsRequired();
            CategoryConfiguration.Ignore(o => o.Cutting);
            CategoryConfiguration.Ignore(o => o.Item);
          
    }
    }
}
