﻿using Dispatching.Inventory.Models.Entities;
using DispatchProduct.BuildingBlocks.BaseEntities.EntityConfigurations;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Dispatching.Inventory.Models.EntityConfigurations
{
    public class LKP_CategoryConfigEntityTypeConfiguration : BaseLKPEntityTypeConfiguration<LKP_CategoryConfig>,IEntityTypeConfiguration<LKP_CategoryConfig>
    {
        public new void Configure(EntityTypeBuilder<LKP_CategoryConfig> CategoryConfiguration)
        {
            base.Configure(CategoryConfiguration);
            CategoryConfiguration.Property(o => o.Id).ValueGeneratedOnAdd();
            CategoryConfiguration.ToTable("LKP_CategoryConfig");
            CategoryConfiguration.Ignore(o => o.CategoryDeliveryOptions);
            CategoryConfiguration.Ignore(o => o.CategorySubOptions);

        }
    }
}
