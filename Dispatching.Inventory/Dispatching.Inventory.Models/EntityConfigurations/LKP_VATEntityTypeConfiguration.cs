﻿using Dispatching.Inventory.Models.Entities;
using DispatchProduct.BuildingBlocks.BaseEntities.EntityConfigurations;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Dispatching.Inventory.Models.EntityConfigurations
{
    public class LKP_VATEntityTypeConfiguration : BaseLKPEntityTypeConfiguration<LKP_VAT>,IEntityTypeConfiguration<LKP_VAT>
    {
        public new void Configure(EntityTypeBuilder<LKP_VAT> CategoryConfiguration)
        {
            base.Configure(CategoryConfiguration);
            CategoryConfiguration.Property(o => o.Id).ValueGeneratedOnAdd();
            CategoryConfiguration.ToTable("LKP_VAT");
            CategoryConfiguration.Property(o => o.Percentage).IsRequired();
            CategoryConfiguration.Property(o => o.DateFrom).IsRequired(false);
            CategoryConfiguration.Property(o => o.DateTo).IsRequired(false);
        }
    }
}
