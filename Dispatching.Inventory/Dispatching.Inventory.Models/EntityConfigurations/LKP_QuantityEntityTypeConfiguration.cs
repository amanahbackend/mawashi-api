﻿using Dispatching.Inventory.Models.Entities;
using DispatchProduct.BuildingBlocks.BaseEntities.EntityConfigurations;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Dispatching.Inventory.Models.EntityConfigurations
{
    public class LKP_QuantityEntityTypeConfiguration : BaseLKPEntityTypeConfiguration<LKP_Quantity>, IEntityTypeConfiguration<LKP_Quantity>
    {
        public new void Configure(EntityTypeBuilder<LKP_Quantity> CategoryConfiguration)
        {
            base.Configure(CategoryConfiguration);
            CategoryConfiguration.ToTable("LKP_Quantity");
            CategoryConfiguration.Property(o => o.Id).ValueGeneratedOnAdd();
            CategoryConfiguration.Property(o => o.Value).IsRequired();
            CategoryConfiguration.Ignore(o => o.AllowableItemsQuatity);

        }
    }
}
