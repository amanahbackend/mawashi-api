﻿using Dispatching.Inventory.Models.Entities;
using DispatchProduct.BuildingBlocks.BaseEntities.EntityConfigurations;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Dispatching.Inventory.Models.EntityConfigurations
{
    public class LKP_CurrencyEntityTypeConfiguration : BaseLKPEntityTypeConfiguration<LKP_Currency>, IEntityTypeConfiguration<LKP_Currency>
    {
        public new void Configure(EntityTypeBuilder<LKP_Currency> CategoryConfiguration)
        {
            base.Configure(CategoryConfiguration);
            CategoryConfiguration.ToTable("LKP_Currency");
            CategoryConfiguration.Property(o => o.Id).ValueGeneratedOnAdd();
        }
    }
}
