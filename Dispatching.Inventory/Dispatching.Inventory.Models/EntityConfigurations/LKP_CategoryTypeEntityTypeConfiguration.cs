﻿using Dispatching.Inventory.Models.Entities;
using DispatchProduct.BuildingBlocks.BaseEntities.EntityConfigurations;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Dispatching.Inventory.Models.EntityConfigurations
{
    public class LKP_CategoryTypeEntityTypeConfiguration : BaseLKPEntityTypeConfiguration<LKP_CategoryType>,IEntityTypeConfiguration<LKP_CategoryType>
    {
        public new void Configure(EntityTypeBuilder<LKP_CategoryType> CategoryConfiguration)
        {
            base.Configure(CategoryConfiguration);
            CategoryConfiguration.Property(o => o.Id).ValueGeneratedOnAdd();
            CategoryConfiguration.ToTable("LKP_CategoryType");
            CategoryConfiguration.Ignore(o => o.Categories);

        }
    }
}
