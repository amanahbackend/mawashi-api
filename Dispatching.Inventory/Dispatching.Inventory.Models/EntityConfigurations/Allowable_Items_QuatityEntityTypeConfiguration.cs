﻿using Dispatching.Inventory.Models.Entities;
using DispatchProduct.BuildingBlocks.BaseEntities.EntityConfigurations;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Dispatching.Inventory.Models.EntityConfigurations
{
    public class Allowable_Items_QuatityEntityTypeConfiguration : BaseEntityTypeConfiguration<Allowable_Items_Quatity>, IEntityTypeConfiguration<Allowable_Items_Quatity>
    {
        public new void Configure(EntityTypeBuilder<Allowable_Items_Quatity> CategoryConfiguration)
        {
            base.Configure(CategoryConfiguration);

            CategoryConfiguration.ToTable("Allowable_Items_Quatity");
            CategoryConfiguration.Property(o => o.Id).ValueGeneratedOnAdd();
            CategoryConfiguration.Property(o => o.FK_Item_Id).IsRequired();
            CategoryConfiguration.Property(o => o.FK_Quatity_Id).IsRequired();
            CategoryConfiguration.Ignore(o => o.Quantity);
            CategoryConfiguration.Ignore(o => o.Item);
        }
    }
}
