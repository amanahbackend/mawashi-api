﻿using Dispatching.Inventory.Models.Entities;
using DispatchProduct.BuildingBlocks.BaseEntities.EntityConfigurations;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Dispatching.Inventory.Models.EntityConfigurations
{
    public class LKP_CategoryEntityTypeConfiguration : BaseLKPEntityTypeConfiguration<LKP_Category>,IEntityTypeConfiguration<LKP_Category>
    {
        public new void Configure(EntityTypeBuilder<LKP_Category> CategoryConfiguration)
        {
            base.Configure(CategoryConfiguration);

            CategoryConfiguration.ToTable("LKP_Category");
            CategoryConfiguration.Property(o => o.DescriptionAR).IsRequired(false);
            CategoryConfiguration.Property(o => o.DescriptionEN).IsRequired(false);
            CategoryConfiguration.Property(o => o.Id).ValueGeneratedOnAdd();
            CategoryConfiguration.Property(o => o.Code).IsRequired();
            CategoryConfiguration.Property(o => o.IsActive).IsRequired(false);
            CategoryConfiguration.Property(o => o.AvailableDateFrom).IsRequired(false);
            CategoryConfiguration.Property(o => o.AvailableDateTo).IsRequired(false);
            CategoryConfiguration.Ignore(o => o.PictureURL);
            CategoryConfiguration.Property(o => o.PicturePath).IsRequired(false);
            CategoryConfiguration.Ignore(o => o.CategoryType);
            CategoryConfiguration.Ignore(o => o.Config);
            CategoryConfiguration.Ignore(o => o.Items);
            CategoryConfiguration.Ignore(o => o.Count);

        }
    }
}
