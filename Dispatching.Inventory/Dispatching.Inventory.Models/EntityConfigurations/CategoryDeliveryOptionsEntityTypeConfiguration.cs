﻿using Dispatching.Inventory.Models.Entities;
using DispatchProduct.BuildingBlocks.BaseEntities.EntityConfigurations;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Dispatching.Inventory.Models.EntityConfigurations
{
    public class CategoryDeliveryOptionsEntityTypeConfiguration : BaseEntityTypeConfiguration<CategoryDeliveryOptions>,IEntityTypeConfiguration<CategoryDeliveryOptions>
    {
        public new void Configure(EntityTypeBuilder<CategoryDeliveryOptions> CategoryConfiguration)
        {
            base.Configure(CategoryConfiguration);
            CategoryConfiguration.Property(o => o.Id).ValueGeneratedOnAdd();
            CategoryConfiguration.Property(o => o.FK_CategoryConfig_Id).IsRequired();
            CategoryConfiguration.ToTable("CategoryDeliveryOptions");
            CategoryConfiguration.Ignore(o => o.CategoryConfig);
            CategoryConfiguration.Property(o => o.DeliveryType).IsRequired();

        }
    }
}
