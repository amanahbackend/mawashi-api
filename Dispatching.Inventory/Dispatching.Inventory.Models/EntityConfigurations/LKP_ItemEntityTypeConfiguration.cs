﻿using Dispatching.Inventory.Models.Entities;
using DispatchProduct.BuildingBlocks.BaseEntities.EntityConfigurations;
using DispatchProduct.Inventory.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;

namespace Dispatching.Inventory.Models.EntityConfigurations
{
    public class LKP_ItemEntityTypeConfiguration : BaseLKPEntityTypeConfiguration<LKP_Item>, IEntityTypeConfiguration<LKP_Item>
    {
        public new void Configure(EntityTypeBuilder<LKP_Item> CategoryConfiguration)
        {
            base.Configure(CategoryConfiguration);

            CategoryConfiguration.ToTable("LKP_Item");
            CategoryConfiguration.Property(o => o.Id).ValueGeneratedOnAdd();
            CategoryConfiguration.Property(o => o.Code).IsRequired();
            CategoryConfiguration.Property(o => o.DescriptionAR).IsRequired(false);
            CategoryConfiguration.Property(o => o.DescriptionEN).IsRequired(false);
            CategoryConfiguration.Property(o => o.Weight).IsRequired(false);
            CategoryConfiguration.Property(o => o.CurrentStock).IsRequired();
            CategoryConfiguration.Property(o => o.Price).IsRequired();
            CategoryConfiguration.Property(o => o.FK_Category_Id).IsRequired();
            CategoryConfiguration.Property(o => o.FK_Discount_Id).IsRequired();
            CategoryConfiguration.Property(o => o.FK_Currency_Id).IsRequired();
            CategoryConfiguration.Property(o => o.FK_VAT_Id).IsRequired();
            CategoryConfiguration.Property(o => o.IsActive).IsRequired(false);
            CategoryConfiguration.Property(o => o.IsInStock).IsRequired();
            CategoryConfiguration.Property(o => o.AvailableDateFrom).IsRequired(false);
            CategoryConfiguration.Property(o => o.AvailableDateTo).IsRequired(false);
            CategoryConfiguration.Ignore(o => o.PictureURL);
            CategoryConfiguration.Property(o => o.PicturePath).IsRequired(false);
            CategoryConfiguration.Ignore(o => o.Category);
            CategoryConfiguration.Ignore(o => o.VAT);
            CategoryConfiguration.Ignore(o => o.Discount);
            CategoryConfiguration.Ignore(o => o.Category);
            CategoryConfiguration.Ignore(o => o.Currency);
            CategoryConfiguration.Ignore(o => o.AllowableItemsCutting);
            CategoryConfiguration.Ignore(o => o.AllowableItemsQuatity);
        }
    }
}