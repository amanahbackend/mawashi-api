﻿using CommonEnums;
using DispatchProduct.BuildingBlocks.BaseEntities.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace Dispatching.Inventory.Models.Entities
{
    public class CategoryDeliveryOptions : BaseEntity, ICategoryDeliveryOptions
    {
        public LKP_CategoryConfig CategoryConfig { get; set; }
        public int FK_CategoryConfig_Id { get; set; }
        public DeliveryType DeliveryType { get; set; }
        public DateTypes DateTypes { get; set; }
    }
}
