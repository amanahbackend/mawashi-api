﻿using DispatchProduct.BuildingBlocks.BaseEntities.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace Dispatching.Inventory.Models.Entities
{
    public class LKP_CategoryType : BaseLKPEntity, ILKP_CategoryType
    {
        public List<LKP_Category> Categories { get; set; }
    }
}
