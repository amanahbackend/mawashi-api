﻿using DispatchProduct.BuildingBlocks.BaseEntities.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace Dispatching.Inventory.Models.Entities
{
    public class Allowable_Items_Cutting : BaseEntity, IAllowable_Items_Cutting
    {
        public int FK_Item_Id { get; set; }
        public int FK_Cutting_Id { get; set; }
        public LKP_Cutting Cutting { get; set; }
        public LKP_Item Item { get; set; }
    }
}
