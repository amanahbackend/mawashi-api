﻿using DispatchProduct.BuildingBlocks.BaseEntities.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace Dispatching.Inventory.Models.Entities
{
    public class LKP_CategoryConfig : BaseLKPEntity, ILKP_CategoryConfig
    {
        //public int FK_Category_Id { get; set; }
        //public LKP_Category Category { get; set; }
        public bool HasOptions { get; set; }
        public List<CategorySubOptions> CategorySubOptions { get; set; }
        public List<CategoryDeliveryOptions> CategoryDeliveryOptions { get; set; }
        public bool AcceptMulti { get; set; }

    }
}
