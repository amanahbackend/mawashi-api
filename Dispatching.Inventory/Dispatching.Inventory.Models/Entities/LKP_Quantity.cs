﻿using DispatchProduct.BuildingBlocks.BaseEntities.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace Dispatching.Inventory.Models.Entities
{
    public class LKP_Quantity:BaseLKPEntity, ILKP_Quantity
    {
        public int Value { get; set; }
        public string DescriptionAR { get; set; }
        public string DescriptionEN { get; set; }
        public List<Allowable_Items_Quatity> AllowableItemsQuatity { get; set; }

    }
}
