﻿using DispatchProduct.BuildingBlocks.BaseEntities.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace Dispatching.Inventory.Models.Entities
{
    public class Allowable_Items_Quatity:BaseEntity,IAllowable_Items_Quatity
    {
        public int FK_Item_Id { get; set; }
        public int FK_Quatity_Id { get; set; }
        public LKP_Quantity Quantity { get; set; }
        public LKP_Item Item { get; set; }
}
}
