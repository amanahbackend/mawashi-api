﻿using AutoMapper;
using Dispatching.Inventory.API.ViewModel;
using Dispatching.Inventory.BLL.CommonModels;
using Dispatching.Inventory.Models.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Utilites.PaginatedItems;
using Utilites.PaginatedItemsViewModel;
using Utilities.Utilites;

namespace Dispatching.Inventory.API.AutoMapperConfig
{
    public class MappingProfile : Profile
    {
        public MappingProfile()
        {

            CreateMap<ItemDetailsModel, ItemDetailsViewModel>()
            .ForMember(dest => dest.Cuttings, opt => opt.MapFrom(src => src.Cuttings))
            .ForMember(dest => dest.Quantities, opt => opt.MapFrom(src => src.Quantities));
            CreateMap<ItemDetailsViewModel, ItemDetailsModel>()
            .ForMember(dest => dest.Cuttings, opt => opt.MapFrom(src => src.Cuttings))
            .ForMember(dest => dest.Quantities, opt => opt.MapFrom(src => src.Quantities));

            CreateMap<LKP_Item, LKP_ItemViewModel>()
              .ForMember(dest => dest.File, opt => opt.Ignore());
            CreateMap<LKP_ItemViewModel, LKP_Item>()
                .IgnoreBaseEntityProperties();

            CreateMap<LKP_Currency, LKP_CurrencyViewModel>();
            CreateMap<LKP_CurrencyViewModel, LKP_Currency>()
                .IgnoreBaseEntityProperties();


            CreateMap<Allowable_Items_CuttingViewModel, Allowable_Items_Cutting>()
                .IgnoreBaseEntityProperties();
            CreateMap<Allowable_Items_Cutting, Allowable_Items_CuttingViewModel>();

            CreateMap<LKP_VAT, LKP_VATViewModel>();
            CreateMap<LKP_VATViewModel, LKP_VAT>()
                .IgnoreBaseEntityProperties();
            CreateMap<DiscountViewModel, Discount>()
                .IgnoreBaseEntityProperties();
            CreateMap<Discount, DiscountViewModel>();

            CreateMap<LKP_CategoryTypeViewModel, LKP_CategoryType>()
                .IgnoreBaseEntityProperties();

            CreateMap<LKP_CategoryType, LKP_CategoryTypeViewModel>();

            CreateMap<Allowable_Items_QuatityViewModel, Allowable_Items_Quatity>()
                .IgnoreBaseEntityProperties();
            CreateMap<Allowable_Items_Quatity, Allowable_Items_QuatityViewModel>();

            CreateMap<LKP_CategoryViewModel, LKP_Category>()

                .IgnoreBaseEntityProperties();
            CreateMap<LKP_Category, LKP_CategoryViewModel>()
              .ForMember(dest => dest.File, opt => opt.Ignore());
            CreateMap<LKP_CuttingViewModel, LKP_Cutting>()
                .IgnoreBaseEntityProperties();
            CreateMap<LKP_Cutting, LKP_CuttingViewModel>();



            CreateMap<LKP_QuantityViewModel, LKP_Quantity>()
                .IgnoreBaseEntityProperties();

            CreateMap<LKP_Quantity, LKP_QuantityViewModel>();


            CreateMap<CategoryDeliveryOptionsViewModel, CategoryDeliveryOptions>()
            .ForMember(dest => dest.CategoryConfig, opt => opt.Ignore())
            .IgnoreBaseEntityProperties();
            CreateMap<CategoryDeliveryOptions, CategoryDeliveryOptionsViewModel>();


            CreateMap<CategorySubOptionsViewModel, CategorySubOptions>()
            .ForMember(dest => dest.CategoryConfig, opt => opt.Ignore())
           .IgnoreBaseEntityProperties();
            CreateMap<CategorySubOptions, CategorySubOptionsViewModel>();

            CreateMap<LKP_CategoryConfigViewModel, LKP_CategoryConfig>()
            .ForMember(dest => dest.CategorySubOptions, opt => opt.MapFrom(src => src.Options))
            //.ForMember(dest => dest.Category, opt => opt.Ignore())
            .ForMember(dest => dest.CategoryDeliveryOptions, opt => opt.Ignore())
           .IgnoreBaseEntityProperties();
            CreateMap<LKP_CategoryConfig, LKP_CategoryConfigViewModel>()
             .ForMember(dest => dest.Options, opt => opt.MapFrom(src => src.CategorySubOptions));

            CreateMap(typeof(PaginatedItems<>), typeof(PaginatedItemsViewModel<>));
            CreateMap(typeof(PaginatedItemsViewModel<>), typeof(PaginatedItems<>));

        }


    }
}
