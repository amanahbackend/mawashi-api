﻿using DispatchProduct.BuildingBlocks.BaseEntities.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace Dispatching.Inventory.API.ViewModel
{
    public class LKP_CuttingViewModel : BaseLKPEntityViewModel
    {
        public int Value { get; set; }
        public int Price { get; set; }
        public string DescriptionAR { get; set; }
        public string DescriptionEN { get; set; }
        public double VATValue { get; set; }
        public double DiscountValue { get; set; }
        public double FinalPrice { get; set; }
        public int FK_VAT_Id { get; set; }
        public int FK_Discount_Id { get; set; }
        public List<Allowable_Items_CuttingViewModel> AllowableItemsCutting { get; set; }
        public LKP_CategoryTypeViewModel Category { get; set; }
        public LKP_VATViewModel VAT { get; set; }
    }
}
