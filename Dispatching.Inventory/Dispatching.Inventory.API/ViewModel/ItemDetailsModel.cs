﻿using Dispatching.Inventory.Models.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace Dispatching.Inventory.API.ViewModel
{
    public class ItemDetailsViewModel
    {
        public int Fk_Item_Id { get; set; }
        public List<Allowable_Items_QuatityViewModel> Quantities { get; set; }
        public List<Allowable_Items_CuttingViewModel> Cuttings { get; set; }
    }
}
