﻿using DispatchProduct.BuildingBlocks.BaseEntities.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace Dispatching.Inventory.API.ViewModel
{
    public class UpdateAcceptMultiViewModel 
    {
        public int FK_CategoryConfig_Id { get; set; }
        public bool AcceptMulti { get; set; }
    }
}
