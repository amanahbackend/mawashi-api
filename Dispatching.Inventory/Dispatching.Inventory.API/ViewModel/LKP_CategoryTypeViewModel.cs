﻿using Dispatching.Inventory.API.ViewModel;
using DispatchProduct.BuildingBlocks.BaseEntities.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace Dispatching.Inventory.API.ViewModel
{
    public class LKP_CategoryTypeViewModel : BaseLKPEntityViewModel
    {
        public List<LKP_CategoryViewModel> Categories { get; set; }

    }
}
