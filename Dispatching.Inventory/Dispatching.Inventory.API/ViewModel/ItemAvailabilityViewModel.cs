﻿using Dispatching.BuisnessCommon.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Dispatching.Inventory.API.ViewModel
{
    public class ItemAvailabilityViewModel
    {
        public int ItemId { get; set; }
        public string DescriptionEN { get; set; }
        public int CurrentStock { get; set; }
        public int CurrentHeadStock { get; set; }
        public int CurrentKgsStock { get; set; }
        public int CurrentPiecesStock { get; set; }
        public bool IsAvailable { get; set; }
        public MeasurmentType MeasurmentType { get; set; }
        public OrderType OrderType { get; set; }
    }
}
