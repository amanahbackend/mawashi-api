﻿using System.ComponentModel.DataAnnotations.Schema;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using DispatchProduct.BuildingBlocks.BaseEntities.Entities;
using System.Text;
using DispatchProduct.Inventory.Entities;
using Utilites.UploadFile;
using Dispatching.Inventory.Models.Entities;

namespace Dispatching.Inventory.API.ViewModel
{
    public class LKP_ItemViewModel : BaseLKPEntityViewModel
    {
        public string Code { get; set; }
        public string DescriptionAR { get; set; }
        public string DescriptionEN { get; set; }
        public string Weight { get; set; }
        public string Age { get; set; }
        public int CurrentStock { get; set; }
        public int CurrentHeadStock { get; set; }
        public int CurrentKgsStock { get; set; }
        public int CurrentPiecesStock { get; set; }
        public double Price { get; set; }
        public double VATValue { get; set; }
        public double DiscountValue { get; set; }
        public double FinalPrice { get; set; }
        public int FK_VAT_Id { get; set; }
        public int FK_Discount_Id { get; set; }
        public int MinimumNo { get; set; }
        public int MaximumNo { get; set; }
        public int FK_Category_Id { get; set; }
        public bool? IsActive { get; set; }
        public bool IsInStock { get; set; }
        public bool HasCuttings { get; set; } = false;
        public bool HasQuantities { get; set; } = false;
        public DateTime? AvailableDateFrom { get; set; }
        public DateTime? AvailableDateTo { get; set; }
        public string PictureURL { get; set; }
        public int FK_Currency_Id { get; set; }
        public string PicturePath { get; set; }
        public LKP_CategoryViewModel Category { get; set; }
        public LKP_CurrencyViewModel Currency { get; set; }
        public LKP_VATViewModel VAT { get; set; }
        public DiscountViewModel Discount { get; set; }
        public List<Allowable_Items_CuttingViewModel> AllowableItemsCutting { get; set; }
        public List<Allowable_Items_QuatityViewModel> AllowableItemsQuatity { get; set; }
        public UploadFile File { get; set; }


    }
}
