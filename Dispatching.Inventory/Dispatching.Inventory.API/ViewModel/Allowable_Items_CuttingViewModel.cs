﻿using DispatchProduct.BuildingBlocks.BaseEntities.Entities;
using DispatchProduct.BuildingBlocks.BaseEntities.IEntities;
using System;
using System.Collections.Generic;
using System.Text;

namespace Dispatching.Inventory.API.ViewModel
{
    public class Allowable_Items_CuttingViewModel : RepoistryBaseEntity
    {
        public int FK_Item_Id { get; set; }
        public int FK_Cutting_Id { get; set; }
        public LKP_CuttingViewModel Cutting { get; set; }
        public LKP_ItemViewModel Item { get; set; }
    }
}
