﻿using CommonEnums;
using DispatchProduct.BuildingBlocks.BaseEntities.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace Dispatching.Inventory.API.ViewModel
{
    public class CategoryDeliveryOptionsViewModel : RepoistryBaseEntity
    {
        //public LKP_CategoryConfigViewModel CategoryConfig { get; set; }
        public int FK_CategoryConfig_Id { get; set; }
        public DeliveryType DeliveryType { get; set; }
        public DateTypes DateTypes { get; set; }
    }
}
