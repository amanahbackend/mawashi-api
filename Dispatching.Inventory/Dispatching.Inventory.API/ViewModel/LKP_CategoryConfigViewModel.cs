﻿using DispatchProduct.BuildingBlocks.BaseEntities.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace Dispatching.Inventory.API.ViewModel
{
    public class LKP_CategoryConfigViewModel : BaseLKPEntityViewModel
    {
        //public int FK_Category_Id { get; set; }
        //public LKP_CategoryViewModel Category { get; set; }
        public bool HasOptions { get; set; }
        public List<CategorySubOptionsViewModel> Options { get; set; }
        //public List<CategoryDeliveryOptionsViewModel> CategoryDeliveryOptions { get; set; }
        public bool AcceptMulti { get; set; }
    }
}
