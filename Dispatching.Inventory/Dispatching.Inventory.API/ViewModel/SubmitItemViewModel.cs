﻿using DispatchProduct.BuildingBlocks.BaseEntities.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace Dispatching.Inventory.API.ViewModel
{
    public class SubmitItemViewModel 
    {
        public LKP_ItemViewModel Item { get; set; }
        public List<int> CuttingIds { get; set; }
        public List<int> QuantitiesIds { get; set; }
        public List<Allowable_Items_CuttingViewModel> Cuttings { get; set; }
        public List<Allowable_Items_QuatityViewModel> Quantities { get; set; }
    }
}
