﻿using DispatchProduct.BuildingBlocks.BaseEntities.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace Dispatching.Inventory.API.ViewModel
{
    public class LKP_QuantityViewModel : BaseLKPEntityViewModel
    {
        public int Value { get; set; }
        public string DescriptionAR { get; set; }
        public string DescriptionEN { get; set; }
        public List<Allowable_Items_QuatityViewModel> AllowableItemsQuatity { get; set; }
    }
}
