﻿using CommonEnums;
using DispatchProduct.BuildingBlocks.BaseEntities.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace Dispatching.Inventory.API.ViewModel
{
    public class CategoryInputDeliveryOptions: BaseLKPEntityViewModel
    {
        public int FK_CategoryConfig_Id { get; set; }
        public int DeliveryId { get; set; }
    }
}
