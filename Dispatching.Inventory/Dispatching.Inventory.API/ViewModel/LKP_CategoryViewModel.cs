﻿using BuisnessCommon.Enums;
using DispatchProduct.BuildingBlocks.BaseEntities.Entities;
using System;
using System.Collections.Generic;
using Utilites.UploadFile;

namespace Dispatching.Inventory.API.ViewModel
{
    public class LKP_CategoryViewModel : BaseLKPEntityViewModel
    {
        public string DescriptionAR { get; set; }
        public string DescriptionEN { get; set; }
        public int FK_CategoryConfig_Id { get; set; }
        public string Code { get; set; }
        public bool? IsActive { get; set; }
        public DateTime? AvailableDateFrom { get; set; }
        public DateTime? AvailableDateTo { get; set; }
        public string PictureURL { get; set; }
        public string PicturePath { get; set; }
        public int FK_CategoryType_Id { get; set; }
        public UploadFile File { get; set; }
        public LKP_CategoryTypeViewModel CategoryType { get; set; }
        public LKP_CategoryConfigViewModel Config { get; set; }
        public List<LKP_ItemViewModel> Items { get; set; }
        public bool AcceptMulti { get; set; }
        public int Count { get; set; }
        public bool IsParent { get; set; }
        public int? Fk_Parent_Id { get; set; }
        public bool AllowTodayDelivery { get; set; }
        public bool IsSponsored { get; set; }
        public SortType ItemsSortType { get; set; }
    }
}
