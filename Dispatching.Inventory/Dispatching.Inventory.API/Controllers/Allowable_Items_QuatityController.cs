﻿using System;
using System.Collections.Generic;
using DispatchProduct.RepositoryModule;
using System.Text;
using Dispatching.Inventory.Models.Entities;
using Dispatching.Inventory.BLL.IManagers;
using Dispatching.Inventory.Models.Context;
using DispatchProduct.Controllers;
using AutoMapper;
using Utilites.ProcessingResult;
using Microsoft.AspNetCore.Mvc;
using Dispatching.Inventory.API.ViewModel;
using DispatchProduct.Controllers.V1;

namespace Dispatching.Inventory.API.Controllers
{
    [ApiVersion("1.0")]
    [Route("v{version:apiVersion}/[controller]")]
    public class Allowable_Items_QuatityController : BaseControllerV1<IAllowable_Items_QuatityManager, Allowable_Items_Quatity, Allowable_Items_QuatityViewModel>
    {
        public new IAllowable_Items_QuatityManager manger;
        public new readonly IMapper mapper;
        IProcessResultMapper processResultMapper;
        IProcessResultPaginatedMapper processResultPaginatedMapper;
        public Allowable_Items_QuatityController(IAllowable_Items_QuatityManager _manger, IMapper _mapper, IProcessResultMapper _processResultMapper, IProcessResultPaginatedMapper _processResultPaginatedMapper):base(_manger,_mapper,_processResultMapper,_processResultPaginatedMapper)
        {
            this.manger = _manger;
            this.mapper = _mapper;
            processResultMapper = _processResultMapper;
            processResultPaginatedMapper = _processResultPaginatedMapper;
        }
        [Route("GetByItemId/{id}")]
        [HttpGet]
        [MapToApiVersion("1.0")]
        public IProcessResultViewModel<List<Allowable_Items_QuatityViewModel>> GetByItemId([FromRoute]int id)
        {
            var entityResult = manger.GetByItemId(id);
            return processResultMapper.Map<List<Allowable_Items_Quatity>, List<Allowable_Items_QuatityViewModel>>(entityResult);
        }
    }
}

