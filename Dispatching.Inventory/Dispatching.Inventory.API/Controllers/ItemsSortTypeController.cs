﻿using BuisnessCommon.Enums;
using CommonEnums;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;

namespace Dispatching.Inventory.API.Controllers
{
    [ApiVersion("1.0")]
    [Route("v{version:apiVersion}/[controller]")]
    public class ItemsSortTypeController : Controller
    {

        public ItemsSortTypeController()
        {
        }

        [Route("GetAll")]
        [HttpGet]
        public List<EnumEntity> GetAll()
        {
            return EnumManager<SortType>.GetEnumList();
        }
    }
}
