﻿using System;
using System.Collections.Generic;
using System.Text;
using DispatchProduct.RepositoryModule;
using Dispatching.Inventory.Models.Entities;
using Dispatching.Inventory.BLL.IManagers;
using Dispatching.Inventory.Models.Context;
using Microsoft.AspNetCore.Mvc;
using DispatchProduct.Controllers;
using AutoMapper;
using Utilites.ProcessingResult;
using Dispatching.Inventory.API.ViewModel;
using Dispatching.Inventory.BLL.CommonModels;
using Microsoft.Extensions.Options;
using Dispatching.Inventory.Models.Settings;
using Utilites.UploadFile;
using Utilites.PaginatedItemsViewModel;
using DispatchProduct.Controllers.V1;
using BuildingBlocks.ServiceDiscovery;
using DnsClient;
using Microsoft.AspNetCore.Hosting;
using CommonEnums;
using System.Linq.Expressions;

namespace Dispatching.Inventory.API.Controllers
{

    [ApiVersion("1.0")]
    [Route("v{version:apiVersion}/[controller]")]
    public class LKP_CategoryController : BaseControllerV1<ILKP_CategoryManager, LKP_Category, LKP_CategoryViewModel>
    {
        public new ILKP_CategoryManager manger;
        public new readonly IMapper mapper;
        IProcessResultMapper processResultMapper;
        IProcessResultPaginatedMapper processResultPaginatedMapper;
        InventoryAppSettings settings;
        IUploadImageFileManager imageManager;
        ServiceDisvoveryOptions discoveryOptions;
        IHostingEnvironment _hostingEnv;
        public LKP_CategoryController(ILKP_CategoryManager _manger, IMapper _mapper, IProcessResultMapper _processResultMapper, IProcessResultPaginatedMapper _processResultPaginatedMapper, IOptions<InventoryAppSettings> _settings, IUploadImageFileManager _imageManager, IOptions<ServiceDisvoveryOptions> _discoveryOptions, IHostingEnvironment hostingEnv) : base(_manger, _mapper, _processResultMapper, _processResultPaginatedMapper)
        {
            _hostingEnv = hostingEnv;
            this.manger = _manger;
            this.mapper = _mapper;
            processResultMapper = _processResultMapper;
            processResultPaginatedMapper = _processResultPaginatedMapper;
            settings = _settings.Value;
            imageManager = _imageManager;
            discoveryOptions = _discoveryOptions.Value;
        }
        [Route("GetByCategoryTypeId/{id}")]
        [HttpGet]
        [MapToApiVersion("1.0")]
        public ProcessResultViewModel<List<LKP_CategoryViewModel>> GetByCategoryTypeId([FromRoute]int id)
        {
            var entityResult = manger.GetByCategoryTypeId(id);
            var result = processResultMapper.Map<List<LKP_Category>, List<LKP_CategoryViewModel>>(entityResult);
            if (result.IsSucceeded)
            {
                result.Data = BindFilesURL(result.Data);
            }
            return result;
        }
        [Route("SetAvailableDate")]
        [HttpPost]
        [MapToApiVersion("1.0")]
        public ProcessResultViewModel<bool> SetAvailableDate([FromBody]AvailableDateViewModel model)
        {
            var entityResult = manger.SetAvailableDate(model);
            return processResultMapper.Map<bool, bool>(entityResult);
        }
        [Route("Add")]
        [HttpPost]
        [MapToApiVersion("1.0")]
        public override ProcessResultViewModel<LKP_CategoryViewModel> Post([FromBody]LKP_CategoryViewModel model)
        {
            var result = base.Post(model);
            if (result != null && result.IsSucceeded && model.File != null)
            {
                model.File.FileName = result.Data.Id + ".jpeg";
                if (settings.CategoryImagesPath == null)
                {
                    settings.CategoryImagesPath = "CategoryImages";
                }
                var path = _hostingEnv.WebRootPath + "/" + settings.CategoryImagesPath + "/" + result.Data.Id;
                var dbPath = settings.CategoryImagesPath + "/" + result.Data.Id;
                var imageResult = imageManager.AddFile(model.File, path);
                if (imageResult.IsSucceeded)
                {
                    result.Data.PicturePath = $"{dbPath}/{model.File.FileName}";
                    var updateResult = base.Put(result.Data);
                    if (updateResult.IsSucceeded)
                    {
                        result.Data = BindFilesURL(result.Data);
                    }
                }
            }
            return result;
        }
        [Route("Update")]
        [HttpPut]
        [MapToApiVersion("1.0")]
        public override ProcessResultViewModel<bool> Put([FromBody] LKP_CategoryViewModel model)
        {
            ProcessResultViewModel<bool> result = null;
            if (model.File != null)
            {
                model.File.FileName = model.Id + ".jpeg";
                if (settings.CategoryImagesPath == null)
                {
                    settings.CategoryImagesPath = "CategoryImages";
                }
                var path = _hostingEnv.WebRootPath + "/" + settings.CategoryImagesPath + "/" + model.Id;
                var dbPath = settings.CategoryImagesPath + "/" + model.Id;
                var imageResult = imageManager.AddFile(model.File, path);
                if (imageResult.IsSucceeded)
                {
                    model.PicturePath = $"{dbPath}/{model.File.FileName}";
                }
            }
            else
            {
                var oldCatRes = Get(model.Id);
                if (oldCatRes.IsSucceeded && oldCatRes.Data != null)
                {
                    model.PicturePath = oldCatRes.Data.PicturePath;
                }
            }
            result = base.Put(model);
            model = BindFilesURL(model);
            return result;
        }
        [Route("Activate/{id}")]
        [MapToApiVersion("1.0")]
        [HttpGet]
        public ProcessResultViewModel<bool> Activate([FromRoute]int id)
        {
            var entityResult = manger.SetCategoryActivation(id, true);
            return processResultMapper.Map<bool, bool>(entityResult);
        }
        [Route("DeActivate/{id}")]
        [MapToApiVersion("1.0")]
        [HttpGet]
        public ProcessResultViewModel<bool> DeActivate([FromRoute]int id)
        {
            var entityResult = manger.SetCategoryActivation(id, false);
            return processResultMapper.Map<bool, bool>(entityResult);
        }

        public ProcessResultViewModel<List<LKP_CategoryViewModel>> GetAllActive(SystemCategoryType type = 0)
        {
            var entityResult = manger.GetAll(true, type);
            var result = processResultMapper.Map<List<LKP_Category>, List<LKP_CategoryViewModel>>(entityResult);
            if (result.IsSucceeded)
            {
                result.Data = BindFilesURL(result.Data);
            }
            return result;
        }

        public ProcessResultViewModel<List<LKP_CategoryViewModel>> GetAllActiveParents(SystemCategoryType type = 0)
        {
            var entityResult = manger.GetAllParents(true, type);
            var result = processResultMapper.Map<List<LKP_Category>, List<LKP_CategoryViewModel>>(entityResult);
            if (result.IsSucceeded)
            {
                result.Data = BindFilesURL(result.Data);
            }
            return result;
        }

        public ProcessResultViewModel<List<LKP_CategoryViewModel>> GetAllActiveByParent(int parentId, SystemCategoryType type = 0)
        {
            var entityResult = manger.GetAllByParentId(parentId, true, type);
            var result = processResultMapper.Map<List<LKP_Category>, List<LKP_CategoryViewModel>>(entityResult);
            if (result.IsSucceeded)
            {
                result.Data = BindFilesURL(result.Data);
            }
            return result;
        }

        [MapToApiVersion("1.0")]
        [Route("GetAllActive")]
        [HttpGet]
        public ProcessResultViewModel<List<LKP_CategoryViewModel>> GetAllActiveForIndividual()
        {
            return GetAllActive(SystemCategoryType.Individual);
        }

        [MapToApiVersion("1.0")]
        [Route("GetAllActiveParents")]
        [HttpGet]
        public ProcessResultViewModel<List<LKP_CategoryViewModel>> GetAllActiveParentsForIndividual()
        {
            return GetAllActiveParents(SystemCategoryType.Individual);
        }

        [MapToApiVersion("1.0")]
        [Route("GetAllActiveByParent")]
        [HttpGet]
        public ProcessResultViewModel<List<LKP_CategoryViewModel>> GetAllActiveByParentForIndividual([FromQuery]int parentId)
        {
            return GetAllActiveByParent(parentId, SystemCategoryType.Individual);
        }

        [Route("GetAllSalesActive")]
        [HttpGet]
        [MapToApiVersion("1.0")]
        public ProcessResultViewModel<List<LKP_CategoryViewModel>> GetAllActiveForSales()
        {
            return GetAllActive(SystemCategoryType.Sales);
        }

        [Route("GetAllSalesActiveParents")]
        [HttpGet]
        [MapToApiVersion("1.0")]
        public ProcessResultViewModel<List<LKP_CategoryViewModel>> GetAllActiveParentsForSales()
        {
            return GetAllActiveParents(SystemCategoryType.Sales);
        }

        [MapToApiVersion("1.0")]
        [Route("GetAllSalesActiveByParent")]
        [HttpGet]
        public ProcessResultViewModel<List<LKP_CategoryViewModel>> GetAllActiveByParentForSales([FromQuery]int parentId)
        {
            return GetAllActiveByParent(parentId, SystemCategoryType.Sales);
        }

        [Route("GetAllSalesDeActive")]
        [HttpGet]
        [MapToApiVersion("1.0")]
        public ProcessResultViewModel<List<LKP_CategoryViewModel>> GetAllSalesDeActive()
        {
            return GetAllDeActive(SystemCategoryType.Sales);
        }
        [MapToApiVersion("1.0")]
        [Route("GetAll")]
        [HttpGet]
        public override ProcessResultViewModel<List<LKP_CategoryViewModel>> Get()
        {
            var result = base.Get();
            if (result.IsSucceeded)
            {
                result.Data = BindFilesURL(result.Data);
            }
            return result;
        }
        [Route("Get")]
        [HttpGet]
        [MapToApiVersion("1.0")]
        public override ProcessResultViewModel<LKP_CategoryViewModel> Get([FromRoute] int id)
        {
            var result = base.Get(id);
            if (result.IsSucceeded)
            {
                result.Data = BindFilesURL(result.Data);
            }
            return result;
        }

        [Route("GetAllPaginated")]
        [MapToApiVersion("1.0")]
        public override ProcessResultViewModel<PaginatedItemsViewModel<LKP_CategoryViewModel>> GetAllPaginated(PaginatedItemsViewModel<LKP_CategoryViewModel> model = null)
        {
            var result = base.GetAllPaginated(model);
            if (result.IsSucceeded)
            {
                result.Data.Data = BindFilesURL(result.Data.Data);
            }
            return result;
        }
        [Route("GetAllDeActive")]
        [HttpGet]
        [MapToApiVersion("1.0")]
        public ProcessResultViewModel<List<LKP_CategoryViewModel>> GetAllDeActiveIndividual(SystemCategoryType type)
        {
            return GetAllDeActive(type);
        }

        public ProcessResultViewModel<List<LKP_CategoryViewModel>> GetAllDeActive(SystemCategoryType type)
        {
            var entityResult = manger.GetAll(false, type);
            var result = processResultMapper.Map<List<LKP_Category>, List<LKP_CategoryViewModel>>(entityResult);
            if (result.IsSucceeded)
            {
                result.Data = BindFilesURL(result.Data);
            }
            return result;
        }
        private List<LKP_CategoryViewModel> BindFilesURL(List<LKP_CategoryViewModel> model)
        {
            foreach (var item in model)
            {
                BindFilesURL(item);
            }
            return model;
        }
        private LKP_CategoryViewModel BindFilesURL(LKP_CategoryViewModel model)
        {
            var baseURl = $"{settings.APIGateway}/{discoveryOptions.ServiceName}";

            if (model.PicturePath != null)
            {
                model.PicturePath = model.PicturePath.Replace('\\', '/');
                model.PictureURL = $"{baseURl}/{model.PicturePath}";
            }
            return model;
        }
    }
}

