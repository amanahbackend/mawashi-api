﻿using System.ComponentModel.DataAnnotations.Schema;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;
using DispatchProduct.RepositoryModule;
using Dispatching.Inventory.Models.Entities;
using Dispatching.Inventory.BLL.IManagers;
using Dispatching.Inventory.Models.Context;
using DispatchProduct.Controllers;
using AutoMapper;
using Utilites.ProcessingResult;
using Microsoft.AspNetCore.Mvc;
using Dispatching.Inventory.API.ViewModel;
using Dispatching.Inventory.BLL.CommonModels;
using Utilites.UploadFile;
using Microsoft.Extensions.Options;
using Microsoft.Extensions.DependencyInjection;
using Dispatching.Inventory.Models.Settings;
using Utilites.PaginatedItemsViewModel;
using DispatchProduct.Controllers.V1;
using BuildingBlocks.ServiceDiscovery;
using DnsClient;
using Microsoft.AspNetCore.Hosting;
using CommonEnums;
using System.Linq.Expressions;
using Dispatching.BuisnessCommon.Enums;

namespace Dispatching.Inventory.API.Controllers
{
    [ApiVersion("1.0")]
    [Route("v{version:apiVersion}/[controller]")]
    public class LKP_ItemController : BaseControllerV1<ILKP_ItemManager, LKP_Item, LKP_ItemViewModel>
    {
        public new ILKP_ItemManager manger;
        public IUploadImageFileManager imageManager;
        public new readonly IMapper mapper;
        IProcessResultMapper processResultMapper;
        IProcessResultPaginatedMapper processResultPaginatedMapper;
        InventoryAppSettings settings;
        ServiceDisvoveryOptions discoveryOptions;
        IServiceProvider serviceProvider;
        IHostingEnvironment _hostingEnv;
        public LKP_ItemController(IServiceProvider _serviceProvider, ILKP_ItemManager _manger, IMapper _mapper, IProcessResultMapper _processResultMapper, IProcessResultPaginatedMapper _processResultPaginatedMapper, IUploadImageFileManager _imageManager, IOptions<InventoryAppSettings> _settings, IOptions<ServiceDisvoveryOptions> _discoveryOptions, IHostingEnvironment hostingEnv) : base(_manger, _mapper, _processResultMapper, _processResultPaginatedMapper)
        {
            _hostingEnv = hostingEnv;
            this.manger = _manger;
            this.mapper = _mapper;
            processResultMapper = _processResultMapper;
            processResultPaginatedMapper = _processResultPaginatedMapper;
            imageManager = _imageManager;
            settings = _settings.Value;
            discoveryOptions = _discoveryOptions.Value;
            serviceProvider = _serviceProvider;
        }
        [Route("GetAll")]
        [HttpGet]
        [MapToApiVersion("1.0")]
        public override ProcessResultViewModel<List<LKP_ItemViewModel>> Get()
        {
            var result = base.Get();
            if (result.IsSucceeded)
            {
                result.Data = BindFilesURL(result.Data);
                result.Data = BindCurrency(result.Data);
            }
            return result;
        }
        [Route("Get")]
        [HttpGet]
        [MapToApiVersion("1.0")]
        public override ProcessResultViewModel<LKP_ItemViewModel> Get([FromRoute] int id)
        {
            var result = base.Get(id);
            if (result.IsSucceeded)
            {
                result.Data = BindFilesURL(result.Data);
                result.Data = BindCurrency(result.Data);

            }
            return result;
        }
        [Route("GetItemDetailsById/{id}")]
        [HttpGet]
        [MapToApiVersion("1.0")]
        public ProcessResultViewModel<ItemDetailsViewModel> GetItemDetailsById([FromRoute] int id)
        {
            var entityResult = manger.GetItemDetails(id);
            return processResultMapper.Map<ItemDetailsModel, ItemDetailsViewModel>(entityResult);
        }
        [HttpPost]
        [Route("GetAllPaginated")]
        [MapToApiVersion("1.0")]
        public override ProcessResultViewModel<PaginatedItemsViewModel<LKP_ItemViewModel>> GetAllPaginated(PaginatedItemsViewModel<LKP_ItemViewModel> model = null)
        {
            var result = base.GetAllPaginated(model);
            if (result.IsSucceeded)
            {
                result.Data.Data = BindFilesURL(result.Data.Data);
                result.Data.Data = BindCurrency(result.Data.Data);

            }
            return result;
        }
        [Route("GetByIds")]
        [MapToApiVersion("1.0")]
        [HttpPost]
        public override ProcessResultViewModel<List<LKP_ItemViewModel>> GetByIds([FromBody] List<int> ids)
        {
            var result = base.GetByIds(ids);
            if (result.IsSucceeded && result.Data != null)
            {
                result.Data = BindFilesURL(result.Data);
                result.Data = BindCurrency(result.Data);

            }
            return result;
        }
        [Route("Add")]
        [HttpPost]
        [MapToApiVersion("1.0")]
        public override ProcessResultViewModel<LKP_ItemViewModel> Post([FromBody]LKP_ItemViewModel model)
        {
            var currencyResult = LKP_CurrencyManager.GetDefaultCurrency();
            if (currencyResult.IsSucceeded && currencyResult.Data != null)
            {
                model.FK_Currency_Id = currencyResult.Data.Id;
            }
            var result = base.Post(model);
            if (result != null && result.IsSucceeded && model.File != null)
            {

                model.File.FileName = result.Data.Id + ".jpeg";
                if (settings.ItemImagesPath == null)
                {
                    settings.ItemImagesPath = "ItemImages";
                }
                var path = _hostingEnv.WebRootPath + "/" + settings.ItemImagesPath + "/" + result.Data.Id;
                var dbPath = settings.ItemImagesPath + "/" + result.Data.Id;
                var imageResult = imageManager.AddFile(model.File, path);
                if (imageResult.IsSucceeded)
                {
                    result.Data.PicturePath = $"{dbPath}/{model.File.FileName}";
                    var updateResult = base.Put(result.Data);
                    if (updateResult.IsSucceeded)
                    {
                        result.Data = BindFilesURL(result.Data);
                        result.Data = BindCurrency(result.Data);
                    }
                }
            }
            return result;
        }
        [Route("Update")]
        [HttpPost]
        [MapToApiVersion("1.0")]
        public override ProcessResultViewModel<bool> Put([FromBody] LKP_ItemViewModel model)
        {

            if (model.FK_Currency_Id == 0)
            {
                var currencyResult = LKP_CurrencyManager.GetDefaultCurrency();
                if (currencyResult.IsSucceeded && currencyResult.Data != null)
                {
                    model.FK_Currency_Id = currencyResult.Data.Id;
                }
            }
            ProcessResultViewModel<bool> result = null;
            if (model.File != null)
            {
                model.File.FileName = model.Id + ".jpeg";
                if (settings.ItemImagesPath == null)
                {
                    settings.ItemImagesPath = "ItemImages";
                }
                var path = _hostingEnv.WebRootPath + "/" + settings.ItemImagesPath + "/" + model.Id;
                var dbPath = settings.ItemImagesPath + "/" + model.Id;
                var imageResult = imageManager.AddFile(model.File, path);
                if (imageResult.IsSucceeded)
                {
                    model.PicturePath = $"{dbPath}/{model.File.FileName}";
                }
            }
            else
            {

                var oldItemRes = Get(model.Id);
                if (oldItemRes.IsSucceeded && oldItemRes.Data != null)
                {
                    model.PicturePath = oldItemRes.Data.PicturePath;
                }
            }
            result = base.Put(model);
            if (result.IsSucceeded)
            {
                model = BindFilesURL(model);
                model = BindCurrency(model);
            }
            return result;
        }
        [Route("Submit")]
        [HttpPost]
        [MapToApiVersion("1.0")]
        public ProcessResultViewModel<SubmitItemViewModel> Submit([FromBody]SubmitItemViewModel model)
        {
            ProcessResultViewModel<SubmitItemViewModel> result = null;
            var addOrUpdate = false;
            model = BindminMaxItemNo(model);
            if (model.Item.Id > 0)
            {

                var updateRes = Put(model.Item);
                if (updateRes.IsSucceeded)
                {
                    addOrUpdate = true;
                }
                else
                {
                    result = ProcessResultViewModelHelper.MapToProcessResultViewModel<bool, SubmitItemViewModel>(updateRes);
                }
            }
            else
            {
                var itemRes = Post(model.Item);
                if (itemRes.IsSucceeded)
                {
                    addOrUpdate = true;
                    model.Item = itemRes.Data;
                }
                else
                {
                    result = ProcessResultViewModelHelper.MapToProcessResultViewModel<LKP_ItemViewModel, SubmitItemViewModel>(itemRes);
                }
            }

            if (addOrUpdate)
            {
                return SubmitCuttingQuantities(model);
            }
            return result;
        }
        public SubmitItemViewModel BindminMaxItemNo(SubmitItemViewModel model)
        {
            if (model.Item != null && model.QuantitiesIds != null && model.QuantitiesIds.Count > 0)
            {
                var minRes = QuantityManager.GetMinimumQuantity(model.QuantitiesIds);
                if (minRes.IsSucceeded)
                {
                    model.Item.MinimumNo = minRes.Data;
                }
                var maxRes = QuantityManager.GetMaxQuantity(model.QuantitiesIds);
                if (maxRes.IsSucceeded)
                {
                    model.Item.MaximumNo = maxRes.Data;
                }
            }
            return model;
        }
        public ProcessResultViewModel<SubmitItemViewModel> SubmitCuttingQuantities([FromBody]SubmitItemViewModel model)
        {
            ProcessResultViewModel<SubmitItemViewModel> result = null;
            if (model.CuttingIds != null)
            {
                var cutRes = AllowableCuttingManager.Submit(model.CuttingIds, model.Item.Id);
                if (cutRes.IsSucceeded)
                {
                    model.Cuttings = mapper.Map<List<Allowable_Items_Cutting>, List<Allowable_Items_CuttingViewModel>>(cutRes.Data);


                }
                else
                {
                    result = ProcessResultViewModelHelper.Failed<SubmitItemViewModel>(null, cutRes.Exception.Message);
                }
            }
            if (model.QuantitiesIds != null)
            {
                var quantRes = AllowableQuatityManager.Submit(model.QuantitiesIds, model.Item.Id);
                if (quantRes.IsSucceeded)
                {
                    model.Quantities = mapper.Map<List<Allowable_Items_Quatity>, List<Allowable_Items_QuatityViewModel>>(quantRes.Data);
                    result = ProcessResultViewModelHelper.Succedded(model);
                }
                else
                {
                    result = ProcessResultViewModelHelper.Failed<SubmitItemViewModel>(null, quantRes.Exception.Message);
                }
            }
            if (result == null)
            {
                result = ProcessResultViewModelHelper.Succedded(model);
            }
            return result;
        }

        [Route("GetActiveByCategoryId/{id}")]
        [HttpGet]
        [MapToApiVersion("1.0")]
        public ProcessResultViewModel<List<LKP_ItemViewModel>> GetActiveByCategoryId([FromRoute]int id)
        {
            var entityResult = manger.GetByCategoryId(id, true);
            var result = processResultMapper.Map<List<LKP_Item>, List<LKP_ItemViewModel>>(entityResult);
            if (result.IsSucceeded)
            {
                result.Data = BindFilesURL(result.Data);
                result.Data = BindCurrency(result.Data);
            }
            return result;
        }

        [Route("GetActiveByCategoryId/Web/{id}")]
        [HttpGet]
        [MapToApiVersion("1.0")]
        public ProcessResultViewModel<List<LKP_ItemViewModel>> GetActiveByCategoryIdForWeb([FromRoute]int id)
        {
            var result = GetActiveByCategoryId(id);
            if (result.IsSucceeded && result.Data != null && result.Data.Count > 0)
            {

                var catRes = CategoryController.Get(id);
                if (catRes.IsSucceeded && catRes.Data != null)
                {
                    foreach (var item in result.Data)
                    {
                        item.Category = catRes.Data;
                    }
                }
            }
            return result;
        }
        [Route("GetDeActiveByCategoryId/{id}")]
        [HttpGet]
        [MapToApiVersion("1.0")]
        public ProcessResultViewModel<List<LKP_ItemViewModel>> GetDeActiveByCategoryId([FromRoute]int id)
        {
            var entityResult = manger.GetByCategoryId(id, false);
            var result = processResultMapper.Map<List<LKP_Item>, List<LKP_ItemViewModel>>(entityResult);
            if (result.IsSucceeded)
            {
                result.Data = BindFilesURL(result.Data);
                result.Data = BindCurrency(result.Data);
            }
            return result;
        }
        [Route("SetAvailableDate")]
        [HttpPost]
        [MapToApiVersion("1.0")]
        public ProcessResultViewModel<bool> SetAvailableDate([FromBody]AvailableDateViewModel model)
        {
            var entityResult = manger.SetAvailableDate(model);
            return processResultMapper.Map<bool, bool>(entityResult);
        }

        [Route("Activate/{id}")]
        [HttpGet]
        [MapToApiVersion("1.0")]
        public ProcessResultViewModel<bool> Activate([FromRoute]int id)
        {
            var entityResult = manger.SetItemActivation(id, true);
            return processResultMapper.Map<bool, bool>(entityResult);
        }
        [Route("DeActivate/{id}")]
        [HttpGet]
        [MapToApiVersion("1.0")]
        public ProcessResultViewModel<bool> DeActivate([FromRoute]int id)
        {
            var entityResult = manger.SetItemActivation(id, false);
            return processResultMapper.Map<bool, bool>(entityResult);
        }


        public ProcessResultViewModel<List<LKP_ItemViewModel>> GetAllActive(SystemCategoryType type = 0)
        {
            var entityResult = manger.GetAll(true, type);
            var result = processResultMapper.Map<List<LKP_Item>, List<LKP_ItemViewModel>>(entityResult);
            if (result.IsSucceeded)
            {
                result.Data = BindFilesURL(result.Data);
                result.Data = BindCurrency(result.Data);
            }
            return result;
        }

        [Route("GetAllActive")]
        [HttpGet]
        [MapToApiVersion("1.0")]
        public ProcessResultViewModel<List<LKP_ItemViewModel>> GetAllActiveForIndividual()
        {
            return GetAllActive(SystemCategoryType.Individual);
        }

        [Route("GetAllSalesActive")]
        [HttpGet]
        [MapToApiVersion("1.0")]
        public ProcessResultViewModel<List<LKP_ItemViewModel>> GetAllActiveForSales()
        {
            return GetAllActive(SystemCategoryType.Sales);
        }
        [Route("GetAllDeActive")]
        [HttpGet]
        [MapToApiVersion("1.0")]
        public ProcessResultViewModel<List<LKP_ItemViewModel>> GetAllDeActive()
        {
            var entityResult = manger.GetAll(false);
            var result = processResultMapper.Map<List<LKP_Item>, List<LKP_ItemViewModel>>(entityResult);
            if (result.IsSucceeded)
            {
                result.Data = BindFilesURL(result.Data);
                result.Data = BindCurrency(result.Data);

            }
            return result;
        }

        [Route("CanCombineItems")]
        [HttpPost]
        [MapToApiVersion("1.0")]
        public ProcessResultViewModel<bool> CanCombineItems([FromBody]List<int> itemIds)
        {
            var entityResult = manger.CanCombineItems(itemIds);
            return processResultMapper.Map<bool, bool>(entityResult);
        }
        [Route("UpdateStock")]
        [HttpPost]
        [MapToApiVersion("1.0")]
        public ProcessResultViewModel<bool> UpdateStock([FromBody]List<UpdateItemsStockViewModel> model)
        {
            ProcessResultViewModel<bool> result = null;
            foreach (var item in model)
            {
                var updateStockRes = manger.UpdateStock(item.ItemId, item.Count, item.OrderType, item.MeasurmentType);
                if (!updateStockRes.IsSucceeded || !updateStockRes.Data)
                {
                    result = processResultMapper.Map<bool, bool>(updateStockRes);
                    return result;
                }
            }
            result = ProcessResultViewModelHelper.Succedded(true);
            return result;
        }

        [HttpPost, Route("CheckItemsAvailability"), MapToApiVersion("1.0")]
        public ProcessResultViewModel<List<ItemAvailabilityViewModel>> CheckItemsAvailability([FromBody]List<CheckItemAvailabilityViewModel> models)
        {
            var result = new ProcessResultViewModel<List<ItemAvailabilityViewModel>>();
            try
            {
                var resultData = new List<ItemAvailabilityViewModel>();
                foreach (var item in models)
                {
                    var itemResult = manger.Get(item.ItemId);
                    if (itemResult != null && itemResult.IsSucceeded && itemResult.Data != null)
                    {
                        var quantityResult = QuantityManager.Get(item.QuantityId);
                        if (quantityResult != null && quantityResult.IsSucceeded && quantityResult.Data != null)
                        {
                            var model = new ItemAvailabilityViewModel()
                            {
                                MeasurmentType = item.MeasurmentType,
                                OrderType = item.OrderType,
                                DescriptionEN = itemResult.Data.DescriptionEN
                            };
                            switch (item.OrderType)
                            {
                                case OrderType.Individual:
                                    model.IsAvailable = itemResult.Data.CurrentStock >= quantityResult.Data.Value;
                                    model.CurrentStock = itemResult.Data.CurrentStock;
                                    break;
                                case OrderType.Retail:
                                    switch (item.MeasurmentType)
                                    {
                                        case MeasurmentType.Head:
                                            model.IsAvailable = itemResult.Data.CurrentHeadStock >= quantityResult.Data.Value;
                                            model.CurrentStock = itemResult.Data.CurrentHeadStock;
                                            break;
                                        case MeasurmentType.Kgs:
                                            model.IsAvailable = itemResult.Data.CurrentKgsStock >= quantityResult.Data.Value;
                                            model.CurrentStock = itemResult.Data.CurrentKgsStock;
                                            break;
                                        case MeasurmentType.Pieces:
                                            model.IsAvailable = itemResult.Data.CurrentPiecesStock >= quantityResult.Data.Value;
                                            model.CurrentStock = itemResult.Data.CurrentPiecesStock;
                                            break;
                                        case MeasurmentType.Box:
                                            break;
                                    }
                                    break;
                            }
                            resultData.Add(model);
                        }
                        else
                        {
                            result = processResultMapper.Map<LKP_Quantity, List<ItemAvailabilityViewModel>>(quantityResult);
                        }
                    }
                    else
                    {
                        result = processResultMapper.Map<LKP_Item, List<ItemAvailabilityViewModel>>(itemResult);
                    }
                }
                result = ProcessResultViewModelHelper.Succedded(resultData);
            }
            catch (Exception ex)
            {
                result = ProcessResultViewModelHelper.Failed<List<ItemAvailabilityViewModel>>(null, ex.Message);
            }
            return result;
        }

        private List<LKP_ItemViewModel> BindFilesURL(List<LKP_ItemViewModel> model)
        {
            foreach (var item in model)
            {
                BindFilesURL(item);
            }
            return model;
        }
        private LKP_ItemViewModel BindFilesURL(LKP_ItemViewModel model)
        {
            var baseURl = $"{settings.APIGateway}/{discoveryOptions.ServiceName}";
            if (model.PicturePath != null)
            {
                model.PicturePath = model.PicturePath.Replace('\\', '/');
                model.PictureURL = $"{baseURl}/{model.PicturePath}";
            }

            return model;
        }
        private LKP_ItemViewModel BindCurrency(LKP_ItemViewModel model)
        {
            var curncyRes = LKP_CurrencyController.Get(model.FK_Currency_Id);
            if (curncyRes.IsSucceeded && curncyRes.Data != null)
            {
                model.Currency = curncyRes.Data;
            }
            return model;
        }
        private List<LKP_ItemViewModel> BindCurrency(List<LKP_ItemViewModel> model)
        {
            foreach (var item in model)
            {
                BindCurrency(item);
            }
            return model;
        }
        private ILKP_CurrencyManager LKP_CurrencyManager
        {
            get { return serviceProvider.GetService<ILKP_CurrencyManager>(); }
        }
        private IAllowable_Items_QuatityManager AllowableQuatityManager
        {
            get { return serviceProvider.GetService<IAllowable_Items_QuatityManager>(); }
        }
        private ILKP_QuantityManager QuantityManager
        {
            get { return serviceProvider.GetService<ILKP_QuantityManager>(); }
        }
        private IAllowable_Items_CuttingManager AllowableCuttingManager
        {
            get { return serviceProvider.GetService<IAllowable_Items_CuttingManager>(); }
        }
        private LKP_CurrencyController LKP_CurrencyController
        {
            get { return serviceProvider.GetService<LKP_CurrencyController>(); }
        }
        private LKP_CategoryController CategoryController
        {
            get { return serviceProvider.GetService<LKP_CategoryController>(); }
        }
    }
}

