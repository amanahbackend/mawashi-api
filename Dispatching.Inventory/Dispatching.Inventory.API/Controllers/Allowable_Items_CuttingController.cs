﻿using AutoMapper;
using Dispatching.Inventory.API.ViewModel;
using Dispatching.Inventory.BLL.IManagers;
using Dispatching.Inventory.Models.Entities;
using DispatchProduct.Controllers;
using DispatchProduct.Controllers.V1;
using Utilites.ProcessingResult;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Security.Claims;
using System.Text;


namespace Dispatching.Inventory.API.Controllers
{
    [ApiVersion("1.0")]
    [Route("v{version:apiVersion}/[controller]")]
    public class Allowable_Items_CuttingController : BaseControllerV1<IAllowable_Items_CuttingManager, Allowable_Items_Cutting, Allowable_Items_CuttingViewModel>
    {
        public new IAllowable_Items_CuttingManager manger;
        public new readonly IMapper mapper;
        IProcessResultMapper processResultMapper;
        IProcessResultPaginatedMapper processResultPaginatedMapper;
        public Allowable_Items_CuttingController(IAllowable_Items_CuttingManager _manger, IMapper _mapper, IProcessResultMapper _processResultMapper, IProcessResultPaginatedMapper _processResultPaginatedMapper):base(_manger,_mapper,_processResultMapper,_processResultPaginatedMapper)
        {
            this.manger = _manger;
            this.mapper = _mapper;
            processResultMapper = _processResultMapper;
            processResultPaginatedMapper = _processResultPaginatedMapper;
        }
        [Route("GetByItemId/{id}")]
        [HttpGet]
        [MapToApiVersion("1.0")]
        public IProcessResultViewModel<List<Allowable_Items_CuttingViewModel>> GetByItemId([FromRoute]int id)
        {
            var entityResult = manger.GetByItemId(id);
            return processResultMapper.Map<List<Allowable_Items_Cutting>, List<Allowable_Items_CuttingViewModel>>(entityResult);
        }

        
    }

}
