﻿using AutoMapper;
using Dispatching.Inventory.API.ViewModel;
using Dispatching.Inventory.BLL.IManagers;
using Dispatching.Inventory.Models.Entities;
using DispatchProduct.Controllers.V1;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using Utilites.ProcessingResult;

namespace Dispatching.Inventory.API.Controllers
{
    [ApiVersion("1.0")]
    [Route("v{version:apiVersion}/[controller]")]
    public class LKP_CategoryTypeController : BaseControllerV1<ILKP_CategoryTypeManager, LKP_CategoryType, LKP_CategoryTypeViewModel>
    {
        public LKP_CategoryTypeController(ILKP_CategoryTypeManager _manger, IMapper _mapper, IProcessResultMapper _processResultMapper, IProcessResultPaginatedMapper _processResultPaginatedMapper) : base(_manger, _mapper, _processResultMapper, _processResultPaginatedMapper)
        {

        }

        [HttpGet, Route("GetAll"), MapToApiVersion("1.0")]
        public override ProcessResultViewModel<List<LKP_CategoryTypeViewModel>> Get()
        {
            return base.Get();
        }

        [HttpGet, Route("Get/{id}"), MapToApiVersion("1.0")]
        public override ProcessResultViewModel<LKP_CategoryTypeViewModel> Get([FromRoute] int id)
        {
            return base.Get(id);
        }
    }
}

