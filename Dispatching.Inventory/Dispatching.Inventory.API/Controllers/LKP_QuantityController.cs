﻿using Dispatching.Inventory.BLL.IManagers;
using Dispatching.Inventory.Models.Context;
using Dispatching.Inventory.Models.Entities;
using DispatchProduct.RepositoryModule;
using System;
using System.Collections.Generic;
using System.Text;
using DispatchProduct.Controllers;
using AutoMapper;
using Utilites.ProcessingResult;
using Microsoft.AspNetCore.Mvc;
using Dispatching.Inventory.API.ViewModel;
using DispatchProduct.Controllers.V1;

namespace Dispatching.Inventory.API.Controllers
{
    [ApiVersion("1.0")]
    [Route("v{version:apiVersion}/[controller]")]
    public class LKP_QuantityController : BaseControllerV1<ILKP_QuantityManager, LKP_Quantity, LKP_QuantityViewModel>
    {
        public LKP_QuantityController(ILKP_QuantityManager _manger, IMapper _mapper, IProcessResultMapper _processResultMapper, IProcessResultPaginatedMapper _processResultPaginatedMapper) : base(_manger, _mapper, _processResultMapper, _processResultPaginatedMapper)
        {

        }
    }
}
