﻿using Dispatching.Inventory.BLL.IManagers;
using Dispatching.Inventory.Models.Context;
using Dispatching.Inventory.Models.Entities;
using DispatchProduct.RepositoryModule;
using System;
using System.Collections.Generic;
using System.Text;
using DispatchProduct.Controllers;
using AutoMapper;
using Utilites.ProcessingResult;
using Microsoft.AspNetCore.Mvc;
using Dispatching.Inventory.API.ViewModel;
using DispatchProduct.Controllers.V1;

namespace Dispatching.Inventory.API.Controllers
{
    [ApiVersion("1.0")]
    [Route("v{version:apiVersion}/[controller]")]
    public class LKP_CurrencyController : BaseControllerV1<ILKP_CurrencyManager, LKP_Currency, LKP_CurrencyViewModel>
    {
        public new ILKP_CurrencyManager manger;
        public new readonly IMapper mapper;
        IProcessResultMapper processResultMapper;
        IProcessResultPaginatedMapper processResultPaginatedMapper;
        public LKP_CurrencyController(ILKP_CurrencyManager _manger, IMapper _mapper, IProcessResultMapper _processResultMapper, IProcessResultPaginatedMapper _processResultPaginatedMapper) : base(_manger, _mapper, _processResultMapper, _processResultPaginatedMapper)
        {
            this.manger = _manger;
            this.mapper = _mapper;
            processResultMapper = _processResultMapper;
            processResultPaginatedMapper = _processResultPaginatedMapper;
        }
        [Route("CurrentCurrency")]
        [HttpGet]
        [MapToApiVersion("1.0")]
        public ProcessResultViewModel<LKP_CurrencyViewModel> GetDefaultCurrency()
        {
           var entityRes= manger.GetDefaultCurrency();
           return  processResultMapper.Map<LKP_Currency, LKP_CurrencyViewModel>(entityRes);
        }
    }
}
