﻿using AutoMapper;
using Dispatching.Inventory.API.ViewModel;
using Dispatching.Inventory.BLL.IManagers;
using Dispatching.Inventory.Models.Entities;
using DispatchProduct.Controllers.V1;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using Utilites.ProcessingResult;

namespace Dispatching.Inventory.API.Controllers
{
    [ApiVersion("1.0")]
    [Route("v{version:apiVersion}/[controller]")]
    public class LKP_CategoryConfigController : BaseControllerV1<ILKP_CategoryConfigManager, LKP_CategoryConfig, LKP_CategoryConfigViewModel>
    {
        public new ILKP_CategoryConfigManager manger;
        public new readonly IMapper mapper;
        IProcessResultMapper processResultMapper;
        IProcessResultPaginatedMapper processResultPaginatedMapper;
        public LKP_CategoryConfigController(ILKP_CategoryConfigManager _manger, IMapper _mapper, IProcessResultMapper _processResultMapper, IProcessResultPaginatedMapper _processResultPaginatedMapper) : base(_manger, _mapper, _processResultMapper, _processResultPaginatedMapper)
        {
            manger = _manger;
            mapper = _mapper;
            processResultMapper = _processResultMapper;
            processResultPaginatedMapper = _processResultPaginatedMapper;
        }

        [HttpGet, Route("GetAll"), MapToApiVersion("1.0")]
        public override ProcessResultViewModel<List<LKP_CategoryConfigViewModel>> Get()
        {
            return base.Get();
        }

        [HttpGet, Route("Get/{id}"), MapToApiVersion("1.0")]
        public override ProcessResultViewModel<LKP_CategoryConfigViewModel> Get([FromRoute] int id)
        {
            return base.Get(id);
        }

        [HttpPost]
        [Route("UpdateCombineCategory")]
        [MapToApiVersion("1.0")]
        public ProcessResultViewModel<bool> UpdateCombineCategory([FromBody]UpdateAcceptMultiViewModel model)
        {
            var entityResult = manger.UpdateAcceptMulti(model.AcceptMulti, model.FK_CategoryConfig_Id);
            return processResultMapper.Map<bool, bool>(entityResult);
        }
    }
}

