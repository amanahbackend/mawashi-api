﻿using System;
using System.Collections.Generic;
using System.Text;
using DispatchProduct.RepositoryModule;
using Dispatching.Inventory.Models.Entities;
using Dispatching.Inventory.BLL.IManagers;
using Dispatching.Inventory.Models.Context;
using Microsoft.AspNetCore.Mvc;
using DispatchProduct.Controllers;
using AutoMapper;
using Utilites.ProcessingResult;
using Dispatching.Inventory.API.ViewModel;
using DispatchProduct.Controllers.V1;
using CommonEnums;

namespace Dispatching.Inventory.API.Controllers
{
    [ApiVersion("1.0")]
    [Route("v{version:apiVersion}/[controller]")]
    public class CategoryDeliveryOptionsController : BaseControllerV1<ICategoryDeliveryOptionsManager, CategoryDeliveryOptions, CategoryDeliveryOptionsViewModel>
    {
        public new ICategoryDeliveryOptionsManager manger;
        public new readonly IMapper mapper;
        IProcessResultMapper processResultMapper;
        IProcessResultPaginatedMapper processResultPaginatedMapper;
        public CategoryDeliveryOptionsController(ICategoryDeliveryOptionsManager _manger, IMapper _mapper, IProcessResultMapper _processResultMapper, IProcessResultPaginatedMapper _processResultPaginatedMapper) : base(_manger, _mapper, _processResultMapper, _processResultPaginatedMapper)
        {
            manger = _manger;
            mapper = _mapper;
            processResultMapper = _processResultMapper;
            processResultPaginatedMapper = _processResultPaginatedMapper;
        }
        [Route("GetDateType")]
        [HttpPost]
        [MapToApiVersion("1.0")]
        public ProcessResultViewModel<int> GetDateType([FromBody]CategoryInputDeliveryOptions model)
        {
            var entityRes=manger.GetDateType(model.FK_CategoryConfig_Id, model.DeliveryId);
            return processResultMapper.Map< int, int>(entityRes);
        }
        [Route("GetDeliveryOptions/{configId}")]
        [HttpGet]
        [MapToApiVersion("1.0")]
        public ProcessResultViewModel<List<EnumEntity>> GetDeliveryOptions(int configId)
        {
            var entityRsult = manger.GetDeliveryOptionsByConfigId(configId);
            return processResultMapper.Map<List<EnumEntity>, List<EnumEntity>>(entityRsult);
        }
    }
}

