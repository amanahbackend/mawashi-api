﻿using Dispatching.Inventory.BLL.IManagers;
using Dispatching.Inventory.Models.Context;
using Dispatching.Inventory.Models.Entities;
using DispatchProduct.RepositoryModule;
using System;
using System.Collections.Generic;
using System.Text;
using DispatchProduct.Controllers;
using AutoMapper;
using Utilites.ProcessingResult;
using Microsoft.AspNetCore.Mvc;
using Dispatching.Inventory.API.ViewModel;
using DispatchProduct.Controllers.V1;

namespace Dispatching.Inventory.API.Controllers
{
    [ApiVersion("1.0")]
    [Route("v{version:apiVersion}/[controller]")]
    public class VATController : BaseControllerV1<ILKP_VATManager, LKP_VAT, LKP_VATViewModel>
    {
        public new ILKP_VATManager manger;
        public new readonly IMapper mapper;
        IProcessResultMapper processResultMapper;
        IProcessResultPaginatedMapper processResultPaginatedMapper;
        public VATController(ILKP_VATManager _manger, IMapper _mapper, IProcessResultMapper _processResultMapper, IProcessResultPaginatedMapper _processResultPaginatedMapper) : base(_manger, _mapper, _processResultMapper, _processResultPaginatedMapper)
        {

            this.manger = _manger;
            this.mapper = _mapper;
            processResultMapper = _processResultMapper;
            processResultPaginatedMapper = _processResultPaginatedMapper;
        }
        [Route("GetVat/{vatName}")]
        [HttpGet]
        [MapToApiVersion("1.0")]
        public ProcessResultViewModel<LKP_VATViewModel> GetVat([FromRoute]string vatName = null)
        {
            var entityRes = manger.GetVat(vatName);
            return processResultMapper.Map<LKP_VAT, LKP_VATViewModel>(entityRes);
        }
    }
}
