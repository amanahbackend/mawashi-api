﻿
using DispatchProduct.RepositoryModule;
using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Dispatching.Inventory.Models.Context;
using Dispatching.Inventory.Models.Entities;
using Dispatching.Inventory.Models.Settings;
using CommonEnums;

namespace DispatchProduct.Inventory.API.Seed
{
    public class InventoryDbContextSeed : ContextSeed
    {
        InventoryAppSettings settings = null;
        public async Task SeedAsync(InventoryDbContext context, IHostingEnvironment env,
            ILogger<InventoryDbContextSeed> logger, int? retry = 0)
        {
            int retryForAvaiability = retry.Value;

            try
            {
               // settings = appSettings.Value;
                var contentRootPath = env.ContentRootPath;
                var webroot = env.WebRootPath;
                var lstLKP_Item = new List<LKP_Item>();

                var currencies = GetDefaultCurrencies();
                var vat = GetDefaultVAT();

                var lstLKP_Category = GetDefaultLKP_Category();
                var lstCategoryConfig = GetDefaultLKP_CategoryConfig();
                var lstSubOptions = GetDefaultCategorySubOptions();
                var lstDeliveryOptions = GetDefaultCategoryDeliveryOptions();
                var lstCategoryType = GetDefaultLKP_CategoryType();

                using (var transaction = context.Database.BeginTransaction())
                {

                    await SeedEntityAsync(context, vat);
                    await SeedEntityAsync(context, lstLKP_Item);
                    await SeedEntityAsync(context, currencies, false);
                    context.Database.ExecuteSqlCommand("SET IDENTITY_INSERT LKP_Currency ON");
                    context.SaveChanges();
                    context.Database.ExecuteSqlCommand("SET IDENTITY_INSERT LKP_Currency OFF");

                    await SeedEntityAsync(context, lstLKP_Category);
                    await SeedEntityAsync(context, lstCategoryConfig,false);
                    context.Database.ExecuteSqlCommand("SET IDENTITY_INSERT LKP_CategoryConfig ON");
                    context.SaveChanges();
                    context.Database.ExecuteSqlCommand("SET IDENTITY_INSERT LKP_CategoryConfig OFF");
                    await SeedEntityAsync(context, lstSubOptions);
                    await SeedEntityAsync(context, lstDeliveryOptions);
                    await SeedEntityAsync(context, lstCategoryType,false);
                    context.Database.ExecuteSqlCommand("SET IDENTITY_INSERT LKP_CategoryType ON");
                    context.SaveChanges();
                    context.Database.ExecuteSqlCommand("SET IDENTITY_INSERT LKP_CategoryType OFF");

                    transaction.Commit();
                }
            }
            catch (Exception ex)
            {
                if (retryForAvaiability < 3)
                {
                    retryForAvaiability++;

                    logger.LogError(ex.Message, $"There is an error migrating data for CustomerDbContextSeed");

                    await SeedAsync(context, env, logger, retryForAvaiability);
                }
            }
        }
        private List<LKP_Item> GetDefaultLKP_Items()
        {
            List<LKP_Item> result = new List<LKP_Item>();
            //result.Add(new LKP_Item() { NameEN = "Order", NameAR = "أوردر" });
            //result.Add(new LKP_Item() { NameEN = "Application", NameAR = "ابلاكاشن" });
            return result;
        }
        private List<LKP_Currency> GetDefaultCurrencies()
        {
            List<LKP_Currency> result = new List<LKP_Currency>();

            if (settings == null)
            {
                result.Add(new LKP_Currency() { Id = 1, NameEN = "KWD", NameAR = "دينار كويتى" });
                result.Add(new LKP_Currency() { Id = 2, NameEN = "AED", NameAR = "درهم اماراتى" });
            }
            else
            {
                result.Add(new LKP_Currency() { Id = 1, NameEN = settings.KWCurrencyEN, NameAR = settings.KWCurrencyAR });
                result.Add(new LKP_Currency() { Id = 2, NameEN = settings.UAECurrencyEN, NameAR = settings.UAECurrencyAR });
            }
            return result;
        }
        private List<LKP_VAT> GetDefaultVAT()
        {
            List<LKP_VAT> result = new List<LKP_VAT>();

            if (settings == null)
            {
                result.Add(new LKP_VAT() { Id = 1, NameEN = "Default", NameAR = "الافتراضى",Percentage=20 });
            }
            else
            {
                result.Add(new LKP_VAT() { Id = 1, NameEN = settings.VatEN, NameAR = settings.VatAR,Percentage= settings.VatPercentage});
            }
            return result;
        }
        private List<LKP_Category> GetDefaultLKP_Category()
        {
            List<LKP_Category> result = new List<LKP_Category>
            {
                new LKP_Category() {FK_CategoryConfig_Id=1, FK_CategoryType_Id =(int)SystemCategoryType.Individual,NameAR="المخزن المباشر",NameEN = "Live Stock",AcceptMulti=true},
                new LKP_Category() {FK_CategoryConfig_Id=2, FK_CategoryType_Id =(int)SystemCategoryType.Individual,NameAR="Adahi",NameEN = "أضاحى",AcceptMulti=false},
                new LKP_Category() {FK_CategoryConfig_Id=3, FK_CategoryType_Id =(int)SystemCategoryType.Individual,NameAR="أعمال خيرية",NameEN = "Charity",AcceptMulti=false},
                new LKP_Category() {FK_CategoryConfig_Id=4, FK_CategoryType_Id =(int)SystemCategoryType.Individual,NameAR="أخرى",NameEN = "Otherts",AcceptMulti=true }
            };

            return result;
        }
        private List<LKP_CategoryType> GetDefaultLKP_CategoryType()
        {
            List<LKP_CategoryType> result = new List<LKP_CategoryType>
            {
                new LKP_CategoryType() {Id=(int)SystemCategoryType.Individual, NameEN=EnumManager<SystemCategoryType>.GetName(SystemCategoryType.Individual),NameAR =EnumManager<SystemCategoryType>.GetDescription(SystemCategoryType.Individual),IsSystemKey=true},
                new LKP_CategoryType() {Id=(int)SystemCategoryType.Sales, NameEN=EnumManager<SystemCategoryType>.GetName(SystemCategoryType.Sales),NameAR =EnumManager<SystemCategoryType>.GetDescription(SystemCategoryType.Sales),IsSystemKey=true}
            };

            return result;
        }
        private List<LKP_CategoryConfig> GetDefaultLKP_CategoryConfig()
        {
            List<LKP_CategoryConfig> result = new List<LKP_CategoryConfig>
            {
                new LKP_CategoryConfig() {Id=1, NameAR="المخزن المباشر",NameEN = "Live Stock",AcceptMulti=true, HasOptions=true },
                new LKP_CategoryConfig() {Id=2, NameAR="أضاحى",NameEN = "Adahi",AcceptMulti=false, HasOptions=false },
                new LKP_CategoryConfig() {Id=3, NameAR="أعمال خيرية",NameEN = "Charity",AcceptMulti=false, HasOptions=true },
                new LKP_CategoryConfig() {Id=4, NameAR="أخرى",NameEN = "Otherts",AcceptMulti=true, HasOptions=false },
            };

            return result;
        }
        private List<CategorySubOptions> GetDefaultCategorySubOptions()
        {
            List<CategorySubOptions> result = new List<CategorySubOptions>
            {
                new CategorySubOptions() { FK_CategoryConfig_Id=1, NameAR ="عادى",NameEN = "Regular" },
                new CategorySubOptions() { FK_CategoryConfig_Id=1, NameAR="عقيقة",NameEN = "Akika"},
            };

            return result;
        }
        private List<CategoryDeliveryOptions> GetDefaultCategoryDeliveryOptions()
        {
            List<CategoryDeliveryOptions> result = new List<CategoryDeliveryOptions>
            {
                //for live stock
                new CategoryDeliveryOptions() {FK_CategoryConfig_Id=1, DeliveryType=DeliveryType.Home,DateTypes = DateTypes.CustomerDate },
                //for  charity

                new CategoryDeliveryOptions() {FK_CategoryConfig_Id=3, DeliveryType=DeliveryType.Home,DateTypes = DateTypes.CustomerDate },
                new CategoryDeliveryOptions() {FK_CategoryConfig_Id=3, DeliveryType=DeliveryType.Donation,DateTypes = DateTypes.DonationDates },
                new CategoryDeliveryOptions() {FK_CategoryConfig_Id=3, DeliveryType=DeliveryType.PickUp,DateTypes = DateTypes.PickUpDates },

                //for  Adahi

                new CategoryDeliveryOptions() { FK_CategoryConfig_Id=2,DeliveryType=DeliveryType.Home,DateTypes = DateTypes.CustomerDate },
                new CategoryDeliveryOptions() { FK_CategoryConfig_Id=2,DeliveryType=DeliveryType.Donation,DateTypes = DateTypes.PickUpDates },
                new CategoryDeliveryOptions() { FK_CategoryConfig_Id=2,DeliveryType=DeliveryType.Donation,DateTypes = DateTypes.EidDates },
                new CategoryDeliveryOptions() { FK_CategoryConfig_Id=2,DeliveryType=DeliveryType.PickUp,DateTypes = DateTypes.PickUpDates },
                //for others
                new CategoryDeliveryOptions() {FK_CategoryConfig_Id=4, DeliveryType=DeliveryType.Home,DateTypes = DateTypes.CustomerDate },

            };

            return result;
        }
    }
}
