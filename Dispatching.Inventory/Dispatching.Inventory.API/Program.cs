﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Dispatching.Inventory.Models.Context;
using DispatchProduct.Inventory.API.Seed;
using Microsoft.Extensions.DependencyInjection;
using Serilog;
using Dispatching.Inventory.Models.Settings;
using Microsoft.Extensions.Options;
using Utilities.Utilites.SerilogExtensions;

namespace Dispatching.Inventory.API
{
    public class Program
    {
        public static void Main(string[] args)
        {
            BuildWebHost(args).MigrateDbContext<InventoryDbContext>((context, services) =>
            {
                var env = services.GetService<IHostingEnvironment>();
                var logger = services.GetService<ILogger<InventoryDbContextSeed>>();
                //var settings = services.GetService<IOptions<InventoryAppSettings>>();
                new InventoryDbContextSeed()
                    .SeedAsync(context, env, logger)
                    .Wait();
            }).Run();
        }

        public static IWebHost BuildWebHost(string[] args) =>
            WebHost.CreateDefaultBuilder(args)
                .UseKestrel()
                .UseContentRoot(Directory.GetCurrentDirectory())
                .UseIISIntegration()
                .UseStartup<Startup>()
               // .UseSetting("detailedErrors", "true")
                 .ConfigureLogging((hostingContext, builder) =>
                 {
                     builder.AddConfiguration(hostingContext.Configuration.GetSection("Logging"));
                     builder.AddConsole();
                     builder.AddDebug();
                 }).Build();
    }
}
