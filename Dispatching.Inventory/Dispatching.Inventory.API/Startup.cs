﻿using Autofac;
using Autofac.Extensions.DependencyInjection;
using AutoMapper;
using BuildingBlocks.ServiceDiscovery;
using Dispatching.Inventory.API.Controllers;
using Dispatching.Inventory.API.ScheduleTasks;
using Dispatching.Inventory.BLL.CommonModels;
using Dispatching.Inventory.BLL.IManagers;
using Dispatching.Inventory.BLL.Managers;
using Dispatching.Inventory.Models.Context;
using Dispatching.Inventory.Models.Entities;
using Dispatching.Inventory.Models.Settings;
using DispatchProduct.Inventory.Entities;
using DispatchProduct.RepositoryModule;
using DnsClient;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.FileProviders;
using Newtonsoft.Json.Serialization;
using Swashbuckle.AspNetCore.Swagger;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Linq.Expressions;
using System.Net;
using System.Net.Sockets;
using Utilites.PaginatedItems;
using Utilites.ProcessingResult;
using Utilites.UploadFile;

namespace Dispatching.Inventory.API
{
    public class Startup
    {
        public IHostingEnvironment _env;
        public IConfigurationRoot Configuration { get; }
        public Startup(IHostingEnvironment env)
        {
            _env = env;
            var builder = new ConfigurationBuilder()
                .SetBasePath(env.ContentRootPath)
                .AddEnvironmentVariables();
            Configuration = builder.Build();

            var country = Configuration.GetSection("DockerContainerName").Value.Split('.').Last().ToLower();
            //var country = "uae";
            builder = new ConfigurationBuilder()
                .SetBasePath(env.ContentRootPath)
                .AddJsonFile($"appsettings.{country}.json", optional: false, reloadOnChange: true)
                .AddEnvironmentVariables();

            Configuration = builder.Build();
        }

        // This method gets called by the runtime. Use this method to add services to the container.
        public IServiceProvider ConfigureServices(IServiceCollection services)
        {
            services.AddCors(options =>
            {
                options.AddPolicy("AllowAll",
                    builder =>
                    {
                        builder
                        .AllowAnyOrigin()
                        .AllowAnyMethod()
                        .AllowAnyHeader()
                        .AllowCredentials();
                    });
            });
            services.AddSingleton(provider => Configuration);

            services.AddMvc().AddJsonOptions(options =>
            {
                options.SerializerSettings.ContractResolver = new CamelCasePropertyNamesContractResolver();
            });

            string consulContainerName = Configuration.GetSection("ServiceRegisteryContainerName").Value;
            int consulPort = Convert.ToInt32(Configuration.GetSection("ServiceRegisteryPort").Value);
            IPAddress ip = Dns.GetHostEntry(consulContainerName).AddressList.Where(o => o.AddressFamily == AddressFamily.InterNetwork).First();
            services.AddSingleton<IDnsQuery>(new LookupClient(ip, consulPort));
            services.AddServiceDiscovery(Configuration.GetSection("ServiceDiscovery"));
            services.AddApiVersioning(o => o.ReportApiVersions = true);

            services.AddDbContext<InventoryDbContext>(options =>
            options.UseSqlServer(Configuration["ConnectionString"],
            sqlOptions => sqlOptions.MigrationsAssembly("Dispatching.Inventory.EFCore.MSSQL")));
            services.AddOptions();
            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1",
                    new Info()
                    {
                        Title = "Calling API",
                        Description = "Calling  API"
                    });
                c.AddSecurityDefinition("oauth2", new OAuth2Scheme
                {
                    Type = "oauth2",
                    Flow = "implicit",
                    AuthorizationUrl = $"{Configuration.GetValue<string>("IdentityUrlExternal")}/connect/authorize",
                    TokenUrl = $"{Configuration.GetValue<string>("IdentityUrlExternal")}/connect/token",
                    Scopes = new Dictionary<string, string>()
                    {
                        { "calling", "calling API" }
                    }
                });
            });


            services.AddMvc();
            services.AddAutoMapper(typeof(Startup));

            services.Configure<InventoryAppSettings>(Configuration);
            services.Configure<ServiceDisvoveryOptions>(Configuration.GetSection("ServiceDiscovery"));
            services.AddScoped<DbContext, InventoryDbContext>();
            services.AddScoped(typeof(IRepository<>), typeof(Repository<>));
            services.AddScoped(typeof(ILKP_Item), typeof(LKP_Item));
            services.AddTransient(typeof(IAllowable_Items_Cutting), typeof(Allowable_Items_Cutting));
            services.AddTransient(typeof(IAllowable_Items_Quatity), typeof(Allowable_Items_Quatity));
            services.AddScoped(typeof(ILKP_Category), typeof(LKP_Category));
            services.AddScoped(typeof(LKP_Category), typeof(LKP_Category));
            services.AddScoped(typeof(Expression<>), typeof(Expression<>));
            services.AddScoped(typeof(Func<>), typeof(Func<>));
            services.AddScoped(typeof(ILKP_Cutting), typeof(LKP_Cutting));
            services.AddTransient(typeof(ILKP_Quantity), typeof(LKP_Quantity));
            services.AddScoped(typeof(ILKP_CategoryType), typeof(LKP_CategoryType));
            services.AddScoped(typeof(ILKP_VAT), typeof(LKP_VAT));
            services.AddScoped(typeof(IDiscount), typeof(Discount));
            services.AddScoped(typeof(ILKP_Currency), typeof(LKP_Currency));
            services.AddScoped(typeof(ICategoryDeliveryOptions), typeof(CategoryDeliveryOptions));
            services.AddScoped(typeof(ICategorySubOptions), typeof(CategorySubOptions));
            services.AddScoped(typeof(ILKP_CategoryConfig), typeof(LKP_CategoryConfig));

            services.AddScoped(typeof(IItemDetailsModel), typeof(ItemDetailsModel));
            services.AddScoped(typeof(ItemDetailsModel), typeof(ItemDetailsModel));

            services.AddScoped(typeof(ILKP_ItemManager), typeof(LKP_ItemManager));
            services.AddScoped(typeof(IAllowable_Items_CuttingManager), typeof(Allowable_Items_CuttingManager));
            services.AddScoped(typeof(IAllowable_Items_QuatityManager), typeof(Allowable_Items_QuatityManager));
            services.AddScoped(typeof(ILKP_CategoryManager), typeof(LKP_CategoryManager));
            services.AddScoped(typeof(ILKP_CuttingManager), typeof(LKP_CuttingManager));
            services.AddScoped(typeof(ILKP_QuantityManager), typeof(LKP_QuantityManager));
            services.AddScoped(typeof(ILKP_CategoryTypeManager), typeof(LKP_CategoryTypeManager));
            services.AddScoped(typeof(IUploadImageFileManager), typeof(UploadImageFileManager));
            services.AddScoped(typeof(ILKP_VATManager), typeof(LKP_VATManager));
            services.AddScoped(typeof(IDiscountManager), typeof(DiscountManager));
            services.AddScoped(typeof(IUploadFileManager), typeof(UploadFileManager));
            services.AddScoped(typeof(ILKP_CurrencyManager), typeof(LKP_CurrencyManager));
            services.AddScoped(typeof(LKP_CurrencyController), typeof(LKP_CurrencyController));
            services.AddScoped(typeof(LKP_CategoryController), typeof(LKP_CategoryController));
            services.AddScoped(typeof(ICategoryDeliveryOptionsManager), typeof(CategoryDeliveryOptionsManager));
            services.AddScoped(typeof(ICategorySubOptionsManager), typeof(CategorySubOptionsManager));
            services.AddScoped(typeof(ILKP_CategoryConfigManager), typeof(LKP_CategoryConfigManager));
            services.AddScoped(typeof(IAvailableScheduleTask), typeof(AvailableScheduleTask));


            services.AddScoped(typeof(IProcessResultMapper), typeof(ProcessResultMapper));
            services.AddScoped(typeof(IProcessResultPaginatedMapper), typeof(ProcessResultPaginatedMapper));
            services.AddScoped(typeof(IPaginatedItems<>), typeof(PaginatedItems<>));
            Mapper.AssertConfigurationIsValid();
            var container = new ContainerBuilder();
            container.Populate(services);

            var serviceProvider = new AutofacServiceProvider(container.Build());
            return serviceProvider;//new AutofacServiceProvider(container.Build());
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            app.UseCors("AllowAll");
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
                app.UseDatabaseErrorPage();
            }
            else
            {
                app.UseExceptionHandler("/Home/Error");
            }
            app.UseStaticFiles();
            app.UseFileServer(new FileServerOptions
            {
                FileProvider = new PhysicalFileProvider(
                 Path.Combine(Directory.GetCurrentDirectory(), "wwwroot", "ItemImages")),
                RequestPath = "/ItemImages",
                EnableDirectoryBrowsing = true
            });
            app.UseFileServer(new FileServerOptions
            {
                FileProvider = new PhysicalFileProvider(
                Path.Combine(Directory.GetCurrentDirectory(), "wwwroot", "CategoryImages")),
                RequestPath = "/CategoryImages",
                EnableDirectoryBrowsing = true
            });
            app.UseSwagger();
            app.UseSwaggerUI(c =>
            {
                c.SwaggerEndpoint("/swagger/v1/swagger.json", "Reviewing API");
            });

            app.UseMvc();
            app.UseConsulRegisterService();
        }
    }
}
