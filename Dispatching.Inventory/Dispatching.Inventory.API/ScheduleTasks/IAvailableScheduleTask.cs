﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Utilites.ProcessingResult;

namespace Dispatching.Inventory.API.ScheduleTasks
{
    public interface IAvailableScheduleTask
    {
        ProcessResult<bool> CheckItemActiveDueDate();
        ProcessResult<bool> CheckCategoryActiveDueDate();
    }
}
