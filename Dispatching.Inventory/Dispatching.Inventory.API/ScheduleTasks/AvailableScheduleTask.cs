﻿using Dispatching.Inventory.BLL.IManagers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Utilites.ProcessingResult;

namespace Dispatching.Inventory.API.ScheduleTasks
{
    public class AvailableScheduleTask : IAvailableScheduleTask
    {
        ILKP_ItemManager itemManager;
        ILKP_CategoryManager catManger;
        public AvailableScheduleTask(ILKP_ItemManager _itemManager, ILKP_CategoryManager _catManger)
        {
            catManger = _catManger;
            itemManager = _itemManager;
        }
        public ProcessResult<bool> CheckItemActiveDueDate()
        {
            return itemManager.CheckAvailableIems();
        }
        public ProcessResult<bool> CheckCategoryActiveDueDate()
        {
            return catManger.CheckAvailableCategories();
        }
    }
}
