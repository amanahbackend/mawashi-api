﻿namespace BuisnessCommon.Enums
{
    public enum SortType
    {
        Ascending = 1,
        Descending = 2
    }
}
