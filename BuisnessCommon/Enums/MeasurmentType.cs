﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Dispatching.BuisnessCommon.Enums
{
    public enum MeasurmentType
    {
        Head=1,
        Kgs=2,
        Pieces=3,
        Box=4
    }
}
