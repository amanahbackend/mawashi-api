﻿using AutoMapper;
using Dispatching.Location.API.ViewModels;
using MakaniLocationService;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Dispatching.Location.API.Controllers
{
    [ApiVersion("1.0")]
    [Route("v{version:apiVersion}/[controller]")]
    public class MakaniController : Controller
    {
        public MakaniController()
        {
        }

        [HttpGet("GetMakaniInfoFromCoord"), MapToApiVersion("1.0")]
        public async Task<IActionResult> GetMakaniInfoFromCoord([FromQuery] double lat, double lng)
        {
            var result = await MakaniHelper.GetMakaniInfoByCoord(lat, lng);
            var locationModel = Mapper.Map<MakaniModel, LocationViewModel>(result);
            return Ok(locationModel);
        }

        [HttpGet("GetMakaniInfoFromNumber"), MapToApiVersion("1.0")]
        public async Task<IActionResult> GetMakaniInfoFromNumber([FromQuery] string makaniNumber)
        {
            var result = await MakaniHelper.GetMakaniInfoByNumber(makaniNumber);
            var locationModel = Mapper.Map<MakaniModel, LocationViewModel>(result);
            return Ok(locationModel);
        }

    }
}
