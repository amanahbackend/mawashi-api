﻿using AutoMapper;
using Dispatching.Location.API.ViewModels;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Utilites.PACI;
using Utilites.ProcessingResult;
using Utilities.Utilites.PACI;
using static Utilites.PACI.PACIHelper;

namespace Dispatching.Location.API.Controllers
{
    [ApiVersion("1.0")]
    [Route("v{version:apiVersion}/[controller]")]
    public class PaciController : Controller
    {
        private IConfigurationRoot _configurationRoot;
        public PaciController(IConfigurationRoot configurationRoot)
        {
            _configurationRoot = configurationRoot;
        }

        [HttpGet]
        [Route("GetLocationByPaci"), MapToApiVersion("1.0")]
        public IActionResult GetLocationByPaci(string paciNumber)
        {
            string proxyUrl = _configurationRoot["Location:ProxyUrl"];
            string paciServiceUrl = _configurationRoot["Location:PACIService:ServiceUrl"];
            string paciFieldName = _configurationRoot["Location:PACIService:PACIFieldName"];
            string blockServiceUrl = _configurationRoot["Location:BlockService:ServiceUrl"];
            string streetServiceUrl = _configurationRoot["Location:StreetService:ServiceUrl"];
            string blockNameFieldNameBlockService = _configurationRoot["Location:BlockService:BlockNameFieldName"];
            string blockNameFieldNameStreetService = _configurationRoot["Location:StreetService:BlockNameFieldName"];
            string streetFieldName = _configurationRoot["Location:StreetService:StreetFieldName"];
            string nhoodNameFieldName = _configurationRoot["Location:BlockService:AreaNameFieldName"];

            //var result = PACIHelper.GetLocationByPACI(Convert.ToInt32(paciNumber), proxyUrl, paciServiceUrl,
            //                            paciFieldName, blockServiceUrl, blockNameFieldNameBlockService,
            //                            nhoodNameFieldName, streetServiceUrl, blockNameFieldNameStreetService,
            //                            streetFieldName);
            var result = new PACIInfo();
            var locationModel = Mapper.Map<PACIInfo, LocationViewModel>(result);
            return Ok(locationModel);
        }

        [HttpGet]
        [Route("GetAllGovernorates"), MapToApiVersion("1.0")]
        public IActionResult GetAllGovernorates()
        {
            string proxyUrl = _configurationRoot["Location:ProxyUrl"];
            string serviceUrl = _configurationRoot["Location:GovernorateService:ServiceUrl"];

            var result = PACIHelper.GetAllGovernorates(proxyUrl, serviceUrl);
            return Ok(result);
        }

        [HttpGet]
        [Route("GetAreas"), MapToApiVersion("1.0")]
        public IActionResult GetAreas(int? govId)
        {
            string proxyUrl = _configurationRoot["Location:ProxyUrl"];
            string serviceUrl = _configurationRoot["Location:AreaService:ServiceUrl"];
            string govIdfieldName = _configurationRoot["Location:AreaService:GovernorateIdFieldName"];
            var result = PACIHelper.GetAreas(govId, govIdfieldName, proxyUrl, serviceUrl);
            return Ok(result);
        }

        [HttpGet]
        [Route("GetBlocks"), MapToApiVersion("1.0")]
        public IActionResult GetBlocks(int? areaId)
        {
            string proxyUrl = _configurationRoot["Location:ProxyUrl"];
            string serviceUrl = _configurationRoot["Location:BlockService:ServiceUrl"];
            string areaIdfieldName = _configurationRoot["Location:BlockService:AreaIdFieldName"];
            var result = PACIHelper.GetBlocks(areaId, areaIdfieldName, proxyUrl, serviceUrl);
            return Ok(result);
        }

        [HttpGet]
        [Route("GetStreets"), MapToApiVersion("1.0")]
        public IActionResult GetStreets(int? govId, int? areaId, string blockName)
        {
            string proxyUrl = _configurationRoot["Location:ProxyUrl"];
            string serviceUrl = _configurationRoot["Location:StreetService:ServiceUrl"];
            string govIdFieldName = _configurationRoot["Location:StreetService:GovIdFieldName"];
            string areaIdFieldName = _configurationRoot["Location:StreetService:AreaIdFieldName"];
            string blockNameFieldName = _configurationRoot["Location:StreetService:BlockNameFieldName"];
            var result = PACIHelper.GetStreets(govId, govIdFieldName, areaId, areaIdFieldName,
                                                blockName, blockNameFieldName, proxyUrl, serviceUrl);
            return Ok(result);
        }

        [HttpGet("GetStreetCoors"), MapToApiVersion("1.0")]
        public ProcessResultViewModel<Point> GetStreetCoors([FromQuery]string streetName, string blockName)
        {
            try
            {
                string proxyUrl = _configurationRoot["Location:ProxyUrl"];
                string serviceUrl = _configurationRoot["Location:StreetService:ServiceUrl"];
                string streetFieldName = _configurationRoot["Location:StreetService:StreetFieldName"];
                string blockNameFieldNameStreetService = _configurationRoot["Location:StreetService:BlockNameFieldName"];

                var result = PACIHelper.GetStreetPACItModel(streetName, blockName, proxyUrl,
                                            serviceUrl, streetFieldName, blockNameFieldNameStreetService);
                var point = new Point();
                if (result != null)
                {
                    point.Latitude = (double)result.features[0].attributes.CENTROID_Y;
                    point.Longitude = (double)result.features[0].attributes.CENTROID_X;
                }
                return ProcessResultViewModelHelper.Succedded(point);
            }
            catch (Exception ex)
            {
                return ProcessResultViewModelHelper.Failed<Point>(null, ex.Message);
            }
        }
    }
}
