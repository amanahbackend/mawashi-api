﻿using AutoMapper;
using Dispatching.Location.API.ViewModels;
using MakaniLocationService;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using static Utilites.PACI.PACIHelper;

namespace Dispatching.Location.API.AutoMapperConfig
{
    public class MappingProfile : Profile
    {
        public MappingProfile()
        {
            CreateMap<DropPACI, LocationItemViewModel>()
                .ForMember(src => src.Id, opt => opt.MapFrom(dest => dest.Id))
                .ForMember(src => src.Name, opt => opt.MapFrom(dest => dest.Name));


            CreateMap<PACIInfo, LocationViewModel>()
                .ForMember(src => src.Area, opt => opt.MapFrom(dest => dest.Area))
                .ForMember(src => src.Block, opt => opt.MapFrom(dest => dest.Block))
                .ForMember(src => src.Governorate, opt => opt.MapFrom(dest => dest.Governorate))
                .ForMember(src => src.Street, opt => opt.MapFrom(dest => dest.Street));
                //.ForMember(src => src.Lat, opt => opt.MapFrom(dest => dest.Lat))
                //.ForMember(src => src.Lng, opt => opt.MapFrom(dest => dest.Lng))
                //.ForMember(src => src.Number, opt => opt.MapFrom(dest => dest.PACINumber));

            CreateMap<MakaniModel, LocationViewModel>()
                .ForPath(src => src.Area.Id, opt => opt.Ignore())
                .ForPath(src => src.Block.Id, opt => opt.Ignore())
                .ForPath(src => src.Governorate.Id, opt => opt.Ignore())
                .ForPath(src => src.Street.Id, opt => opt.Ignore())
                .ForPath(src => src.Street.Name, opt => opt.Ignore())
                .ForPath(src => src.Area.Name, opt => opt.MapFrom(dest => dest.COMMUNITY_E))
                .ForPath(src => src.Governorate.Name, opt => opt.MapFrom(dest => dest.EMIRATE_E))
                .ForPath(src => src.Block.Name, opt => opt.MapFrom(dest => dest.BLDG_NAME_E))
                .ForMember(src => src.Lat, opt => opt.MapFrom(dest => dest.LAT))
                .ForMember(src => src.Lng, opt => opt.MapFrom(dest => dest.LNG))
                .ForMember(src => src.Number, opt => opt.MapFrom(dest => dest.MAKANI));




        }
    }
}
