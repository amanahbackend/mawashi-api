﻿using Dispatching.ApiGateway.API.ServiceCommunications.Identity;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Configuration;
using Microsoft.Net.Http.Headers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;

namespace Dispatching.ApiGateway.API.Middleware
{
    public class AuthorizationMiddleware
    {
        public readonly RequestDelegate _next;
        private readonly IConfigurationRoot _configuration;

        public AuthorizationMiddleware(RequestDelegate next, IConfigurationRoot configuration)
        {
            _configuration = configuration;
            _next = next;
        }

        public async Task Invoke(HttpContext httpContext, IIdentityUserService identityUserService, IConfigurationRoot config)
        {
            string requestedAction = httpContext.Request.Path.Value.ToLower();
            if (IsNoAuthResourceRequest(requestedAction, config))
            {
                await _next.Invoke(httpContext);
            }
            else
            {

                var urlSegments = requestedAction.Split('/');
                string service = urlSegments[1];
                string segment2 = urlSegments[2];

                if (segment2.ToLower().Equals("uae") || segment2.ToLower().Equals("kw"))
                {
                    string country = segment2;
                    string apiVersion = urlSegments[3];
                    requestedAction = requestedAction.Remove(0, service.Length + 1);
                    requestedAction = requestedAction.Remove(0, country.Length + 1);
                    requestedAction = requestedAction.Remove(0, apiVersion.Length + 1);
                }
                else
                {
                    string apiVersion = segment2;
                    requestedAction = requestedAction.Remove(0, service.Length + 1);
                    requestedAction = requestedAction.Remove(0, apiVersion.Length + 1);
                }


                if (IsNoAuthAction(requestedAction, config))
                {
                    await _next.Invoke(httpContext);
                }
                else
                {
                    string authorizationValue = httpContext.Request.Headers[HeaderNames.Authorization];
                    if (!String.IsNullOrEmpty(authorizationValue))
                    {
                        string token = authorizationValue.Substring("Bearer ".Length).Trim();
                        bool isValid = await identityUserService.IsTokenValid("v1", token);
                        if (isValid)
                        {
                            bool isUserHasPrivilege = await identityUserService.IsUserHavePrivelege("v1", token, requestedAction);

                            if (isUserHasPrivilege)
                            {
                                await _next.Invoke(httpContext);
                            }
                            else
                            {
                                // all users will access all actions .. seed privelege tables

                                await _next.Invoke(httpContext);
                                //httpContext.Response.StatusCode = (int)HttpStatusCode.Unauthorized;
                            }
                        }
                        else
                        {
                            httpContext.Response.StatusCode = (int)HttpStatusCode.Unauthorized;
                        }
                    }
                    else
                    {
                        httpContext.Response.StatusCode = (int)HttpStatusCode.Unauthorized;
                    }
                }
            }            
        }

        private bool IsNoAuthAction(string action, IConfigurationRoot config)
        {
            var noAuthActions = config.GetSection("NoAuthActions").Get<string[]>();
            bool result = false;
            foreach (var item in noAuthActions)
            {
                if (action.ToLower().Contains(item.ToLower()))
                {
                    result = true;
                    break;
                }
            }
            return result;
        }

        private bool IsNoAuthResourceRequest(string action, IConfigurationRoot config)
        {
            var noAuthFileExtensions = config.GetSection("NoAuthFileExtensions").Get<string[]>();
            var segments = action.Split('/').Last().Split('.');
            if (segments.Count() > 1)
            {
                var extension = segments.Last();
                var result = noAuthFileExtensions.Contains(extension.ToLower());
                return result;
            }
            return false;
        }
    }
}
