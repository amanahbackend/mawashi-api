﻿using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Options;
using Dispatching.ApiGateway.API.Proxy;
using DnsClient;
using System.Net;
using BuildingBlocks.ServiceDiscovery;
using Microsoft.AspNetCore.Hosting;

namespace Dispatching.ApiGateway.API.Middleware
{
    public class RoutingMiddleware
    {
        public readonly RequestDelegate _next;
        public IHostingEnvironment _hostingEnv;

        public RoutingMiddleware(RequestDelegate next, IHostingEnvironment hostingEnv)
        {
            _hostingEnv = hostingEnv;
            _next = next;
        }

        public async Task Invoke(HttpContext httpContext, IDnsQuery dnsQuery)
        {
            string requestedAction = httpContext.Request.Path.Value;

            LogResult($"{DateTime.UtcNow.ToString("")} : {requestedAction}");
            LogResult("=========================================================");

            if (requestedAction.ToLower().Contains("apigateway"))
            {
                await _next.Invoke(httpContext);
            }
            if (requestedAction.ToLower().Contains("websurge-allow.txt"))
            {
                await _next.Invoke(httpContext);
            }
            else
            {
                var urlSegments = requestedAction.Split('/');
                string service = urlSegments[1];
                string segment2 = urlSegments[2].ToLower();

                string basePath = "";

                if (segment2.Equals("uae") || segment2.Equals("kw"))
                {
                    if (!service.ToLower().Equals("contentapi"))
                    {
                        string country = segment2.ToLower();
                        basePath = await ServiceDiscoveryHelper.GetServiceUrl(dnsQuery, $"{service}_{country}");
                    }
                    else
                    {
                        basePath = await ServiceDiscoveryHelper.GetServiceUrl(dnsQuery, service);
                    }
                }
                else
                {
                    basePath = await ServiceDiscoveryHelper.GetServiceUrl(dnsQuery, service);
                }


                if (String.IsNullOrEmpty(basePath))
                {
                    httpContext.Response.StatusCode = (int)HttpStatusCode.NotFound;
                }
                else
                {
                    var queryString = httpContext.Request.QueryString.Value;
                    requestedAction = requestedAction.Remove(0, service.Length + 1);
                    if (segment2.Equals("uae") || segment2.Equals("kw"))
                    {
                        if (!service.ToLower().Equals("contentapi"))
                        {
                            string country = segment2;
                            requestedAction = requestedAction.Remove(0, country.Length + 1);
                        }
                    }
                    string url = $"{basePath}{requestedAction}{queryString}";

                    await httpContext.ProxyRequest(new Uri(url));
                }
            }
        }

        public void LogResult(string resultMessage)
        {
            string date = DateTime.Now.Day + "_" + DateTime.Now.Month + "_" + DateTime.Now.Year;
            //string dir = @"D:\Amanah\Dev\Migration"; 
            string dir = $"{_hostingEnv.WebRootPath}/Logs";

            DirectoryInfo dirInfo = new DirectoryInfo(dir);
            if (!dirInfo.Exists)
            {
                dirInfo.Create();
            }
            string path = dir + "/" + "Request_Log_" + date + ".txt";
            if (!File.Exists(path))
            {
                // Create a file to write to.
                using (StreamWriter sw = File.CreateText(path))
                {
                    sw.WriteLine(resultMessage);
                }
            }
            else
            {
                using (StreamWriter sw = File.AppendText(path))
                {
                    sw.WriteLine(resultMessage);
                }
            }
        }
    }

}
