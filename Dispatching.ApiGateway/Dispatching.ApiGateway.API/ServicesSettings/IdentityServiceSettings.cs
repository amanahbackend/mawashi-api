﻿using BuildingBlocks.ServiceDiscovery;
using DispatchProduct.HttpClient;
using DnsClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Dispatching.ApiGateway.API.ServicesSettings
{
    public class IdentityServiceSettings : DefaultHttpClientSettings
    {
        private new string Uri { get => base.Uri; set => base.Uri = value; }

        public string IsUserHasPrivilegeAction { get; set; }
        public string IsTokenValidAction { get; set; }

        //public async Task<string> GetBaseUrl(IDnsQuery dnsQuery)
        //{
        //    return await ServiceDiscoveryHelper.GetServiceUrl(dnsQuery, ServiceName);
        //}


    }
}
