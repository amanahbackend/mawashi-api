﻿using CommonEnums;
using DispatchProduct.BuildingBlocks.BaseEntities.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Dispatching.ApiGateway.API.ServicesViewModels
{
    public class ApplicationUserViewModel : IdentityBaseEntity
    {
        public virtual string Id { get; set; }
        public string UserName { get; set; }
        public string Email { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Phone1 { get; set; }
        public string Phone2 { get; set; }
        public string PicturePath { get; set; }
        public List<string> RoleNames { get; set; }
        public string Picture { get; set; }
        public string PictureName { get; set; }
        //[Required]
        //[StringLength(100, ErrorMessage = "The {0} must be at least {2} and at max {1} characters long.", MinimumLength = 6)]
        //[DataType(DataType.Password)]
        //[Display(Name = "Password")]
        public string Password { get; set; }
        public Country Fk_Country_Id { get; set; }
        public SupportedLanguage Language { get; set; }
    }
}
