﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Dispatching.ApiGateway.API.ServiceCommunications.Identity
{
    public interface IIdentityUserService
    {
        Task<bool> IsTokenValid(string version, string token);
        Task<bool> IsUserHavePrivelege(string version, string token, string privelege);
    }
}
