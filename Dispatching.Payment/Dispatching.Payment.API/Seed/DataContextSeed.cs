﻿using Dispatching.Payment.BLL.IManagers;
using Microsoft.Extensions.Configuration;
using Serilog;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Dispatching.Payment.API.Seed
{
    public class DataContextSeed
    {
        private IPaymentMethodManager _paymentMethodManager;
        private IConfigurationRoot _configuration;
        public DataContextSeed(IPaymentMethodManager paymentMethodManager, IConfigurationRoot configuration)
        {
            _configuration = configuration;
            _paymentMethodManager = paymentMethodManager;
        }

        public void Seed(int? retry = 0)
        {
            int retryForAvaiability = retry.Value;
            try
            {
                var country = _configuration.GetSection("DockerContainerName").Value.Split('.').Last().ToLower();
                if (country.ToLower().Equals("kw"))
                {
                    _paymentMethodManager.SeedKw();
                }
                else if (country.ToLower().Equals("uae"))
                {
                    _paymentMethodManager.SeedUae();
                }
            }
            catch (Exception ex)
            {
                if (retryForAvaiability < 10)
                {
                    retryForAvaiability++;

                    Log.Error(ex, "There is an error migrating data for Customer DataContext. Exception Message");

                    Seed(retryForAvaiability);
                }
            }
        }
    }
}
