﻿using Dispatching.Payment.API.ServicesSettings;
using Dispatching.Payment.API.ViewModels;
using DispatchProduct.HttpClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Dispatching.Payment.API.ServiceCommunications.Ordering
{
    public interface IOrderService
        : IDefaultHttpClientCrud<OrderServiceSettings, PaymentInfoViewModel, PaymentInfoViewModel>
    {
        Task UpdateOrderPayment(string version, PaymentInfoViewModel model);
    }
}
