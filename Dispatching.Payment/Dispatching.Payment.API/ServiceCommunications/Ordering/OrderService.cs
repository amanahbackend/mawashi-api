﻿using Dispatching.Payment.API.ServicesSettings;
using Dispatching.Payment.API.ViewModels;
using DispatchProduct.HttpClient;
using DnsClient;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Dispatching.Payment.API.ServiceCommunications.Ordering
{
    public class OrderService :
        DefaultHttpClientCrud<OrderServiceSettings, PaymentInfoViewModel, PaymentInfoViewModel>,
        IOrderService
    {
        OrderServiceSettings _settings;
        IDnsQuery _dnsQuery;
        public OrderService(IOptions<OrderServiceSettings> obj, IDnsQuery dnsQuery) : base(obj.Value, dnsQuery)
        {
            _dnsQuery = dnsQuery;
            _settings = obj.Value;
        }

        public async Task UpdateOrderPayment(string version, PaymentInfoViewModel model)
        {
            string baseUrl = await _settings.GetBaseUrl(_dnsQuery);
            while (baseUrl.EndsWith('/'))
            {
                baseUrl = baseUrl.Substring(0, baseUrl.Length - 1);
            }
            if (!String.IsNullOrEmpty(baseUrl))
            {
                string requestedAction = _settings.UpdateOrderPaymentAction;
                var url = $"{baseUrl}/{version}/{requestedAction}";
                await Post(url, model);
            }
        }
    }
}
