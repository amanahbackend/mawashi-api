﻿using Microsoft.AspNetCore.Hosting;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dispatching.Payment.API.Utilities
{
    public class LoggingManager
    {
        IHostingEnvironment _hostingEnv;
        public LoggingManager(IHostingEnvironment hostingEnv)
        {
            _hostingEnv = hostingEnv;
        }

        public void LogResult(string resultMessage)
        {
            string date = DateTime.Now.Day + "_" + DateTime.Now.Month + "_" + DateTime.Now.Year;
            //string dir = @"D:\Amanah\Dev\Migration";
            string dir = $"{_hostingEnv.WebRootPath}/Logs";

            DirectoryInfo dirInfo = new DirectoryInfo(dir);
            if (!dirInfo.Exists)
            {
                dirInfo.Create();
            }
            string path = dir + "/" + "Result_Log_" + date + ".txt";
            if (!File.Exists(path))
            {
                // Create a file to write to.
                using (StreamWriter sw = File.CreateText(path))
                {
                    sw.WriteLine(resultMessage);
                }
            }
            else
            {
                using (StreamWriter sw = File.AppendText(path))
                {
                    sw.WriteLine(resultMessage);
                }
            }
        }

        public void LogError(string errorMessage, Exception exception, string methodName)
        {
            string date = DateTime.Now.Day + "_" + DateTime.Now.Month + "_" + DateTime.Now.Year;
            string dir = $"{_hostingEnv.WebRootPath}/Logs";

            DirectoryInfo dirInfo = new DirectoryInfo(dir);
            if (!dirInfo.Exists)
            {
                dirInfo.Create();
            }
            string path = dir + "/" + "Error_Log_" + date + ".txt";
            if (!File.Exists(path))
            {
                // Create a file to write to.
                using (StreamWriter sw = File.CreateText(path))
                {
                    sw.WriteLine(errorMessage);
                    sw.WriteLine(FlattenException(exception, methodName));
                }
            }
            else
            {
                using (StreamWriter sw = File.AppendText(path))
                {
                    sw.WriteLine(errorMessage);
                    sw.WriteLine(FlattenException(exception, methodName));
                }
            }
        }

        private string FlattenException(Exception exception, string Method)
        {
            string logFormat;
            logFormat = DateTime.Now.ToShortDateString().ToString() + " " + DateTime.Now.ToLongTimeString().ToString();
            var stringBuilder = new StringBuilder();
            stringBuilder.AppendLine(logFormat);
            stringBuilder.AppendLine("********************************");

            var st = new StackTrace(exception, true);
            var frame = st.GetFrame(0);
            stringBuilder.AppendLine("Line Number: " + frame.GetFileLineNumber());
            stringBuilder.AppendLine("File Name: " + frame.GetFileName());
            stringBuilder.AppendLine("Method Name: " + frame.GetMethod());
            stringBuilder.AppendLine(Method);

            while (exception != null)
            {
                stringBuilder.AppendLine("-------------------------------------");
                stringBuilder.AppendLine("\n Message=" + exception.Message);
                stringBuilder.AppendLine("\n StackTrace=" + exception.StackTrace);
                stringBuilder.AppendLine("\n Exception=" + exception.ToString());
                stringBuilder.AppendLine("----------------------------------------------------------");
                exception = exception.InnerException;
            }
            stringBuilder.AppendLine("**************END******************");
            return stringBuilder.ToString();
        }
    }
}
