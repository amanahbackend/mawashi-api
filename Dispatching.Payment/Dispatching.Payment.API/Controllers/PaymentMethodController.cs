﻿using Dispatching.Payment.BLL.IManagers;
using Dispatching.Payment.BLL.Managers;
using Dispatching.Payment.Models.Entities;
using DispatchProduct.Controllers.V1;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Utilites.ProcessingResult;
using Dispatching.Payment.API.ViewModels;
using Microsoft.AspNetCore.Mvc;
using CommonEnums;
using Dispatching.BuisnessCommon.Enums;

namespace Dispatching.Payment.API.Controllers
{
    [ApiVersion("1.0")]
    [Route("v{version:apiVersion}/[controller]")]
    public class PaymentMethodController
        : BaseControllerV1<IPaymentMethodManager, PaymentMethod, PaymentMethodViewModel>
    {
        IProcessResultMapper processResultMapper;
        public PaymentMethodController(IPaymentMethodManager _manger, IMapper _mapper,
            IProcessResultMapper _processResultMapper,
            IProcessResultPaginatedMapper _processResultPaginatedMapper)
            : base(_manger, _mapper, _processResultMapper, _processResultPaginatedMapper)
        {
            processResultMapper = _processResultMapper;
        }

        [HttpGet, Route("GetByCountry"), MapToApiVersion("1.0")]
        public ProcessResultViewModel<List<PaymentMethodViewModel>> GetByCountry([FromQuery]string countryCode)
        {
            ProcessResultViewModel<List<PaymentMethodViewModel>> result = null;
            try
            {
                var dataResult = manger.GetByCountryCode(countryCode, OrderType.Individual);
                if (dataResult.IsSucceeded)
                {
                    var data = mapper.Map<List<PaymentMethod>, List<PaymentMethodViewModel>>(dataResult.Data);
                    result = ProcessResultViewModelHelper.Succedded(data);
                }
                else
                {
                    result = ProcessResultViewModelHelper.Failed<List<PaymentMethodViewModel>>(null, dataResult.Status.Message);
                }
            }
            catch (Exception ex)
            {
                result = ProcessResultViewModelHelper.Failed<List<PaymentMethodViewModel>>(null, ex.Message);
            }
            return result;
        }

        [HttpGet, Route("GetByCountryForCompany"), MapToApiVersion("1.0")]
        public ProcessResultViewModel<List<PaymentMethodViewModel>> GetByCountryForCompany([FromQuery]string countryCode)
        {
            ProcessResultViewModel<List<PaymentMethodViewModel>> result = null;
            try
            {
                var dataResult = manger.GetByCountryCode(countryCode, OrderType.Retail);
                if (dataResult.IsSucceeded)
                {
                    var data = mapper.Map<List<PaymentMethod>, List<PaymentMethodViewModel>>(dataResult.Data);
                    result = ProcessResultViewModelHelper.Succedded(data);
                }
                else
                {
                    result = ProcessResultViewModelHelper.Failed<List<PaymentMethodViewModel>>(null, dataResult.Status.Message);
                }
            }
            catch (Exception ex)
            {
                result = ProcessResultViewModelHelper.Failed<List<PaymentMethodViewModel>>(null, ex.Message);
            }
            return result;
        }

        [HttpGet, Route("GetByCountryId/{countryId}"), MapToApiVersion("1.0")]
        public ProcessResultViewModel<List<PaymentMethodViewModel>> GetByCountryId([FromRoute]int countryId)
        {
            ProcessResultViewModel<List<PaymentMethodViewModel>> result = null;
            try
            {
                var dataResult = manger.GetByCountryId(countryId, OrderType.Individual);
                result = processResultMapper.Map<List<PaymentMethod>, List<PaymentMethodViewModel>>(dataResult);
            }
            catch (Exception ex)
            {
                result = ProcessResultViewModelHelper.Failed<List<PaymentMethodViewModel>>(null, ex.Message);
            }
            return result;
        }

        [HttpGet, Route("GetByCountryIdForCompany/{countryId}"), MapToApiVersion("1.0")]
        public ProcessResultViewModel<List<PaymentMethodViewModel>> GetByCountryIdForCompany([FromRoute]int countryId)
        {
            ProcessResultViewModel<List<PaymentMethodViewModel>> result = null;
            try
            {
                var dataResult = manger.GetByCountryId(countryId, OrderType.Retail);
                result = processResultMapper.Map<List<PaymentMethod>, List<PaymentMethodViewModel>>(dataResult);
            }
            catch (Exception ex)
            {
                result = ProcessResultViewModelHelper.Failed<List<PaymentMethodViewModel>>(null, ex.Message);
            }
            return result;
        }

        [HttpGet, Route("GetByCountryDelivery"), MapToApiVersion("1.0")]
        public ProcessResultViewModel<List<PaymentMethodViewModel>> GetByCountryDelivery([FromQuery]int countryId, DeliveryType deliveryType)
        {
            ProcessResultViewModel<List<PaymentMethodViewModel>> result = null;
            try
            {
                var dataResult = manger.GetByCountryDelivery(countryId, deliveryType);
                dataResult.Data = dataResult.Data.Where(c => c.AvailableFor == OrderType.Individual).ToList();
                result = processResultMapper.Map<List<PaymentMethod>, List<PaymentMethodViewModel>>(dataResult);
            }
            catch (Exception ex)
            {
                result = ProcessResultViewModelHelper.Failed<List<PaymentMethodViewModel>>(null, ex.Message);
            }
            return result;
        }

        [HttpGet, Route("Activate/{id}"), MapToApiVersion("1.0")]
        public ProcessResultViewModel<bool> Activate([FromRoute]int id)
        {
            ProcessResultViewModel<bool> result = null;
            try
            {
                var updateResult = manger.Activate(id);
                result = processResultMapper.Map<bool, bool>(updateResult);
            }
            catch (Exception ex)
            {
                result = ProcessResultViewModelHelper.Failed(false, ex.Message);
            }
            return result;
        }

        [HttpGet, Route("Deactivate/{id}"), MapToApiVersion("1.0")]
        public ProcessResultViewModel<bool> Deactivate([FromRoute]int id)
        {
            ProcessResultViewModel<bool> result = null;
            try
            {
                var updateResult = manger.Deactivate(id);
                result = processResultMapper.Map<bool, bool>(updateResult);
            }
            catch (Exception ex)
            {
                result = ProcessResultViewModelHelper.Failed(false, ex.Message);
            }
            return result;
        }

        [HttpGet, Route("GetAllByCountryId/{countryId}"), MapToApiVersion("1.0")]
        public ProcessResultViewModel<List<PaymentMethodViewModel>> GetAllByCountryId([FromRoute]int countryId)
        {
            ProcessResultViewModel<List<PaymentMethodViewModel>> result = null;
            try
            {
                var dataResult = manger.GetByCountryId(countryId);

                result = processResultMapper.Map<List<PaymentMethod>, List<PaymentMethodViewModel>>(dataResult);
                foreach (var item in result.Data)
                {
                    item.AvailableForName = Enum.GetName(typeof(OrderType), item.AvailableFor);
                }
            }
            catch (Exception ex)
            {
                result = ProcessResultViewModelHelper.Failed<List<PaymentMethodViewModel>>(null, ex.Message);
            }
            return result;
        }
    }
}
