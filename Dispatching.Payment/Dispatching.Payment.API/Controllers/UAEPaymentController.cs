﻿using Dispatching.Payment.API.ServiceCommunications.Ordering;
using Dispatching.Payment.API.UAEPaymentHelpers;
using Dispatching.Payment.API.Utilities;
using Dispatching.Payment.API.ViewModels;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;
using System.Web;
using Utilites;
using Utilites.ProcessingResult;

namespace Dispatching.Payment.API.Controllers
{
    [ApiVersion("1.0")]
    [Route("v{version:apiVersion}/[controller]")]
    public class UAEPaymentController : Controller
    {
        private VPCRequest _vpcRequest;
        private IConfigurationRoot _configurationRoot;
        private IOrderService _orderService;
        private readonly IHostingEnvironment _hostingEnv;
        LoggingManager _loggingManager;
        public UAEPaymentController(VPCRequest vpcRequest,
            IConfigurationRoot configurationRoot, IOrderService orderService,
            IHostingEnvironment hostingEnv)
        {
            _configurationRoot = configurationRoot;
            _vpcRequest = vpcRequest;
            _orderService = orderService;
            _hostingEnv = hostingEnv;
            _loggingManager = new LoggingManager(hostingEnv);
        }


        [HttpPost, Route("Pay"), MapToApiVersion("1.0")]
        public ProcessResultViewModel<string> Pay([FromBody] PaymentViewModel model)
        {
            ProcessResultViewModel<string> result = null;
            try
            {
                _loggingManager.LogResult($"Time: {DateTime.UtcNow.ToString("MM/dd/yyyy hh:mm tt")}");
                _loggingManager.LogResult($"Calling UAE Pay for order: {model.OrderId} - Amount: {model.Amount}");

                model.Amount = model.Amount * 100;
                string vpc_MerchTxnRef = _configurationRoot["UAECreditPayment:vpc_MerchTxnRef"];
                string vpc_Currency = _configurationRoot["UAECreditPayment:vpc_Currency"];
                string vpc_Locale = _configurationRoot["UAECreditPayment:vpc_Locale"];
                string vpc_ReturnURL = _configurationRoot["UAECreditPayment:vpc_ReturnURL"];


                _vpcRequest.AddDigitalOrderField("vpc_Version", _vpcRequest.Version);
                _vpcRequest.AddDigitalOrderField("vpc_Command", _vpcRequest.Command);
                _vpcRequest.AddDigitalOrderField("vpc_AccessCode", _vpcRequest.AccessCode);
                _vpcRequest.AddDigitalOrderField("vpc_Merchant", _vpcRequest.MerchantID);
                _vpcRequest.AddDigitalOrderField("vpc_ReturnURL", vpc_ReturnURL);
                _vpcRequest.AddDigitalOrderField("vpc_MerchTxnRef", vpc_MerchTxnRef);
                _vpcRequest.AddDigitalOrderField("vpc_OrderInfo", model.OrderId.ToString());
                _vpcRequest.AddDigitalOrderField("vpc_Amount", model.Amount.ToString());
                _vpcRequest.AddDigitalOrderField("vpc_Currency", vpc_Currency);
                _vpcRequest.AddDigitalOrderField("vpc_Locale", vpc_Locale);

                String url = _vpcRequest.Create3PartyQueryString();
                result = ProcessResultViewModelHelper.Succedded(url);
                _loggingManager.LogResult($"Time: {DateTime.UtcNow.ToString("MM/dd/yyyy hh:mm tt")}");
                _loggingManager.LogResult($"Payment url generated successfully: {url}");
                _loggingManager.LogResult("================================================================");

            }
            catch (Exception ex)
            {
                result = ProcessResultViewModelHelper.Failed<string>(null, ex.Message);
                _loggingManager.LogResult($"Time: {DateTime.UtcNow.ToString("MM/dd/yyyy hh:mm tt")}");
                _loggingManager.LogResult($"Payment url generation error: {ex.Message}");
                _loggingManager.LogResult("================================================================");

                _loggingManager.LogError($"Exception occured while generating url", ex, MethodBase.GetCurrentMethod().Name);
            }
            return result;
        }

        [HttpGet, Route("Receipt"), MapToApiVersion("1.0")]
        public void Receipt()
        {
            try
            {

                var query = HttpUtility.ParseQueryString(Request.QueryString.Value);
                Console.WriteLine(query);

                _vpcRequest.Process3PartyResponse(query);

                var amount = Convert.ToDouble(_vpcRequest.GetResultField("vpc_Amount", "Unknown")) / 100;
                var orderId = Convert.ToInt32(_vpcRequest.GetResultField("vpc_OrderInfo", "Unknown"));

                var receipt = new UAEPaymentReceiptViewModel
                {
                    TxnResponseCode = _vpcRequest.GetResultField("vpc_TxnResponseCode", "Unknown"),
                    MerchTxnRef = _vpcRequest.GetResultField("vpc_MerchTxnRef", "Unknown"),
                    OrderInfo = orderId.ToString(),
                    Merchant = _vpcRequest.GetResultField("vpc_Merchant", "Unknown"),
                    Amount = amount.ToString(),
                    Message = _vpcRequest.GetResultField("vpc_Message", "Unknown"),
                    ReceiptNo = _vpcRequest.GetResultField("vpc_ReceiptNo", "Unknown"),
                    AcqResponseCode = _vpcRequest.GetResultField("vpc_AcqResponseCode", "Unknown"),
                    AuthorizeId = _vpcRequest.GetResultField("vpc_AuthorizeId", "Unknown"),
                    BatchNo = _vpcRequest.GetResultField("vpc_BatchNo", "Unknown"),
                    TransactionNo = _vpcRequest.GetResultField("vpc_TransactionNo", "Unknown"),
                    Card = _vpcRequest.GetResultField("vpc_Card", "Unknown"),
                    TxnResponseCodeDesc = PaymentCodesHelper.GetTxnResponseCodeDescription(_vpcRequest.GetResultField("vpc_TxnResponseCode", "Unknown")),

                    // Card Security Code Fields
                    CscResultCode = _vpcRequest.GetResultField("vpc_cscResultCode", "Unknown"),
                    CscResultCodeDesc = PaymentCodesHelper.GetCSCDescription(_vpcRequest.GetResultField("vpc_cscResultCode", "Unknown"))
                };

                Console.WriteLine(receipt);
                _loggingManager.LogResult($"Time: {DateTime.UtcNow.ToString("MM/dd/yyyy hh:mm tt")}");
                _loggingManager.LogResult($"Calling UAE Reciept for order: {orderId} - Amount: {amount} - Result: {receipt.TxnResponseCode} -  {receipt.TxnResponseCodeDesc}");

                var paymentInfo = new PaymentInfoViewModel
                {
                    Amount = amount,
                    CardType = receipt.Card,
                    OrderId = orderId,
                    ReciptNo = receipt.ReceiptNo,
                    TransactionCode = receipt.TxnResponseCode,
                    TransactionNo = receipt.TransactionNo,
                    PaymentMessage = receipt.TxnResponseCodeDesc
                };

                if (receipt.TxnResponseCode.Equals("0"))
                {
                    _loggingManager.LogResult($"Set payment succeeded = true");
                    paymentInfo.IsPaymentSucceeded = true;
                }
                else
                {
                    _loggingManager.LogResult($"Set payment succeeded = false");
                    paymentInfo.IsPaymentSucceeded = false;
                }

                _loggingManager.LogResult($"Calling UpdatePayment Service");
                _orderService.UpdateOrderPayment("v1", paymentInfo);
                _loggingManager.LogResult($"UpdatePayment Service called");
                _loggingManager.LogResult("================================================================");

            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                _loggingManager.LogResult($"Time: {DateTime.UtcNow.ToString("MM/dd/yyyy hh:mm tt")}");
                _loggingManager.LogResult($"Payment receipt service error: {ex.Message}");
                _loggingManager.LogResult("================================================================");
                _loggingManager.LogError($"Exception occured in receipt service", ex, MethodBase.GetCurrentMethod().Name);
            }
        }
    }
}
