﻿using Dispatching.Payment.API.ServiceCommunications.Ordering;
using Dispatching.Payment.API.Utilities;
using Dispatching.Payment.API.ViewModels;
using DispatchProduct.Api.HttpClient;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;
using Utilites.ProcessingResult;

namespace Dispatching.Payment.API.Controllers
{
    [ApiVersion("1.0")]
    [Route("v{version:apiVersion}/[controller]")]
    public class KWPaymentController : Controller
    {
        private IConfigurationRoot _configurationRoot;
        private IOrderService _orderService;
        private LoggingManager _loggingManager;
        private IHostingEnvironment _hostingEnvironment;
        public KWPaymentController(IConfigurationRoot configurationRoot, IOrderService orderService, IHostingEnvironment hostingEnvironment)
        {
            _configurationRoot = configurationRoot;
            _orderService = orderService;
            _hostingEnvironment = hostingEnvironment;
            _loggingManager = new LoggingManager(hostingEnvironment);
        }

        [HttpPost, Route("Pay"), MapToApiVersion("1.0")]
        public async Task<ProcessResultViewModel<string>> Pay([FromBody] PaymentViewModel data)
        {
            ProcessResultViewModel<string> result = null;
            try
            {
                _loggingManager.LogResult($"Time: {DateTime.UtcNow.ToString("MM/dd/yyyy hh:mm tt")}");
                _loggingManager.LogResult($"Calling KW Pay for order: {data.OrderId} - Amount: {data.Amount}");

                var knetUrl = _configurationRoot["KWCreditPayment:KnetPaymentApiURL"];
                var obj = new { model = data };
                var response = await HttpRequestFactory.Post(knetUrl, obj);
                var paymentUrlResult = response.ContentAsType<KWPaymentUrlResult>();
                var url = paymentUrlResult.d;
                result = ProcessResultViewModelHelper.Succedded(url);
                _loggingManager.LogResult($"Time: {DateTime.UtcNow.ToString("MM/dd/yyyy hh:mm tt")}");
                _loggingManager.LogResult($"Payment url generated successfully: {url}");
                _loggingManager.LogResult("================================================================");
            }
            catch (Exception ex)
            {
                result = ProcessResultViewModelHelper.Failed<string>(null, ex.Message);
                _loggingManager.LogResult($"Time: {DateTime.UtcNow.ToString("MM/dd/yyyy hh:mm tt")}");
                _loggingManager.LogResult($"Payment url generation error: {ex.Message}");
                _loggingManager.LogResult("================================================================");
                _loggingManager.LogError($"Exception occured while generating url", ex, MethodBase.GetCurrentMethod().Name);
            }
            return result;
        }

        [HttpPost, Route("Success"), MapToApiVersion("1.0")]
        public void Success([FromBody] KWPaymentReceiptViewModel data)
        {
            try
            {
                _loggingManager.LogResult($"Time: {DateTime.UtcNow.ToString("MM/dd/yyyy hh:mm tt")}");
                _loggingManager.LogResult($"Calling KW Success for order: {data.Udf1} - Amount: {data.Udf4} - Result: {data.Result}");

                Console.WriteLine(data.Result);
                var paymentInfo = new PaymentInfoViewModel
                {
                    Amount = Convert.ToDouble(data.Udf4),
                    OrderId = Convert.ToInt32(data.Udf1),
                    ReciptNo = data.Refr,
                    TransactionNo = data.TranId,
                    PaymentMessage = data.Result
                };
                if (data.Result.Equals("CAPTURED"))
                {
                    _loggingManager.LogResult($"Set payment succeeded = true");
                    paymentInfo.IsPaymentSucceeded = true;
                }
                else
                {
                    _loggingManager.LogResult($"Set payment succeeded = false");
                    paymentInfo.IsPaymentSucceeded = false;
                }
                _loggingManager.LogResult($"Calling UpdatePayment Service");
                _orderService.UpdateOrderPayment("v1", paymentInfo);
                _loggingManager.LogResult($"UpdatePayment Service called");
                _loggingManager.LogResult("================================================================");
            }
            catch (Exception ex)
            {
                _loggingManager.LogResult($"Time: {DateTime.UtcNow.ToString("MM/dd/yyyy hh:mm tt")}");
                _loggingManager.LogResult($"Payment Success service error: {ex.Message}");
                _loggingManager.LogResult("================================================================");
                _loggingManager.LogError($"Exception occured in success service", ex, MethodBase.GetCurrentMethod().Name);
            }
        }


        [HttpPost, Route("Error"), MapToApiVersion("1.0")]
        public void Error([FromBody] KWPaymentReceiptViewModel data)
        {
            try
            {
                _loggingManager.LogResult($"Time: {DateTime.UtcNow.ToString("MM/dd/yyyy hh:mm tt")}");
                _loggingManager.LogResult($"Calling KW Error for order: {data.Udf1} - Amount: {data.Udf4} - Result: {data.Result}");

                Console.WriteLine(data.Result);
                var paymentInfo = new PaymentInfoViewModel
                {
                    Amount = Convert.ToDouble(data.Udf4),
                    OrderId = Convert.ToInt32(data.Udf1),
                    ReciptNo = data.Refr,
                    TransactionNo = data.TranId,
                    PaymentMessage = data.Result,
                    IsPaymentSucceeded = false
                };
                _loggingManager.LogResult($"Calling UpdatePayment Service");
                _orderService.UpdateOrderPayment("v1", paymentInfo);
                _loggingManager.LogResult($"UpdatePayment Service called");
                _loggingManager.LogResult("================================================================");

            }
            catch (Exception ex)
            {
                _loggingManager.LogResult($"Time: {DateTime.UtcNow.ToString("MM/dd/yyyy hh:mm tt")}");
                _loggingManager.LogResult($"Payment Error service error: {ex.Message}");
                _loggingManager.LogResult("================================================================");
                _loggingManager.LogError($"Exception occured in error service", ex, MethodBase.GetCurrentMethod().Name);
            }

        }
    }
}
