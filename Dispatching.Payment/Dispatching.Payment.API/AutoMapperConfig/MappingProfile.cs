﻿using AutoMapper;
using CommonEnums;
using Dispatching.BuisnessCommon.Enums;
using Dispatching.Payment.API.ViewModels;
using Dispatching.Payment.Models.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Utilities.Utilites;

namespace Dispatching.Payment.API.AutoMapperConfig
{
    public class MappingProfile : Profile
    {
        public MappingProfile()
        {
            CreateMap<PaymentMethod, PaymentMethodViewModel>()
             .ForMember(dest => dest.AvailableForName, opt => opt.ResolveUsing(BindOrderType));

            CreateMap<PaymentMethodViewModel, PaymentMethod>()
                .IgnoreBaseEntityProperties();
        }
        private string BindOrderType(object model, PaymentMethodViewModel orderViewModel)
        {
            string result = "";
            if (orderViewModel.AvailableFor > 0)
            {
                result = EnumManager<OrderType>.GetName(orderViewModel.AvailableFor);
            }
            return result;
        }
    }
}
