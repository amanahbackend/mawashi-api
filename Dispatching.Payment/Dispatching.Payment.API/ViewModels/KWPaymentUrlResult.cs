﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Dispatching.Payment.API.ViewModels
{
    public class KWPaymentUrlResult
    {
        public string d { get; set; }
    }

    public class Data
    {
        public string Url { get; set; }
        public string ErrorMsg { get; set; }
    }
}
