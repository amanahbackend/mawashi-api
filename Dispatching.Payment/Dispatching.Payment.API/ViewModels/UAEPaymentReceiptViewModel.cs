﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Dispatching.Payment.API.ViewModels
{
    public class UAEPaymentReceiptViewModel
    {
        public string TxnResponseCode { get; set; }
        public string MerchTxnRef { get; set; }
        public string OrderInfo { get; set; }
        public string Merchant { get; set; }
        public string Amount { get; set; }
        public string Message { get; set; }
        public string ReceiptNo { get; set; }
        public string AcqResponseCode { get; set; }
        public string AuthorizeId { get; set; }
        public string BatchNo { get; set; }
        public string TransactionNo { get; set; }
        public string Card { get; set; }
        public string TxnResponseCodeDesc { get; set; }
        public string CscResultCode { get; set; }
        public string CscResultCodeDesc { get; set; }
    }
}
