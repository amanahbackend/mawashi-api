﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Dispatching.Payment.API.ViewModels
{
    public class PaymentInfoViewModel
    {
		public int OrderId { get; set; }
		public double Amount { get; set; }
		public string TransactionNo { get; set; }
		public string CardType { get; set; }
		public string ReciptNo { get; set; }
		public string TransactionCode { get; set; }
        public string PaymentMessage { get; set; }
        public bool IsPaymentSucceeded { get; set; }
    }
}
