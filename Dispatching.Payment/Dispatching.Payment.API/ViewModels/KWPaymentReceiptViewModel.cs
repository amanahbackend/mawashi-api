﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Dispatching.Payment.API.ViewModels
{
    public class KWPaymentReceiptViewModel
    {
        public string PaymentID { get; set; }
        public string Result { get; set; }
        public string PostDate { get; set; }
        public string TranId { get; set; }
        public string Auth { get; set; }
        public string Refr { get; set; }
        public string TrackId { get; set; }

        public string Udf1 { get; set; }
        public string Udf2 { get; set; }
        public string Udf3 { get; set; }
        public string Udf4 { get; set; }
        public string Udf5 { get; set; }
    }
}
