﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Dispatching.Payment.API.ViewModels
{
    public class PaymentViewModel
    {
        public int OrderId { get; set; }
        public double Amount { get; set; }
    }
}
