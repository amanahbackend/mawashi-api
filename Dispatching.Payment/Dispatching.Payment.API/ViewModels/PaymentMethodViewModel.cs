﻿using Dispatching.BuisnessCommon.Enums;
using DispatchProduct.BuildingBlocks.BaseEntities.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Dispatching.Payment.API.ViewModels
{
    public class PaymentMethodViewModel : BaseLKPEntityViewModel
    {
        public bool IsActivated { get; set; }
        public string CountryCode { get; set; }
        public int CountryId { get; set; }
        public string Code { get; set; }
        public OrderType AvailableFor { get; set; }
        public string AvailableForName { get; set; }
    }
}
