﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Dispatching.Payment.EFCore.MSSQL.Migrations
{
    public partial class addAvailableForFieldToPaymentMethodModel : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "AvailableFor",
                table: "PaymentMethod",
                nullable: false,
                defaultValue: 0);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "AvailableFor",
                table: "PaymentMethod");
        }
    }
}
