﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Dispatching.Payment.EFCore.MSSQL.Migrations
{
    public partial class modifyingPaymentMethodTableName : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropPrimaryKey(
                name: "PK_Lkp_AddressType",
                table: "Lkp_AddressType");

            migrationBuilder.RenameTable(
                name: "Lkp_AddressType",
                newName: "PaymentMethod");

            migrationBuilder.AddPrimaryKey(
                name: "PK_PaymentMethod",
                table: "PaymentMethod",
                column: "Id");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropPrimaryKey(
                name: "PK_PaymentMethod",
                table: "PaymentMethod");

            migrationBuilder.RenameTable(
                name: "PaymentMethod",
                newName: "Lkp_AddressType");

            migrationBuilder.AddPrimaryKey(
                name: "PK_Lkp_AddressType",
                table: "Lkp_AddressType",
                column: "Id");
        }
    }
}
