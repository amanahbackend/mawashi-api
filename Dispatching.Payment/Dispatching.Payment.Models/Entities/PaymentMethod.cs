﻿using Dispatching.BuisnessCommon.Enums;
using DispatchProduct.BuildingBlocks.BaseEntities.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace Dispatching.Payment.Models.Entities
{
    public class PaymentMethod : BaseLKPEntity
    {
        public bool IsActivated { get; set; }
        public string CountryCode { get; set; }
        public int CountryId { get; set; }
        public string Code { get; set; }
        public OrderType AvailableFor { get; set; }
    }
}
