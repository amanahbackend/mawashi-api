﻿using Dispatching.Payment.Models.Entities;
using DispatchProduct.BuildingBlocks.BaseEntities.EntityConfigurations;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;

namespace Dispatching.Payment.Models.EntitiesConfiguration
{
    class PaymentMethodTypeConfiguration : BaseLKPEntityTypeConfiguration<PaymentMethod>,
        IEntityTypeConfiguration<PaymentMethod>
    {
        public new void Configure(EntityTypeBuilder<PaymentMethod> builder)
        {
            base.Configure(builder);
            builder.ToTable("PaymentMethod");
            builder.Property(p => p.CountryCode).IsRequired(true);
            builder.Property(p => p.IsActivated).IsRequired(true).HasDefaultValue(true);
            builder.Property(p => p.CountryId).IsRequired();
        }
    }
}
