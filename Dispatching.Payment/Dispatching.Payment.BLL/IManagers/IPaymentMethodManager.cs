﻿using CommonEnums;
using Dispatching.BuisnessCommon.Enums;
using Dispatching.Payment.Models.Entities;
using DispatchProduct.RepositoryModule;
using System;
using System.Collections.Generic;
using System.Text;
using Utilites.ProcessingResult;

namespace Dispatching.Payment.BLL.IManagers
{
    public interface IPaymentMethodManager : IRepository<PaymentMethod>
    {
        void SeedKw();
        void SeedUae();
        ProcessResult<List<PaymentMethod>> GetByCountryId(int countryId);
        ProcessResult<List<PaymentMethod>> GetByCountryCode(string code, OrderType availableFor);
        ProcessResult<List<PaymentMethod>> GetByCountryId(int countryId, OrderType availableFor);
        ProcessResult<List<PaymentMethod>> GetByCountryDelivery(int countryId, DeliveryType deliveryType);
        ProcessResult<bool> Activate(int paymentMethodId);
        ProcessResult<bool> Deactivate(int paymentMethodId);
    }
}
