﻿using Dispatching.Payment.BLL.IManagers;
using Dispatching.Payment.Models.Entities;
using DispatchProduct.RepositoryModule;
using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.EntityFrameworkCore;
using Dispatching.Payment.Models.Context;
using System.Linq;
using Utilites.ProcessingResult;
using CommonEnums;
using Dispatching.BuisnessCommon.Enums;

namespace Dispatching.Payment.BLL.Managers
{
    public class PaymentMethodManager : Repository<PaymentMethod>, IPaymentMethodManager
    {
        private readonly DataContext _context;

        public PaymentMethodManager(DataContext context) : base(context)
        {
            _context = context;
        }

        public void SeedKw()
        {
            if (!_context.PaymentMethods.Any())
            {
                List<PaymentMethod> methods = new List<PaymentMethod>()
                {
                    new PaymentMethod
                    {
                        CountryCode = "KW",
                        CountryId = (int)Country.Kuwait,
                        Code = EnumManager<PaymentType>.GetName(PaymentType.KNet),
                        NameEN = "Knet",
                        NameAR = "كي نت",
                        AvailableFor = OrderType.Individual
                    },
                    new PaymentMethod
                    {
                        CountryCode = "KW",
                        CountryId = (int)Country.Kuwait,
                        Code = EnumManager<PaymentType>.GetName(PaymentType.KuwaitCashOnDelivery),
                        NameEN = "Cash on Delivery",
                        NameAR = "الدفع عند الاستلام",
                        AvailableFor = OrderType.Individual
                    },
                    new PaymentMethod
                    {
                        CountryCode = "KW",
                        CountryId = (int)Country.Kuwait,
                        Code = EnumManager<PaymentType>.GetName(PaymentType.KuwaitCashOnDelivery),
                        NameEN = "Cash on Delivery",
                        NameAR = "الدفع عند الاستلام",
                        AvailableFor = OrderType.Retail
                    },
                    new PaymentMethod
                    {
                        CountryCode = "KW",
                        CountryId = (int)Country.Kuwait,
                        Code = EnumManager<PaymentType>.GetName(PaymentType.Wallet),
                        NameEN = "Wallet",
                        NameAR = "المحفظة",
                        AvailableFor = OrderType.Retail
                    }
                };
                Add(methods);
            }
        }

        public void SeedUae()
        {
            if (!_context.PaymentMethods.Any())
            {
                List<PaymentMethod> methods = new List<PaymentMethod>()
                {
                    new PaymentMethod
                    {
                        CountryCode = "UAE",
                        CountryId = (int)Country.UAE,
                        Code = EnumManager<PaymentType>.GetName(PaymentType.MasterCard),
                        NameEN = "Credit Card",
                        NameAR = "بطاقة الائتمان",
                        AvailableFor = OrderType.Individual
                    },
                    new PaymentMethod
                    {
                        CountryCode = "UAE",
                        CountryId = (int)Country.UAE,
                        Code = EnumManager<PaymentType>.GetName(PaymentType.UAECashOnDelivery),
                        NameEN = "Cash on Delivery",
                        NameAR = "الدفع عند الاستلام",
                        AvailableFor = OrderType.Individual
                    },
                    new PaymentMethod
                    {
                        CountryCode = "UAE",
                        CountryId = (int)Country.UAE,
                        Code = EnumManager<PaymentType>.GetName(PaymentType.UAECashOnDelivery),
                        NameEN = "Cash on Delivery",
                        NameAR = "الدفع عند الاستلام",
                        AvailableFor = OrderType.Retail
                    },
                    new PaymentMethod
                    {
                        CountryCode = "UAE",
                        CountryId = (int)Country.UAE,
                        Code = EnumManager<PaymentType>.GetName(PaymentType.Wallet),
                        NameEN = "Wallet",
                        NameAR = "المحفظة",
                        AvailableFor = OrderType.Retail
                    }
                };
                Add(methods);
            }
        }

        public ProcessResult<List<PaymentMethod>> GetByCountryId(int countryId)
        {
            ProcessResult<List<PaymentMethod>> result = null;
            try
            {
                var data = GetAllQuerable().Data.Where(p => p.IsActivated && p.CountryId == countryId).ToList();
                result = ProcessResultHelper.Succedded(data);
            }
            catch (Exception ex)
            {
                result = ProcessResultHelper.Failed<List<PaymentMethod>>(null, ex);
            }
            return result;
        }

        public ProcessResult<List<PaymentMethod>> GetByCountryCode(string code, OrderType availableFor=0)
        {
            ProcessResult<List<PaymentMethod>> result = null;
            try
            {
                List<PaymentMethod> data = null;
                if (availableFor > 0)
                {
                    data = GetAllQuerable().Data.Where(p => p.IsActivated &&
                           p.CountryCode.ToLower().Equals(code.ToLower()) &&
                           p.AvailableFor == availableFor).ToList();
                }
                else
                {
                    data = GetAllQuerable().Data.Where(
                           p => p.IsActivated &&
                           p.CountryCode.ToLower().Equals(code.ToLower())).ToList();
                }
                result = ProcessResultHelper.Succedded(data);
            }
            catch (Exception ex)
            {
                result = ProcessResultHelper.Failed<List<PaymentMethod>>(null, ex);
            }
            return result;
        }

        public ProcessResult<List<PaymentMethod>> GetByCountryId(int countryId, OrderType availableFor)
        {
            ProcessResult<List<PaymentMethod>> result = null;
            try
            {
                var data = GetAllQuerable().Data.Where(p => p.IsActivated && p.CountryId == countryId && p.AvailableFor == availableFor).ToList();
                result = ProcessResultHelper.Succedded(data);
            }
            catch (Exception ex)
            {
                result = ProcessResultHelper.Failed<List<PaymentMethod>>(null, ex);
            }
            return result;
        }

        public ProcessResult<List<PaymentMethod>> GetByCountryDelivery(int countryId, DeliveryType deliveryType)
        {
            ProcessResult<List<PaymentMethod>> result = null;
            try
            {
                var data = GetAllQuerable().Data.Where(p => p.IsActivated && p.CountryId == countryId).ToList();
                if (deliveryType == DeliveryType.Donation || 
                    deliveryType == DeliveryType.PickUp)
                {
                    if (countryId == (int)Country.Kuwait)
                    {
                        var kwCOD = EnumManager<PaymentType>.GetName(PaymentType.KuwaitCashOnDelivery);
                        data = data.Where(p => !p.Code.Equals(kwCOD)).ToList();
                    }
                    else if (countryId == (int)Country.UAE)
                    {
                        var uaeCOD = EnumManager<PaymentType>.GetName(PaymentType.UAECashOnDelivery);
                        data = data.Where(p => !p.Code.Equals(uaeCOD)).ToList();
                    }
                }
                result = ProcessResultHelper.Succedded(data);
            }
            catch (Exception ex)
            {
                result = ProcessResultHelper.Failed<List<PaymentMethod>>(null, ex);
            }
            return result;
        }

        public ProcessResult<bool> Activate(int paymentMethodId)
        {
            var entity = Get(paymentMethodId);
            entity.Data.IsActivated = true;
            return Update(entity.Data);
        }

        public ProcessResult<bool> Deactivate(int paymentMethodId)
        {
            var entity = Get(paymentMethodId);
            entity.Data.IsActivated = false;
            return Update(entity.Data);
        }
    }
}
