﻿using AutoMapper;
using DispatchProduct.Controllers;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.Extensions.DependencyInjection;
using System.Threading.Tasks;

using System.Linq;
using Dispatching.PromoCodeModule.BLL.IManagers;
using DispatchProduct.Controllers.V1;
using Dispatching.PromoCodeModule.Models.Entities;
using Utilites.ProcessingResult;

namespace Dispatching.PromoCodeHistoryModule.API.Controllers
{
    [ApiVersion("1.0")]
    [Route("v{version:apiVersion}/[controller]")]
    public class PromoCodeHistoryController : BaseControllerV1<IPromoCodeHistoryManager, PromoCodeHistory, PromoCodeHistoryViewModel>
    {
        public new IPromoCodeHistoryManager manger;
        public readonly new IMapper mapper;
        IProcessResultMapper processResultMapper;
        IProcessResultPaginatedMapper processResultPaginatedMapper;
        IServiceProvider serviceProvider;
        public PromoCodeHistoryController(IServiceProvider _serviceProvider, IPromoCodeHistoryManager _manger, IMapper _mapper, IProcessResultMapper _processResultMapper, IProcessResultPaginatedMapper _processResultPaginatedMapper) : base(_manger, _mapper, _processResultMapper, _processResultPaginatedMapper)
        {
            this.manger = _manger;
            this.mapper = _mapper;
            serviceProvider = _serviceProvider;
            processResultMapper = _processResultMapper;
            processResultPaginatedMapper = _processResultPaginatedMapper;
        }
       
    }

}
