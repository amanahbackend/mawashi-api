﻿using AutoMapper;
using DispatchProduct.Controllers;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Text;
using Utilites.ProcessingResult;
using Microsoft.Extensions.DependencyInjection;
using System.Threading.Tasks;
using DispatchProduct.Controllers.V1;
using System.Linq;
using Dispatching.PromoCodeModule.BLL.IManagers;
using Dispatching.PromoCodeModule.Models.Entities;

namespace Dispatching.PromoCodeModule.API.Controllers
{
    [ApiVersion("1.0")]
    [Route("v{version:apiVersion}/[controller]")]
    public class PromoCodeController : BaseControllerV1<IPromoCodeManager, PromoCode, PromoCodeViewModel>
    {
        public new IPromoCodeManager manger;
        public readonly new IMapper mapper;
        IProcessResultMapper processResultMapper;
        IProcessResultPaginatedMapper processResultPaginatedMapper;
        IServiceProvider serviceProvider;
        public PromoCodeController(IServiceProvider _serviceProvider, IPromoCodeManager _manger, IMapper _mapper, IProcessResultMapper _processResultMapper, IProcessResultPaginatedMapper _processResultPaginatedMapper) : base(_manger, _mapper, _processResultMapper, _processResultPaginatedMapper)
        {
            this.manger = _manger;
            this.mapper = _mapper;
            serviceProvider = _serviceProvider;
            processResultMapper = _processResultMapper;
            processResultPaginatedMapper = _processResultPaginatedMapper;
        }
        [Route("SubmitPromoCode")]
        [HttpPost]
        [MapToApiVersion("1.0")]
        public ProcessResult<PromoResult> SubmitPromoCode([FromBody]PromoCodeValidation model)
        {
            ProcessResult<PromoResult> result = null;
            result = PromoCodeManager.CheckPromoCodeValidation(model.PromoCode, model.Value, model.CustomerId, model.IsUse);
            return result;
        }
        private IPromoCodeManager PromoCodeManager
        {
            get { return serviceProvider.GetService<IPromoCodeManager>(); }
        }
    }

}
