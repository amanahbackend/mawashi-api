﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Dispatching.PromoCodeModule.Models.Context;
using Microsoft.AspNetCore;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Serilog;
using Utilities.Utilites.SerilogExtensions;
using Microsoft.Extensions.DependencyInjection;
using DispatchProduct.PromoCodeModule.API.Seed;

namespace Dispatching.PromoCodeModule.API
{
    public class Program
    {
        public static void Main(string[] args)
        {
            Log.Logger = new LoggerConfiguration()
                 .Enrich.With<CustomExceptionEnricher>()
                 .MinimumLevel.Error()
                 .Enrich.FromLogContext()
                 .WriteTo.File("logs/log_.csv", rollingInterval: RollingInterval.Day,
                 outputTemplate: "{Timestamp:yyyy-MM-dd HH:mm:ss.fff zzz},[{Level}],{Message},{ExceptionMessage},{ExceptionSource},{ExceptionType},{ExceptionStackTrace},{NewLine}")
                 .CreateLogger();

            BuildWebHost(args).MigrateDbContext<PromoCodeDbContext>((context, services) =>
            {
                var env = services.GetService<IHostingEnvironment>();
                var logger = services.GetService<ILogger<PromoCodeDbContextSeed>>();
                new PromoCodeDbContextSeed()
                    .SeedAsync(context, env, logger)
                    .Wait();
            }).Run();
        }

        public static IWebHost BuildWebHost(string[] args) =>
                       WebHost.CreateDefaultBuilder(args)
                           .UseKestrel()
                           .UseContentRoot(Directory.GetCurrentDirectory())
                           .UseIISIntegration()
                           .UseStartup<Startup>()
                           .ConfigureLogging((hostingContext, builder) =>
                           {
                               builder.AddConfiguration(hostingContext.Configuration.GetSection("Logging"));
                               builder.AddConsole();
                               builder.AddDebug();
                           })
                           .Build();
    }
}
