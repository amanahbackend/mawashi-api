﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Threading.Tasks;
using Autofac;
using Autofac.Extensions.DependencyInjection;
using AutoMapper;
using BuildingBlocks.ServiceDiscovery;
using Dispatching.PromoCodeModule.BLL.IManagers;
using Dispatching.PromoCodeModule.BLL.Managers;
using Dispatching.PromoCodeModule.Models.Context;
using Dispatching.PromoCodeModule.Models.Entities;
using Dispatching.PromoCodeModule.Models.Settings;
using DispatchProduct.RepositoryModule;
using DnsClient;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Newtonsoft.Json.Serialization;
using Swashbuckle.AspNetCore.Swagger;
using Utilites.PaginatedItems;
using Utilites.ProcessingResult;

namespace Dispatching.PromoCodeModule.API
{
    public class Startup
    {
        public IHostingEnvironment _env;
        public IConfigurationRoot Configuration { get; }
        public Startup(IHostingEnvironment env)
        {
            _env = env;
            var builder = new ConfigurationBuilder()
                .SetBasePath(env.ContentRootPath)
                .AddEnvironmentVariables();
            Configuration = builder.Build();

            var country = Configuration.GetSection("DockerContainerName").Value.Split('.').Last().ToLower();
           builder = new ConfigurationBuilder()
                .SetBasePath(env.ContentRootPath)
                .AddJsonFile($"appsettings.{country}.json", optional: false, reloadOnChange: true)
                .AddEnvironmentVariables();

            Configuration = builder.Build();
        }


        // This method gets called by the runtime. Use this method to add services to the container.
        public IServiceProvider ConfigureServices(IServiceCollection services)
        {
            services.AddCors(options =>
            {
                options.AddPolicy("AllowAll",
                    builder =>
                    {
                        builder
                        .AllowAnyOrigin()
                        .AllowAnyMethod()
                        .AllowAnyHeader()
                        .AllowCredentials();
                    });
            });

            services.AddSingleton(provider => Configuration);

            services.AddMvc().AddJsonOptions(options =>
            {
                options.SerializerSettings.ContractResolver = new CamelCasePropertyNamesContractResolver();
            });

            string consulContainerName = Configuration.GetSection("ServiceRegisteryContainerName").Value;
            int consulPort = Convert.ToInt32(Configuration.GetSection("ServiceRegisteryPort").Value);
            IPAddress ip = Dns.GetHostEntry(consulContainerName).AddressList.Where(o => o.AddressFamily == AddressFamily.InterNetwork).First();
            services.AddSingleton<IDnsQuery>(new LookupClient(ip, consulPort));

            services.AddServiceDiscovery(Configuration.GetSection("ServiceDiscovery"));

            services.AddApiVersioning(o => o.ReportApiVersions = true);
            services.AddDbContext<PromoCodeDbContext>(options =>
            options.UseSqlServer(Configuration["ConnectionString"],
            sqlOptions => sqlOptions.MigrationsAssembly("Dispatching.PromoCodeModule.EFCore.MSSQL")));

            services.AddOptions();
            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1",
                    new Info()
                    {
                        Title = "PromoCode API",
                        Description = "PromoCode  API"
                    });
                c.AddSecurityDefinition("oauth2", new OAuth2Scheme
                {
                    Type = "oauth2",
                    Flow = "implicit",
                    AuthorizationUrl = $"{Configuration.GetValue<string>("IdentityUrlExternal")}/connect/authorize",
                    TokenUrl = $"{Configuration.GetValue<string>("IdentityUrlExternal")}/connect/token",
                    Scopes = new Dictionary<string, string>()
                    {
                        { "PromoCode", "PromoCode API" }
                    }
                });
            });
            services.AddMvc();
            services.AddAutoMapper(typeof(Startup));
            Mapper.AssertConfigurationIsValid();
            services.Configure<PromoCodeAppSettings>(Configuration);
            services.AddScoped<DbContext, PromoCodeDbContext>();
            services.AddScoped(typeof(IRepository<>), typeof(Repository<>));
            services.AddScoped(typeof(IPromoCode), typeof(PromoCode));
            services.AddScoped(typeof(PromoResult), typeof(PromoResult));
            services.AddScoped(typeof(IPromoCodeHistory), typeof(PromoCodeHistory));
            
            services.AddScoped(typeof(IPromoCodeManager), typeof(PromoCodeManager));
            services.AddScoped(typeof(IPromoCodeHistoryManager), typeof(PromoCodeHistoryManager));

            services.AddScoped(typeof(IProcessResultMapper), typeof(ProcessResultMapper));
            services.AddScoped(typeof(IProcessResultPaginatedMapper), typeof(ProcessResultPaginatedMapper));
            services.AddScoped(typeof(IPaginatedItems<>), typeof(PaginatedItems<>));
            var container = new ContainerBuilder();
            container.Populate(services);
            return new AutofacServiceProvider(container.Build());
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            app.UseCors("AllowAll");
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
                app.UseDatabaseErrorPage();
            }
            else
            {
                app.UseExceptionHandler("/Home/Error");
            }
            app.UseStaticFiles();
            app.UseSwagger();
            app.UseSwaggerUI(c =>
            {
                c.SwaggerEndpoint("/swagger/v1/swagger.json", "Reviewing API");
            });

            app.UseMvc();
            app.UseConsulRegisterService();
        }
    }
}
