﻿
using DispatchProduct.RepositoryModule;
using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Dispatching.PromoCodeModule.Models.Context;
using Dispatching.PromoCodeModule.Models.Entities;

namespace DispatchProduct.PromoCodeModule.API.Seed
{
    public class PromoCodeDbContextSeed : ContextSeed
    {
        public async Task SeedAsync(PromoCodeDbContext context, IHostingEnvironment env,
            ILogger<PromoCodeDbContextSeed> logger,  int? retry = 0)
        {
            int retryForAvaiability = retry.Value;

            try
            {
                var contentRootPath = env.ContentRootPath;
                var webroot = env.WebRootPath;
                List<PromoCode> lstPromoCode = new List<PromoCode>();
                List<PromoCodeHistory> lstPromoCodeHistory = new List<PromoCodeHistory>();
                //lstPromoCode = GetDefaultPromoCodes();
                //lstPromoCodeHistory = GetDefaultPromoCodeHistory();
              
                await SeedEntityAsync(context, lstPromoCode);

                await SeedEntityAsync(context, lstPromoCodeHistory);

            }
            catch (Exception ex)
            {
                if (retryForAvaiability < 3)
                {
                    retryForAvaiability++;

                    logger.LogError(ex.Message, $"There is an error migrating data for CustomerDbContextSeed");

                    await SeedAsync(context, env, logger, retryForAvaiability);
                }
            }
        }
    }
}
