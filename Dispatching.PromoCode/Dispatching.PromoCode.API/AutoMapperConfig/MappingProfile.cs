﻿using AutoMapper;
using Dispatching.PromoCodeModule.Models.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Utilites.PaginatedItems;
using Utilites.PaginatedItemsViewModel;
using Utilities.Utilites;

namespace Dispatching.Inventory.API.AutoMapperConfig
{
    public class MappingProfile : Profile
    {
        public MappingProfile()
        {
            CreateMap<PromoCodeHistory, PromoCodeHistoryViewModel > ();
            CreateMap<PromoCodeHistoryViewModel, PromoCodeHistory>()
                .IgnoreBaseEntityProperties();

            CreateMap<PromoCode, PromoCodeViewModel>();
            CreateMap<PromoCodeViewModel, PromoCode>()
                .IgnoreBaseEntityProperties();

        }
    }
}
