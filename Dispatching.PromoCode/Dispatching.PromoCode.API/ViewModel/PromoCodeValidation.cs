﻿
using DispatchProduct.BuildingBlocks.BaseEntities.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace Dispatching.PromoCodeModule.Models.Entities
{
    public class PromoCodeValidation 
    {
        public string PromoCode { get; set; }
        public int Value { get; set; }
        public int CustomerId { get; set; }
        public bool IsUse { get; set; }
    }
}
