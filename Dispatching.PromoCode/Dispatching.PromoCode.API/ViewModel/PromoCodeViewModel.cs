﻿
using DispatchProduct.BuildingBlocks.BaseEntities.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace Dispatching.PromoCodeModule.Models.Entities
{
    public class PromoCodeViewModel : RepoistryBaseEntity
    {
        public DateTime StartDate { get; set; }
        public DateTime ExpiryDate { get; set; }
        public int CountUse { get; set; }
        public bool IsActive { get; set; }
        public int Value { get; set; }
        public int? MinimumOrderPrice  { get; set; }
        public string Code { get; set; }
        public List<PromoCodeHistory> LstPromoCodeHistory { get; set; }

    }
}
