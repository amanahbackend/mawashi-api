﻿
using DispatchProduct.BuildingBlocks.BaseEntities.Entities;
using DispatchProduct.BuildingBlocks.BaseEntities.IEntities;
using System;
using System.Collections.Generic;
using System.Text;

namespace Dispatching.PromoCodeModule.Models.Entities
{
    public interface IPromoCode : IBaseEntity
    {
        DateTime StartDate { get; set; }
        DateTime ExpiryDate { get; set; }
        int CountUse { get; set; }
        bool IsActive { get; set; }
        int Value { get; set; }
        int? MinimumOrderPrice { get; set; }
        string Code { get; set; }
        List<PromoCodeHistory> LstPromoCodeHistory { get; set; }

    }
}
