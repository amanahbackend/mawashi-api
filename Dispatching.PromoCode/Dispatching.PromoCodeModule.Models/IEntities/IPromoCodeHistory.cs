﻿
using DispatchProduct.BuildingBlocks.BaseEntities.Entities;
using DispatchProduct.BuildingBlocks.BaseEntities.IEntities;
using System;
using System.Collections.Generic;
using System.Text;

namespace Dispatching.PromoCodeModule.Models.Entities
{
    public interface IPromoCodeHistory : IBaseEntity
    {
        int FK_Promo_Id { get; set; }
        int FK_Customer_Id { get; set; }
        DateTime UsedDate { get; set; }
        PromoCode PromoCode { get; set; }

    }
}
