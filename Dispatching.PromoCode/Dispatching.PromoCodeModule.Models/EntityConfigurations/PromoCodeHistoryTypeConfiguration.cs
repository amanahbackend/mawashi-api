﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using DispatchProduct.BuildingBlocks.BaseEntities.EntityConfigurations;
using Dispatching.PromoCodeModule.Models.Entities;

namespace Dispatching.PromoCodeModule.Models.EntitiesConfiguration
{
    public class PromoCodeHistoryTypeConfiguration : BaseEntityTypeConfiguration<PromoCodeHistory>, IEntityTypeConfiguration<PromoCodeHistory>
    {
        public new void Configure(EntityTypeBuilder<PromoCodeHistory> builder)
        {
            base.Configure(builder);
            builder.ToTable("PromoCodeHistory");
            builder.Property(l => l.FK_Promo_Id).IsRequired();
            builder.Property(o => o.Id).ValueGeneratedOnAdd();
            builder.Property(l => l.FK_Customer_Id).IsRequired();
            builder.Property(l => l.UsedDate).IsRequired();
            builder.Ignore(l => l.PromoCode);

        }
    }
}
