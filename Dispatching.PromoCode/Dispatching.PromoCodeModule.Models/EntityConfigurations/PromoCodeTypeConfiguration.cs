﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using DispatchProduct.BuildingBlocks.BaseEntities.EntityConfigurations;
using Dispatching.PromoCodeModule.Models.Entities;

namespace Dispatching.PromoCodeModule.Models.EntitiesConfiguration
{
    public class PromoCodeTypeConfiguration : BaseEntityTypeConfiguration<PromoCode>, IEntityTypeConfiguration<PromoCode>
    {
        public new void Configure(EntityTypeBuilder<PromoCode> builder)
        {

            base.Configure(builder);
            builder.ToTable("PromoCode");
            builder.Property(l => l.StartDate).IsRequired();
            builder.Property(l => l.ExpiryDate).IsRequired();
            builder.Property(l => l.CountUse).IsRequired();
            builder.Property(l => l.IsActive).IsRequired();
            builder.Property(o => o.Id).ValueGeneratedOnAdd();
            builder.Property(l => l.Value).IsRequired();
            builder.Property(l => l.MinimumOrderPrice).IsRequired(false);
            builder.Property(l => l.Code).IsRequired();
            builder.Ignore(l => l.LstPromoCodeHistory);
        }
    }
}
