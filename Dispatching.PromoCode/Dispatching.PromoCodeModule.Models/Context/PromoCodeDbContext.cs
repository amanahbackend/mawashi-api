﻿using Dispatching.PromoCodeModule.Models.Entities;
using Dispatching.PromoCodeModule.Models.EntitiesConfiguration;
using Microsoft.EntityFrameworkCore;

namespace Dispatching.PromoCodeModule.Models.Context
{
    public class PromoCodeDbContext : DbContext
    {
        public DbSet<PromoCode> CityFees { get; set; }
        //public DbSet<LKP_OrderType> OrderType { get; set; }
        public DbSet<PromoCodeHistory> OrderStatus { get; set; }
       

        public PromoCodeDbContext(DbContextOptions<PromoCodeDbContext> options)
            : base(options)
        {

        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.ApplyConfiguration(new PromoCodeTypeConfiguration());
            modelBuilder.ApplyConfiguration(new PromoCodeHistoryTypeConfiguration());
        }
    }
}
