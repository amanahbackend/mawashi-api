﻿
using DispatchProduct.BuildingBlocks.BaseEntities.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace Dispatching.PromoCodeModule.Models.Entities
{
    public class PromoCodeHistory : BaseEntity, IPromoCodeHistory
    {
        public int FK_Promo_Id { get; set; }
        public int FK_Customer_Id { get; set; }
        public DateTime UsedDate { get; set; }
        public PromoCode PromoCode { get; set; }
    }
}
