﻿using System;
using System.Collections.Generic;
using DispatchProduct.RepositoryModule;
using System.Text;
using Utilites.ProcessingResult;
using System.Linq;
using Microsoft.Extensions.DependencyInjection;
using Dispatching.PromoCodeModule.Models.Entities;
using Dispatching.PromoCodeModule.Models.Context;
using Dispatching.PromoCodeModule.BLL.IManagers;

namespace Dispatching.PromoCodeModule.BLL.Managers
{
    public class PromoCodeHistoryManager : Repository<PromoCodeHistory>, IPromoCodeHistoryManager
    {
        IProcessResultMapper processResultMapper;
        IServiceProvider serviceProvider;
        public PromoCodeHistoryManager(PromoCodeDbContext context, IProcessResultMapper _processResultMapper, IServiceProvider _serviceProvider)
            : base(context)
        {
           processResultMapper = _processResultMapper;
            serviceProvider = _serviceProvider;
        }

        public ProcessResult<bool> IsPromoCodeValid(int promoId, int maxCount, int customerId, bool isAdd = false)
        {
            ProcessResult<bool> result = null;
            var count = GetAllQuerable().Data.Where(promo => promo.FK_Promo_Id == promoId && promo.FK_Customer_Id == customerId).Count();
            if (count < maxCount)
            {

                if (isAdd)
                {
                    var promResult = AddPromoCodeToHistory(promoId, customerId);
                    if (promResult.IsSucceeded)
                    {
                        result = ProcessResultHelper.Succedded(true);
                    }
                    else
                    {
                        result = ProcessResultHelper.MapToProcessResult<PromoCodeHistory,bool>(promResult);
                    }

                }
                else
                {
                    result = ProcessResultHelper.Succedded(true);
                }
            }
            else
            {
                result = ProcessResultHelper.Succedded(false);
            }
            return result;
        }

        private IPromoCodeHistory PromoCodeHistory
        {
            get { return serviceProvider.GetService<IPromoCodeHistory>(); }
        }
        public ProcessResult<PromoCodeHistory> AddPromoCodeToHistory(int promoId, int customerId)
        {
            PromoCodeHistory.FK_Promo_Id = promoId;
            PromoCodeHistory.FK_Customer_Id = customerId;
            PromoCodeHistory.UsedDate = DateTime.Now;
            return Add((PromoCodeHistory)PromoCodeHistory);
        }
    }
}
