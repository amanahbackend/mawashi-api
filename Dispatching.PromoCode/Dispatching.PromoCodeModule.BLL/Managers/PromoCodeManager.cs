﻿using System;
using System.Collections.Generic;
using DispatchProduct.RepositoryModule;
using System.Text;
using Utilites.ProcessingResult;
using System.Linq;
using Dispatching.PromoCodeModule.Models.Entities;
using Dispatching.PromoCodeModule.Models.Context;
using Dispatching.PromoCodeModule.BLL.IManagers;
using Microsoft.Extensions.DependencyInjection;
namespace Dispatching.PromoCodeModule.BLL.Managers
{
    public class PromoCodeManager : Repository<PromoCode>, IPromoCodeManager
    {
        IProcessResultMapper processResultMapper;
        IServiceProvider serviceProvider;
        public PromoCodeManager(IServiceProvider _serviceProvider, PromoCodeDbContext context, IProcessResultMapper _processResultMapper)
            : base(context)
        {
            processResultMapper = _processResultMapper;
            serviceProvider = _serviceProvider;
        }
        public ProcessResult<PromoResult> CheckPromoCodeValidation(string promocode, int orderprice, int customerId, bool isAdd = false)
        {
            ProcessResult<PromoResult> result = null;

            var PromoObj = GetAllQuerable().Data.Where(
                   Promo => Promo.Code == promocode
                   &&
                   //Check Promo Date
                   Promo.StartDate <= DateTime.Now
                   &&
                   Promo.ExpiryDate >= DateTime.Now
                   //Check Promo Active
                   &&
                   Promo.IsActive == true
                   &&
                   Promo.MinimumOrderPrice <= orderprice
                   ).FirstOrDefault();

            if (PromoObj != null)
            {
                var historyResult = PromoCodeHistoryManager.IsPromoCodeValid(PromoObj.Id, PromoObj.CountUse, customerId, isAdd);
                if (historyResult.IsSucceeded)
                {
                    if (historyResult.Data)
                    {
                        result = ProcessResultHelper.Succedded(BindValidPromoResult(PromoObj));
                    }
                    else
                    {
                        result = ProcessResultHelper.Succedded(BindNotValidPromoResult());
                    }
                }
                else
                {

                    result = ProcessResultHelper.MapToProcessResult<bool, PromoResult>(historyResult);
                }
            }
            else
            {
                result = ProcessResultHelper.Succedded(BindNotValidPromoResult());
            }
            return result;
        }
        private PromoResult BindNotValidPromoResult()
        {
            PromoResult.Valid = false;
            PromoResult.Value = 0;
            return PromoResult;
        }
        private PromoResult BindValidPromoResult(PromoCode promo)
        {
            PromoResult.Valid = true;
            PromoResult.Value = promo.Value;
            PromoResult.FK_Promo_Id = promo.Id;
            return PromoResult;
        }
        private PromoResult PromoResult
        {
            get { return serviceProvider.GetService<PromoResult>(); }
        }
        private IPromoCodeHistoryManager PromoCodeHistoryManager
        {
            get { return serviceProvider.GetService<IPromoCodeHistoryManager>(); }
        }
    }
}

