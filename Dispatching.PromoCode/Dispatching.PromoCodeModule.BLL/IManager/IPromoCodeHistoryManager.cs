﻿using System;
using System.Collections.Generic;
using DispatchProduct.RepositoryModule;
using System.Text;
using Utilites.ProcessingResult;
using Dispatching.PromoCodeModule.Models.Entities;

namespace Dispatching.PromoCodeModule.BLL.IManagers
{
    public interface IPromoCodeHistoryManager : IRepository<PromoCodeHistory>
    {
        ProcessResult<bool> IsPromoCodeValid(int promoId, int maxCount, int customerId, bool isAdd = false);
    }
}
