﻿using System;
using System.Collections.Generic;
using DispatchProduct.RepositoryModule;
using System.Text;
using Utilites.ProcessingResult;
using Dispatching.PromoCodeModule.Models.Entities;

namespace Dispatching.PromoCodeModule.BLL.IManagers
{
    public interface IPromoCodeManager : IRepository<PromoCode>
    {
        ProcessResult<PromoResult> CheckPromoCodeValidation(string promocode, int orderprice, int customerId, bool isAdd = false);
    }
}
