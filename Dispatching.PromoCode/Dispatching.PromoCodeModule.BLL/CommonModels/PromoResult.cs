﻿
using DispatchProduct.BuildingBlocks.BaseEntities.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace Dispatching.PromoCodeModule.Models.Entities
{
    public class PromoResult 
    {
        public int Value { get; set; }
        public int FK_Promo_Id { get; set; }
        public bool Valid { get; set; }
    }
}
