﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Dispatching.Dropout.API.ServiceSettings;
using Dispatching.Dropout.API.ViewModels;
using DispatchProduct.HttpClient;
using Utilites.ProcessingResult;

namespace Dispatching.Dropout.API.ServiceCommunications.Identity
{
    public interface IIdentityService :
        IDefaultHttpClientCrud<IdentityServiceSettings, string, List<UserDeviceViewModel>>
    {
        Task<ProcessResultViewModel<List<UserDeviceViewModel>>> GetUserDevices(string version, string userId);
    }
}
