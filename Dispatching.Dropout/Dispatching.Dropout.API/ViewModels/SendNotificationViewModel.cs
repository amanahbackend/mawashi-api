﻿using System.Collections.Generic;

namespace Dispatching.Dropout.API.ViewModels
{
    //public class SendNotificationViewModel
    //{
    //    public string UserId { get; set; }
    //    public NotificationType NotificationType { get; set; }
    //    public Dictionary<string, string> Data { get; set; }

    //}

    //public enum NotificationType
    //{
    //    OrderStatus = 1
    //}

    public class SendNotificationViewModel
    {
        public string DeviceId { get; set; }
        public string UserId { get; set; }
        public string Title { get; set; }
        public string Body { get; set; }
        public dynamic Data { get; set; }
    }
}
