﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Dispatching.Dropout.API.ViewModels
{
    public class UserDeviceViewModel
    {
        public int Id { get; set; }
        public string Fk_AppUser_Id { get; set; }
        public string DeveiceId { get; set; }
    }
}
