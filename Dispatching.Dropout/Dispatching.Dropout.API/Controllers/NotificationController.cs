﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Dispatching.Dropout.API.ServiceCommunications.Identity;
using Dispatching.Dropout.API.ViewModels;
using DispatchProduct.Api.HttpClient;
using FcmSharp;
using FcmSharp.Requests;
using FcmSharp.Responses;
using FcmSharp.Settings;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using Utilites.ProcessingResult;

namespace Dispatching.Dropout.API.Controllers
{
    [ApiVersion("1.0")]
    [Route("v{version:apiVersion}/[controller]")]
    public class NotificationController : Controller
    {
        private readonly IConfigurationRoot _configuration;
        private readonly IHostingEnvironment _hostingEnvironment;
        private readonly IIdentityService _identityService;
        public NotificationController(IConfigurationRoot configuration, IHostingEnvironment hostingEnvironment,
            IIdentityService identityService)
        {
            _configuration = configuration;
            _hostingEnvironment = hostingEnvironment;
            _identityService = identityService;
        }

        //[HttpPost, Route("Send"), MapToApiVersion("1.0")]
        //public async Task<IActionResult> Send([FromBody]SendNotificationViewModel model)
        //{
        //    //var accKeyFilePath = _configuration["FcmSettings:AccountKeyFilePath"];

        //    var accKeyFilePath = Path.Combine(_hostingEnvironment.ContentRootPath, _configuration["FcmSettings:AccountKeyFilePath"]);
        //    var projectId = _configuration["FcmSettings:ProjectId"];

        //    // Read the Credentials from a File, which is not under Version Control:
        //    var settings = FileBasedFcmClientSettings.CreateFromFile(projectId, accKeyFilePath);

        //    // Construct the Client:
        //    using (var client = new FcmClient(settings))
        //    {
        //        // Construct the Data Payload to send:

        //        var data = new Dictionary<string, string>
        //        {
        //            {nameof(model.NotificationType), Enum.GetName(typeof(NotificationType), model.NotificationType)}
        //        };
        //        foreach (var item in model.Data)
        //        {
        //            data.Add(item.Key, item.Value);
        //        }

        //        var result = new FcmMessageResponse();
        //        var userDevicesResult = await _identityService.GetUserDevices("v1", model.UserId);
        //        if (userDevicesResult != null && userDevicesResult.IsSucceeded)
        //        {
        //            var userDevices = userDevicesResult.Data;
        //            foreach (var item in userDevices)
        //            {
        //                var registrationId = item.DeveiceId;
        //                // The Message should be sent to the given token:
        //                var message = new FcmMessage()
        //                {
        //                    ValidateOnly = false,
        //                    Message = new Message
        //                    {
        //                        Token = registrationId,
        //                        Data = data
        //                    }
        //                };
        //                // Finally send the Message and wait for the Result:
        //                var cts = new CancellationTokenSource();
        //                // Send the Message and wait synchronously:
        //                result = client.SendAsync(message, cts.Token).GetAwaiter().GetResult();
        //            }
        //        }
        //        // Print the Result to the Console:
        //        return Ok(result);
        //    }
        //}

        [HttpPost, Route("Send"), MapToApiVersion("1.0")]
        public async Task<ProcessResultViewModel<string>> Send([FromBody]SendNotificationViewModel model)
        {
            string result = "";
            var apiKey = _configuration["FcmSettings:ApiKey"];
            var userDevices = await _identityService.GetUserDevices("v1", model.UserId);
            foreach (var item in userDevices.Data)
            {
                result = SendPushNotification(item.DeveiceId, model.Title, model.Body, model.Data, apiKey);
            }
            return ProcessResultViewModelHelper.Succedded(result);
        }

        [HttpPost, Route("SendToDevice"), MapToApiVersion("1.0")]
        public ProcessResultViewModel<string> SendToDevice([FromBody]SendNotificationViewModel model)
        {
            string result = "";
            var apiKey = _configuration["FcmSettings:ApiKey"];
            result = SendPushNotification(model.DeviceId, model.Title, model.Body, model.Data, apiKey);
            return ProcessResultViewModelHelper.Succedded(result);
        }

        public static string SendPushNotification(string deviceId, string title, string body,
            dynamic dataObj, string ApplicationId)
        {
            string str;
            try
            {
                WebRequest tRequest = WebRequest.Create("https://fcm.googleapis.com/fcm/send");
                tRequest.Method = "post";
                tRequest.ContentType = "application/json";
                var data = new
                {
                    to = deviceId,
                    notification = new
                    {
                        body,
                        title,
                        sound = "Enabled"
                    },
                    data = dataObj
                };
                var json = JsonConvert.SerializeObject(data);
                Byte[] byteArray = Encoding.UTF8.GetBytes(json);
                tRequest.Headers.Add(string.Format("Authorization: key={0}", ApplicationId));
                tRequest.ContentLength = byteArray.Length;
                using (Stream dataStream = tRequest.GetRequestStream())
                {
                    dataStream.Write(byteArray, 0, byteArray.Length);
                    using (WebResponse tResponse = tRequest.GetResponse())
                    {
                        using (Stream dataStreamResponse = tResponse.GetResponseStream())
                        {
                            using (StreamReader tReader = new StreamReader(dataStreamResponse))
                            {
                                String sResponseFromServer = tReader.ReadToEnd();
                                str = sResponseFromServer;
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                str = ex.Message;
            }
            return str;
        }
    }
}
