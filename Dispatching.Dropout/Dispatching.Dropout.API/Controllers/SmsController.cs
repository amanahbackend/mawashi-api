﻿using Dispatching.Dropout.API.ViewModels;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using Utilites.ProcessingResult;

namespace Dispatching.Dropout.API.Controllers
{
    [ApiVersion("1.0")]
    [Route("v{version:apiVersion}/[controller]")]
    public class SmsController : Controller
    {
        public SmsController()
        {
        }

        [HttpPost, Route("SendKW"), MapToApiVersion("1.0")]
        public ProcessResultViewModel<bool> SendKW([FromBody] SendSmsViewModel model)
        {
            var smsUrl = string.Format("http://smsbox.com/smsgateway/services/messaging.asmx/Http_SendSMS?username=alghanim&password=alghanim@321&customerid=994&sendertext=A ENG&messagebody={0}&recipientnumbers={1}&defdate=&isblink=false&isflash=false", model.Message, model.PhoneNumber);

            WebRequest request = WebRequest.Create(smsUrl);
            request.Method = "POST";
            request.ContentLength = 0;
            WebResponse response = request.GetResponse();
            return ProcessResultViewModelHelper.Succedded(true);
        }

        [HttpPost, Route("SendUAE"), MapToApiVersion("1.0")]
        public ProcessResultViewModel<bool> SendUAE([FromBody] SendSmsViewModel model)
        {
            string dateTimeNow = DateTime.UtcNow.ToString("yyyy-MM-dd HH:mm:ss");
            string smsUrl = $"https://smartsmsgateway.com/api/api_http.php?username=mashawilivestock&password=mls01&senderid=Al%20Mawashi&to={model.PhoneNumber}&text={model.Message}&type=text&datetime={dateTimeNow}";
            //var smsUrl = string.Format("https://smartsmsgateway.com/api/api_http.php?username=mashawilivestock&password=mls01&senderid=Al%20Mawashi&to={0}&text={1}&type=text&datetime={2}", model.Message, model.PhoneNumber,dateTimeNow);

            WebRequest request = WebRequest.Create(smsUrl);
            request.Method = "GET";
            request.ContentLength = 0;
            
            WebResponse response = request.GetResponse();
            return ProcessResultViewModelHelper.Succedded(true);
        }
    }
}
