﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using BuildingBlocks.ServiceDiscovery;
using DispatchProduct.HttpClient;
using DnsClient;

namespace Dispatching.Dropout.API.ServiceSettings
{
    public class IdentityServiceSettings : DefaultHttpClientSettings
    {
        public string GetUserDevicesAction { get; set; }

        public async Task<string> GetBaseUrl(IDnsQuery dnsQuery)
        {
            return await ServiceDiscoveryHelper.GetServiceUrl(dnsQuery, ServiceName);
        }
    }
}
