﻿using Dispatching.CustomerModule.BLL.IManagers;
using Dispatching.CustomerModule.Models.Context;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Dispatching.CustomerModule.API.Seed
{
    public class DataContextSeed
    {
        private readonly ICustomerManager _customerManager;
        public DataContextSeed(ICustomerManager customerManager)
        {
            _customerManager = customerManager;
        }

        public void Seed(DataContext context, ILogger<DataContextSeed> logger, int? retry = 0)
        {
            int retryForAvaiability = retry.Value;
            try
            {
                //if (!context.Lkp_Countries.Any())
                //{
                //    _countryManager.Seed();
                //}
                //if (!context.Lkp_AddressTypes.Any())
                //{
                //    _addressTypeManager.Seed();
                //}
            }
            catch (Exception ex)
            {
                if (retryForAvaiability < 10)
                {
                    retryForAvaiability++;

                    logger.LogError(ex.Message, $"There is an error migrating data for Customer DataContext");

                    Seed(context, logger, retryForAvaiability);
                }
            }
        }
    }
}
