﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Serilog;
using Utilities.Utilites.SerilogExtensions;
using Microsoft.Extensions.DependencyInjection;
using Dispatching.CustomerModule.API.Seed;
using Dispatching.CustomerModule.BLL.IManagers;
using Dispatching.CustomerModule.Models.Context;
namespace Dispatching.CustomerModule.API
{
    public class Program
    {
        public static void Main(string[] args)
        {
            //BuildWebHost(args).Run();

            Log.Logger = new LoggerConfiguration()
                .Enrich.With<CustomExceptionEnricher>()
                .MinimumLevel.Error()
                .Enrich.FromLogContext()
                .WriteTo.File("logs/log_.csv", rollingInterval: RollingInterval.Day,
                outputTemplate: "{Timestamp:yyyy-MM-dd HH:mm:ss.fff zzz},[{Level}],{Message},{ExceptionMessage},{ExceptionSource},{ExceptionType},{ExceptionStackTrace},{NewLine}")
                .CreateLogger();

            BuildWebHost(args)
                .MigrateDbContext<DataContext>((context, services) =>
                {
                    var logger = services.GetService<ILogger<DataContextSeed>>();
                    var customerManager = services.GetService<ICustomerManager>();

                    new DataContextSeed(customerManager)
                        .Seed(context, logger);
                }).Run();
        }

        public static IWebHost BuildWebHost(string[] args) =>
          WebHost.CreateDefaultBuilder(args)
              .UseKestrel()
              .UseContentRoot(Directory.GetCurrentDirectory())
              .UseIISIntegration()
              .UseStartup<Startup>()
              .UseSerilog()
              .Build();
    }
}
