﻿using DispatchProduct.BuildingBlocks.BaseEntities.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Dispatching.CustomerModule.API.ViewModels
{
	public class CustomerDeviceViewModel : RepoistryBaseEntity
	{
		public int Fk_Customer_Id { get; set; }
		public string DeviceId { get; set; }
		public string Platform { get; set; }
	}
}
