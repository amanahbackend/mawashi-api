﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Dispatching.CustomerModule.API.ViewModels
{
    public class UpdateCompanyWallet
    {
        public int FK_Company_Id { get; set; }
        public double Wallet { get; set; }
    }
}
