﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Dispatching.Customer.API.ViewModels
{
    public class RegisterCustomerDeviceViewModel
    {
        public int UserId { get; set; }
        public string DeviceId { get; set; }
    }
}
