﻿using DispatchProduct.BuildingBlocks.BaseEntities.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CommonEnums;

namespace Dispatching.CustomerModule.API.ViewModels
{
    public class LocationViewModel : RepoistryBaseEntity
    {
        public AddressType Fk_AddressType_Id { get; set; }
        public string Number { get; set; }
        public Country Fk_Country_Id { get; set; }
        public string Name { get; set; }
        public string GovernorateName { get; set; }
        public string GovernorateId { get; set; }
        public string AreaName { get; set; }
        public string AreaId { get; set; }

        public string BlockId { get; set; }
        public string BlockName { get; set; }

        public string StreetId { get; set; }
        public string StreetName { get; set; }
        public string Building { get; set; }
        public string Floor { get; set; }
        public string AppartmentNumber { get; set; }
        public double? Latitude { get; set; }
        public double? Longitude { get; set; }
        public string DeliveryNote { get; set; }
        public int Fk_Customer_Id { get; set; }
    }
}
