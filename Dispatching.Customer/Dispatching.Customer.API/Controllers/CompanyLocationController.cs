﻿using Dispatching.CustomerModule.API.ViewModels;
using Dispatching.CustomerModule.BLL.IManagers;
using Dispatching.CustomerModule.Models.Context;
using DispatchProduct.Controllers.V1;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Utilites.ProcessingResult;
using Microsoft.AspNetCore.Mvc;
using Dispatching.CustomerModule.API.ServiceCommunications.Location;
using Microsoft.Extensions.DependencyInjection;
using CommonEnums;
using Dispatching.CustomerModule.API.ServiceCommunications.Identity;
using Microsoft.AspNetCore.Http;
namespace Dispatching.CustomerModule.API.Controllers
{
    [ApiVersion("1.0")]
    [Route("v{version:apiVersion}/[controller]")]
    public class CompanyLocationController : BaseControllerV1<ICompanyLocationManager, CompanyLocation, CompanyLocationViewModel>
    {
        ILocationService _locationService;
        IServiceProvider _serviceProvider;
        private readonly IMapper _mapper;
        IProcessResultPaginatedMapper _processResultPaginatedMapper;
        ICompanyLocationManager _manger;
        IProcessResultMapper _processResultMapper;
        public CompanyLocationController(IMapper mapper, IServiceProvider serviceProvider, ICompanyLocationManager manger,
            IProcessResultMapper processResultMapper, IProcessResultPaginatedMapper processResultPaginatedMapper,
            ILocationService locationService)
            : base(manger, mapper, processResultMapper, processResultPaginatedMapper)
        {
            _mapper = mapper;
            _serviceProvider = serviceProvider;
            _manger = manger;
            _processResultMapper = processResultMapper;
            _processResultPaginatedMapper = processResultPaginatedMapper;
            _locationService = locationService;

        }

        [HttpPost, Route("AddAddress"), MapToApiVersion("1.0")]
        public async Task<ProcessResultViewModel<CompanyLocationViewModel>> AddAddress([FromBody] CompanyLocationViewModel model)
        {
            if (model.Fk_Country_Id == Country.Kuwait &&
                String.IsNullOrEmpty(model.Number) &&
                model.Latitude == null && model.Longitude == null)
            {
                var streetPtResult = await _locationService.GetStreetCoords("v1", model.StreetName, model.BlockName);
                if (streetPtResult.IsSucceeded)
                {
                    model.Latitude = streetPtResult.Data.Latitude;
                    model.Longitude = streetPtResult.Data.Longitude;
                }
                return base.Post(model);
            }
            else
            {
                return base.Post(model);
            }
        }

        [HttpPut, Route("EditAddress"), MapToApiVersion("1.0")]
        public async Task<ProcessResultViewModel<bool>> EditAddress([FromBody] CompanyLocationViewModel model)
        {
            if (model.Fk_Country_Id == Country.Kuwait &&
                String.IsNullOrEmpty(model.Number) &&
                model.Latitude == null && model.Longitude == null)
            {
                var streetPtResult = await _locationService.GetStreetCoords("v1", model.StreetName, model.BlockName);
                if (streetPtResult.IsSucceeded)
                {
                    model.Latitude = streetPtResult.Data.Latitude;
                    model.Longitude = streetPtResult.Data.Longitude;
                }
                return base.Put(model);
            }
            else
            {
                return base.Put(model);
            }
        }

        [HttpGet, Route("GetByCompanyId"), MapToApiVersion("1.0")]
        public ProcessResultViewModel<List<CompanyLocationViewModel>> GetByCompanyId([FromQuery]int companyId, Country countryId)
        {
            try
            {
                var result = manger.GetByCompanyId(companyId, countryId);
                if (result.IsSucceeded)
                {
                    var models = mapper.Map<List<CompanyLocation>, List<CompanyLocationViewModel>>(result.Data);
                    return ProcessResultViewModelHelper.Succedded(models);
                }
                else
                {
                    return ProcessResultViewModelHelper.Failed<List<CompanyLocationViewModel>>(null, result.Status.Message);
                }
            }
            catch (Exception ex)
            {
                return ProcessResultViewModelHelper.Failed<List<CompanyLocationViewModel>>(null, ex.Message);
            }
        }

        [HttpDelete, Route("DeleteAddress/{id}"), MapToApiVersion("1.0")]
        public ProcessResultViewModel<bool> DeleteAddress(int id)
        {
            return base.Delete(id);
        }
    }
}
