﻿using Dispatching.CustomerModule.API.ViewModels;
using Dispatching.CustomerModule.BLL.IManagers;
using DispatchProduct.Controllers;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Utilites.ProcessingResult;
using Dispatching.CustomerModule.API.ServicesViewModels.Identity;
using Dispatching.CustomerModule.API.ServiceCommunications.Identity;
using Newtonsoft.Json.Linq;
using Dispatching.CustomerModule.Models.Context;
using CommonEnums;
using Microsoft.Net.Http.Headers;
using Dispatching.CustomerModule.API.ServiceCommunications.Dropout;
using System.Net;
using Dispatching.Customer.API.ServicesViewModels.Identity;
using Microsoft.Extensions.Configuration;
using Utilites.PaginatedItems;
using Dispatching.CustomerModule.BLL.Managers;
using Dispatching.Customer.API.ViewModels;

namespace Dispatching.CustomerModule.API.Controllers
{
    [ApiVersion("1.0")]
    [Route("v{version:apiVersion}/[controller]")]
    public class CustomerController
    : BaseController<ICustomerManager, Models.Context.Customer, CustomerViewModel>
    {
        private readonly ICustomerManager _customerManager;
        private readonly IMapper _mapper;
        private readonly IProcessResultMapper _processResultMapper;
        private readonly IProcessResultPaginatedMapper _processResultPaginatedMapper;
        private readonly IIdentityUserService _identityUserService;
        private readonly IDropoutService _dropoutService;
        private readonly IConfigurationRoot _configuration;
        private readonly string _kwPhoneCode = "00965";
        private readonly string _uaePhoneCode = "00971";

        public CustomerController(ICustomerManager _manger, IMapper _mapper,
            IProcessResultMapper _processResultMapper, IProcessResultPaginatedMapper _processResultPaginatedMapper,
            IIdentityUserService identityUserService, IDropoutService dropoutService,
            IConfigurationRoot configuration)
            : base(_manger, _mapper, _processResultMapper, _processResultPaginatedMapper)
        {
            _customerManager = _manger;
            this._mapper = _mapper;
            this._processResultMapper = _processResultMapper;
            this._processResultPaginatedMapper = _processResultPaginatedMapper;
            _identityUserService = identityUserService;
            _dropoutService = dropoutService;
            _configuration = configuration;
        }


        [HttpPost, Route("Register"), MapToApiVersion("1.0")]
        public async Task<ProcessResultViewModel<CustomerViewModel>> Register([FromBody] CustomerViewModel customerModel)
        {
            customerModel.UserName = customerModel.Email;
            switch (customerModel.Fk_Country_Id)
            {
                case Country.Kuwait:
                    if (customerModel.Phone1.Substring(_kwPhoneCode.Length).StartsWith('0'))
                    {
                        customerModel.Phone1 = customerModel.Phone1.Remove(_kwPhoneCode.Length, 1);
                    }
                    break;
                case Country.UAE:
                    if (customerModel.Phone1.Substring(_uaePhoneCode.Length).StartsWith('0'))
                    {
                        customerModel.Phone1 = customerModel.Phone1.Remove(_kwPhoneCode.Length, 1);
                    }
                    break;
                default:
                    break;
            }
            var userModel = mapper.Map<CustomerViewModel, ApplicationUserViewModel>(customerModel);
            userModel.IsBasicRegister = false;
            var addUserResult = await _identityUserService.AddCustomerUser("v1", userModel);

            if (addUserResult.IsSucceeded
                && addUserResult.Data != null
                && !String.IsNullOrEmpty(addUserResult.Data.Id))
            {
                userModel = addUserResult.Data;
                customerModel = mapper.Map<ApplicationUserViewModel, CustomerViewModel>(userModel);
                var result = base.Post(customerModel);
                if (result.IsSucceeded)
                {
                    customerModel.Id = result.Data.Id;
                    bool isVerify = Convert.ToBoolean(_configuration["RegisterConfig:SendVerify"]);
                    if (isVerify)
                    {
                        PhoneValidationViewModel phoneValidationViewModel = new PhoneValidationViewModel
                        {
                            Country = customerModel.Fk_Country_Id,
                            Phone = customerModel.Phone1,
                            Username = customerModel.UserName
                        };
                        await _identityUserService.GetPhoneVerficationToken("v1", phoneValidationViewModel);
                    }
                    return ProcessResultViewModelHelper.Succedded(customerModel);
                }
                else
                {
                    return ProcessResultViewModelHelper.Failed<CustomerViewModel>(null, "Something went wrong while adding customer");
                }
            }
            else
            {
                return ProcessResultViewModelHelper.MapToProcessResultViewModel<ApplicationUserViewModel, CustomerViewModel>(addUserResult);
            }
        }

        [HttpPost, Route("Migrate"), MapToApiVersion("1.0")]
        public async Task<ProcessResultViewModel<CustomerViewModel>> Migrate([FromBody] CustomerViewModel customerModel)
        {
            customerModel.UserName = customerModel.Email;
            switch (customerModel.Fk_Country_Id)
            {
                case Country.Kuwait:
                    if (customerModel.Phone1.Substring(_kwPhoneCode.Length).StartsWith('0'))
                    {
                        customerModel.Phone1 = customerModel.Phone1.Remove(_kwPhoneCode.Length, 1);
                    }
                    break;
                case Country.UAE:
                    if (customerModel.Phone1.Substring(_uaePhoneCode.Length).StartsWith('0'))
                    {
                        customerModel.Phone1 = customerModel.Phone1.Remove(_kwPhoneCode.Length, 1);
                    }
                    break;
                default:
                    break;
            }
            var userModel = mapper.Map<CustomerViewModel, ApplicationUserViewModel>(customerModel);
            userModel.IsBasicRegister = true;
            var addUserResult = await _identityUserService.AddCustomerUser("v1", userModel);

            if (addUserResult.IsSucceeded
                && addUserResult.Data != null
                && !String.IsNullOrEmpty(addUserResult.Data.Id))
            {
                userModel = addUserResult.Data;
                customerModel = mapper.Map<ApplicationUserViewModel, CustomerViewModel>(userModel);
                var result = base.Post(customerModel);
                if (result.IsSucceeded)
                {
                    customerModel.Id = result.Data.Id;
                    bool isVerify = Convert.ToBoolean(_configuration["RegisterConfig:SendVerify"]);
                    if (isVerify)
                    {
                        PhoneValidationViewModel phoneValidationViewModel = new PhoneValidationViewModel
                        {
                            Country = customerModel.Fk_Country_Id,
                            Phone = customerModel.Phone1,
                            Username = customerModel.UserName
                        };
                        await _identityUserService.GetPhoneVerficationToken("v1", phoneValidationViewModel);
                    }
                    return ProcessResultViewModelHelper.Succedded(customerModel);
                }
                else
                {
                    return ProcessResultViewModelHelper.Failed<CustomerViewModel>(null, "Something went wrong while adding customer");
                }
            }
            else
            {
                return ProcessResultViewModelHelper.MapToProcessResultViewModel<ApplicationUserViewModel, CustomerViewModel>(addUserResult);
            }
        }

        [HttpPut, Route("UpdateCustomerUser"), MapToApiVersion("1.0")]
        public async Task<ProcessResultViewModel<CustomerViewModel>> UpdateCustomerUser([FromBody] CustomerViewModel customerModel)
        {
            var customerId = customerModel.Id;
            var customerRes = manger.Get(customerId);
            if (customerRes.IsSucceeded && customerRes.Data != null)
            {
                customerModel.Fk_AppUser_Id = customerRes.Data.Fk_AppUser_Id;
                var userModel = mapper.Map<CustomerViewModel, ApplicationUserViewModel>(customerModel);

                var updateUserResult = await _identityUserService.UpdateCustomerUser("v1", userModel);

                if (updateUserResult.IsSucceeded && updateUserResult.Data != null)
                {
                    userModel = updateUserResult.Data;
                    customerModel = mapper.Map<ApplicationUserViewModel, CustomerViewModel>(userModel);
                    customerModel.Id = customerId;
                    var result = base.Put(customerModel);
                    if (result.IsSucceeded)
                    {
                        return ProcessResultViewModelHelper.Succedded(customerModel);
                    }
                    else
                    {
                        return ProcessResultViewModelHelper.Failed<CustomerViewModel>(null, "Something went wrong while editing customer data");
                    }
                }
                else
                {
                    return ProcessResultViewModelHelper.MapToProcessResultViewModel<ApplicationUserViewModel, CustomerViewModel>(updateUserResult);
                }
            }
            else
            {
                return ProcessResultViewModelHelper.Failed(customerModel, "No customer with this Infos has been Registered to update");
            }
        }

        [HttpPut, Route("UpdateCustomerPhone"), MapToApiVersion("1.0")]
        public async Task<ProcessResultViewModel<CustomerViewModel>> UpdateCustomerPhone([FromBody] CustomerViewModel customerModel)
        {
            switch (customerModel.Fk_Country_Id)
            {
                case Country.Kuwait:
                    if (customerModel.Phone1.Substring(_kwPhoneCode.Length).StartsWith('0'))
                    {
                        customerModel.Phone1 = customerModel.Phone1.Remove(_kwPhoneCode.Length, 1);
                    }
                    break;
                case Country.UAE:
                    if (customerModel.Phone1.Substring(_uaePhoneCode.Length).StartsWith('0'))
                    {
                        customerModel.Phone1 = customerModel.Phone1.Remove(_kwPhoneCode.Length, 1);
                    }
                    break;
                default:
                    break;
            }
            var customerId = customerModel.Id;
            var customerRes = manger.Get(customerId);
            if (customerRes.IsSucceeded && customerRes.Data != null)
            {
                customerModel.Fk_AppUser_Id = customerRes.Data.Fk_AppUser_Id;
                var userModel = mapper.Map<CustomerViewModel, ApplicationUserViewModel>(customerModel);

                var updateUserResult = await _identityUserService.UpdateCustomerPhone("v1", userModel);

                if (updateUserResult.IsSucceeded && updateUserResult.Data != null)
                {
                    userModel = updateUserResult.Data;
                    customerModel = mapper.Map<ApplicationUserViewModel, CustomerViewModel>(userModel);
                    customerModel.Id = customerId;
                    var result = base.Put(customerModel);
                    if (result.IsSucceeded)
                    {
                        return ProcessResultViewModelHelper.Succedded(customerModel);
                    }
                    else
                    {
                        return ProcessResultViewModelHelper.Failed<CustomerViewModel>(null, "Something went wrong while editing customer data");
                    }
                }
                else
                {
                    return ProcessResultViewModelHelper.MapToProcessResultViewModel<ApplicationUserViewModel, CustomerViewModel>(updateUserResult);
                }
            }
            else
            {
                return ProcessResultViewModelHelper.Failed(customerModel, "No customer with this Infos has been Registered to update");
            }
        }

        [HttpPut, Route("UpdateCustomerEmail"), MapToApiVersion("1.0")]
        public async Task<ProcessResultViewModel<CustomerViewModel>> UpdateCustomerEmail([FromBody] CustomerViewModel customerModel)
        {
            var customerId = customerModel.Id;
            var customerRes = manger.Get(customerId);
            if (customerRes.IsSucceeded && customerRes.Data != null)
            {
                customerModel.Fk_AppUser_Id = customerRes.Data.Fk_AppUser_Id;
                var userModel = mapper.Map<CustomerViewModel, ApplicationUserViewModel>(customerModel);

                var updateUserResult = await _identityUserService.UpdateCustomerEmail("v1", userModel);

                if (updateUserResult.IsSucceeded && updateUserResult.Data != null)
                {
                    userModel = updateUserResult.Data;
                    customerModel = mapper.Map<ApplicationUserViewModel, CustomerViewModel>(userModel);
                    customerModel.Id = customerId;
                    var result = base.Put(customerModel);
                    if (result.IsSucceeded)
                    {
                        return ProcessResultViewModelHelper.Succedded(customerModel);
                    }
                    else
                    {
                        return ProcessResultViewModelHelper.Failed<CustomerViewModel>(null, "Something went wrong while editing customer data");
                    }
                }
                else
                {
                    return ProcessResultViewModelHelper.MapToProcessResultViewModel<ApplicationUserViewModel, CustomerViewModel>(updateUserResult);
                }
            }
            else
            {
                return ProcessResultViewModelHelper.Failed(customerModel, "No customer with this Infos has been Registered to update");
            }
        }

        [HttpPut, Route("UpdateCustomerPicture"), MapToApiVersion("1.0")]
        public async Task<ProcessResultViewModel<CustomerViewModel>> UpdateCustomerPicture([FromBody] CustomerViewModel customerModel)
        {
            var customerId = customerModel.Id;
            var customerRes = manger.Get(customerId);
            if (customerRes.IsSucceeded && customerRes.Data != null)
            {
                customerModel.Fk_AppUser_Id = customerRes.Data.Fk_AppUser_Id;
                var userModel = mapper.Map<CustomerViewModel, ApplicationUserViewModel>(customerModel);

                var updateUserResult = await _identityUserService.UpdateCustomerPicture("v1", userModel);

                if (updateUserResult.IsSucceeded && updateUserResult.Data != null)
                {
                    userModel = updateUserResult.Data;
                    customerModel = mapper.Map<ApplicationUserViewModel, CustomerViewModel>(userModel);
                    customerModel.Id = customerId;
                    var result = base.Put(customerModel);
                    if (result.IsSucceeded)
                    {
                        return ProcessResultViewModelHelper.Succedded(customerModel);
                    }
                    else
                    {
                        return ProcessResultViewModelHelper.Failed<CustomerViewModel>(null, "Something went wrong while editing customer data");
                    }
                }
                else
                {
                    return ProcessResultViewModelHelper.MapToProcessResultViewModel<ApplicationUserViewModel, CustomerViewModel>(updateUserResult);
                }
            }
            else
            {
                return ProcessResultViewModelHelper.Failed(customerModel, "No customer with this Infos has been Registered to update");
            }
        }

        [HttpGet, Route("GetByUserId"), MapToApiVersion("1.0")]
        public async Task<ProcessResultViewModel<CustomerViewModel>> GetByUserId([FromQuery]string userId)
        {
            ProcessResultViewModel<CustomerViewModel> result = null;
            try
            {
                var customerEntityResult = _customerManager.GetByUserId(userId);
                if (customerEntityResult.IsSucceeded)
                {
                    CustomerViewModel customerModel = mapper.Map<Models.Context.Customer, CustomerViewModel>(customerEntityResult.Data);
                    var customerId = customerModel.Id;
                    var userResult = await _identityUserService.GetById("v1", userId);
                    if (userResult.IsSucceeded)
                    {
                        customerModel = mapper.Map<ApplicationUserViewModel, CustomerViewModel>(userResult.Data);
                        customerModel.Id = customerId;
                        result = ProcessResultViewModelHelper.Succedded(customerModel);
                    }
                    else
                    {
                        result = ProcessResultViewModelHelper.Failed<CustomerViewModel>(null, "Failed to get user data");
                    }
                }
                else
                {
                    result = ProcessResultHelper.MapProcessResult<Models.Context.Customer, CustomerViewModel>(customerEntityResult);
                }
            }
            catch (Exception ex)
            {
                result = ProcessResultViewModelHelper.Failed<CustomerViewModel>(null, ex.Message);
            }
            return result;
        }

        [HttpGet, Route("GetById"), MapToApiVersion("1.0")]
        public async Task<ProcessResultViewModel<CustomerViewModel>> GetById([FromQuery]int id)
        {
            ProcessResultViewModel<CustomerViewModel> result = null;
            try
            {
                var customerEntityResult = _customerManager.Get(id);
                if (customerEntityResult.IsSucceeded && customerEntityResult.Data != null)
                {
                    CustomerViewModel customerModel = mapper.Map<Models.Context.Customer, CustomerViewModel>(customerEntityResult.Data);
                    var customerId = customerModel.Id;
                    var userResult = await _identityUserService.GetById("v1", customerModel.Fk_AppUser_Id);
                    if (userResult.IsSucceeded)
                    {
                        customerModel = mapper.Map<ApplicationUserViewModel, CustomerViewModel>(userResult.Data);
                        customerModel.Id = customerId;
                        result = ProcessResultViewModelHelper.Succedded(customerModel);
                    }
                    else
                    {
                        result = ProcessResultViewModelHelper.Failed<CustomerViewModel>(null, "Failed to get user data");
                    }
                }
                else
                {
                    result = ProcessResultHelper.MapProcessResult<Models.Context.Customer, CustomerViewModel>(customerEntityResult);
                }
            }
            catch (Exception ex)
            {
                result = ProcessResultViewModelHelper.Failed<CustomerViewModel>(null, ex.Message);
            }
            return result;
        }

        [HttpPost, Route("GetByCustomerIds"), MapToApiVersion("1.0")]
        public async Task<ProcessResultViewModel<List<CustomerViewModel>>> GetByCustomerIds([FromBody]List<int> ids)
        {
            List<CustomerViewModel> data = new List<CustomerViewModel>();
            foreach (var id in ids)
            {
                var customerRes = await GetById(id);
                if (customerRes.IsSucceeded && customerRes.Data != null)
                {
                    data.Add(customerRes.Data);
                }
                else
                {
                    return ProcessResultViewModelHelper.MapToProcessResultViewModel<CustomerViewModel, List<CustomerViewModel>>(customerRes);
                }
            }
            return ProcessResultViewModelHelper.Succedded(data);
        }

        [HttpDelete, Route("DeleteCustomer"), MapToApiVersion("1.0")]
        public async Task<ProcessResultViewModel<bool>> DeleteCustomer([FromQuery] int id)
        {
            ProcessResultViewModel<bool> result = null;
            try
            {
                var customerEntityResult = _customerManager.Get(id);

                if (customerEntityResult.IsSucceeded &&
                    customerEntityResult.Data != null)
                {
                    await _identityUserService.DeleteById("v1", customerEntityResult.Data.Fk_AppUser_Id);
                    result = Delete(id);
                }
                else
                {
                    result = ProcessResultViewModelHelper.Failed(false, "Error in finding customer");
                }
            }
            catch (Exception ex)
            {
                result = ProcessResultViewModelHelper.Failed(false, ex.Message);
            }
            return result;
        }

        [HttpPost, Route("Login"), MapToApiVersion("1.0")]
        public async Task<IActionResult> Login([FromBody]LoginViewModel loginViewModel)
        {
            var loginResult = await _identityUserService.Login("v1", loginViewModel);
            if (loginResult.Equals(HttpStatusCode.Unauthorized))
            {
                return Unauthorized();
            }
            string[] roles = loginResult.roles.ToObject<string[]>();
            if (roles.Contains("Customer"))
            {
                string userId = loginResult.userId;
                var result = manger.GetByUserId(userId);
                var customerId = result.Data.Id;

                return Ok(new
                {
                    loginResult.token,
                    loginResult.userName,
                    loginResult.roles,
                    loginResult.name,
                    UserId = customerId,
                    loginResult.fk_Country_Id,
                    loginResult.language
                });
            }
            else
            {
                return Ok("The username or password is incorrect");
            }
        }

        [HttpPost, Route("RegisterDevice"), MapToApiVersion("1.0")]
        public async Task<IActionResult> RegisterDevice([FromBody]RegisterCustomerDeviceViewModel model)
        {
            var appUserId = Get(model.UserId).Data.Fk_AppUser_Id;
            await _identityUserService.RegisterDevice("v1", new RegisterDeviceViewModel
            {
                UserId = appUserId,
                DeviceId = model.DeviceId
            });
            return Ok();
        }

        [HttpGet, Route("GetAllCustomers"), MapToApiVersion("1.0")]
        public async Task<ProcessResultViewModel<List<CustomerViewModel>>> GetAllCustomers()
        {
            ProcessResultViewModel<List<CustomerViewModel>> result = null;
            try
            {
                var customerModelsResult = base.Get();
                if (customerModelsResult.IsSucceeded && customerModelsResult.Data.Count > 0)
                {

                    var ids = customerModelsResult.Data.Select(c => c.Fk_AppUser_Id).ToList();
                    var users = await _identityUserService.GetByUserIds("v1", ids);
                    if (users.IsSucceeded)
                    {
                        var customers = Mapper.Map<List<ApplicationUserViewModel>, List<CustomerViewModel>>(users.Data);
                        foreach (var item in customers)
                        {
                            item.Id = customerModelsResult.Data.FirstOrDefault(c => c.Fk_AppUser_Id == item.Fk_AppUser_Id).Id;
                        }
                        result = ProcessResultViewModelHelper.Succedded(customers);
                    }
                    else
                    {
                        result = customerModelsResult;
                    }
                }
                else
                {
                    result = customerModelsResult;
                }
            }
            catch (Exception ex)
            {
                result = ProcessResultViewModelHelper.Failed<List<CustomerViewModel>>(null, ex.Message);
            }
            return result;
        }

        [HttpGet, Route("GetAllCustomersCount"), MapToApiVersion("1.0")]
        public async Task<ProcessResultViewModel<List<CustomerCountPerCountryViewModel>>> GetAllCustomersCount()
        {
            ProcessResultViewModel<List<CustomerCountPerCountryViewModel>> result = null;
            try
            {
                var customerModelsResult = manger.GetCountPerCountry();
                if (customerModelsResult.IsSucceeded)
                {
                    var models = customerModelsResult.Data;
                    result = ProcessResultViewModelHelper.Succedded(models);
                }
                else
                {
                    result = ProcessResultViewModelHelper.Failed<List<CustomerCountPerCountryViewModel>>(null, customerModelsResult.Status.Message);
                }
            }
            catch (Exception ex)
            {
                result = ProcessResultViewModelHelper.Failed<List<CustomerCountPerCountryViewModel>>(null, ex.Message);
            }
            return result;
        }

        [HttpPost, Route("GetAllCustomersWithPaging/{country}"), MapToApiVersion("1.0")]
        public async Task<ProcessResultViewModel<IdentityPaginatedItems<CustomerViewModel>>>
            GetAllCustomersWithPaging([FromRoute] Country country, [FromBody] IdentityPaginatedItems<CustomerViewModel> paginatedItems)
        {
            ProcessResultViewModel<IdentityPaginatedItems<CustomerViewModel>> result = null;
            try
            {
                var model = new IdentityPaginatedItems<ApplicationUserViewModel>
                {
                    PageSize = paginatedItems.PageSize,
                    PageNo = paginatedItems.PageNo
                };
                model = await _identityUserService.GetCustomerUsersPaginatedByCountry("v1", country, model);
                paginatedItems.Data = mapper.Map<List<ApplicationUserViewModel>, List<CustomerViewModel>>(model.Data);
                paginatedItems.PageNo = model.PageNo;
                paginatedItems.PageSize = model.PageSize;
                paginatedItems.Count = model.Count;
                result = ProcessResultViewModelHelper.Succedded(paginatedItems);
            }
            catch (Exception ex)
            {
                result = ProcessResultViewModelHelper.Failed<IdentityPaginatedItems<CustomerViewModel>>(null, ex.Message);
            }
            return result;
        }

        [HttpGet, Route("GetAllCustomers/{country}"), MapToApiVersion("1.0")]
        public async Task<ProcessResultViewModel<List<CustomerViewModel>>> GetAllCustomers([FromRoute] Country country)
        {
            ProcessResultViewModel<List<CustomerViewModel>> result = null;
            try
            {
                var customerModelsResult = base.Get();
                if (customerModelsResult.IsSucceeded && customerModelsResult.Data.Count > 0)
                {
                    var ids = customerModelsResult.Data.Select(c => c.Fk_AppUser_Id).ToList();
                    var users = await _identityUserService.GetByUserIds("v1", ids);
                    if (users.IsSucceeded && users.Data != null)
                    {
                        var countryUsers = users.Data.Where(u => u.Fk_Country_Id == country).ToList();
                        var customers = Mapper.Map<List<ApplicationUserViewModel>, List<CustomerViewModel>>(countryUsers);
                        foreach (var item in customers)
                        {
                            item.Id = customerModelsResult.Data.FirstOrDefault(c => c.Fk_AppUser_Id == item.Fk_AppUser_Id).Id;
                        }
                        result = ProcessResultViewModelHelper.Succedded(customers);
                    }
                    else
                    {
                        result = customerModelsResult;
                    }
                }
                else
                {
                    result = customerModelsResult;
                }
            }
            catch (Exception ex)
            {
                result = ProcessResultViewModelHelper.Failed<List<CustomerViewModel>>(null, ex.Message);
            }
            return result;
        }


        [HttpGet, Route("SearchUAECustomers"), MapToApiVersion("1.0")]
        public async Task<ProcessResultViewModel<List<CustomerViewModel>>> SearchUAECustomers([FromQuery]string searchToken)
        {
            ProcessResultViewModel<List<CustomerViewModel>> result = null;
            try
            {
                var usersResult = await _identityUserService.Search("v1", searchToken);
                if (usersResult.IsSucceeded)
                {
                    if (usersResult.Data != null && usersResult.Data.Count > 0)
                    {
                        var uaeCustomerUsers = usersResult.Data.Where(u => u.Fk_Country_Id == Country.UAE &&
                                                                           u.RoleNames.Contains("Customer")).ToList();
                        var customers = mapper.Map<List<CustomerViewModel>>(uaeCustomerUsers);
                        foreach (var item in customers)
                        {
                            var customer = manger.GetByUserId(item.Fk_AppUser_Id);
                            item.Id = customer.IsSucceeded && customer.Data != null ? customer.Data.Id : 0;
                        }
                        result = ProcessResultViewModelHelper.Succedded(customers);
                    }
                    else
                    {
                        result = ProcessResultViewModelHelper.Succedded<List<CustomerViewModel>>(null, "No customers found.");
                    }
                }
                else
                {
                    result = ProcessResultViewModelHelper.MapToProcessResultViewModel<List<ApplicationUserViewModel>, List<CustomerViewModel>>(usersResult);
                }
            }
            catch (Exception ex)
            {
                result = ProcessResultViewModelHelper.Failed<List<CustomerViewModel>>(null, ex.Message);
            }
            return result;
        }

        [HttpGet, Route("SearchKWCustomers"), MapToApiVersion("1.0")]
        public async Task<ProcessResultViewModel<List<CustomerViewModel>>> SearchKWCustomers([FromQuery]string searchToken)
        {
            ProcessResultViewModel<List<CustomerViewModel>> result = null;
            try
            {
                var usersResult = await _identityUserService.Search("v1", searchToken);
                if (usersResult.IsSucceeded)
                {
                    if (usersResult.Data != null && usersResult.Data.Count > 0)
                    {
                        var uaeCustomerUsers = usersResult.Data.Where(u => u.Fk_Country_Id == Country.Kuwait &&
                                                                           u.RoleNames.Contains("Customer")).ToList();
                        var customers = mapper.Map<List<CustomerViewModel>>(uaeCustomerUsers);
                        foreach (var item in customers)
                        {
                            var customer = manger.GetByUserId(item.Fk_AppUser_Id);
                            item.Id = customer.IsSucceeded && customer.Data != null ? customer.Data.Id : 0;
                        }
                        result = ProcessResultViewModelHelper.Succedded(customers);
                    }
                    else
                    {
                        result = ProcessResultViewModelHelper.Succedded<List<CustomerViewModel>>(null, "No customers found.");
                    }
                }
                else
                {
                    result = ProcessResultViewModelHelper.MapToProcessResultViewModel<List<ApplicationUserViewModel>, List<CustomerViewModel>>(usersResult);
                }
            }
            catch (Exception ex)
            {
                result = ProcessResultViewModelHelper.Failed<List<CustomerViewModel>>(null, ex.Message);
            }
            return result;
        }

        [HttpGet, Route("UpdateCustomerCountry"), MapToApiVersion("1.0")]
        public async Task<ProcessResultViewModel<CustomerViewModel>> UpdateCustomerCountry([FromQuery]Country countryId)
        {
            ProcessResultViewModel<CustomerViewModel> result = null;
            try
            {
                string authorizationValue = HttpContext.Request.Headers[HeaderNames.Authorization];
                string token = authorizationValue.Substring("Bearer ".Length).Trim();
                var userResult = await _identityUserService.UpdateUserCountry("v1", countryId, token);
                if (userResult.IsSucceeded && userResult.Data != null)
                {
                    var customer = mapper.Map<CustomerViewModel>(userResult.Data);
                    var customerObj = manger.GetByUserId(customer.Fk_AppUser_Id);
                    customer.Id = customerObj.IsSucceeded && customerObj.Data != null ? customerObj.Data.Id : 0;
                    result = ProcessResultViewModelHelper.Succedded(customer);
                }
                else
                {
                    result = ProcessResultViewModelHelper.MapToProcessResultViewModel<ApplicationUserViewModel, CustomerViewModel>(userResult);
                }
            }
            catch (Exception ex)
            {
                result = ProcessResultViewModelHelper.Failed<CustomerViewModel>(null, ex.Message);
            }
            return result;
        }

        [HttpGet, Route("UpdateCustomerLanguage"), MapToApiVersion("1.0")]
        public async Task<ProcessResultViewModel<CustomerViewModel>> UpdateCustomerLanguage([FromQuery]SupportedLanguage languageId)
        {
            ProcessResultViewModel<CustomerViewModel> result = null;
            try
            {
                string authorizationValue = HttpContext.Request.Headers[HeaderNames.Authorization];
                string token = authorizationValue.Substring("Bearer ".Length).Trim();
                var userResult = await _identityUserService.UpdateUserLanguage("v1", languageId, token);
                if (userResult.IsSucceeded && userResult.Data != null)
                {
                    var customer = mapper.Map<CustomerViewModel>(userResult.Data);
                    var customerObj = manger.GetByUserId(customer.Fk_AppUser_Id);
                    customer.Id = customerObj.IsSucceeded && customerObj.Data != null ? customerObj.Data.Id : 0;
                    result = ProcessResultViewModelHelper.Succedded(customer);
                }
                else
                {
                    result = ProcessResultViewModelHelper.MapToProcessResultViewModel<ApplicationUserViewModel, CustomerViewModel>(userResult);
                }
            }
            catch (Exception ex)
            {
                result = ProcessResultViewModelHelper.Failed<CustomerViewModel>(null, ex.Message);
            }
            return result;
        }

        [HttpPost, Route("BasicRegister"), MapToApiVersion("1.0")]
        public async Task<ProcessResultViewModel<CustomerViewModel>> BasicRegister([FromBody] CustomerViewModel customerModel)
        {
            customerModel = FillNonRequiredFields(customerModel);
            var userModel = mapper.Map<CustomerViewModel, ApplicationUserViewModel>(customerModel);
            userModel.IsBasicRegister = true;
            var addUserResult = await _identityUserService.AddCustomerUser("v1", userModel);

            if (addUserResult.IsSucceeded
                && addUserResult.Data != null
                && !String.IsNullOrEmpty(addUserResult.Data.Id))
            {
                userModel = addUserResult.Data;
                customerModel = mapper.Map<ApplicationUserViewModel, CustomerViewModel>(userModel);
                var result = base.Post(customerModel);
                if (result.IsSucceeded)
                {
                    customerModel.Id = result.Data.Id;
                    return ProcessResultViewModelHelper.Succedded(customerModel);
                }
                else
                {
                    return ProcessResultViewModelHelper.Failed<CustomerViewModel>(null, "Something went wrong while adding customer");
                }
            }
            else
            {
                return ProcessResultViewModelHelper.MapToProcessResultViewModel<ApplicationUserViewModel, CustomerViewModel>(addUserResult);
            }
        }

        private CustomerViewModel FillNonRequiredFields(CustomerViewModel model)
        {
            if (String.IsNullOrEmpty(model.Email))
            {
                model.Email = $"{model.FirstName}{model.LastName}{model.Phone1}@generatedmail.com";
            }
            if (String.IsNullOrEmpty(model.Password))
            {
                model.Password = "#$%@!)P@ssw0rd(2922G";
            }
            if (String.IsNullOrEmpty(model.UserName))
            {
                model.UserName = model.Email;
            }
            return model;
        }
    }
}