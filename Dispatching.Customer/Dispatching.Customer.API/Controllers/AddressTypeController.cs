﻿using System;
using System.Collections.Generic;
using System.Text;
using DispatchProduct.RepositoryModule;
using Microsoft.AspNetCore.Mvc;
using DispatchProduct.Controllers;
using AutoMapper;
using Utilites.ProcessingResult;
using DispatchProduct.Controllers.V1;
using Microsoft.Extensions.DependencyInjection;
using CommonEnums;

namespace Dispatching.Ordering.API.Controllers
{
    [ApiVersion("1.0")]
    [Route("v{version:apiVersion}/[controller]")]
    public class AddressTypeController : Controller
    {
        [Route("GetAll")]
        [HttpGet]
        public List<EnumEntity> GetAll()
        {
           return  EnumManager<AddressType>.GetEnumList();
        }
    }
}

