﻿using AutoMapper;
using Dispatching.CustomerModule.API.ViewModels;
using Dispatching.CustomerModule.BLL.IManagers;
using DispatchProduct.Controllers.V1;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Utilites.ProcessingResult;
using Dispatching.CustomerModule.Models.Context;
namespace Dispatching.CustomerModule.API.Controllers
{
	[ApiVersion("1.0")]
	[Route("v{version:apiVersion}/[controller]")]
	public class CustomerDeviceController
		: BaseControllerV1<ICustomerDeviceManager, CustomerDevice, CustomerDeviceViewModel>
	{
		public CustomerDeviceController(ICustomerDeviceManager _manger, IMapper _mapper, IProcessResultMapper _processResultMapper, IProcessResultPaginatedMapper _processResultPaginatedMapper)
			: base(_manger, _mapper, _processResultMapper, _processResultPaginatedMapper)
		{
		}

		[HttpGet, Route("GetCustomerDevices"), MapToApiVersion("1.0")]
		public ProcessResultViewModel<List<CustomerDeviceViewModel>> GetCustomerDevices([FromQuery]int customerId)
		{
			ProcessResultViewModel<List<CustomerDeviceViewModel>> result = null;
			try
			{
				var customerDevicesResult = manger.GetCustomerDevices(customerId);
				if (customerDevicesResult.IsSucceeded)
				{
					var models = mapper.Map<List<CustomerDevice>, List<CustomerDeviceViewModel>>(customerDevicesResult.Data);
					result = ProcessResultViewModelHelper.Succedded(models);
				}
				else
				{
					result = ProcessResultViewModelHelper.Failed<List<CustomerDeviceViewModel>>(null);
				}
			}
			catch (Exception ex)
			{
				result = ProcessResultViewModelHelper.Failed<List<CustomerDeviceViewModel>>(null, ex.Message);
			}
			return result;
		}
	}
}
