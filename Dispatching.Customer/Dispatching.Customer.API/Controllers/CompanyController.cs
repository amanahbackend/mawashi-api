﻿using Dispatching.CustomerModule.API.ViewModels;
using Dispatching.CustomerModule.BLL.IManagers;
using DispatchProduct.Controllers;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Utilites.ProcessingResult;
using Dispatching.CustomerModule.API.ServicesViewModels.Identity;
using Dispatching.CustomerModule.API.ServiceCommunications.Identity;
using Dispatching.CompanyModule.BLL.IManagers;
using Dispatching.CustomerModule.Models.Context;
using CommonEnums;
using Microsoft.Net.Http.Headers;
using Utilites.PaginatedItems;

namespace Dispatching.CustomerModule.API.Controllers
{
    [ApiVersion("1.0")]
    [Route("v{version:apiVersion}/[controller]")]
    public class CompanyController
    : BaseController<ICompanyManager, Company, CompanyViewModel>
    {
        private readonly ICompanyManager _CompanyManager;
        private readonly IMapper _mapper;
        private readonly IProcessResultMapper _processResultMapper;
        private readonly IProcessResultPaginatedMapper _processResultPaginatedMapper;
        private readonly IIdentityUserService _identityUserService;

        public CompanyController(ICompanyManager _manger, IMapper _mapper,
            IProcessResultMapper _processResultMapper, IProcessResultPaginatedMapper _processResultPaginatedMapper,
            IIdentityUserService identityUserService)
            : base(_manger, _mapper, _processResultMapper, _processResultPaginatedMapper)
        {
            this._CompanyManager = _manger;
            this._mapper = _mapper;
            this._processResultMapper = _processResultMapper;
            this._processResultPaginatedMapper = _processResultPaginatedMapper;
            _identityUserService = identityUserService;
        }

        [HttpGet, Route("GetAllCompanies/{country}"), MapToApiVersion("1.0")]
        public async Task<ProcessResultViewModel<List<CompanyViewModel>>> GetAllCompanies([FromRoute] Country country)
        {
            ProcessResultViewModel<List<CompanyViewModel>> result = null;
            try
            {
                var companyResult = _CompanyManager.GetByCountry(country);
                var companyModelsResult = _processResultMapper.Map<List<Company>, List<CompanyViewModel>>(companyResult);
                if (companyModelsResult.IsSucceeded && companyModelsResult.Data.Count > 0)
                {
                    var ids = companyModelsResult.Data.Select(c => c.Fk_AppUser_Id).ToList();
                    var users = await _identityUserService.GetByUserIds("v1", ids);
                    if (users.IsSucceeded && users.Data != null)
                    {
                        for (int i = 0; i < companyModelsResult.Data.Count; i++)
                        {
                            var user = users.Data.Where(r => r.Id == companyModelsResult.Data[i].Fk_AppUser_Id).FirstOrDefault();
                            Mapper.Map(user, companyModelsResult.Data[i]);
                        }
                        result = ProcessResultViewModelHelper.Succedded(companyModelsResult.Data);
                    }
                    else
                    {
                        result = companyModelsResult;
                    }
                }
                else
                {
                    result = companyModelsResult;
                }
            }
            catch (Exception ex)
            {
                result = ProcessResultViewModelHelper.Failed<List<CompanyViewModel>>(null, ex.Message);
            }
            return result;
        }

        [HttpPost, Route("GetAllCompaniesPaginated/{country}"), MapToApiVersion("1.0")]
        public async Task<ProcessResultViewModel<PaginatedItems<CompanyViewModel>>> GetAllCompaniesPaginated
            ([FromRoute] Country country, [FromBody]PaginatedItems<CompanyViewModel> model)
        {
            ProcessResultViewModel<PaginatedItems<CompanyViewModel>> result = null;
            try
            {
                var paginatedItems = new PaginatedItems<Company>
                {
                    PageNo = model.PageNo,
                    PageSize = model.PageSize
                };
                var companyResult = _CompanyManager.GetByCountryPaginated(country, paginatedItems);
                var companies = companyResult.Data.Data;
                var companyModels = _mapper.Map<List<Company>, List<CompanyViewModel>>(companies);

                if (companyModels.Count > 0)
                {
                    var ids = companyModels.Select(c => c.Fk_AppUser_Id).ToList();
                    var users = await _identityUserService.GetByUserIds("v1", ids);
                    if (users.IsSucceeded && users.Data != null)
                    {
                        for (int i = 0; i < companyModels.Count; i++)
                        {
                            var user = users.Data.FirstOrDefault(r => r.Id == companyModels[i].Fk_AppUser_Id);
                            Mapper.Map(user, companyModels[i]);
                        }

                        model.Data = companyModels;
                        model.Count = paginatedItems.Count;
                        model.PageNo = paginatedItems.PageNo;
                        model.PageSize = paginatedItems.PageSize;

                        result = ProcessResultViewModelHelper.Succedded(model);
                    }
                    else
                    {
                        //result = companyModels;
                    }
                }
                else
                {
                    //result = companyModels;
                }
            }
            catch (Exception ex)
            {
                result = ProcessResultViewModelHelper.Failed<PaginatedItems<CompanyViewModel>>(null, ex.Message);
            }
            return result;
        }

        [HttpPost, Route("Register"), MapToApiVersion("1.0")]
        public async Task<ProcessResultViewModel<CompanyViewModel>> Register([FromBody] CompanyViewModel CompanyModel)
        {
            var userModel = mapper.Map<CompanyViewModel, ApplicationUserViewModel>(CompanyModel);
            var addUserResult = await _identityUserService.AddCompanyUser("v1", userModel);

            if (addUserResult.IsSucceeded
                && addUserResult.Data != null
                && !String.IsNullOrEmpty(addUserResult.Data.Id))
            {
                userModel = addUserResult.Data;
                CompanyModel.Fk_AppUser_Id = addUserResult.Data.Id;
                //CompanyModel = mapper.Map<ApplicationUserViewModel, CompanyViewModel>(userModel);
                var result = base.Post(CompanyModel);
                if (result.IsSucceeded)
                {
                    CompanyModel.Id = result.Data.Id;
                    return ProcessResultViewModelHelper.Succedded(CompanyModel);
                }
                else
                {
                    return ProcessResultViewModelHelper.Failed<CompanyViewModel>(null, "Something went wrong while adding Company");
                }
            }
            else
            {
                return ProcessResultViewModelHelper.MapToProcessResultViewModel<ApplicationUserViewModel, CompanyViewModel>(addUserResult);
            }
        }

        [HttpPut, Route("UpdateCompanyUser"), MapToApiVersion("1.0")]
        public async Task<ProcessResultViewModel<CompanyViewModel>> UpdateCompanyUser([FromBody] CompanyViewModel CompanyModel)
        {
            var CompanyId = CompanyModel.Id;
            var CompanyRes = manger.Get(CompanyId);
            if (CompanyRes.IsSucceeded && CompanyRes.Data != null)
            {
                CompanyModel.Fk_AppUser_Id = CompanyRes.Data.Fk_AppUser_Id;
                var userModel = mapper.Map<CompanyViewModel, ApplicationUserViewModel>(CompanyModel);

                var updateUserResult = await _identityUserService.UpdateCompanyUser("v1", userModel);

                if (updateUserResult.IsSucceeded && updateUserResult.Data != null)
                {
                    //userModel = updateUserResult.Data;
                    //CompanyModel = mapper.Map<ApplicationUserViewModel, CompanyViewModel>(userModel);
                    var result = base.Put(CompanyModel);
                    if (result.IsSucceeded)
                    {
                        return ProcessResultViewModelHelper.Succedded(CompanyModel);
                    }
                    else
                    {
                        return ProcessResultViewModelHelper.Failed<CompanyViewModel>(null, "Something went wrong while editing Company data");
                    }
                }
                else
                {
                    return ProcessResultViewModelHelper.MapToProcessResultViewModel<ApplicationUserViewModel, CompanyViewModel>(updateUserResult);
                }
            }
            else
            {
                return ProcessResultViewModelHelper.Failed(CompanyModel, "No Company with this Infos has been Registered to update");
            }
        }

        [HttpGet, Route("GetByUderId"), MapToApiVersion("1.0")]
        public async Task<ProcessResultViewModel<CompanyViewModel>> GetByUderId([FromQuery]string userId)
        {
            ProcessResultViewModel<CompanyViewModel> result = null;
            try
            {
                var CompanyEntityResult = _CompanyManager.GetByUserId(userId);
                if (CompanyEntityResult.IsSucceeded)
                {
                    CompanyViewModel CompanyModel = mapper.Map<Company, CompanyViewModel>(CompanyEntityResult.Data);
                    var CompanyId = CompanyModel.Id;
                    var userResult = await _identityUserService.GetById("v1", userId);
                    if (userResult.IsSucceeded)
                    {
                        CompanyModel = mapper.Map<ApplicationUserViewModel, CompanyViewModel>(userResult.Data);
                        CompanyModel.Id = CompanyId;
                        result = ProcessResultViewModelHelper.Succedded(CompanyModel);
                    }
                    else
                    {
                        result = ProcessResultViewModelHelper.Failed<CompanyViewModel>(null, "Failed to get user data");
                    }
                }
                else
                {
                    result = ProcessResultHelper.MapProcessResult<Company, CompanyViewModel>(CompanyEntityResult);
                }
            }
            catch (Exception ex)
            {
                result = ProcessResultViewModelHelper.Failed<CompanyViewModel>(null, ex.Message);
            }
            return result;
        }

        [HttpGet, Route("GetByUserId"), MapToApiVersion("1.0")]
        public async Task<ProcessResultViewModel<CompanyViewModel>> GetByUserId([FromQuery]string userId)
        {
            ProcessResultViewModel<CompanyViewModel> result = null;
            try
            {
                var companyEntityResult = _CompanyManager.GetByUserId(userId);
                if (companyEntityResult.IsSucceeded)
                {
                    CompanyViewModel companyModel = mapper.Map<Company, CompanyViewModel>(companyEntityResult.Data);
                    var companyId = companyModel.Id;
                    var userResult = await _identityUserService.GetById("v1", userId);
                    if (userResult.IsSucceeded)
                    {
                        mapper.Map(userResult.Data, companyModel);
                        //companyModel.Id = companyId;
                        //companyModel.ContactPhone = companyEntityResult.Data.ContactPhone;
                        //companyModel.ContactName = companyEntityResult.Data.ContactName;
                        //companyModel.ContactMail = companyEntityResult.Data.ContactMail;
                        //companyModel.Name = companyEntityResult.Data.Name;
                        result = ProcessResultViewModelHelper.Succedded(companyModel);
                    }
                    else
                    {
                        result = ProcessResultViewModelHelper.Failed<CompanyViewModel>(null, "Failed to get user data");
                    }
                }
                else
                {
                    result = ProcessResultHelper.MapProcessResult<Company, CompanyViewModel>(companyEntityResult);
                }
            }
            catch (Exception ex)
            {
                result = ProcessResultViewModelHelper.Failed<CompanyViewModel>(null, ex.Message);
            }
            return result;
        }

        [HttpGet, Route("GetById"), MapToApiVersion("1.0")]
        public async Task<ProcessResultViewModel<CompanyViewModel>> GetById([FromQuery]int id)
        {
            ProcessResultViewModel<CompanyViewModel> result = null;
            try
            {
                var CompanyEntityResult = _CompanyManager.Get(id);
                if (CompanyEntityResult.IsSucceeded)
                {
                    CompanyViewModel CompanyModel = mapper.Map<Company, CompanyViewModel>(CompanyEntityResult.Data);
                    var CompanyId = CompanyModel.Id;
                    var userResult = await _identityUserService.GetById("v1", CompanyModel.Fk_AppUser_Id);
                    if (userResult.IsSucceeded)
                    {
                        mapper.Map(userResult.Data, CompanyModel);
                        //CompanyModel.Id = CompanyId;
                        //CompanyModel.ContactPhone = CompanyEntityResult.Data.ContactPhone;
                        //CompanyModel.ContactName = CompanyEntityResult.Data.ContactName;
                        //CompanyModel.ContactMail = CompanyEntityResult.Data.ContactMail;
                        //CompanyModel.Name = CompanyEntityResult.Data.Name;
                        result = ProcessResultViewModelHelper.Succedded(CompanyModel);
                    }
                    else
                    {
                        result = ProcessResultViewModelHelper.Failed<CompanyViewModel>(null, "Failed to get user data");
                    }
                }
                else
                {
                    result = ProcessResultHelper.MapProcessResult<Company, CompanyViewModel>(CompanyEntityResult);
                }
            }
            catch (Exception ex)
            {
                result = ProcessResultViewModelHelper.Failed<CompanyViewModel>(null, ex.Message);
            }
            return result;
        }


        [HttpDelete, Route("DeleteCompany"), MapToApiVersion("1.0")]
        public async Task<ProcessResultViewModel<bool>> DeleteCompany([FromQuery] int id)
        {
            ProcessResultViewModel<bool> result = null;
            try
            {
                var CompanyEntityResult = _CompanyManager.Get(id);

                if (CompanyEntityResult.IsSucceeded &&
                    CompanyEntityResult.Data != null)
                {
                    await _identityUserService.DeleteById("v1", CompanyEntityResult.Data.Fk_AppUser_Id);
                    result = Delete(id);
                }
                else
                {
                    result = ProcessResultViewModelHelper.Failed(false, "Error in finding Company");
                }
            }
            catch (Exception ex)
            {
                result = ProcessResultViewModelHelper.Failed(false, ex.Message);
            }
            return result;
        }

        [HttpPut, Route("UpdateCompanyWallet"), MapToApiVersion("1.0")]
        public IProcessResultViewModel<bool> UpdateCompanyWallet([FromBody]UpdateCompanyWallet model)
        {
            if (model != null)
            {
                var companyRes = _CompanyManager.Get(model.FK_Company_Id);
                if (companyRes.IsSucceeded && companyRes.Data != null)
                {
                    companyRes.Data.Wallet = model.Wallet;
                }
                var updateRes = _CompanyManager.Update(companyRes.Data);
                return _processResultMapper.Map<bool, bool>(updateRes);
            }
            else
            {
                return ProcessResultViewModelHelper.Failed(false, "Please provide FK_Company_Id and Wallet value");
            }
        }


        [HttpPost, Route("GetByCompanyIds"), MapToApiVersion("1.0")]
        public async Task<ProcessResultViewModel<List<CompanyViewModel>>> GetByCompanyIds([FromBody]List<int> ids)
        {
            List<CompanyViewModel> data = new List<CompanyViewModel>();
            foreach (var id in ids)
            {
                var CompanyRes = await GetById(id);
                if (CompanyRes.IsSucceeded && CompanyRes.Data != null)
                {
                    data.Add(CompanyRes.Data);
                }
                else
                {
                    return ProcessResultViewModelHelper.MapToProcessResultViewModel<CompanyViewModel, List<CompanyViewModel>>(CompanyRes);
                }
            }
            return ProcessResultViewModelHelper.Succedded(data);
        }

        [HttpPost, Route("Login"), MapToApiVersion("1.0")]
        public async Task<IActionResult> Login([FromBody]LoginViewModel loginViewModel)
        {
            var loginResult = await _identityUserService.Login("v1", loginViewModel);
            if (loginResult.Equals("The username or password is incorrect"))
            {
                return Ok("The username or password is incorrect");
            }
            string[] roles = loginResult.roles.ToObject<string[]>();
            if (roles.Contains("Company"))
            {
                string userId = loginResult.userId;
                var result = manger.GetByUserId(userId);
                var CompanyId = result.Data.Id;

                return Ok(new
                {
                    loginResult.token,
                    loginResult.userName,
                    loginResult.roles,
                    UserId = CompanyId,
                    loginResult.fk_Country_Id,
                    loginResult.language
                });
            }
            else
            {
                return Ok("The username or password is incorrect");
            }
        }

        [HttpGet, Route("GetAllCompanies"), MapToApiVersion("1.0")]
        public async Task<ProcessResultViewModel<List<CompanyViewModel>>> GetAllCompanies()
        {
            ProcessResultViewModel<List<CompanyViewModel>> result = null;
            try
            {
                var CompanyModelsResult = base.Get();
                if (CompanyModelsResult.IsSucceeded && CompanyModelsResult.Data.Count > 0)
                {

                    var ids = CompanyModelsResult.Data.Select(c => c.Fk_AppUser_Id).ToList();
                    var users = await _identityUserService.GetByUserIds("v1", ids);
                    if (users.IsSucceeded)
                    {
                        for (int i = 0; i < CompanyModelsResult.Data.Count; i++)
                        {
                            var user = users.Data.Where(r => r.Id == CompanyModelsResult.Data[i].Fk_AppUser_Id).FirstOrDefault();
                            Mapper.Map(user, CompanyModelsResult.Data[i]);
                        }
                        result = ProcessResultViewModelHelper.Succedded(CompanyModelsResult.Data);
                    }
                    else
                    {
                        result = CompanyModelsResult;
                    }
                }
                else
                {
                    result = CompanyModelsResult;
                }
            }
            catch (Exception ex)
            {
                result = ProcessResultViewModelHelper.Failed<List<CompanyViewModel>>(null, ex.Message);
            }
            return result;
        }

        [HttpGet, Route("SearchUAECompanies"), MapToApiVersion("1.0")]
        public async Task<ProcessResultViewModel<List<CompanyViewModel>>> SearchUAECompanies([FromQuery]string searchToken)
        {
            ProcessResultViewModel<List<CompanyViewModel>> result = null;
            try
            {
                var usersResult = await _identityUserService.Search("v1", searchToken);
                if (usersResult.IsSucceeded)
                {
                    if (usersResult.Data != null && usersResult.Data.Count > 0)
                    {
                        var uaeCompanyUsers = usersResult.Data.Where(u => u.Fk_Country_Id == Country.UAE &&
                                                                           u.RoleNames.Contains("Company")).ToList();
                        var Companies = mapper.Map<List<CompanyViewModel>>(uaeCompanyUsers);
                        foreach (var item in Companies)
                        {
                            var Company = manger.GetByUserId(item.Fk_AppUser_Id);
                            item.Id = Company.IsSucceeded && Company.Data != null ? Company.Data.Id : 0;
                            item.Name = Company.IsSucceeded && Company.Data != null ? Company.Data.Name : "";
                            item.ContactMail = Company.IsSucceeded && Company.Data != null ? Company.Data.ContactMail : "";
                            item.ContactName = Company.IsSucceeded && Company.Data != null ? Company.Data.ContactName : "";
                            item.ContactPhone = Company.IsSucceeded && Company.Data != null ? Company.Data.ContactPhone : "";
                            item.Wallet = Company.IsSucceeded && Company.Data != null ? Company.Data.Wallet : 0;
                        }
                        result = ProcessResultViewModelHelper.Succedded(Companies);
                    }
                    else
                    {
                        result = ProcessResultViewModelHelper.Succedded<List<CompanyViewModel>>(null, "No Companies found.");
                    }
                }
                else
                {
                    result = ProcessResultViewModelHelper.MapToProcessResultViewModel<List<ApplicationUserViewModel>, List<CompanyViewModel>>(usersResult);
                }
            }
            catch (Exception ex)
            {
                result = ProcessResultViewModelHelper.Failed<List<CompanyViewModel>>(null, ex.Message);
            }
            return result;
        }

        [HttpGet, Route("SearchKWCompanies"), MapToApiVersion("1.0")]
        public async Task<ProcessResultViewModel<List<CompanyViewModel>>> SearchKWCompanies([FromQuery]string searchToken)
        {
            ProcessResultViewModel<List<CompanyViewModel>> result = null;
            try
            {
                var usersResult = await _identityUserService.Search("v1", searchToken);
                if (usersResult.IsSucceeded)
                {
                    if (usersResult.Data != null && usersResult.Data.Count > 0)
                    {
                        var uaeCompanyUsers = usersResult.Data.Where(u => u.Fk_Country_Id == Country.Kuwait &&
                                                                           u.RoleNames.Contains("Company")).ToList();
                        var Companies = mapper.Map<List<CompanyViewModel>>(uaeCompanyUsers);
                        foreach (var item in Companies)
                        {
                            var Company = manger.GetByUserId(item.Fk_AppUser_Id);
                            item.Id = Company.IsSucceeded && Company.Data != null ? Company.Data.Id : 0;
                            item.Name = Company.IsSucceeded && Company.Data != null ? Company.Data.Name : "";
                            item.ContactMail = Company.IsSucceeded && Company.Data != null ? Company.Data.ContactMail : "";
                            item.ContactName = Company.IsSucceeded && Company.Data != null ? Company.Data.ContactName : "";
                            item.ContactPhone = Company.IsSucceeded && Company.Data != null ? Company.Data.ContactPhone : "";
                            item.Wallet = Company.IsSucceeded && Company.Data != null ? Company.Data.Wallet : 0;
                        }
                        result = ProcessResultViewModelHelper.Succedded(Companies);
                    }
                    else
                    {
                        result = ProcessResultViewModelHelper.Succedded<List<CompanyViewModel>>(null, "No Companies found.");
                    }
                }
                else
                {
                    result = ProcessResultViewModelHelper.MapToProcessResultViewModel<List<ApplicationUserViewModel>, List<CompanyViewModel>>(usersResult);
                }
            }
            catch (Exception ex)
            {
                result = ProcessResultViewModelHelper.Failed<List<CompanyViewModel>>(null, ex.Message);
            }
            return result;
        }

        [HttpGet, Route("UpdateCompanyCountry"), MapToApiVersion("1.0")]
        public async Task<ProcessResultViewModel<CompanyViewModel>> UpdateCompanyCountry([FromQuery]Country countryId)
        {
            ProcessResultViewModel<CompanyViewModel> result = null;
            try
            {
                string authorizationValue = HttpContext.Request.Headers[HeaderNames.Authorization];
                string token = authorizationValue.Substring("Bearer ".Length).Trim();
                var userResult = await _identityUserService.UpdateUserCountry("v1", countryId, token);
                if (userResult.IsSucceeded && userResult.Data != null)
                {
                    var Company = mapper.Map<CompanyViewModel>(userResult.Data);
                    var CompanyObj = manger.GetByUserId(Company.Fk_AppUser_Id);
                    Company.Id = CompanyObj.IsSucceeded && CompanyObj.Data != null ? CompanyObj.Data.Id : 0;
                    result = ProcessResultViewModelHelper.Succedded(Company);
                }
                else
                {
                    result = ProcessResultViewModelHelper.MapToProcessResultViewModel<ApplicationUserViewModel, CompanyViewModel>(userResult);
                }
            }
            catch (Exception ex)
            {
                result = ProcessResultViewModelHelper.Failed<CompanyViewModel>(null, ex.Message);
            }
            return result;
        }

        [HttpGet, Route("UpdateCompanyLanguage"), MapToApiVersion("1.0")]
        public async Task<ProcessResultViewModel<CompanyViewModel>> UpdateCompanyLanguage([FromQuery]SupportedLanguage languageId)
        {
            ProcessResultViewModel<CompanyViewModel> result = null;
            try
            {
                string authorizationValue = HttpContext.Request.Headers[HeaderNames.Authorization];
                string token = authorizationValue.Substring("Bearer ".Length).Trim();
                var userResult = await _identityUserService.UpdateUserLanguage("v1", languageId, token);
                if (userResult.IsSucceeded && userResult.Data != null)
                {
                    var Company = mapper.Map<CompanyViewModel>(userResult.Data);
                    var CompanyObj = manger.GetByUserId(Company.Fk_AppUser_Id);
                    Company.Id = CompanyObj.IsSucceeded && CompanyObj.Data != null ? CompanyObj.Data.Id : 0;
                    result = ProcessResultViewModelHelper.Succedded(Company);
                }
                else
                {
                    result = ProcessResultViewModelHelper.MapToProcessResultViewModel<ApplicationUserViewModel, CompanyViewModel>(userResult);
                }
            }
            catch (Exception ex)
            {
                result = ProcessResultViewModelHelper.Failed<CompanyViewModel>(null, ex.Message);
            }
            return result;
        }
        [HttpPost, Route("PayOrderFromWallet"), MapToApiVersion("1.0")]
        public ProcessResultViewModel<bool> PayOrderFromWallet([FromBody]PayFromWallet model)
        {
            ProcessResultViewModel<bool> result = null;
            if (model != null)
            {
                var companyRes = Get(model.CompanyId);
                if (companyRes.IsSucceeded && companyRes.Data != null)
                {
                    if (companyRes.Data.Wallet < model.OrderFees)
                    {
                        result = ProcessResultViewModelHelper.Failed(false, $"{companyRes.Data.Name} Wallet doesn't have Enough money to deduct {model.OrderFees} order fees from  {companyRes.Data.Wallet} total amount in wallet");
                    }
                    else
                    {
                        companyRes.Data.Wallet = companyRes.Data.Wallet - model.OrderFees;
                        result = Put(companyRes.Data);
                    }
                }
                else
                {
                    result = ProcessResultViewModelHelper.Failed(false, $"There is no Company with this Id {model.CompanyId}");
                }
            }
            else
            {

                result = ProcessResultViewModelHelper.Failed(false, $"Parameter PayFromWallet Model Can't be null");

            }
            return result;
        }

        [HttpPost, Route("CanPayOrderFromWallet"), MapToApiVersion("1.0")]
        public ProcessResultViewModel<bool> CanPayOrderFromWallet([FromBody]PayFromWallet model)
        {
            ProcessResultViewModel<bool> result = null;
            if (model != null)
            {
                var companyRes = Get(model.CompanyId);
                if (companyRes.IsSucceeded && companyRes.Data != null)
                {
                    if (companyRes.Data.Wallet < model.OrderFees)
                    {
                        result = ProcessResultViewModelHelper.Failed(false, $"{companyRes.Data.Name} Wallet doesn't have Enough money to deduct {model.OrderFees} order fees from  {companyRes.Data.Wallet} total amount in wallet");
                    }
                    else
                    {
                        result = ProcessResultViewModelHelper.Succedded(true);
                    }
                }
            }
            else
            {

                result = ProcessResultViewModelHelper.Failed(false, $"Parameter PayFromWallet Model Can't be null");

            }
            return result;
        }
    }
}
