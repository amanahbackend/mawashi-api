﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Dispatching.CustomerModule.Models.Context;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json.Serialization;
using AutoMapper;
using Swashbuckle.AspNetCore.Swagger;
using BuildingBlocks.ServiceDiscovery;
using Dispatching.CustomerModule.BLL.IManagers;
using Dispatching.CustomerModule.BLL.Managers;
using DispatchProduct.RepositoryModule;
using Utilites.ProcessingResult;
using Utilites.PaginatedItems;
using Dispatching.CustomerModule.API.ServicesSettings;
using Dispatching.CustomerModule.API.ServiceCommunications.Identity;
using System.Net;
using DnsClient;
using System.Net.Sockets;
using Dispatching.CustomerModule.API.ServiceCommunications.Location;
using CommonServicesAPI.ServicesSettings.Identity;
using Dispatching.CustomerModule.API.Controllers;
using Dispatching.CustomerModule.API.ViewModels;
using Dispatching.CustomerModule.Models.Settings;
using Dispatching.CustomerModule.API.AutoMapperConfig;
using Dispatching.CompanyModule.BLL.IManagers;
using Dispatching.CustomerModule.API.ServiceCommunications.Dropout;

namespace Dispatching.CustomerModule.API
{
    public class Startup
    {
        public IConfigurationRoot Configuration { get; }
        public IHostingEnvironment _env;
        public Startup(IHostingEnvironment env)
        {
            _env = env;
            var builder = new ConfigurationBuilder()
                .SetBasePath(env.ContentRootPath)
                .AddJsonFile("appsettings.json", optional: false, reloadOnChange: true)
                .AddJsonFile($"appsettings.{env.EnvironmentName}.json", optional: true)
                .AddEnvironmentVariables();
            Configuration = builder.Build();
        }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddCors(options =>
            {
                options.AddPolicy("AllowAll",
                    builder =>
                    {
                        builder
                        .AllowAnyOrigin()
                        .AllowAnyMethod()
                        .AllowAnyHeader()
                        .AllowCredentials();
                    });
            });
            services.AddSingleton(provider => Configuration);

            // Add framework services.
            services.AddDbContext<DataContext>(options =>
             options.UseSqlServer(Configuration["ConnectionString"],
              sqlOptions => sqlOptions.MigrationsAssembly("Dispatching.Customer.EFCore.MSSQL")));

            services.AddMvc().AddJsonOptions(options =>
{
    options.SerializerSettings.ContractResolver = new CamelCasePropertyNamesContractResolver();
});

            services.Configure<IdentityServiceSettings>
                (Configuration.GetSection("ServiceCommunications:IdentityService"));
            services.Configure<LocationServiceSettings>
                (Configuration.GetSection("ServiceCommunications:LocationService"));

            services.Configure<CustomerAppSettings>(Configuration);


            services.AddAutoMapper(typeof(Startup));
            Mapper.AssertConfigurationIsValid();

            #region Data & Managers
            services.AddScoped<DbContext, DataContext>();
            services.AddScoped(typeof(IRepository<>), typeof(Repository<>));
            services.AddScoped(typeof(IProcessResultMapper), typeof(ProcessResultMapper));
            services.AddScoped(typeof(IProcessResultPaginatedMapper), typeof(ProcessResultPaginatedMapper));
            services.AddScoped(typeof(IPaginatedItems<>), typeof(PaginatedItems<>));
            services.AddScoped(typeof(IProcessResultViewModel<>), typeof(ProcessResultViewModel<>));
            services.AddScoped(typeof(IProcessResultStatus), typeof(ProcessResultStatus));
            services.AddScoped(typeof(MappingProfile), typeof(MappingProfile));



            services.AddScoped<ICustomerManager, CustomerManager>();
            services.AddScoped<ICompanyManager, CompanyManager>();

            services.AddScoped<ILocationManager, LocationManager>();
            services.AddScoped<ICompanyLocationManager, CompanyLocationManager>();
            services.AddScoped<ICustomerDeviceManager, CustomerDeviceManager>();



            services.AddScoped<IIdentityUserService, IdentityUserService>();
            services.AddScoped<ILocationService, LocationService>();
            //services.AddScoped<IDeliveryOptionService, DeliveryOptionService>();
            services.AddScoped(typeof(IDropoutService), typeof(DropoutService));



            #endregion Data & Managers

            services.Configure<DropoutServiceSettings>
                (Configuration.GetSection("ServiceCommunications:DropoutService"));

            // Dns Query for consul - service discovery
            string consulContainerName = Configuration.GetSection("ServiceRegisteryContainerName").Value;
            int consulPort = Convert.ToInt32(Configuration.GetSection("ServiceRegisteryPort").Value);
            IPAddress ip = Dns.GetHostEntry(consulContainerName).AddressList.Where(o => o.AddressFamily == AddressFamily.InterNetwork).First();
            services.AddSingleton<IDnsQuery>(new LookupClient(ip, consulPort));

            // Register the Swagger generator, defining one or more Swagger documents
            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1", new Info { Title = "Customer", Version = "v1" });
            });


            services.AddServiceDiscovery(Configuration.GetSection("ServiceDiscovery"));

            services.AddApiVersioning(o => o.ReportApiVersions = true);
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            app.UseCors("AllowAll");

            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
                app.UseDatabaseErrorPage();
            }
            //else
            //{
            //    app.UseExceptionHandler("/Home/Error");
            //}

            // Enable middleware to serve generated Swagger as a JSON endpoint.
            app.UseSwagger();
            // Enable middleware to serve swagger-ui (HTML, JS, CSS, etc.), specifying the Swagger JSON endpoint.
            app.UseSwaggerUI(c =>
            {
                c.SwaggerEndpoint("/swagger/v1/swagger.json", "Customer V1");
            });


            app.UseMvc();

            // Autoregister using server.Features (does not work in reverse proxy mode)
            app.UseConsulRegisterService();
        }
    }
}
