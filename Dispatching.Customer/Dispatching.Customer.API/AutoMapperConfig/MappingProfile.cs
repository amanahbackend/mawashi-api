﻿using AutoMapper;
using CommonEnums;
using Dispatching.CustomerModule.API.ServicesViewModels.Identity;
using Dispatching.CustomerModule.API.ViewModels;
using Dispatching.CustomerModule.Models.Context;
using Dispatching.CustomerModule.Models.Settings;
using DispatchProduct.BuildingBlocks.BaseEntities.Entities;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Utilities;
using Utilities.Utilites;
namespace Dispatching.CustomerModule.API.AutoMapperConfig
{
    public class MappingProfile : Profile
    {

        public MappingProfile()
        {

            CreateMap<Models.Context.Customer, CustomerViewModel>()
                .ForMember(dest => dest.Email, opt => opt.Ignore())
                .ForMember(dest => dest.FirstName, opt => opt.Ignore())
                .ForMember(dest => dest.LastName, opt => opt.Ignore())
                .ForMember(dest => dest.Password, opt => opt.Ignore())
                .ForMember(dest => dest.Phone1, opt => opt.Ignore())
                .ForMember(dest => dest.Phone2, opt => opt.Ignore())
                .ForMember(dest => dest.Picture, opt => opt.Ignore())
                .ForMember(dest => dest.PicturePath, opt => opt.Ignore())
                .ForMember(dest => dest.RoleNames, opt => opt.Ignore())
                .ForMember(dest => dest.PictureUrl, opt => opt.Ignore())
                .ForMember(dest => dest.Language, opt => opt.Ignore())
                .ForMember(dest => dest.UserName, opt => opt.Ignore());

            CreateMap<CustomerViewModel, Models.Context.Customer>()
                .IgnoreBaseEntityProperties();



            CreateMap<Company, CompanyViewModel>()
            .ForMember(dest => dest.Email, opt => opt.Ignore())
            .ForMember(dest => dest.FirstName, opt => opt.Ignore())
            .ForMember(dest => dest.LastName, opt => opt.Ignore())
            .ForMember(dest => dest.Password, opt => opt.Ignore())
            .ForMember(dest => dest.Phone1, opt => opt.Ignore())
            .ForMember(dest => dest.Phone2, opt => opt.Ignore())
            .ForMember(dest => dest.Picture, opt => opt.Ignore())
            .ForMember(dest => dest.PicturePath, opt => opt.Ignore())
            .ForMember(dest => dest.RoleNames, opt => opt.Ignore())
            .ForMember(dest => dest.Fk_Country_Id, opt => opt.Ignore())
            .ForMember(dest => dest.CurrentUserId, opt => opt.Ignore())
            .ForMember(dest => dest.PictureUrl, opt => opt.Ignore())
            .ForMember(dest => dest.Language, opt => opt.Ignore())
            .ForMember(dest => dest.UserName, opt => opt.Ignore());

            CreateMap<CompanyViewModel, Company>()
                .IgnoreBaseEntityProperties();

            CreateMap<ApplicationUserViewModel, CustomerViewModel>()
                .ForMember(dest => dest.Fk_AppUser_Id, opt => opt.MapFrom(src => src.Id))
                .ForMember(dest => dest.CurrentUserId, opt => opt.Ignore())
                .ForMember(dest => dest.Id, opt => opt.Ignore());


            CreateMap<CustomerViewModel, ApplicationUserViewModel>()
                .ForMember(dest => dest.Id, opt => opt.MapFrom(src => src.Fk_AppUser_Id))
                .ForMember(dest => dest.IsBasicRegister, opt => opt.Ignore())
                .IgnoreIdentityBaseEntityProperties();




            CreateMap<ApplicationUserViewModel, CompanyViewModel>()
           .ForMember(dest => dest.Fk_AppUser_Id, opt => opt.MapFrom(src => src.Id))
           .ForMember(dest => dest.Name, opt => opt.Ignore())
           .ForMember(dest => dest.Fk_Country_Id, opt => opt.Ignore())
           .ForMember(dest => dest.CurrentUserId, opt => opt.Ignore())
           .ForMember(dest => dest.Wallet, opt => opt.Ignore())
           .ForMember(dest => dest.ContactName, opt => opt.Ignore())
           .ForMember(dest => dest.ContactPhone, opt => opt.Ignore())
           .ForMember(dest => dest.ContactMail, opt => opt.Ignore())
           .ForMember(dest => dest.Id, opt => opt.Ignore());


            CreateMap<CompanyViewModel, ApplicationUserViewModel>()
                .ForMember(dest => dest.Id, opt => opt.MapFrom(src => src.Fk_AppUser_Id))
                .ForMember(dest => dest.IsBasicRegister, opt => opt.Ignore())
                .IgnoreIdentityBaseEntityProperties();


            CreateMap<LocationViewModel, Location>()
                .IgnoreBaseEntityProperties();
            CreateMap<Location, LocationViewModel>();


            CreateMap<CompanyLocationViewModel, CompanyLocation>()
             .IgnoreBaseEntityProperties();
            CreateMap<CompanyLocation, CompanyLocationViewModel>();




            CreateMap<CustomerDeviceViewModel, CustomerDevice>()
                .IgnoreBaseEntityProperties();
            CreateMap<CustomerDevice, CustomerDeviceViewModel>();

        }

    }
}
