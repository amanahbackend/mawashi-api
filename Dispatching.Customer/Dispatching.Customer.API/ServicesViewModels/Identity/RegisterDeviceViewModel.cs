﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Dispatching.Customer.API.ServicesViewModels.Identity
{
    public class RegisterDeviceViewModel
    {
        public string UserId { get; set; }
        public string DeviceId { get; set; }
    }
}
