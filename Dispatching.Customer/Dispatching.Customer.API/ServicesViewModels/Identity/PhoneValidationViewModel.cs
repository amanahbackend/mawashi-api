﻿using CommonEnums;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Dispatching.CustomerModule.API.ServicesViewModels.Identity
{
    public class PhoneValidationViewModel
    {
        [Required]
        [Display(Name = "Username")]
        public string Username { get; set; }

        [Required]
        [Display(Name = "Phone")]
        public string Phone { get; set; }
        public string Code { get; set; }
        public Country Country { get; set; }
    }
}
