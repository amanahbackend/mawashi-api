﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Dispatching.Customer.API.ServicesViewModels.Identity
{
    public class IdentityPaginatedItems<ApplicationUserViewModel> 
    {
        public int PageNo { get; set; }

        public int PageSize { get; set; }

        public long Count { get; set; }

        public List<ApplicationUserViewModel> Data { get; set; }
    }
}
