﻿using Dispatching.CustomerModule.API.ServicesSettings;
using Dispatching.CustomerModule.API.ServicesViewModels.Dropout;
using DispatchProduct.HttpClient;
using DnsClient;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Dispatching.CustomerModule.API.ServiceCommunications.Dropout
{
    public class DropoutService
       : DefaultHttpClientCrud<DropoutServiceSettings, SendSmsViewModel, SendSmsViewModel>,
       IDropoutService
    {
        DropoutServiceSettings _settings;
        IDnsQuery _dnsQuery;

        public DropoutService(IOptions<DropoutServiceSettings> obj, IDnsQuery dnsQuery) : base(obj.Value, dnsQuery)
        {
            _dnsQuery = dnsQuery;
            _settings = obj.Value;
        }

        public async Task SendKw(string version, SendSmsViewModel model)
        {
            string baseUrl = await _settings.GetBaseUrl(_dnsQuery);
            while (baseUrl.EndsWith('/'))
            {
                baseUrl = baseUrl.Substring(0, baseUrl.Length - 1);
            }
            if (!String.IsNullOrEmpty(baseUrl))
            {
                string requestedAction = _settings.SendKWAction;
                var url = $"{baseUrl}/{version}/{requestedAction}";
                await Post(url, model);
            }
        }

        public async Task SendUae(string version, SendSmsViewModel model)
        {
            string baseUrl = await _settings.GetBaseUrl(_dnsQuery);
            while (baseUrl.EndsWith('/'))
            {
                baseUrl = baseUrl.Substring(0, baseUrl.Length - 1);
            }
            if (!String.IsNullOrEmpty(baseUrl))
            {
                string requestedAction = _settings.SendUAEAction;
                var url = $"{baseUrl}/{version}/{requestedAction}";
                await Post(url, model);
            }
        }
    }
}
