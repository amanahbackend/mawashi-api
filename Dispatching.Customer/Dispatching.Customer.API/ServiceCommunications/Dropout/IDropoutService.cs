﻿using Dispatching.CustomerModule.API.ServicesSettings;
using Dispatching.CustomerModule.API.ServicesViewModels.Dropout;
using DispatchProduct.HttpClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Dispatching.CustomerModule.API.ServiceCommunications.Dropout
{
    public interface IDropoutService :
        IDefaultHttpClientCrud<DropoutServiceSettings, SendSmsViewModel, SendSmsViewModel>
    {
        Task SendKw(string version, SendSmsViewModel model);
        Task SendUae(string version, SendSmsViewModel model);

    }
}
