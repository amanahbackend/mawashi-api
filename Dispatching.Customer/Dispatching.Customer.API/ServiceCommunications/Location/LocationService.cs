﻿using Dispatching.CustomerModule.API.ServicesSettings;
using DispatchProduct.HttpClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DnsClient;
using Microsoft.Extensions.Options;
using Utilities.Utilites.PACI;
using Utilites.ProcessingResult;

namespace Dispatching.CustomerModule.API.ServiceCommunications.Location
{
    public class LocationService
        : DefaultHttpClientCrud<LocationServiceSettings, Point, Point>, ILocationService
    {
        LocationServiceSettings _settings;
        IDnsQuery _dnsQuery;
        public LocationService(IOptions<LocationServiceSettings> obj, IDnsQuery dnsQuery)
            : base(obj.Value, dnsQuery)
        {
            _settings = obj.Value;
            _dnsQuery = dnsQuery;
        }

        public async Task<ProcessResultViewModel<Point>> GetStreetCoords(string version, string streetName, string blockName)
        {
            string baseUrl = await _settings.GetUri(_dnsQuery);
            while (baseUrl.EndsWith('/'))
            {
                baseUrl = baseUrl.Substring(0, baseUrl.Length - 1);
            }
            if (!String.IsNullOrEmpty(baseUrl))
            {
                var requestedAction = _settings.GetStreetCoorsAction;
                var url = $"{baseUrl}/{version}/{requestedAction}?streetName={streetName}&blockName={blockName}";
                return await GetByUri(url);
            }
            return null;
        }
    }
}
