﻿using Dispatching.CustomerModule.API.ServicesSettings;
using DispatchProduct.HttpClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Utilites.ProcessingResult;
using Utilities.Utilites.PACI;

namespace Dispatching.CustomerModule.API.ServiceCommunications.Location
{
    public interface ILocationService
        : IDefaultHttpClientCrud<LocationServiceSettings, Point, Point>
    {
        Task<ProcessResultViewModel<Point>> GetStreetCoords(string version, string streetName, string blockName);
    }
}
