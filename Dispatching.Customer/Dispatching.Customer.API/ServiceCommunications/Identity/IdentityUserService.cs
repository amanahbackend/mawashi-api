﻿using CommonEnums;
using CommonServicesAPI.ServicesSettings.Identity;
using Dispatching.CustomerModule.API.ServicesViewModels.Identity;
using Dispatching.CustomerModule.API.ViewModels;
using DispatchProduct.Api.HttpClient;
using DispatchProduct.HttpClient;
using DnsClient;
using Microsoft.Extensions.Options;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using Dispatching.Customer.API.ServicesViewModels.Identity;
using Utilites.ProcessingResult;

namespace Dispatching.CustomerModule.API.ServiceCommunications.Identity
{
    public class IdentityUserService
       : DefaultHttpClientCrud<IdentityServiceSettings, ApplicationUserViewModel, ApplicationUserViewModel>,
       IIdentityUserService
    {
        IdentityServiceSettings _settings;
        IDnsQuery _dnsQuery;

        public IdentityUserService(IOptions<IdentityServiceSettings> obj, IDnsQuery dnsQuery) : base(obj.Value, dnsQuery)
        {
            _dnsQuery = dnsQuery;
            _settings = obj.Value;
        }

        public async Task<ProcessResultViewModel<ApplicationUserViewModel>> AddCustomerUser(string version, ApplicationUserViewModel user)
        {
            ProcessResultViewModel<ApplicationUserViewModel> result = null;
            string baseUrl = await _settings.GetBaseUrl(_dnsQuery);
            while (baseUrl.EndsWith('/'))
            {
                baseUrl = baseUrl.Substring(0, baseUrl.Length - 1);
            }
            if (!String.IsNullOrEmpty(baseUrl))
            {
                string requestedAction = _settings.AddUserAction;
                var url = $"{baseUrl}/{version}/{requestedAction}";
                user.RoleNames = new List<string>() { "Customer" };
                result = await Post(url, user);
            }
            return result;
        }

        public async Task<ProcessResultViewModel<ApplicationUserViewModel>> AddCompanyUser(string version, ApplicationUserViewModel user)
        {
            ProcessResultViewModel<ApplicationUserViewModel> result = null;
            string baseUrl = await _settings.GetBaseUrl(_dnsQuery);
            while (baseUrl.EndsWith('/'))
            {
                baseUrl = baseUrl.Substring(0, baseUrl.Length - 1);
            }
            if (!String.IsNullOrEmpty(baseUrl))
            {
                string requestedAction = _settings.AddUserAction;
                var url = $"{baseUrl}/{version}/{requestedAction}";
                user.RoleNames = new List<string>() { "Company" };
                result = await Post(url, user);
            }
            return result;
        }

        public async Task<ProcessResultViewModel<ApplicationUserViewModel>> UpdateCustomerUser(string version, ApplicationUserViewModel user)
        {
            string baseUrl = await _settings.GetBaseUrl(_dnsQuery);
            while (baseUrl.EndsWith('/'))
            {
                baseUrl = baseUrl.Substring(0, baseUrl.Length - 1);
            }
            if (!String.IsNullOrEmpty(baseUrl))
            {
                string requestedAction = _settings.UpdateUserAction;
                var url = $"{baseUrl}/{version}/{requestedAction}";
                user.RoleNames = new List<string>() { "Customer" };
                return await Put(url, user);
            }
            return null;
        }

        public async Task<ProcessResultViewModel<ApplicationUserViewModel>> UpdateCustomerPhone(string version, ApplicationUserViewModel user)
        {
            string baseUrl = await _settings.GetBaseUrl(_dnsQuery);
            while (baseUrl.EndsWith('/'))
            {
                baseUrl = baseUrl.Substring(0, baseUrl.Length - 1);
            }
            if (!String.IsNullOrEmpty(baseUrl))
            {
                string requestedAction = _settings.UpdatePhone;
                var url = $"{baseUrl}/{version}/{requestedAction}";
                user.RoleNames = new List<string>() { "Customer" };
                return await Put(url, user);
            }
            return null;
        }

        public async Task<ProcessResultViewModel<ApplicationUserViewModel>> UpdateCustomerEmail(string version, ApplicationUserViewModel user)
        {
            string baseUrl = await _settings.GetBaseUrl(_dnsQuery);
            while (baseUrl.EndsWith('/'))
            {
                baseUrl = baseUrl.Substring(0, baseUrl.Length - 1);
            }
            if (!String.IsNullOrEmpty(baseUrl))
            {
                string requestedAction = _settings.UpdateEmail;
                var url = $"{baseUrl}/{version}/{requestedAction}";
                user.RoleNames = new List<string>() { "Customer" };
                return await Put(url, user);
            }
            return null;
        }

        public async Task<ProcessResultViewModel<ApplicationUserViewModel>> UpdateCustomerPicture(string version, ApplicationUserViewModel user)
        {
            string baseUrl = await _settings.GetBaseUrl(_dnsQuery);
            while (baseUrl.EndsWith('/'))
            {
                baseUrl = baseUrl.Substring(0, baseUrl.Length - 1);
            }
            if (!String.IsNullOrEmpty(baseUrl))
            {
                string requestedAction = _settings.UpdatePicture;
                var url = $"{baseUrl}/{version}/{requestedAction}";
                user.RoleNames = new List<string>() { "Customer" };
                return await Put(url, user);
            }
            return null;
        }

        public async Task<ProcessResultViewModel<ApplicationUserViewModel>> UpdateCompanyUser(string version, ApplicationUserViewModel user)
        {
            string baseUrl = await _settings.GetBaseUrl(_dnsQuery);
            while (baseUrl.EndsWith('/'))
            {
                baseUrl = baseUrl.Substring(0, baseUrl.Length - 1);
            }
            if (!String.IsNullOrEmpty(baseUrl))
            {
                string requestedAction = _settings.UpdateUserAction;
                var url = $"{baseUrl}/{version}/{requestedAction}";
                user.RoleNames = new List<string>() { "Company" };
                return await Put(url, user);
            }
            return null;
        }

        public async Task<ProcessResultViewModel<ApplicationUserViewModel>> GetById(string version, string userId)
        {
            string baseUrl = await _settings.GetBaseUrl(_dnsQuery);
            while (baseUrl.EndsWith('/'))
            {
                baseUrl = baseUrl.Substring(0, baseUrl.Length - 1);
            }
            if (!String.IsNullOrEmpty(baseUrl))
            {
                string requestedAction = _settings.GetUserById;
                var url = $"{baseUrl}/{version}/{requestedAction}?id={userId}";
                return await Get(url);
            }
            return null;
        }

        public async Task DeleteById(string version, string userId)
        {
            string baseUrl = await _settings.GetBaseUrl(_dnsQuery);
            while (baseUrl.EndsWith('/'))
            {
                baseUrl = baseUrl.Substring(0, baseUrl.Length - 1);
            }
            if (!String.IsNullOrEmpty(baseUrl))
            {
                string requestedAction = _settings.DeleteUserById;
                var url = $"{baseUrl}/{version}/{requestedAction}?id={userId}";
                await HttpRequestFactory.Delete(url);
            }
        }

        public async Task<dynamic> Login(string version, LoginViewModel loginViewModel)
        {
            string baseUrl = await _settings.GetBaseUrl(_dnsQuery);
            while (baseUrl.EndsWith('/'))
            {
                baseUrl = baseUrl.Substring(0, baseUrl.Length - 1);
            }
            if (!String.IsNullOrEmpty(baseUrl))
            {
                string requestedAction = _settings.Login;
                var url = $"{baseUrl}/{version}/{requestedAction}";
                var result = await HttpRequestFactory.Post(url, loginViewModel);
                if (result.StatusCode == HttpStatusCode.OK)
                {
                    var resultString = await result.Content.ReadAsStringAsync();
                    return JsonConvert.DeserializeObject(resultString);
                }
                else if (result.StatusCode == HttpStatusCode.Unauthorized)
                {
                    return HttpStatusCode.Unauthorized;
                }
            }
            return null;
        }

        public async Task<ProcessResultViewModel<List<ApplicationUserViewModel>>> GetByUserIds(string version, List<string> ids)
        {
            string baseUrl = await _settings.GetBaseUrl(_dnsQuery);
            while (baseUrl.EndsWith('/'))
            {
                baseUrl = baseUrl.Substring(0, baseUrl.Length - 1);
            }
            if (!String.IsNullOrEmpty(baseUrl))
            {
                string requestedAction = _settings.GetByIdsAction;
                var url = $"{baseUrl}/{version}/{requestedAction}";
                return await PostCustomize<List<string>, List<ApplicationUserViewModel>>(url, ids);
            }
            return null;
        }

        public async Task<ProcessResultViewModel<List<ApplicationUserViewModel>>> Search(string version, string searchToken)
        {
            ProcessResultViewModel<List<ApplicationUserViewModel>> result = null;
            string baseUrl = await _settings.GetBaseUrl(_dnsQuery);
            while (baseUrl.EndsWith('/'))
            {
                baseUrl = baseUrl.Substring(0, baseUrl.Length - 1);
            }
            if (!String.IsNullOrEmpty(baseUrl))
            {
                string requestedAction = _settings.SearchAction;
                var url = $"{baseUrl}/{version}/{requestedAction}?searchToken={searchToken}";
                var response = await HttpRequestFactory.Get(url);
                if (response.StatusCode == System.Net.HttpStatusCode.OK)
                {
                    result = response.ContentAsType<ProcessResultViewModel<List<ApplicationUserViewModel>>>();
                }
            }
            return result;
        }


        public async Task<IdentityPaginatedItems<ApplicationUserViewModel>> GetCustomerUsersPaginatedByCountry
            (string version, Country country, IdentityPaginatedItems<ApplicationUserViewModel> paginatedItems)
        {
            IdentityPaginatedItems<ApplicationUserViewModel> result = null;
            string baseUrl = await _settings.GetBaseUrl(_dnsQuery);
            while (baseUrl.EndsWith('/'))
            {
                baseUrl = baseUrl.Substring(0, baseUrl.Length - 1);
            }
            if (!String.IsNullOrEmpty(baseUrl))
            {
                string requestedAction = _settings.GetCustomerUsersPaginatedByCountry;
                var url = $"{baseUrl}/{version}/{requestedAction}/{country}";
                var response = await HttpRequestFactory.Post(url, paginatedItems);
                if (response.StatusCode == System.Net.HttpStatusCode.OK)
                {
                    result = response.ContentAsType<IdentityPaginatedItems<ApplicationUserViewModel>>();
                }
            }
            return result;
        }

        public async Task<ProcessResultViewModel<ApplicationUserViewModel>> UpdateUserCountry(string version, Country countryId, string token)
        {
            ProcessResultViewModel<ApplicationUserViewModel> result = null;
            string baseUrl = await _settings.GetBaseUrl(_dnsQuery);
            while (baseUrl.EndsWith('/'))
            {
                baseUrl = baseUrl.Substring(0, baseUrl.Length - 1);
            }
            if (!String.IsNullOrEmpty(baseUrl))
            {
                string requestedAction = _settings.UpdateUserCountryAction;
                var url = $"{baseUrl}/{version}/{requestedAction}?countryId={countryId}";
                var response = await HttpRequestFactory.Get(url, token);
                if (response.StatusCode == System.Net.HttpStatusCode.OK)
                {
                    result = response.ContentAsType<ProcessResultViewModel<ApplicationUserViewModel>>();
                }
            }
            return result;
        }

        public async Task<ProcessResultViewModel<ApplicationUserViewModel>> UpdateUserLanguage(string version, SupportedLanguage languageId, string token)
        {
            ProcessResultViewModel<ApplicationUserViewModel> result = null;
            string baseUrl = await _settings.GetBaseUrl(_dnsQuery);
            while (baseUrl.EndsWith('/'))
            {
                baseUrl = baseUrl.Substring(0, baseUrl.Length - 1);
            }
            if (!String.IsNullOrEmpty(baseUrl))
            {
                string requestedAction = _settings.UpdateUserLangauageAction;
                var url = $"{baseUrl}/{version}/{requestedAction}?languageId={languageId}";
                var response = await HttpRequestFactory.Get(url, token);
                if (response.StatusCode == System.Net.HttpStatusCode.OK)
                {
                    result = response.ContentAsType<ProcessResultViewModel<ApplicationUserViewModel>>();
                }
            }
            return result;
        }

        public async Task GetPhoneVerficationToken(string version, PhoneValidationViewModel model)
        {
            string baseUrl = await _settings.GetBaseUrl(_dnsQuery);
            while (baseUrl.EndsWith('/'))
            {
                baseUrl = baseUrl.Substring(0, baseUrl.Length - 1);
            }
            if (!String.IsNullOrEmpty(baseUrl))
            {
                string requestedAction = _settings.GetPhoneVerficationToken;
                var url = $"{baseUrl}/{version}/{requestedAction}";
                await HttpRequestFactory.Post(url, model);
            }
        }

        public async Task RegisterDevice(string version,RegisterDeviceViewModel model)
        {
            string baseUrl = await _settings.GetBaseUrl(_dnsQuery);
            while (baseUrl.EndsWith('/'))
            {
                baseUrl = baseUrl.Substring(0, baseUrl.Length - 1);
            }
            if (!String.IsNullOrEmpty(baseUrl))
            {
                string requestedAction = _settings.RegisterDevice;
                var url = $"{baseUrl}/{version}/{requestedAction}";
                await HttpRequestFactory.Post(url, model);
            }
        }
    }
}
