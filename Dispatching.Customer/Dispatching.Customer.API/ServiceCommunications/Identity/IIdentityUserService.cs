﻿using CommonEnums;
using CommonServicesAPI.ServicesSettings.Identity;
using Dispatching.CustomerModule.API.ServicesViewModels.Identity;
using Dispatching.CustomerModule.API.ViewModels;
using DispatchProduct.HttpClient;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Dispatching.Customer.API.ServicesViewModels.Identity;
using Utilites.ProcessingResult;

namespace Dispatching.CustomerModule.API.ServiceCommunications.Identity

{
    public interface IIdentityUserService
       : IDefaultHttpClientCrud<IdentityServiceSettings, ApplicationUserViewModel, ApplicationUserViewModel>
    {
        Task<ProcessResultViewModel<ApplicationUserViewModel>> AddCustomerUser(string version, ApplicationUserViewModel user);
        Task<ProcessResultViewModel<ApplicationUserViewModel>> AddCompanyUser(string version, ApplicationUserViewModel user);
        Task<ProcessResultViewModel<ApplicationUserViewModel>> UpdateCustomerUser(string version, ApplicationUserViewModel user);
        Task<ProcessResultViewModel<ApplicationUserViewModel>> GetById(string version, string userId);
        Task DeleteById(string version, string userId);
        Task<dynamic> Login(string version, LoginViewModel loginViewModel);
        Task<ProcessResultViewModel<List<ApplicationUserViewModel>>> GetByUserIds(string version, List<string> ids);
        Task<ProcessResultViewModel<List<ApplicationUserViewModel>>> Search(string version, string searchToken);
        Task<ProcessResultViewModel<ApplicationUserViewModel>> UpdateUserCountry(string version, Country countryId, string token);
        Task<ProcessResultViewModel<ApplicationUserViewModel>> UpdateUserLanguage(string version, SupportedLanguage languageId, string token);
        Task GetPhoneVerficationToken(string version, PhoneValidationViewModel model);
        Task<ProcessResultViewModel<ApplicationUserViewModel>> UpdateCompanyUser(string version, ApplicationUserViewModel user);
        Task<ProcessResultViewModel<ApplicationUserViewModel>> UpdateCustomerPicture(string version, ApplicationUserViewModel user);
        Task<ProcessResultViewModel<ApplicationUserViewModel>> UpdateCustomerPhone(string version, ApplicationUserViewModel user);
        Task<ProcessResultViewModel<ApplicationUserViewModel>> UpdateCustomerEmail(string version, ApplicationUserViewModel user);

        Task<IdentityPaginatedItems<ApplicationUserViewModel>> GetCustomerUsersPaginatedByCountry
            (string version, Country country, IdentityPaginatedItems<ApplicationUserViewModel> paginatedItems);
        Task RegisterDevice(string version, RegisterDeviceViewModel model);
    }
}
