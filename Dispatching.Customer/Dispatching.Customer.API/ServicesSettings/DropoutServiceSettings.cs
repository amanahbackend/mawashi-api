﻿using BuildingBlocks.ServiceDiscovery;
using DispatchProduct.HttpClient;
using DnsClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Dispatching.CustomerModule.API.ServicesSettings
{
    public class DropoutServiceSettings : DefaultHttpClientSettings
    {
        public string SendUAEAction { get; set; }
        public string SendKWAction { get; set; }

        public async Task<string> GetBaseUrl(IDnsQuery dnsQuery)
        {
            return await ServiceDiscoveryHelper.GetServiceUrl(dnsQuery, ServiceName);
        }
    }
}
