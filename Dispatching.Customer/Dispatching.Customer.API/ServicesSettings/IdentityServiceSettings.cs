﻿using BuildingBlocks.ServiceDiscovery;
using DispatchProduct.HttpClient;
using DnsClient;
using System.Threading.Tasks;

namespace CommonServicesAPI.ServicesSettings.Identity
{
    public class IdentityServiceSettings : DefaultHttpClientSettings
    {

        private new string Uri { get => base.Uri; set => base.Uri = value; }

        public string ServiceName { get; set; }
        public string AddUserAction { get; set; }
        public string UpdateUserAction { get; set; }
        public string GetUserById { get; set; }
        public string DeleteUserById { get; set; }
        public string Login { get; set; }
        public string GetByIdsAction { get; set; }
        public string SearchAction { get; set; }
        public string UpdateUserCountryAction { get; set; }
        public string UpdateUserLangauageAction { get; set; }
        public string GetPhoneVerficationToken { get; set; }
        public string UpdatePhone { get; set; }
        public string UpdateEmail { get; set; }
        public string UpdatePicture { get; set; }
        public string GetCustomerUsersPaginatedByCountry { get; set; }
        public string RegisterDevice { get; set; }

        public async Task<string> GetBaseUrl(IDnsQuery dnsQuery)
        {
            return await ServiceDiscoveryHelper.GetServiceUrl(dnsQuery, ServiceName);
        }
    }
}
