﻿using DispatchProduct.HttpClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Dispatching.CustomerModule.API.ServicesSettings
{
    public class LocationServiceSettings : DefaultHttpClientSettings
    {
        private new string Uri { get => base.Uri; set => base.Uri = value; }

        public string GetStreetCoorsAction { get; set; }
    }
}
