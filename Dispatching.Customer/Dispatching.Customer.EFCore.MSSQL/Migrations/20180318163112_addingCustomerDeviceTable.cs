﻿using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Dispatching.CustomerModule.EFCore.MSSQL.Migrations
{
    public partial class addingCustomerDeviceTable : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            //migrationBuilder.CreateTable(
            //    name: "CustomerDevice",
            //    columns: table => new
            //    {
            //        Id = table.Column<int>(nullable: false)
            //            .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
            //        CreatedDate = table.Column<DateTime>(nullable: false),
            //        DeletedDate = table.Column<DateTime>(nullable: false),
            //        DeviceId = table.Column<string>(nullable: false),
            //        FK_CreatedBy_Id = table.Column<string>(nullable: true),
            //        FK_DeletedBy_Id = table.Column<string>(nullable: true),
            //        FK_UpdatedBy_Id = table.Column<string>(nullable: true),
            //        Fk_Customer_Id = table.Column<int>(nullable: false),
            //        IsDeleted = table.Column<bool>(nullable: false),
            //        Platfrom = table.Column<string>(nullable: false),
            //        UpdatedDate = table.Column<DateTime>(nullable: false)
            //    },
            //    constraints: table =>
            //    {
            //        table.PrimaryKey("PK_CustomerDevice", x => x.Id);
            //    });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            //migrationBuilder.DropTable(
            //    name: "CustomerDevice");
        }
    }
}
