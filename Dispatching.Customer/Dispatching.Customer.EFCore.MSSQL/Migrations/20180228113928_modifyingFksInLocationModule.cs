﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Dispatching.CustomerModule.EFCore.MSSQL.Migrations
{
    public partial class modifyingFksInLocationModule : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "Country",
                table: "Location",
                newName: "Fk_Country_Id");

            migrationBuilder.RenameColumn(
                name: "AddressType",
                table: "Location",
                newName: "Fk_AddressType_Id");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "Fk_Country_Id",
                table: "Location",
                newName: "Country");

            migrationBuilder.RenameColumn(
                name: "Fk_AddressType_Id",
                table: "Location",
                newName: "AddressType");
        }
    }
}
