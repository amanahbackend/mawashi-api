﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Dispatching.CustomerModule.EFCore.MSSQL.Migrations
{
    public partial class modifyingLocationModel : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "Street",
                table: "Location",
                newName: "StreetName");

            migrationBuilder.RenameColumn(
                name: "Governorate",
                table: "Location",
                newName: "StreetId");

            migrationBuilder.RenameColumn(
                name: "Block",
                table: "Location",
                newName: "GovernorateName");

            migrationBuilder.RenameColumn(
                name: "Area",
                table: "Location",
                newName: "GovernorateId");

            migrationBuilder.AddColumn<string>(
                name: "AreaId",
                table: "Location",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "AreaName",
                table: "Location",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "BlockId",
                table: "Location",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "BlockName",
                table: "Location",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "AreaId",
                table: "Location");

            migrationBuilder.DropColumn(
                name: "AreaName",
                table: "Location");

            migrationBuilder.DropColumn(
                name: "BlockId",
                table: "Location");

            migrationBuilder.DropColumn(
                name: "BlockName",
                table: "Location");

            migrationBuilder.RenameColumn(
                name: "StreetName",
                table: "Location",
                newName: "Street");

            migrationBuilder.RenameColumn(
                name: "StreetId",
                table: "Location",
                newName: "Governorate");

            migrationBuilder.RenameColumn(
                name: "GovernorateName",
                table: "Location",
                newName: "Block");

            migrationBuilder.RenameColumn(
                name: "GovernorateId",
                table: "Location",
                newName: "Area");
        }
    }
}
