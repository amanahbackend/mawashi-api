﻿using DispatchProduct.BuildingBlocks.BaseEntities.Entities;
using DispatchProduct.BuildingBlocks.BaseEntities.IEntities;
using System;
using System.Collections.Generic;
using System.Text;

namespace Dispatching.CustomerModule.Models.Context
{
    public interface ILocation : IBaseEntity
    {
         int? Fk_AddressType_Id { get; set; }
         string Number { get; set; }
         int? Fk_Country_Id { get; set; }
         string Name { get; set; }
         string Governorate { get; set; }
         string Area { get; set; }
         string Block { get; set; }
         string Street { get; set; }
         string Building { get; set; }
         string Floor { get; set; }
         string AppartmentNumber { get; set; }
         double? Latitude { get; set; }
         double? Longitude { get; set; }
         string DeliveryNote { get; set; }
         int Fk_Customer_Id { get; set; }
    }
}
