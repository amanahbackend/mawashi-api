﻿using CommonEnums;
using DispatchProduct.BuildingBlocks.BaseEntities.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace Dispatching.CustomerModule.Models.Context
{
    public class Company : BaseEntity
    {
        public string Fk_AppUser_Id { get; set; }
        public string Name { get; set; }
        public string ContactName { get; set; }
        public string ContactPhone { get; set; }
        public string ContactMail { get; set; }
        public double Wallet { get; set; }
        public Country Fk_Country_Id { get; set; }
    }
}
