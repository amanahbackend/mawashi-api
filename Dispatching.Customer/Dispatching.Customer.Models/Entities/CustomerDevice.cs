﻿using DispatchProduct.BuildingBlocks.BaseEntities.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace Dispatching.CustomerModule.Models.Context
{
	public class CustomerDevice : BaseEntity
	{
		public int Fk_Customer_Id { get; set; }
		public string DeviceId { get; set; }
		public string Platform { get; set; }
	}
}
