﻿using CommonEnums;
using DispatchProduct.BuildingBlocks.BaseEntities.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace Dispatching.CustomerModule.Models.Context
{
    public class Customer : BaseEntity
    {
        public string Fk_AppUser_Id { get; set; }
        public Country Fk_Country_Id { get; set; }
    }
}
