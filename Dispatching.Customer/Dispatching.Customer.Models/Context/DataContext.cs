﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;
using Dispatching.CustomerModule.Models.ContextConfiguration;

namespace Dispatching.CustomerModule.Models.Context
{
    public class DataContext : DbContext
    {
        public DataContext(DbContextOptions<DataContext> options) : base(options)
        {
        }

        public DbSet<Customer> Customers { get; set; }
        public DbSet<Company> Company { get; set; }
        public DbSet<Location> Locations { get; set; }
        public DbSet<CompanyLocation> CompanyLocation { get; set; }
     
       
        public DbSet<CustomerDevice> CustomerDevices { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
            modelBuilder.ApplyConfiguration(new CustomerTypeConfiguration());
            modelBuilder.ApplyConfiguration(new LocationTypeConfiguration());
            modelBuilder.ApplyConfiguration(new CompanyTypeConfiguration());
            modelBuilder.ApplyConfiguration(new CustomerDeviceTypeConfiguration());
            modelBuilder.ApplyConfiguration(new CompanyLocationTypeConfiguration());

        }
    }
}
