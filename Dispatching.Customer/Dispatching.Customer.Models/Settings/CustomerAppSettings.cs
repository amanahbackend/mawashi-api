﻿namespace Dispatching.CustomerModule.Models.Settings
{
    public class CustomerAppSettings
    {
        public string DefaultLanguage { get; set; }
    }
}
