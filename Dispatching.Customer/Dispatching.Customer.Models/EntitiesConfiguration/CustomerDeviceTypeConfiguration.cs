﻿using Dispatching.CustomerModule.Models.Context;
using DispatchProduct.BuildingBlocks.BaseEntities.EntityConfigurations;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;

namespace Dispatching.CustomerModule.Models.ContextConfiguration
{
	public class CustomerDeviceTypeConfiguration : BaseEntityTypeConfiguration<CustomerDevice>,
		IEntityTypeConfiguration<CustomerDevice>
	{
		public new void Configure(EntityTypeBuilder<CustomerDevice> builder)
		{
			base.Configure(builder);
			builder.ToTable("CustomerDevice");
			builder.Property(c => c.DeviceId).IsRequired(true);
			builder.Property(c => c.Fk_Customer_Id).IsRequired(true);
			builder.Property(c => c.Platform).IsRequired(true);
		}
	}
}
