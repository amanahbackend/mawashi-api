﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;
using Dispatching.CustomerModule.Models.Context;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using DispatchProduct.BuildingBlocks.BaseEntities.EntityConfigurations;

namespace Dispatching.CustomerModule.Models.ContextConfiguration
{
    public class CustomerTypeConfiguration : BaseEntityTypeConfiguration<Customer>, IEntityTypeConfiguration<Customer>
    {
        public new void Configure(EntityTypeBuilder<Customer> builder)
        {
            base.Configure(builder);
            
            builder.Property(c => c.Fk_AppUser_Id).IsRequired(true);

        }
    }
}
