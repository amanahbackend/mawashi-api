﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using DispatchProduct.BuildingBlocks.BaseEntities.EntityConfigurations;
using Dispatching.CustomerModule.Models.Context;

namespace Dispatching.CustomerModule.Models.ContextConfiguration
{
    public class CompanyTypeConfiguration : BaseEntityTypeConfiguration<Company>, IEntityTypeConfiguration<Company>
    {
        public new void Configure(EntityTypeBuilder<Company> builder)
        {
            base.Configure(builder);
            builder.Property(c => c.Fk_AppUser_Id).IsRequired();
            builder.Property(c => c.Name).IsRequired();
            builder.Property(c => c.ContactName).IsRequired(false);
            builder.Property(c => c.ContactPhone).IsRequired(false);
            builder.Property(c => c.ContactMail).IsRequired(false);
        }
    }
}
