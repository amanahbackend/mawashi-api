﻿using Dispatching.CustomerModule.Models.Context;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using DispatchProduct.BuildingBlocks.BaseEntities.EntityConfigurations;

namespace Dispatching.CustomerModule.Models.ContextConfiguration
{
    public class LocationTypeConfiguration : BaseEntityTypeConfiguration<Location>, IEntityTypeConfiguration<Location>
    {
        public new void Configure(EntityTypeBuilder<Location> builder)
        {
            base.Configure(builder);
            builder.ToTable("Location");
            //builder.Property(l => l.Fk_Country_Id).IsRequired(false);
            builder.Property(l => l.AppartmentNumber).IsRequired(false);
            builder.Property(l => l.AreaName).IsRequired(false);
            builder.Property(l => l.AreaId).IsRequired(false);

            builder.Property(l => l.BlockName).IsRequired(false);
            builder.Property(l => l.BlockId).IsRequired(false);

            builder.Property(l => l.Building).IsRequired(false);
           // builder.Property(l => l.Fk_AddressType_Id).IsRequired(false);
            builder.Property(l => l.Floor).IsRequired(false);
            builder.Property(l => l.GovernorateName).IsRequired(false);
            builder.Property(l => l.GovernorateId).IsRequired(false);

            builder.Property(l => l.Latitude).IsRequired(false);
            builder.Property(l => l.Longitude).IsRequired(false);
            builder.Property(l => l.Number).IsRequired(false);
            builder.Property(l => l.StreetName).IsRequired(false);
            builder.Property(l => l.StreetId).IsRequired(false);

            builder.Property(l => l.Name).IsRequired(true);
            builder.Property(l => l.Fk_Customer_Id).IsRequired(true);
        }
    }
}
