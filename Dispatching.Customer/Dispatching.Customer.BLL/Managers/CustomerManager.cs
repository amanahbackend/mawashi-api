﻿using System;
using System.Collections.Generic;
using System.Text;
using Dispatching.CustomerModule.BLL.IManagers;
using Dispatching.CustomerModule.Models.Context;
using DispatchProduct.RepositoryModule;
using Utilites.ProcessingResult;
using System.Linq;
using CommonEnums;

namespace Dispatching.CustomerModule.BLL.Managers
{
    public class CustomerManager : Repository<Customer>, ICustomerManager
    {
        public CustomerManager(DataContext context) : base(context)
        {
        }
        public ProcessResult<Customer> GetByUserId(string userId)
        {
            ProcessResult<Customer> result = null;
            try
            {
                var customer = GetAllQuerable().Data.FirstOrDefault(c => c.Fk_AppUser_Id.Equals(userId));
                if (customer != null)
                {
                    result = ProcessResultHelper.Succedded(customer);
                }
                else
                {
                    result = ProcessResultHelper.Failed<Customer>(null, null, "Customer is not found");
                }
            }
            catch (Exception ex)
            {
                result = ProcessResultHelper.Failed<Customer>(null, ex, "An error has been occured");
            }
            return result;
        }

        public ProcessResult<List<CustomerCountPerCountryViewModel>> GetCountPerCountry()
        {
            var data = GetAllQuerable().Data.GroupBy(x => x.Fk_Country_Id, (key, c) => new CustomerCountPerCountryViewModel { Country = key, CustomerCount = c.Count() }).ToList();
            return ProcessResultHelper.Succedded(data);
        }
    }

    public class CustomerCountPerCountryViewModel
    {
        public Country Country { get; set; }
        public int CustomerCount { get; set; }

    }
}
