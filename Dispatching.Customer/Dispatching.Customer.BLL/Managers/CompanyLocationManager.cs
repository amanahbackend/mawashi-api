﻿using Dispatching.CustomerModule.BLL.IManagers;
using Dispatching.CustomerModule.Models.Context;
using System;
using System.Collections.Generic;
using System.Text;
using Dispatching.CustomerModule.Models.Context;
using DispatchProduct.RepositoryModule;
using Utilites.ProcessingResult;
using System.Linq;
using CommonEnums;

namespace Dispatching.CustomerModule.BLL.Managers
{
    public class CompanyLocationManager : Repository<CompanyLocation>, ICompanyLocationManager
    {

        public CompanyLocationManager(DataContext context) : base(context)
        {
        }

        public override ProcessResult<CompanyLocation> Add(CompanyLocation entity)
        {
            entity = SetAddressType(entity);
            return base.Add(entity);
        }

        public override ProcessResult<bool> Update(CompanyLocation entity)
        {
            entity = SetAddressType(entity);
            return base.Update(entity);
        }

        private CompanyLocation SetAddressType(CompanyLocation entity)
        {
            if (entity.Fk_Country_Id == Country.Kuwait)
            {
                if (!String.IsNullOrEmpty(entity.Number))
                {
                    entity.Fk_AddressType_Id = AddressType.Paci;
                }
            }
            else if (entity.Fk_Country_Id == Country.UAE)
            {
                if (!String.IsNullOrEmpty(entity.Number))
                {
                    entity.Fk_AddressType_Id = AddressType.Makani;
                }
            }
            return entity;
        }

        public ProcessResult<List<CompanyLocation>> GetByCompanyId(int customerId, Country country)
        {
            try
            {
                List<CompanyLocation> result = null;
                if (country > 0)
                {
                    result = GetAllQuerable().Data.Where(l => l.Fk_Company_Id == customerId && l.Fk_Country_Id == country).ToList();
                }
                else
                {
                    result = GetAllQuerable().Data.Where(l => l.Fk_Company_Id == customerId).ToList();
                }
                return ProcessResultHelper.Succedded(result);
            }
            catch (Exception ex)
            {
                return ProcessResultHelper.Failed<List<CompanyLocation>>(null, ex);
            }
        }
    }
}
