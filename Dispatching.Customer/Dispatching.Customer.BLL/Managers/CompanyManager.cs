﻿using System;
using System.Collections.Generic;
using System.Text;
using Dispatching.CustomerModule.BLL.IManagers;
using Dispatching.CustomerModule.Models.Context;
using DispatchProduct.RepositoryModule;
using Utilites.ProcessingResult;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using Dispatching.CompanyModule.BLL.IManagers;
using CommonEnums;
using Utilites.PaginatedItems;

namespace Dispatching.CustomerModule.BLL.Managers
{
    public class CompanyManager : Repository<Company>, ICompanyManager
    {
        public CompanyManager(DataContext context) : base(context)
        {
        }

        public ProcessResult<Company> GetByUserId(string userId)
        {
            ProcessResult<Company> result = null;
            try
            {
                var Company = GetAllQuerable().Data.FirstOrDefault(c => c.Fk_AppUser_Id.Equals(userId));
                if (Company != null)
                {
                    result = ProcessResultHelper.Succedded(Company);
                }
                else
                {
                    result = ProcessResultHelper.Failed<Company>(null, null, "Company is not found");
                }
            }
            catch (Exception ex)
            {
                result = ProcessResultHelper.Failed<Company>(null, ex, "An error has been occured");
            }
            return result;
        }

        public ProcessResult<List<Company>> GetByCountry(Country country)
        {
            var data = GetAllQuerable().Data.Where(r => r.Fk_Country_Id == country).ToList();
            return ProcessResultHelper.Succedded(data);
        }

        public ProcessResult<PaginatedItems<Company>> GetByCountryPaginated(Country country, PaginatedItems<Company> paginatedItems)
        {
            return GetAllPaginated(paginatedItems, x => x.Fk_Country_Id == country, x => x.CreatedDate);
        }

    }
}
