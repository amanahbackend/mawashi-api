﻿using Dispatching.CustomerModule.BLL.IManagers;
using Dispatching.CustomerModule.Models.Context;
using DispatchProduct.RepositoryModule;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Utilites.ProcessingResult;

namespace Dispatching.CustomerModule.BLL.Managers
{
	public class CustomerDeviceManager : Repository<CustomerDevice>, ICustomerDeviceManager
	{
		public CustomerDeviceManager(DataContext context) : base(context)
		{
		}

		public ProcessResult<List<CustomerDevice>> GetCustomerDevices(int customerId)
		{
			ProcessResult<List<CustomerDevice>> result = null;
			try
			{
				var customerDevices = GetAllQuerable().Data.Where(c => c.Fk_Customer_Id == customerId).ToList();
				if (customerDevices != null)
				{
					result = ProcessResultHelper.Succedded(customerDevices);
				}
				else
				{
					result = ProcessResultHelper.Failed<List<CustomerDevice>>(null, null, "Customer has not devices");
				}
			}
			catch (Exception ex)
			{
				result = ProcessResultHelper.Failed<List<CustomerDevice>>(null, ex, "An error has been occured");
			}
			return result;
		}
	}
}
