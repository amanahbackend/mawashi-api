﻿using CommonEnums;
using Dispatching.CustomerModule.Models.Context;
using DispatchProduct.RepositoryModule;
using System;
using System.Collections.Generic;
using System.Text;
using Utilites.ProcessingResult;

namespace Dispatching.CustomerModule.BLL.IManagers
{
    public interface ICompanyLocationManager : IRepository<CompanyLocation>
    {
        ProcessResult<List<CompanyLocation>> GetByCompanyId(int customerId, Country country);
    }
}
