﻿using CommonEnums;
using Dispatching.CustomerModule.Models.Context;
using DispatchProduct.RepositoryModule;
using System;
using System.Collections.Generic;
using System.Text;
using Utilites.ProcessingResult;

namespace Dispatching.CustomerModule.BLL.IManagers
{
    public interface ILocationManager : IRepository<Location>
    {
        ProcessResult<List<Location>> GetByCustomerId(int customerId, Country country);
    }
}
