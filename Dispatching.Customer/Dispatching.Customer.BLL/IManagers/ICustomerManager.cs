﻿using DispatchProduct.RepositoryModule;
using Dispatching.CustomerModule.Models.Context;
using System.Collections.Generic;
using Utilites.ProcessingResult;
using Dispatching.CustomerModule.BLL.Managers;

namespace Dispatching.CustomerModule.BLL.IManagers
{
    public interface ICustomerManager : IRepository<Customer>
    {
        ProcessResult<Customer> GetByUserId(string userId);
        ProcessResult<List<CustomerCountPerCountryViewModel>> GetCountPerCountry();
    }
}
