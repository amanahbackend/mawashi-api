﻿using CommonEnums;
using Dispatching.CustomerModule.Models.Context;
using DispatchProduct.RepositoryModule;
using System;
using System.Collections.Generic;
using System.Text;
using Utilites.PaginatedItems;
using Utilites.ProcessingResult;

namespace Dispatching.CompanyModule.BLL.IManagers
{
    public interface ICompanyManager : IRepository<Company>
    {
        ProcessResult<Company> GetByUserId(string userId);
        ProcessResult<List<Company>> GetByCountry(Country country);

        ProcessResult<PaginatedItems<Company>> GetByCountryPaginated(Country country,
            PaginatedItems<Company> paginatedItems);
    }
}
