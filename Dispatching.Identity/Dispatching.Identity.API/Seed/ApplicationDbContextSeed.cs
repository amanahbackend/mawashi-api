﻿using Dispatching.Identity.BLL.IManagers;
using Dispatching.Identity.Models.Context;
using Dispatching.Identity.Models.Entities;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.IO;
using System.IO.Compression;
using System.Linq;
using System.Threading.Tasks;

namespace Dispatching.Identity.API.Seed
{
    public class ApplicationDbContextSeed
    {
        IApplicationUserManager applicationUserManager;
        IApplicationRoleManager applicationRoleManager;

        public ApplicationDbContextSeed(IApplicationUserManager _applicationUserManager,
            IApplicationRoleManager _applicationRoleManager)
        {
            applicationUserManager = _applicationUserManager;
            applicationRoleManager = _applicationRoleManager;
        }
        public async Task SeedAsync(ApplicationDbContext context, IHostingEnvironment env,
            ILogger<ApplicationDbContextSeed> logger, int? retry = 0)
        {
            int retryForAvaiability = retry.Value;
            try
            {
                if (!context.Roles.Any())
                {
                    await applicationRoleManager.AddRolesAsync(GetDefaultRoles());
                }
                if (!context.Users.Any())
                {
                    await applicationUserManager.AddUsersAsync(GetDefaultUsers());
                }
                //if (!context.Countries.Any())
                //{
                //    countryManager.Seed();
                //}
            }
            catch (Exception ex)
            {
                if (retryForAvaiability < 10)
                {
                    retryForAvaiability++;

                    logger.LogError(ex.Message, $"There is an error migrating data for ApplicationDbContext");

                    await SeedAsync(context, env, logger, retryForAvaiability);
                }
            }
        }

        private List<Tuple<ApplicationUser, string>> GetDefaultUsers()
        {
            var user1 = new ApplicationUser()
            {
                FirstName = "admin",
                LastName = "admin",
                Phone1 = "12345678",
                UserName = "admin1",
                Email = "admin@microsoft.com",
                RoleNames = new List<string> { "Admin" },
                Deactivated = false,
                PhoneNumberConfirmed = true,
                Fk_Country_Id = CommonEnums.Country.UAE
            };
            var user2 = new ApplicationUser()
            {
                FirstName = "customer",
                LastName = "customer",
                Phone1 = "87654321",
                UserName = "customer1",
                Email = "Customer@microsoft.com",
                RoleNames = new List<string> { "Customer" },
                Deactivated = false,
                PhoneNumberConfirmed = true,
                Fk_Country_Id = CommonEnums.Country.UAE
            };
            var user3 = new ApplicationUser()
            {
                FirstName = "super",
                LastName = "admin",
                Phone1 = "56487321",
                UserName = "Superadmin",
                Email = "Superadmin@microsoft.com",
                RoleNames = new List<string> { "Superadmin" },
                Deactivated = false,
                PhoneNumberConfirmed = true,
                Fk_Country_Id = CommonEnums.Country.UAE
            };
            return new List<Tuple<ApplicationUser, string>>()
            {
                new Tuple<ApplicationUser, string>(user1,"P@ssw0rd"),
                new Tuple<ApplicationUser, string>(user2,"P@ssw0rd"),
                new Tuple<ApplicationUser, string>(user3,"Mawashi@s0per")
            };
        }

        private List<ApplicationRole> GetDefaultRoles()
        {
            return new List<ApplicationRole>()
            {
                new ApplicationRole(){Name="Admin"},
                new ApplicationRole(){Name="Customer"},
                new ApplicationRole(){Name="Superadmin"},
                new ApplicationRole(){Name="CustomerService"},
                new ApplicationRole(){Name="Logistics"},
                new ApplicationRole(){Name="Production"},
                new ApplicationRole(){Name="Guest"},
                new ApplicationRole(){Name="Sales"},
                new ApplicationRole(){Name="Fleet"},
                new ApplicationRole(){Name="Accountant"},
                new ApplicationRole(){Name="Driver"}
            };
        }


    }
}
