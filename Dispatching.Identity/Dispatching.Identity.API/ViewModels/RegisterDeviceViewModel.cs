﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Dispatching.Identity.API.ViewModels
{
    public class RegisterDeviceViewModel
    {
        public string UserId { get; set; }
        public string DeviceId { get; set; }
    }
}
