﻿using CommonEnums;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Dispatching.Identity.API.Controllers
{
    [ApiVersion("1.0")]
    [Route("v{version:apiVersion}/[controller]")]
    public class LanguageController : Controller
    {
        [Route("GetAll")]
        [HttpGet]
        public List<EnumEntity> GetAll()
        {
            return EnumManager<SupportedLanguage>.GetEnumList();
        }
    }
}
