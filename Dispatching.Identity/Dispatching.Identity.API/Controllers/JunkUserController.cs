﻿using AutoMapper;
using Dispatching.Identity.API.ViewModels;
using Dispatching.Identity.BLL.IManagers;
using Dispatching.Identity.Models.Entities;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Utilites.ProcessingResult;

namespace Dispatching.Identity.API.Controllers
{
    [ApiVersion("1.0")]
    [Route("v{version:apiVersion}/[controller]")]
    public class JunkUserController : Controller
    {
        private IJunkUserManager _junkUserManager;

        public JunkUserController(IJunkUserManager junkUserManager)
        {
            _junkUserManager = junkUserManager;
        }

        [HttpGet, Route("GetAll"), MapToApiVersion("1.0")]
        public ProcessResultViewModel<List<JunkUserViewModel>> GetAll()
        {
            ProcessResultViewModel<List<JunkUserViewModel>> result = null;
            try
            {
                var junkUsers = _junkUserManager.GetAll().Data.ToList();
                var junkUsersModel = Mapper.Map<List<JunkUser>, List<JunkUserViewModel>>(junkUsers);
                result = ProcessResultViewModelHelper.Succedded(junkUsersModel);
            }
            catch (Exception ex)
            {
                result = ProcessResultViewModelHelper.Failed<List<JunkUserViewModel>>(null, ex.Message);
            }
            return result;
        }

        [HttpDelete, Route("Delete"), MapToApiVersion("1.0")]
        public ProcessResultViewModel<bool> Delete([FromQuery] string username)
        {
            ProcessResultViewModel<bool> result = null;
            try
            {
                bool isDeleted = _junkUserManager.DeleteByUsername(username).Data;
                result = ProcessResultViewModelHelper.Succedded(isDeleted);
            }
            catch (Exception ex)
            {
                result = ProcessResultViewModelHelper.Failed(false, ex.Message);
            }
            return result;
        }
    }
}
