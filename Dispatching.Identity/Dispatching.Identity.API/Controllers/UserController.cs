﻿using AutoMapper;
using Dispatching.Identity.API.Utilities;
using Dispatching.Identity.API.ViewModels;
using Dispatching.Identity.BLL.IManagers;
using Dispatching.Identity.Models.Entities;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Net.Http.Headers;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Threading.Tasks;
using Utilites.ProcessingResult;
using Utilites.UploadFile;
using DnsClient;
using Utilities.Utilites.GenericListToExcel;
using Utilites;
using System.IO;
using CommonEnums;
using Dispatching.Identity.API.ServiceCommunications.Dropout;
using Dispatching.Identity.API.ServicesViewModels.Dropout;
using Utilites.PaginatedItems;

namespace Dispatching.Identity.API.Controllers
{
    [ApiVersion("1.0")]
    [Route("v{version:apiVersion}/[controller]")]
    public class UserController : Controller
    {
        private readonly IHostingEnvironment _hostingEnv;
        private readonly IApplicationUserManager _applicationUserManager;
        private readonly IConfigurationRoot _configuration;
        private readonly IPasswordTokenPinManager _passwordTokenPinManager;
        private readonly IPasswordHasher<ApplicationUser> _passwordHasher;
        private readonly IUploadImageFileManager _imageManager;
        private readonly IDropoutService _dropoutService;
        private readonly IUserDeviceManager _userDeviceManager;

        public UserController(IApplicationUserManager applicationUserManager,
            IHostingEnvironment hostingEnv, IConfigurationRoot configuration,
            IPasswordTokenPinManager passwordTokenPinManager, IPasswordHasher<ApplicationUser> passwordHasher,
             IUploadImageFileManager imageManager, IDropoutService dropoutService, IUserDeviceManager userDeviceManager)
        {
            _dropoutService = dropoutService;
            _userDeviceManager = userDeviceManager;
            _passwordHasher = passwordHasher;
            _passwordTokenPinManager = passwordTokenPinManager;
            _configuration = configuration;
            _hostingEnv = hostingEnv;
            _applicationUserManager = applicationUserManager;
            _imageManager = imageManager;
        }


        [HttpPost, Route("Login"), MapToApiVersion("1.0")]
        public async Task<IActionResult> Login([FromBody]LoginViewModel loginViewModel)
        {
            var response = await TokenManager.GetToken(loginViewModel, _applicationUserManager, _passwordHasher, _userDeviceManager);
            if (response != null && response.Item2 != null && response.Item1 != null)
            {
                return Ok(new
                {
                    Token = new JwtSecurityTokenHandler().WriteToken(response.Item2),
                    response.Item1.UserName,
                    Name = response.Item1.FirstName + " " + response.Item1.LastName,
                    Roles = response.Item1.RoleNames,
                    userId = response.Item1.Id,
                    response.Item1.Fk_Country_Id,
                    response.Item1.Language
                });
            }
            else
            {
                return Unauthorized();
            }
        }

        [HttpPost, Route("RegisterDevice"), MapToApiVersion("1.0")]
        public IActionResult RegisterDevice([FromBody]RegisterDeviceViewModel model)
        {
            _userDeviceManager.AddIfNotExist(new UserDevice
            {
                DeveiceId = model.DeviceId,
                Fk_AppUser_Id = model.UserId
            });
            return Ok();
        }

        [HttpPost, Route("UnregisterDevice"), MapToApiVersion("1.0")]
        public IActionResult UnregisterDevice([FromBody]RegisterDeviceViewModel model)
        {
            _userDeviceManager.DeleteDevice(model.DeviceId);
            return Ok();
        }
        [HttpGet, Route("Test"), MapToApiVersion("1.0")]
        public IActionResult Test()
        {
            //var currentUserName = HttpContext.User.FindFirst(ClaimTypes.NameIdentifier).Value;
            var currentUserName2 = HttpContext.User.FindFirst(JwtRegisteredClaimNames.Sub).Value;
            var currentUserId = HttpContext.User.FindFirst(JwtRegisteredClaimNames.Jti).Value;
            return Ok();
        }

        [HttpPost, Route("Add"), MapToApiVersion("1.0")]
        public async Task<ProcessResultViewModel<ApplicationUserViewModel>> Add([FromBody] ApplicationUserViewModel userViewModel)
        {
            ProcessResultViewModel<ApplicationUserViewModel> result = null;
            try
            {
                if (userViewModel.Fk_Country_Id == 0)
                {
                    result = ProcessResultViewModelHelper.Failed<ApplicationUserViewModel>(null, "Country should be filled.");
                }
                else
                {
                    if (userViewModel.Picture != null)
                    {
                        userViewModel.PicturePath = ProfilePictureManager.SaveProfilePicture(userViewModel.Picture,
                                                                     userViewModel.UserName, _imageManager, _hostingEnv);
                    }
                    ApplicationUser user = Mapper.Map<ApplicationUserViewModel, ApplicationUser>(userViewModel);

                    var userAddResult = await _applicationUserManager.AddUserAsync(user, userViewModel.Password);
                    if (userAddResult.IsSucceeded)
                    {
                        user = userAddResult.Data;
                        userViewModel = Mapper.Map<ApplicationUser, ApplicationUserViewModel>(user);
                        result = ProcessResultViewModelHelper.Succedded(userViewModel);
                    }
                    else
                    {
                        ProfilePictureManager.DeleteProfilePicture(user.UserName, _hostingEnv);
                        result = ProcessResultViewModelHelper.Failed<ApplicationUserViewModel>(null, userAddResult.Exception.Message);
                    }
                }
            }
            catch (Exception ex)
            {
                result = ProcessResultViewModelHelper.Failed<ApplicationUserViewModel>(null, ex.Message);
            }
            return result;
        }

        [HttpGet, Route("GetBy"), MapToApiVersion("1.0")]
        public async Task<ProcessResultViewModel<ApplicationUserViewModel>> GetBy([FromQuery] string username)
        {
            ProcessResultViewModel<ApplicationUserViewModel> result = null;
            try
            {
                ApplicationUser user = await _applicationUserManager.GetBy(username);
                if (user != null)
                {
                    //user.Picture = ProfilePictureManager.GetProfilePictureBase64(username, _hostingEnv);
                    var userModel = Mapper.Map<ApplicationUser, ApplicationUserViewModel>(user);
                    userModel = ProfilePictureManager.BindFileURL(userModel, _configuration);

                    result = ProcessResultViewModelHelper.Succedded(userModel);
                }
                else
                {
                    result = ProcessResultViewModelHelper.Failed<ApplicationUserViewModel>(null, "User not found");
                }

            }
            catch (Exception ex)
            {
                result = ProcessResultViewModelHelper.Failed<ApplicationUserViewModel>(null, ex.Message);
            }
            return result;
        }

        [HttpPut, Route("Update"), MapToApiVersion("1.0")]
        public async Task<ProcessResultViewModel<ApplicationUserViewModel>> Update([FromBody]ApplicationUserViewModel userViewModel)
        {
            if (userViewModel.Picture != null)
            {
                userViewModel.PicturePath = ProfilePictureManager.SaveProfilePicture(userViewModel.Picture,
                                                                 userViewModel.UserName, _imageManager, _hostingEnv);
            }
            var user = Mapper.Map<ApplicationUserViewModel, ApplicationUser>(userViewModel);
            var updateUserResult = await _applicationUserManager.UpdateUserAsync(user);
            if (updateUserResult.IsSucceeded)
            {
                user = updateUserResult.Data;
                userViewModel = Mapper.Map<ApplicationUser, ApplicationUserViewModel>(user);
                return ProcessResultViewModelHelper.Succedded(userViewModel);
            }
            else
            {
                return ProcessResultViewModelHelper.Failed<ApplicationUserViewModel>(null, updateUserResult.Exception.Message);
            }

        }

        [HttpGet, Route("GetAll"), MapToApiVersion("1.0")]
        public async Task<ProcessResultViewModel<List<ApplicationUserViewModel>>> GetAll()
        {
            ProcessResultViewModel<List<ApplicationUserViewModel>> result = null;
            try
            {
                var users = await _applicationUserManager.GetAll();
                if (users.Count > 0)
                {
                    List<ApplicationUserViewModel> userModels = Mapper.Map<List<ApplicationUser>, List<ApplicationUserViewModel>>(users);
                    result = ProcessResultViewModelHelper.Succedded(userModels);
                }
                else
                {
                    result = ProcessResultViewModelHelper.Failed<List<ApplicationUserViewModel>>(null, "No users found");
                }
            }
            catch (Exception ex)
            {
                result = ProcessResultViewModelHelper.Failed<List<ApplicationUserViewModel>>(null, ex.Message);
            }
            return result;
        }

        [HttpPost, Route("GetCustomerUsersPaginatedByCountry/{country}"), MapToApiVersion("1.0")]
        public async Task<IdentityPaginatedItems<ApplicationUserViewModel>> GetCustomerUsersPaginatedByCountry
            ([FromRoute]Country country, [FromBody] IdentityPaginatedItems<ApplicationUserViewModel> model)
        {
            var paginatedItems = new IdentityPaginatedItems<ApplicationUser>()
            {
                Count = model.Count,
                PageNo = model.PageNo,
                PageSize = model.PageSize
            };
            var resultData = await _applicationUserManager.GetAllCustomersPaginated(paginatedItems, country);
            model.Count = resultData.Count;
            model.PageNo = resultData.PageNo;
            model.PageSize = resultData.PageSize;
            model.Data = Mapper.Map<List<ApplicationUser>, List<ApplicationUserViewModel>>(resultData.Data);
            return model;
        }

        [HttpPost, Route("GetCompanyUsersPaginatedByCountry/{country}"), MapToApiVersion("1.0")]
        public async Task<IdentityPaginatedItems<ApplicationUserViewModel>> GetCompanyUsersPaginatedByCountry
            ([FromRoute]Country country, [FromBody] IdentityPaginatedItems<ApplicationUserViewModel> model)
        {
            var paginatedItems = new IdentityPaginatedItems<ApplicationUser>()
            {
                Count = model.Count,
                PageNo = model.PageNo,
                PageSize = model.PageSize
            };
            var resultData = await _applicationUserManager.GetAllCompaniesPaginated(paginatedItems, country);
            model.Count = resultData.Count;
            model.PageNo = resultData.PageNo;
            model.PageSize = resultData.PageSize;
            model.Data = Mapper.Map<List<ApplicationUser>, List<ApplicationUserViewModel>>(resultData.Data);
            return model;
        }

        [HttpDelete, Route("Delete"), MapToApiVersion("1.0")]
        public async Task<ProcessResultViewModel<bool>> Delete([FromQuery] string username)
        {
            ProcessResultViewModel<bool> result = null;
            try
            {
                var isDeleted = await _applicationUserManager.DeleteAsync(username);
                if (isDeleted)
                {
                    ProfilePictureManager.DeleteProfilePicture(username, _hostingEnv);
                    result = ProcessResultViewModelHelper.Succedded(isDeleted);
                }
                else
                {
                    result = ProcessResultViewModelHelper.Failed(false, "Failed to delete user");
                }
            }
            catch (Exception ex)
            {
                result = ProcessResultViewModelHelper.Failed(false, ex.Message);
            }
            return result;
        }

        [HttpDelete, Route("DeleteById"), MapToApiVersion("1.0")]
        public async Task<ProcessResultViewModel<bool>> DeleteById([FromQuery] string id)
        {
            ProcessResultViewModel<bool> result = null;
            try
            {
                var user = await _applicationUserManager.Get(id);
                var username = user?.UserName;
                var isDeleted = await _applicationUserManager.DeleteByIdAsunc(id);
                if (isDeleted)
                {
                    ProfilePictureManager.DeleteProfilePicture(username, _hostingEnv);
                    result = ProcessResultViewModelHelper.Succedded(isDeleted);
                }
                else
                {
                    result = ProcessResultViewModelHelper.Failed(false, "Failed to delete user");
                }
            }
            catch (Exception ex)
            {
                result = ProcessResultViewModelHelper.Failed(false, ex.Message);
            }
            return result;
        }

        [HttpGet, Route("GetByRole"), MapToApiVersion("1.0")]
        public async Task<ProcessResultViewModel<List<ApplicationUserViewModel>>> GetByRole([FromQuery] string role, [FromRoute]Country country = 0)
        {
            ProcessResultViewModel<List<ApplicationUserViewModel>> result = null;
            try
            {
                var users = (List<ApplicationUser>)await _applicationUserManager.GetUsersInRole(role, country);
                if (users.Count > 0)
                {
                    var userModels = Mapper.Map<List<ApplicationUser>, List<ApplicationUserViewModel>>(users);
                    result = ProcessResultViewModelHelper.Succedded(userModels);
                }
                else
                {
                    result = ProcessResultViewModelHelper.Failed<List<ApplicationUserViewModel>>(null, "No users with this role");
                }
            }
            catch (Exception ex)
            {
                result = ProcessResultViewModelHelper.Failed<List<ApplicationUserViewModel>>(null, ex.Message);
            }
            return result;
        }

        [HttpGet, Route("IsEmailExist"), MapToApiVersion("1.0")]
        public async Task<ProcessResultViewModel<bool>> IsEmailExist([FromQuery]string email)
        {
            ProcessResultViewModel<bool> result = null;
            try
            {
                var isEmailExist = await _applicationUserManager.IsEmailExistAsync(email);
                result = ProcessResultViewModelHelper.Succedded(isEmailExist);
            }
            catch (Exception ex)
            {
                result = ProcessResultViewModelHelper.Failed(false, ex.Message);
            }
            return result;
        }

        [HttpGet, Route("IsUsernameExist"), MapToApiVersion("1.0")]
        public async Task<ProcessResultViewModel<bool>> IsUsernameExist([FromQuery]string username)
        {
            ProcessResultViewModel<bool> result = null;
            try
            {
                var isExisting = await _applicationUserManager.IsUserNameExistAsync(username);
                result = ProcessResultViewModelHelper.Succedded(isExisting);
            }
            catch (Exception ex)
            {
                result = ProcessResultViewModelHelper.Failed(false, ex.Message);
            }
            return result;
        }

        [HttpGet, Route("IsPhoneExist"), MapToApiVersion("1.0")]
        public ProcessResultViewModel<bool> IsPhoneExist([FromQuery] string phone)
        {
            ProcessResultViewModel<bool> result = null;
            try
            {
                var isPhoneExist = _applicationUserManager.IsPhoneExist(phone);
                result = ProcessResultViewModelHelper.Succedded(isPhoneExist);
            }
            catch (Exception ex)
            {
                result = ProcessResultViewModelHelper.Failed(false, ex.Message);
            }
            return result;
        }

        [HttpGet, Route("Search"), MapToApiVersion("1.0")]
        public async Task<ProcessResultViewModel<List<ApplicationUserViewModel>>> Search([FromQuery] string searchToken)
        {
            ProcessResultViewModel<List<ApplicationUserViewModel>> result = null;
            try
            {
                string[] searchFields = _configuration.GetSection("UserSearchFields").Get<string[]>();
                var users = await _applicationUserManager.Search(searchToken, searchFields);
                if (users.Count > 0)
                {
                    var userModels = Mapper.Map<List<ApplicationUser>, List<ApplicationUserViewModel>>(users);
                    result = ProcessResultViewModelHelper.Succedded(userModels);
                }
                else
                {
                    result = ProcessResultViewModelHelper.Succedded<List<ApplicationUserViewModel>>(null, "No users found");
                }
            }
            catch (Exception ex)
            {
                result = ProcessResultViewModelHelper.Failed<List<ApplicationUserViewModel>>(null, ex.Message);
            }
            return result;
        }

        [HttpGet, Route("Get"), MapToApiVersion("1.0")]
        public async Task<ProcessResultViewModel<ApplicationUserViewModel>> Get([FromQuery]string id)
        {
            var user = await _applicationUserManager.Get(id);
            if (user != null)
            {
                var userModel = Mapper.Map<ApplicationUser, ApplicationUserViewModel>(user);
                userModel = ProfilePictureManager.BindFileURL(userModel, _configuration);
                return ProcessResultViewModelHelper.Succedded(userModel);
            }
            else
            {
                return ProcessResultViewModelHelper.Failed<ApplicationUserViewModel>(null, "User can not be found");
            }
        }

        [HttpGet, Route("Deactivate"), MapToApiVersion("1.0")]
        public async Task<ProcessResultViewModel<bool>> Deactivate([FromQuery] string username)
        {
            ProcessResultViewModel<bool> result = null;
            try
            {
                var isDeactivated = await _applicationUserManager.Deactivate(username);
                result = ProcessResultViewModelHelper.Succedded(isDeactivated);
            }
            catch (Exception ex)
            {
                result = ProcessResultViewModelHelper.Failed(false, ex.Message);
            }
            return result;
        }

        [HttpGet, Route("Activate"), MapToApiVersion("1.0")]
        public async Task<ProcessResultViewModel<bool>> Activate([FromQuery] string username)
        {
            ProcessResultViewModel<bool> result = null;
            try
            {
                var isActivated = await _applicationUserManager.Activate(username);
                result = ProcessResultViewModelHelper.Succedded(isActivated);
            }
            catch (Exception ex)
            {
                result = ProcessResultViewModelHelper.Failed(false, ex.Message);
            }
            return result;
        }

        [HttpPost, Route("GetPhoneVerficationToken"), MapToApiVersion("1.0")]
        public async Task<ProcessResultViewModel<string>> GetPhoneVerficationToken([FromBody] PhoneValidationViewModel model)
        {
            ProcessResultViewModel<string> result = null;
            try
            {
                var code = await _applicationUserManager.GeneratePhoneNumberToken(model.Username, model.Phone);
                SendSmsViewModel sendSmsViewModel = new SendSmsViewModel
                {
                    Message = $"Mawashi verification code: {code}",
                    PhoneNumber = model.Phone
                };
                if (model.Country == Country.Kuwait)
                {
                    _dropoutService.SendKw("v1", sendSmsViewModel);
                }
                else if (model.Country == Country.UAE)
                {
                    _dropoutService.SendUae("v1", sendSmsViewModel);
                }
                var user = await _applicationUserManager.GetBy(model.Username);
                _dropoutService.SendMail("v1", new SendMailViewModel
                {
                    Subject = "Mawashi: Activate Your Account",
                    To = new List<string> { user.Email },
                    Body = $"<h2>Welcome {user.FirstName} {user.LastName}</h2><hr/><h4>Mawashi verification code: {code}</h4>"
                });
                result = ProcessResultViewModelHelper.Succedded(code);
            }
            catch (Exception ex)
            {
                result = ProcessResultViewModelHelper.Failed<string>(null, ex.Message);
            }
            return result;
        }

        [HttpPost, Route("IsPhoneVerificationTokenValid"), MapToApiVersion("1.0")]
        public async Task<ProcessResultViewModel<bool>> IsPhoneVerificationTokenValid([FromBody] PhoneValidationViewModel model)
        {
            ProcessResultViewModel<bool> result = null;
            try
            {
                var isValid = await _applicationUserManager.CheckPhoneValidationToken(model.Username, model.Phone, model.Code);
                result = ProcessResultViewModelHelper.Succedded(isValid);
            }
            catch (Exception ex)
            {
                result = ProcessResultViewModelHelper.Failed(false, ex.Message);
            }
            return result;
        }

        [HttpPost, Route("ConfirmPhone"), MapToApiVersion("1.0")]
        public async Task<ProcessResultViewModel<bool>> ConfirmPhone([FromBody]PhoneValidationViewModel model)
        {
            ProcessResultViewModel<bool> result = null;
            try
            {
                var confirmed = await _applicationUserManager.ConfirmPhone(model.Username, model.Phone, model.Code);
                result = ProcessResultViewModelHelper.Succedded(confirmed);
            }
            catch (Exception ex)
            {
                result = ProcessResultViewModelHelper.Failed(false, ex.Message);
            }
            return result;
        }

        [HttpGet, Route("GenerateForgetPasswordToken"), MapToApiVersion("1.0")]
        public async Task<ProcessResultViewModel<string>> GenerateForgetPasswordToken([FromQuery]string username)
        {
            ProcessResultViewModel<string> result = null;
            try
            {
                var token = await _applicationUserManager.GenerateForgetPasswordToken(username);
                // we will implement send sms with this token to the user phone
                var user = await _applicationUserManager.GetBy(username);

                SendSmsViewModel sendSmsViewModel = new SendSmsViewModel
                {
                    Message = $"Mawashi forget password verification code: {token}",
                    PhoneNumber = user.Phone1
                };
                if (user.Fk_Country_Id == Country.Kuwait)
                {
                    _dropoutService.SendKw("v1", sendSmsViewModel);
                }
                else if (user.Fk_Country_Id == Country.UAE)
                {
                    _dropoutService.SendUae("v1", sendSmsViewModel);
                }
                _dropoutService.SendMail("v1", new SendMailViewModel
                {
                    Subject = "Mawashi: Forget Password",
                    To = new List<string> { user.Email },
                    Body = $"<h2>Hello {user.FirstName} {user.LastName}</h2><hr/><h4>Mawashi forget password verification code: {token}</h4>"
                });

                result = ProcessResultViewModelHelper.Succedded(token);
            }
            catch (Exception ex)
            {
                result = ProcessResultViewModelHelper.Failed<string>(null, ex.Message);
            }
            return result;
        }

        [HttpPost, Route("ForgetPassword"), MapToApiVersion("1.0")]
        public async Task<ProcessResultViewModel<bool>> ForgetPassword([FromBody]ChangePasswordViewModel model)
        {
            ProcessResultViewModel<bool> result = null;
            try
            {
                var changed = await _applicationUserManager.ForgetPassword(model.Username, model.NewPassword, model.Code);
                result = ProcessResultViewModelHelper.Succedded(changed);
            }
            catch (Exception ex)
            {
                result = ProcessResultViewModelHelper.Failed(false, ex.Message);
            }
            return result;
        }

        [HttpPost, Route("ChangePassword"), MapToApiVersion("1.0")]
        public async Task<ProcessResultViewModel<bool>> ChangePassword([FromBody]ChangePasswordViewModel model)
        {
            ProcessResultViewModel<bool> result = null;
            try
            {
                var changed = await _applicationUserManager.ChangePassword(model.Username, model.OldPassword, model.NewPassword);
                result = ProcessResultViewModelHelper.Succedded(changed);
            }
            catch (Exception ex)
            {
                result = ProcessResultViewModelHelper.Failed(false, ex.Message);
            }
            return result;
        }

        [HttpPost, Route("ResetPassword"), MapToApiVersion("1.0")]
        public async Task<ProcessResultViewModel<bool>> ResetPassword([FromBody]ChangePasswordViewModel model)
        {
            ProcessResultViewModel<bool> result = null;
            try
            {
                var isReset = await _applicationUserManager.ResetPassword(model.Username, model.NewPassword);
                result = ProcessResultViewModelHelper.Succedded(isReset);
            }
            catch (Exception ex)
            {
                result = ProcessResultViewModelHelper.Failed(false, ex.Message);
            }
            return result;
        }

        [HttpGet, Route("IsPasswordPinExist"), MapToApiVersion("1.0")]
        public ProcessResultViewModel<bool> IsPasswordPinExist([FromQuery]string pin)
        {
            ProcessResultViewModel<bool> result = null;
            try
            {
                var isExist = _passwordTokenPinManager.IsPinExist(pin);
                result = ProcessResultViewModelHelper.Succedded(isExist);
            }
            catch (Exception ex)
            {
                result = ProcessResultViewModelHelper.Failed(false, ex.Message);
            }
            return result;
        }

        [HttpGet, Route("IsTokenValid"), MapToApiVersion("1.0")]
        public ProcessResultViewModel<bool> IsTokenValid()
        {
            ProcessResultViewModel<bool> result = null;
            try
            {
                string authorizationValue = HttpContext.Request.Headers[HeaderNames.Authorization];
                string token = authorizationValue.Substring("Bearer ".Length).Trim();
                bool isValid = TokenManager.ValidateToken(token);
                result = ProcessResultViewModelHelper.Succedded(isValid);
            }
            catch (Exception ex)
            {
                result = ProcessResultViewModelHelper.Failed(false, ex.Message);
            }
            return result;
        }

        [HttpPost, Route("GetByIds"), MapToApiVersion("1.0")]
        public async Task<ProcessResultViewModel<List<ApplicationUserViewModel>>> GetByIds([FromBody]List<string> ids)
        {
            ProcessResultViewModel<List<ApplicationUserViewModel>> result = null;
            try
            {
                var users = await _applicationUserManager.GetByIds(ids);
                var models = Mapper.Map<List<ApplicationUser>, List<ApplicationUserViewModel>>(users);
                result = ProcessResultViewModelHelper.Succedded(models);
            }
            catch (Exception ex)
            {
                result = ProcessResultViewModelHelper.Failed<List<ApplicationUserViewModel>>(null, ex.Message);
            }
            return result;
        }

        [HttpPost, Route("AssignToRole"), MapToApiVersion("1.0")]
        public async Task<ProcessResultViewModel<bool>> AssignToRole([FromBody]AssignToRoleViewModel model)
        {
            ProcessResultViewModel<bool> result = null;
            try
            {
                var user = await _applicationUserManager.Get(model.UserId);
                user.RoleNames = model.Roles.ToList();
                var isSucceed = await _applicationUserManager.AddUserToRolesAsync(user);
                result = ProcessResultViewModelHelper.Succedded(isSucceed);
            }
            catch (Exception ex)
            {
                result = ProcessResultViewModelHelper.Failed(false, ex.Message);
            }
            return result;
        }

        [HttpGet, Route("SearchToExcel"), MapToApiVersion("1.0")]
        public async Task<ProcessResultViewModel<object>> SearchToExcel([FromQuery] string searchToken)
        {
            ProcessResultViewModel<object> result = null;
            try
            {
                string[] searchFields = _configuration.GetSection("UserSearchFields").Get<string[]>();
                var users = await _applicationUserManager.Search(searchToken, searchFields);
                if (users.Count > 0)
                {
                    var userModels = Mapper.Map<List<ApplicationUser>, List<UserSearchResultViewModel>>(users);
                    foreach (var item in userModels)
                    {
                        var user = users.FirstOrDefault(u => u.Id == item.Id);
                        if (user.Fk_Country_Id > 0)
                        {
                            item.CountryCode = EnumManager<Country>.GetName(user.Fk_Country_Id);
                        }
                    }
                    var webRootInfo = _hostingEnv.ContentRootPath;
                    var dirPath = webRootInfo + @"/wwwroot/Reports";
                    FileUtilities.CreateIfMissing(dirPath);
                    string fileName = $"UserSearchResult_{ DateTime.UtcNow.ToString("MMddyyyy")}.xls";
                    var filePath = $"{dirPath}/{fileName}";

                    ListToExcelHelper.WriteObjectsToExcel(userModels, filePath);
                    result = ProcessResultViewModelHelper.Succedded<object>(new { fileName });
                }
                else
                {
                    result = ProcessResultViewModelHelper.Failed<object>(null, "No users found");
                }
            }
            catch (Exception ex)
            {
                result = ProcessResultViewModelHelper.Failed<object>(null, ex.Message);
            }
            return result;
        }

        [HttpGet, Route("DownloadReportFile"), MapToApiVersion("1.0")]
        public async Task<IActionResult> DownloadReportFile([FromQuery] string fileName)
        {
            var webRootInfo = _hostingEnv.ContentRootPath;
            var dirPath = webRootInfo + @"/wwwroot/Reports";
            string filePath = $"{dirPath}/{fileName}";
            var memoryStream = new MemoryStream();
            using (var stream = new FileStream(filePath, FileMode.Open))
            {
                await stream.CopyToAsync(memoryStream);
            }
            memoryStream.Position = 0;
            return File(memoryStream, FileUtilities.GetContentType(filePath), Path.GetFileName(filePath));
        }

        [HttpGet, Route("UpdateUserCountry"), MapToApiVersion("1.0")]
        public async Task<ProcessResultViewModel<ApplicationUserViewModel>> UpdateUserCountry([FromQuery]Country countryId)
        {
            string authorizationValue = HttpContext.Request.Headers[HeaderNames.Authorization];
            string token = authorizationValue.Substring("Bearer ".Length).Trim();
            string userId = TokenManager.GetUserIdFromToken(token);
            var user = await _applicationUserManager.UpdateUserCountry(userId, countryId);
            if (user != null)
            {
                var userViewModel = Mapper.Map<ApplicationUser, ApplicationUserViewModel>(user);
                return ProcessResultViewModelHelper.Succedded(userViewModel);
            }
            else
            {
                return ProcessResultViewModelHelper.Failed<ApplicationUserViewModel>(null, "An error occured while updating user");
            }
        }

        [HttpGet, Route("UpdateUserLanguage"), MapToApiVersion("1.0")]
        public async Task<ProcessResultViewModel<ApplicationUserViewModel>> UpdateUserLanguage([FromQuery]SupportedLanguage languageId)
        {
            string authorizationValue = HttpContext.Request.Headers[HeaderNames.Authorization];
            string token = authorizationValue.Substring("Bearer ".Length).Trim();
            string userId = TokenManager.GetUserIdFromToken(token);
            var user = await _applicationUserManager.UpdateUserLanguage(userId, languageId);
            if (user != null)
            {
                var userViewModel = Mapper.Map<ApplicationUser, ApplicationUserViewModel>(user);
                return ProcessResultViewModelHelper.Succedded(userViewModel);
            }
            else
            {
                return ProcessResultViewModelHelper.Failed<ApplicationUserViewModel>(null, "An error occured while updating user");
            }
        }

        [HttpGet, Route("GetEmployees/{country}"), MapToApiVersion("1.0")]
        public async Task<ProcessResultViewModel<List<ApplicationUserViewModel>>> GetEmployees([FromRoute]Country country)
        {
            var employees = await _applicationUserManager.GetEmployees(country);
            var userModels = Mapper.Map<List<ApplicationUserViewModel>>(employees);
            ProcessResultViewModel<List<ApplicationUserViewModel>> result = ProcessResultViewModelHelper.Succedded(userModels);
            return result;
        }

        [HttpPost, Route("GetEmployeesPaginated/{country}"), MapToApiVersion("1.0")]
        public async Task<ProcessResultViewModel<IdentityPaginatedItems<ApplicationUser>>> GetEmployeesPaginated([FromBody]IdentityPaginatedItems<ApplicationUser> paginatedItems, [FromRoute]Country country)
        {
            var employees = await _applicationUserManager.GetEmployeesPaginated(paginatedItems, country);
            ProcessResultViewModel<IdentityPaginatedItems<ApplicationUser>> result = ProcessResultViewModelHelper.Succedded(employees);
            return result;
        }

        [HttpGet, Route("GetAllDrivers/{country?}"), MapToApiVersion("1.0")]
        public async Task<ProcessResultViewModel<List<ApplicationUserViewModel>>> GetAllDrivers([FromRoute]Country country = 0)
        {
            ProcessResultViewModel<List<ApplicationUserViewModel>> result = null;
            try
            {
                result = await GetByRole(_configuration["DriverRoleName"], country);
            }
            catch (Exception ex)
            {
                result = ProcessResultViewModelHelper.Failed<List<ApplicationUserViewModel>>(null, ex.Message);
            }
            return result;
        }

        [HttpGet, Route("GetAllProducers/{country?}"), MapToApiVersion("1.0")]
        public async Task<ProcessResultViewModel<List<ApplicationUserViewModel>>> GetAllProducers([FromRoute]Country country = 0)
        {
            ProcessResultViewModel<List<ApplicationUserViewModel>> result = null;
            try
            {
                result = await GetByRole(_configuration["ProducerRoleName"], country);
            }
            catch (Exception ex)
            {
                result = ProcessResultViewModelHelper.Failed<List<ApplicationUserViewModel>>(null, ex.Message);
            }
            return result;
        }

        [HttpGet, Route("GetDevices/{userId}"), MapToApiVersion("1.0")]
        public ProcessResultViewModel<List<UserDevice>> GetDevices([FromRoute] string userId)
        {
            var result = new ProcessResultViewModel<List<UserDevice>>();
            try
            {
                var userDevices = _userDeviceManager.GetByUserId(userId);
                result = ProcessResultViewModelHelper.Succedded(userDevices);
            }
            catch (Exception e)
            {
                result = ProcessResultViewModelHelper.Failed<List<UserDevice>>(null, e.Message);
            }
            return result;
        }
    }
}
