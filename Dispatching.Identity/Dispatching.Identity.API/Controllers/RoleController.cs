﻿using AutoMapper;
using Dispatching.Identity.API.ViewModels;
using Dispatching.Identity.BLL.IManagers;
using Dispatching.Identity.Models.Entities;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Utilites.ProcessingResult;

namespace Dispatching.Identity.API.Controllers
{
    [ApiVersion("1.0")]
    [Route("v{version:apiVersion}/[controller]")]
    public class RoleController : Controller
    {
        private IApplicationRoleManager _applicationRoleManager;

        public RoleController(IApplicationRoleManager applicationRoleManager)
        {
            _applicationRoleManager = applicationRoleManager;
        }

        [HttpPost, Route("Add"), MapToApiVersion("1.0")]
        public async Task<ProcessResultViewModel<ApplicationRoleViewModel>> Add([FromBody] ApplicationRoleViewModel roleViewModel)
        {
            ProcessResultViewModel<ApplicationRoleViewModel> result = null;
            try
            {
                var role = Mapper.Map<ApplicationRoleViewModel, ApplicationRole>(roleViewModel);
                role = await _applicationRoleManager.AddRoleAsync(role);
                if (role != null)
                {
                    roleViewModel = Mapper.Map<ApplicationRole, ApplicationRoleViewModel>(role);
                    result = ProcessResultViewModelHelper.Succedded(roleViewModel);
                }
                else
                {
                    result = ProcessResultViewModelHelper.Failed<ApplicationRoleViewModel>(null, "Couldn't create role");
                }
            }
            catch (Exception ex)
            {
                result = ProcessResultViewModelHelper.Failed<ApplicationRoleViewModel>(null, ex.Message);
            }
            return result;
        }

        [HttpDelete, Route("Delete"), MapToApiVersion("1.0")]
        public async Task<ProcessResultViewModel<bool>> Delete([FromQuery]string roleName)
        {
            ProcessResultViewModel<bool> result = null;
            try
            {
                var isDeleted = await _applicationRoleManager.Delete(roleName);
                result = ProcessResultViewModelHelper.Succedded(isDeleted);
            }
            catch (Exception ex)
            {
                result = ProcessResultViewModelHelper.Failed(false, ex.Message);
            }
            return result;
        }

        [HttpPut, Route("Update"), MapToApiVersion("1.0")]
        public async Task<ProcessResultViewModel<ApplicationRoleViewModel>> Update([FromBody] ApplicationRoleViewModel roleViewModel)
        {
            ProcessResultViewModel<ApplicationRoleViewModel> result = null;
            try
            {
                var role = Mapper.Map<ApplicationRoleViewModel, ApplicationRole>(roleViewModel);
                role = await _applicationRoleManager.Update(role);
                if (role != null)
                {
                    roleViewModel = Mapper.Map<ApplicationRole, ApplicationRoleViewModel>(role);
                    result = ProcessResultViewModelHelper.Succedded(roleViewModel);
                }
                else
                {
                    result = ProcessResultViewModelHelper.Failed<ApplicationRoleViewModel>(null, "Role can not be updated");
                }
            }
            catch (Exception ex)
            {
                result = ProcessResultViewModelHelper.Failed<ApplicationRoleViewModel>(null, ex.Message);
            }
            return result;
        }

        [HttpGet, Route("GetAll"), MapToApiVersion("1.0")]
        public ProcessResultViewModel<List<ApplicationRoleViewModel>> GetAll()
        {
            ProcessResultViewModel<List<ApplicationRoleViewModel>> result = null;
            try
            {
                var roles = _applicationRoleManager.GetAll();
                var roleViewModels = Mapper.Map<List<ApplicationRole>, List<ApplicationRoleViewModel>>(roles);
                result = ProcessResultViewModelHelper.Succedded(roleViewModels);
            }
            catch (Exception ex)
            {
                result = ProcessResultViewModelHelper.Failed<List<ApplicationRoleViewModel>>(null, ex.Message);
            }
            return result;
        }

    }
}
