﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;

namespace Dispatching.Identity.API.Services
{
    public static class SmsService
    {
        public static void SendKW(string token, string CustomerNo)
        {
            string message = $"Al Mawashi pin code: {token}";
            var smsUrl = string.Format("http://smsbox.com/smsgateway/services/messaging.asmx/Http_SendSMS?username=alghanim&password=alghanim@321&customerid=994&sendertext=A ENG&messagebody={0}&recipientnumbers=965{1}&defdate=&isblink=false&isflash=false", message, CustomerNo);

            WebRequest request = WebRequest.Create(smsUrl);
            request.Method = "POST";
            request.ContentLength = 0;
            WebResponse response = request.GetResponse();
        }

        public static void SendUAE(string token, string customerNo)
        {
            string message = $"Al Mawashi pin code: {token}";
            string dateTimeNow = DateTime.UtcNow.ToString("yyyy-MM-dd HH:mm:ss");
            string smsUrl = $"https://smartsmsgateway.com/api/api_http.php?username=mashawilivestock&password=mls01&senderid=Al%20Mawashi&to={customerNo}&text={message}&type=text&datetime={dateTimeNow}";

            WebRequest request = WebRequest.Create(smsUrl);
            request.Method = "GET";
            request.ContentLength = 0;
            WebResponse response = request.GetResponse();
        }
    }
}
