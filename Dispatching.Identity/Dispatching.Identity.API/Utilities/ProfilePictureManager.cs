﻿using BuildingBlocks.ServiceDiscovery;
using Dispatching.Identity.API.ViewModels;
using DnsClient;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Utilites;
using Utilites.UploadFile;
using Utilities;

namespace Dispatching.Identity.API.Utilities
{
    public static class ProfilePictureManager
    {
        public static ApplicationUserViewModel BindFileURL(ApplicationUserViewModel model,
            IConfigurationRoot configuration)
        {
            var baseUrl = configuration["ApiGatewayBaseUrl"];

            var identityServiceName = configuration["ServiceDiscovery:ServiceName"];

            var basePath = $"{baseUrl}/{identityServiceName}";

            if (model.PicturePath != null)
            {
                model.PicturePath = model.PicturePath.Replace('\\', '/');
                var segments = model.PicturePath.Split('/');
                model.PicturePath = model.PicturePath.Remove(0, segments[1].Length + 1).Remove(0, segments[2].Length + 1);
                model.PictureUrl = $"{basePath}/{model.PicturePath}";
            }
            return model;
        }

        public static string SaveProfilePicture(string base64File, string username, IUploadImageFileManager imageManager
            , IHostingEnvironment hostingEnv)
        {
            var fileExt = StringUtilities.GetFileExtension(base64File);
            string fileName = $"{username}.{fileExt}";
            string path = $"{hostingEnv.WebRootPath}/ProfilePictures";
            var imageResult = imageManager.AddFile(new UploadFile
            {
                FileName = fileName,
                FileContent = base64File
            }, path);

            if (imageResult.IsSucceeded)
            {
                return $"{imageResult.Data}/{fileName}";
            }
            return String.Empty;
        }

        public static void DeleteProfilePicture(string username, IHostingEnvironment hostingEnv)
        {
            string path = $"{hostingEnv.WebRootPath}/ProfilePictures";
            var pictures = Directory.EnumerateFiles(path);
            var fileName = pictures.FirstOrDefault(p => p.ToLower().Contains(username.ToLower()));
            if (!String.IsNullOrEmpty(fileName))
            {
                path = $"{path}/{fileName}";
                File.Delete(path);
            }
        }
    }
}
