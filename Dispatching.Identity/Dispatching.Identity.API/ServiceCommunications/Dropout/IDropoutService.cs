﻿using Dispatching.Identity.API.ServicesSettings;
using Dispatching.Identity.API.ServicesViewModels.Dropout;
using DispatchProduct.HttpClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Dispatching.Identity.API.ServiceCommunications.Dropout
{
    public interface IDropoutService :
        IDefaultHttpClientCrud<DropoutServiceSettings, SendSmsViewModel, bool>
    {
        Task SendKw(string version, SendSmsViewModel model);
        Task SendUae(string version, SendSmsViewModel model);
        Task SendMail(string version, SendMailViewModel model);
    }
}
