﻿using AutoMapper;
using Dispatching.Identity.API.ViewModels;
using Dispatching.Identity.Models.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Utilities.Utilites;

namespace Dispatching.Identity.API.AutoMapperConfig
{
    public class MappingProfile : Profile
    {
        public MappingProfile()
        {
            CreateMap<ApplicationRoleViewModel, ApplicationRole>()
                .ForMember(dest => dest.ConcurrencyStamp, opt => opt.Ignore())
                .ForMember(dest => dest.NormalizedName, opt => opt.Ignore());
            CreateMap<ApplicationRole, ApplicationRoleViewModel>();

            CreateMap<ApplicationUserViewModel, ApplicationUser>()
                .ForMember(dest => dest.ConcurrencyStamp, opt => opt.Ignore())
                .ForMember(dest => dest.AccessFailedCount, opt => opt.Ignore())
                .ForMember(dest => dest.EmailConfirmed, opt => opt.Ignore())
                .ForMember(dest => dest.LockoutEnabled, opt => opt.Ignore())
                .ForMember(dest => dest.LockoutEnd, opt => opt.Ignore())
                .ForMember(dest => dest.NormalizedEmail, opt => opt.Ignore())
                .ForMember(dest => dest.NormalizedUserName, opt => opt.Ignore())
                .ForMember(dest => dest.PasswordHash, opt => opt.Ignore())
                .ForMember(dest => dest.PhoneNumberConfirmed, opt => opt.Ignore())
                .ForMember(dest => dest.PhoneNumber, opt => opt.Ignore())
                .ForMember(dest => dest.SecurityStamp, opt => opt.Ignore())
                .ForMember(dest => dest.TwoFactorEnabled, opt => opt.Ignore())
                .ForMember(dest => dest.Deactivated, opt => opt.Ignore());

            CreateMap<ApplicationUser, ApplicationUserViewModel>()
                .ForMember(dest => dest.Password, opt => opt.Ignore())
                .ForMember(dest => dest.PictureUrl, opt => opt.Ignore());


            CreateMap<JunkUserViewModel, JunkUser>()
                .ForMember(dest => dest.ConcurrencyStamp, opt => opt.Ignore())
                .ForMember(dest => dest.AccessFailedCount, opt => opt.Ignore())
                .ForMember(dest => dest.EmailConfirmed, opt => opt.Ignore())
                .ForMember(dest => dest.LockoutEnabled, opt => opt.Ignore())
                .ForMember(dest => dest.LockoutEnd, opt => opt.Ignore())
                .ForMember(dest => dest.NormalizedEmail, opt => opt.Ignore())
                .ForMember(dest => dest.NormalizedUserName, opt => opt.Ignore())
                .ForMember(dest => dest.PasswordHash, opt => opt.Ignore())
                .ForMember(dest => dest.PhoneNumberConfirmed, opt => opt.Ignore())
                .ForMember(dest => dest.PhoneNumber, opt => opt.Ignore())
                .ForMember(dest => dest.SecurityStamp, opt => opt.Ignore())
                .ForMember(dest => dest.TwoFactorEnabled, opt => opt.Ignore())
                .ForMember(dest => dest.Deactivated, opt => opt.Ignore());
            CreateMap<JunkUser, JunkUserViewModel>();

            CreateMap<Privilege, PrivilegeViewModel>();
            CreateMap<PrivilegeViewModel, Privilege>();

            //CreateMap<Lkp_CountryViewModel, Lkp_Country>()
            //    .IgnoreBaseEntityProperties();
            //CreateMap<Lkp_Country, Lkp_CountryViewModel>();

            CreateMap<ApplicationUser, UserSearchResultViewModel>()
                .ForMember(dest => dest.RoleNames, opt => opt.MapFrom(u => string.Join(",", u.RoleNames.ToArray())))
                .ForMember(dest => dest.CountryCode, opt => opt.Ignore());


        }
    }
}
