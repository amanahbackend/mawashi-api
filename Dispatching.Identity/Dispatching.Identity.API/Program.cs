﻿using System.IO;
using Microsoft.AspNetCore;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Logging;
using Dispatching.Identity.Models.Context;
using IdentityServer4.EntityFramework.DbContexts;
using Dispatching.Identity.API.Seed;
using Dispatching.Identity.BLL.IManagers;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Configuration;
using Serilog;
using Utilities.Utilites.SerilogExtensions;

namespace Dispatching.Identity.API
{
    public class Program
    {
        public static void Main(string[] args)
        {
            #region Default
            //BuildWebHost(args).Run();
            #endregion

            Log.Logger = new LoggerConfiguration()
                .Enrich.With<CustomExceptionEnricher>()
                .MinimumLevel.Error()
                .Enrich.FromLogContext()
                .WriteTo.File("logs/log_.csv", rollingInterval: RollingInterval.Day,
                outputTemplate: "{Timestamp:yyyy-MM-dd HH:mm:ss.fff zzz},[{Level}],{Message},{ExceptionMessage},{ExceptionSource},{ExceptionType},{ExceptionStackTrace},{NewLine}")
                .CreateLogger();

            BuildWebHost(args)
                .MigrateDbContext<PersistedGrantDbContext>((_, __) => { })
                .MigrateDbContext<ApplicationDbContext>((context, services) =>
                {
                    var env = services.GetService<IHostingEnvironment>();
                    var logger = services.GetService<ILogger<ApplicationDbContextSeed>>();
                    //var settings = services.GetService<IOptions<AppSettings>>();
                    var appUserManager = services.GetService<IApplicationUserManager>();
                    var appRoleManager = services.GetService<IApplicationRoleManager>();
                    //var countryManager = services.GetService<ILkp_CountryManager>();


                    new ApplicationDbContextSeed(appUserManager, appRoleManager)
                        .SeedAsync(context, env, logger)
                        .Wait();
                })
                .MigrateDbContext<ConfigurationDbContext>((context, services) =>
                {
                    var configuration = services.GetService<IConfiguration>();

                    new ConfigurationDbContextSeed()
                        .SeedAsync(context, configuration)
                        .Wait();
                }).Run();
        }
        #region Default

        //public static IWebHost BuildWebHost(string[] args) =>
        //    WebHost.CreateDefaultBuilder(args)
        //        .UseStartup<Startup>()
        //        .Build();
        #endregion

        public static IWebHost BuildWebHost(string[] args) =>
           WebHost.CreateDefaultBuilder(args)
               .UseKestrel()
               .UseContentRoot(Directory.GetCurrentDirectory())
               .UseIISIntegration()
               .UseStartup<Startup>()
               .UseSetting("detailedErrors", "true")
               .CaptureStartupErrors(true)
               .UseSerilog()
               .Build();
    }
}
