﻿using Dispatching.Identity.Models.Entities;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using DispatchProduct.BuildingBlocks.BaseEntities.EntityConfigurations;
using CommonEnums;

namespace Dispatching.Identity.Models.EntitiesConfiguration
{
    public class ApplicationUserTypeConfiguration
        : IdentityBaseEntityTypeConfiguration<ApplicationUser>, IEntityTypeConfiguration<ApplicationUser>
    {
        public new void Configure(EntityTypeBuilder<ApplicationUser> builder)
        {
            base.Configure(builder);
            builder.Property(u => u.FirstName).IsRequired(true);
            builder.Property(u => u.LastName).IsRequired(true);
            builder.Property(u => u.Phone1).IsRequired(true);
            builder.Property(u => u.Phone2).IsRequired(false);
            builder.Property(u => u.PicturePath).IsRequired(false);
            builder.Property(u => u.Language).HasDefaultValue(SupportedLanguage.NotSet);

            // builder.Property(u => u.Fk_Country_Id).IsRequired(false);
        }
    }
}
