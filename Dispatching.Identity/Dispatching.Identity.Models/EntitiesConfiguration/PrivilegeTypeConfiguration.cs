﻿using Dispatching.Identity.Models.Entities;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Dispatching.Identity.Models.EntitiesConfiguration
{
    public class PrivilegeTypeConfiguration
        : IEntityTypeConfiguration<Privilege>
    {
        public void Configure(EntityTypeBuilder<Privilege> builder)
        {
            builder.ToTable("Privilege");
            builder.Property(p => p.Id).UseSqlServerIdentityColumn();
            builder.Property(p => p.Name).IsRequired(true);
            builder.Ignore(p => p.Roles);
        }
    }
}
