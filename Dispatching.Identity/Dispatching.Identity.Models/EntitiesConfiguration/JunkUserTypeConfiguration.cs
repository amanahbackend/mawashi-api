﻿using Dispatching.Identity.Models.Entities;
using DispatchProduct.BuildingBlocks.BaseEntities.EntityConfigurations;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;

namespace Dispatching.Identity.Models.EntitiesConfiguration
{
    public class JunkUserTypeConfiguration
        : IdentityBaseEntityTypeConfiguration<JunkUser>, IEntityTypeConfiguration<JunkUser>
    {
        public new void Configure(EntityTypeBuilder<JunkUser> builder)
        {
            base.Configure(builder);
            builder.ToTable("JunkUser");
            //builder.Property(u => u.CreatedDate).IsRequired();
            //builder.Property(u => u.DeletedDate).IsRequired();
            //builder.Property(u => u.UpdatedDate).IsRequired();
            //builder.Property(u => u.FK_UpdatedBy_Id).IsRequired(false);
            //builder.Property(u => u.FK_DeletedBy_Id).IsRequired(false);
            //builder.Property(u => u.FK_CreatedBy_Id).IsRequired(false);
            //builder.Property(u => u.IsDeleted).IsRequired(true);
            builder.Property(u => u.FirstName).IsRequired(true);
            builder.Property(u => u.LastName).IsRequired(true);
            builder.Property(u => u.Phone1).IsRequired(true);
            builder.Property(u => u.Phone2).IsRequired(false);
            builder.Property(u => u.PicturePath).IsRequired(false);
        }
    }
}
