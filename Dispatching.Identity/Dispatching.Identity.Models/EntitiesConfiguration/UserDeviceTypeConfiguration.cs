﻿using Dispatching.Identity.Models.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Dispatching.Identity.Models.EntitiesConfiguration
{
    public class UserDeviceTypeConfiguration
    : IEntityTypeConfiguration<UserDevice>
    {
        public void Configure(EntityTypeBuilder<UserDevice> builder)
        {
            builder.ToTable("UserDevice");

            builder.Property(u => u.Id).UseSqlServerIdentityColumn();
            builder.Property(u => u.Fk_AppUser_Id).IsRequired();
            builder.Property(u => u.DeveiceId).IsRequired();
        }
    }
}
