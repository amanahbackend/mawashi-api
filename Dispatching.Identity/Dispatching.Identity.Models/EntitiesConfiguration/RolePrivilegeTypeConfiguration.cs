﻿using Dispatching.Identity.Models.Entities;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Dispatching.Identity.Models.EntitiesConfiguration
{
    public class RolePrivilegeTypeConfiguration
        : IEntityTypeConfiguration<RolePrivilege>
    {
        public void Configure(EntityTypeBuilder<RolePrivilege> builder)
        {
            builder.ToTable("RolePrivilege");
            builder.Property(p => p.Id).UseSqlServerIdentityColumn();
            builder.Property(p => p.Fk_Privilege_Id).IsRequired(true);
            builder.Property(p => p.Fk_ApplicationRole_Id).IsRequired(true);
        }
    }
}
