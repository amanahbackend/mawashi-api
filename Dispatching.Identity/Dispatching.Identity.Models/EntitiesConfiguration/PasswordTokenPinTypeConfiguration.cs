﻿using Dispatching.Identity.Models.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;

namespace Dispatching.Identity.Models.EntitiesConfiguration
{
    public class PasswordTokenPinTypeConfiguration
         : IEntityTypeConfiguration<PasswordTokenPin>
    {
        public void Configure(EntityTypeBuilder<PasswordTokenPin> builder)
        {
            builder.ToTable("PasswordTokenPin");

            builder.Property(u => u.Id).UseSqlServerIdentityColumn();
            builder.Property(u => u.Pin).IsRequired(true);
            builder.Property(u => u.Token).IsRequired(true);           
        }
    }
}
