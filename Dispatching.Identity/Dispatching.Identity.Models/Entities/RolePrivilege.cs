﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Dispatching.Identity.Models.Entities
{
    public class RolePrivilege
    {
        public int Id { get; set; }
        public string Fk_ApplicationRole_Id { get; set; }
        public int Fk_Privilege_Id { get; set; }
    }
}
