﻿using CommonEnums;
using DispatchProduct.BuildingBlocks.BaseEntities.IEntities;
using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Dispatching.Identity.Models.Entities
{
    public class ApplicationUser : IdentityUser, IIdentityBaseEntity
    {
        public ApplicationUser() : base()
        {

        }
        public ApplicationUser(string username) : base(username)
        {

        }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Phone1 { get; set; }
        public string Phone2 { get; set; }
        public string PicturePath { get; set; }
        public string FK_CreatedBy_Id { get; set; }
        public string FK_UpdatedBy_Id { get; set; }
        public string FK_DeletedBy_Id { get; set; }
        public bool IsDeleted { get; set; }
        public DateTime CreatedDate { get; set; }
        public DateTime UpdatedDate { get; set; }
        public DateTime DeletedDate { get; set; }
        public bool Deactivated { get; set; }
        public Country Fk_Country_Id { get; set; }
        public SupportedLanguage Language { get; set; }
        public bool IsBasicRegister { get; set; }
        [NotMapped]
        public List<string> RoleNames { get; set; }
        [NotMapped]
        public string Picture { get; set; }
    }
}
