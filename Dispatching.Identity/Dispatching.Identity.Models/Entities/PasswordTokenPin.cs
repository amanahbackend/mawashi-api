﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Dispatching.Identity.Models.Entities
{
    public class PasswordTokenPin
    {
        public int Id { get; set; }
        public string Token { get; set; }
        public string Pin { get; set; }
    }
}
