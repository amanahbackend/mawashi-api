﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Dispatching.Identity.Models.Entities
{
    public class Privilege
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public List<string> Roles { get; set; }
    }
}
