﻿using Dispatching.Identity.Models.Entities;
using Dispatching.Identity.Models.EntitiesConfiguration;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;

namespace Dispatching.Identity.Models.Context
{
    public class ApplicationDbContext : IdentityDbContext<ApplicationUser, ApplicationRole, string>
    {
        public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options) : base(options)
        {
        }

        public DbSet<PasswordTokenPin> PasswordTokenPins { get; set; }
        public DbSet<JunkUser> JunkUsers { get; set; }
        public DbSet<Privilege> Privileges { get; set; }
        public DbSet<RolePrivilege> RolePrivileges { get; set; }
        public DbSet<UserDevice> UserDevices { get; set; }
        //public DbSet<Lkp_Country> Countries { get; set; }


        protected override void OnModelCreating(ModelBuilder builder)
        {
            base.OnModelCreating(builder);

            builder.ApplyConfiguration(new JunkUserTypeConfiguration());
            builder.ApplyConfiguration(new PasswordTokenPinTypeConfiguration());
            builder.ApplyConfiguration(new ApplicationUserTypeConfiguration());
            builder.ApplyConfiguration(new ApplicationRoleTypeConfiguration());
            builder.ApplyConfiguration(new PrivilegeTypeConfiguration());
            builder.ApplyConfiguration(new RolePrivilegeTypeConfiguration());
            builder.ApplyConfiguration(new UserDeviceTypeConfiguration());

            // builder.ApplyConfiguration(new Lkp_CountryTypeConfiguration());


            // Customize the ASP.NET Identity model and override the defaults if needed.
            // For example, you can rename the ASP.NET Identity table names and more.
            // Add your customizations after calling base.OnModelCreating(builder);
        }
    }
}
