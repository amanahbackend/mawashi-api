﻿using Dispatching.Identity.Models.Entities;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Dispatching.Identity.BLL.IManagers
{
    public interface IApplicationRoleManager
    {
        Task<ApplicationRole> AddRoleAsync(ApplicationRole applicationRole);
        Task AddRolesAsync(List<ApplicationRole> applicationRoles);
        Task<bool> Delete(string roleName);
        Task<ApplicationRole> Update(ApplicationRole role);
        List<ApplicationRole> GetAll();
        Task<ApplicationRole> GetByName(string roleName);
    }
}
