﻿using Dispatching.Identity.Models.Entities;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Dispatching.Identity.BLL.IManagers
{
    public interface IRolePrivilegeManager
    {
        RolePrivilege Add(RolePrivilege entity);
        List<RolePrivilege> GetByRoleId(string roleId);
    }
}
