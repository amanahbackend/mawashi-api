﻿using System.Collections.Generic;
using Dispatching.Identity.Models.Entities;

namespace Dispatching.Identity.BLL.IManagers
{
    public interface IUserDeviceManager
    {
        void AddIfNotExist(UserDevice userDevice);
        List<UserDevice> GetByUserId(string userId);
        bool DeleteDevice(string deviceId);
    }
}
