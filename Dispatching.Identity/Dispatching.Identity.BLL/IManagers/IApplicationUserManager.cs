﻿using CommonEnums;
using Dispatching.Identity.Models.Entities;
using System;
using System.Collections.Generic;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using Utilites.PaginatedItems;
using Utilites.ProcessingResult;

namespace Dispatching.Identity.BLL.IManagers
{
    public interface IApplicationUserManager
    {
        Task<ProcessResult<ApplicationUser>> AddUserAsync(ApplicationUser user, string password);
        Task<bool> AddUserToRolesAsync(ApplicationUser user);
        Task AddUsersAsync(List<Tuple<ApplicationUser, string>> users);
        Task<ApplicationUser> GetBy(string username);
        Task<IList<string>> GetRolesAsync(ApplicationUser user);
        Task<bool> IsUserNameExistAsync(string userName);
        Task<bool> IsEmailExistAsync(string email);
        Task<IList<ApplicationUser>> GetUsersInRole(string roleName, Country country = 0);
        Task<bool> IsUserInRole(string userName, string roleName);
        Task<List<ApplicationUser>> GetAll();
        Task<bool> DeleteAsync(ApplicationUser user);
        Task<bool> DeleteByIdAsunc(string id);
        Task<ProcessResult<ApplicationUser>> UpdateUserAsync(ApplicationUser user);
        Task<ApplicationUser> UpdateUserCountry(string userId, Country countryId);
        Task<ApplicationUser> UpdateUserLanguage(string userId, SupportedLanguage languageId);
        Task<bool> DeleteAsync(string username);
        bool IsPhoneExist(string phone);
        Task<List<ApplicationUser>> Search(string searchToken, string[] searchFields);
        Task<ApplicationUser> Get(string id);
        Task<IList<Claim>> GetClaimsAsync(ApplicationUser user);
        Task<List<ApplicationUser>> GetByIds(List<string> ids);
        Task<List<ApplicationUser>> GetEmployees(Country country);
        Task<ProcessResult<ApplicationUser>> UpdateUserEmail(string userId, string email);
        Task<ProcessResult<ApplicationUser>> UpdateUserPhone(string userId, string phone);
        Task<ProcessResult<ApplicationUser>> UpdateUserPicture(string userId, string picturePath);

        Task<IdentityPaginatedItems<ApplicationUser>> GetAllCustomersPaginated(
            IdentityPaginatedItems<ApplicationUser> paginatedItems, Country country);

        Task<IdentityPaginatedItems<ApplicationUser>> GetAllCompaniesPaginated(
            IdentityPaginatedItems<ApplicationUser> paginatedItems, Country country);

        Task<bool> Deactivate(string username);
        Task<bool> IsUserDeactivated(string username);
        Task<bool> Activate(string username);

        Task<string> GenerateForgetPasswordToken(string username);
        Task<bool> ForgetPassword(string username, string newPassword, string changePasswordToken);
        Task<bool> ResetPassword(string username, string newPassword);
        Task<bool> ChangePassword(string username, string oldPassword, string newPassword);

        Task<string> GeneratePhoneNumberToken(string username, string phone);
        Task<bool> CheckPhoneValidationToken(string username, string phone, string token);
        Task<bool> IsPhoneConfirmed(string username);
        Task<bool> ConfirmPhone(string username, string phone, string token);

        Task<bool> SetAuthentiacationToken(ApplicationUser user, string token);
        Task<bool> RemoveAuthenticationToken(ApplicationUser user);
        Task<string> GetAuthenticationToken(ApplicationUser user);
        Task<IdentityPaginatedItems<ApplicationUser>> GetEmployeesPaginated(IdentityPaginatedItems<ApplicationUser> paginatedItems, Country country);
    }
}
