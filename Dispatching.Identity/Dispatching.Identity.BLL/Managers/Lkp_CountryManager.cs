﻿//using Dispatching.Identity.BLL.IManagers;
//using Dispatching.Identity.Models.Context;
//using Dispatching.Identity.Models.Entities;
//using DispatchProduct.RepositoryModule;
//using System;
//using System.Collections.Generic;
//using System.Linq;
//using System.Text;

//namespace Dispatching.Identity.BLL.Managers
//{
//    public class Lkp_CountryManager : Repository<Lkp_Country>, ILkp_CountryManager
//    {
//        public Lkp_CountryManager(ApplicationDbContext context) : base(context)
//        {
//        }

//        public void Seed()
//        {
//            Add(new Lkp_Country
//            {
//                NameAR = "الكويت",
//                NameEN = "Kuwait",
//                Code = "KW"
//            });
//            Add(new Lkp_Country
//            {
//                NameAR = "الامارات",
//                NameEN = "UAE",
//                Code = "UAE"
//            });
//        }

//        public int GetIdByName(string name)
//        {
//            var entity = GetAllQuerable().Data.FirstOrDefault(x => x.NameEN.ToLower().Equals(name.ToLower()));
//            return entity.Id;
//        }

//        public int Kuwait
//        {
//            get
//            {
//                return GetIdByName("Kuwait");
//            }
//        }

//        public int UAE
//        {
//            get
//            {
//                return GetIdByName("UAE");
//            }
//        }

//        public int GetIdByCode(string code)
//        {
//            var entity = GetAllQuerable().Data.FirstOrDefault(x => x.Code.ToLower().Equals(code.ToLower()));
//            return entity.Id;
//        }
//    }
//}
