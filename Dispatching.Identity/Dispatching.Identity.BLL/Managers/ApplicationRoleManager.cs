﻿using Dispatching.Identity.BLL.IManagers;
using Dispatching.Identity.Models.Entities;
using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dispatching.Identity.BLL.Managers
{
    public class ApplicationRoleManager : IApplicationRoleManager
    {
        RoleManager<ApplicationRole> _identityRoleManager;

        public ApplicationRoleManager(RoleManager<ApplicationRole> identityRoleManager)
        {
            _identityRoleManager = identityRoleManager;
        }

        public async Task<ApplicationRole> GetByName(string roleName)
        {
            var role = await _identityRoleManager.FindByNameAsync(roleName);
            return role;
        }

        public async Task<ApplicationRole> AddRoleAsync(ApplicationRole applicationRole)
        {
            IdentityResult result = await _identityRoleManager.CreateAsync(applicationRole);
            if (result.Succeeded)
            {
                return applicationRole;
            }
            else
            {
                return null;
            }
        }

        public async Task AddRolesAsync(List<ApplicationRole> applicationRoles)
        {
            foreach (var role in applicationRoles)
            {
                await AddRoleAsync(role);
            }
        }

        public async Task<bool> Delete(string roleName)
        {
            var role = await _identityRoleManager.FindByNameAsync(roleName);
            IdentityResult identityResult = await _identityRoleManager.DeleteAsync(role);
            return identityResult.Succeeded;
        }

        public async Task<ApplicationRole> Update(ApplicationRole role)
        {
            var dbRole = await _identityRoleManager.FindByIdAsync(role.Id);
            dbRole.Name = role.Name;
            IdentityResult identityResult = await _identityRoleManager.UpdateAsync(dbRole);
            return identityResult.Succeeded ? dbRole : null;
        }

        public List<ApplicationRole> GetAll()
        {
            var roles = _identityRoleManager.Roles.ToList();
            return roles;
        }
    }
}
