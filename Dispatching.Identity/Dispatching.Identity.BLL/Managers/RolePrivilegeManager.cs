﻿using Dispatching.Identity.BLL.IManagers;
using Dispatching.Identity.Models.Context;
using Dispatching.Identity.Models.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dispatching.Identity.BLL.Managers
{
    public class RolePrivilegeManager : IRolePrivilegeManager
    {
        private ApplicationDbContext _context;
        private IApplicationRoleManager _applicationRoleManager;


        public RolePrivilegeManager(ApplicationDbContext context,
            IApplicationRoleManager applicationRoleManager)
        {
            _applicationRoleManager = applicationRoleManager;
            _context = context;
        }

        public RolePrivilege Add(RolePrivilege entity)
        {
            entity = _context.Add(entity).Entity;
            return _context.SaveChanges() > 0 ? entity : null;
        }

        public List<RolePrivilege> GetByRoleId(string roleId)
        {
            return _context.RolePrivileges.Where(rp => rp.Fk_ApplicationRole_Id.Equals(roleId)).ToList();
        }
    }
}
