﻿using CommonEnums;
using Dispatching.Identity.BLL.IManagers;
using Dispatching.Identity.Models.Context;
using Dispatching.Identity.Models.Entities;
using DispatchProduct.RepositoryModule;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using Utilites.PaginatedItems;
using Utilites.PaginatedItemsViewModel;
using Utilites.ProcessingResult;
using Utilities;

namespace Dispatching.Identity.BLL.Managers
{
    public class ApplicationUserManager : IApplicationUserManager
    {
        private UserManager<ApplicationUser> _identityUserManager;
        private RoleManager<ApplicationRole> _identityRoleManager;
        private IPasswordTokenPinManager _passwordTokenPinManager;
        private IJunkUserManager _junkUserManager;
        private string _loginProvider = "JWT";
        private string _tokenName = "JwtToken";
        private ApplicationDbContext _applicationDbContext;
        private Country countryId;

        public ApplicationUserManager(UserManager<ApplicationUser> identityUserManager,
            RoleManager<ApplicationRole> identityRoleManager, IPasswordTokenPinManager passwordTokenPinManager,
            IJunkUserManager junkUserManager, ApplicationDbContext applicationDbContext)
        {
            _junkUserManager = junkUserManager;
            _passwordTokenPinManager = passwordTokenPinManager;
            _identityUserManager = identityUserManager;
            _identityRoleManager = identityRoleManager;
            _applicationDbContext = applicationDbContext;
        }

        public async Task<IList<Claim>> GetClaimsAsync(ApplicationUser user)
        {
            return await _identityUserManager.GetClaimsAsync(user);
        }

        public async Task<bool> SetAuthentiacationToken(ApplicationUser user, string token)
        {
            var result = await _identityUserManager.SetAuthenticationTokenAsync(user, _loginProvider, _tokenName, token);
            return result.Succeeded;
        }

        public async Task<bool> RemoveAuthenticationToken(ApplicationUser user)
        {
            var result = await _identityUserManager.RemoveAuthenticationTokenAsync(user, _loginProvider, _tokenName);
            return result.Succeeded;
        }

        public async Task<string> GetAuthenticationToken(ApplicationUser user)
        {
            var result = await _identityUserManager.GetAuthenticationTokenAsync(user, _loginProvider, _tokenName);
            return result;
        }

        public async Task<ProcessResult<ApplicationUser>> AddUserAsync(ApplicationUser user, string password)
        {
            ProcessResult<ApplicationUser> result = null;
            user.SecurityStamp = Guid.NewGuid().ToString("D");
            user.Deactivated = false;
            user.PhoneNumberConfirmed = user.RoleNames.Contains("Customer") && !user.IsBasicRegister ? false : true;
            if (IsPhoneExist(user.Phone1))
            {
                var errorMsg = $"Phone '{user.Phone1}' is already taken.";
                result = ProcessResultHelper.Failed<ApplicationUser>(null, new Exception(errorMsg));
            }
            else
            {
                var identityResult = await _identityUserManager.CreateAsync(user, password);

                if (identityResult.Succeeded && user.RoleNames != null && user.RoleNames.Count > 0)
                {
                    await AddUserToRolesAsync(user);
                    result = ProcessResultHelper.Succedded(user);
                }
                else
                {
                    var errorMsg = String.Join(" ", identityResult.Errors.ToList().ConvertAll(x => x.Description));
                    result = ProcessResultHelper.Failed<ApplicationUser>(null, new Exception(errorMsg));
                }
            }
            return result;
        }

        public async Task<bool> AddUserToRolesAsync(ApplicationUser user)
        {
            if (user != null && user.RoleNames != null)
            {
                var roles = await _identityUserManager.GetRolesAsync(user);
                var result = await _identityUserManager.RemoveFromRolesAsync(user, roles.ToArray());

                result = await _identityUserManager.AddToRolesAsync(user, user.RoleNames);
                return result.Succeeded;
            }
            return false;
        }

        public async Task AddUsersAsync(List<Tuple<ApplicationUser, string>> users)
        {
            foreach (var usr in users)
            {
                await AddUserAsync(usr.Item1, usr.Item2);
            }
        }

        public async Task<IList<string>> GetRolesAsync(ApplicationUser user)
        {
            IList<string> rolesNames = await _identityUserManager.GetRolesAsync(user);
            return rolesNames;
        }

        public async Task<ApplicationUser> GetBy(string username)
        {
            ApplicationUser user = await _identityUserManager.FindByNameAsync(username);
            if (user != null)
            {
                user.RoleNames = (List<string>)await GetRolesAsync(user);
            }
            return user;
        }

        public async Task<ApplicationUser> Get(string id)
        {
            ApplicationUser user = await _identityUserManager.FindByIdAsync(id);
            user.RoleNames = (List<string>)await GetRolesAsync(user);
            return user;
        }

        public async Task<bool> IsUserNameExistAsync(string userName)
        {
            var user = await _identityUserManager.FindByNameAsync(userName);
            if (user != null)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public async Task<bool> IsEmailExistAsync(string email)
        {
            var user = await _identityUserManager.FindByEmailAsync(email);
            if (user != null)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public async Task<IList<ApplicationUser>> GetUsersInRole(string roleName, Country country = 0)
        {
            IList<ApplicationUser> result = null;
            if (country > 0)
            {
                result = (await _identityUserManager.GetUsersInRoleAsync(roleName)).Where(c => c.Fk_Country_Id == country).ToList();

            }
            else
            {
                result = await _identityUserManager.GetUsersInRoleAsync(roleName);
            }
            return result;
        }

        public async Task<bool> IsUserInRole(string userName, string roleName)
        {
            var user = await _identityUserManager.FindByNameAsync(userName);
            var result = await _identityUserManager.IsInRoleAsync(user, roleName);
            return result;
        }

        public async Task<List<ApplicationUser>> GetAll()
        {
            var users = _identityUserManager.Users.ToList();
            foreach (var user in users)
            {
                user.RoleNames = (await GetRolesAsync(user)).ToList();
            }
            return users;
        }

        public async Task<IdentityPaginatedItems<ApplicationUser>> GetAllCustomersPaginated(IdentityPaginatedItems<ApplicationUser> paginatedItems, Country country)
        {
            var customers = (await GetAll()).Where(x => x.RoleNames.Contains("Customer") && x.Fk_Country_Id == country).ToList();
            return GetPagesFromList(customers, paginatedItems);
        }

        public async Task<IdentityPaginatedItems<ApplicationUser>> GetAllCompaniesPaginated(IdentityPaginatedItems<ApplicationUser> paginatedItems, Country country)
        {
            var customers = (await GetAll()).Where(x => x.RoleNames.Contains("Company") && x.Fk_Country_Id == country).ToList();
            return GetPagesFromList(customers, paginatedItems);
        }

        public async Task<bool> DeleteAsync(string username)
        {
            var user = await GetBy(username);
            if (user != null)
            {
                return await DeleteAsync(user);
            }
            return false;
        }

        public async Task<bool> DeleteByIdAsunc(string id)
        {
            var user = await Get(id);
            if (user != null)
            {
                return await DeleteAsync(user);
            }
            return false;
        }

        public async Task<bool> DeleteAsync(ApplicationUser user)
        {
            bool result = false;
            var callBack = await _identityUserManager.DeleteAsync(user);
            if (callBack.Succeeded)
            {
                var junkUser = _junkUserManager.GetJunkUser(user);
                if (junkUser.IsSucceeded)
                {
                    _junkUserManager.Add(junkUser.Data);
                    result = true;
                }
            }
            return result;
        }

        public async Task<ProcessResult<ApplicationUser>> UpdateUserAsync(ApplicationUser user)
        {
            ProcessResult<ApplicationUser> result = null;
            var dbUser = await _identityUserManager.FindByIdAsync(user.Id);
            CopyEditingProperties(user, dbUser);
            var count = _identityUserManager.Users.Where(u => (u.Phone1.Equals(user.Phone1) ||
                                                              u.Phone2.Equals(user.Phone1)) &&
                                                              !u.Id.Equals(user.Id)).Count();
            bool isPhoneExistForAnotherUser = count > 0;
            if (isPhoneExistForAnotherUser)
            {
                var errorMsg = $"Phone '{user.Phone1}' is already taken.";
                result = ProcessResultHelper.Failed<ApplicationUser>(null, new Exception(errorMsg));
            }
            else
            {
                IdentityResult identityResult = await _identityUserManager.UpdateAsync(dbUser);
                if (identityResult.Succeeded)
                {
                    await AddUserToRolesAsync(dbUser);
                    result = ProcessResultHelper.Succedded(user);
                }
                else
                {
                    var errorMsg = String.Join(" ", identityResult.Errors.ToList().ConvertAll(x => x.Description));
                    result = ProcessResultHelper.Failed<ApplicationUser>(null, new Exception(errorMsg));
                }
            }
            return result;
        }

        private void CopyEditingProperties(ApplicationUser source, ApplicationUser destination)
        {
            destination.Email = source.Email;
            destination.FirstName = source.FirstName;
            destination.LastName = source.LastName;
            destination.Phone1 = source.Phone1;
            destination.Phone2 = source.Phone2;
            destination.PicturePath = source.PicturePath;
            destination.RoleNames = source.RoleNames;
            destination.UserName = source.UserName;
            destination.PhoneNumberConfirmed = source.PhoneNumberConfirmed;
        }

        public bool IsPhoneExist(string phone)
        {
            var count = _identityUserManager.Users.Where(u => u.Phone1.Equals(phone) ||
                                                              u.Phone2.Equals(phone)).Count();
            return count > 0;
        }

        public async Task<List<ApplicationUser>> Search(string searchToken, string[] searchFields)
        {
            var predicate = PredicateBuilder.False<ApplicationUser>();
            foreach (var searchField in searchFields)
            {
                predicate = predicate.Or(PredicateBuilder.CreateContainsExpression<ApplicationUser>(searchField, searchToken));
            }
            var users = _identityUserManager.Users.Where(predicate).ToList();
            foreach (var user in users)
            {
                user.RoleNames = (await GetRolesAsync(user)).ToList();
            }
            return users;
        }

        public async Task<bool> Deactivate(string username)
        {
            var user = await GetBy(username);
            user.Deactivated = true;
            user = (await UpdateUserAsync(user)).Data;
            return user != null;
        }

        public async Task<bool> Activate(string username)
        {
            var user = await GetBy(username);
            user.Deactivated = false;
            user = (await UpdateUserAsync(user)).Data;
            return user != null;
        }

        public async Task<bool> IsUserDeactivated(string username)
        {
            var user = await GetBy(username);
            return user.Deactivated;
        }

        public async Task<string> GeneratePhoneNumberToken(string username, string phone)
        {
            string token = null;
            var user = await GetBy(username);
            if (user.Phone1.Equals(phone) || user.Phone2.Equals(phone))
            {
                token = await _identityUserManager.GenerateChangePhoneNumberTokenAsync(user, phone);
            }
            return token;
        }

        public async Task<bool> CheckPhoneValidationToken(string username, string phone, string token)
        {
            var user = await GetBy(username);
            return await _identityUserManager.VerifyChangePhoneNumberTokenAsync(user, token, phone);
        }

        public async Task<bool> IsPhoneConfirmed(string username)
        {
            var user = await GetBy(username);
            return await _identityUserManager.IsPhoneNumberConfirmedAsync(user);
        }

        public async Task<bool> ConfirmPhone(string username, string phone, string token)
        {
            var user = await GetBy(username);
            if (await CheckPhoneValidationToken(username, phone, token))
            {
                user.PhoneNumberConfirmed = true;
                return (await UpdateUserAsync(user)) != null;
            }
            else
            {
                return false;
            }
        }

        public async Task<string> GenerateForgetPasswordToken(string username)
        {
            var user = await GetBy(username);
            var forgetPasswordToken = await _identityUserManager.GeneratePasswordResetTokenAsync(user);
            PasswordTokenPin passwordTokenPin = new PasswordTokenPin()
            {
                Token = forgetPasswordToken,
                Pin = StringUtilities.GetBase10(6)
            };
            _passwordTokenPinManager.Add(passwordTokenPin);
            return passwordTokenPin.Pin;
        }

        public async Task<bool> ForgetPassword(string username, string newPassword, string changePasswordPin)
        {
            var user = await GetBy(username);
            PasswordTokenPin passwordTokenPin = _passwordTokenPinManager.GetByPin(changePasswordPin);
            var identityResult = await _identityUserManager.ResetPasswordAsync(user, passwordTokenPin.Token, newPassword);
            if (identityResult.Succeeded)
            {
                _passwordTokenPinManager.Delete(passwordTokenPin);
            }
            return identityResult.Succeeded;
        }

        public async Task<bool> ResetPassword(string username, string newPassword)
        {
            var user = await GetBy(username);            
            var currentPassword = user.PasswordHash;
            var token = await _identityUserManager.GeneratePasswordResetTokenAsync(user);
            var identityResult = await _identityUserManager.ResetPasswordAsync(user, token, newPassword);
            user.PhoneNumberConfirmed = true;
            await UpdateUserAsync(user);
            return identityResult.Succeeded;
        }

        public async Task<List<ApplicationUser>> GetByIds(List<string> ids)
        {
            var result = _identityUserManager.Users.Where(u => ids.Contains(u.Id)).ToList();
            foreach (var user in result)
            {
                user.RoleNames = (List<string>)await GetRolesAsync(user);
            }
            return result;
        }

        public async Task<bool> ChangePassword(string username, string oldPassword, string newPassword)
        {
            var user = await GetBy(username);
            var result = await _identityUserManager.ChangePasswordAsync(user, oldPassword, newPassword);
            return result.Succeeded;
        }

        public async Task<ApplicationUser> UpdateUserCountry(string userId, Country countryId)
        {
            var user = await Get(userId);
            user.Fk_Country_Id = countryId;
            return (await UpdateUserAsync(user)).Data;
        }

        public async Task<ApplicationUser> UpdateUserLanguage(string userId, SupportedLanguage languageId)
        {
            var user = await Get(userId);
            user.Language = languageId;
            return (await UpdateUserAsync(user)).Data;
        }

        public async Task<ProcessResult<ApplicationUser>> UpdateUserEmail(string userId, string email)
        {
            var user = await Get(userId);
            user.Email = email;
            return await UpdateUserAsync(user);
        }

        public async Task<ProcessResult<ApplicationUser>> UpdateUserPhone(string userId, string phone)
        {
            var user = await Get(userId);
            user.Phone1 = phone;
            return await UpdateUserAsync(user);
        }

        public async Task<ProcessResult<ApplicationUser>> UpdateUserPicture(string userId, string picturePath)
        {
            var user = await Get(userId);
            user.PicturePath = picturePath;
            return await UpdateUserAsync(user);
        }

        public async Task<List<ApplicationUser>> GetEmployees(Country country)
        {
            var employees = (await GetAll()).Where(u => !u.RoleNames.Contains("Customer") && !u.RoleNames.Contains("Company") && u.Fk_Country_Id == country).ToList();
            return employees;
        }

        public async Task<IdentityPaginatedItems<ApplicationUser>> GetEmployeesPaginated(IdentityPaginatedItems<ApplicationUser> paginatedItems, Country country)
        {
            var emps = (await GetEmployees(country)).OrderBy(usr => usr.FirstName).ToList();
            try
            {
                //return GetPagesFromList(emps, paginatedItems);
                if (paginatedItems == null)
                {
                    paginatedItems = new IdentityPaginatedItems<ApplicationUser>();
                    paginatedItems.PageNo = 1;
                    paginatedItems.PageSize = 10;
                }
                var count = emps.Count();
                paginatedItems.Count = count;
                var skipCount = (paginatedItems.PageNo - 1) * paginatedItems.PageSize;
                var takeCount = paginatedItems.PageNo * paginatedItems.PageSize;
                if (paginatedItems.PageNo >= 0)
                {
                    if (count > paginatedItems.PageSize)
                    {
                        if (takeCount > count)
                        {
                            if (skipCount < count)
                            {
                                paginatedItems.Data = emps.Skip(skipCount).Take(count - skipCount).ToList();
                            }
                        }
                        else
                        {
                            paginatedItems.Data = emps.Skip(skipCount).Take(paginatedItems.PageSize).ToList();
                        }
                    }
                    else
                    {
                        paginatedItems.Data = emps.ToList();
                    }
                }

                return paginatedItems;

                //var data= new PaginatedItemsViewModel<WorkOrderReportResult>(pageIndex, paginatedItems.PageSize, count, orders);
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        private IdentityPaginatedItems<ApplicationUser> GetPagesFromList(List<ApplicationUser> users,
            IdentityPaginatedItems<ApplicationUser> paginatedItems)
        {
            if (paginatedItems == null)
            {
                paginatedItems = new IdentityPaginatedItems<ApplicationUser>
                {
                    PageNo = 1,
                    PageSize = 10
                };
            }
            var count = users.Count();
            paginatedItems.Count = count;
            var skipCount = (paginatedItems.PageNo - 1) * paginatedItems.PageSize;
            var takeCount = paginatedItems.PageNo * paginatedItems.PageSize;
            if (paginatedItems.PageNo >= 0)
            {
                if (count > paginatedItems.PageSize)
                {
                    if (takeCount > count)
                    {
                        if (skipCount < count)
                        {
                            paginatedItems.Data = users.Skip(skipCount).Take(count - skipCount).ToList();
                        }
                    }
                    else
                    {
                        paginatedItems.Data = users.Skip(skipCount).Take(paginatedItems.PageSize).ToList();
                    }
                }
                else
                {
                    paginatedItems.Data = users.ToList();
                }
            }
            return paginatedItems;
        }
    }
}
