﻿using Dispatching.Identity.BLL.IManagers;
using Dispatching.Identity.Models.Context;
using Dispatching.Identity.Models.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dispatching.Identity.BLL.Managers
{
    public class PrivilegeManager : IPrivilegeManager
    {
        private IRolePrivilegeManager _rolePrivilegeManager;
        private ApplicationDbContext _context;
        private IApplicationRoleManager _applicationRoleManager;

        public PrivilegeManager(IRolePrivilegeManager rolePrivilegeManager,
            ApplicationDbContext applicationDbContext,
            IApplicationRoleManager applicationRoleManager)
        {
            _applicationRoleManager = applicationRoleManager;
            _context = applicationDbContext;
            _rolePrivilegeManager = rolePrivilegeManager;
        }

        public async Task<Privilege> Add(Privilege privilege)
        {
            privilege = _context.Privileges.Add(privilege).Entity;
            if (_context.SaveChanges() > 0)
            {
                foreach (var roleName in privilege.Roles)
                {
                    var role = await _applicationRoleManager.GetByName(roleName);
                    _rolePrivilegeManager.Add(new RolePrivilege
                    {
                        Fk_ApplicationRole_Id = role?.Id,
                        Fk_Privilege_Id = privilege.Id
                    });
                }
                return privilege;
            }
            return null;
        }

        public Privilege GetById(int id)
        {
            return _context.Privileges.FirstOrDefault(p => p.Id == id);
        }

        public async Task<List<string>> GetPrivelegesByRole(string roleName)
        {
            var role = await _applicationRoleManager.GetByName(roleName);
            List<string> result = new List<string>();
            var rolePrivileges = _rolePrivilegeManager.GetByRoleId(role?.Id);
            List<int> privilegeIds = rolePrivileges.Select(x => x.Fk_Privilege_Id).ToList();

            foreach (var privilegeId in privilegeIds)
            {
                var privilege = GetById(privilegeId);
                result.Add(privilege?.Name);
            }
            return result;
        }

        public Privilege GetByName(string privilege)
        {
            return _context.Privileges.FirstOrDefault(p => p.Name.ToLower().Equals(privilege.ToLower()));
        }

        public bool PrivilegeExists(string privilege)
        {
            return _context.Privileges.FirstOrDefault(p => p.Name.ToLower().Equals(privilege.ToLower())) != null;
        }

        public async Task<bool> IsRoleHasPrivilege(string roleName, string privilege)
        {
            if (PrivilegeExists(privilege))
            {
                var privileges = await GetPrivelegesByRole(roleName);
                return privileges.Contains(privilege);
            }
            else
            {
                return true;
            }
        }

        public async Task<bool> IsRolesHasPrivilege(List<string> roles, string privilege)
        {
            if (PrivilegeExists(privilege))
            {
                List<string> privileges = new List<string>();
                foreach (var roleName in roles)
                {
                    privileges.AddRange(await GetPrivelegesByRole(roleName));
                }
                return privileges.Contains(privilege);
            }
            else
            {
                return true;
            }
        }
    }
}
