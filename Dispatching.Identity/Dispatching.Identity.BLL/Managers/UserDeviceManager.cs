﻿using System.Collections.Generic;
using System.Linq;
using Dispatching.Identity.BLL.IManagers;
using Dispatching.Identity.Models.Context;
using Dispatching.Identity.Models.Entities;

namespace Dispatching.Identity.BLL.Managers
{
    public class UserDeviceManager : IUserDeviceManager
    {
        private readonly ApplicationDbContext _dbContext;
        public UserDeviceManager(ApplicationDbContext dbContext)
        {
            _dbContext = dbContext;
        }

        public void AddIfNotExist(UserDevice userDevice)
        {
            var count = _dbContext.UserDevices.Count(x => x.Fk_AppUser_Id == userDevice.Fk_AppUser_Id);
            if (count > 0)
            {
                var isDeviceExist = _dbContext.UserDevices.Count(x =>
                                        x.Fk_AppUser_Id == userDevice.Fk_AppUser_Id &&
                                        x.DeveiceId == userDevice.DeveiceId) > 0;
                if (!isDeviceExist)
                {
                    _dbContext.UserDevices.Add(userDevice);
                }
            }
            else
            {
                _dbContext.UserDevices.Add(userDevice);
            }
            _dbContext.SaveChanges();
        }

        public List<UserDevice> GetByUserId(string userId)
        {
            var userDevices = _dbContext.UserDevices.Where(x => x.Fk_AppUser_Id == userId).ToList();
            return userDevices;
        }

        public bool DeleteDevice(string deviceId)
        {
            var userDevice = _dbContext.UserDevices.FirstOrDefault(x => x.DeveiceId == deviceId);
            _dbContext.Remove(userDevice);
            return _dbContext.SaveChanges() > 0;
        }
    }
}
