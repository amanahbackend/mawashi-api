﻿using System.Collections.Generic;
namespace Utilites.PaginatedItems
{
    public interface IIdentityPaginatedItems<TEntity> 
    {
         int PageNo { get;  set; }
         int PageSize { get;  set; }
         long Count { get; set; }
         List<TEntity> Data { get;  set; }
    }
}
