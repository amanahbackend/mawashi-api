﻿using System.Collections.Generic;
namespace Utilites.PaginatedItems
{
    public class IdentityPaginatedItems<TEntity>: IIdentityPaginatedItems<TEntity> 
    {
        public int PageNo { get; set; }

        public int PageSize { get; set; }

        public long Count { get; set; }

        public List<TEntity> Data { get;  set; }

        public IdentityPaginatedItems(int pageNo, int pageSize, long count, List<TEntity> data)
        {
            this.PageNo = pageNo;
            this.PageSize = pageSize;
            this.Count = count;
            this.Data = data;
        }
        public IdentityPaginatedItems()
        {
            this.PageNo = 1;
            this.PageSize = 10;
            this.Count = 0;
            this.Data = null;
        }
    }
}
