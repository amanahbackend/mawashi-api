﻿using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Dispatching.Ordering.EFCore.MSSQL.Migrations
{
    public partial class init : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "LKP_CityFees",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Code = table.Column<string>(nullable: false),
                    Country = table.Column<string>(nullable: false),
                    CreatedDate = table.Column<DateTime>(nullable: false),
                    DeletedDate = table.Column<DateTime>(nullable: false),
                    DeliveryFees = table.Column<double>(nullable: false),
                    FK_CreatedBy_Id = table.Column<string>(nullable: true),
                    FK_DeletedBy_Id = table.Column<string>(nullable: true),
                    FK_UpdatedBy_Id = table.Column<string>(nullable: true),
                    IsDeleted = table.Column<bool>(nullable: false),
                    IsSystemKey = table.Column<bool>(nullable: true),
                    NameAR = table.Column<string>(nullable: true),
                    NameEN = table.Column<string>(nullable: true),
                    UpdatedDate = table.Column<DateTime>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_LKP_CityFees", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "LKP_OrderStatus",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    CreatedDate = table.Column<DateTime>(nullable: false),
                    DeletedDate = table.Column<DateTime>(nullable: false),
                    FK_CreatedBy_Id = table.Column<string>(nullable: true),
                    FK_DeletedBy_Id = table.Column<string>(nullable: true),
                    FK_UpdatedBy_Id = table.Column<string>(nullable: true),
                    IsDeleted = table.Column<bool>(nullable: false),
                    IsSystemKey = table.Column<bool>(nullable: true),
                    NameAR = table.Column<string>(nullable: true),
                    NameEN = table.Column<string>(nullable: true),
                    UpdatedDate = table.Column<DateTime>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_LKP_OrderStatus", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Order",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    AppartmentNumber = table.Column<string>(nullable: false),
                    Area = table.Column<string>(nullable: false),
                    Block = table.Column<string>(nullable: false),
                    Building = table.Column<string>(nullable: false),
                    CardType = table.Column<string>(nullable: true),
                    CartPrice = table.Column<double>(nullable: false),
                    CartVAT = table.Column<double>(nullable: false),
                    Code = table.Column<string>(nullable: false),
                    CreatedDate = table.Column<DateTime>(nullable: false),
                    DeletedDate = table.Column<DateTime>(nullable: false),
                    DeliveryDate = table.Column<DateTime>(nullable: false),
                    DeliveryFees = table.Column<double>(nullable: false),
                    DeliveryNote = table.Column<string>(nullable: false),
                    DeliveryType = table.Column<string>(nullable: false),
                    FK_Cart_Id = table.Column<int>(nullable: false),
                    FK_CityDeliveryFees_Id = table.Column<int>(nullable: false),
                    FK_CreatedBy_Id = table.Column<string>(nullable: true),
                    FK_Customer_Id = table.Column<int>(nullable: false),
                    FK_DeletedBy_Id = table.Column<string>(nullable: true),
                    FK_DeliveryType_Id = table.Column<int>(nullable: false),
                    FK_Order_Status_Id = table.Column<int>(nullable: false),
                    FK_Order_Type_Id = table.Column<int>(nullable: false),
                    FK_PaymentType_Id = table.Column<int>(nullable: false),
                    FK_Platform_Id = table.Column<int>(nullable: false),
                    FK_UpdatedBy_Id = table.Column<string>(nullable: true),
                    Fk_AddressType_Id = table.Column<int>(nullable: false),
                    Fk_Country_Id = table.Column<int>(nullable: false),
                    Fk_Location_Id = table.Column<int>(nullable: false),
                    Floor = table.Column<string>(nullable: false),
                    Governorate = table.Column<string>(nullable: false),
                    IsDeleted = table.Column<bool>(nullable: false),
                    Latitude = table.Column<double>(nullable: false),
                    Longitude = table.Column<double>(nullable: false),
                    Name = table.Column<string>(nullable: false),
                    Number = table.Column<string>(nullable: false),
                    PaymentTypeCode = table.Column<string>(nullable: true),
                    Platform = table.Column<string>(nullable: true),
                    Price = table.Column<double>(nullable: false),
                    ReciptNo = table.Column<string>(nullable: true),
                    Street = table.Column<string>(nullable: false),
                    TransactionCode = table.Column<string>(nullable: true),
                    TransactionNo = table.Column<string>(nullable: true),
                    UpdatedDate = table.Column<DateTime>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Order", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "RoleOrderStatus",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    CreatedDate = table.Column<DateTime>(nullable: false),
                    CurrentUserId = table.Column<string>(nullable: true),
                    DeletedDate = table.Column<DateTime>(nullable: false),
                    FK_CreatedBy_Id = table.Column<string>(nullable: true),
                    FK_DeletedBy_Id = table.Column<string>(nullable: true),
                    FK_Order_Status_Id = table.Column<string>(nullable: true),
                    FK_Role_Id = table.Column<string>(nullable: true),
                    FK_UpdatedBy_Id = table.Column<string>(nullable: true),
                    IsDeleted = table.Column<bool>(nullable: false),
                    LKP_OrderStatusId = table.Column<int>(nullable: true),
                    UpdatedDate = table.Column<DateTime>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_RoleOrderStatus", x => x.Id);
                    table.ForeignKey(
                        name: "FK_RoleOrderStatus_LKP_OrderStatus_LKP_OrderStatusId",
                        column: x => x.LKP_OrderStatusId,
                        principalTable: "LKP_OrderStatus",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_RoleOrderStatus_LKP_OrderStatusId",
                table: "RoleOrderStatus",
                column: "LKP_OrderStatusId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "LKP_CityFees");

            migrationBuilder.DropTable(
                name: "Order");

            migrationBuilder.DropTable(
                name: "RoleOrderStatus");

            migrationBuilder.DropTable(
                name: "LKP_OrderStatus");
        }
    }
}
