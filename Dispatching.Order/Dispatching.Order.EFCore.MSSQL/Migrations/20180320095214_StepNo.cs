﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Dispatching.Ordering.EFCore.MSSQL.Migrations
{
    public partial class StepNo : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<int>(
                name: "FK_Order_Status_Id",
                table: "RoleOrderStatus",
                nullable: false,
                oldClrType: typeof(string),
                oldNullable: true);

            migrationBuilder.AddColumn<int>(
                name: "StepNo",
                table: "RoleOrderStatus",
                nullable: false,
                defaultValue: 0);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "StepNo",
                table: "RoleOrderStatus");

            migrationBuilder.AlterColumn<string>(
                name: "FK_Order_Status_Id",
                table: "RoleOrderStatus",
                nullable: true,
                oldClrType: typeof(int));
        }
    }
}
