﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Dispatching.Ordering.EFCore.MSSQL.Migrations
{
    public partial class ProducerInfo : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "FK_Producer_Id",
                table: "Order",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "ProducerName",
                table: "Order",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "FK_Producer_Id",
                table: "Order");

            migrationBuilder.DropColumn(
                name: "ProducerName",
                table: "Order");
        }
    }
}
