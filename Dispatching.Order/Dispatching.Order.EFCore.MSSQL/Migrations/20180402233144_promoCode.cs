﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Dispatching.Ordering.EFCore.MSSQL.Migrations
{
    public partial class promoCode : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "FK_Promo_Id",
                table: "Order",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<bool>(
                name: "HasPromoCode",
                table: "Order",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<string>(
                name: "PromoCode",
                table: "Order",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "PromoValue",
                table: "Order",
                nullable: false,
                defaultValue: 0);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "FK_Promo_Id",
                table: "Order");

            migrationBuilder.DropColumn(
                name: "HasPromoCode",
                table: "Order");

            migrationBuilder.DropColumn(
                name: "PromoCode",
                table: "Order");

            migrationBuilder.DropColumn(
                name: "PromoValue",
                table: "Order");
        }
    }
}
