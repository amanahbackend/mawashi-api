﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Dispatching.Ordering.EFCore.MSSQL.Migrations
{
    public partial class VatObj : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "VAT",
                table: "Order",
                newName: "VATValue");

            migrationBuilder.AddColumn<double>(
                name: "VATNameAR",
                table: "Order",
                nullable: false,
                defaultValue: 0.0);

            migrationBuilder.AddColumn<double>(
                name: "VATNameEN",
                table: "Order",
                nullable: false,
                defaultValue: 0.0);

            migrationBuilder.AddColumn<double>(
                name: "VATPercentage",
                table: "Order",
                nullable: false,
                defaultValue: 0.0);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "VATNameAR",
                table: "Order");

            migrationBuilder.DropColumn(
                name: "VATNameEN",
                table: "Order");

            migrationBuilder.DropColumn(
                name: "VATPercentage",
                table: "Order");

            migrationBuilder.RenameColumn(
                name: "VATValue",
                table: "Order",
                newName: "VAT");
        }
    }
}
