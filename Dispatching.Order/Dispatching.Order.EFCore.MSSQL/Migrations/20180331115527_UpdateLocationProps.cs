﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Dispatching.Ordering.EFCore.MSSQL.Migrations
{
    public partial class UpdateLocationProps : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "Street",
                table: "PickUp",
                newName: "StreetName");

            migrationBuilder.RenameColumn(
                name: "Governorate",
                table: "PickUp",
                newName: "StreetId");

            migrationBuilder.RenameColumn(
                name: "Block",
                table: "PickUp",
                newName: "GovernorateName");

            migrationBuilder.RenameColumn(
                name: "Area",
                table: "PickUp",
                newName: "GovernorateId");

            migrationBuilder.RenameColumn(
                name: "Street",
                table: "Order",
                newName: "StreetName");

            migrationBuilder.RenameColumn(
                name: "Governorate",
                table: "Order",
                newName: "StreetId");

            migrationBuilder.RenameColumn(
                name: "Block",
                table: "Order",
                newName: "GovernorateName");

            migrationBuilder.RenameColumn(
                name: "Area",
                table: "Order",
                newName: "GovernorateId");

            migrationBuilder.RenameColumn(
                name: "Street",
                table: "Donation",
                newName: "StreetName");

            migrationBuilder.RenameColumn(
                name: "Governorate",
                table: "Donation",
                newName: "StreetId");

            migrationBuilder.RenameColumn(
                name: "Block",
                table: "Donation",
                newName: "GovernorateName");

            migrationBuilder.RenameColumn(
                name: "Area",
                table: "Donation",
                newName: "GovernorateId");

            migrationBuilder.AddColumn<string>(
                name: "AreaId",
                table: "PickUp",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "AreaName",
                table: "PickUp",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "BlockId",
                table: "PickUp",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "BlockName",
                table: "PickUp",
                nullable: true);

            migrationBuilder.AlterColumn<int>(
                name: "Fk_Country_Id",
                table: "Order",
                nullable: false,
                oldClrType: typeof(int),
                oldNullable: true);

            migrationBuilder.AddColumn<string>(
                name: "AreaId",
                table: "Order",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "AreaName",
                table: "Order",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "BlockId",
                table: "Order",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "BlockName",
                table: "Order",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "AreaId",
                table: "Donation",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "AreaName",
                table: "Donation",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "BlockId",
                table: "Donation",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "BlockName",
                table: "Donation",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "AreaId",
                table: "PickUp");

            migrationBuilder.DropColumn(
                name: "AreaName",
                table: "PickUp");

            migrationBuilder.DropColumn(
                name: "BlockId",
                table: "PickUp");

            migrationBuilder.DropColumn(
                name: "BlockName",
                table: "PickUp");

            migrationBuilder.DropColumn(
                name: "AreaId",
                table: "Order");

            migrationBuilder.DropColumn(
                name: "AreaName",
                table: "Order");

            migrationBuilder.DropColumn(
                name: "BlockId",
                table: "Order");

            migrationBuilder.DropColumn(
                name: "BlockName",
                table: "Order");

            migrationBuilder.DropColumn(
                name: "AreaId",
                table: "Donation");

            migrationBuilder.DropColumn(
                name: "AreaName",
                table: "Donation");

            migrationBuilder.DropColumn(
                name: "BlockId",
                table: "Donation");

            migrationBuilder.DropColumn(
                name: "BlockName",
                table: "Donation");

            migrationBuilder.RenameColumn(
                name: "StreetName",
                table: "PickUp",
                newName: "Street");

            migrationBuilder.RenameColumn(
                name: "StreetId",
                table: "PickUp",
                newName: "Governorate");

            migrationBuilder.RenameColumn(
                name: "GovernorateName",
                table: "PickUp",
                newName: "Block");

            migrationBuilder.RenameColumn(
                name: "GovernorateId",
                table: "PickUp",
                newName: "Area");

            migrationBuilder.RenameColumn(
                name: "StreetName",
                table: "Order",
                newName: "Street");

            migrationBuilder.RenameColumn(
                name: "StreetId",
                table: "Order",
                newName: "Governorate");

            migrationBuilder.RenameColumn(
                name: "GovernorateName",
                table: "Order",
                newName: "Block");

            migrationBuilder.RenameColumn(
                name: "GovernorateId",
                table: "Order",
                newName: "Area");

            migrationBuilder.RenameColumn(
                name: "StreetName",
                table: "Donation",
                newName: "Street");

            migrationBuilder.RenameColumn(
                name: "StreetId",
                table: "Donation",
                newName: "Governorate");

            migrationBuilder.RenameColumn(
                name: "GovernorateName",
                table: "Donation",
                newName: "Block");

            migrationBuilder.RenameColumn(
                name: "GovernorateId",
                table: "Donation",
                newName: "Area");

            migrationBuilder.AlterColumn<int>(
                name: "Fk_Country_Id",
                table: "Order",
                nullable: true,
                oldClrType: typeof(int));
        }
    }
}
