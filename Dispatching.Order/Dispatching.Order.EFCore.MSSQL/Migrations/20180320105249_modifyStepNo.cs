﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Dispatching.Ordering.EFCore.MSSQL.Migrations
{
    public partial class modifyStepNo : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "StepNo",
                table: "RoleOrderStatus");

            migrationBuilder.AddColumn<int>(
                name: "StepNo",
                table: "LKP_OrderStatus",
                nullable: false,
                defaultValue: 0);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "StepNo",
                table: "LKP_OrderStatus");

            migrationBuilder.AddColumn<int>(
                name: "StepNo",
                table: "RoleOrderStatus",
                nullable: false,
                defaultValue: 0);
        }
    }
}
