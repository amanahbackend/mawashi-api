﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Dispatching.Ordering.EFCore.MSSQL.Migrations
{
    public partial class paymentStatus : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<bool>(
                name: "IsPaymentProcessed",
                table: "Order",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<string>(
                name: "IsPaymentSucceeded",
                table: "Order",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "PaymentMessage",
                table: "Order",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "IsPaymentProcessed",
                table: "Order");

            migrationBuilder.DropColumn(
                name: "IsPaymentSucceeded",
                table: "Order");

            migrationBuilder.DropColumn(
                name: "PaymentMessage",
                table: "Order");
        }
    }
}
