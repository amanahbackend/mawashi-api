﻿using Dispatching.CustomerModule.API.ServicesSettings;
using DispatchProduct.Api.HttpClient;
using DispatchProduct.HttpClient;
using DnsClient;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Utilites.ProcessingResult;
using Dispatching.Ordering.API.ServicesViewModels.DeliveryOption;
using DispatchProduct.Ordering.API.ServicesCommunication.Settings;
using Dispatching.Ordering.API.ServicesViewModels.DropOut;
using Microsoft.AspNetCore.Mvc;

namespace Dispatching.Ordering.API.ServiceCommunications
{
    public class EmailService
       : DefaultHttpClientCrud<EmailServiceSetting, SendMailViewModel, SendMailViewModel>,
       IEmailService
    {
        EmailServiceSetting _settings;
        IDnsQuery _dnsQuery;
        public EmailService(IOptions<EmailServiceSetting> obj, IDnsQuery dnsQuery) : base(obj.Value, dnsQuery)
        {
            _dnsQuery = dnsQuery;
            _settings = obj.Value;
        }
        public async Task<ProcessResultViewModel<bool>> Send(SendMailViewModel model)
        {
            var requesturi = $"{await _settings.GetUri(_dnsQuery)}/{_settings.SendVerb}";
            string baseUrl = await _settings.GetUri(_dnsQuery);
            while (baseUrl.EndsWith('/'))
            {
                baseUrl = baseUrl.Substring(0, baseUrl.Length - 1);
            }
            var result = await PostCustomize<SendMailViewModel, bool>(requesturi, model);
            return result;
        }
    }
}
