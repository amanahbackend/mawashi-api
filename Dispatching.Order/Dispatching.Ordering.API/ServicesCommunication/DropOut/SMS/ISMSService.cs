﻿using Dispatching.CustomerModule.API.ServicesSettings;
using DispatchProduct.HttpClient;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Utilites.ProcessingResult;
using Dispatching.Ordering.API.ServicesViewModels.DeliveryOption;
using DispatchProduct.Ordering.API.ServicesCommunication.Settings;
using Dispatching.Ordering.API.ServicesViewModels.DropOut;
using Microsoft.AspNetCore.Mvc;

namespace Dispatching.Ordering.API.ServiceCommunications

{
    public interface ISMSService
       : IDefaultHttpClientCrud<SMSServiceSetting, SendSmsViewModel, SendSmsViewModel>
    {
        Task<ProcessResultViewModel<bool>> Send(SendSmsViewModel model);
    }
}
