﻿using Dispatching.Ordering.API.ServiceCommunications;
using Dispatching.Ordering.API.ServicesSettings.DropOut.Notification;
using Dispatching.Ordering.API.ServicesViewModels.DropOut;
using DispatchProduct.HttpClient;
using DnsClient;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Utilites.ProcessingResult;

namespace Dispatching.Ordering.API.ServicesCommunication.DropOut.Notification
{
    public class NotificationService :
        DefaultHttpClientCrud<NotificationServiceSetting, SendNotificationViewModel, SendNotificationViewModel>,
       INotificationService
    {
        NotificationServiceSetting _settings;
        IDnsQuery _dnsQuery;
        public NotificationService(IOptions<NotificationServiceSetting> obj, IDnsQuery dnsQuery) : base(obj.Value, dnsQuery)
        {
            _dnsQuery = dnsQuery;
            _settings = obj.Value;
        }

        public async Task<string> Send(SendNotificationViewModel model)
        {
            var requesturi = $"{await _settings.GetUri(_dnsQuery)}/{_settings.SendVerb}";
            string baseUrl = await _settings.GetUri(_dnsQuery);
            while (baseUrl.EndsWith('/'))
            {
                baseUrl = baseUrl.Substring(0, baseUrl.Length - 1);
            }
            var result = await PostCustomize<SendNotificationViewModel, string>(requesturi, model);
            return result.Data;
        }
    }
}
