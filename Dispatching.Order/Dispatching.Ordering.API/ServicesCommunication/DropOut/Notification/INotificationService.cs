﻿using Dispatching.Ordering.API.ServicesSettings.DropOut.Notification;
using Dispatching.Ordering.API.ServicesViewModels.DropOut;
using DispatchProduct.HttpClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Utilites.ProcessingResult;

namespace Dispatching.Ordering.API.ServicesCommunication.DropOut.Notification
{
    public interface INotificationService
        : IDefaultHttpClientCrud<NotificationServiceSetting, SendNotificationViewModel, SendNotificationViewModel>
    {
        Task<string> Send(SendNotificationViewModel model);
    }
}
