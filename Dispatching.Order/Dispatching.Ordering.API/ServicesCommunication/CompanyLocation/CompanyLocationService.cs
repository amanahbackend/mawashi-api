﻿using Dispatching.Ordering.API.ServicesViewModels;
using DispatchProduct.HttpClient;
using DispatchProduct.Ordering.API.ServicesCommunication.Location;
using DispatchProduct.Ordering.API.ServicesCommunication.Settings;
using DnsClient;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Utilites.ProcessingResult;
using Utilities.Utilites.PACI;
namespace Dispatching.Ordering.API.ServicesCommunication.Location
{
    public class CompanyLocationService : DefaultHttpClientCrud<CompanyLocationServiceSetting, CompanyLocationViewModel, CompanyLocationViewModel>, ICompanyLocationService
    {
        CompanyLocationServiceSetting settings;
        IDnsQuery dnsQuery;

        public CompanyLocationService(IOptions<CompanyLocationServiceSetting> _settings, IDnsQuery _dnsQuery) : base(_settings.Value, _dnsQuery)
        {
            settings = _settings.Value;
            dnsQuery = _dnsQuery;
        }

        public async Task<ProcessResultViewModel<List<LocationViewModel>>> GetByCompanyId(int CompanyId)
        {
            var requesturi = $"{await settings.GetUri(dnsQuery)}/{settings.GetByCompanyIdVerb}?CompanyId={CompanyId}";
            string baseUrl = await settings.GetUri(dnsQuery);
            while (baseUrl.EndsWith('/'))
            {
                baseUrl = baseUrl.Substring(0, baseUrl.Length - 1);
            }
            var result = await GetByUriCustomized<List<LocationViewModel>>(requesturi);
            return result;
        }
    }
}
