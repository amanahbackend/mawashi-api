﻿using Dispatching.Ordering.API.ServicesViewModels;
using DispatchProduct.HttpClient;
using DispatchProduct.Ordering.API.ServicesCommunication.Settings;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Utilites.ProcessingResult;
using Utilities.Utilites.PACI;
namespace Dispatching.Ordering.API.ServicesCommunication.Location
{
    public interface ICustomerLocationService : IDefaultHttpClientCrud<CustomerLocationServiceSetting, LocationViewModel, LocationViewModel>
    {
        Task<ProcessResultViewModel<List<LocationViewModel>>> GetByCustomerId(int customerId);
    }
}
