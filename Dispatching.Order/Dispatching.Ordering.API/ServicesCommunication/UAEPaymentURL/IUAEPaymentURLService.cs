﻿using Dispatching.Ordering.API.ServicesViewModels;
using DispatchProduct.HttpClient;
using DispatchProduct.Ordering.API.ServicesCommunication.Settings;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Utilites.ProcessingResult;

namespace DispatchProduct.Ordering.API.ServicesCommunication.PaymentURL
{
    public interface IUAEPaymentURLService : IDefaultHttpClientCrud<UAEPaymentURLServiceSetting, PaymentURLViewModel, PaymentURLViewModel>
    {
        Task<ProcessResultViewModel<string>> GetUAEPaymentURL(PaymentURLViewModel model);
    }
}
