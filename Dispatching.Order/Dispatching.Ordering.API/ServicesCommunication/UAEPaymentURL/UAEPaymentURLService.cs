﻿using Dispatching.Ordering.API.ServicesViewModels;
using DispatchProduct.HttpClient;
using DispatchProduct.Ordering.API.ServicesCommunication.PaymentURL;
using DispatchProduct.Ordering.API.ServicesCommunication.Settings;
using DnsClient;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Utilites.ProcessingResult;

namespace Dispatching.Ordering.API.ServicesCommunication.PaymentURL
{
    public class UAEPaymentURLService : DefaultHttpClientCrud<UAEPaymentURLServiceSetting, PaymentURLViewModel, PaymentURLViewModel>, IUAEPaymentURLService
    {
        UAEPaymentURLServiceSetting settings;
        IDnsQuery dnsQuery;
        public UAEPaymentURLService(IOptions<UAEPaymentURLServiceSetting> _settings, IDnsQuery _dnsQuery) :base(_settings.Value, _dnsQuery)
        {
            settings = _settings.Value;
            dnsQuery = _dnsQuery;

        }
        public async Task<ProcessResultViewModel<string>> GetUAEPaymentURL(PaymentURLViewModel model)
        {
            var requesturi = $"{await settings.GetUri(dnsQuery)}/{settings.PayVerb}";
            string baseUrl = await settings.GetUri(dnsQuery);
            while (baseUrl.EndsWith('/'))
            {
                baseUrl = baseUrl.Substring(0, baseUrl.Length - 1);
            }
            var result = await PostCustomize<PaymentURLViewModel, string>(requesturi, model);
            return result;
        }
    }
}
