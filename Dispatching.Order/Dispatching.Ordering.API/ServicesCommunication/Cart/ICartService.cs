﻿using Dispatching.Ordering.API.ServicesViewModels;
using Dispatching.Ordering.API.ServicesViewModels.Cart;
using Dispatching.Ordering.API.ServicesViewModelsViewModel.Cart;
using DispatchProduct.HttpClient;
using DispatchProduct.Ordering.API.ServicesCommunication.Settings;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Utilites.ProcessingResult;

namespace DispatchProduct.Ordering.API.ServicesCommunication.Cart
{
    public interface ICartService : IDefaultHttpClientCrud<CartServiceSetting, CartViewModel, CartViewModel>
    {
        Task<ProcessResultViewModel<CartViewModel>> SubmitCart(CartViewModel model);
        Task<ProcessResultViewModel<CartViewModel>> SubmitSalesCartSales(SalesCartViewModel model);
        Task<ProcessResultViewModel<bool>> UpdateOrderCartInfo(CartOrderViewModel model);
        Task<ProcessResultViewModel<CartViewModel>> GetCartWithItems(int cartId);
        Task<ProcessResultViewModel<List<CartViewModel>>> GetCartsWithItems(List<int> cartIds);
    }
}
