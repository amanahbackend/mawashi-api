﻿using Dispatching.Ordering.API.ServicesViewModels;
using Dispatching.Ordering.API.ServicesViewModels.Cart;
using Dispatching.Ordering.API.ServicesViewModelsViewModel.Cart;
using DispatchProduct.HttpClient;
using DispatchProduct.Ordering.API.ServicesCommunication.Cart;
using DispatchProduct.Ordering.API.ServicesCommunication.Settings;
using DnsClient;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Utilites.ProcessingResult;

namespace Dispatching.Ordering.API.ServicesCommunication.Cart
{
    public class CartService : DefaultHttpClientCrud<CartServiceSetting, CartViewModel, CartViewModel>, ICartService
    {
        CartServiceSetting settings;
        IDnsQuery dnsQuery;
        public CartService(IOptions<CartServiceSetting> _settings, IDnsQuery _dnsQuery) : base(_settings.Value, _dnsQuery)
        {
            settings = _settings.Value;
            dnsQuery = _dnsQuery;
        }
        public async Task<ProcessResultViewModel<CartViewModel>> SubmitSalesCartSales(SalesCartViewModel model)
        {
            var requesturi = $"{await settings.GetUri(dnsQuery)}/{settings.SubmitSalesVerb}";
            string baseUrl = await settings.GetUri(dnsQuery);
            while (baseUrl.EndsWith('/'))
            {
                baseUrl = baseUrl.Substring(0, baseUrl.Length - 1);
            }
            var result = await PostCustomize<SalesCartViewModel, CartViewModel>(requesturi, model);
            return result;
        }
        public async Task<ProcessResultViewModel<CartViewModel>> SubmitCart(CartViewModel model)
        {
            var requesturi = $"{await settings.GetUri(dnsQuery)}/{settings.SubmitVerb}";
            string baseUrl = await settings.GetUri(dnsQuery);
            while (baseUrl.EndsWith('/'))
            {
                baseUrl = baseUrl.Substring(0, baseUrl.Length - 1);
            }
            var result = await Post(requesturi, model);
            return result;
        }
        public async Task<ProcessResultViewModel<bool>> UpdateOrderCartInfo(CartOrderViewModel model)
        {
            var requesturi = $"{await settings.GetUri(dnsQuery)}/{settings.UpdateOrderCartInfoVerb}";
            string baseUrl = await settings.GetUri(dnsQuery);
            while (baseUrl.EndsWith('/'))
            {
                baseUrl = baseUrl.Substring(0, baseUrl.Length - 1);
            }
            var result = await PostCustomize<CartOrderViewModel, bool>(requesturi, model);
            return result;
        }
        public async Task<ProcessResultViewModel<CartViewModel>> GetCartWithItems(int cartId)
        {
            var requesturi = $"{await settings.GetUri(dnsQuery)}/{settings.GetCartWithItemsVerb}/{cartId}";
            string baseUrl = await settings.GetUri(dnsQuery);
            while (baseUrl.EndsWith('/'))
            {
                baseUrl = baseUrl.Substring(0, baseUrl.Length - 1);
            }
            var result = await GetByUri(requesturi);
            return result;
        }

        public async Task<ProcessResultViewModel<List<CartViewModel>>> GetCartsWithItems(List<int> cartIds)
        {
            var requesturi = $"{await settings.GetUri(dnsQuery)}/{settings.GetCartsWithItemsVerb}";
            string baseUrl = await settings.GetUri(dnsQuery);
            while (baseUrl.EndsWith('/'))
            {
                baseUrl = baseUrl.Substring(0, baseUrl.Length - 1);
            }
            var result = await PostCustomize<List<int>, List<CartViewModel>>(requesturi, cartIds);
            return result;
        }
    }
}
