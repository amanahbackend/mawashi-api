﻿using Dispatching.CustomerModule.API.ServicesSettings;
using DispatchProduct.HttpClient;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Utilites.ProcessingResult;
using Dispatching.Ordering.API.ServicesViewModels.DeliveryOption;

namespace Dispatching.Ordering.API.ServiceCommunications

{
    public interface IDeliveryOptionService
       : IDefaultHttpClientCrud<DeliveryOptionServiceSettings, CategoryInputDeliveryOptions, CategoryInputDeliveryOptions>
    {
        Task<ProcessResultViewModel<int>> GetDateType(CategoryInputDeliveryOptions model);
    }
}
