﻿using Dispatching.CustomerModule.API.ServicesSettings;
using DispatchProduct.Api.HttpClient;
using DispatchProduct.HttpClient;
using DnsClient;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Utilites.ProcessingResult;
using Dispatching.Ordering.API.ServicesViewModels.DeliveryOption;
namespace Dispatching.Ordering.API.ServiceCommunications
{
    public class DeliveryOptionService
       : DefaultHttpClientCrud<DeliveryOptionServiceSettings,CategoryInputDeliveryOptions, CategoryInputDeliveryOptions>,
       IDeliveryOptionService
    {
        DeliveryOptionServiceSettings _settings;
        IDnsQuery _dnsQuery;

        public DeliveryOptionService(IOptions<DeliveryOptionServiceSettings> obj, IDnsQuery dnsQuery) : base(obj.Value, dnsQuery)
        {
            _dnsQuery = dnsQuery;
            _settings = obj.Value;
        }

        public async Task<ProcessResultViewModel<int>> GetDateType(CategoryInputDeliveryOptions model)
        {
            var requesturi = $"{await _settings.GetUri(_dnsQuery)}/{_settings.GetDateTypeVerb}";
            string baseUrl = await _settings.GetUri(_dnsQuery);
            while (baseUrl.EndsWith('/'))
            {
                baseUrl = baseUrl.Substring(0, baseUrl.Length - 1);
            }
            var result = await PostCustomize<CategoryInputDeliveryOptions, int>(requesturi, model);
            return result;
        }
    }
}
