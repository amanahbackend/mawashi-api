﻿using Dispatching.CustomerModule.API.ViewModels;
using Dispatching.Ordering.API.ServicesViewModels;
using DispatchProduct.HttpClient;
using DispatchProduct.Ordering.API.ServicesCommunication.Location;
using DispatchProduct.Ordering.API.ServicesCommunication.Settings;
using DnsClient;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Utilites.ProcessingResult;
using Utilities.Utilites.PACI;
namespace Dispatching.Ordering.API.ServicesCommunication.Location
{
    public class CompanyService : DefaultHttpClientCrud<CompanyServiceSetting, CompanyViewModel, CompanyViewModel>, ICompanyService
    {
        CompanyServiceSetting settings;
        IDnsQuery dnsQuery;

        public CompanyService(IOptions<CompanyServiceSetting> _settings, IDnsQuery _dnsQuery) : base(_settings.Value, _dnsQuery)
        {
            settings = _settings.Value;
            dnsQuery = _dnsQuery;
        }

        public async Task<ProcessResultViewModel<CompanyViewModel>> GetByCompanyId(int customerId)
        {
            var requesturi = $"{await settings.GetUri(dnsQuery)}/{settings.GetByCompanyIdVerb}?id={customerId}";
            string baseUrl = await settings.GetUri(dnsQuery);
            while (baseUrl.EndsWith('/'))
            {
                baseUrl = baseUrl.Substring(0, baseUrl.Length - 1);
            }
            var result = await GetByUriCustomized<CompanyViewModel>(requesturi);
            return result;
        }
        public async Task<ProcessResultViewModel<bool>> PayOrderFromWallet(PayFromWallet model)
        {
            var requesturi = $"{await settings.GetUri(dnsQuery)}/{settings.PayOrderFromWalletVerb}";
            string baseUrl = await settings.GetUri(dnsQuery);
            while (baseUrl.EndsWith('/'))
            {
                baseUrl = baseUrl.Substring(0, baseUrl.Length - 1);
            }
            var result = await PostCustomize<PayFromWallet,bool>(requesturi,model);
            return result;
        }

    }
}
