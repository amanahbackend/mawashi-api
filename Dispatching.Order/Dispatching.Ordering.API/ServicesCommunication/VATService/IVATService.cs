﻿using Dispatching.Ordering.API.ServicesViewModels;
using Dispatching.Ordering.API.ServicesViewModels.VAT;
using DispatchProduct.HttpClient;
using DispatchProduct.Ordering.API.ServicesCommunication.Settings;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Utilites.ProcessingResult;

namespace DispatchProduct.Ordering.API.ServicesCommunication.VAT
{
    public interface IVATService : IDefaultHttpClientCrud<VATServiceSetting, LKP_VATViewModel, LKP_VATViewModel>
    {
        Task<ProcessResultViewModel<LKP_VATViewModel>> GetVAT(string vat=null);
    }
}
