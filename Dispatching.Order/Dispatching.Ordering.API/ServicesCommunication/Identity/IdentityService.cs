﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Dispatching.Ordering.API.ServiceSettings;
using Dispatching.Ordering.API.ServicesViewModels;
using DispatchProduct.HttpClient;
using DnsClient;
using Microsoft.Extensions.Options;
using Utilites.ProcessingResult;

namespace Dispatching.Ordering.API.ServiceCommunications.Identity
{
    public class IdentityService
        : DefaultHttpClientCrud<IdentityServiceSettings, string,List< UserDeviceViewModel>>,
            IIdentityService
    {
        private readonly IdentityServiceSettings _settings;
        private readonly IDnsQuery _dnsQuery;

        public IdentityService(IOptions<IdentityServiceSettings> obj, IDnsQuery dnsQuery) : base(obj.Value, dnsQuery)
        {
            _dnsQuery = dnsQuery;
            _settings = obj.Value;
        }

        public async Task<ProcessResultViewModel<List<UserDeviceViewModel>>> GetUserDevices(string version, string userId)
        {
            var baseUrl = $"{await _settings.GetUri(_dnsQuery)}";

            //var baseUrl = await _settings.GetBaseUrl(_dnsQuery);
            while (baseUrl.EndsWith('/'))
            {
                baseUrl = baseUrl.Substring(0, baseUrl.Length - 1);
            }
            if (!string.IsNullOrEmpty(baseUrl))
            {
                var requestedAction = _settings.GetUserDevicesAction;
                var url = $"{baseUrl}/{requestedAction}/{userId}";
                var result = await Get(url);
                return result;
            }
            return null;
        }
    }
}
