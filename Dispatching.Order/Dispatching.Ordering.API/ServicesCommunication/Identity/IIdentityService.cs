﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Dispatching.Ordering.API.ServiceSettings;
using Dispatching.Ordering.API.ServicesViewModels;
using DispatchProduct.HttpClient;
using Utilites.ProcessingResult;

namespace Dispatching.Ordering.API.ServiceCommunications.Identity
{
    public interface IIdentityService :
        IDefaultHttpClientCrud<IdentityServiceSettings, string, List<UserDeviceViewModel>>
    {
        Task<ProcessResultViewModel<List<UserDeviceViewModel>>> GetUserDevices(string version, string userId);
    }
}
