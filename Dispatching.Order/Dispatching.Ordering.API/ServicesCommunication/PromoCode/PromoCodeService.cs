﻿using Dispatching.Ordering.API.ServicesViewModels;
using Dispatching.PromoCodeModule.Models.Entities;
using DispatchProduct.HttpClient;
using DispatchProduct.Ordering.API.ServicesCommunication.Location;
using DispatchProduct.Ordering.API.ServicesCommunication.PromoCode;
using DispatchProduct.Ordering.API.ServicesCommunication.Settings;
using DnsClient;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Utilites.ProcessingResult;
using Utilities.Utilites.PACI;
namespace Dispatching.Ordering.API.ServicesCommunication.Location
{
    public class PromoCodeService : DefaultHttpClientCrud<PromoCodeServiceSetting, PromoCodeValidation, PromoResult>, IPromoCodeService
    {
        PromoCodeServiceSetting settings;
        IDnsQuery dnsQuery;

        public PromoCodeService(IOptions<PromoCodeServiceSetting> _settings, IDnsQuery _dnsQuery) :base(_settings.Value, _dnsQuery)
        {
            settings = _settings.Value;
            dnsQuery = _dnsQuery;
        }
        public async Task<ProcessResultViewModel<PromoResult>> SubmitPromoCode(PromoCodeValidation model)
        {
            var requesturi = $"{await settings.GetUri(dnsQuery)}/{settings.CheckPromoCodeVerb}";

            string baseUrl = await settings.GetUri(dnsQuery);
            while (baseUrl.EndsWith('/'))
            {
                baseUrl = baseUrl.Substring(0, baseUrl.Length - 1);
            }
            if (!String.IsNullOrEmpty(baseUrl))
            {
                return await Post(requesturi,model);
            }
            return null;
        }

    }
}
