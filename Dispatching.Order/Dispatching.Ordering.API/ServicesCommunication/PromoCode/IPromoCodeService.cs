﻿using Dispatching.Ordering.API.ServicesViewModels;
using Dispatching.PromoCodeModule.Models.Entities;
using DispatchProduct.HttpClient;
using DispatchProduct.Ordering.API.ServicesCommunication.Settings;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Utilites.ProcessingResult;
using Utilities.Utilites.PACI;
namespace DispatchProduct.Ordering.API.ServicesCommunication.PromoCode
{
    public interface IPromoCodeService : IDefaultHttpClientCrud<PromoCodeServiceSetting, PromoCodeValidation, PromoResult>
    {
        Task<ProcessResultViewModel<PromoResult>> SubmitPromoCode(PromoCodeValidation model);
    }
}
