﻿using Dispatching.Ordering.API.ServicesViewModels;
using DispatchProduct.HttpClient;
using DispatchProduct.Ordering.API.ServicesCommunication.Settings;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Utilites.ProcessingResult;
using Utilities.Utilites.PACI;
namespace DispatchProduct.Ordering.API.ServicesCommunication.Location
{
    public interface ILocationService : IDefaultHttpClientCrud<LocationServiceSetting, Point, Point>
    {
        Task<ProcessResultViewModel<Point>> GetStreetCoords(string version, string streetName, string blockName);
    }
}
