﻿using Dispatching.Ordering.API.ServicesViewModels;
using DispatchProduct.HttpClient;
using DispatchProduct.Ordering.API.ServicesCommunication.Location;
using DispatchProduct.Ordering.API.ServicesCommunication.Settings;
using DnsClient;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Utilites.ProcessingResult;
using Utilities.Utilites.PACI;
namespace Dispatching.Ordering.API.ServicesCommunication.Location
{
    public class LocationService : DefaultHttpClientCrud<LocationServiceSetting, Point, Point>, ILocationService
    {
        LocationServiceSetting settings;
        IDnsQuery dnsQuery;

        public LocationService(IOptions<LocationServiceSetting> _settings, IDnsQuery _dnsQuery) :base(_settings.Value, _dnsQuery)
        {
            settings = _settings.Value;
            dnsQuery = _dnsQuery;
        }
        public async Task<ProcessResultViewModel<Point>> GetStreetCoords(string version, string streetName, string blockName)
        {
            string baseUrl = await settings.GetUri(dnsQuery);
            while (baseUrl.EndsWith('/'))
            {
                baseUrl = baseUrl.Substring(0, baseUrl.Length - 1);
            }
            if (!String.IsNullOrEmpty(baseUrl))
            {
                var requestedAction = settings.GetStreetCoorsAction;
                var url = $"{baseUrl}/{version}/{requestedAction}?streetName={streetName}&blockName={blockName}";
                return await GetByUri(url);
            }
            return null;
        }

    }
}
