﻿using Dispatching.Ordering.API.ServicesViewModels;
using Dispatching.Ordering.API.ServicesViewModels.VAT;
using DispatchProduct.HttpClient;
using DispatchProduct.Ordering.API.ServicesCommunication.VAT;
using DispatchProduct.Ordering.API.ServicesCommunication.Settings;
using DnsClient;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Utilites.ProcessingResult;
using Dispatching.Settings.ViewModel;

namespace Dispatching.Ordering.API.ServicesCommunication.VAT
{
    public class SettingService : DefaultHttpClientCrud<SettingServiceConfig, string, List<SettingViewModel>>, ISettingService
    {
        SettingServiceConfig settings;
        IDnsQuery dnsQuery;
        public SettingService(IOptions<SettingServiceConfig> _settings, IDnsQuery _dnsQuery) : base(_settings.Value, _dnsQuery)
        {
            settings = _settings.Value;
            dnsQuery = _dnsQuery;
        }
        public async Task<ProcessResultViewModel<string>> GetSettings(string key)
        {
            ProcessResultViewModel<string> result = null;
            var requesturi = $"{await settings.GetUri(dnsQuery)}/{settings.GetSettingsVerb}/{key}";
            string baseUrl = await settings.GetUri(dnsQuery);
            while (baseUrl.EndsWith('/'))
            {
                baseUrl = baseUrl.Substring(0, baseUrl.Length - 1);
            }
            var settingRes = await GetByUri(requesturi);
            if (settingRes.IsSucceeded && settingRes.Data != null)
            {
                result = ProcessResultViewModelHelper.Succedded(settingRes.Data.FirstOrDefault().Value);
            }
            else
            {
                result = ProcessResultViewModelHelper.MapToProcessResultViewModel<List<SettingViewModel>,string>(settingRes);
            }
            return result;
        }

    }
}
