﻿using Dispatching.Ordering.API.ServicesViewModels;
using Dispatching.Ordering.API.ServicesViewModels.VAT;
using Dispatching.Settings.ViewModel;
using DispatchProduct.HttpClient;
using DispatchProduct.Ordering.API.ServicesCommunication.Settings;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Utilites.ProcessingResult;

namespace DispatchProduct.Ordering.API.ServicesCommunication.VAT
{
    public interface ISettingService : IDefaultHttpClientCrud<SettingServiceConfig, string, List<SettingViewModel>>
    {
        Task<ProcessResultViewModel<string>> GetSettings(string key);
    }
}
