﻿using DispatchProduct.Controllers;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Utilites.ProcessingResult;
using Dispatching.Ordering.Models.Entities;
using Dispatching.Ordering.BLL.IManagers;
using Dispatching.Ordering.API.ViewModels;
namespace Dispatching.Ordering.API.Controllers
{
    [ApiVersion("1.0")]
    [Route("v{version:apiVersion}/[controller]")]
    public class DonationDatesController
    : BaseController<IDonationDatesManager, DonationDates, BaseDatesViewModel>
    {
        private readonly IDonationDatesManager _DonationDatesManager;
        private readonly IMapper _mapper;
        private readonly IProcessResultMapper _processResultMapper;
        private readonly IProcessResultPaginatedMapper _processResultPaginatedMapper;

        public DonationDatesController(IDonationDatesManager _manger, IMapper _mapper,
            IProcessResultMapper _processResultMapper, IProcessResultPaginatedMapper _processResultPaginatedMapper)
            : base(_manger, _mapper, _processResultMapper, _processResultPaginatedMapper)
        {
            this._DonationDatesManager = _manger;
            this._mapper = _mapper;
            this._processResultMapper = _processResultMapper;
            this._processResultPaginatedMapper = _processResultPaginatedMapper;
        }

    }
}
