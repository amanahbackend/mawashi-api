﻿using DispatchProduct.Controllers;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Utilites.ProcessingResult;
using Dispatching.Ordering.Models.Entities;
using Dispatching.Ordering.BLL.IManagers;
using Dispatching.Ordering.API.ViewModels;
using Microsoft.Extensions.DependencyInjection;
using Dispatching.Ordering.API.ServicesViewModels.DeliveryOption;
using CommonEnums;
using Dispatching.Ordering.API.ServicesViewModels;
using Dispatching.Ordering.API.ServiceCommunications;
using Dispatching.Ordering.API.ServicesCommunication.Location;
using DispatchProduct.BuildingBlocks.BaseEntities.Entities;
using Utilities;
using Microsoft.Extensions.Options;
using Dispatching.Ordering.Models.Settings;

namespace Dispatching.Ordering.API.Controllers
{
    [ApiVersion("1.0")]
    [Route("v{version:apiVersion}/[controller]")]
    public class CategoryLocationController
    : BaseController<IPickUpDatesManager, PickUpDates, PickUpDates>
    {
        private readonly IPickUpDatesManager _PickUpDatesManager;
        private readonly IMapper _mapper;
        private readonly IProcessResultMapper _processResultMapper;
        private readonly IProcessResultPaginatedMapper _processResultPaginatedMapper;
        IServiceProvider serviceProvider;
        OrderAppSettings orderSettings;
        public CategoryLocationController(IOptions<OrderAppSettings> _orderSettings,IServiceProvider _serviceProvider,IPickUpDatesManager _manger, IMapper _mapper,
            IProcessResultMapper _processResultMapper, IProcessResultPaginatedMapper _processResultPaginatedMapper)
            : base(_manger, _mapper, _processResultMapper, _processResultPaginatedMapper)
        {
            this._PickUpDatesManager = _manger;
            this._mapper = _mapper;
            this._processResultMapper = _processResultMapper;
            this._processResultPaginatedMapper = _processResultPaginatedMapper;
            serviceProvider = _serviceProvider;
            orderSettings = _orderSettings.Value;
        }

        private DonationController DonationController
        {
            get { return serviceProvider.GetService<DonationController>(); }
        }
        private PickUpController PickUpController
        {
            get { return serviceProvider.GetService<PickUpController>(); }
        }
        private DonationDatesController DonationDatesController
        {
            get { return serviceProvider.GetService<DonationDatesController>(); }
        }
        private PickUpDatesController PickUpDatesController
        {
            get { return serviceProvider.GetService<PickUpDatesController>(); }
        }
        private EidDatesController EidDatesController
        {
            get { return serviceProvider.GetService<EidDatesController>(); }
        }
        private ICustomerLocationService CustomerLocation
        {
            get { return serviceProvider.GetService<ICustomerLocationService>(); }
        }
        [HttpPost, Route("GetLocationsDates"), MapToApiVersion("1.0")]
        public async Task<ProcessResultViewModel<LocationDatesConfig>> GetCategorylocationsDates([FromBody]LocationTypeViewModel type)
        {
            ProcessResultViewModel<LocationDatesConfig> result = null;
            if (type != null && type.FK_Config_Id > 0)
            {
                var locResult = await GetCategoryLocations(type);
                var datResult = await GetCategoryDates(type);
                if (locResult.IsSucceeded)
                {
                    LocationDatesConfig.Locations = locResult.Data;
                    result = ProcessResultViewModelHelper.Succedded(LocationDatesConfig);
                }
                else
                {
                    result = ProcessResultViewModelHelper.MapToProcessResultViewModel<List<LocationViewModel>, LocationDatesConfig>(locResult);
                }
                if (datResult.IsSucceeded)
                {
                    LocationDatesConfig.Dates = datResult.Data;
                    result = ProcessResultViewModelHelper.Succedded(LocationDatesConfig);
                }
                else
                {
                    result = ProcessResultViewModelHelper.MapToProcessResultViewModel<List<BaseDatesViewModel>, LocationDatesConfig>(datResult);
                }
            }
            else
            {
                result = ProcessResultViewModelHelper.Failed<LocationDatesConfig>(null, "missing arguments");
            }
            return result;
        }
        public async Task<ProcessResultViewModel<List<LocationViewModel>>> GetCategoryLocations(LocationTypeViewModel type)
        {
            ProcessResultViewModel<List<LocationViewModel>> result = null;
            LocationViewModel config = new LocationViewModel();
            if (type != null)
            {
                if (type.DeliveryType == DeliveryType.Donation)
                {
                    var donResult = DonationController.Get();
                    if (donResult.IsSucceeded && donResult.Data != null)
                    {
                        var locations = mapper.Map<List<DonationViewModel>, List<LocationViewModel>>(donResult.Data);
                        for (int i = 0; i < locations.Count; i++)
                        {
                            locations[i].Name = MapName(donResult.Data[i]);
                        }
                       
                        result = ProcessResultViewModelHelper.Succedded(locations);
                    }
                }
                else if (type.DeliveryType == DeliveryType.PickUp)
                {
                    var pickResult = PickUpController.Get();
                    if (pickResult.IsSucceeded && pickResult.Data != null)
                    {
                        var locations = mapper.Map<List<PickUpViewModel>, List<LocationViewModel>>(pickResult.Data);
                        for (int i = 0; i < locations.Count; i++)
                        {
                            locations[i].Name = MapName(pickResult.Data[i]);
                        }
                        result = ProcessResultViewModelHelper.Succedded(locations);
                    }
                }
                else if (type.DeliveryType == DeliveryType.Home)
                {
                    var custResult = await CustomerLocation.GetByCustomerId(type.FK_Customer_Id);
                    if (custResult.IsSucceeded && custResult.Data != null)
                    {
                        result = ProcessResultViewModelHelper.Succedded(custResult.Data);
                    }
                }
            }
            else
            {
                result = ProcessResultViewModelHelper.Failed<List<LocationViewModel>>(null, "missing arguments");
            }
            return result;
        }
        public async Task<ProcessResultViewModel<List<BaseDatesViewModel>>> GetCategoryDates(LocationTypeViewModel type)
        {
            ProcessResultViewModel<List<BaseDatesViewModel>> result = null;
            var config = _mapper.Map<LocationTypeViewModel, CategoryInputDeliveryOptions>(type);
            var dateTypeResult = await DeliveryOptionService.GetDateType(config);
            if (dateTypeResult.IsSucceeded)
            {
                if (dateTypeResult.Data == (int)DateTypes.DonationDates)
                {
                    result = DonationDatesController.Get();
                }
                else if (dateTypeResult.Data == (int)DateTypes.EidDates)
                {
                    result = EidDatesController.Get();
                }
                else if (dateTypeResult.Data == (int)DateTypes.PickUpDates)
                {
                    result = PickUpDatesController.Get();
                }
                else if (dateTypeResult.Data == (int)DateTypes.CustomerDate)
                {
                    result = ProcessResultViewModelHelper.Succedded<List<BaseDatesViewModel>>(null, "Fill Customer Date");
                }
                else
                {
                    result = ProcessResultViewModelHelper.Failed<List<BaseDatesViewModel>>(null, $"out of range argument {dateTypeResult}");
                }
            }
            else
            {
                result = ProcessResultViewModelHelper.MapToProcessResultViewModel<int, List<BaseDatesViewModel>>(dateTypeResult);
            }
            return result;
        }
        private LocationDatesConfig LocationDatesConfig
        {
            get { return serviceProvider.GetService<LocationDatesConfig>(); }
        }
        private IDeliveryOptionService DeliveryOptionService
        {
            get { return serviceProvider.GetService<IDeliveryOptionService>(); }
        }
        private CategoryInputDeliveryOptions CategoryInputDeliveryOptions
        {
            get { return serviceProvider.GetService<CategoryInputDeliveryOptions>(); }
        }



        private string MapName(BaseLKPEntityViewModel model)
        {
            string dataResult = null;
            SupportedLanguage data = GetSupportedLanguage();
            if (data == SupportedLanguage.Arabic)
            {
                dataResult = model.NameAR;
            }
            else if (data == SupportedLanguage.English)
            {
                dataResult = model.NameEN;
            }
            else
            {
                dataResult = model.NameEN;
            }
            return dataResult;
        }
        private string MapName(BaseLKPEntity model)
        {
            string dataResult = null;
            SupportedLanguage data = GetSupportedLanguage();
            if (data == SupportedLanguage.Arabic)
            {
                dataResult = model.NameAR;
            }
            else if (data == SupportedLanguage.English)
            {
                dataResult = model.NameEN;
            }
            else
            {
                dataResult = model.NameEN;
            }
            return dataResult;
        }
        private SupportedLanguage GetSupportedLanguage()
        {
            SupportedLanguage data;
            var languageResult = Headers.GetSupportedLanguage(Request);
            string langSetting = orderSettings.DefaultLanguage;
            if ((!languageResult.IsSucceeded || languageResult.Data == SupportedLanguage.NotSet) && !string.IsNullOrEmpty(orderSettings.DefaultLanguage))
            {
                Enum.TryParse(orderSettings.DefaultLanguage, out data);
            }
            else
            {
                data = languageResult.Data;
            }
            return data;
        }
    }
}
