﻿using Dispatching.Ordering.BLL.IManagers;
using Dispatching.Ordering.Models.Entities;
using DispatchProduct.Controllers.V1;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Utilites.ProcessingResult;
using Microsoft.AspNetCore.Mvc;
using CommonEnums;
using Dispatching.Ordering.API.ViewModels;
using DispatchProduct.Ordering.API.ServicesCommunication.Location;
namespace Dispatching.Ordering.API.Controllers
{
    [ApiVersion("1.0")]
    [Route("v{version:apiVersion}/[controller]")]
    public class DonationController : BaseControllerV1<IDonationManager, Donation, DonationViewModel>
    {

        ILocationService _locationService;

        public DonationController(IDonationManager _manger, IMapper _mapper,
            IProcessResultMapper _processResultMapper, IProcessResultPaginatedMapper _processResultPaginatedMapper,ILocationService locationService)
            : base(_manger, _mapper, _processResultMapper, _processResultPaginatedMapper)
        {
           
            _locationService = locationService;
        }

        [HttpPost, Route("AddAddress"), MapToApiVersion("1.0")]
        public async Task<ProcessResultViewModel<DonationViewModel>> AddAddress([FromBody] DonationViewModel model)
        {
            if (model.Fk_Country_Id == Country.Kuwait &&
                String.IsNullOrEmpty(model.Number) &&
                model.Latitude == null && model.Longitude == null)
            {
                var streetPtResult = await _locationService.GetStreetCoords("v1", model.StreetName, model.BlockName);
                if (streetPtResult.IsSucceeded)
                {
                    model.Latitude = streetPtResult.Data.Latitude;
                    model.Longitude = streetPtResult.Data.Longitude;
                }
                return base.Post(model);
            }
            else
            {
                return base.Post(model);
            }
        }

        [HttpPut, Route("EditAddress"), MapToApiVersion("1.0")]
        public async Task<ProcessResultViewModel<bool>> EditAddress([FromBody] DonationViewModel model)
        {
            if (model.Fk_Country_Id == Country.Kuwait &&
                String.IsNullOrEmpty(model.Number) &&
                model.Latitude == null && model.Longitude == null)
            {
                var streetPtResult = await _locationService.GetStreetCoords("v1", model.StreetName, model.BlockName);
                if (streetPtResult.IsSucceeded)
                {
                    model.Latitude = streetPtResult.Data.Latitude;
                    model.Longitude = streetPtResult.Data.Longitude;
                }
                return base.Put(model);
            }
            else
            {
                return base.Put(model);
            }
        }
    }
}
