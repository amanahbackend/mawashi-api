﻿using DispatchProduct.Controllers;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Utilites.ProcessingResult;
using Dispatching.Ordering.Models.Entities;
using Dispatching.Ordering.BLL.IManagers;
using Dispatching.Ordering.API.ViewModels;
namespace Dispatching.Ordering.API.Controllers
{
    [ApiVersion("1.0")]
    [Route("v{version:apiVersion}/[controller]")]
    public class PickUpDatesController
    : BaseController<IPickUpDatesManager, PickUpDates, BaseDatesViewModel>
    {
        private readonly IPickUpDatesManager _PickUpDatesManager;
        private readonly IMapper _mapper;
        private readonly IProcessResultMapper _processResultMapper;
        private readonly IProcessResultPaginatedMapper _processResultPaginatedMapper;

        public PickUpDatesController(IPickUpDatesManager _manger, IMapper _mapper,
            IProcessResultMapper _processResultMapper, IProcessResultPaginatedMapper _processResultPaginatedMapper)
            : base(_manger, _mapper, _processResultMapper, _processResultPaginatedMapper)
        {
            this._PickUpDatesManager = _manger;
            this._mapper = _mapper;
            this._processResultMapper = _processResultMapper;
            this._processResultPaginatedMapper = _processResultPaginatedMapper;
        }

    }
}
