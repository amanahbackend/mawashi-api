﻿using AutoMapper;
using Dispatching.Ordering.API.ViewModel;
using Dispatching.Ordering.BLL.IManagers;
using Dispatching.Ordering.Models.Entities;
using DispatchProduct.Controllers.V1;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using Utilites.ProcessingResult;
namespace Dispatching.Ordering.API.Controllers
{
    [ApiVersion("1.0")]
    [Route("v{version:apiVersion}/[controller]")]
    public class LKP_OrderStatusController : BaseControllerV1<ILKP_OrderStatusManager, LKP_OrderStatus, LKP_OrderStatusViewModel>
    {
        IServiceProvider serviceProvider;
        ILKP_OrderStatusManager manger;
        IMapper mapper;
        IProcessResultMapper processResultMapper;
        public LKP_OrderStatusController(IServiceProvider _serviceProvider, ILKP_OrderStatusManager _manger, IMapper _mapper, IProcessResultMapper _processResultMapper, IProcessResultPaginatedMapper _processResultPaginatedMapper) : base(_manger, _mapper, _processResultMapper, _processResultPaginatedMapper)
        {
            serviceProvider = _serviceProvider;
            manger = _manger;
            mapper = _mapper;
            processResultMapper = _processResultMapper;
        }
        private ILKP_OrderStatusManager OrderStatusManager
        {
            get { return serviceProvider.GetService<ILKP_OrderStatusManager>(); }
        }
        public ProcessResultViewModel<List<OrderViewModel>> GetByOrderStatus(string statusName)
        {
            ProcessResultViewModel<List<OrderViewModel>> result = null;
            var orderStatusResult = OrderStatusManager.GetOrderStatus(statusName);
            if (orderStatusResult != null && orderStatusResult.IsSucceeded && orderStatusResult.Data != null)
            {
                var ordersResult = OrderManager.GetByOrderStatusId(orderStatusResult.Data.Id);
                result = processResultMapper.Map<List<Order>, List<OrderViewModel>>(ordersResult);
            }
            else
            {
                result = ProcessResultViewModelHelper.Failed<List<OrderViewModel>>(null, $"There isn't status with this this Name {statusName}");
            }
            return result;
        }
        private IOrderManager OrderManager
        {
            get { return serviceProvider.GetService<IOrderManager>(); }
        }
        [HttpGet, Route("GetAll"), MapToApiVersion("1.0")]
        public override ProcessResultViewModel<List<LKP_OrderStatusViewModel>> Get()
        {
            return base.Get();
        }
        [HttpGet, Route("Get/{id}"), MapToApiVersion("1.0")]
        public override ProcessResultViewModel<LKP_OrderStatusViewModel> Get([FromRoute] int id)
        {
            return base.Get(id);
        }
    }
}

