﻿using System;
using System.Collections.Generic;
using Dispatching.Ordering.Models.Entities;
using Dispatching.Ordering.BLL.IManagers;
using Microsoft.AspNetCore.Mvc;
using AutoMapper;
using Utilites.ProcessingResult;
using Dispatching.Ordering.API.ViewModel;
using DispatchProduct.Controllers.V1;
using Microsoft.Extensions.DependencyInjection;
using Dispatching.Ordering.API.ServicesViewModels;
using System.Threading.Tasks;
using DispatchProduct.Ordering.API.ServicesCommunication.Cart;
using CommonEnums;
using Microsoft.Extensions.Options;
using Microsoft.AspNetCore.Http;
using Dispatching.Ordering.Models.Settings;
using DispatchProduct.Ordering.API.ServicesCommunication.PaymentURL;
using Dispatching.Ordering.Models;
using Dispatching.Ordering.API.ViewModels;
using Dispatching.Ordering.API.ServicesCommunication.Location;
using Utilities;
using DispatchProduct.BuildingBlocks.BaseEntities.Entities;
using Dispatching.Ordering.API.ServicesViewModelsViewModel.Cart;
using Dispatching.Ordering.API.ServicesViewModels.Cart;
using Dispatching.PromoCodeModule.Models.Entities;
using DispatchProduct.Ordering.API.ServicesCommunication.PromoCode;
using DispatchProduct.Ordering.API.ServicesCommunication.VAT;
using Dispatching.Ordering.API.ServicesViewModels.VAT;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Utilities.Utilites.GenericListToExcel;
using Utilites;
using System.Linq;
using System.Linq.Expressions;
using DispatchProduct.RepositoryModule;
using Utilites.PaginatedItemsViewModel;
using Dispatching.Ordering.BLL.Managers;
using Dispatching.CustomerModule.API.ViewModels;
using Dispatching.BuisnessCommon.Enums;
using Dispatching.Ordering.API.ServiceCommunications;
using Dispatching.Ordering.API.ServicesViewModels.DropOut;
using Microsoft.CodeAnalysis.CSharp.Syntax;
using Utilites.PaginatedItems;
using Newtonsoft.Json;
using System.IO;
using System.Text;
using System.Diagnostics;
using Dispatching.Ordering.API.ServicesCommunication.DropOut.Notification;

namespace Dispatching.Ordering.API.Controllers
{
    [ApiVersion("1.0")]
    [Route("v{version:apiVersion}/[controller]")]
    public class OrderController : BaseControllerV1<IOrderManager, Order, OrderViewModel>
    {
        public new IOrderManager manger;
        public readonly new IMapper mapper;
        IProcessResultMapper processResultMapper;
        IProcessResultPaginatedMapper processResultPaginatedMapper;
        IServiceProvider serviceProvider;
        OrderAppSettings orderSettings;
        IHostingEnvironment hostingEnv;
        IConfigurationRoot configuration;
        IEmailService _emailService;
        INotificationService _notificationService;
        public OrderController(IOrderManager _manger, IMapper _mapper, IProcessResultMapper _processResultMapper,
            IProcessResultPaginatedMapper _processResultPaginatedMapper, IServiceProvider _serviceProvider,
            IOptions<OrderAppSettings> _orderSettings, IHostingEnvironment _hostingEnv,
            IConfigurationRoot _configuration, IEmailService emailService, INotificationService notificationService) : base(_manger, _mapper, _processResultMapper, _processResultPaginatedMapper)
        {
            _notificationService = notificationService;
            _emailService = emailService;
            configuration = _configuration;
            hostingEnv = _hostingEnv;
            manger = _manger;
            mapper = _mapper;
            serviceProvider = _serviceProvider;
            processResultMapper = _processResultMapper;
            processResultPaginatedMapper = _processResultPaginatedMapper;
            orderSettings = _orderSettings.Value;
        }

        private async Task<ProcessResultViewModel<OrderViewModel>> SubmitOrder([FromBody]OrderViewModel model)
        {
            OrderViewModel data = null;
            ProcessResultViewModel<OrderViewModel> result = null;
            model = FillEnumProps(model);
            if (model.Fk_Location_Id > 0 && model.FK_Cart_Id > 0)
            {
                var cartResult = await GetCartById(model.FK_Cart_Id);
                var locResult = await GetLocationById(model.Fk_Location_Id, model.FK_DeliveryType_Id, model.OrderType);
                if (!locResult.IsSucceeded || locResult.Data == null)
                {
                    result = ProcessResultViewModelHelper.MapToProcessResultViewModel<LocationViewModel, OrderViewModel>(locResult);

                }
                else if (!cartResult.IsSucceeded || cartResult.Data == null)
                {
                    result = ProcessResultViewModelHelper.MapToProcessResultViewModel<CartViewModel, OrderViewModel>(cartResult);
                }
                else
                {
                    mapper.Map(locResult.Data, model);
                    mapper.Map(cartResult.Data, model);
                    Console.WriteLine($"locRes {locResult.Data.GovernorateName}");
                    Console.WriteLine($"cartResult {cartResult.Data.Price}");
                    if (!string.IsNullOrEmpty(model.PromoCode))
                    {
                        model.HasPromoCode = true;
                        var promoResult = await SubmitPromoCode(model);
                        if (promoResult.IsSucceeded)
                        {
                            model = promoResult.Data;
                        }
                        else
                        {
                            return promoResult;
                        }
                    }
                    var entityModel = mapper.Map<OrderViewModel, Order>(model);
                    var entityresult = manger.Submit(entityModel);
                    result = processResultMapper.Map<Order, OrderViewModel>(entityresult);
                }
            }
            else
            {
                result = ProcessResultViewModelHelper.Failed(data, "Fk_Location_Id Property or FK_Cart_Id not Set");
            }
            return result;
        }
        private async Task<ProcessResultViewModel<OrderViewModel>> UpdateCartInfo(OrderViewModel model)
        {
            ProcessResultViewModel<OrderViewModel> result = null;
            var serviceData = mapper.Map<OrderViewModel, CartOrderViewModel>(model);
            var cartResult = await CartService.UpdateOrderCartInfo(serviceData);
            if (cartResult.IsSucceeded)
            {
                result = ProcessResultViewModelHelper.Succedded(model);
            }
            else
            {
                result = ProcessResultViewModelHelper.MapToProcessResultViewModel<bool, OrderViewModel>(cartResult);
            }
            return result;
        }
        private async Task<ProcessResultViewModel<OrderViewModel>> Submit([FromBody]OrderViewModel model)
        {
            ProcessResultViewModel<OrderViewModel> result = null;

            var vatRes = await BindVat(model);
            if (vatRes.IsSucceeded)
            {
                model = vatRes.Data;
                var paymentResult = BindPaymentToOrder(model);
                if (!paymentResult.IsSucceeded)
                {
                    return paymentResult;
                }
                var platformResult = GetPlatform(Request);
                if (platformResult.IsSucceeded)
                {
                    model.FK_Platform_Id = platformResult.Data;
                }
                else
                {
                    return ProcessResultViewModelHelper.MapToProcessResultViewModel<Platform, OrderViewModel>(platformResult);
                }
                result = await SubmitOrder(model);
                if (result.IsSucceeded)
                {
                    model = result.Data;
                    result = await BindPaymentURLToOrder(model);
                    if (result.IsSucceeded)
                    {
                        result = await UpdateCartInfo(result.Data);
                    }
                }
            }
            else
            {
                result = vatRes;
            }
            if (result.IsSucceeded && model.PaymenMechanism == PaymentType.KuwaitCashOnDelivery || model.PaymenMechanism == PaymentType.UAECashOnDelivery)
            {
                try
                {
                    if (model.OrderType == OrderType.Individual)
                    {
                        await SendEmail(model);
                        await SendSMS(model);
                    }
                }
                catch (Exception ex)
                {

                }
            }
            return result;
        }
        [HttpPost, Route("CheckOut")]
        public async Task<ProcessResultViewModel<OrderViewModel>> SubmitIndividual([FromBody]OrderViewModel model)
        {
            ProcessResultViewModel<OrderViewModel> result = null;
            if (model != null)
            {
                result = await BindCustomerToOrder(model);
                if (result.IsSucceeded)
                {
                    model.OrderType = OrderType.Individual;
                    model.FK_Order_Type_Id = (int)OrderType.Individual;
                    result = await Submit(model);
                }
                else
                {
                    return result;
                }
            }
            else
            {
                result = ProcessResultViewModelHelper.Failed(model, "Parameters should be filled in request body");
            }
            return result;
        }
        [HttpPost, Route("CheckOutRetail")]
        public async Task<ProcessResultViewModel<OrderViewModel>> SubmitRetail([FromBody]OrderViewModel model)
        {
            ProcessResultViewModel<OrderViewModel> result = null;
            if (model != null)
            {
                result = await BindCompanyToOrder(model);
                if (result.IsSucceeded)
                {
                    model.FK_Order_Type_Id = (int)OrderType.Retail;
                    model.OrderType = OrderType.Retail;
                    result = await Submit(model);
                    if (result.IsSucceeded)
                    {
                        if (model.PaymenMechanism == PaymentType.Wallet)
                        {
                            result = await PayFromCompanyWallet(result.Data);
                        }
                    }
                }
                else
                {
                    return result;
                }
            }

            else
            {
                result = ProcessResultViewModelHelper.Failed(model, "Parameters should be filled in request body");
            }
            return result;
        }
        private ICartService CartService
        {
            get { return serviceProvider.GetService<ICartService>(); }
        }
        private IRoleOrderStatusManager RoleOrderStatusManager
        {
            get { return serviceProvider.GetService<IRoleOrderStatusManager>(); }
        }
        private IPromoCodeService PromoCodeService
        {
            get { return serviceProvider.GetService<IPromoCodeService>(); }
        }


        private PickUpController PickUp
        {
            get { return serviceProvider.GetService<PickUpController>(); }
        }
        private DonationController Donation
        {
            get { return serviceProvider.GetService<DonationController>(); }
        }
        private IOrderWorkFlowManager WorkFlowManager
        {
            get { return serviceProvider.GetService<IOrderWorkFlowManager>(); }
        }

        private IUAEPaymentURLService UAEPaymentURLService
        {
            get { return serviceProvider.GetService<IUAEPaymentURLService>(); }
        }
        private IKWPaymentURLService KWPaymentURLService
        {
            get { return serviceProvider.GetService<IKWPaymentURLService>(); }
        }
        private IVATService VATService
        {
            get { return serviceProvider.GetService<IVATService>(); }
        }
        private ISettingService SettingService
        {
            get { return serviceProvider.GetService<ISettingService>(); }
        }
        private ICustomerLocationService CustomerLocation
        {
            get { return serviceProvider.GetService<ICustomerLocationService>(); }
        }
        private ICompanyLocationService CompanyLocation
        {
            get { return serviceProvider.GetService<ICompanyLocationService>(); }
        }
        private ICustomerService CustomerService
        {
            get { return serviceProvider.GetService<ICustomerService>(); }
        }
        private IEmailService EmailService
        {
            get { return serviceProvider.GetService<IEmailService>(); }
        }
        //private NotificationController NotificationController
        //{
        //    get { return serviceProvider.GetService<NotificationController>(); }
        //}
        //private SendNotificationViewModel SendNotificationViewModel
        //{
        //    get { return serviceProvider.GetService<SendNotificationViewModel>(); }
        //}
        private ISMSService SMSService
        {
            get { return serviceProvider.GetService<ISMSService>(); }
        }
        private ICompanyService CompanyService
        {
            get { return serviceProvider.GetService<ICompanyService>(); }
        }

        private async Task<ProcessResultViewModel<LocationViewModel>> GetLocationById(int locationId, DeliveryType delivery, OrderType orderType)
        {
            ProcessResultViewModel<LocationViewModel> result = null;
            LocationViewModel data = null;
            if (delivery == DeliveryType.Donation)
            {
                var donResult = Donation.Get(locationId);
                if (donResult.Data != null)
                {
                    data = mapper.Map<DonationViewModel, LocationViewModel>(donResult.Data);
                    data.Name = MapName(donResult.Data);
                }
                else
                {
                    result = ProcessResultViewModelHelper.Failed<LocationViewModel>(null, $"there is no Donation location by this Id {locationId}");
                }

            }
            else if (delivery == DeliveryType.PickUp)
            {
                var pickupResult = PickUp.Get(locationId);
                if (pickupResult.Data != null)
                {
                    data = mapper.Map<PickUpViewModel, LocationViewModel>(pickupResult.Data);
                    data.Name = MapName(pickupResult.Data);
                }
                else
                {
                    result = ProcessResultViewModelHelper.Failed<LocationViewModel>(null, $"there is no PickUp location by this Id {locationId}");
                }
            }
            else if (delivery == DeliveryType.Home)
            {
                ProcessResultViewModel<LocationViewModel> locProcessResult = null;
                if (orderType == OrderType.Individual)
                {
                    locProcessResult = await CustomerLocation.GetItem(locationId.ToString());
                }
                else if (orderType == OrderType.Retail)
                {
                    var compLocProcessResult = await CompanyLocation.GetItem(locationId.ToString());
                    if (compLocProcessResult.IsSucceeded && compLocProcessResult.Data != null)
                    {
                        locProcessResult = ProcessResultViewModelHelper.Succedded(mapper.Map<CompanyLocationViewModel, LocationViewModel>(compLocProcessResult.Data));
                    }
                    else
                    {
                        locProcessResult = ProcessResultViewModelHelper.MapToProcessResultViewModel<CompanyLocationViewModel, LocationViewModel>(compLocProcessResult);
                    }
                }
                if (locProcessResult.Data != null)
                {
                    data = locProcessResult.Data;
                }
                else
                {
                    result = ProcessResultViewModelHelper.Failed<LocationViewModel>(null, $"there is no location by this Id {locationId}");
                }
            }
            if (data != null)
            {
                result = ProcessResultViewModelHelper.Succedded(data);
            }
            else
            {
                result = ProcessResultViewModelHelper.Failed<LocationViewModel>(null, $"there isn't location by this Id {locationId}");
            }
            return result;
        }
        private async Task<ProcessResultViewModel<CartViewModel>> GetCartById(int cartId)
        {
            ProcessResultViewModel<CartViewModel> result = null;
            var cartProcessResult = await CartService.GetItem(cartId.ToString());
            if (cartProcessResult != null && cartProcessResult.Data != null)
            {
                result = cartProcessResult;
            }
            else
            {
                result = ProcessResultViewModelHelper.Failed<CartViewModel>(null, $"there isn't cart by this Id {cartId}");
            }
            return result;
        }



        [HttpPost, Route("UpdateOrderItems")]
        public async Task<ProcessResultViewModel<OrderViewModel>> UpdateOrderItems([FromBody]UpdateOrderItemsViewModel model)
        {
            ProcessResultViewModel<OrderViewModel> result = null;
            var orderResult = Get(model.FK_Order_Id);
            if (orderResult.IsSucceeded && orderResult.Data != null && model.Cart != null)
            {
                if (orderResult.Data.PaymenMechanism == 0 || orderResult.Data.PaymenMechanism == PaymentType.KuwaitCashOnDelivery || orderResult.Data.PaymenMechanism == PaymentType.UAECashOnDelivery)
                {
                    model.Cart.Id = orderResult.Data.FK_Cart_Id;
                    var updateResult = await UpdateOrderItemsService(model);
                    if (updateResult.IsSucceeded)
                    {
                        result = await Submit(orderResult.Data);
                    }
                    else
                    {
                        result = ProcessResultViewModelHelper.MapToProcessResultViewModel(orderResult, result);
                    }
                }
                else
                {
                    ProcessResultViewModelHelper.Failed("Sorry,You Can't Update order Items because Fees are paid");
                }
            }
            else
            {
                result = ProcessResultViewModelHelper.MapToProcessResultViewModel(orderResult, result);
            }
            return result;
        }
        [HttpPost, Route("UpdateOrderSalesItems")]
        public async Task<ProcessResultViewModel<OrderViewModel>> UpdateOrderSalesItems([FromBody]UpdateOrderSalesItemsViewModel model)
        {
            ProcessResultViewModel<OrderViewModel> result = null;
            var orderResult = Get(model.FK_Order_Id);
            if (orderResult.IsSucceeded && orderResult.Data != null && model.Cart != null)
            {
                if (orderResult.Data.PaymenMechanism == 0 || orderResult.Data.PaymenMechanism == PaymentType.KuwaitCashOnDelivery || orderResult.Data.PaymenMechanism == PaymentType.UAECashOnDelivery)
                {
                    model.Cart.Id = orderResult.Data.FK_Cart_Id;
                    var updateResult = await UpdateOrderSalesItemsService(model);
                    if (updateResult.IsSucceeded)
                    {
                        result = await Submit(orderResult.Data);
                    }
                    else
                    {
                        result = ProcessResultViewModelHelper.MapToProcessResultViewModel(orderResult, result);
                    }
                }
                else
                {
                    ProcessResultViewModelHelper.Failed("Sorry,You Can't Update order Items because Fees are paid");
                }
            }
            else
            {
                result = ProcessResultViewModelHelper.MapToProcessResultViewModel(orderResult, result);
            }
            return result;
        }
        [HttpGet, Route("GetOrderCountByCustomer/{customerId}")]
        public ProcessResultViewModel<int> GetOrderCountByCustomer([FromRoute]int customerId)
        {
            var entityResult = manger.GetOrderCountByCustomerId(customerId);
            return processResultMapper.Map<int, int>(entityResult);
        }
        [HttpGet, Route("GetCompanyOrders/{companyId}")]
        public ProcessResultViewModel<List<OrderViewModel>> GetCompanyOrders([FromRoute]int companyId)
        {
            var entityResult = manger.GetCompanyOrders(companyId);
            return processResultMapper.Map<List<Order>, List<OrderViewModel>>(entityResult);
        }
        [HttpGet, Route("GetCompaniesOrders")]
        public ProcessResultViewModel<List<OrderViewModel>> GetCompaniesOrders()
        {
            var entityResult = manger.GetCompaniesOrders();
            return processResultMapper.Map<List<Order>, List<OrderViewModel>>(entityResult);
        }
        [HttpGet, Route("GetOrdersByCustomer/{customerId}")]
        public ProcessResultViewModel<List<OrderViewModel>> GetOrdersByCustomer([FromRoute]int customerId)
        {
            var entityResult = manger.GetOrdersByCustomerId(customerId);
            return processResultMapper.Map<List<Order>, List<OrderViewModel>>(entityResult);
        }
        [HttpPost, Route("UpdateOrderPaymentInfo")]
        public async Task<ProcessResultViewModel<bool>> UpdateOrderPaymentInfo([FromBody]UpdatePaymentOrderViewModel model)
        {
            var entityResult = manger.UpdateOrderPaymentInfo(model);
            if (entityResult.IsSucceeded)
            {
                var OrderRes = Get(model.OrderId);
                try
                {
                    if (OrderRes.Data.OrderType == OrderType.Individual)
                    {
                        await SendEmail(OrderRes.Data);
                        await SendSMS(OrderRes.Data);
                    }
                }
                catch (Exception ex)
                {

                }
            }
            return processResultMapper.Map<bool, bool>(entityResult);
        }
        [HttpPost, Route("UpdateOrderLocation")]
        public async Task<ProcessResultViewModel<OrderViewModel>> UpdateOrderLocation([FromBody]UpdateOrderLocationViewModel model)
        {
            ProcessResultViewModel<OrderViewModel> result = null;
            if (model.FK_Location_Id > 0 && model.FK_Order_Id > 0)
            {
                var orderResult = Get(model.FK_Order_Id);
                if (orderResult.IsSucceeded && orderResult.Data != null)
                {
                    orderResult.Data.Fk_Location_Id = model.FK_Location_Id;
                    var locResult = await GetLocationById(model.FK_Location_Id, orderResult.Data.FK_DeliveryType_Id, orderResult.Data.OrderType);
                    if (!locResult.IsSucceeded || locResult.Data == null)
                    {
                        result = ProcessResultViewModelHelper.MapToProcessResultViewModel<LocationViewModel, OrderViewModel>(locResult);
                    }
                    else
                    {
                        var order = mapper.Map(locResult.Data, orderResult.Data);
                        var updateRes = Put(order);
                        if (updateRes.IsSucceeded && updateRes.Data)
                        {
                            result = ProcessResultViewModelHelper.Succedded(order);
                        }
                    }
                }
                else
                {
                    result = ProcessResultViewModelHelper.MapToProcessResultViewModel(orderResult, result);
                }
            }
            else
            {
                result = ProcessResultViewModelHelper.Failed<OrderViewModel>(null, "FK_Location_Id property or FK_Order_Id property not set");
            }
            return result;
        }
        private async Task<ProcessResultViewModel<UpdateOrderItemsViewModel>> UpdateOrderItemsService(UpdateOrderItemsViewModel model)
        {
            UpdateOrderItemsViewModel data = null;
            ProcessResultViewModel<UpdateOrderItemsViewModel> result = null;
            if (model.Cart != null)
            {
                data = model;
                var cartResult = await CartService.SubmitCart(model.Cart);
                if (cartResult.IsSucceeded)
                {
                    data.Cart = cartResult.Data;
                    result = ProcessResultViewModelHelper.Succedded(data);
                }
            }
            else
            {
                result = ProcessResultViewModelHelper.Failed<UpdateOrderItemsViewModel>(null, "ther are no Items To Update");
            }
            return result;
        }
        private async Task<ProcessResultViewModel<UpdateOrderItemsViewModel>> UpdateOrderSalesItemsService(UpdateOrderSalesItemsViewModel model)
        {
            UpdateOrderItemsViewModel data = null;
            ProcessResultViewModel<UpdateOrderItemsViewModel> result = null;
            if (model.Cart != null)
            {
                data = mapper.Map<UpdateOrderSalesItemsViewModel, UpdateOrderItemsViewModel>(model);
                var cartResult = await CartService.SubmitSalesCartSales(model.Cart);
                if (cartResult.IsSucceeded)
                {
                    data.Cart = cartResult.Data;
                    result = ProcessResultViewModelHelper.Succedded(data);
                }
            }
            else
            {
                result = ProcessResultViewModelHelper.Failed<UpdateOrderItemsViewModel>(null, "ther are no Items To Update");
            }
            return result;
        }
        [HttpPost, Route("ChangeOrderStatus")]
        public async Task<ProcessResultViewModel<bool>> ChangeOrderStatus([FromBody]UpdateOrderStatusViewModel model)
        {
            ProcessResultViewModel<bool> result = null;
            if (model.FK_OrderStatus_Id > 0 && model.FK_Order_Id > 0)
            {
                var orderResult = Get(model.FK_Order_Id);
                if (orderResult.IsSucceeded && orderResult.Data != null)
                {
                    orderResult.Data.FK_Order_Status_Id = model.FK_OrderStatus_Id;
                    result = Put(orderResult.Data);
                    await SendDriverNotificationWithOrder(orderResult.Data);
                    await SendNotificationToCustomer(orderResult.Data);
                }
                else
                {
                    result = ProcessResultViewModelHelper.MapToProcessResultViewModel(orderResult, result);
                }
            }
            else
            {
                result = ProcessResultViewModelHelper.Failed(false, "FK_OrderStatus_Id property or FK_Order_Id property not set");
            }
            return result;
        }

        private async Task SendNotificationToCustomer(OrderViewModel order)
        {
            order.OrderStatus = mapper.Map<LKP_OrderStatusViewModel>(OrderStatusManager.Get(order.FK_Order_Status_Id).Data);
            await BindCustomerToOrder(order);
            var data = new SendNotificationViewModel
            {
                UserId = order.Customer.Fk_AppUser_Id,
                Title = "OrderStatus",
                Body = $"Order with code: {order.Code} is now {order.OrderStatus.NameEN}.",
                Data = new
                {
                    OrderId = order.Id,
                    OrderCode = order.Code
                }
            };
            await _notificationService.Send(data);
        }

        public async Task SendDriverNotificationWithOrder(OrderViewModel order)
        {
            var statusResultIds = RoleOrderStatusManager.GetStatusByRoleId(orderSettings.RoleIds.DriverRoleId);
            if (statusResultIds.IsSucceeded && statusResultIds.Data != null)
            {
                var id = OrderStatusManager.Get(x => x.NameEN.Equals("Ready to Deliver")).Data.Id;
                var canNotify = statusResultIds.Data.Contains(order.FK_Order_Status_Id) &&
                    order.FK_Driver_Id != null &&
                    order.FK_Order_Status_Id == id;

                if (canNotify)
                {

                    var data = new SendNotificationViewModel
                    {
                        UserId = order.FK_Driver_Id,
                        Title = "New order assigned to you",
                        Body = $"Order with code: {order.Code} assigned to you.",
                        Data = new
                        {
                            OrderId = order.Id,
                            OrderCode = order.Code
                        }
                    };
                    await _notificationService.Send(data);
                }
            }
        }

        [HttpGet, Route("Deliver/{orderId}")]
        public ProcessResultViewModel<bool> Deliver([FromRoute] int orderId)
        {
            ProcessResultViewModel<bool> result = null;
            var orderResult = Get(orderId);
            var deliverRes = GetDeliveredOrderStatus();
            if (deliverRes.IsSucceeded)
            {

                if (orderResult.IsSucceeded && orderResult.Data != null)
                {
                    orderResult.Data.FK_Order_Status_Id = deliverRes.Data.Id;
                    result = Put(orderResult.Data);
                }
                else
                {
                    result = ProcessResultViewModelHelper.MapToProcessResultViewModel(orderResult, result);
                }
            }
            else
            {
                result = ProcessResultViewModelHelper.MapToProcessResultViewModel<LKP_OrderStatusViewModel, bool>(deliverRes);
            }
            return result;
        }

        [HttpGet, Route("Cancel/{orderId}")]
        public ProcessResultViewModel<bool> Cancel([FromRoute] int orderId)
        {
            ProcessResultViewModel<bool> result = null;
            var orderResult = Get(orderId);
            var cancelRes = GetCancelledOrderStatus();
            if (cancelRes.IsSucceeded)
            {

                if (orderResult.IsSucceeded && orderResult.Data != null)
                {
                    orderResult.Data.FK_Order_Status_Id = cancelRes.Data.Id;
                    result = Put(orderResult.Data);
                }
                else
                {
                    result = ProcessResultViewModelHelper.MapToProcessResultViewModel(orderResult, result);
                }
            }
            else
            {
                result = ProcessResultViewModelHelper.MapToProcessResultViewModel<LKP_OrderStatusViewModel, bool>(cancelRes);
            }
            return result;
        }


        public ProcessResultViewModel<PaginatedItemsViewModel<OrderViewModel>> GetOrdersByRoleIdPaginated([FromRoute]string roleId, PaginatedItemsViewModel<OrderViewModel> pagination = null, Expression<Func<Order, bool>> expression = null, OrderType type = 0, string driverId = null, string producerId = null)
        {
            var paginationModel = mapper.Map<PaginatedItemsViewModel<OrderViewModel>, PaginatedItems<Order>>(pagination);
            var entityResult = manger.GetOrdersByRolePaginated(roleId, paginationModel, expression, type, driverId, producerId);
            return processResultMapper.Map<PaginatedItems<Order>, PaginatedItemsViewModel<OrderViewModel>>(entityResult);
        }

        [HttpGet, Route("GetPaymentStatus/{orderId}")]
        public ProcessResultViewModel<PaymentStatusViewModel> GetPaymentStatus([FromRoute]int orderId)
        {
            var entityResult = manger.Get(orderId);
            return processResultMapper.Map<Order, PaymentStatusViewModel>(entityResult);
        }

        public IProcessResultViewModel<LKP_OrderStatusViewModel> GetNextStaus([FromRoute]int orderId)
        {
            var entityResult = manger.GetNextStaus(orderId);
            return processResultMapper.Map<LKP_OrderStatus, LKP_OrderStatusViewModel>(entityResult);
        }
        private ILKP_OrderStatusManager OrderStatusManager
        {
            get { return serviceProvider.GetService<ILKP_OrderStatusManager>(); }
        }
        private async Task<ProcessResultViewModel<string>> GetUAEPaymentURL(PaymentURLViewModel model)
        {
            return await UAEPaymentURLService.GetUAEPaymentURL(model);
        }
        private async Task<ProcessResultViewModel<string>> GetKWPaymentURL(PaymentURLViewModel model)
        {
            return await KWPaymentURLService.GetKWPaymentURL(model);
        }
        private List<OrderViewModel> FillEnumProps(List<OrderViewModel> orders)
        {
            foreach (var item in orders)
            {
                FillEnumProps(item);
            }
            return orders;
        }
        private OrderViewModel FillEnumProps(OrderViewModel order)
        {
            if (order.FK_DeliveryType_Id > 0)
            {
                order.DeliveryType = (order.FK_DeliveryType_Id).ToString();
            }
            if (order.FK_Platform_Id > 0)
            {
                order.Platform = (order.FK_Platform_Id).ToString();
            }
            return order;
        }
        private ProcessResultViewModel<LKP_OrderStatusViewModel> GetOrderStatus()
        {
            ProcessResultViewModel<LKP_OrderStatusViewModel> result = null;
            if (!string.IsNullOrEmpty(orderSettings.IntialOrderStatusEN))
            {
                var orderStatus = OrderStatusManager.GetOrderStatus(orderSettings.IntialOrderStatusEN);
                if (orderStatus.IsSucceeded && orderStatus.Data != null)
                {
                    result = processResultMapper.Map<LKP_OrderStatus, LKP_OrderStatusViewModel>(orderStatus);
                }
                else
                {
                    result = ProcessResultViewModelHelper.Failed<LKP_OrderStatusViewModel>(null, "IntialOrderStatus Value Not Added in Order Status Table");
                }

            }
            else
            {
                result = ProcessResultViewModelHelper.Failed<LKP_OrderStatusViewModel>(null, "IntialOrderStatus not configured in appSetting.json");
            }
            return result;
        }
        private ProcessResultViewModel<LKP_OrderStatusViewModel> GetDeliveredOrderStatus()
        {
            ProcessResultViewModel<LKP_OrderStatusViewModel> result = null;
            if (!string.IsNullOrEmpty(orderSettings.DeliveredOrderStatusEN))
            {
                var orderStatus = OrderStatusManager.GetOrderStatus(orderSettings.DeliveredOrderStatusEN);
                if (orderStatus.IsSucceeded && orderStatus.Data != null)
                {
                    result = processResultMapper.Map<LKP_OrderStatus, LKP_OrderStatusViewModel>(orderStatus);
                }
                else
                {
                    result = ProcessResultViewModelHelper.Failed<LKP_OrderStatusViewModel>(null, "DeliveredOrderStatusEN Value Not Added in Order Status Table");
                }

            }
            else
            {
                result = ProcessResultViewModelHelper.Failed<LKP_OrderStatusViewModel>(null, "DeliveredOrderStatusEN not configured in appSetting.json");
            }
            return result;
        }
        private ProcessResultViewModel<LKP_OrderStatusViewModel> GetCancelledOrderStatus()
        {
            ProcessResultViewModel<LKP_OrderStatusViewModel> result = null;
            if (!string.IsNullOrEmpty(orderSettings.DeliveredOrderStatusEN))
            {
                var orderStatus = OrderStatusManager.GetOrderStatus(orderSettings.DeliveredOrderStatusEN);
                if (orderStatus.IsSucceeded && orderStatus.Data != null)
                {
                    result = processResultMapper.Map<LKP_OrderStatus, LKP_OrderStatusViewModel>(orderStatus);
                }
                else
                {
                    result = ProcessResultViewModelHelper.Failed<LKP_OrderStatusViewModel>(null, "CancelledOrderStatusEN Value Not Added in Order Status Table");
                }

            }
            else
            {
                result = ProcessResultViewModelHelper.Failed<LKP_OrderStatusViewModel>(null, "CancelledOrderStatusEN not configured in appSetting.json");
            }
            return result;
        }
        private ProcessResultViewModel<OrderViewModel> BindPaymentToOrder(OrderViewModel model)
        {
            if (model.PaymentTypeCode != null)
            {
                if (model.PaymentTypeCode == EnumManager<PaymentType>.GetName(PaymentType.KNet))
                {
                    model.PaymenMechanism = PaymentType.KNet;
                }
                else if (model.PaymentTypeCode == EnumManager<PaymentType>.GetName(PaymentType.Wallet))
                {
                    model.PaymenMechanism = PaymentType.Wallet;
                }
                else if (model.PaymentTypeCode == EnumManager<PaymentType>.GetName(PaymentType.MasterCard))
                {
                    model.PaymenMechanism = PaymentType.MasterCard;
                }
                else if (model.PaymentTypeCode == EnumManager<PaymentType>.GetName(PaymentType.KuwaitCashOnDelivery))
                {
                    model.PaymenMechanism = PaymentType.KuwaitCashOnDelivery;
                    model.IsPaymentProcessed = true;
                    model.PaymentMessage = "Cash On Delivery";
                }
                else if (model.PaymentTypeCode == EnumManager<PaymentType>.GetName(PaymentType.UAECashOnDelivery))
                {
                    //model.Code = EnumManager<PaymentType>.GetName(PaymentType.UAECashOnDelivery);
                    model.PaymenMechanism = PaymentType.UAECashOnDelivery;
                    model.IsPaymentProcessed = true;
                    model.PaymentMessage = "Cash On Delivery";
                }
                else
                {
                    return ProcessResultViewModelHelper.Failed(model, $"No payment Mechanism Defined per {model.PaymentTypeCode}");
                }
            }
            else
            {
                return ProcessResultViewModelHelper.Failed(model, "No payment Mechanism Defined please set property PaymentTypeCode");
            }
            return ProcessResultViewModelHelper.Succedded(model);
        }
        private async Task<ProcessResultViewModel<OrderViewModel>> BindPaymentURLToOrder(OrderViewModel model)
        {
            ProcessResultViewModel<OrderViewModel> result = null;
            if (model.PaymenMechanism != PaymentType.UAECashOnDelivery && model.PaymenMechanism != PaymentType.KuwaitCashOnDelivery && model.PaymenMechanism != PaymentType.Wallet)
            {
                var paymentModel = mapper.Map<PaymentURLViewModel>(model);
                if (model.PaymenMechanism == PaymentType.MasterCard)
                {
                    var paymentURLResult = await GetUAEPaymentURL(paymentModel);
                    if (paymentURLResult.IsSucceeded)
                    {
                        model.PaymentURL = paymentURLResult.Data;
                        result = ProcessResultViewModelHelper.Succedded(model);
                    }
                    else
                    {
                        result = ProcessResultViewModelHelper.Failed(model, "Issue While  Get MasterCard Payment URL");
                    }

                }
                else if (model.PaymenMechanism == PaymentType.KNet)
                {
                    var paymentURLResult = await GetKWPaymentURL(paymentModel);
                    if (paymentURLResult.IsSucceeded)
                    {
                        model.PaymentURL = paymentURLResult.Data;
                        result = ProcessResultViewModelHelper.Succedded(model);
                    }
                    else
                    {
                        result = ProcessResultViewModelHelper.Failed(model, "Issue While  Get KNet Payment URL");
                    }
                }
            }
            else
            {
                result = ProcessResultViewModelHelper.Succedded(model);
            }
            return result;
        }
        private async Task<ProcessResultViewModel<OrderViewModel>> BindVat(OrderViewModel model)
        {
            var vat = await VATService.GetVAT();
            if (vat != null && vat.IsSucceeded && vat.Data != null)
            {
                model = mapper.Map(vat.Data, model);
                return ProcessResultViewModelHelper.Succedded(model);
            }
            else
            {
                return ProcessResultViewModelHelper.Failed(model, "No Configured VAT in System");
            }

        }
        private ProcessResultViewModel<Platform> GetPlatform(HttpRequest request)
        {
            ProcessResultViewModel<Platform> result = null;
            object data;
            var platformHeader = Request.Headers["Platform"].ToString();
            if (!string.IsNullOrEmpty(platformHeader))
            {
                Enum.TryParse(typeof(Platform), platformHeader, out data);
                if (data != null && data is Platform)
                {
                    result = ProcessResultViewModelHelper.Succedded((Platform)data);
                }
                else
                {
                    result = ProcessResultViewModelHelper.Failed<Platform>(0, "Platform Header value not supported");
                }
            }
            else
            {
                result = ProcessResultViewModelHelper.Failed<Platform>(0, "Platform Header Requst not Defined");
            }
            return result;
        }

        private string MapName(BaseLKPEntityViewModel model)
        {
            string dataResult = null;
            SupportedLanguage data = GetSupportedLanguage();
            if (data == SupportedLanguage.Arabic)
            {
                dataResult = model.NameAR;
            }
            else if (data == SupportedLanguage.English)
            {
                dataResult = model.NameEN;
            }
            else
            {
                dataResult = model.NameEN;
            }
            return dataResult;
        }
        private string MapName(BaseLKPEntity model)
        {
            string dataResult = null;
            SupportedLanguage data = GetSupportedLanguage();
            if (data == SupportedLanguage.Arabic)
            {
                dataResult = model.NameAR;
            }
            else if (data == SupportedLanguage.English)
            {
                dataResult = model.NameEN;
            }
            else
            {
                dataResult = model.NameEN;
            }
            return dataResult;
        }
        private SupportedLanguage GetSupportedLanguage()
        {
            SupportedLanguage data;
            var languageResult = Headers.GetSupportedLanguage(Request);
            string langSetting = orderSettings.DefaultLanguage;
            if ((!languageResult.IsSucceeded || languageResult.Data == SupportedLanguage.NotSet) && !string.IsNullOrEmpty(orderSettings.DefaultLanguage))
            {
                Enum.TryParse(orderSettings.DefaultLanguage, out data);
            }
            else
            {
                data = languageResult.Data;
            }
            return data;
        }
        private async Task<ProcessResultViewModel<OrderViewModel>> SubmitPromoCode(OrderViewModel model)
        {
            ProcessResultViewModel<OrderViewModel> result = null;
            var promo = Mapper.Map<OrderViewModel, PromoCodeValidation>(model);
            var promoResult = await PromoCodeService.SubmitPromoCode(promo);
            if (promoResult.IsSucceeded && promoResult.Data != null)
            {
                model = Mapper.Map(promoResult.Data, model);

                if (promoResult.Data.Valid)
                {
                    result = ProcessResultViewModelHelper.Succedded(model);
                }
                else
                {
                    result = ProcessResultViewModelHelper.Failed(model, "Can't Submit Order With Invalid or Expired PromoCode");
                }
            }
            else
            {
                result = ProcessResultViewModelHelper.Failed(model);
            }
            return result;
        }

        [HttpGet, Route("GetOrdersByRoleId/{roleId}")]
        public ProcessResultViewModel<List<OrderViewModel>> GetOrdersByRoleId([FromRoute]string roleId, OrderType type = 0, string driverId = null, string producerId = null)
        {
            var entityResult = manger.GetOrdersByRole(roleId, type, driverId, producerId);
            return processResultMapper.Map<List<Order>, List<OrderViewModel>>(entityResult);
        }

        [HttpGet, Route("GetOrdersRetails")]
        public ProcessResultViewModel<List<OrderViewModel>> GetOrdersRetail()
        {
            var entityRes = manger.GetAllRetailsOrder();
            return processResultMapper.Map<List<Order>, List<OrderViewModel>>(entityRes);
        }

        [HttpGet, Route("GetOrdersIndividual")]
        public ProcessResultViewModel<List<OrderViewModel>> GetOrdersIndividual()
        {
            var entityRes = manger.GetAllIndividalOrder();
            return processResultMapper.Map<List<Order>, List<OrderViewModel>>(entityRes);
        }

        [HttpGet, Route("GetAllExcludedLastStep")]
        public ProcessResultViewModel<List<OrderViewModel>> GetAllExcludedLastStep()
        {
            var entityResult = manger.GetAllExcludeLastStep();
            return processResultMapper.Map<List<Order>, List<OrderViewModel>>(entityResult);
        }

        [HttpPost, Route("GetAllExcludedLastStepPaginated")]
        public ProcessResultViewModel<PaginatedItemsViewModel<OrderViewModel>> GetAllExcludedLastStepPaginated([FromBody]PaginatedFilterViewModel model)
        {
            if (model != null)
            {
                Expression<Func<Order, bool>> predicateFilter = null;
                if (model.Filters != null)
                {
                    predicateFilter = CreateOrdersFilterPredicate(model.Filters);
                }
                var paginationModel = mapper.Map<PaginatedItemsViewModel<OrderViewModel>, PaginatedItems<Order>>(model.PageInfo);
                var entityResult = manger.GetAllExcludeLastStepPaginated(paginationModel, predicateFilter);
                return processResultMapper.Map<PaginatedItems<Order>, PaginatedItemsViewModel<OrderViewModel>>(entityResult);
            }
            else
            {
                return null;
            }
        }

        [HttpPost, Route("GetOrdersByCreatedUserNamePaginated/{userName}")]
        public ProcessResultViewModel<PaginatedItemsViewModel<OrderViewModel>> GetOrdersByCreatedUserNamePaginated([FromRoute] string userName, [FromBody]PaginatedFilterViewModel model)
        {
            if (model != null)
            {
                Expression<Func<Order, bool>> predicateFilter = null;
                if (model.Filters != null)
                {
                    predicateFilter = CreateOrdersFilterPredicate(model.Filters);
                }
                var paginationModel = mapper.Map<PaginatedItemsViewModel<OrderViewModel>, PaginatedItems<Order>>(model.PageInfo);
                var entityResult = manger.GetOrdersByCreatedUserNamePaginated(userName, paginationModel, predicateFilter);
                return processResultMapper.Map<PaginatedItems<Order>, PaginatedItemsViewModel<OrderViewModel>>(entityResult);
            }
            else
            {
                return null;
            }
        }

        [HttpGet, Route("GetCustomerServiceOrders")]
        public ProcessResultViewModel<List<OrderViewModel>> GetCustomerServiceOrders()
        {
            return GetOrdersByRoleId(orderSettings.RoleIds.CustomerServiceRoleId);
        }
        //PaginatedFilterViewModel CompleteTomorrow

        [HttpPost, Route("GetCustomerServiceOrdersPaginated")]
        public ProcessResultViewModel<PaginatedItemsViewModel<OrderViewModel>> GetCustomerServiceOrdersPaginated([FromBody]PaginatedFilterViewModel model = null)
        {
            if (model != null)
            {
                Expression<Func<Order, bool>> predicateFilter = null;
                if (model.Filters != null)
                {
                    predicateFilter = CreateOrdersFilterPredicate(model.Filters);
                }
                return GetOrdersByRoleIdPaginated(orderSettings.RoleIds.CustomerServiceRoleId, model.PageInfo, predicateFilter);
            }
            else
            {
                return GetOrdersByRoleIdPaginated(orderSettings.RoleIds.CustomerServiceRoleId);
            }
        }

        [HttpGet, Route("GetOrdersByCreatedUserId/{userId}")]
        public ProcessResultViewModel<List<OrderViewModel>> GetOrdersByCreatedUserId([FromRoute] string userId)
        {
            var entityResult = manger.GetOrdersByCreatedUserId(userId);
            return processResultMapper.Map<List<Order>, List<OrderViewModel>>(entityResult);
        }

        [HttpGet, Route("GetOrdersByCreatedUserName/{userName}")]
        public ProcessResultViewModel<List<OrderViewModel>> GetOrdersByCreatedUserName([FromRoute] string userName)
        {
            var entityResult = manger.GetOrdersByCreatedUserName(userName);
            return processResultMapper.Map<List<Order>, List<OrderViewModel>>(entityResult);
        }

        [HttpGet, Route("GetLogisticsOrders")]
        public ProcessResultViewModel<List<OrderViewModel>> GetLogisticsOrders()
        {
            return GetOrdersByRoleId(orderSettings.RoleIds.LogisticsRoleId);
        }

        [HttpPost, Route("GetLogisticsOrdersPaginated")]
        public ProcessResultViewModel<PaginatedItemsViewModel<OrderViewModel>> GetLogisticsOrdersPaginated([FromBody]PaginatedFilterViewModel model = null)
        {
            if (model != null)
            {
                Expression<Func<Order, bool>> predicateFilter = null;
                if (model.Filters != null)
                {
                    predicateFilter = CreateOrdersFilterPredicate(model.Filters);
                }
                return GetOrdersByRoleIdPaginated(orderSettings.RoleIds.LogisticsRoleId, model.PageInfo, predicateFilter);
            }
            else
            {
                return GetOrdersByRoleIdPaginated(orderSettings.RoleIds.LogisticsRoleId);
            }
        }

        [HttpGet, Route("GetDriverOrders/{userId?}")]
        public ProcessResultViewModel<List<OrderViewModel>> GetDriverOrders([FromRoute]string userId = null)
        {
            ProcessResultViewModel<List<OrderViewModel>> result = null;
            result = GetOrdersByRoleId(orderSettings.RoleIds.DriverRoleId, 0, userId, null);
            //if (result.IsSucceeded && result.Data != null)
            //{
            //    var orders = result.Data.Where(ord => ord.DriverName != null && ord.FK_Driver_Id != null).ToList();
            //    result = ProcessResultViewModelHelper.Succedded(orders);
            //}
            return result;
        }

        [HttpPost, Route("GetDriverOrdersPaginated/{userId?}")]
        public ProcessResultViewModel<PaginatedItemsViewModel<OrderViewModel>> GetDriverOrdersPaginated([FromRoute]string userId = null, [FromBody]PaginatedFilterViewModel model = null)
        {
            ProcessResultViewModel<PaginatedItemsViewModel<OrderViewModel>> result = null;
            if (model != null)
            {
                Expression<Func<Order, bool>> predicateFilter = null;
                if (model.Filters != null)
                {
                    predicateFilter = CreateOrdersFilterPredicate(model.Filters);
                }
                result = GetOrdersByRoleIdPaginated(orderSettings.RoleIds.DriverRoleId, model.PageInfo, predicateFilter, 0, userId, null);
            }
            else
            {
                result = GetOrdersByRoleIdPaginated(orderSettings.RoleIds.DriverRoleId, null, null, 0, userId, null);
            }
            //if (result.IsSucceeded && result.Data != null)
            //{
            //    var orders = result.Data.Data.Where(ord => ord.DriverName != null && ord.FK_Driver_Id != null).ToList();
            //    result.Data.Data = orders;
            //}
            return result;
        }

        [HttpGet, Route("GetProducerOrders/{userId?}")]
        public ProcessResultViewModel<List<OrderViewModel>> GetProducerOrders([FromRoute]string userId = null)
        {
            ProcessResultViewModel<List<OrderViewModel>> result = null;
            result = GetOrdersByRoleId(orderSettings.RoleIds.ProducerRoleId, 0, null, userId);
            if (result.IsSucceeded && result.Data != null)
            {
                var orders = result.Data.Where(ord => ord.ProducerName != null && ord.FK_Producer_Id != null).ToList();
                result = ProcessResultViewModelHelper.Succedded(orders);
            }
            return result;
        }

        [HttpPost, Route("GetProducerOrdersPaginated/{userId?}")]
        public ProcessResultViewModel<PaginatedItemsViewModel<OrderViewModel>> GetProducerOrdersPaginated([FromRoute]string userId = null, [FromBody]PaginatedFilterViewModel model = null)
        {
            ProcessResultViewModel<PaginatedItemsViewModel<OrderViewModel>> result = null;
            if (model != null)
            {

                Expression<Func<Order, bool>> predicateFilter = null;
                if (model.Filters != null)
                {
                    predicateFilter = CreateOrdersFilterPredicate(model.Filters);
                }
                result = GetOrdersByRoleIdPaginated(orderSettings.RoleIds.ProducerRoleId, model.PageInfo, predicateFilter, 0, null, userId);
            }
            else
            {
                result = GetOrdersByRoleIdPaginated(orderSettings.RoleIds.ProducerRoleId, null, null, 0, null, userId);
            }
            if (result.IsSucceeded && result.Data != null)
            {
                var orders = result.Data.Data.Where(ord => ord.ProducerName != null && ord.FK_Producer_Id != null).ToList();
                result.Data.Data = orders;
            }
            return result;
        }

        [HttpGet, Route("GetCustomerServiceOrders/App")]
        public ProcessResultViewModel<List<OrderViewModel>> GetCustomerServiceOrdersApp()
        {
            return GetOrdersByRoleId(orderSettings.RoleIds.CustomerServiceRoleId, OrderType.Individual);
        }

        [HttpPost, Route("GetCustomerServiceOrdersPaginated/App")]
        public ProcessResultViewModel<PaginatedItemsViewModel<OrderViewModel>> GetCustomerServiceOrdersPaginatedApp([FromBody]PaginatedFilterViewModel model = null)
        {
            if (model != null)
            {
                Expression<Func<Order, bool>> predicateFilter = null;
                if (model.Filters != null)
                {
                    predicateFilter = CreateOrdersFilterPredicate(model.Filters);
                }
                return GetOrdersByRoleIdPaginated(orderSettings.RoleIds.CustomerServiceRoleId, model.PageInfo, predicateFilter, OrderType.Individual);
            }
            else
            {
                return GetOrdersByRoleIdPaginated(orderSettings.RoleIds.CustomerServiceRoleId, null, null, OrderType.Individual);
            }
        }

        [HttpGet, Route("GetLogisticsOrders/App")]
        public ProcessResultViewModel<List<OrderViewModel>> GetLogisticsOrdersApp()
        {
            return GetOrdersByRoleId(orderSettings.RoleIds.LogisticsRoleId, OrderType.Individual);
        }

        [HttpPost, Route("GetLogisticsOrdersPaginated/App")]
        public ProcessResultViewModel<PaginatedItemsViewModel<OrderViewModel>> GetLogisticsOrdersPaginatedApp([FromBody]PaginatedFilterViewModel model = null)
        {
            if (model != null)
            {
                Expression<Func<Order, bool>> predicateFilter = null;
                if (model.Filters != null)
                {
                    predicateFilter = CreateOrdersFilterPredicate(model.Filters);
                }
                return GetOrdersByRoleIdPaginated(orderSettings.RoleIds.LogisticsRoleId, model.PageInfo, predicateFilter, OrderType.Individual);
            }
            else
            {
                return GetOrdersByRoleIdPaginated(orderSettings.RoleIds.LogisticsRoleId, null, null, OrderType.Individual);
            }
        }

        [HttpGet, Route("GetDriverOrders/App/{userId?}")]
        public ProcessResultViewModel<List<OrderViewModel>> GetDriverOrdersApp([FromRoute]string userId = null)
        {
            ProcessResultViewModel<List<OrderViewModel>> result = null;
            result = GetOrdersByRoleId(orderSettings.RoleIds.DriverRoleId, OrderType.Individual, userId, null);
            if (result.IsSucceeded && result.Data != null)
            {
                var orders = result.Data.Where(ord => ord.DriverName != null && ord.FK_Driver_Id != null).ToList();
                result = ProcessResultViewModelHelper.Succedded(orders);
            }
            return result;
        }

        [HttpPost, Route("GetDriverOrdersPaginated/App/{userId?}")]
        public ProcessResultViewModel<PaginatedItemsViewModel<OrderViewModel>> GetDriverOrdersPaginatedApp([FromRoute]string userId = null, [FromBody]PaginatedFilterViewModel model = null)
        {
            ProcessResultViewModel<PaginatedItemsViewModel<OrderViewModel>> result = null;
            if (model != null)
            {
                Expression<Func<Order, bool>> predicateFilter = null;
                if (model.Filters != null)
                {
                    predicateFilter = CreateOrdersFilterPredicate(model.Filters);
                }
                result = GetOrdersByRoleIdPaginated(orderSettings.RoleIds.DriverRoleId, model.PageInfo, predicateFilter, OrderType.Individual, userId, null);
            }
            else
            {
                result = GetOrdersByRoleIdPaginated(orderSettings.RoleIds.DriverRoleId, null, null, OrderType.Individual, userId, null);
            }
            //if (result.IsSucceeded && result.Data != null)
            //{
            //    var orders = result.Data.Data.Where(ord => ord.DriverName != null && ord.FK_Driver_Id != null).ToList();
            //    result.Data.Data = orders;
            //}
            return result;
        }

        [HttpGet, Route("GetProducerOrders/App/{userId?}")]
        public ProcessResultViewModel<List<OrderViewModel>> GetProducerOrdersApp([FromRoute]string userId = null)
        {
            ProcessResultViewModel<List<OrderViewModel>> result = null;
            result = GetOrdersByRoleId(orderSettings.RoleIds.ProducerRoleId, OrderType.Individual, null, userId);
            if (result.IsSucceeded && result.Data != null)
            {
                var orders = result.Data.Where(ord => ord.ProducerName != null && ord.FK_Producer_Id != null).ToList();
                result = ProcessResultViewModelHelper.Succedded(orders);
            }
            return result;
        }

        [HttpPost, Route("GetProducerOrdersPaginated/App/{userId?}")]
        public ProcessResultViewModel<PaginatedItemsViewModel<OrderViewModel>> GetProducerOrdersPaginatedApp([FromRoute]string userId = null, [FromBody]PaginatedFilterViewModel model = null)
        {
            ProcessResultViewModel<PaginatedItemsViewModel<OrderViewModel>> result = null;
            if (model != null)
            {
                Expression<Func<Order, bool>> predicateFilter = null;
                if (model.Filters != null)
                {
                    predicateFilter = CreateOrdersFilterPredicate(model.Filters);
                }
                result = GetOrdersByRoleIdPaginated(orderSettings.RoleIds.ProducerRoleId, model.PageInfo, predicateFilter, OrderType.Individual, null, userId);
            }
            else
            {
                result = GetOrdersByRoleIdPaginated(orderSettings.RoleIds.ProducerRoleId, null, null, OrderType.Individual, null, userId);
            }
            if (result.IsSucceeded && result.Data != null)
            {
                var orders = result.Data.Data.Where(ord => ord.ProducerName != null && ord.FK_Producer_Id != null).ToList();
                result.Data.Data = orders;
            }
            return result;
        }

        [HttpGet, Route("GetCustomerServiceOrders/Retail")]
        public ProcessResultViewModel<List<OrderViewModel>> GetCustomerServiceOrdersRetail()
        {
            return GetOrdersByRoleId(orderSettings.RoleIds.CustomerServiceRoleId, OrderType.Retail);
        }

        [HttpPost, Route("GetCustomerServiceOrdersPaginated/Retail")]
        public ProcessResultViewModel<PaginatedItemsViewModel<OrderViewModel>> GetCustomerServiceOrdersPaginatedRetail([FromBody]PaginatedFilterViewModel model = null)
        {
            if (model != null)
            {
                Expression<Func<Order, bool>> predicateFilter = null;
                if (model.Filters != null)
                {
                    predicateFilter = CreateOrdersFilterPredicate(model.Filters);
                }
                return GetOrdersByRoleIdPaginated(orderSettings.RoleIds.CustomerServiceRoleId, model.PageInfo, predicateFilter, OrderType.Retail);
            }
            else
            {
                return GetOrdersByRoleIdPaginated(orderSettings.RoleIds.CustomerServiceRoleId, null, null, OrderType.Retail);
            }
        }

        [HttpGet, Route("GetLogisticsOrders/Retail")]
        public ProcessResultViewModel<List<OrderViewModel>> GetLogisticsOrdersRetail()
        {
            return GetOrdersByRoleId(orderSettings.RoleIds.LogisticsRoleId, OrderType.Retail);
        }

        [HttpPost, Route("GetLogisticsOrdersPaginated/Retail")]
        public ProcessResultViewModel<PaginatedItemsViewModel<OrderViewModel>> GetLogisticsOrdersPaginatedRetail([FromBody]PaginatedFilterViewModel model = null)
        {
            if (model != null)
            {
                Expression<Func<Order, bool>> predicateFilter = null;
                if (model.Filters != null)
                {
                    predicateFilter = CreateOrdersFilterPredicate(model.Filters);
                }
                return GetOrdersByRoleIdPaginated(orderSettings.RoleIds.LogisticsRoleId, model.PageInfo, predicateFilter, OrderType.Retail);
            }
            else
            {
                return GetOrdersByRoleIdPaginated(orderSettings.RoleIds.LogisticsRoleId, model.PageInfo, null, OrderType.Retail);
            }
        }

        [HttpGet, Route("GetDriverOrders/Retail/{userId?}")]
        public ProcessResultViewModel<List<OrderViewModel>> GetDriverOrdersRetail([FromRoute]string userId = null)
        {
            ProcessResultViewModel<List<OrderViewModel>> result = null;
            result = GetOrdersByRoleId(orderSettings.RoleIds.DriverRoleId, OrderType.Retail, userId, null);
            if (result.IsSucceeded && result.Data != null)
            {
                var orders = result.Data.Where(ord => ord.DriverName != null && ord.FK_Driver_Id != null).ToList();
                result = ProcessResultViewModelHelper.Succedded(orders);
            }
            return result;
        }

        [HttpPost, Route("GetDriverOrdersPaginated/Retail/{userId?}")]
        public ProcessResultViewModel<PaginatedItemsViewModel<OrderViewModel>> GetDriverOrdersPaginatedRetail([FromRoute]string userId = null, [FromBody]PaginatedFilterViewModel model = null)
        {
            ProcessResultViewModel<PaginatedItemsViewModel<OrderViewModel>> result = null;
            if (model != null)
            {
                Expression<Func<Order, bool>> predicateFilter = null;
                if (model.Filters != null)
                {
                    predicateFilter = CreateOrdersFilterPredicate(model.Filters);
                }
                result = GetOrdersByRoleIdPaginated(orderSettings.RoleIds.DriverRoleId, model.PageInfo, predicateFilter, OrderType.Retail, userId, null);
            }
            else
            {
                result = GetOrdersByRoleIdPaginated(orderSettings.RoleIds.DriverRoleId, null, null, OrderType.Retail, userId, null);
            }
            //if (result.IsSucceeded && result.Data != null)
            //{
            //    var orders = result.Data.Data.Where(ord => ord.DriverName != null && ord.FK_Driver_Id != null).ToList();
            //    result.Data.Data = orders;
            //}
            return result;
        }

        [HttpGet, Route("GetProducerOrders/Retail/{userId?}")]
        public ProcessResultViewModel<List<OrderViewModel>> GetProducerOrdersRetail([FromRoute]string userId = null)
        {
            ProcessResultViewModel<List<OrderViewModel>> result = null;
            result = GetOrdersByRoleId(orderSettings.RoleIds.ProducerRoleId, OrderType.Retail, null, userId);
            if (result.IsSucceeded && result.Data != null)
            {
                var orders = result.Data.Where(ord => ord.ProducerName != null && ord.FK_Producer_Id != null).ToList();
                result = ProcessResultViewModelHelper.Succedded(orders);
            }
            return result;
        }

        [HttpPost, Route("GetProducerOrdersPaginated/Retail/{userId?}")]
        public ProcessResultViewModel<PaginatedItemsViewModel<OrderViewModel>> GetProducerOrdersPaginatedRetail([FromRoute]string userId = null, [FromBody]PaginatedFilterViewModel model = null)
        {
            ProcessResultViewModel<PaginatedItemsViewModel<OrderViewModel>> result = null;
            if (model != null)
            {
                Expression<Func<Order, bool>> predicateFilter = null;
                if (model.Filters != null)
                {
                    predicateFilter = CreateOrdersFilterPredicate(model.Filters);
                }
                result = GetOrdersByRoleIdPaginated(orderSettings.RoleIds.ProducerRoleId, model.PageInfo, predicateFilter, OrderType.Retail, null, userId);
            }
            else
            {
                result = GetOrdersByRoleIdPaginated(orderSettings.RoleIds.ProducerRoleId, null, null, OrderType.Retail, null, userId);
            }
            if (result.IsSucceeded && result.Data != null)
            {
                var orders = result.Data.Data.Where(ord => ord.ProducerName != null && ord.FK_Producer_Id != null).ToList();
                result.Data.Data = orders;
            }
            return result;
        }

        [HttpGet, Route("GetOrdersCountByDays/{daysNumber}")]
        public ProcessResultViewModel<List<OrdersCountPerDayViewModel>> GetOrdersCountByDays([FromRoute] int daysNumber = 7)
        {
            ProcessResultViewModel<List<OrdersCountPerDayViewModel>> result = null;
            try
            {
                var ordersCount = manger.GetOrdersCountPerDay(daysNumber).Data.ToList();
                var model = mapper.Map<List<OrdersCountPerDayViewModel>>(ordersCount);
                result = ProcessResultViewModelHelper.Succedded(model);
            }
            catch (Exception ex)
            {
                result = ProcessResultViewModelHelper.Failed<List<OrdersCountPerDayViewModel>>(null, ex.Message);
            }
            return result;
        }

        [HttpGet, Route("GetPaymentPerDay/{daysNumber}")]
        public ProcessResultViewModel<List<PaymentPerDayViewModel>> GetPaymentPerDay([FromRoute] int daysNumber = 7)
        {
            ProcessResultViewModel<List<PaymentPerDayViewModel>> result = null;
            try
            {
                var ordersCount = manger.GetPaymentPerDay(daysNumber).Data.ToList();
                var model = mapper.Map<List<PaymentPerDayViewModel>>(ordersCount);
                result = ProcessResultViewModelHelper.Succedded(model);
            }
            catch (Exception ex)
            {
                result = ProcessResultViewModelHelper.Failed<List<PaymentPerDayViewModel>>(null, ex.Message);
            }
            return result;
        }

        [HttpGet, Route("GetTodayOrderStats")]
        public ProcessResultViewModel<TodayOrderStats> GetTodayOrderStats()
        {
            ProcessResultViewModel<TodayOrderStats> result = null;
            try
            {
                TodayOrderStats todayOrderStats = new TodayOrderStats();
                var deliveredOrdersCount = manger.GetTodayDeliveredOrdersCount();
                if (deliveredOrdersCount.IsSucceeded)
                {
                    todayOrderStats.DeliveredOrdersCount = deliveredOrdersCount.Data;
                }
                var ordersCount = manger.GetTodayOrdersCount();
                if (ordersCount.IsSucceeded)
                {
                    todayOrderStats.OrdersCount = ordersCount.Data;
                }
                var todayPayment = manger.GetTodayPayment();
                if (todayPayment.IsSucceeded)
                {
                    todayOrderStats.TotalPayment = Math.Round(todayPayment.Data, 3);
                }
                result = ProcessResultViewModelHelper.Succedded(todayOrderStats);
            }
            catch (Exception ex)
            {
                result = ProcessResultViewModelHelper.Failed<TodayOrderStats>(null, ex.Message);
            }

            return result;
        }

        [HttpPost, Route("FilterApp")]
        public async Task<ProcessResultViewModel<PaginatedItemsViewModel<OrderFilterViewModel>>> FilterApp([FromBody]PaginatedFilterViewModel model = null)
        {
            return await Filter(model, OrderType.Individual);
        }

        [HttpPost, Route("FilterSales")]
        public async Task<ProcessResultViewModel<PaginatedItemsViewModel<OrderFilterViewModel>>> FilterSales([FromBody]PaginatedFilterViewModel model = null)
        {
            return await Filter(model, OrderType.Retail);
        }

        [HttpPost, Route("Filter")]
        public async Task<ProcessResultViewModel<PaginatedItemsViewModel<OrderFilterViewModel>>> Filter([FromBody]PaginatedFilterViewModel model = null, OrderType orderType = 0)
        {
            ProcessResultViewModel<PaginatedItemsViewModel<OrderFilterViewModel>> orderResult = null;
            ProcessResultViewModel<PaginatedItemsViewModel<OrderViewModel>> result = null;
            try
            {
                OrderViewModel r = new OrderViewModel();
                Expression<Func<Order, bool>> predicate = null;
                PaginatedItemsViewModel<OrderViewModel> pageInfo = null;
                if (orderType > 0)
                {
                    predicate = PredicateBuilder.CreateEqualSingleExpression<Order>(nameof(r.OrderType), orderType);
                }
                if (model != null)
                {
                    pageInfo = model.PageInfo;
                    if (model.Filters != null && predicate != null)
                    {
                        var predicateFilter = CreateOrdersFilterPredicate(model.Filters);
                        predicate = PredicateBuilder.And(predicate, predicateFilter);
                    }
                    else if (model.Filters != null && predicate == null)
                    {
                        predicate = CreateOrdersFilterPredicate(model.Filters);
                    }
                }

                result = GetAllPaginatedByPredicate(pageInfo, predicate);
                if (result.IsSucceeded && result.Data != null)
                {
                    var navPropResult = await BindNavProperties(result.Data.Data);
                    if (navPropResult.IsSucceeded)
                    {
                        result.Data.Data = navPropResult.Data;
                        var orderFiltered = mapper.Map<List<OrderViewModel>, List<OrderFilterViewModel>>(result.Data.Data);
                        orderResult = processResultPaginatedMapper.MapModel(result, orderResult);
                    }
                    else
                    {
                        orderResult = ProcessResultViewModelHelper.MapToProcessResultViewModel<List<OrderViewModel>, PaginatedItemsViewModel<OrderFilterViewModel>>(navPropResult);
                    }
                }
                else
                {
                    orderResult = ProcessResultViewModelHelper.MapToProcessResultViewModel<PaginatedItemsViewModel<OrderViewModel>, PaginatedItemsViewModel<OrderFilterViewModel>>(result);
                }
            }
            catch (Exception ex)
            {
                orderResult = ProcessResultViewModelHelper.Failed<PaginatedItemsViewModel<OrderFilterViewModel>>(null, ex.Message);
            }
            return orderResult;
        }

        [HttpPost, Route("FilterToBulkPrint")]
        public async Task<ProcessResultViewModel<InvoicePrintViewModel>> FilterToBulkPrint([FromBody]List<FilterViewModel> filters)
        {
            var result = new ProcessResultViewModel<InvoicePrintViewModel>();
            try
            {
                Expression<Func<Order, bool>> predicate = CreateOrdersFilterPredicate(filters);
                var orders = manger.GetAllQuerable().Data.Where(predicate).ToList();
                var orderModels = mapper.Map<List<OrderViewModel>>(orders);
                var navPropResult = await BindNavProperties(orderModels);
                if (navPropResult.IsSucceeded)
                {
                    var filteredOrders = mapper.Map<List<OrderViewModel>, List<OrderFilterViewModel>>(navPropResult.Data);
                    var resultData = new InvoicePrintViewModel
                    {
                        TotalOrders = filteredOrders.Count(),
                        Orders = filteredOrders,
                        TotalPayment = filteredOrders.Select(x => x.Price).Sum()
                    };
                    result = ProcessResultViewModelHelper.Succedded(resultData);
                }
                else
                {
                    result = ProcessResultViewModelHelper.MapToProcessResultViewModel<List<OrderViewModel>, InvoicePrintViewModel>(navPropResult);
                }
            }
            catch (Exception ex)
            {
                result = ProcessResultViewModelHelper.Failed<InvoicePrintViewModel>(null, ex.Message);
            }
            return result;
        }

        [HttpGet, Route("GetDetailedOrder/{orderId}")]
        public async Task<ProcessResultViewModel<OrderViewModel>> GetDetailedOrder([FromRoute]int orderId)
        {
            var result = Get(orderId);
            if (result.IsSucceeded && result.Data != null)
            {
                result = await BindCartItems(result.Data);
            }
            return result;
        }

        [HttpPost, Route("FilterInvoices")]
        public async Task<ProcessResultViewModel<PaginatedItemsViewModel<OrderFilterViewModel>>> FilterInvoices([FromBody]PaginatedFilterViewModel model = null)
        {
            var result = await Filter(model);
            return result;
        }

        private async Task<ProcessResultViewModel<List<OrderViewModel>>> BindCartItems(List<OrderViewModel> model)
        {
            var cartIds = model.Select(x => x.FK_Cart_Id).ToList();
            var carts = await CartService.GetCartsWithItems(cartIds);
            foreach (var item in model)
            {
                item.Cart = carts.Data.FirstOrDefault(x => x.Id == item.FK_Cart_Id);
            }
            return ProcessResultViewModelHelper.Succedded(model);
            //for (int i = 0; i < model.Count; i++)
            //{
            //    var res = await BindCartItems(model[i]);
            //    if (res.IsSucceeded && res.Data != null)
            //    {
            //        model[i] = res.Data;
            //    }
            //    else
            //    {
            //        return ProcessResultViewModelHelper.MapToProcessResultViewModel<OrderViewModel, List<OrderViewModel>>(res);
            //    }
            //}
            //return ProcessResultViewModelHelper.Succedded(model);
        }

        private async Task<ProcessResultViewModel<OrderViewModel>> BindCartItems(OrderViewModel model)
        {
            var res = await CartService.GetCartWithItems(model.FK_Cart_Id);
            if (res.IsSucceeded)
            {
                model.Cart = res.Data;
                return ProcessResultViewModelHelper.Succedded(model);
            }
            else
            {
                return ProcessResultViewModelHelper.MapToProcessResultViewModel<CartViewModel, OrderViewModel>(res);
            }
        }

        [HttpPost, Route("FilterToExcel")]
        public async Task<ProcessResultViewModel<string>> FilterToExcel([FromBody]List<FilterViewModel> model)
        {
            ProcessResultViewModel<string> result = null;
            List<OrderExcelFilterViewModel> repeatedOrders = new List<OrderExcelFilterViewModel>();
            try
            {
                List<Order> data = null;
                OrderViewModel neworder = new OrderViewModel();
                var predicate = PredicateBuilder.CreateEqualSingleExpression<Order>(nameof(neworder.OrderType), OrderType.Individual);
                if (model != null)
                {
                    var filterPredicate = CreateOrdersFilterPredicate(model);
                    predicate = PredicateBuilder.And(predicate, filterPredicate);
                }
                data = manger.GetAllQuerable().Data.Where(predicate).ToList();
                var orderModels = mapper.Map<List<OrderViewModel>>(data);
                var navPropResult = await BindNavProperties(orderModels);
                if (navPropResult.IsSucceeded)
                {
                    orderModels = navPropResult.Data;
                    var webRootInfo = hostingEnv.ContentRootPath;
                    var dirPath = webRootInfo + @"/wwwroot/OrderReports";
                    FileUtilities.CreateIfMissing(dirPath);
                    string fileName = $"FilterReport_{ DateTime.UtcNow.ToString("MMddyyyy")}.xls";
                    var filePath = $"{dirPath}/{fileName}";
                    var filteredOrderModels = mapper.Map<List<OrderViewModel>, List<OrderFilterViewModel>>(orderModels);
                    foreach (var order in filteredOrderModels)
                    {
                        for (int i = 0; i < order.CartItems.Count; i++)
                        {
                            var excelOrder = mapper.Map<OrderFilterViewModel, OrderExcelFilterViewModel>(order);
                            var cartItm = order.CartItems[i];
                            if (cartItm != null)
                            {
                                excelOrder.CuttingName = cartItm.CuttingNameEN;
                                excelOrder.Qty = cartItm.QuantityValue;
                                excelOrder.ItemName = cartItm.ItemNameEN;
                                excelOrder.ItemPrice = cartItm.ItemPrice;
                                excelOrder.CartItemPrice = cartItm.CartItemPrice;
                                excelOrder.But = cartItm.ItemCode;
                            }
                            repeatedOrders.Add(excelOrder);
                        }
                    }
                    //ListToExcelHelper.WriteObjectsToExcel(orderModels, filePath);
                    ListToExcelHelper.WriteObjectsToExcel(repeatedOrders, filePath);
                    var fileUrl = BindFileURL(filePath, configuration);
                    result = ProcessResultViewModelHelper.Succedded(fileUrl);
                }
                else
                {
                    result = ProcessResultViewModelHelper.MapToProcessResultViewModel<List<OrderViewModel>, string>(navPropResult);
                }

            }
            catch (Exception ex)
            {
                result = ProcessResultViewModelHelper.Failed<string>(null, ex.Message);
            }
            return result;
        }

        [HttpPost, Route("FilterToSalesExcel")]
        public async Task<ProcessResultViewModel<string>> FilterToSalesExcel([FromBody]List<FilterViewModel> model)
        {
            ProcessResultViewModel<string> result = null;
            List<OrderSalesExcelFilterViewModel> repeatedOrders = new List<OrderSalesExcelFilterViewModel>();
            try
            {
                List<Order> data = null;
                OrderViewModel neworder = new OrderViewModel();
                var predicate = PredicateBuilder.CreateEqualSingleExpression<Order>(nameof(neworder.OrderType), OrderType.Retail);

                if (model != null)
                {
                    var filterPredicate = CreateOrdersFilterPredicate(model);
                    predicate = PredicateBuilder.And(predicate, filterPredicate);
                }
                data = manger.GetAllQuerable().Data.Where(predicate).ToList();
                var orderModels = mapper.Map<List<OrderViewModel>>(data);
                var navPropResult = await BindNavProperties(orderModels);
                if (navPropResult.IsSucceeded)
                {
                    orderModels = navPropResult.Data;
                    var webRootInfo = hostingEnv.ContentRootPath;
                    var dirPath = webRootInfo + @"/wwwroot/OrderReports";
                    FileUtilities.CreateIfMissing(dirPath);
                    string fileName = $"FilterReport_{ DateTime.UtcNow.ToString("MMddyyyy")}.xls";
                    var filePath = $"{dirPath}/{fileName}";
                    var filteredOrderModels = mapper.Map<List<OrderViewModel>, List<OrderFilterViewModel>>(orderModels);
                    foreach (var order in filteredOrderModels)
                    {
                        for (int i = 0; i < order.CartItems.Count; i++)
                        {
                            var excelOrder = mapper.Map<OrderFilterViewModel, OrderSalesExcelFilterViewModel>(order);
                            var cartItm = order.CartItems[i];
                            if (cartItm != null)
                            {
                                excelOrder.Qty = cartItm.QuantityValue;
                                excelOrder.IsSalesChangePrice = cartItm.IsSalesChangePrice;
                                excelOrder.SalesPricePerItem = cartItm.SalesPricePerItem;
                                excelOrder.ItemName = cartItm.ItemNameEN;
                                excelOrder.But = cartItm.ItemCode;
                                excelOrder.Measurment = cartItm.Measurment;
                                excelOrder.QuantityKG = cartItm.ItemQuantityByKG;
                                excelOrder.ItemPrice = cartItm.ItemNetPrice;
                                //excelOrder.CartItemPrice = cartItm.CartItemPrice;
                            }
                            repeatedOrders.Add(excelOrder);
                        }
                    }
                    //ListToExcelHelper.WriteObjectsToExcel(orderModels, filePath);
                    ListToExcelHelper.WriteObjectsToExcel(repeatedOrders, filePath);
                    var fileUrl = BindFileURL(filePath, configuration);
                    result = ProcessResultViewModelHelper.Succedded(fileUrl);
                }
                else
                {
                    result = ProcessResultViewModelHelper.MapToProcessResultViewModel<List<OrderViewModel>, string>(navPropResult);
                }

            }
            catch (Exception ex)
            {
                result = ProcessResultViewModelHelper.Failed<string>(null, ex.Message);
            }
            return result;
        }

        private Expression<Func<Order, bool>> CreateOrdersFilterPredicate(List<FilterViewModel> filters)
        {
            var predicate = PredicateBuilder.True<Order>();
            foreach (var filter in filters)
            {
                filter.PropertyName = filter.PropertyName.ToPascalCase();
                if (filter.PropertyName.ToLower().Contains("date"))
                {
                    predicate = predicate.And(PredicateBuilder.CreateGreaterThanOrLessThanSingleExpression<Order>(filter.PropertyName, filter.Value));
                }
                else
                {
                    predicate = predicate.And(PredicateBuilder.CreateEqualSingleExpression<Order>(filter.PropertyName, filter.Value));
                }
            }
            return predicate;
        }

        private string BindFileURL(string filePath, IConfigurationRoot configuration)
        {
            string result = "";
            var baseUrl = configuration["ApiGatewayBaseUrl"];

            var serviceName = configuration["ServiceDiscovery:ServiceName"].Replace('_', '/');

            var basePath = $"{baseUrl}/{serviceName}";

            if (filePath != null)
            {
                filePath = filePath.Replace('\\', '/');
                var segments = filePath.Split('/');
                filePath = filePath.Remove(0, segments[1].Length + 1).Remove(0, segments[2].Length + 1);
                result = $"{basePath}{filePath}";
            }
            return result;
        }

        [HttpGet, Route("MoveToNextStep/{orderId}")]
        public ProcessResultViewModel<OrderViewModel> MoveOrderToNextStatus([FromRoute]int orderId)
        {
            ProcessResultViewModel<OrderViewModel> result = null;
            result = Get(orderId);
            if (result.IsSucceeded)
            {
                var entityModel = mapper.Map<OrderViewModel, Order>(result.Data);
                var wfRes = WorkFlowManager.HandleWorkFlow(entityModel);
                result = processResultMapper.Map<Order, OrderViewModel>(wfRes);
            }
            return result;
        }

        [HttpGet, Route("PrintOrder/{id}")]
        public async Task<ProcessResultViewModel<OrderViewModel>> PrintOrder([FromRoute] int id)
        {
            var orderRes = await GetDetailedOrder(id);
            //if (orderRes.IsSucceeded && orderRes.Data != null)
            //{
            //    var customerRes = await CustomerService.GetByCustomerId(orderRes.Data.FK_Customer_Id);
            //    if (customerRes != null && customerRes.IsSucceeded && customerRes.Data != null)
            //    {
            //        orderRes.Data.Customer = customerRes.Data;
            //    }
            //    else
            //    {
            //        orderRes = ProcessResultViewModelHelper.MapToProcessResultViewModel<CustomerViewModel, OrderViewModel>(customerRes);
            //    }
            //}
            return orderRes;
        }

        [HttpPost, Route("AssignDriverToOrder")]
        public async Task<ProcessResultViewModel<OrderViewModel>> AssignDriverToOrder([FromBody]AssignDriverToOrderViewModel model)
        {
            var result = Get(model.FK_Order_Id);
            if (result.IsSucceeded && result.Data != null)
            {
                result.Data = mapper.Map(model, result.Data);
                var driverRes = Put(result.Data);
                if (!driverRes.IsSucceeded || !driverRes.Data)
                {
                    result = ProcessResultViewModelHelper.MapToProcessResultViewModel<bool, OrderViewModel>(driverRes);
                }
                else
                {
                    //result = MoveOrderToNextStatus(model.FK_Order_Id);
                    //push notification
                    await SendDriverNotificationWithOrder(result.Data);
                }
            }
            else
            {
                ProcessResultViewModelHelper.Failed($"There is no order with this {model.FK_Order_Id}");
            }
            return result;
        }



        [HttpPost, Route("AssignProducerToOrder")]
        public ProcessResultViewModel<OrderViewModel> AssignProducerToOrder([FromBody]AssignProducerToOrderViewModel model)
        {
            var result = Get(model.FK_Order_Id);
            if (result.IsSucceeded && result.Data != null)
            {
                result.Data = mapper.Map(model, result.Data);
                var producerRes = Put(result.Data);
                if (!producerRes.IsSucceeded || !producerRes.Data)
                {
                    result = ProcessResultViewModelHelper.MapToProcessResultViewModel<bool, OrderViewModel>(producerRes);
                }
                else
                {
                    result = MoveOrderToNextStatus(model.FK_Order_Id);
                }

            }
            else
            {
                ProcessResultViewModelHelper.Failed($"There is no order with this {model.FK_Order_Id}");
            }
            return result;
        }
        private async Task<ProcessResultViewModel<OrderViewModel>> BindCustomerToOrder(OrderViewModel model)
        {
            ProcessResultViewModel<OrderViewModel> result = null;
            if (model != null && model.FK_Customer_Id > 0)
            {
                var customerRes = await CustomerService.GetByCustomerId(model.FK_Customer_Id);
                if (customerRes != null && customerRes.IsSucceeded && customerRes.Data != null)
                {
                    mapper.Map(customerRes.Data, model);
                    model.Customer = customerRes.Data;
                    result = ProcessResultViewModelHelper.Succedded(model);
                }
                else
                {
                    result = ProcessResultViewModelHelper.MapToProcessResultViewModel<CustomerViewModel, OrderViewModel>(customerRes);
                }
            }
            return result;
        }

        private async Task<ProcessResultViewModel<OrderViewModel>> BindCompanyToOrder(OrderViewModel model)
        {
            ProcessResultViewModel<OrderViewModel> result = null;
            if (model != null && model.FK_Customer_Id > 0)
            {
                var customerRes = await CompanyService.GetByCompanyId(model.FK_Customer_Id);
                if (customerRes != null && customerRes.IsSucceeded && customerRes.Data != null)
                {
                    mapper.Map(customerRes.Data, model);
                    result = ProcessResultViewModelHelper.Succedded(model);
                }
                else
                {
                    result = ProcessResultViewModelHelper.MapToProcessResultViewModel<CompanyViewModel, OrderViewModel>(customerRes);
                }
            }
            return result;
        }

        private async Task<ProcessResultViewModel<bool>> SendEmail(OrderViewModel model)
        {
            ProcessResultViewModel<bool> result = null;
            var orderRes = await GetDetailedOrder(model.Id);
            if (orderRes.IsSucceeded)
            {
                var data = mapper.Map<OrderViewModel, SendMailViewModel>(model);
                data.To = new List<string>();
                data.To.Add(model.CustomerEmail);
                data.Subject = "Order Fees";
                //data.Body = $"Order Fees :{model.Price}";
                data.Body = await BindEmailBody(model);
                result = await EmailService.Send(data);
            }
            else
            {
                result = ProcessResultViewModelHelper.MapToProcessResultViewModel<OrderViewModel, bool>(orderRes);
            }
            return result;
        }

        private async Task<string> BindEmailBody(OrderViewModel model)
        {
            model = (await BindCustomerToOrder(model)).Data;
            var path = $"{hostingEnv.WebRootPath}/assets/invoice_mail.html";
            var mailTemplate = System.IO.File.ReadAllText(path);
            mailTemplate = mailTemplate.Replace("{CustomerName}", $"{ model.Customer.FirstName} {model.Customer.LastName}");
            mailTemplate = mailTemplate.Replace("{OrderNumber}", model.Code);
            mailTemplate = mailTemplate.Replace("{DeliveryDate}", model.DeliveryDate.ToString("dddd, dd MMMM yyyy"));
            mailTemplate = mailTemplate.Replace("{Total}", model.Price.ToString());

            var itemTemplatePath = $"{hostingEnv.WebRootPath}/assets/item_template.html";
            var itemTemplate = System.IO.File.ReadAllText(itemTemplatePath);
            List<string> items = new List<string>();
            model = (await BindCartItems(model)).Data;
            foreach (var item in model.Cart.CartItems)
            {
                var itemData = "";
                itemData = itemTemplate.Replace("{itemName}", item.Item.NameEN);
                itemData = itemData.Replace("{quantity}", item.Quantity.Value.ToString());
                itemData = itemData.Replace("{price}", item.Price.ToString());
                items.Add(itemData);
            }
            if (model.VATValue != 0)
            {
                var vatItemData = "";
                vatItemData = itemTemplate.Replace("{itemName}", "VAT");
                vatItemData = vatItemData.Replace("{quantity}", "-");
                vatItemData = vatItemData.Replace("{price}", model.VATValue.ToString());
                items.Add(vatItemData);
            }
            if (model.DeliveryFees != 0)
            {
                var deliveryItemData = "";
                deliveryItemData = itemTemplate.Replace("{itemName}", "DeliveryFees");
                deliveryItemData = deliveryItemData.Replace("{quantity}", "-");
                deliveryItemData = deliveryItemData.Replace("{price}", model.DeliveryFees.ToString());
                items.Add(deliveryItemData);
            }
            var itemsData = String.Join(' ', items);
            mailTemplate = mailTemplate.Replace("{items}", itemsData);

            return mailTemplate;
        }

        private async Task<ProcessResultViewModel<bool>> SendSMS(OrderViewModel model)
        {
            ProcessResultViewModel<bool> result = null;
            var data = mapper.Map<OrderViewModel, SendSmsViewModel>(model);
            //data.Message = $"Order Fees :{model.Price}";
            data.Message = await BindSMS(model);
            result = await SMSService.Send(data);
            return result;
        }
        private async Task<string> BindSMS(OrderViewModel model)
        {

            var sms = orderSettings.SMSTemplateMessage;
            sms = sms.Replace("{OrderNo}", model.Code);
            // var date = String.Format("{0:MM/dd/yyyy}", model.DeliveryDate);  // "03/09/2008"
            // var date = String.Format("{0:ddd, MMM d, yyyy}", model.DeliveryDate);  // "Sun, Mar 9, 2008"
            var date = String.Format("{0:dddd, MMMM d, yyyy}", model.DeliveryDate);  // "Sunday, March 9, 2008"
            sms = sms.Replace("{DeliveryDate}", date);
            sms = sms.Replace("{OrderNo}", model.Code);
            var PhoneNumberRes = await SettingService.GetSettings("HOTLINE");
            if (PhoneNumberRes != null && PhoneNumberRes.IsSucceeded)
            {
                sms = sms.Replace("{CSPhoneNumber}", PhoneNumberRes.Data);
            }
            else
            {
                sms = sms.Replace("{CSPhoneNumber}", orderSettings.HotLine);
            }

            return sms;
        }

        private async Task<ProcessResultViewModel<OrderViewModel>> PayFromCompanyWallet(OrderViewModel model)
        {
            ProcessResultViewModel<OrderViewModel> result = null;
            var walletInfo = mapper.Map<OrderViewModel, PayFromWallet>(model);
            var walletRes = await CompanyService.PayOrderFromWallet(walletInfo);
            if (walletRes.IsSucceeded && walletRes.Data)
            {
                //await SendSMS(model);
                //await SendEmail(model);
                var order = mapper.Map<OrderViewModel, Order>(model);
                var orderRes = manger.UpdateOrderWalletPayment(order);
                result = processResultMapper.Map<Order, OrderViewModel>(orderRes);
            }
            else
            {
                var deleteOrdRes = Delete(model.Id);
                if (deleteOrdRes.IsSucceeded)
                {
                    result = ProcessResultViewModelHelper.MapToProcessResultViewModel<bool, OrderViewModel>(deleteOrdRes);
                }
                result = ProcessResultViewModelHelper.MapToProcessResultViewModel<bool, OrderViewModel>(walletRes);
            }

            return result;
        }

        private List<OrderViewModel> BindOrderType(List<OrderViewModel> orders)
        {
            foreach (var item in orders)
            {
                item.OrderTypeName = EnumManager<OrderType>.GetName(item.OrderType);
            }
            return orders;
        }
        private List<OrderViewModel> BindOrderStatus(List<OrderViewModel> orders)
        {
            foreach (var item in orders)
            {
                var orderRes = OrderStatusManager.Get(item.FK_Order_Status_Id);
                if (orderRes.IsSucceeded && orderRes.Data != null)
                {
                    item.OrderStatus = mapper.Map<LKP_OrderStatus, LKP_OrderStatusViewModel>(orderRes.Data);
                }
            }
            return orders;
        }

        private async Task<ProcessResultViewModel<List<OrderViewModel>>> BindNavProperties(List<OrderViewModel> orders)
        {
            var result = await BindCartItems(orders);
            if (result.IsSucceeded)
            {
                result.Data = BindOrderType(orders);
                result.Data = BindOrderStatus(orders);
            }
            return result;
        }

        [HttpGet, Route("GetKnetPaymentStatus/{orderId}")]
        public ProcessResultViewModel<int?> GetKnetPaymentStatus([FromRoute]int orderId)
        {
            ProcessResultViewModel<int?> result;
            try
            {
                var order = manger.Get(orderId).Data;
                int? resultData = 0;

                if (String.IsNullOrEmpty(order.PaymentMessage))
                {
                    resultData = 0;
                }
                else if (order.PaymentMessage.Equals("CAPTURED"))
                {
                    resultData = 1;
                }
                else
                {
                    resultData = 2;
                }
                result = ProcessResultViewModelHelper.Succedded(resultData);
            }
            catch (Exception e)
            {
                result = ProcessResultViewModelHelper.Failed<int?>(null, e.Message);
            }
            return result;
        }

        [HttpPut, Route("UpdateDeliveryDate")]
        public ProcessResultViewModel<bool> UpdateDeliveryDate([FromBody] UpdateDeliveryDateViewModel model)
        {
            var order = manger.Get(o => o.Id == model.OrderId).Data;
            order.DeliveryDate = model.DeliveryDate;
            OrderViewModel orderViewModel = mapper.Map<OrderViewModel>(order);
            return Put(orderViewModel);
        }

        [HttpGet, Route("UpdateEidDates")]
        public async Task UpdateEidDates()
        {
            try
            {
                var eidOrderCodes22 = "WBM-18655,Taw-11548,Taw-75532,KTA-15909,Taw-95502,Taw-73935,KTA-92375,Bar-88241,Taw-73486,Bar-71778,KTA-74528,Taw-75036,Bar-85002,Taw-19773,Taw-05762,Taw-28024,Bar-04010,WBM-43912,WBM-10466,KTA-01690,WBM-19139,Taw-41873,Bar-32701,WBM-49520,Taw-07423,Bar-99114,Taw-70963,Taw-32030,KTA-91256,WBM-83534,Taw-98540,Taw-20696,Taw-26772,Taw-45416,KTA-28594,WBM-02021,Bar-86561,Taw-64398,Taw-65534,Taw-47386,KTA-30126,Taw-47086,Taw-13789,Bar-80945,Taw-93616,Bar-20974,WBM-86627,KTA-23912,KTA-34063,Taw-68886,KTA-75780,Bar-09174,Bar-98519,WBM-81294,KTA-46364,Bar-84144,Taw-01057,WBM-99693,Taw-21535,Taw-10573,Bar-52035,Bar-94880,Taw-40322,WBM-44001,WBM-78318,Bar-71608,JAC-51791,WBM-59711,Taw-43762,Taw-32326,JAC-54620,WBM-79020,WBM-83593,WBM-67124,WBM-52879,Taw-13260,Bar-20900,WBM-38807,Taw-34499";
                var eidOrderCodes23 = "KTA-67031,JAC-37070,Bar-78823,Taw-36487,Taw-12325,Taw-65379,Taw-52836,Bar-94925,KTA-11512,DUB-91842,Bar-22236,Taw-75601,WBM-93875,Taw-26278,WBM-56973,Bar-16682,Taw-34123,Bar-33132,Taw-53568,Taw-66996";
                var ordersList22 = eidOrderCodes22.Split(',');
                var ordersList23 = eidOrderCodes23.Split(',');
                foreach (var item in ordersList22)
                {
                    LogResult($"Updating Order: {item}");
                    var order = manger.Get(o => o.Code.Equals(item)).Data;
                    order.DeliveryDate = new DateTime(2018, 8, 21);
                    var email = order.CustomerEmail;
                    var code = order.Code;
                    var phone = order.CustomerPhone;
                    var updateResult = manger.Update(order);
                    if (updateResult.IsSucceeded)
                    {
                        LogResult($"Order: {item} updated successfully");

                        LogResult($"Order: {item} - Sending mail to: {email}");
                        var model = new SendMailViewModel
                        {
                            To = new List<string> { email },
                            Subject = "Order Delivery Confirmation",
                            Body = $"Dear Customer , your order ({code}) will be delivered on (21 Aug. 2018) , for more info please call 800888822"
                        };
                        await EmailService.Send(model);
                        LogResult($"Order: {item} - Mail sent to: {email}");

                        LogResult($"Order: {item} - Sending sms to: {phone}");
                        var smsModel = new SendSmsViewModel
                        {
                            PhoneNumber = phone,
                            Message = $"Dear Customer , your order ({code}) will be delivered on (21 Aug. 2018) , for more info please call 800888822"
                        };
                        await SMSService.Send(smsModel);
                        LogResult($"Order: {item} - Sms sent to: {phone}");
                        LogResult("=============================================================");
                    }
                }

                foreach (var item in ordersList23)
                {
                    LogResult($"Updating Order: {item}");
                    var order = manger.Get(o => o.Code.Equals(item)).Data;
                    order.DeliveryDate = new DateTime(2018, 8, 22);
                    var email = order.CustomerEmail;
                    var code = order.Code;
                    var phone = order.CustomerPhone;
                    var updateResult = manger.Update(order);
                    if (updateResult.IsSucceeded)
                    {
                        LogResult($"Order: {item} updated successfully");

                        LogResult($"Order: {item} - Sending mail to: {email}");
                        var model = new SendMailViewModel
                        {
                            To = new List<string> { email },
                            Subject = "Order Delivery Confirmation",
                            Body = $"Dear Customer , your order ({code}) will be delivered on (22 Aug. 2018) , for more info please call 800888822"
                        };
                        await EmailService.Send(model);
                        LogResult($"Order: {item} - Mail sent to: {email}");

                        LogResult($"Order: {item} - Sending sms to: {phone}");
                        var smsModel = new SendSmsViewModel
                        {
                            PhoneNumber = phone,
                            Message = $"Dear Customer , your order ({code}) will be delivered on (22 Aug. 2018) , for more info please call 800888822"
                        };
                        await SMSService.Send(smsModel);
                        LogResult($"Order: {item} - Sms sent to: {phone}");
                        LogResult("=============================================================");
                    }
                }
            }
            catch (Exception ex)
            {
                LogError("Error", ex, System.Reflection.MethodBase.GetCurrentMethod().Name);
            }
        }

        public void LogResult(string resultMessage)
        {
            string date = DateTime.Now.Day + "_" + DateTime.Now.Month + "_" + DateTime.Now.Year;
            string dir = $"{hostingEnv.WebRootPath}/Logs";
            DirectoryInfo dirInfo = new DirectoryInfo(dir);
            if (!dirInfo.Exists)
            {
                dirInfo.Create();
            }
            string path = dir + "/" + "Result_Log_" + date + ".txt";
            if (!System.IO.File.Exists(path))
            {
                // Create a file to write to.
                using (StreamWriter sw = System.IO.File.CreateText(path))
                {
                    sw.WriteLine($"Time : {DateTime.Now.ToString("dd/MM/yyyy hh:mm tt")} - {resultMessage}");
                }
            }
            else
            {
                using (StreamWriter sw = System.IO.File.AppendText(path))
                {
                    sw.WriteLine(resultMessage);
                }
            }
        }

        public void LogError(string errorMessage, Exception exception, string methodName)
        {
            string date = DateTime.Now.Day + "_" + DateTime.Now.Month + "_" + DateTime.Now.Year;
            string dir = $"{hostingEnv.WebRootPath}/Logs";

            DirectoryInfo dirInfo = new DirectoryInfo(dir);
            if (!dirInfo.Exists)
            {
                dirInfo.Create();
            }
            string path = dir + "/" + "Error_Log_" + date + ".txt";
            if (!System.IO.File.Exists(path))
            {
                // Create a file to write to.
                using (StreamWriter sw = System.IO.File.CreateText(path))
                {
                    sw.WriteLine($"Time : {DateTime.Now.ToString("dd/MM/yyyy hh:mm tt")} - {errorMessage}");
                    sw.WriteLine(FlattenException(exception, methodName));
                }
            }
            else
            {
                using (StreamWriter sw = System.IO.File.AppendText(path))
                {
                    sw.WriteLine(errorMessage);
                    sw.WriteLine(FlattenException(exception, methodName));
                }
            }
        }

        private string FlattenException(Exception exception, string Method)
        {
            string logFormat;
            logFormat = DateTime.Now.ToShortDateString().ToString() + " " + DateTime.Now.ToLongTimeString().ToString();
            var stringBuilder = new StringBuilder();
            stringBuilder.AppendLine(logFormat);
            stringBuilder.AppendLine("********************************");
            try
            {
                var st = new StackTrace(exception, true);
                var frame = st.GetFrame(0);
                stringBuilder.AppendLine("Line Number: " + frame.GetFileLineNumber());
                stringBuilder.AppendLine("File Name: " + frame.GetFileName());
                stringBuilder.AppendLine("Method Name: " + frame.GetMethod());
            }
            catch (Exception)
            {
            }
            stringBuilder.AppendLine(Method);

            while (exception != null)
            {
                stringBuilder.AppendLine("-------------------------------------");
                stringBuilder.AppendLine("\n Message=" + exception.Message);
                stringBuilder.AppendLine("\n StackTrace=" + exception.StackTrace);
                stringBuilder.AppendLine("\n Exception=" + exception.ToString());
                stringBuilder.AppendLine("----------------------------------------------------------");
                exception = exception.InnerException;
            }
            stringBuilder.AppendLine("**************END******************");
            return stringBuilder.ToString();
        }

        [HttpGet, Route("OrderDelivered/{orderId}")]
        public ProcessResultViewModel<bool> OrderDelivered(int orderId)
        {
            ProcessResultViewModel<bool> result = null;
            if (orderId > 0)
            {
                var orderResult = Get(orderId);
                if (orderResult.IsSucceeded && orderResult.Data != null)
                {

                    orderResult.Data.FK_Order_Status_Id = OrderStatusManager.Get(x => x.NameEN.ToLower().Equals("delivered")).Data.Id;
                    result = Put(orderResult.Data);
                }
                else
                {
                    result = ProcessResultViewModelHelper.MapToProcessResultViewModel(orderResult, result);
                }
            }
            else
            {
                result = ProcessResultViewModelHelper.Failed(false, "Order id property not set");
            }
            return result;
        }

        [HttpGet, Route("OrderCancelled/{orderId}")]
        public ProcessResultViewModel<bool> OrderCancelled(int orderId)
        {
            ProcessResultViewModel<bool> result = null;
            if (orderId > 0)
            {
                var orderResult = Get(orderId);
                if (orderResult.IsSucceeded && orderResult.Data != null)
                {

                    orderResult.Data.FK_Order_Status_Id = OrderStatusManager.Get(x => x.NameEN.ToLower().Equals("cancelled")).Data.Id;
                    result = Put(orderResult.Data);
                }
                else
                {
                    result = ProcessResultViewModelHelper.MapToProcessResultViewModel(orderResult, result);
                }
            }
            else
            {
                result = ProcessResultViewModelHelper.Failed(false, "Order id property not set");
            }
            return result;
        }
    }
}

