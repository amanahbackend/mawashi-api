﻿using System;
using System.Collections.Generic;
using System.Text;
using DispatchProduct.RepositoryModule;
using Dispatching.Ordering.Models.Entities;
using Dispatching.Ordering.BLL.IManagers;
using Dispatching.Ordering.Models.Context;
using Microsoft.AspNetCore.Mvc;
using DispatchProduct.Controllers;
using AutoMapper;
using Utilites.ProcessingResult;
using Dispatching.Ordering.API.ViewModel;
using DispatchProduct.Controllers.V1;
using Dispatching.Ordering.BLL.Managers;

namespace Dispatching.Ordering.API.Controllers
{
    [ApiVersion("1.0")]
    [Route("v{version:apiVersion}/[controller]")]
    public class RoleOrderStatusController : BaseControllerV1<RoleOrderStatusManager, Role_Order_Status, RoleOrderStatusViewModel>
    {
        public RoleOrderStatusController(RoleOrderStatusManager _manger, IMapper _mapper, IProcessResultMapper _processResultMapper, IProcessResultPaginatedMapper _processResultPaginatedMapper) : base(_manger, _mapper, _processResultMapper, _processResultPaginatedMapper)
        {

        }

    }
}

