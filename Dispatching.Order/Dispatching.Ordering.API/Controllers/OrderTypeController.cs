﻿using System;
using System.Collections.Generic;
using System.Text;
using DispatchProduct.RepositoryModule;
using Dispatching.Ordering.Models.Entities;
using Dispatching.Ordering.BLL.IManagers;
using Dispatching.Ordering.Models.Context;
using Microsoft.AspNetCore.Mvc;
using DispatchProduct.Controllers;
using AutoMapper;
using Utilites.ProcessingResult;
using Dispatching.Ordering.API.ViewModel;
using DispatchProduct.Controllers.V1;
using CommonEnums;
using Dispatching.BuisnessCommon.Enums;

namespace Dispatching.Ordering.API.Controllers
{
    [ApiVersion("1.0")]
    [Route("v{version:apiVersion}/[controller]")]
    public class OrderTypeController : Controller
    {
        [Route("GetAll")]
        [HttpGet]
        public List<EnumEntity> GetAll()
        {
            return EnumManager<OrderType>.GetEnumList();
        }
    }
}

