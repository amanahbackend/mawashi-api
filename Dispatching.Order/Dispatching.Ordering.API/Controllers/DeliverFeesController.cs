﻿using System;
using System.Collections.Generic;
using System.Text;
using DispatchProduct.RepositoryModule;
using Dispatching.Ordering.Models.Entities;
using Dispatching.Ordering.BLL.IManagers;
using Dispatching.Ordering.Models.Context;
using Microsoft.AspNetCore.Mvc;
using DispatchProduct.Controllers;
using AutoMapper;
using Utilites.ProcessingResult;
using Dispatching.Ordering.API.ViewModel;
using DispatchProduct.Controllers.V1;
using DispatchProduct.Ordering.API.ServicesCommunication.VAT;
using Microsoft.Extensions.DependencyInjection;
using System.Threading.Tasks;

namespace Dispatching.Ordering.API.Controllers
{
    [ApiVersion("1.0")]
    [Route("v{version:apiVersion}/[controller]")]
    public class DeliveryFeesController : BaseControllerV1<ILKP_CityFeesManager, LKP_CityFees, LKP_CityFeesViewModel>
    {
        IServiceProvider serviceProvider;
        ILKP_CityFeesManager manger;
        IMapper mapper;
        IProcessResultMapper processResultMapper;
        private IVATService VATService
        {
            get { return serviceProvider.GetService<IVATService>(); }
        }
        public DeliveryFeesController(IServiceProvider _serviceProvider, ILKP_CityFeesManager _manger, IMapper _mapper, IProcessResultMapper _processResultMapper, IProcessResultPaginatedMapper _processResultPaginatedMapper) : base(_manger, _mapper, _processResultMapper, _processResultPaginatedMapper)
        {
            serviceProvider = _serviceProvider;
            manger = _manger;
            mapper = _mapper;
            processResultMapper = _processResultMapper;
        }

        [HttpGet, Route("GetFees")]
        public ProcessResultViewModel<double> GetFees(string cityName = null)
        {
            var entityResult = manger.GetCityDelivery(cityName);
            if (entityResult.IsSucceeded)
            {
                return processResultMapper.Map<double, double>(entityResult);
            }
            else
            {
                return ProcessResultViewModelHelper.Failed<double>(0, "Failed while Get Fees");
            }
        }
        [HttpPost, Route("Submit")]
        public async Task<ProcessResultViewModel<LKP_CityFeesViewModel>> Submit([FromBody] LKP_CityFeesViewModel model)
        {
            ProcessResultViewModel<LKP_CityFeesViewModel> result = null;
            

            result = await BindVat(model);
            if (result.IsSucceeded)
            {
                model = result.Data;
                if (model.Id == 0)
                {
                    result = base.Post(model);
                }
                else
                {
                   var updateRes = base.Put(model);
                    if (updateRes.IsSucceeded)
                    {
                        result = ProcessResultViewModelHelper.Succedded(model);
                    }
                    else
                    {
                        ProcessResultViewModelHelper.MapToProcessResultViewModel<bool, LKP_CityFeesViewModel>(updateRes);
                    }

                }
            }
            return result;
        }
        private async Task<ProcessResultViewModel<LKP_CityFeesViewModel>> BindVat(LKP_CityFeesViewModel model)
        {
            var vat = await VATService.GetVAT();
            if (vat != null && vat.IsSucceeded && vat.Data != null)
            {
                model.DeliveryVAT = vat.Data.Percentage * model.DeliveryFees / 100;
                model.DeliveryFinalFees = model.DeliveryVAT + model.DeliveryFees;
                return ProcessResultViewModelHelper.Succedded(model);
            }
            else
            {
                return ProcessResultViewModelHelper.Failed(model, "No Configured VAT in System");
            }
        }
    }
}

