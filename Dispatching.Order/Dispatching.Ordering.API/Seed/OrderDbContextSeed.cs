﻿
using Dispatching.Ordering.Models.Context;
using Dispatching.Ordering.Models.Entities;
using Dispatching.Ordering.Models.Settings;
using DispatchProduct.Ordering.API.ServicesCommunication.VAT;
using DispatchProduct.RepositoryModule;
using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Utilites.ProcessingResult;
using Microsoft.Extensions.DependencyInjection;
namespace DispatchProduct.Ordering.API.Seed
{
    public class OrderDbContextSeed : ContextSeed
    {
        OrderAppSettings settings = null;
        IServiceProvider serviceProvider;
        public async Task SeedAsync(IServiceProvider _serviceProvider, IOptions<OrderAppSettings> _settings, OrderDbContext context, IHostingEnvironment env,
            ILogger<OrderDbContextSeed> logger, int? retry = 0)
        {
            int retryForAvaiability = retry.Value;
            serviceProvider = _serviceProvider;
            settings = _settings.Value;
            try
            {


                var contentRootPath = env.ContentRootPath;
                var webroot = env.WebRootPath;
                List<LKP_OrderStatus> lstOrderStatus = new List<LKP_OrderStatus>();
                List<LKP_CityFees> lstCityFees = new List<LKP_CityFees>();
                List<Role_Order_Status> lstRoleOrderStatus = new List<Role_Order_Status>();
                lstCityFees =await GetDefaultLKPCityFees();
                lstOrderStatus = GetDefaultLKP_OrderStatus();
                lstRoleOrderStatus = GetDefaultRoleOrderStatus();
                await SeedEntityAsync(context, lstCityFees);
                await SeedEntityAsync(context, lstRoleOrderStatus);
                using (var transaction = context.Database.BeginTransaction())
                {
                    context.Database.OpenConnection();
                    await SeedEntityAsync(context, lstOrderStatus, false);
                    context.Database.ExecuteSqlCommand("SET IDENTITY_INSERT LKP_OrderStatus ON");
                    context.SaveChanges();
                    context.Database.ExecuteSqlCommand("SET IDENTITY_INSERT LKP_OrderStatus OFF");
                    transaction.Commit();
                }
            }
            catch (Exception ex)
            {
                if (retryForAvaiability < 3)
                {
                    retryForAvaiability++;

                    logger.LogError(ex.Message, $"There is an error migrating data for CustomerDbContextSeed");

                    await SeedAsync(_serviceProvider,_settings, context, env, logger, retryForAvaiability);
                }
            }
        }
        private async Task<List<LKP_CityFees>> GetDefaultLKPCityFees()
        {
            List<LKP_CityFees> result = new List<LKP_CityFees>
            {
                new LKP_CityFees() { Country="Kuwait", NameEN = "Kuwait", NameAR = "Kuwait",DeliveryFees=100,Code="KUW"},
                new LKP_CityFees() { Country=settings.DefaultFeesKeyEN, NameEN = settings.DefaultFeesKeyEN, NameAR = settings.DefaultFeesKeyAR,DeliveryFees=100,Code=settings.DefaultFeesKeyEN},
            };
            await BindVat(result);
            return result;
        }

        private List<LKP_OrderStatus> GetDefaultLKP_OrderStatus()
        {
            return settings.SystemOrderStatus;
            //List<LKP_OrderStatus> result = new List<LKP_OrderStatus>();
            //{
            //    new LKP_OrderStatus() { NameAR=settings.IntialOrderStatusAR,NameEN = settings.IntialOrderStatusEN,StepNo=settings.IntialOrderStatusStepNo},
            //    new LKP_OrderStatus() { NameAR=settings.PaidOrderStatusAR,NameEN = settings.PaidOrderStatusEN,StepNo=settings.PaidOrderStatusStepNo},
            //    new LKP_OrderStatus() { NameAR=settings.DeliveredOrderStatusAR,NameEN = settings.DeliveredOrderStatusEN,StepNo=settings.DeliveredOrderStatusStepNo}
            //};
            //return result;
        }

        private List<Role_Order_Status> GetDefaultRoleOrderStatus()
        {
            return settings.SystemRoleOrderStatus;
        }

        private IVATService VATService
        {
            get { return serviceProvider.GetService<IVATService>(); }
        }
        private async Task<ProcessResult<LKP_CityFees>> BindVat(LKP_CityFees model)
        {
            var vat = await VATService.GetVAT();
            if (vat != null && vat.IsSucceeded && vat.Data != null)
            {
                model.DeliveryVAT = vat.Data.Percentage* model.DeliveryFees / 100;
                model.DeliveryFinalFees = model.DeliveryVAT + model.DeliveryFees;
                return ProcessResultHelper.Succedded(model);
            }
            else
            {
                return ProcessResultHelper.Failed(model, new Exception("No Configured VAT in System"));
            }
        }
        private async Task<ProcessResult<List<LKP_CityFees>>> BindVat(List<LKP_CityFees> cityFees)
        {
            ProcessResult<List<LKP_CityFees>> result = null;
            for (int i = 0; i < cityFees.Count; i++)
            {
                var vatRes = await BindVat(cityFees[i]);
                if (vatRes.IsSucceeded)
                {
                    cityFees[i] = vatRes.Data;
                }
                else
                {
                    result = ProcessResultHelper.MapToProcessResult<LKP_CityFees, List<LKP_CityFees>>(vatRes);
                    return result;
                }
            }
            result = ProcessResultHelper.Succedded(cityFees);
            return result;
        }
    }
}

