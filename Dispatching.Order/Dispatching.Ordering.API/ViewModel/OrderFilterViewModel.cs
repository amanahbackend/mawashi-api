﻿using DispatchProduct.BuildingBlocks.BaseEntities.IEntities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Dispatching.Ordering.API.ViewModel
{
    public class OrderFilterViewModel : IRepoistryBaseEntity
    {
        public string Status { get; set; }
        public string TransactionNo { get; set; }
        public string CardType { get; set; }
        public string TransactionCode { get; set; }
        public bool IsPaymentProcessed { get; set; }
        public string PaymentMessage { get; set; }
        public bool IsPaymentSucceeded { get; set; }
        public string ReciptNo { get; set; }
        public string PaymentTypeCode { get; set; }
        public int FK_Cart_Id { get; set; }
        public List<CartItemFilterViewModel> CartItems { get; set; }
        public DateTime DeliveryDate { get; set; }
        public string DeliveryType { get; set; }
        public string OrderTypeName { get; set; }
        public double DeliveryFees { get; set; }
        public double CartPrice { get; set; }
        public double CartVAT { get; set; }
        public double PriceExcludedVat { get; set; }
        public double Price { get; set; }
        public double VATValue { get; set; }
        public double VATPercentage { get; set; }
        public string VATNameAR { get; set; }
        public string VATNameEN { get; set; }
        public string Code { get; set; }
        public string PromoCode { get; set; }
        public string Platform { get; set; }
        public bool HasPromoCode { get; set; }
        public int PromoValue { get; set; }
        public string CustomerPhone { get; set; }
        public string CustomerName { get; set; }
        public string CustomerEmail { get; set; }
        public string DriverName { get; set; }
        public string ProducerName { get; set; }
        public string CreatedUserName { get; set; }

        public string Number { get; set; }
        public string Name { get; set; }
        public string GovernorateName { get; set; }
        public string AreaName { get; set; }
        public string BlockName { get; set; }
        public string StreetName { get; set; }
        public string Building { get; set; }
        public string Floor { get; set; }
        public string AppartmentNumber { get; set; }
        public double? Latitude { get; set; }
        public double? Longitude { get; set; }
        public string DeliveryNote { get; set; }
        public int Id { get; set; }
        public string CurrentUserId { get; set; }
        public DateTime CreatedDate { get; set; }

    }
}
