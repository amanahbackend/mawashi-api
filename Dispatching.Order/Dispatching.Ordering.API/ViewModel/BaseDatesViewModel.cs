﻿using DispatchProduct.BuildingBlocks.BaseEntities.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace Dispatching.Ordering.API.ViewModels
{
    public class BaseDatesViewModel : BaseLKPEntityViewModel
    {
        public DateTime Date { get; set; }
    }
}
