﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Dispatching.Ordering.API.ViewModel
{
    public class OrdersCountPerDayViewModel
    {
        public DateTime Day { get; set; }
        public int OrdersCount { get; set; }
    }
}
