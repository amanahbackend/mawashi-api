﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Utilites.PaginatedItemsViewModel;

namespace Dispatching.Ordering.API.ViewModel
{
    public class FilterViewModel
    {
        public string PropertyName { get; set; }
        public object Value { get; set; }


    }
}
