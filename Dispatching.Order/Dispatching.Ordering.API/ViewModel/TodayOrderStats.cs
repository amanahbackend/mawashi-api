﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Dispatching.Ordering.API.ViewModel
{
    public class TodayOrderStats
    {
        public int OrdersCount { get; set; }
        public int DeliveredOrdersCount { get; set; }
        public double TotalPayment { get; set; }
    }
}
