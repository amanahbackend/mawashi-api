﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Dispatching.Ordering.API.ViewModel
{
    public class PaymentPerDayViewModel
    {
        public DateTime Day { get; set; }
        public double TotalPayment { get; set; }
    }
}
