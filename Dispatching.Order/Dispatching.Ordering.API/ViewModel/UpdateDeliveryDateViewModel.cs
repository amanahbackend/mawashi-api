﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Dispatching.Ordering.API.ViewModel
{
    public class UpdateDeliveryDateViewModel
    {
        public int OrderId { get; set; }
        public DateTime DeliveryDate { get; set; }
    }
}
