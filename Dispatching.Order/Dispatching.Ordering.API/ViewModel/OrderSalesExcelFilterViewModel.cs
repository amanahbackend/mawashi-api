﻿using DispatchProduct.BuildingBlocks.BaseEntities.IEntities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Dispatching.Ordering.API.ViewModel
{
    public class OrderSalesExcelFilterViewModel 
    {
        public int OrderId { get; set; }
        public string Status { get; set; }
        public string Code { get; set; }
        public string ItemName { get; set; }
        public bool IsSalesChangePrice { get; set; }
        public int Qty { get; set; }
        public double SalesPricePerItem { get; set; }
        public double QuantityKG { get; set; }
        public double ItemPrice { get; set; }
        //public double CartItemPrice { get; set; }
        public double Price { get; set; }
        public string Measurment { get; set; }
        public string CustomerName { get; set; }
        public string ProducerName { get; set; }
        public DateTime DeliveryDate { get; set; }
        public string Governorate { get; set; }
        public string Judda { get; set; }
        public string Block { get; set; }
        public string Street { get; set; }
        public string House { get; set; }
        public string Floor { get; set; }
        public string AppartmentNumber { get; set; }
        public double? Latitude { get; set; }
        public double? Longitude { get; set; }
        public string DeliveryNote { get; set; }
        public string But { get; set; }
        public string DeliveryType { get; set; }
        public string PaymentTypeCode { get; set; }
        public double VATValue { get; set; }
        public string CustomerPhone { get; set; }
        public string CustomerEmail { get; set; }
        public string CreatedUserName { get; set; }
        public DateTime CreatedDate { get; set; }

        //public double DeliveryFees { get; set; }
        //public string DriverName { get; set; }
        //public string Platform { get; set; }
        //public string TransactionNo { get; set; }
        //public string CardType { get; set; }
        // public string TransactionCode { get; set; }
        //public bool IsPaymentProcessed { get; set; }
        //public string ReciptNo { get; set; }
        //public int FK_Cart_Id { get; set; }
        ////public List<CartItemFilterViewModel> CartItems { get; set; }
        //public string OrderTypeName { get; set; }
        //public double CartPrice { get; set; }
        //public double CartVAT { get; set; }
        //public double PriceExcludedVat { get; set; }
        //public double VATPercentage { get; set; }
        //public string VATNameAR { get; set; }
        //public string VATNameEN { get; set; }
        //public string Number { get; set; }
        //public string Name { get; set; }
        //public int Id { get; set; }
        //public string CurrentUserId { get; set; }
    }
}
