﻿using Dispatching.Ordering.Models.IEntities;
using DispatchProduct.BuildingBlocks.BaseEntities.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace Dispatching.Ordering.API.ViewModel
{
    public class LKP_OrderStatusViewModel : BaseLKPEntityViewModel
    {
        public int StepNo { get; set; }
    }
}
