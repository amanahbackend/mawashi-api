﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Utilites.PaginatedItemsViewModel;

namespace Dispatching.Ordering.API.ViewModel
{
    public class PaginatedFilterViewModel
    {
        public List<FilterViewModel> Filters { get; set; }
        public PaginatedItemsViewModel<OrderViewModel> PageInfo { get; set; } = null;

    }
}
