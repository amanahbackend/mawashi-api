﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Dispatching.Ordering.API.ViewModel
{
    public class InvoicePrintViewModel
    {
        public int TotalOrders { get; set; }
        public double TotalPayment { get; set; }
        public List<TotalOrdersPerCategory> TotalOrdersPerCategories { get; set; }
        public List<OrderFilterViewModel> Orders { get; set; }

    }

    public class TotalOrdersPerCategory
    {
        public string CategoryName { get; set; }
        public int TotalOrders { get; set; }
    }
}
