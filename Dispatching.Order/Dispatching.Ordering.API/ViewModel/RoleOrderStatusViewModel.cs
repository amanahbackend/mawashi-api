﻿using Dispatching.Ordering.Models.IEntities;
using DispatchProduct.BuildingBlocks.BaseEntities.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace Dispatching.Ordering.API.ViewModel
{
    public class RoleOrderStatusViewModel: RepoistryBaseEntity
    {
        public string RoleName { get; set; }
        public string FK_Role_Id { get; set; }
        public int FK_Order_Status_Id { get; set; }
        public LKP_OrderStatusViewModel LKP_OrderStatus { get; set; }
    }
}
