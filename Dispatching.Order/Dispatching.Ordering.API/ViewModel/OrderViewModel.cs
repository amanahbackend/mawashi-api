﻿using CommonEnums;
using Dispatching.BuisnessCommon.Enums;
using Dispatching.CustomerModule.API.ViewModels;
using Dispatching.Ordering.API.ServicesViewModels;
using Dispatching.Ordering.API.ServicesViewModelsViewModel.Cart;
using Dispatching.Ordering.Models.IEntities;
using DispatchProduct.BuildingBlocks.BaseEntities.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace Dispatching.Ordering.API.ViewModel
{
    public class OrderViewModel : RepoistryBaseEntity
    {
        public string TransactionNo { get; set; }
        public string CardType { get; set; }
        public string TransactionCode { get; set; }
        public bool IsPaymentProcessed { get; set; }
        public string PaymentMessage { get; set; }
        public bool IsPaymentSucceeded { get; set; }
        public int FK_Cart_Id { get; set; }
        public string ReciptNo { get; set; }
        public string CreatedUserName { get; set; }
        public int FK_Order_Type_Id { get; set; }
        public int FK_Order_Status_Id { get; set; }
        public int FK_Customer_Id { get; set; }
        public PaymentType PaymenMechanism { get; set; }
        public CartViewModel Cart { get; set; }
        public string PaymentTypeCode { get; set; }
        public PaymentMethodViewModel PaymentType { get; set; }
        public DateTime DeliveryDate { get; set; }
        public DeliveryType FK_DeliveryType_Id { get; set; }
        public string DeliveryType { get; set; }
        public OrderType OrderType { get; set; }
        public EnumEntity OrderTypeObject { get; set; }
        public string OrderTypeName { get; set; }
        public LKP_OrderStatusViewModel OrderStatus { get; set; }
        public int FK_CityDeliveryFees_Id { get; set; }
        public double DeliveryFees { get; set; }
        public double CartPrice { get; set; }
        public double CartVAT { get; set; }
        public double PriceExcludedVat { get; set; }
        public double Price { get; set; }
        public double VATValue { get; set; }
        public double VATPercentage { get; set; }
        public string VATNameAR { get; set; }
        public string VATNameEN { get; set; }
        public string Code { get; set; }
        public string PromoCode { get; set; }
        public bool HasPromoCode { get; set; }
        public int PromoValue { get; set; }
        public int FK_Promo_Id { get; set; }
        public Platform FK_Platform_Id { get; set; }
        public string PaymentURL { get; set; }
        public string Platform { get; set; }
        public CustomerViewModel Customer { get; set; }
        public string CustomerPhone { get; set; }
        public string CustomerName { get; set; }
        public string CustomerEmail { get; set; }
        public string FK_Driver_Id { get; set; }
        public string DriverName { get; set; }
        public string FK_Producer_Id { get; set; }
        public string ProducerName { get; set; }
        #region Location
        public int Fk_Location_Id { get; set; }
        public AddressType Fk_AddressType_Id { get; set; }
        public string Number { get; set; }
        public Country Fk_Country_Id { get; set; }
        public string Name { get; set; }
        public string GovernorateName { get; set; }
        public string GovernorateId { get; set; }

        public string AreaName { get; set; }
        public string AreaId { get; set; }

        public string BlockId { get; set; }
        public string BlockName { get; set; }

        public string StreetId { get; set; }
        public string StreetName { get; set; }
        public string Building { get; set; }
        public string Floor { get; set; }
        public string AppartmentNumber { get; set; }
        public double? Latitude { get; set; }
        public double? Longitude { get; set; }
        public string DeliveryNote { get; set; }
        public string LocationDeliveryNote { get; set; }
        public DateTime CreatedDate { get; set; }
        #endregion
    }
}
