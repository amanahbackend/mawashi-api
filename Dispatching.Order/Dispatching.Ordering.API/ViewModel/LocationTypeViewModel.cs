﻿using CommonEnums;
using DispatchProduct.BuildingBlocks.BaseEntities.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace Dispatching.Ordering.API.ViewModels
{
    public class LocationTypeViewModel
    {
        public DeliveryType DeliveryType { get; set; }
        public int FK_Customer_Id { get; set; }
        public int FK_Config_Id { get; set; }
    }
}
