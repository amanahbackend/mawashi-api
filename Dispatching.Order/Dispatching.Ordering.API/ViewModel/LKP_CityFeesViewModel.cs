﻿using Dispatching.Ordering.Models.IEntities;
using DispatchProduct.BuildingBlocks.BaseEntities.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace Dispatching.Ordering.API.ViewModel
{
    public class LKP_CityFeesViewModel : BaseLKPEntityViewModel
    {
        public string Country { get; set; }
        public string Code { get; set; }
        public double DeliveryFees { get; set; }
        public double DeliveryVAT { get; set; }
        public double DeliveryFinalFees { get; set; }
    }
}
