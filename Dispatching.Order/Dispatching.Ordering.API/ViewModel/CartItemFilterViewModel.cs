﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Dispatching.Ordering.API.ViewModel
{
    public class CartItemFilterViewModel
    {
        public string CuttingNameAR { get; set; }
        public string CuttingNameEN { get; set; }
        public string ItemNameAR { get; set; }
        public string ItemNameEN { get; set; }
        public string ItemCode { get; set; }
        public double ItemQuantityByKG { get; set; }
        public string Measurment { get; set; }
        public double ItemPrice { get; set; }
        public double CartItemPrice { get; set; }
        public double ItemNetPrice { get; set; }
        public int QuantityValue { get; set; }
        public double SalesPricePerItem { get; set; }
        public bool IsSalesChangePrice { get; set; }
    }
}
