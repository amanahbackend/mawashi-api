﻿using Dispatching.Ordering.Models.IEntities;
using DispatchProduct.BuildingBlocks.BaseEntities.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace Dispatching.Ordering.API.ViewModel
{
    public class PaymentStatusViewModel: RepoistryBaseEntity
    {
        public bool IsPaymentProcessed { get; set; }
        public string PaymentMessage { get; set; }
        public bool IsPaymentSucceeded { get; set; }
    }
}
