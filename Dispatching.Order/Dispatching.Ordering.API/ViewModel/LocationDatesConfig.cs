﻿using CommonEnums;
using DispatchProduct.BuildingBlocks.BaseEntities.Entities;
using System;
using System.Collections.Generic;
using System.Text;
using Dispatching.Ordering.API.ServicesViewModels;
namespace Dispatching.Ordering.API.ViewModels
{
    public class LocationDatesConfig
    {
        public List<LocationViewModel> Locations { get; set;}
        public List<BaseDatesViewModel> Dates { get; set;}
    }
}
