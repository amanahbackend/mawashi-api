﻿using CommonEnums;
using DispatchProduct.BuildingBlocks.BaseEntities.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace Dispatching.Ordering.API.ViewModels
{
    public class AssignDriverToOrderViewModel
    {
        public int FK_Order_Id { get; set; }
        public string FK_Driver_Id { get; set; }
        public string DriverName { get; set; }
    }
}
