﻿using DispatchProduct.HttpClient;

namespace DispatchProduct.Ordering.API.ServicesCommunication.Settings
{
    public class CustomerLocationServiceSetting : DefaultHttpClientSettings
    {
        public override string Uri
        {
            get; set;
        }
        public string GetByCustomerIdVerb
        {
            get; set;
        }
    }
}
