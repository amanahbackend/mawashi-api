﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using BuildingBlocks.ServiceDiscovery;
using DispatchProduct.HttpClient;
using DnsClient;

namespace Dispatching.Ordering.API.ServiceSettings
{
    public class IdentityServiceSettings : DefaultHttpClientSettings
    {
        public string GetUserDevicesAction { get; set; }
        public override string Uri
        {
            get; set;
        }
    }
}
