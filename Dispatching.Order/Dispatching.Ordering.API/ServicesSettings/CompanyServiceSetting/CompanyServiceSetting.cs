﻿using DispatchProduct.HttpClient;

namespace DispatchProduct.Ordering.API.ServicesCommunication.Settings
{
    public class CompanyServiceSetting : DefaultHttpClientSettings
    {
        public override string Uri
        {
            get; set;
        }
        public string GetByCompanyIdVerb
        {
            get; set;
        }
        public string PayOrderFromWalletVerb
        {
            get; set;
        }
        
    }
}
