﻿using DispatchProduct.HttpClient;

namespace DispatchProduct.Ordering.API.ServicesCommunication.Settings
{
    public class CartServiceSetting : DefaultHttpClientSettings
    {
        public override string Uri
        {
            get; set;
        }
        public string SubmitVerb
        {
            get; set;
        }
        public string SubmitSalesVerb
        {
            get; set;
        }
        public string UpdateOrderCartInfoVerb
        {
            get; set;
        }
        public string GetCartWithItemsVerb
        {
            get; set;
        }
        public string GetCartsWithItemsVerb { get; set; }
    }
}
