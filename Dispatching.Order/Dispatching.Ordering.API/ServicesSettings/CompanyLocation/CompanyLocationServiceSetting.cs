﻿using DispatchProduct.HttpClient;

namespace DispatchProduct.Ordering.API.ServicesCommunication.Settings
{
    public class CompanyLocationServiceSetting : DefaultHttpClientSettings
    {
        public override string Uri
        {
            get; set;
        }
        public string GetByCompanyIdVerb
        {
            get; set;
        }
    }
}
