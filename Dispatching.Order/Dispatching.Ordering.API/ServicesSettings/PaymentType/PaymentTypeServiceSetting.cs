﻿using DispatchProduct.HttpClient;

namespace DispatchProduct.Ordering.API.ServicesCommunication.Settings
{
    public class PaymentTypeServiceSetting : DefaultHttpClientSettings
    {
        public override string Uri
        {
            get; set;
        }
    }
}
