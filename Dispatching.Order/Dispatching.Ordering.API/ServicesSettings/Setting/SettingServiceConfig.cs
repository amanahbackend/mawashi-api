﻿using DispatchProduct.HttpClient;

namespace DispatchProduct.Ordering.API.ServicesCommunication.Settings
{
    public class SettingServiceConfig : DefaultHttpClientSettings
    {
        public override string Uri
        {
            get; set;
        }
        public string GetSettingsVerb
        {
            get; set;
        }
    }
}
