﻿using DispatchProduct.HttpClient;

namespace DispatchProduct.Ordering.API.ServicesCommunication.Settings
{
    public class PickUpServiceSetting : DefaultHttpClientSettings
    {
        public override string Uri
        {
            get; set;
        }
    }
}
