﻿using DispatchProduct.HttpClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Dispatching.CustomerModule.API.ServicesSettings
{
    public class DeliveryOptionServiceSettings : DefaultHttpClientSettings
    {
        public override string Uri
        {
            get; set;
        }
        public string GetDateTypeVerb
        {
            get; set;
        }
    }
}
