﻿using DispatchProduct.HttpClient;

namespace DispatchProduct.Ordering.API.ServicesCommunication.Settings
{
    public class SMSServiceSetting : DefaultHttpClientSettings
    {
        public override string Uri
        {
            get; set;
        }
        public string SendVerb
        {
            get; set;
        }
    }
}
