﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Threading.Tasks;
using Autofac;
using AutoMapper;
using BuildingBlocks.ServiceDiscovery;
using Dispatching.Ordering.BLL.IManagers;
using Dispatching.Ordering.BLL.Managers;
using Dispatching.Ordering.Models.Context;
using Dispatching.Ordering.Models.Entities;
using Dispatching.Ordering.Models.IEntities;
using DispatchProduct.RepositoryModule;
using DnsClient;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Newtonsoft.Json.Serialization;
using Swashbuckle.AspNetCore.Swagger;
using Utilites.PaginatedItems;
using Utilites.ProcessingResult;
using Autofac.Extensions.DependencyInjection;
using DispatchProduct.Ordering.API.ServicesCommunication.Cart;
using Dispatching.Ordering.API.ServicesCommunication.Cart;
using Dispatching.Ordering.API.ServicesCommunication.Location;
using DispatchProduct.Ordering.API.ServicesCommunication.Location;
using DispatchProduct.Ordering.API.ServicesCommunication.Settings;
using Dispatching.Ordering.Models.Settings;
using Dispatching.Ordering.API.ServicesCommunication.PaymentURL;
using Dispatching.Ordering.API.Controllers;
using Dispatching.Ordering.API.ViewModels;
using Dispatching.Ordering.API.ServicesViewModels.DeliveryOption;
using Dispatching.CustomerModule.API.ServicesSettings;
using DispatchProduct.Ordering.API.ServicesCommunication.PaymentURL;
using Dispatching.Ordering.API.ServiceCommunications;
using DispatchProduct.Ordering.API.ServicesCommunication.PromoCode;
using DispatchProduct.Ordering.API.ServicesCommunication.VAT;
using Dispatching.Ordering.API.ServicesCommunication.VAT;
using Utilites.PaginatedItemsViewModel;
using Dispatching.Ordering.API.ServiceCommunications.Identity;
using Dispatching.Ordering.API.ViewModel;
using Dispatching.Ordering.API.ServiceSettings;
using Microsoft.Extensions.FileProviders;
using System.IO;
using Dispatching.Ordering.API.ServicesCommunication.DropOut.Notification;
using Dispatching.Ordering.API.ServicesSettings.DropOut.Notification;

namespace Dispatching.Ordering.API
{
    public class Startup
    {
        public IConfigurationRoot Configuration { get; }
        public IHostingEnvironment _env;
        public Startup(IHostingEnvironment env)
        {
            _env = env;
            var builder = new ConfigurationBuilder()
                .SetBasePath(env.ContentRootPath)
                .AddEnvironmentVariables();
            Configuration = builder.Build();

            var country = Configuration.GetSection("DockerContainerName").Value.Split('.').Last().ToLower();
            // var country = "kw";
             builder = new ConfigurationBuilder()
                .SetBasePath(env.ContentRootPath)
                .AddJsonFile($"appsettings.{country}.json", optional: false, reloadOnChange: true)
                .AddEnvironmentVariables();

            Configuration = builder.Build();
        }

        // This method gets called by the runtime. Use this method to add services to the container.
        public IServiceProvider ConfigureServices(IServiceCollection services)
        {
            services.AddCors(options =>
            {
                options.AddPolicy("AllowAll",
                    builder =>
                    {
                        builder
                        .AllowAnyOrigin()
                        .AllowAnyMethod()
                        .AllowAnyHeader()
                        .AllowCredentials();
                    });
            });
            services.AddSingleton(provider => Configuration);

            services.Configure<CartServiceSetting>
               (Configuration.GetSection("ServiceCommunications:CartService"));
            services.Configure<SMSServiceSetting>
              (Configuration.GetSection("ServiceCommunications:SMSService"));
            services.Configure<EmailServiceSetting>
              (Configuration.GetSection("ServiceCommunications:EmailService"));
            services.Configure<NotificationServiceSetting>
              (Configuration.GetSection("ServiceCommunications:NotificationService"));

            services.Configure<CustomerLocationServiceSetting>
               (Configuration.GetSection("ServiceCommunications:CustomerLocationService"));

            services.Configure<IdentityServiceSettings>
              (Configuration.GetSection("ServiceCommunications:IdentityService"));

            services.Configure<DeliveryOptionServiceSettings>
              (Configuration.GetSection("ServiceCommunications:DeliveryOptionService"));
            services.Configure<CustomerServiceSetting>
              (Configuration.GetSection("ServiceCommunications:CustomerService"));

            services.Configure<SettingServiceConfig>
             (Configuration.GetSection("ServiceCommunications:SettingService"));

            services.Configure<CompanyServiceSetting>
              (Configuration.GetSection("ServiceCommunications:CompanyService"));
            services.Configure<UAEPaymentURLServiceSetting>
               (Configuration.GetSection("ServiceCommunications:UAEPaymentURLService"));

            services.Configure<KWPaymentURLServiceSetting>
              (Configuration.GetSection("ServiceCommunications:KWPaymentURLService"));

            services.Configure<LocationServiceSetting>
              (Configuration.GetSection("ServiceCommunications:LocationService"));

            services.Configure<CompanyLocationServiceSetting>
             (Configuration.GetSection("ServiceCommunications:CompanyLocationService"));

            services.Configure<VATServiceSetting>
            (Configuration.GetSection("ServiceCommunications:VatService"));

            services.Configure<PromoCodeServiceSetting>
             (Configuration.GetSection("ServiceCommunications:PromoCodeService"));

            services.Configure<OrderAppSettings>(Configuration);
          



            
            // Add framework services.
            services.AddDbContext<OrderDbContext>(options =>
             options.UseSqlServer(Configuration["ConnectionString"],
              sqlOptions => sqlOptions.MigrationsAssembly("Dispatching.Ordering.EFCore.MSSQL")));
            services.AddOptions();
            services.AddMvc().AddJsonOptions(options =>
            {
                options.SerializerSettings.ContractResolver = new CamelCasePropertyNamesContractResolver();
            });
            services.AddAutoMapper(typeof(Startup));
            Mapper.AssertConfigurationIsValid();

            #region Data & Managers
            services.AddScoped<DbContext, OrderDbContext>();
            services.AddScoped(typeof(IRepository<>), typeof(Repository<>));
            services.AddScoped(typeof(IProcessResultMapper), typeof(ProcessResultMapper));
            services.AddScoped(typeof(IProcessResultPaginatedMapper), typeof(ProcessResultPaginatedMapper));
            services.AddScoped(typeof(IPaginatedItems<>), typeof(PaginatedItems<>));
            services.AddScoped(typeof(IPaginatedItemsViewModel<>), typeof(PaginatedItemsViewModel<>));

            services.AddScoped(typeof(ILKP_CityFeesManager), typeof(LKP_CityFeesManager));
            services.AddScoped(typeof(ILKP_OrderStatusManager), typeof(LKP_OrderStatusManager));
            //services.AddScoped(typeof(ILKP_OrderTypeManager), typeof(LKP_OrderTypeManager));
            services.AddScoped(typeof(IOrderManager), typeof(OrderManager));
            services.AddScoped(typeof(IRoleOrderStatusManager), typeof(RoleOrderStatusManager));
            services.AddScoped(typeof(IOrderWorkFlowManager), typeof(OrderWorkFlowManager));

            
            services.AddScoped(typeof(ILKP_CityFees), typeof(LKP_CityFees));
            services.AddScoped(typeof(ILKP_OrderStatus), typeof(LKP_OrderStatus));
            //services.AddScoped(typeof(ILKP_OrderType), typeof(LKP_OrderType));
            services.AddScoped(typeof(ICartService), typeof(CartService));
            services.AddScoped(typeof(ILocationService), typeof(LocationService));
            services.AddScoped(typeof(ICustomerLocationService), typeof(CustomerLocationService));
            services.AddScoped(typeof(IPromoCodeService), typeof(PromoCodeService));
            services.AddScoped(typeof(ICustomerService), typeof(CustomerService));
            services.AddScoped(typeof(ICompanyService), typeof(CompanyService));
            services.AddScoped<ICompanyLocationService, CompanyLocationService>();
            services.AddScoped<IVATService, VATService>();
            services.AddScoped<ISMSService, SMSService>();
            services.AddScoped<IEmailService, EmailService>();
            services.AddScoped<INotificationService, NotificationService>();
            services.AddScoped(typeof(IUAEPaymentURLService), typeof(UAEPaymentURLService));
            services.AddScoped(typeof(IKWPaymentURLService), typeof(KWPaymentURLService));
            services.AddScoped(typeof(IDeliveryOptionService), typeof(DeliveryOptionService));
            services.AddScoped(typeof(ISettingService), typeof(SettingService));
            services.AddScoped(typeof(IIdentityService), typeof(IdentityService));
            services.AddScoped<IDonationDatesManager, DonationDatesManager>();
            services.AddScoped<IDonationManager, DonationManager>();
            services.AddScoped<IEidDatesManager, EidDatesManager>();
            services.AddScoped<IPickUpDatesManager, PickUpDatesManager>();
            services.AddScoped<IPickUpManager, PickUpManager>();
            services.AddScoped(typeof(IDonationDates), typeof(DonationDates));
            services.AddScoped(typeof(IDonation), typeof(Donation));
            services.AddScoped(typeof(IEidDates), typeof(EidDates));
            services.AddScoped(typeof(IPickUpDates), typeof(PickUpDates));
            services.AddScoped(typeof(IPickUp), typeof(PickUp));

            services.AddScoped<DonationController, DonationController>();
            //services.AddScoped<NotificationController, NotificationController>();
            //services.AddScoped<SendNotificationViewModel, SendNotificationViewModel>();
            services.AddScoped<PickUpController, PickUpController>();
            services.AddScoped<DonationDatesController, DonationDatesController>();
            services.AddScoped<PickUpDatesController, PickUpDatesController>();
            services.AddScoped<EidDatesController, EidDatesController>();
            services.AddScoped<LocationDatesConfig, LocationDatesConfig>();
            services.AddScoped<CategoryInputDeliveryOptions, CategoryInputDeliveryOptions>();


            #endregion Data & Managers

            //Dns Query for consul - service discovery

            string consulContainerName = Configuration.GetSection("ServiceRegisteryContainerName").Value;
            int consulPort = Convert.ToInt32(Configuration.GetSection("ServiceRegisteryPort").Value);
            IPAddress ip = Dns.GetHostEntry(consulContainerName).AddressList.Where(o => o.AddressFamily == AddressFamily.InterNetwork).First();
            services.AddSingleton<IDnsQuery>(new LookupClient(ip, consulPort));
            services.AddServiceDiscovery(Configuration.GetSection("ServiceDiscovery"));

            // Register the Swagger generator, defining one or more Swagger documents
            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1", new Info { Title = "", Version = "v1" });
            });

            //services.Configure<OrderAppSettings>(Configuration);


            services.AddApiVersioning(o => o.ReportApiVersions = true);
            var container = new ContainerBuilder();
            container.Populate(services);
            return new AutofacServiceProvider(container.Build());
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            app.UseCors("AllowAll");

            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
                app.UseDatabaseErrorPage();
            }
            //else
            //{
            //    app.UseExceptionHandler("/Home/Error");
            //}

            // Enable middleware to serve generated Swagger as a JSON endpoint.
            app.UseStaticFiles();
            app.UseSwagger();
            // Enable middleware to serve swagger-ui (HTML, JS, CSS, etc.), specifying the Swagger JSON endpoint.
            app.UseSwaggerUI(c =>
            {
                c.SwaggerEndpoint("/swagger/v1/swagger.json", "");
            });

            app.UseStaticFiles();
            app.UseFileServer(new FileServerOptions
            {
                FileProvider = new PhysicalFileProvider(
                 Path.Combine(Directory.GetCurrentDirectory(), "wwwroot", "Logs")),
                RequestPath = "/Logs",
                EnableDirectoryBrowsing = true
            });
            app.UseMvc();

            // Autoregister using server.Features (does not work in reverse proxy mode)
            app.UseConsulRegisterService();
        }
    }
}
