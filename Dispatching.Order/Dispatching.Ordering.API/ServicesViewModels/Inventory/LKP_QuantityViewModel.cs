﻿using DispatchProduct.BuildingBlocks.BaseEntities.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace Dispatching.Ordering.API.ServicesViewModels.Inventory
{
    public class LKP_QuantityViewModel : BaseLKPEntityViewModel
    {
        public int Value { get; set; }
        public string DescriptionAR { get; set; }
        public string DescriptionEN { get; set; }
    }
}
