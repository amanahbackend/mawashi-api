﻿using DispatchProduct.BuildingBlocks.BaseEntities.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Dispatching.Ordering.API.ServicesViewModels
{
    public class PaymentMethodViewModel : BaseLKPEntityViewModel
    {
        public bool IsActivated { get; set; }
        public string CountryCode { get; set; }
        public int CountryId { get; set; }
        public string Code { get; set; }
    }
}
