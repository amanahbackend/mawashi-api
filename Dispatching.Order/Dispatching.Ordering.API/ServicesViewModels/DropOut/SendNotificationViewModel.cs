﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Dispatching.Ordering.API.ServicesViewModels.DropOut
{
    public class SendNotificationViewModel
    {
        public string UserId { get; set; }
        public string Title { get; set; }
        public string Body { get; set; }
        public dynamic Data { get; set; }
    }
}
