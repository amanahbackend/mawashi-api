﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Dispatching.Ordering.API.ServicesViewModels.DropOut
{
    public class SendSmsViewModel
    {
        public string Message { get; set; }
        public string PhoneNumber { get; set; }
    }
}
