﻿using System.ComponentModel.DataAnnotations.Schema;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;
using DispatchProduct.BuildingBlocks.BaseEntities.Entities;
using Dispatching.Ordering.API.ServicesViewModelsViewModel.Cart;

namespace Dispatching.Ordering.API.ServicesViewModels.Cart
{
    public class CartOrderViewModel 
    {
        public int FK_Cart_Id { get; set; }
        public int FK_Order_Id { get; set; }
        public CartViewModel Cart { get; set; }
    }
}
