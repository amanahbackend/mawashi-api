﻿using DispatchProduct.BuildingBlocks.BaseEntities.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Dispatching.Ordering.API.ServicesViewModels
{
    public class PaymentURLViewModel 
    {
        public int OrderId { get; set; }
        public double Amount { get; set; }
    }
}
