﻿using CommonEnums;
using DispatchProduct.BuildingBlocks.BaseEntities.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace Dispatching.Ordering.API.ServicesViewModels.DeliveryOption
{
    public class CategoryInputDeliveryOptions
    {
        public int FK_CategoryConfig_Id { get; set; }
        public int DeliveryId { get; set; }
    }
}
