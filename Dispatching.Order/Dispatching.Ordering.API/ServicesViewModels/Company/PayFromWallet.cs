﻿using CommonEnums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Dispatching.CustomerModule.API.ViewModels
{
    public class PayFromWallet
    {
        public int OrderId  { get; set; }
        public int OrderFees { get; set; }
        public int CompanyId { get; set; }

    }
}
