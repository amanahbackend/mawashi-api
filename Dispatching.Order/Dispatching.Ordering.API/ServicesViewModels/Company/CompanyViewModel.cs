﻿using CommonEnums;
using DispatchProduct.BuildingBlocks.BaseEntities.Entities;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Dispatching.CustomerModule.API.ViewModels
{
    public class CompanyViewModel : RepoistryBaseEntity
    {
        public string UserName { get; set; }
        public string Email { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Phone1 { get; set; }
        public string Phone2 { get; set; }
        public string PicturePath { get; set; }
        public List<string> RoleNames { get; set; }
        public string Picture { get; set; }
        [Required]
        [StringLength(100, ErrorMessage = "The {0} must be at least {2} and at max {1} characters long.", MinimumLength = 6)]
        [DataType(DataType.Password)]
        [Display(Name = "Password")]
        public string Password { get; set; }
        public string Fk_AppUser_Id { get; set; }
        public Country Fk_Country_Id { get; set; }
        public string PictureUrl { get; set; }
        public SupportedLanguage Language { get; set; }
        public string Name { get; set; }
        public string ContactName { get; set; }
        public string ContactPhone { get; set; }
        public string ContactMail { get; set; }
        public double Wallet { get; set; }
    }
}
