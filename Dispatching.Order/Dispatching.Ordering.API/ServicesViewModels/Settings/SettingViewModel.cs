﻿using CommonEnums;
using DispatchProduct.BuildingBlocks.BaseEntities.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace Dispatching.Settings.ViewModel
{
    public class SettingViewModel: BaseEntity
    {
        public string Key { get; set; }
        public int FK_Group_Id { get; set; }
        public string Value { get; set; }
        public SettingGroupViewModel Group { get; set; }
    }
}
