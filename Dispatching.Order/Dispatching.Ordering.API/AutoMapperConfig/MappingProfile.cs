﻿using AutoMapper;
using CommonEnums;
using Dispatching.Ordering.API.ViewModels;
using Dispatching.Ordering.Models.Entities;
using Dispatching.Ordering.API.ServicesViewModels;
using Dispatching.Ordering.API.ViewModel;
using Dispatching.Ordering.Models;
using Dispatching.Ordering.Models.Settings;
using DispatchProduct.BuildingBlocks.BaseEntities.Entities;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Utilites.PaginatedItems;
using Utilites.PaginatedItemsViewModel;
using Utilities;
using Utilities.Utilites;
using Dispatching.Ordering.Models.Context;
using Dispatching.Ordering.API.ServicesViewModels.DeliveryOption;
using Dispatching.Ordering.API.ServicesViewModels.Cart;
using Dispatching.Ordering.API.ServicesViewModelsViewModel.Cart;
using Dispatching.PromoCodeModule.Models.Entities;
using Dispatching.Ordering.API.ServicesViewModels.VAT;
using Dispatching.CustomerModule.API.ViewModels;
using Dispatching.Ordering.API.ServicesViewModels.DropOut;
using Dispatching.BuisnessCommon.Enums;
using DispatchProduct.BuildingBlocks.BaseEntities.IEntities;

namespace Dispatching.Inventory.API.AutoMapperConfig
{
    public class MappingProfile : Profile
    {
        public MappingProfile()
        {

            CreateMap<UpdateOrderItemsViewModel, UpdateOrderSalesItemsViewModel>()
               .ForMember(dest => dest.Cart, opt => opt.Ignore());

            CreateMap<UpdateOrderSalesItemsViewModel,UpdateOrderItemsViewModel>()
             .ForMember(dest => dest.Cart, opt => opt.Ignore());


            CreateMap<OrderFilterViewModel, OrderFilterViewModel>();

            CreateMap<CartItemsViewModel, CartItemFilterViewModel>()
                .ForMember(dest => dest.CuttingNameAR, opt => opt.MapFrom(src => src.Cutting.NameAR))
                .ForMember(dest => dest.CuttingNameEN, opt => opt.MapFrom(src => src.Cutting.NameEN))
                .ForMember(dest => dest.ItemCode, opt => opt.MapFrom(src => src.Item.Code))
                .ForMember(dest => dest.ItemNameAR, opt => opt.MapFrom(src => src.Item.NameAR))
                .ForMember(dest => dest.ItemNameEN, opt => opt.MapFrom(src => src.Item.NameEN))
                .ForMember(dest => dest.ItemPrice, opt => opt.MapFrom(src => src.Item.Price))
                .ForMember(dest => dest.ItemNetPrice, opt => opt.MapFrom(src => src.ItemNetPrice))
                .ForMember(dest => dest.CartItemPrice, opt => opt.MapFrom(src => src.Price))
                .ForMember(dest => dest.ItemQuantityByKG, opt => opt.MapFrom(src => src.ItemQuantityByKG))
                .ForMember(dest => dest.Measurment, opt => opt.MapFrom(src => src.MeasurmentUnit.Name))
                .ForMember(dest => dest.QuantityValue, opt => opt.MapFrom(src => src.QuantityValue));
               

            CreateMap<OrderViewModel, OrderFilterViewModel>()
                .ForMember(dest => dest.Status, opt => opt.ResolveUsing(BindOrderStatus))
                .ForMember(dest => dest.CartItems, opt => opt.MapFrom(src => src.Cart.CartItems));

            CreateMap<OrderFilterViewModel, OrderExcelFilterViewModel>()
               .ForMember(dest => dest.OrderId, opt => opt.MapFrom(src => src.Id))
               .ForMember(dest => dest.House, opt => opt.MapFrom(src => src.Building))
               .ForMember(dest => dest.CuttingName, opt => opt.Ignore())
               .ForMember(dest => dest.Governorate, opt => opt.MapFrom(src => src.GovernorateName))
               .ForMember(dest => dest.Judda, opt => opt.MapFrom(src => src.AreaName))
               .ForMember(dest => dest.Block, opt => opt.MapFrom(src => src.BlockName))
               .ForMember(dest => dest.Street, opt => opt.MapFrom(src => src.StreetName))
               .ForMember(dest => dest.Qty, opt => opt.Ignore())
               //.ForMember(dest => dest.ItemArabicName, opt => opt.Ignore())
               .ForMember(dest => dest.ItemName, opt => opt.Ignore())
               .ForMember(dest => dest.ItemPrice, opt => opt.Ignore())
               .ForMember(dest => dest.CartItemPrice, opt => opt.Ignore())
               .ForMember(dest => dest.ItemName, opt => opt.Ignore())
               .ForMember(dest => dest.But, opt => opt.Ignore());

            CreateMap<OrderFilterViewModel, OrderSalesExcelFilterViewModel>()
             .ForMember(dest => dest.OrderId, opt => opt.MapFrom(src => src.Id))
             .ForMember(dest => dest.House, opt => opt.MapFrom(src => src.Building))
             .ForMember(dest => dest.Governorate, opt => opt.MapFrom(src => src.GovernorateName))
             .ForMember(dest => dest.Judda, opt => opt.MapFrom(src => src.AreaName))
             .ForMember(dest => dest.Block, opt => opt.MapFrom(src => src.BlockName))
             .ForMember(dest => dest.Street, opt => opt.MapFrom(src => src.StreetName))
             .ForMember(dest => dest.Qty, opt => opt.Ignore())
             //.ForMember(dest => dest.ItemArabicName, opt => opt.Ignore())
             .ForMember(dest => dest.ItemName, opt => opt.Ignore())
             .ForMember(dest => dest.QuantityKG, opt => opt.Ignore())
             .ForMember(dest => dest.ItemPrice, opt => opt.Ignore())
             //.ForMember(dest => dest.CartItemPrice, opt => opt.Ignore())
             .ForMember(dest => dest.Measurment, opt => opt.Ignore())
             .ForMember(dest => dest.SalesPricePerItem, opt => opt.Ignore())
             .ForMember(dest => dest.IsSalesChangePrice, opt => opt.Ignore())
             .ForMember(dest => dest.But, opt => opt.Ignore());

            CreateMap<OrderViewModel, PayFromWallet>()
                  .ForMember(dest => dest.CompanyId, opt => opt.MapFrom(src => src.FK_Customer_Id))
                  .ForMember(dest => dest.OrderFees, opt => opt.MapFrom(src => src.Price))
                  .ForMember(dest => dest.OrderId, opt => opt.MapFrom(src => src.Id))
                  .ForAllOtherMembers(opt => opt.Ignore());

            CreateMap<OrderViewModel, SendMailViewModel>()
              //.ForMember(dest => dest.To, opt => opt.MapFrom(src => src.CustomerEmail))
              // .ForMember(dest => dest.Subject, opt => opt.MapFrom("Order Submission"))
              .ForAllOtherMembers(opt => opt.Ignore());
            CreateMap<OrderViewModel, SendSmsViewModel>()
             .ForMember(dest => dest.PhoneNumber, opt => opt.MapFrom(src => src.CustomerPhone))
             .ForAllOtherMembers(opt => opt.Ignore());




            CreateMap<AssignDriverToOrderViewModel, OrderViewModel>()
            .ForMember(dest => dest.FK_Driver_Id, opt => opt.MapFrom(src => src.FK_Driver_Id))
            .ForMember(dest => dest.DriverName, opt => opt.MapFrom(src => src.DriverName))
            .ForMember(dest => dest.OrderTypeObject, opt => opt.ResolveUsing(BindOrderType))
             .ForAllOtherMembers(opt => opt.Ignore());

            CreateMap<AssignProducerToOrderViewModel, OrderViewModel>()
           .ForMember(dest => dest.FK_Producer_Id, opt => opt.MapFrom(src => src.FK_Producer_Id))
           .ForMember(dest => dest.ProducerName, opt => opt.MapFrom(src => src.ProducerName))
           .ForMember(dest => dest.OrderTypeObject, opt => opt.ResolveUsing(BindOrderType))
            .ForAllOtherMembers(opt => opt.Ignore());

            CreateMap<Order, OrderViewModel>()
                .ForMember(dest => dest.PaymentType, opt => opt.Ignore())
                .ForMember(dest => dest.PaymentURL, opt => opt.Ignore())
                .ForMember(dest => dest.Cart, opt => opt.Ignore())
                .ForMember(dest => dest.OrderTypeName, opt => opt.Ignore())
            .ForMember(dest => dest.OrderTypeObject, opt => opt.ResolveUsing(BindOrderType))
                .ForMember(dest => dest.Customer, opt => opt.Ignore());

            CreateMap<OrderViewModel, Order>()
            .ForMember(e => e.FK_CreatedBy_Id, c => c.Ignore())
            .ForMember(e => e.FK_UpdatedBy_Id, c => c.Ignore())
            .ForMember(e => e.FK_DeletedBy_Id, c => c.Ignore())
            .ForMember(e => e.CreatedDate, opt => opt.MapFrom(src =>src.CreatedDate))
            .ForMember(e => e.UpdatedDate, c => c.Ignore())
            .ForMember(e => e.DeletedDate, c => c.Ignore())
            .ForMember(e => e.IsDeleted, c => c.Ignore())
            .ForMember(e => e.CurrentUserId, c => c.Ignore());
            //.IgnoreBaseEntityProperties();

            CreateMap<LKP_CityFees, LKP_CityFeesViewModel>();
            CreateMap<LKP_CityFeesViewModel, LKP_CityFees>()
                .IgnoreBaseEntityProperties();


            //CreateMap<LKP_OrderType, LKP_OrderTypeViewModel>();
            //CreateMap<LKP_OrderTypeViewModel, LKP_OrderType>()
            //    .IgnoreBaseEntityProperties();

            CreateMap<Role_Order_Status, RoleOrderStatusViewModel>();
            CreateMap<RoleOrderStatusViewModel, Role_Order_Status>()
                .IgnoreBaseEntityProperties();

            CreateMap<Order, PaymentStatusViewModel>()
            .ForMember(dest => dest.IsPaymentProcessed, opt => opt.MapFrom(src => src.IsPaymentProcessed))
            .ForMember(dest => dest.IsPaymentSucceeded, opt => opt.MapFrom(src => src.IsPaymentSucceeded))
            .ForMember(dest => dest.PaymentMessage, opt => opt.MapFrom(src => src.PaymentMessage))
            .ForMember(dest => dest.Id, opt => opt.MapFrom(src => src.Id))
             .ForAllOtherMembers(opt => opt.Ignore());
            CreateMap<PaymentStatusViewModel, Order>()
            .ForMember(dest => dest.IsPaymentProcessed, opt => opt.MapFrom(src => src.IsPaymentProcessed))
            .ForMember(dest => dest.IsPaymentSucceeded, opt => opt.MapFrom(src => src.IsPaymentSucceeded))
            .ForMember(dest => dest.PaymentMessage, opt => opt.MapFrom(src => src.PaymentMessage))
            .ForMember(dest => dest.Id, opt => opt.MapFrom(src => src.Id))
            .ForAllOtherMembers(opt => opt.Ignore());

            CreateMap<CustomerViewModel, OrderViewModel>()
             .ForMember(dest => dest.CustomerEmail, opt => opt.MapFrom(src => src.Email))
             .ForMember(dest => dest.CustomerPhone, opt => opt.MapFrom(src => src.Phone1))
             .ForMember(dest => dest.CustomerName, opt => opt.ResolveUsing(BindCustomerName))
            .ForMember(dest => dest.OrderTypeObject, opt => opt.ResolveUsing(BindOrderType))
             .ForAllOtherMembers(opt => opt.Ignore());

            CreateMap<CompanyViewModel, OrderViewModel>()
            .ForMember(dest => dest.CustomerEmail, opt => opt.MapFrom(src => src.Email))
            .ForMember(dest => dest.CustomerPhone, opt => opt.MapFrom(src => src.Phone1))
            .ForMember(dest => dest.CustomerName, opt => opt.ResolveUsing(BindCompanyName))
            .ForMember(dest => dest.OrderTypeObject, opt => opt.ResolveUsing(BindOrderType))
            .ForAllOtherMembers(opt => opt.Ignore());




            CreateMap<OrderViewModel, CartOrderViewModel>()
             .ForMember(dest => dest.FK_Cart_Id, opt => opt.MapFrom(src => src.FK_Cart_Id))
             .ForMember(dest => dest.FK_Order_Id, opt => opt.MapFrom(src => src.Id))
             .ForMember(dest => dest.Cart, opt => opt.Ignore());
            CreateMap<CartOrderViewModel, OrderViewModel>()
            .ForMember(dest => dest.FK_Cart_Id, opt => opt.MapFrom(src => src.FK_Cart_Id))
            .ForMember(dest => dest.Id, opt => opt.MapFrom(src => src.FK_Order_Id))
            .ForMember(dest => dest.OrderTypeObject, opt => opt.ResolveUsing(BindOrderType))
            .ForAllOtherMembers(opt => opt.Ignore());

            CreateMap<LKP_OrderStatus, LKP_OrderStatusViewModel>();
            CreateMap<LKP_OrderStatusViewModel, LKP_OrderStatus>()
                .IgnoreBaseEntityProperties();


            CreateMap<LocationViewModel, OrderViewModel>()
                    .ForMember(dest => dest.Fk_AddressType_Id, opt => opt.MapFrom(src => src.Fk_AddressType_Id))
                    .ForMember(dest => dest.Number, opt => opt.MapFrom(src => src.Number))
                    .ForMember(dest => dest.Fk_Country_Id, opt => opt.MapFrom(src => src.Fk_Country_Id))
                    .ForMember(dest => dest.Name, opt => opt.MapFrom(src => src.Name))
                    .ForMember(dest => dest.GovernorateName, opt => opt.MapFrom(src => src.GovernorateName))
                    .ForMember(dest => dest.GovernorateId, opt => opt.MapFrom(src => src.GovernorateId))
                    .ForMember(dest => dest.AreaName, opt => opt.MapFrom(src => src.AreaName))
                    .ForMember(dest => dest.AreaId, opt => opt.MapFrom(src => src.AreaId))
                    .ForMember(dest => dest.BlockName, opt => opt.MapFrom(src => src.BlockName))
                    .ForMember(dest => dest.BlockId, opt => opt.MapFrom(src => src.BlockId))
                    .ForMember(dest => dest.StreetId, opt => opt.MapFrom(src => src.StreetId))
                    .ForMember(dest => dest.StreetName, opt => opt.MapFrom(src => src.StreetName))
                    .ForMember(dest => dest.Building, opt => opt.MapFrom(src => src.Building))
                    .ForMember(dest => dest.Floor, opt => opt.MapFrom(src => src.Floor))
                    .ForMember(dest => dest.AppartmentNumber, opt => opt.MapFrom(src => src.AppartmentNumber))
                    .ForMember(dest => dest.Latitude, opt => opt.MapFrom(src => src.Latitude))
                    .ForMember(dest => dest.Longitude, opt => opt.MapFrom(src => src.Longitude))
                    .ForMember(dest => dest.LocationDeliveryNote, opt => opt.MapFrom(src => src.DeliveryNote))
                    .ForMember(dest => dest.OrderTypeObject, opt => opt.ResolveUsing(BindOrderType))

                    //.ForMember(dest => dest.FK_Customer_Id, opt => opt.MapFrom(src => src.Fk_Customer_Id))
                    .ForAllOtherMembers(r => r.Ignore());

            CreateMap<CartViewModel, OrderViewModel>()
                .ForMember(dest => dest.FK_Cart_Id, opt => opt.MapFrom(src => src.Id))
                .ForMember(dest => dest.CartPrice, opt => opt.MapFrom(src => src.Price))
                .ForMember(dest => dest.CartVAT, opt => opt.MapFrom(src => src.VAT))
                .ForMember(dest => dest.OrderTypeObject, opt => opt.ResolveUsing(BindOrderType))

               .ForAllOtherMembers(r => r.Ignore());


            CreateMap<PaymentMethodViewModel, OrderViewModel>()
                //.ForMember(dest => dest.FK_PaymentType_Id, opt => opt.MapFrom(src => src.Id))
                .ForMember(dest => dest.PaymentTypeCode, opt => opt.MapFrom(src => src.NameEN))
                .ForMember(dest => dest.PaymentType, opt => opt.Ignore())
                .ForMember(dest => dest.OrderTypeObject, opt => opt.ResolveUsing(BindOrderType))

               .ForAllOtherMembers(r => r.Ignore());

            CreateMap<UpdatePaymentOrderViewModel, OrderViewModel>()
              .ForMember(dest => dest.TransactionNo, opt => opt.MapFrom(src => src.TransactionNo))
              .ForMember(dest => dest.TransactionCode, opt => opt.MapFrom(src => src.TransactionCode))
              .ForMember(dest => dest.ReciptNo, opt => opt.MapFrom(src => src.ReciptNo))
              .ForMember(dest => dest.CardType, opt => opt.MapFrom(src => src.CardType))
              .ForMember(dest => dest.IsPaymentSucceeded, opt => opt.MapFrom(src => src.IsPaymentSucceeded))
              .ForMember(dest => dest.PaymentMessage, opt => opt.MapFrom(src => src.PaymentMessage))
              .ForMember(dest => dest.IsPaymentProcessed, opt => opt.MapFrom(src => src.IsPaymentProcessed))
              .ForMember(dest => dest.OrderTypeObject, opt => opt.ResolveUsing(BindOrderType))

              .ForAllOtherMembers(r => r.Ignore());

            CreateMap<UpdatePaymentOrderViewModel, Order>()
            .ForMember(dest => dest.TransactionNo, opt => opt.MapFrom(src => src.TransactionNo))
            .ForMember(dest => dest.TransactionCode, opt => opt.MapFrom(src => src.TransactionCode))
            .ForMember(dest => dest.ReciptNo, opt => opt.MapFrom(src => src.ReciptNo))
            .ForMember(dest => dest.CardType, opt => opt.MapFrom(src => src.CardType))
            .ForMember(dest => dest.IsPaymentSucceeded, opt => opt.MapFrom(src => src.IsPaymentSucceeded))
            .ForMember(dest => dest.PaymentMessage, opt => opt.MapFrom(src => src.PaymentMessage))
            .ForMember(dest => dest.IsPaymentProcessed, opt => opt.MapFrom(src => src.IsPaymentProcessed))
            .ForAllOtherMembers(r => r.Ignore());

            CreateMap<OrderViewModel, PaymentURLViewModel>()
             .ForMember(dest => dest.Amount, opt => opt.MapFrom(src => src.Price))
             .ForMember(dest => dest.OrderId, opt => opt.MapFrom(src => src.Id))
            .ForAllOtherMembers(r => r.Ignore());



            CreateMap<DonationViewModel, LocationViewModel>()
             //.ForMember(dest => dest.Name, opt => opt.MapFrom(src => src.NameEN))
             .ForMember(dest => dest.Fk_Customer_Id, opt => opt.Ignore());

            CreateMap<LocationViewModel, DonationViewModel>()
             .ForMember(dest => dest.Phone, opt => opt.Ignore())
            .ForMember(dest => dest.NameAR, opt => opt.Ignore())
            .ForMember(dest => dest.NameEN, opt => opt.Ignore())
             .ForMember(dest => dest.IsSystemKey, opt => opt.Ignore());




            CreateMap<EidDates, BaseDatesViewModel>();
            CreateMap<BaseDatesViewModel, EidDates>()
             .IgnoreBaseEntityProperties();

            CreateMap<PickUpDates, BaseDatesViewModel>();
            CreateMap<BaseDatesViewModel, PickUpDates>()
             .IgnoreBaseEntityProperties();

            CreateMap<DonationDates, BaseDatesViewModel>();
            CreateMap<BaseDatesViewModel, DonationDates>()
             .IgnoreBaseEntityProperties();

            CreateMap<BaseDates, BaseDatesViewModel>();
            CreateMap<BaseDatesViewModel, BaseDates>()
              .IgnoreBaseEntityProperties();

            CreateMap<PickUpViewModel, LocationViewModel>()
            // .ForMember(dest => dest.Name, opt => opt.MapFrom(src => src.NameEN))
            .ForMember(dest => dest.Fk_Customer_Id, opt => opt.Ignore());

            CreateMap<LocationViewModel, PickUpViewModel>()
            .ForMember(dest => dest.Phone, opt => opt.Ignore())
            .ForMember(dest => dest.NameAR, opt => opt.Ignore())
            .ForMember(dest => dest.NameEN, opt => opt.Ignore())
             .ForMember(dest => dest.IsSystemKey, opt => opt.Ignore());


            CreateMap<Donation, Location>()
                .ForMember(dest => dest.Fk_Customer_Id, opt => opt.Ignore())
                .ForMember(dest => dest.Name, opt => opt.Ignore())
               .IgnoreBaseEntityProperties();
            CreateMap<Location, Donation>()
                 .ForMember(dest => dest.Phone, opt => opt.Ignore())
                .ForMember(dest => dest.NameAR, opt => opt.Ignore())
                .ForMember(dest => dest.NameEN, opt => opt.Ignore())
                .ForMember(dest => dest.IsSystemKey, opt => opt.Ignore())
            .ForMember(dest => dest.Name, opt => opt.MapFrom(src => src.Id));
            CreateMap<PickUp, Location>()
                .ForMember(dest => dest.Fk_Customer_Id, opt => opt.Ignore())
                .ForMember(dest => dest.Name, opt => opt.Ignore())
                .IgnoreBaseEntityProperties();
            CreateMap<Location, PickUp>()
                .ForMember(dest => dest.Phone, opt => opt.Ignore())
                .ForMember(dest => dest.NameAR, opt => opt.Ignore())
                .ForMember(dest => dest.NameEN, opt => opt.Ignore())
                .ForMember(dest => dest.IsSystemKey, opt => opt.Ignore());


            CreateMap<DonationViewModel, Donation>()
             .IgnoreBaseEntityProperties();
            CreateMap<Donation, DonationViewModel>();

            CreateMap<LocationTypeViewModel, CategoryInputDeliveryOptions>()
            .ForMember(dest => dest.DeliveryId, opt => opt.MapFrom(src => src.DeliveryType))
            .ForMember(dest => dest.FK_CategoryConfig_Id, opt => opt.MapFrom(src => src.FK_Config_Id))
            .ForAllOtherMembers(opt => opt.Ignore());
            CreateMap<CategoryInputDeliveryOptions, LocationTypeViewModel>()
           .ForMember(dest => dest.DeliveryType, opt => opt.MapFrom(src => src.DeliveryId))
           .ForMember(dest => dest.FK_Config_Id, opt => opt.MapFrom(src => src.FK_CategoryConfig_Id))
           .ForAllOtherMembers(opt => opt.Ignore());

            CreateMap<PickUpViewModel, PickUp>()
              .IgnoreBaseEntityProperties();
            CreateMap<PickUp, PickUpViewModel>();



            CreateMap<OrderViewModel, PromoCodeValidation>()
            .ForMember(dest => dest.PromoCode, opt => opt.MapFrom(src => src.PromoCode))
            .ForMember(dest => dest.CustomerId, opt => opt.MapFrom(src => src.FK_Customer_Id))
            .ForMember(dest => dest.Value, opt => opt.MapFrom(src => src.CartPrice))
            .ForMember(dest => dest.IsUse, opt => opt.UseValue(true))
            .ForAllOtherMembers(opt => opt.Ignore());

            CreateMap<CompanyLocationViewModel, LocationViewModel>()
            .ForMember(dest => dest.Fk_Customer_Id, opt => opt.MapFrom(src => src.Fk_Company_Id));

            CreateMap<LocationViewModel, CompanyLocationViewModel>()
            .ForMember(dest => dest.Fk_Company_Id, opt => opt.MapFrom(src => src.Fk_Customer_Id));


            CreateMap<PromoResult, OrderViewModel>()
            .ForMember(dest => dest.Price, opt => opt.ResolveUsing(CalculateOrderPrice))
            .ForMember(dest => dest.FK_Promo_Id, opt => opt.MapFrom(src => src.FK_Promo_Id))
            .ForMember(dest => dest.PromoValue, opt => opt.MapFrom(src => src.Value))
            .ForMember(dest => dest.HasPromoCode, opt => opt.UseValue(true))
            .ForMember(dest => dest.OrderTypeObject, opt => opt.ResolveUsing(BindOrderType))
            .ForAllOtherMembers(opt => opt.Ignore());


            CreateMap<LKP_VATViewModel, OrderViewModel>()
            .ForMember(dest => dest.VATNameAR, opt => opt.MapFrom(src => src.NameAR))
            .ForMember(dest => dest.VATNameEN, opt => opt.MapFrom(src => src.NameEN))
            .ForMember(dest => dest.VATPercentage, opt => opt.MapFrom(src => src.Percentage))
            .ForMember(dest => dest.OrderTypeObject, opt => opt.ResolveUsing(BindOrderType))
            .ForAllOtherMembers(opt => opt.Ignore());


            CreateMap<KeyValuePair<DateTime, int>, OrdersCountPerDayViewModel>()
                .ForMember(dest => dest.Day, opt => opt.MapFrom(src => src.Key))
                .ForMember(dest => dest.OrdersCount, opt => opt.MapFrom(src => src.Value));

            CreateMap<KeyValuePair<DateTime, double>, PaymentPerDayViewModel>()
                .ForMember(dest => dest.Day, opt => opt.MapFrom(src => src.Key))
                .ForMember(dest => dest.TotalPayment, opt => opt.MapFrom(src => src.Value));
        }
        private double CalculateOrderPrice(PromoResult promo, OrderViewModel order)
        {
            return order.Price - promo.Value;
        }
        private string BindCustomerName(CustomerViewModel customer, OrderViewModel order)
        {
            string name = "";
            if (!string.IsNullOrEmpty(customer.FirstName))
            {
                name = customer.FirstName;
            }
            if (!string.IsNullOrEmpty(customer.LastName))
            {
                name = $"{name} {customer.LastName}";
            }
            return name;
        }

        private string BindOrderStatus(OrderViewModel order, OrderFilterViewModel orderFilter)
        {
            string status = "";
            if (order.OrderStatus!=null)
            {
                status = order.OrderStatus.NameEN;
            }
            return status;
        }

        private string BindCompanyName(CompanyViewModel company, OrderViewModel order)
        {
            string name = "";
            if (!string.IsNullOrEmpty(company.FirstName))
            {
                name = company.FirstName;
            }
            if (!string.IsNullOrEmpty(company.LastName))
            {
                name = $"{name} {company.LastName}";
            }
            return name;
        }
        private EnumEntity BindOrderType(object model, OrderViewModel orderViewModel)
        {
            EnumEntity result = null;
            if (orderViewModel.OrderType > 0)
            {
                result = EnumManager<OrderType>.GetEnumObject(orderViewModel.OrderType);
            }
            return result;
        }
    }
}
