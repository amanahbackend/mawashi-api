﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Dispatching.Ordering.Models.Settings
{
    public class RoleIds
    {
        public string CustomerServiceRoleId { get; set; }
        public string LogisticsRoleId { get; set; }
        public string ProducerRoleId { get; set; }
        public string DriverRoleId { get; set; }
    }
}
