﻿using Dispatching.Ordering.Models.Entities;
using System.Collections.Generic;

namespace Dispatching.Ordering.Models.Settings
{
    public class OrderAppSettings
    {
        public List<LKP_OrderStatus> SystemOrderStatus { get; set; }
        public List<Role_Order_Status> SystemRoleOrderStatus { get; set; }
        public RoleIds RoleIds { get; set; }
        
        public string IntialOrderStatusEN { get; set; }
        public string IntialOrderStatusAR { get; set; }
        public int IntialOrderStatusStepNo { get; set; }

        public string PaidOrderStatusEN { get; set; }
        public string PaidOrderStatusAR { get; set; }
        public int PaidOrderStatusStepNo { get; set; }
        public string DeliveredOrderStatusEN { get; set; }
        public string DeliveredOrderStatusAR { get; set; }
        public string CancelledOrderStatusEN { get; set; }
        public string CancelledOrderStatusAR { get; set; }
        public int DeliveredOrderStatusStepNo { get; set; }

        public bool IsFeesByCity { get; set; }
        public string DefaultFeesKeyEN { get; set; }
        public string DefaultFeesKeyAR { get; set; }
        public int IncrmentStepValue { get; set; }
        public string DefaultLanguage { get; set; }
        public string SMSTemplateMessage { get; set; }
        public string EmailTemplateMessage { get; set; }
        public string HotLine { get; set; }
        
    }
}
