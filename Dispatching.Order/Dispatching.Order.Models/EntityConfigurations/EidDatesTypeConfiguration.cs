﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using DispatchProduct.BuildingBlocks.BaseEntities.EntityConfigurations;
using Dispatching.Ordering.Models.Entities;

namespace Dispatching.Ordering.Models.EntitiesConfiguration
{
    public class EidDatesTypeConfiguration : BaseLKPEntityTypeConfiguration<EidDates>, IEntityTypeConfiguration<EidDates>
    {
        public new void Configure(EntityTypeBuilder<EidDates> builder)
        {
            base.Configure(builder);
        }
    }
}
