﻿using Dispatching.Ordering.Models.Entities;
using DispatchProduct.BuildingBlocks.BaseEntities.EntityConfigurations;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Dispatching.Ordering.Models.EntityConfigurations
{
    public class OrderEntityTypeConfiguration : BaseEntityTypeConfiguration<Order>,IEntityTypeConfiguration<Order>
    {
        public new void  Configure(EntityTypeBuilder<Order> CategoryConfiguration)
        {
            base.Configure(CategoryConfiguration);
            CategoryConfiguration.ToTable("Order");
            CategoryConfiguration.Property(o => o.Id).ValueGeneratedOnAdd();
            CategoryConfiguration.Property(o => o.TransactionNo).IsRequired(false);
            CategoryConfiguration.Property(o => o.CardType).IsRequired(false);
            CategoryConfiguration.Property(o => o.FK_Cart_Id).IsRequired();
            CategoryConfiguration.Property(o => o.ReciptNo).IsRequired(false);
           // CategoryConfiguration.Property(o => o.FK_PaymentType_Id).IsRequired(false);
            CategoryConfiguration.Property(o => o.FK_Order_Type_Id).IsRequired();
            CategoryConfiguration.Property(o => o.OrderType).IsRequired();
            CategoryConfiguration.Ignore(o => o.OrderStatus);
            CategoryConfiguration.Property(o => o.FK_Customer_Id).IsRequired();
            //CategoryConfiguration.Property(o => o.DeliveryDate).IsRequired(false);
            CategoryConfiguration.Property(o => o.DeliveryType).IsRequired();
            CategoryConfiguration.Property(o => o.FK_CityDeliveryFees_Id).IsRequired();
            CategoryConfiguration.Property(o => o.DeliveryFees).IsRequired();
            CategoryConfiguration.Property(o => o.CartPrice).IsRequired();
            CategoryConfiguration.Property(o => o.CartVAT).IsRequired();
            CategoryConfiguration.Property(o => o.Price).IsRequired();
            CategoryConfiguration.Property(o => o.Code).IsRequired();
            //CategoryConfiguration.Property(o => o.Fk_AddressType_Id).IsRequired();
            CategoryConfiguration.Property(o => o.Number).IsRequired(false);
            //CategoryConfiguration.Property(o => o.Fk_Country_Id).IsRequired(false);
            CategoryConfiguration.Property(o => o.Name).IsRequired(false);
            CategoryConfiguration.Property(o => o.GovernorateName).IsRequired(false);
            CategoryConfiguration.Property(o => o.AreaName).IsRequired(false);
            CategoryConfiguration.Property(o => o.BlockName).IsRequired(false);
            CategoryConfiguration.Property(o => o.StreetName).IsRequired(false);
            CategoryConfiguration.Property(o => o.Building).IsRequired(false);
            CategoryConfiguration.Property(o => o.Floor).IsRequired(false);
            CategoryConfiguration.Property(o => o.AppartmentNumber).IsRequired(false);
            CategoryConfiguration.Property(o => o.Latitude).IsRequired(false);
            CategoryConfiguration.Property(o => o.Longitude).IsRequired(false);
            CategoryConfiguration.Property(o => o.DeliveryNote).IsRequired(false);
        }
    }
}
