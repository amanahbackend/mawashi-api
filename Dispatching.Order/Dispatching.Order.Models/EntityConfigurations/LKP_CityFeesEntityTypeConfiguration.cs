﻿using Dispatching.Ordering.Models.Entities;
using DispatchProduct.BuildingBlocks.BaseEntities.EntityConfigurations;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Dispatching.Ordering.Models.EntityConfigurations
{
    public class LKP_CityFeesEntityTypeConfiguration : BaseEntityTypeConfiguration<LKP_CityFees>,IEntityTypeConfiguration<LKP_CityFees>
    {
        public new void  Configure(EntityTypeBuilder<LKP_CityFees> CategoryConfiguration)
        {
            base.Configure(CategoryConfiguration);

            CategoryConfiguration.ToTable("LKP_CityFees");
            CategoryConfiguration.Property(o => o.Id).ValueGeneratedOnAdd();
            CategoryConfiguration.Property(o => o.Country).IsRequired();
            CategoryConfiguration.Property(o => o.Code).IsRequired();
            CategoryConfiguration.Property(o => o.DeliveryFees).IsRequired();
    }
    }
}
