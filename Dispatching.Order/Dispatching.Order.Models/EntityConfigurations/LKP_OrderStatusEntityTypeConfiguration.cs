﻿using Dispatching.Ordering.Models.Entities;
using DispatchProduct.BuildingBlocks.BaseEntities.EntityConfigurations;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Dispatching.Ordering.Models.EntityConfigurations
{
    public class LKP_OrderStatusEntityTypeConfiguration : BaseEntityTypeConfiguration<LKP_OrderStatus>,IEntityTypeConfiguration<LKP_OrderStatus>
    {
        public new void  Configure(EntityTypeBuilder<LKP_OrderStatus> CategoryConfiguration)
        {
            base.Configure(CategoryConfiguration);
            CategoryConfiguration.ToTable("LKP_OrderStatus");
            CategoryConfiguration.Property(o => o.Id).ValueGeneratedOnAdd();
    }
    }
}
