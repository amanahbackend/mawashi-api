﻿using Dispatching.Ordering.Models.Entities;
using DispatchProduct.BuildingBlocks.BaseEntities.EntityConfigurations;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Dispatching.Ordering.Models.EntityConfigurations
{
    public class Role_Order_StatusEntityTypeConfiguration : BaseEntityTypeConfiguration<Role_Order_Status>,IEntityTypeConfiguration<Role_Order_Status>
    {
        public new void  Configure(EntityTypeBuilder<Role_Order_Status> CategoryConfiguration)
        {
            base.Configure(CategoryConfiguration);
            CategoryConfiguration.ToTable("Role_Order_Status");
            CategoryConfiguration.Property(o => o.Id).ValueGeneratedOnAdd();
            CategoryConfiguration.Ignore(o => o.LKP_OrderStatus);
    }
    }
}
