﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using DispatchProduct.BuildingBlocks.BaseEntities.EntityConfigurations;
using Dispatching.Ordering.Models.Entities;

namespace Dispatching.Ordering.Models.EntitiesConfiguration
{
    public class DonationDatesTypeConfiguration : BaseLKPEntityTypeConfiguration<DonationDates>, IEntityTypeConfiguration<DonationDates>
    {
        public new void Configure(EntityTypeBuilder<DonationDates> builder)
        {
            base.Configure(builder);



        }
    }
}
