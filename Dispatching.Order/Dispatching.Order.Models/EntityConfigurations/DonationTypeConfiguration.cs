﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using DispatchProduct.BuildingBlocks.BaseEntities.EntityConfigurations;
using Dispatching.Ordering.Models.Entities;

namespace Dispatching.Ordering.Models.EntitiesConfiguration
{
    public class DonationTypeConfiguration : BaseLKPEntityTypeConfiguration<Donation>, IEntityTypeConfiguration<Donation>
    {
        public new void Configure(EntityTypeBuilder<Donation> builder)
        {
            base.Configure(builder);
            base.Configure(builder);
            builder.ToTable("Donation");
            //builder.Property(l => l.Fk_Country_Id).IsRequired(false);
            builder.Property(l => l.AppartmentNumber).IsRequired(false);
            builder.Property(l => l.AreaName).IsRequired(false);
            builder.Property(l => l.BlockName).IsRequired(false);
            builder.Property(l => l.Building).IsRequired(false);
            //builder.Property(l => l.Fk_AddressType_Id).IsRequired(false);
            builder.Property(l => l.Floor).IsRequired(false);
            builder.Property(l => l.GovernorateName).IsRequired(false);
            builder.Property(l => l.Latitude).IsRequired(false);
            builder.Property(l => l.Longitude).IsRequired(false);
            builder.Property(l => l.Number).IsRequired(false);
            builder.Property(l => l.StreetName).IsRequired(false);
            builder.Ignore(l => l.Name);
        }
    }
}
