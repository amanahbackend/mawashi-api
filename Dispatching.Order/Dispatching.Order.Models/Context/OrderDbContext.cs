﻿using Dispatching.Ordering.Models.Entities;
using Dispatching.Ordering.Models.EntitiesConfiguration;
using Dispatching.Ordering.Models.EntityConfigurations;
using Microsoft.EntityFrameworkCore;

namespace Dispatching.Ordering.Models.Context
{
    public class OrderDbContext : DbContext
    {
        public DbSet<LKP_CityFees> CityFees { get; set; }
        //public DbSet<LKP_OrderType> OrderType { get; set; }
        public DbSet<LKP_OrderStatus> OrderStatus { get; set; }
        public DbSet<Order> Order { get; set; }
        public DbSet<Role_Order_Status> RoleOrderStatus { get; set; }
        public DbSet<Donation> Donation { get; set; }
        public DbSet<DonationDates> DonationDates { get; set; }
        public DbSet<EidDates> EidDates { get; set; }
        public DbSet<PickUp> PickUp { get; set; }
        public DbSet<PickUpDates> PickUpDates { get; set; }

        public OrderDbContext(DbContextOptions<OrderDbContext> options)
            : base(options)
        {
            


        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.ApplyConfiguration(new LKP_CityFeesEntityTypeConfiguration());
            //modelBuilder.ApplyConfiguration(new LKP_OrderTypeEntityTypeConfiguration());
            modelBuilder.ApplyConfiguration(new LKP_OrderStatusEntityTypeConfiguration());
            modelBuilder.ApplyConfiguration(new OrderEntityTypeConfiguration());
            modelBuilder.ApplyConfiguration(new OrderEntityTypeConfiguration());
            modelBuilder.ApplyConfiguration(new DonationTypeConfiguration());
            modelBuilder.ApplyConfiguration(new DonationDatesTypeConfiguration());
            modelBuilder.ApplyConfiguration(new EidDatesTypeConfiguration());
            modelBuilder.ApplyConfiguration(new PickUpDatesTypeConfiguration());
            modelBuilder.ApplyConfiguration(new PickUpTypeConfiguration());
        }
    }
}
