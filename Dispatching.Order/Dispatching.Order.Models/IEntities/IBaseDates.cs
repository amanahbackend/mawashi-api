﻿using DispatchProduct.BuildingBlocks.BaseEntities.Entities;
using DispatchProduct.BuildingBlocks.BaseEntities.IEntities;
using System;
using System.Collections.Generic;
using System.Text;

namespace Dispatching.Ordering.Models.Entities
{
    public interface IBaseDates : IBaseLKPEntity
    {
        DateTime Date { get; set; }
    }
}
