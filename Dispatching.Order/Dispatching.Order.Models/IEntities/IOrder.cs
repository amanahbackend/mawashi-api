﻿using CommonEnums;
using Dispatching.BuisnessCommon.Enums;
using Dispatching.Ordering.Models.Entities;
using DispatchProduct.BuildingBlocks.BaseEntities.IEntities;
using System;
using System.Collections.Generic;
using System.Text;

namespace Dispatching.Ordering.Models.IEntities
{
    public interface IOrder : IBaseEntity
    {
        string TransactionNo { get; set; }
        string CardType { get; set; }
        string TransactionCode { get; set; }
        string ReciptNo { get; set; }
        string CreatedUserName { get; set; }
        PaymentType PaymenMechanism { get; set; }
        //int FK_PaymentType_Id { get; set; }
        string PaymentTypeCode { get; set; }
        bool IsPaymentProcessed { get; set; }
        string PaymentMessage { get; set; }
        bool IsPaymentSucceeded { get; set; }
        int FK_Cart_Id { get; set; }
        int FK_Order_Type_Id { get; set; }
        int FK_Order_Status_Id { get; set; }
        int FK_Customer_Id { get; set; }
        DateTime DeliveryDate { get; set; }
        DeliveryType FK_DeliveryType_Id { get; set; }
        string DeliveryType { get; set; }
        OrderType OrderType { get; set; }
        LKP_OrderStatus OrderStatus { get; set; }
        int FK_CityDeliveryFees_Id { get; set; }
        double DeliveryFees { get; set; }
        double CartPrice { get; set; }
        double CartVAT { get; set; }
        double Price { get; set; }
        double PriceExcludedVat { get; set; }
        string VATNameAR { get; set; }
        string VATNameEN { get; set; }
        double VATValue { get; set; }
        string Code { get; set; }
        string PromoCode { get; set; }
        bool HasPromoCode { get; set; }
        int PromoValue { get; set; }
        int FK_Promo_Id { get; set; }
        Platform FK_Platform_Id { get; set; }
        string Platform { get; set; }
        string CustomerPhone { get; set; }
        string CustomerName { get; set; }
        string CustomerEmail { get; set; }
        string FK_Driver_Id { get; set; }
        string DriverName { get; set; }
        string FK_Producer_Id { get; set; }
        string ProducerName { get; set; }
        #region Location
        int Fk_Location_Id { get; set; }
        AddressType Fk_AddressType_Id { get; set; }
        string Number { get; set; }
        Country Fk_Country_Id { get; set; }
        string Name { get; set; }
        string GovernorateName { get; set; }
        string GovernorateId { get; set; }

        string AreaName { get; set; }
        string AreaId { get; set; }

        string BlockId { get; set; }
        string BlockName { get; set; }

        string StreetId { get; set; }
        string StreetName { get; set; }
        string Building { get; set; }
        string Floor { get; set; }
        string AppartmentNumber { get; set; }
        double? Latitude { get; set; }
        double? Longitude { get; set; }
        string DeliveryNote { get; set; }
        string LocationDeliveryNote { get; set; }
        #endregion
    }
}
