﻿using Dispatching.Ordering.Models.Entities;
using DispatchProduct.BuildingBlocks.BaseEntities.Entities;
using DispatchProduct.BuildingBlocks.BaseEntities.IEntities;
using System;
using System.Collections.Generic;
using System.Text;

namespace Dispatching.Ordering.Models.IEntities
{
    public interface IRole_Order_Status : IBaseEntity
    {
        string FK_Role_Id { get; set; }
        string RoleName { get; set; }
        int FK_Order_Status_Id { get; set; }
        LKP_OrderStatus LKP_OrderStatus { get; set; }
    }
}
