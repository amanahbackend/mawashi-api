﻿using CommonEnums;
using DispatchProduct.BuildingBlocks.BaseEntities.Entities;
using DispatchProduct.BuildingBlocks.BaseEntities.IEntities;
using System;
using System.Collections.Generic;
using System.Text;

namespace Dispatching.Ordering.Models.Entities
{
    public interface IDonation : IBaseLKPEntity
    {
         AddressType Fk_AddressType_Id { get; set; }
         string Number { get; set; }
         Country Fk_Country_Id { get; set; }
         string Name { get; set; }
         string GovernorateName { get; set; }
         string GovernorateId { get; set; }

         string AreaName { get; set; }
         string AreaId { get; set; }

         string BlockId { get; set; }
         string BlockName { get; set; }

         string StreetId { get; set; }
         string StreetName { get; set; }

         string Building { get; set; }
         string Floor { get; set; }
         string AppartmentNumber { get; set; }
         double? Latitude { get; set; }
         double? Longitude { get; set; }
         string DeliveryNote { get; set; }
         string Phone { get; set; }
    }
}
