﻿using Dispatching.Ordering.Models.IEntities;
using DispatchProduct.BuildingBlocks.BaseEntities.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace Dispatching.Ordering.Models.Entities
{
    public class Role_Order_Status: BaseEntity, IRole_Order_Status
    {
        public string FK_Role_Id { get; set; }
        public string RoleName { get; set; }
        public int FK_Order_Status_Id { get; set; }
        public LKP_OrderStatus LKP_OrderStatus { get; set; }
    }
}
