﻿using DispatchProduct.BuildingBlocks.BaseEntities.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace Dispatching.Ordering.Models.Entities
{
    public class BaseDates : BaseLKPEntity
    {
        public DateTime Date { get; set; }
    }
}
