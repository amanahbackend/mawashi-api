﻿using Dispatching.Ordering.Models.IEntities;
using DispatchProduct.BuildingBlocks.BaseEntities.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace Dispatching.Ordering.Models.Entities
{
    public class LKP_OrderStatus:BaseLKPEntity, ILKP_OrderStatus
    {
        public int StepNo { get; set; }
    }
}
