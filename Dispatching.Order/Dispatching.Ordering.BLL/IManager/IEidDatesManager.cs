﻿using Dispatching.Ordering.Models.Entities;
using DispatchProduct.RepositoryModule;
using System;
using System.Collections.Generic;
using System.Text;
using Utilites.ProcessingResult;

namespace Dispatching.Ordering.BLL.IManagers
{
    public interface IEidDatesManager : IRepository<EidDates>
    {
    }
}
