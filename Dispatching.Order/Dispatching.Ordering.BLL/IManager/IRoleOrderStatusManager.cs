﻿using System;
using System.Collections.Generic;
using DispatchProduct.RepositoryModule;
using System.Text;
using Utilites.ProcessingResult;
using Dispatching.Ordering.Models.Entities;

namespace Dispatching.Ordering.BLL.IManagers
{
    public interface IRoleOrderStatusManager : IRepository<Role_Order_Status>
    {
        ProcessResult<List<int>> GetStatusByRoleId(string roleId);
    }
}
