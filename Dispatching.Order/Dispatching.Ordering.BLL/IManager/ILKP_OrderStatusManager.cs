﻿using System;
using System.Collections.Generic;
using DispatchProduct.RepositoryModule;
using System.Text;
using Utilites.ProcessingResult;
using Dispatching.Ordering.Models.Entities;

namespace Dispatching.Ordering.BLL.IManagers
{
    public interface ILKP_OrderStatusManager : IRepository<LKP_OrderStatus>
    {
        ProcessResult<LKP_OrderStatus> GetOrderStatus(string statusName);
        ProcessResult<LKP_OrderStatus> GetNextStaus(int statusId);
        ProcessResult<LKP_OrderStatus> GetOrderStatusByStepNo(int stepNo);
        List<int> GetStatusIdForLastStep();
    }
}
