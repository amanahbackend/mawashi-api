﻿using Dispatching.Ordering.Models.Entities;
using System;
using System.Collections.Generic;
using System.Text;
using Utilites.ProcessingResult;

namespace Dispatching.Ordering.BLL.Managers
{
    public interface IOrderWorkFlowManager
    {
        ProcessResult<Order> HandleWorkFlow(Order order);
    }
}
