﻿using System;
using System.Collections.Generic;
using DispatchProduct.RepositoryModule;
using System.Text;
using Utilites.ProcessingResult;
using Dispatching.Ordering.Models.Entities;
using Dispatching.Ordering.Models;
using Dispatching.BuisnessCommon.Enums;
using Utilites.PaginatedItems;
using System.Linq.Expressions;

namespace Dispatching.Ordering.BLL.IManagers
{
    public interface IOrderManager : IRepository<Order>
    {
        ProcessResult<string> CreateUniqueOrderCode(string cityName, OrderType orderType);
        ProcessResult<List<Order>> GetAllExcludeLastStep();
        ProcessResult<Order> Submit(Order order);
        ProcessResult<List<Order>> GetByOrderStatusId(int statusId);
        ProcessResult<int> GetOrderCountByCustomerId(int customerId);
        ProcessResult<List<Order>> GetOrdersByCustomerId(int customerId);
        ProcessResult<List<Order>> GetCompanyOrders(int companyId);
        ProcessResult<List<Order>> GetCompaniesOrders();
        ProcessResult<List<Order>> GetOrdersByRole(string roleId, OrderType type = 0, string driverId = null, string producerId = null);
        ProcessResult<LKP_OrderStatus> GetNextStaus(int orderId);
        ProcessResult<bool> UpdateOrderPaymentInfo(UpdatePaymentOrderViewModel model);
        ProcessResult<Order> Get(params object[] id);
        ProcessResult<Order> GetOrdersById(int orderId);
        ProcessResult<List<Order>> GetAllIndividalOrder();
        ProcessResult<List<Order>> GetAllRetailsOrder();
        ProcessResult<int> GetTodayOrdersCount();
        ProcessResult<double> GetTodayPayment();
        ProcessResult<int> GetTodayDeliveredOrdersCount();
        ProcessResult<int> GetOrdersCount(int daysNumber);
        ProcessResult<Dictionary<DateTime, int>> GetOrdersCountPerDay(int daysNumber);
        ProcessResult<Dictionary<DateTime, double>> GetPaymentPerDay(int daysNumber);
        ProcessResult<Order> UpdateOrderWalletPayment(Order model);
        ProcessResult<List<Order>> GetOrdersByCreatedUserId(string createdId);
        ProcessResult<List<Order>> GetOrdersByCreatedUserName(string createdUserName);
        ProcessResult<PaginatedItems<Order>> GetOrdersByRolePaginated(string roleId, PaginatedItems<Order> pagination = null, Expression<Func<Order, bool>> expression = null, OrderType type = 0, string driverId = null, string producerId = null);
        ProcessResult<PaginatedItems<Order>> GetAllExcludeLastStepPaginated(PaginatedItems<Order> pagination = null, Expression<Func<Order, bool>> filterExpression = null);
        ProcessResult<PaginatedItems<Order>> GetOrdersByCreatedUserNamePaginated(string createdUserName, PaginatedItems<Order> pagination = null, Expression<Func<Order, bool>> filterExpression = null);
    }
}
