﻿using System;
using System.Collections.Generic;
using DispatchProduct.RepositoryModule;
using System.Text;
using Utilites.ProcessingResult;
using Dispatching.Ordering.Models.Entities;

namespace Dispatching.Ordering.BLL.IManagers
{
    public interface ILKP_CityFeesManager : IRepository<LKP_CityFees>
    {
        ProcessResult<string> GetCityCode(string government);
        ProcessResult<double> GetCityDelivery(string government=null);
    }
}
