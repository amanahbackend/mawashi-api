﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using Utilites.ProcessingResult;
using Dispatching.Ordering.Models.Entities;
using Dispatching.Ordering.Models.Context;
using DispatchProduct.RepositoryModule;
using Dispatching.Ordering.BLL.IManagers;
using Dispatching.Ordering.Models.Settings;
using Microsoft.Extensions.Options;

namespace Dispatching.Ordering.BLL.Managers
{
    public class LKP_OrderStatusManager : Repository<LKP_OrderStatus>, ILKP_OrderStatusManager
    {
        IProcessResultMapper processResultMapper;
        IServiceProvider serviceProvider;
        OrderAppSettings appSetting;
        public LKP_OrderStatusManager(IOptions<OrderAppSettings> _appSetting, OrderDbContext context, IProcessResultMapper _processResultMapper, IServiceProvider _serviceProvider)
            : base(context)
        {
            _processResultMapper = processResultMapper;
            _serviceProvider = serviceProvider;
            appSetting = _appSetting.Value;
        }
        public ProcessResult<LKP_OrderStatus> GetOrderStatus(string statusName)
        {
            var status = GetAll().Data.Where(st => st.NameAR == statusName || st.NameEN == statusName).FirstOrDefault();
            if (status != null)
            {
                return ProcessResultHelper.Succedded(status);
            }
            else
            {
                return ProcessResultHelper.Failed<LKP_OrderStatus>(null, new Exception($"There isn't status with this this Name {statusName}"));
            }
        }
        public ProcessResult<LKP_OrderStatus> GetOrderStatusByStepNo(int stepNo)
        {
            var status = GetAllQuerable().Data.Where(st => st.StepNo == stepNo).FirstOrDefault();
            if (status != null)
            {
                return ProcessResultHelper.Succedded(status);
            }
            else
            {
                return ProcessResultHelper.Failed<LKP_OrderStatus>(null, new Exception($"There isn't status with this this Name {stepNo}"));
            }
        }
        public ProcessResult<LKP_OrderStatus> GetNextStaus(int statusId)
        {
            int? nextStepNo = null;
            ProcessResult<LKP_OrderStatus> result = null;
            LKP_OrderStatus nextStatus = null;
            LKP_OrderStatus currentStatus = null;

            if (statusId != 0)
            {
                currentStatus = GetAllQuerable().Data.Where(st => st.Id == statusId).FirstOrDefault();
                if (currentStatus != null && currentStatus.StepNo > 0)
                {
                    nextStepNo = currentStatus.StepNo + appSetting.IncrmentStepValue;
                    nextStatus = GetAllQuerable().Data.Where(st => st.StepNo == nextStepNo).FirstOrDefault();
                    if (nextStatus != null)
                    {
                        result = ProcessResultHelper.Succedded(nextStatus);
                    }
                    else
                    {
                        result = ProcessResultHelper.Failed<LKP_OrderStatus>(null, new Exception($" no next Status by step No Value: {nextStepNo}"));
                    }

                }
                else
                {
                    result = ProcessResultHelper.Failed<LKP_OrderStatus>(null, new Exception($"No Valid StatusId: {statusId}"));
                }
            }
            else
            {
                var step = GetAllQuerable().Data.Min(st => st.StepNo);
                nextStatus = GetAllQuerable().Data.Where(st => st.StepNo == step).FirstOrDefault();
                if (nextStatus != null)
                {
                    result = ProcessResultHelper.Succedded(nextStatus);
                }
                else
                {
                    result = ProcessResultHelper.Failed<LKP_OrderStatus>(null, new Exception($" no next Status by step No Value: {nextStepNo}"));
                }
            }

            return result;
        }
        public List<int> GetStatusIdForLastStep()
        {
            List<int> result= new List<int>();
            var maxStep = GetAllQuerable().Data.Max(st => st.StepNo);
            var ordrers = GetAllQuerable().Data.Where(status => status.StepNo == maxStep);
            if (ordrers != null)
            {
                result = ordrers.Select(r=>r.Id).ToList();
            }
            return result;
        }
    }
}
