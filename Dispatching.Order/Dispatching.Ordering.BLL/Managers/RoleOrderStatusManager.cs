﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using Utilites.ProcessingResult;
using Dispatching.Ordering.Models.Entities;
using Dispatching.Ordering.Models.Context;
using DispatchProduct.RepositoryModule;
using Dispatching.Ordering.BLL.IManagers;
using Utilites.UniqueKey;
using Microsoft.Extensions.DependencyInjection;
using CommonEnums;

namespace Dispatching.Ordering.BLL.Managers
{
    public class RoleOrderStatusManager : Repository<Role_Order_Status>, IRoleOrderStatusManager
    {
        IProcessResultMapper processResultMapper;
        IServiceProvider serviceProvider;
        public RoleOrderStatusManager(OrderDbContext context, IProcessResultMapper _processResultMapper, IServiceProvider _serviceProvider)
            : base(context)
        {
            processResultMapper = _processResultMapper;
            serviceProvider = _serviceProvider;
        }
        public ProcessResult<List<int>>GetStatusByRoleId(string roleId)
        {
            var statusIds=GetAllQuerable().Data.Where(r => r.FK_Role_Id == roleId).Select(r => r.FK_Order_Status_Id).ToList();
            return ProcessResultHelper.Succedded(statusIds);
        }
      

       
    }
}
