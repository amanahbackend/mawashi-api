﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using Utilites.ProcessingResult;
using Dispatching.Ordering.Models.Entities;
using Dispatching.Ordering.Models.Context;
using DispatchProduct.RepositoryModule;
using Dispatching.Ordering.BLL.IManagers;
using Microsoft.Extensions.Options;
using Dispatching.Ordering.Models.Settings;

namespace Dispatching.Ordering.BLL.Managers
{
    public class LKP_CityFeesManager : Repository<LKP_CityFees>, ILKP_CityFeesManager
    {
        IProcessResultMapper processResultMapper;
        IServiceProvider serviceProvider;
        OrderAppSettings orderSettings;
        public LKP_CityFeesManager(OrderDbContext context, IProcessResultMapper _processResultMapper, IServiceProvider _serviceProvider, IOptions<OrderAppSettings> _orderSettings)
            : base(context)
        {
            _processResultMapper = processResultMapper;
            _serviceProvider = serviceProvider;
            orderSettings = _orderSettings.Value;
        }
        public ProcessResult<string> GetCityCode(string government)
        {
            string data =null;
            ProcessResult<string> result = null;
            if (!string.IsNullOrEmpty(government))
            {
                var city = GetAll().Data.Where(c => c.NameAR == government || c.NameEN == government).FirstOrDefault();
                if (city != null)
                {
                    data = city.Code;
                    result = ProcessResultHelper.Succedded(data);
                }
                else
                {
                    if (government.Length > 3)
                    {
                        data = government.Substring(0, 3);
                        result = ProcessResultHelper.Succedded(data);
                    }
                    else
                    {
                        data = government;
                        result = ProcessResultHelper.Succedded(data);
                    }
                }
            }
            else
            {
                data = "NAN";
                result = ProcessResultHelper.Succedded(data);
                //result = ProcessResultHelper.Failed(data, new ArgumentOutOfRangeException($"government parameter is empty or null"));
            }
            return result;
        }
        public ProcessResult<double> GetCityDelivery(string government=null)
        {
            double data = 0;
            ProcessResult<double> result = null;
            LKP_CityFees city = null;
            if (orderSettings.IsFeesByCity)
            {
                 city = GetAll().Data.Where(c => c.NameAR == government || c.NameEN == government).FirstOrDefault();
            }
            else
            {
                 city = GetAll().Data.Where(c => c.NameAR == orderSettings.DefaultFeesKeyAR || c.NameEN == orderSettings.DefaultFeesKeyEN).FirstOrDefault();
            }
            if (city != null)
            {
                data = city.DeliveryFees;
                result = ProcessResultHelper.Succedded(data);
            }
            else
            {
                result = ProcessResultHelper.Failed(data, new ArgumentOutOfRangeException($"No Fees registered"));
            }
            return result;
        }
    }
}
