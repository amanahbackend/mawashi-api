﻿using Dispatching.Ordering.BLL.IManagers;
using Dispatching.Ordering.Models.Entities;
using System;
using System.Collections.Generic;
using System.Text;
using Utilites.ProcessingResult;
using Microsoft.Extensions.DependencyInjection;
namespace Dispatching.Ordering.BLL.Managers
{
    public class OrderWorkFlowManager : IOrderWorkFlowManager
    {
        IServiceProvider serviceProvider;
        public OrderWorkFlowManager(IServiceProvider _serviceProvider)
        {
            serviceProvider = _serviceProvider;
        }
        public ProcessResult<Order> HandleWorkFlow(Order order)
        {
            ProcessResult<Order> result = null;
            result = UpdateNextStatus(order);
            return result;
        }
        private ProcessResult<Order> UpdateNextStatus(Order order)
        {
            ProcessResult<Order> result = null;
            var orderStatus = OrderManager.GetNextStaus(order.Id);
            if (orderStatus.IsSucceeded && orderStatus.Data != null)
            {
                order.FK_Order_Status_Id = orderStatus.Data.Id;
                order.OrderStatus = orderStatus.Data;
                var updateResult = OrderManager.Update(order);
                if (updateResult.IsSucceeded)
                {
                    result = ProcessResultHelper.Succedded(order);
                }
                else
                {
                    result = ProcessResultHelper.MapToProcessResult(updateResult, result);
                }
            }
            else
            {
                result = ProcessResultHelper.Failed(order, new Exception("Next Status Not Configured"));
            }
            return result;
        }
        private IOrderManager OrderManager
        {
            get
            {
                return serviceProvider.GetService<IOrderManager>();
            }
        }

    }
}
