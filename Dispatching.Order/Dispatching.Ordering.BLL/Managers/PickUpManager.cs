﻿using Dispatching.Ordering.BLL.IManagers;
using Dispatching.Ordering.Models.Entities;
using System;
using System.Collections.Generic;
using System.Text;
using Dispatching.Ordering.Models.Context;
using DispatchProduct.RepositoryModule;
using Utilites.ProcessingResult;
using System.Linq;
using CommonEnums;

namespace Dispatching.Ordering.BLL.Managers
{
    public class PickUpManager : Repository<PickUp>, IPickUpManager
    {

        public PickUpManager(OrderDbContext context) : base(context)
        {
        }

        public override ProcessResult<PickUp> Add(PickUp entity)
        {
            entity = SetAddressType(entity);
            return base.Add(entity);
        }

        public override ProcessResult<bool> Update(PickUp entity)
        {
            entity = SetAddressType(entity);
            return base.Update(entity);
        }

        private PickUp SetAddressType(PickUp entity)
        {
            if (entity.Fk_Country_Id == Country.Kuwait)
            {
                if (!String.IsNullOrEmpty(entity.Number))
                {
                    entity.Fk_AddressType_Id = AddressType.Paci;
                }
            }
            else if (entity.Fk_Country_Id == Country.UAE)
            {
                if (!String.IsNullOrEmpty(entity.Number))
                {
                    entity.Fk_AddressType_Id = AddressType.Makani;
                }
            }
            return entity;
        }
    }
}
