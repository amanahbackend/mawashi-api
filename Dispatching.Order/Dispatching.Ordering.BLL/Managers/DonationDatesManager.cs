﻿using System;
using System.Collections.Generic;
using System.Text;
using DispatchProduct.RepositoryModule;
using Utilites.ProcessingResult;
using System.Linq;
using Dispatching.Ordering.Models.Entities;
using Dispatching.Ordering.BLL.IManagers;
using Dispatching.Ordering.Models.Context;

namespace Dispatching.Ordering.BLL.Managers
{
    public class DonationDatesManager : Repository<DonationDates>, IDonationDatesManager
    {
        public DonationDatesManager(OrderDbContext context) : base(context)
        {
        }
    }
}
