﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using Utilites.ProcessingResult;
using Dispatching.Ordering.Models.Entities;
using Dispatching.Ordering.Models.Context;
using DispatchProduct.RepositoryModule;
using Dispatching.Ordering.BLL.IManagers;
using Utilites.UniqueKey;
using Microsoft.Extensions.DependencyInjection;
using CommonEnums;
using Dispatching.Ordering.Models;
using AutoMapper;
using Microsoft.Extensions.Options;
using Dispatching.Ordering.Models.Settings;
using Dispatching.BuisnessCommon.Enums;
using System.Linq.Expressions;
using Utilites.PaginatedItems;

namespace Dispatching.Ordering.BLL.Managers
{
    public class OrderManager : Repository<Order>, IOrderManager
    {
        IProcessResultMapper processResultMapper;
        IServiceProvider serviceProvider;
        OrderAppSettings orderAppSettings;
        public OrderManager(OrderDbContext context, IProcessResultMapper _processResultMapper, IServiceProvider _serviceProvider,
             IOptions<OrderAppSettings> _orderSettings)
            : base(context)
        {
            processResultMapper = _processResultMapper;
            serviceProvider = _serviceProvider;
            orderAppSettings = _orderSettings.Value;
        }
        public ProcessResult<string> CreateUniqueOrderCode(string government, OrderType orderType)
        {
            string data = null;
            string codePart2 = "";
            var codePart1 = LKP_CityFeesManager.GetCityCode(government);
            if (orderType == OrderType.Individual)
            {
                codePart2 = KeyGenerator.RandomString(5);
                while (!IsOrderCodeExist(codePart2))
                {
                    codePart2 = KeyGenerator.RandomString(5);
                }
            }
            else
            {
                codePart2 = KeyGenerator.GetUniqueKey(5);
                while (!IsOrderCodeExist(codePart2))
                {
                    codePart2 = KeyGenerator.GetUniqueKey(5);
                }
            }
            if (codePart1 != null && codePart1.Data != null && codePart1.Data != null && codePart2 != null)
            {
                data = $"{codePart1.Data}-{codePart2}";
                return ProcessResultHelper.Succedded(data);
            }
            return ProcessResultHelper.Failed(data, new ArgumentOutOfRangeException($"No Code registered for {government} City"));
        }

        private ProcessResult<String> CreateUniqueOrderCode()
        {
            string codePart1 = KeyGenerator.GetUniqueKey(3);
            string codePart2 = KeyGenerator.GetUniqueKey(5);
            var code = $"{codePart1}-{codePart2}";
            while (!IsOrderCodeExist(code))
            {
                codePart1 = KeyGenerator.GetUniqueKey(3);
                codePart2 = KeyGenerator.GetUniqueKey(5);
                code = $"{codePart1}-{codePart2}";
            }
            return ProcessResultHelper.Succedded(code);
        }

        public ProcessResult<LKP_OrderStatus> GetNextStaus(int orderId)
        {
            var orderRes = Get(orderId);
            ProcessResult<LKP_OrderStatus> result = null;
            if (orderRes.IsSucceeded && orderRes.Data != null)
            {
                result = LKP_OrderStatusManager.GetNextStaus(orderRes.Data.FK_Order_Status_Id);
            }
            else
            {
                result = ProcessResultHelper.Failed<LKP_OrderStatus>(null, new Exception($"No Order Found By this Id {orderId}"));
            }
            return result;
        }
        private ILKP_CityFeesManager LKP_CityFeesManager
        {
            get { return serviceProvider.GetService<ILKP_CityFeesManager>(); }
        }
        private IMapper Mapper
        {
            get { return serviceProvider.GetService<IMapper>(); }
        }
        private IRoleOrderStatusManager RoleOrderStatusManager
        {
            get { return serviceProvider.GetService<IRoleOrderStatusManager>(); }
        }
        private IOrderWorkFlowManager OrderWorkFlowManager
        {
            get { return serviceProvider.GetService<IOrderWorkFlowManager>(); }
        }
        private ILKP_OrderStatusManager LKP_OrderStatusManager
        {
            get { return serviceProvider.GetService<ILKP_OrderStatusManager>(); }
        }
        public ProcessResult<Order> Submit(Order order)
        {
            ProcessResult<Order> result = null;
            ProcessResult<string> codeResult;
            if (order.Fk_Country_Id == Country.Kuwait)
            {
                codeResult = CreateUniqueOrderCode();
            }
            else
            {
                codeResult = CreateUniqueOrderCode(order.GovernorateName, order.OrderType);
            }
            Console.WriteLine($"orderCode {codeResult.Data}");

            var feesResult = LKP_CityFeesManager.GetCityDelivery(order.GovernorateName);
            Console.WriteLine($"feesResult {feesResult.Data}");

            if (codeResult != null && codeResult.IsSucceeded && feesResult != null && feesResult.IsSucceeded)
            {

                order.DeliveryFees = feesResult.Data;

                order = CalculateOrderPrice(order);
                order.Code = codeResult.Data;
                if (order.Id > 0)
                {
                    Console.WriteLine($"Try to Update {order.Id}");
                    string jsonString = Newtonsoft.Json.JsonConvert.SerializeObject(order);
                    Console.WriteLine(jsonString);
                    Update(order);
                    result = ProcessResultHelper.Succedded(order);

                }
                else
                {
                    Console.WriteLine($"Try to Add {order.Id}");
                    string jsonString = Newtonsoft.Json.JsonConvert.SerializeObject(order);
                    Console.WriteLine(jsonString);
                    result = Add(order);
                    if (result.IsSucceeded)
                    {
                        result = OrderWorkFlowManager.HandleWorkFlow(order);
                        if (order.PaymenMechanism == PaymentType.KuwaitCashOnDelivery || order.PaymenMechanism == PaymentType.UAECashOnDelivery)
                        {
                            result = OrderWorkFlowManager.HandleWorkFlow(order);
                        }
                    }
                }
            }
            else
            {
                result = ProcessResultHelper.Failed<Order>(null, new Exception($"can't get code or fees per this Governorate {order.GovernorateName}"));
            }
            return result;
        }
        public bool IsOrderCodeExist(string code)
        {
            bool result = false;
            var count = GetAllQuerable().Data.Where(ord => ord.Code == code).Count();
            if (count > 0)
            {
                result = true;
            }
            else
            {
                result = true;
            }
            return result;
        }
        public ProcessResult<List<Order>> GetByOrderStatusId(int statusId)
        {
            var data = GetAll().Data.Where(ord => ord.FK_Order_Status_Id == statusId).OrderByDescending(ord => ord.CreatedDate).ToList();
            return ProcessResultHelper.Succedded(data);
        }
        public ProcessResult<Order> UpdateCashPaymentInfo(Order order)
        {
            if (order.PaymentTypeCode == EnumManager<PaymentType>.GetName(PaymentType.KNet))
            {

            }
            else if (order.PaymentTypeCode == EnumManager<PaymentType>.GetName(PaymentType.MasterCard))
            {

            }
            else if (order.PaymentTypeCode == EnumManager<PaymentType>.GetName(PaymentType.KuwaitCashOnDelivery) ||
              order.PaymentTypeCode == EnumManager<PaymentType>.GetName(PaymentType.UAECashOnDelivery))
            {

            }
            return ProcessResultHelper.Succedded(order);
        }
        private Order CalculateOrderPrice(Order order)
        {
            // Condition order.FK_DeliveryType_Id != DeliveryType.PickUp added at 6/8/2018
            if (order.OrderType == OrderType.Individual /*&& order.FK_DeliveryType_Id != DeliveryType.PickUp*/)
            {
                order.PriceExcludedVat = order.CartPrice + order.DeliveryFees;
            }
            else
            {
                order.PriceExcludedVat = order.CartPrice;

            }
            var discountValue = (order.PriceExcludedVat * order.PromoValue) / 100;
            var priceAfterDiscount = order.PriceExcludedVat - discountValue;
            order.VATValue = (order.VATPercentage * priceAfterDiscount) / 100;
            if (order.Fk_Country_Id == Country.UAE && order.PaymenMechanism == PaymentType.MasterCard)
            {
                order.Price = Math.Truncate(priceAfterDiscount + order.VATValue);
            }
            else
            {
                order.Price = priceAfterDiscount + order.VATValue;
            }
            return order;
        }
        private ProcessResult<bool> IsReserved(string code)
        {
            ProcessResult<bool> result = null;
            var order = GetAll().Data.Where(c => c.Code == code).FirstOrDefault();
            if (order != null)
            {
                result = ProcessResultHelper.Succedded(true);
            }
            else
            {
                result = ProcessResultHelper.Succedded(false);
            }
            return result;
        }
        private List<Order> FillEnumProps(List<Order> orders)
        {
            foreach (var item in orders)
            {
                FillEnumProps(item);
            }
            return orders;
        }
        private Order FillEnumProps(Order order)
        {

            order.DeliveryType = (order.FK_DeliveryType_Id).ToString();
            order.Platform = (order.FK_Platform_Id).ToString();
            return order;
        }
        public ProcessResult<int> GetOrderCountByCustomerId(int customerId)
        {

            var orderCount = GetAllQuerable().Data.Where(ord => ord.FK_Customer_Id == customerId).Count();
            return ProcessResultHelper.Succedded(orderCount);

        }
        public override ProcessResult<List<Order>> GetAll()
        {
            var orders = base.GetAllQuerable().Data.OrderByDescending(ord => ord.CreatedDate).ToList();
            orders = BindStatus(orders);
            return ProcessResultHelper.Succedded(orders);

        }




        public ProcessResult<List<Order>> GetAllRetailsOrder()
        {
            var lastStatusIds = LKP_OrderStatusManager.GetStatusIdForLastStep();
            var orders = base.GetAllQuerable().Data.Where(ord => !lastStatusIds.Contains(ord.FK_Order_Status_Id) && ord.OrderType == OrderType.Retail).OrderByDescending(ord => ord.CreatedDate).ToList();
            orders = BindStatus(orders);
            return ProcessResultHelper.Succedded(orders);
        }
        public ProcessResult<List<Order>> GetAllIndividalOrder()
        {
            var orders = GetAllQuerable().Data.Where(ord => ord.OrderType == OrderType.Individual).OrderByDescending(ord => ord.CreatedDate).ToList();
            orders = BindStatus(orders);
            return ProcessResultHelper.Succedded(orders);
        }
        public ProcessResult<List<Order>> GetCompanyOrders(int companyId)
        {
            var data = GetAllQuerable().Data.Where(ord => ord.FK_Customer_Id == companyId && ord.OrderType == OrderType.Retail).OrderByDescending(ord => ord.CreatedDate).ToList();
            data = BindStatus(data);
            return ProcessResultHelper.Succedded(data);
        }
        public ProcessResult<List<Order>> GetCompaniesOrders()
        {
            var data = GetAllQuerable().Data.Where(ord => ord.OrderType == OrderType.Retail).OrderByDescending(ord => ord.CreatedDate).ToList();
            data = BindStatus(data);
            return ProcessResultHelper.Succedded(data);
        }
        public ProcessResult<List<Order>> GetOrdersByCustomerId(int customerId)
        {
            var orders = GetAllQuerable().Data.Where(ord => ord.FK_Customer_Id == customerId).OrderByDescending(ord => ord.CreatedDate).ToList();
            orders = BindStatus(orders);
            return ProcessResultHelper.Succedded(orders);
        }
        public ProcessResult<List<Order>> GetOrdersByCreatedUserId(string createdId)
        {
            var orderStatusRes = LKP_OrderStatusManager.GetOrderStatusByStepNo(orderAppSettings.PaidOrderStatusStepNo);
            if (orderStatusRes.IsSucceeded && orderStatusRes.Data != null)
            {
                var orders = GetAllQuerable().Data.Where
                    (ord => ord.FK_CreatedBy_Id == createdId &&
                    ord.FK_Order_Status_Id == orderStatusRes.Data.StepNo
                    ).OrderByDescending(ord => ord.CreatedDate).ToList();
                orders = BindStatus(orders);
                return ProcessResultHelper.Succedded(orders);
            }
            else
            {
                return ProcessResultHelper.MapToProcessResult<LKP_OrderStatus, List<Order>>(orderStatusRes);
            }
        }
        public ProcessResult<List<Order>> GetOrdersByCreatedUserName(string createdUserName)
        {
            var orderStatusRes = LKP_OrderStatusManager.GetOrderStatusByStepNo(orderAppSettings.PaidOrderStatusStepNo);
            if (orderStatusRes.IsSucceeded && orderStatusRes.Data != null)
            {
                var orders = GetAllQuerable().Data.Where
                    (
                    ord => ord.CreatedUserName == createdUserName &&
                    ord.FK_Order_Status_Id == orderStatusRes.Data.StepNo
                    ).OrderByDescending(ord => ord.CreatedDate).ToList();
                orders = BindStatus(orders);
                return ProcessResultHelper.Succedded(orders);
            }
            else
            {
                return ProcessResultHelper.MapToProcessResult<LKP_OrderStatus, List<Order>>(orderStatusRes);
            }
        }

        public ProcessResult<PaginatedItems<Order>> GetOrdersByCreatedUserNamePaginated(string createdUserName, PaginatedItems<Order> pagination = null, Expression<Func<Order, bool>> filterExpression = null)
        {
            var orderStatusRes = LKP_OrderStatusManager.GetOrderStatusByStepNo(orderAppSettings.PaidOrderStatusStepNo);
            if (orderStatusRes.IsSucceeded && orderStatusRes.Data != null)
            {
                Expression<Func<Order, bool>> predicate = PredicateBuilder.True<Order>();
                if (filterExpression != null)
                {
                    predicate = PredicateBuilder.And(predicate, filterExpression);
                }
                Expression<Func<Order, bool>> statusPredicate = ord => ord.CreatedUserName == createdUserName && ord.FK_Order_Status_Id == orderStatusRes.Data.StepNo;
                predicate = PredicateBuilder.And(predicate, statusPredicate);
                var orders = GetAllPaginated(pagination, predicate);
                //orders.Data.Data = orders.Data.Data.OrderByDescending(ord => ord.CreatedDate).ToList();
                //orders.Data.Data = BindStatus(orders.Data.Data);
                return orders;
            }
            else
            {
                return ProcessResultHelper.MapToProcessResult<LKP_OrderStatus, PaginatedItems<Order>>(orderStatusRes);
            }
        }

        public ProcessResult<PaginatedItems<Order>> GetAllExcludeLastStepPaginated(PaginatedItems<Order> pagination = null, Expression<Func<Order, bool>> filterExpression = null)
        {
            var lstLastStatus = LKP_OrderStatusManager.GetStatusIdForLastStep();
            Expression<Func<Order, bool>> predicate = PredicateBuilder.True<Order>();
            if (filterExpression != null)
            {
                predicate = PredicateBuilder.And(predicate, filterExpression);
            }
            Expression<Func<Order, bool>> statusPredicate = ord => !lstLastStatus.Contains(ord.FK_Order_Status_Id);
            predicate = PredicateBuilder.And(predicate, statusPredicate);
            var orders = GetAllPaginated(pagination, predicate);
            //orders.Data.Data = orders.Data.Data.OrderByDescending(ord => ord.CreatedDate).ToList();
            //orders.Data.Data = BindStatus(orders.Data.Data);
            return orders;
        }

        public ProcessResult<Order> GetOrdersById(int orderId)
        {
            var orderRes = Get();
            if (orderRes.IsSucceeded && orderRes.Data != null)
            {
                orderRes.Data = BindStatus(orderRes.Data);
                return ProcessResultHelper.Succedded(orderRes.Data);
            }
            else
            {
                return ProcessResultHelper.Failed(orderRes.Data, new Exception($"No Order Exist with this {orderId}"));
            }
        }
        public override ProcessResult<Order> Get(params object[] id)
        {
            var orderRes = base.Get(id);
            if (orderRes.IsSucceeded && orderRes.Data != null)
            {
                orderRes.Data = BindStatus(orderRes.Data);
            }
            else
            {
                return ProcessResultHelper.Failed<Order>(null, new Exception($"no order with this Id {id}"));
            }
            return orderRes;
        }
        private List<Order> BindStatus(List<Order> orders)
        {
            foreach (var order in orders)
            {
                var status = LKP_OrderStatusManager.Get(order.FK_Order_Status_Id);
                if (status.IsSucceeded && status.Data != null)
                {
                    order.OrderStatus = status.Data;
                }
            }
            return orders;
        }
        private Order BindStatus(Order order)
        {

            var status = LKP_OrderStatusManager.Get(order.FK_Order_Status_Id);
            if (status.IsSucceeded && status.Data != null)
            {
                order.OrderStatus = status.Data;
            }

            return order;
        }
        public ProcessResult<List<Order>> GetOrdersByRole(string roleId, OrderType type = 0, string driverId = null, string producerId = null)
        {
            ProcessResult<List<Order>> result = null;
            List<Order> data = null;
            IQueryable<Order> querableData = null;
            var statusIdsResult = RoleOrderStatusManager.GetStatusByRoleId(roleId);
            if (statusIdsResult.IsSucceeded && statusIdsResult.Data != null && statusIdsResult.Data.Count > 0)
            {
                if (type == 0)
                {
                    querableData = GetAllQuerable().Data.Where(r => statusIdsResult.Data.Contains(r.FK_Order_Status_Id));
                }
                else
                {
                    querableData = GetAllQuerable().Data.Where(r => statusIdsResult.Data.Contains(r.FK_Order_Status_Id) && r.OrderType == type);
                }
                if (producerId != null)
                {
                    querableData = querableData.Where(r => r.FK_Producer_Id == producerId);
                }
                if (driverId != null)
                {
                    querableData = querableData.Where(r => r.FK_Driver_Id == driverId);
                }
                data = querableData.OrderByDescending(ord => ord.CreatedDate).ToList();
            }
            data = BindStatus(data);
            result = ProcessResultHelper.Succedded(data);
            return result;
        }

        public ProcessResult<List<Order>> GetAllExcludeLastStep()
        {
            var lstLastStatus = LKP_OrderStatusManager.GetStatusIdForLastStep();
            var orders = base.GetAllQuerable().Data.Where(ord => !lstLastStatus.Contains(ord.FK_Order_Status_Id)).OrderByDescending(ord => ord.CreatedDate).ToList();
            orders = BindStatus(orders);
            return ProcessResultHelper.Succedded(orders);
        }



        public ProcessResult<PaginatedItems<Order>> GetOrdersByRolePaginated(string roleId, PaginatedItems<Order> pagination = null, Expression<Func<Order, bool>> filterExpression = null, OrderType type = 0, string driverId = null, string producerId = null)
        {
            Expression<Func<Order, bool>> predicate = PredicateBuilder.True<Order>();
            if (filterExpression != null)
            {
                predicate = PredicateBuilder.And(predicate, filterExpression);
            }
            Expression<Func<Order, bool>> statusPredicate = null;// PredicateBuilder.True<Order>();
            var statusIdsResult = RoleOrderStatusManager.GetStatusByRoleId(roleId);
            if (statusIdsResult.IsSucceeded && statusIdsResult.Data != null && statusIdsResult.Data.Count > 0)
            {
                foreach (var statusId in statusIdsResult.Data)
                {
                    if (statusPredicate == null)
                    {
                        statusPredicate = PredicateBuilder.CreateEqualSingleExpression<Order>(nameof(Order.FK_Order_Status_Id), statusId);
                    }
                    else
                    {
                        var statuspredicateTemp = PredicateBuilder.CreateEqualSingleExpression<Order>(nameof(Order.FK_Order_Status_Id), statusId);
                        statusPredicate = PredicateBuilder.Or(statusPredicate, statuspredicateTemp);
                    }
                }
                if (statusPredicate != null)
                {
                    predicate = PredicateBuilder.And(statusPredicate, predicate);
                }
                if (type > 0)
                {
                    var predicateType = PredicateBuilder.CreateEqualSingleExpression<Order>(nameof(Order.OrderType), type);
                    predicate = PredicateBuilder.And(predicateType, predicate);
                }
                if (producerId != null)
                {
                    var predicateProducer = PredicateBuilder.CreateEqualSingleExpression<Order>(nameof(Order.FK_Producer_Id), producerId);
                    predicate = PredicateBuilder.And(predicateProducer, predicate);

                }
                if (driverId != null)
                {
                    var predicateDriver = PredicateBuilder.CreateEqualSingleExpression<Order>(nameof(Order.FK_Driver_Id), driverId);
                    predicate = PredicateBuilder.And(predicateDriver, predicate);

                }
            }
            return GetAllPaginated(pagination, predicate);
        }
        public ProcessResult<bool> UpdateOrderPaymentInfo(UpdatePaymentOrderViewModel model)
        {
            ProcessResult<bool> result = null;
            var orderResult = Get(model.OrderId);
            if (orderResult.IsSucceeded)
            {
                if (orderResult.Data != null)
                {
                    model.IsPaymentProcessed = true;
                    Mapper.Map(model, orderResult.Data);
                    result = Update(orderResult.Data);
                    if (result.IsSucceeded)
                    {
                        // TO AVOID UPDATE STATUS IN CASE PAYMENT NOT SUCCEEDED
                        if (orderResult.Data.IsPaymentSucceeded)
                        {
                            var handleWFRes = OrderWorkFlowManager.HandleWorkFlow(orderResult.Data);
                            if (!handleWFRes.IsSucceeded)
                            {
                                result = ProcessResultHelper.Failed(false, new Exception($"Error while Handle WorkFlow of order Id: {model.OrderId}"));
                            }
                        }
                    }
                }
                else
                {
                    result = ProcessResultHelper.Failed(false, new Exception($"no order with this {model.OrderId}"));
                }
            }
            else
            {
                result = ProcessResultHelper.MapToProcessResult(orderResult, result);
            }
            return result;
        }

        public ProcessResult<Order> UpdateOrderWalletPayment(Order model)
        {
            ProcessResult<Order> result = null;
            if (model != null)
            {
                model.PaymentMessage = "Wallet";
                model.IsPaymentProcessed = true;
                model.IsPaymentSucceeded = true;
                var paymentRes = Update(model);
                if (paymentRes.IsSucceeded && paymentRes.Data)
                {
                    result = OrderWorkFlowManager.HandleWorkFlow(model);
                    if (!result.IsSucceeded)
                    {
                        result = ProcessResultHelper.Failed(model, new Exception($"Error while Handle WorkFlow of order Id: {model.Id}"));
                    }
                }
                else
                {
                    result = ProcessResultHelper.MapToProcessResult<bool, Order>(paymentRes);
                }
            }
            return result;
        }

        public ProcessResult<int> GetTodayOrdersCount()
        {
            ProcessResult<int> result = null;
            var todayOrdersCount = GetAll().Data.Where(o => o.CreatedDate.Date == DateTime.UtcNow.Date).Count();
            result = ProcessResultHelper.Succedded(todayOrdersCount);
            return result;
        }

        public ProcessResult<int> GetOrdersCount(int daysNumber)
        {
            ProcessResult<int> result = null;
            var todayOrdersCount = GetAll().Data.Where(o => o.CreatedDate.Date >= DateTime.UtcNow.Date - TimeSpan.FromDays(daysNumber)).Count();
            result = ProcessResultHelper.Succedded(todayOrdersCount);
            return result;
        }

        public ProcessResult<Dictionary<DateTime, int>> GetOrdersCountPerDay(int daysNumber)
        {
            ProcessResult<Dictionary<DateTime, int>> result = null;
            var orders = GetAll().Data.Where(o => o.CreatedDate.Date >= DateTime.UtcNow.Date - TimeSpan.FromDays(daysNumber - 1))
                                      .GroupBy(o => o.CreatedDate.Date, (date, o) => new KeyValuePair<DateTime, int>(date, o.Count()))
                                      .ToDictionary(t => t.Key, t => t.Value);
            if (orders.Count < daysNumber)
            {
                for (int i = 0; i < daysNumber; i++)
                {
                    var day = DateTime.UtcNow.Date - TimeSpan.FromDays(i);
                    if (!orders.Keys.Contains(day))
                    {
                        orders.Add(day, 0);
                    }
                }
            }
            orders = orders.OrderBy(x => x.Key).ToDictionary(x => x.Key, x => x.Value);
            result = ProcessResultHelper.Succedded(orders);
            return result;
        }

        public ProcessResult<Dictionary<DateTime, double>> GetPaymentPerDay(int daysNumber)
        {
            ProcessResult<Dictionary<DateTime, double>> result = null;
            var payments = GetAll().Data.Where(o => o.CreatedDate.Date >= DateTime.UtcNow.Date - TimeSpan.FromDays(daysNumber - 1))
                                      .GroupBy(o => o.CreatedDate.Date, (date, o) => new KeyValuePair<DateTime, double>(date, o.Select(x => x.Price).ToList().Sum()))
                                      .ToDictionary(t => t.Key, t => t.Value);
            if (payments.Count < daysNumber)
            {
                for (int i = 0; i < daysNumber; i++)
                {
                    var day = DateTime.UtcNow.Date - TimeSpan.FromDays(i);
                    if (!payments.Keys.Contains(day))
                    {
                        payments.Add(day, 0);
                    }
                }
            }
            payments = payments.OrderBy(x => x.Key).ToDictionary(x => x.Key, x => x.Value);
            result = ProcessResultHelper.Succedded(payments);
            return result;
        }

        public ProcessResult<double> GetTodayPayment()
        {
            ProcessResult<double> result = null;
            var todayOrdersPrices = GetAll().Data.Where(o => o.CreatedDate.Date == DateTime.UtcNow.Date).Select(o => o.Price).ToList();
            double todayPayment = todayOrdersPrices.Sum();
            result = ProcessResultHelper.Succedded(todayPayment);
            return result;
        }

        public ProcessResult<int> GetTodayDeliveredOrdersCount()
        {
            ProcessResult<int> result = null;
            var deliverdStatus = LKP_OrderStatusManager.GetOrderStatus(orderAppSettings.DefaultFeesKeyEN);
            if (deliverdStatus.IsSucceeded && deliverdStatus.Data != null)
            {
                var todayOrdersCount = GetAll().Data.Where(o => o.CreatedDate.Date == DateTime.UtcNow.Date &&
                                                                o.FK_Order_Status_Id == deliverdStatus.Data.Id).Count();
                result = ProcessResultHelper.Succedded(todayOrdersCount);
            }
            else
            {
                result = ProcessResultHelper.Succedded(0);
            }
            return result;
        }

        public override ProcessResult<PaginatedItems<Order>> GetAllPaginated(PaginatedItems<Order> paginatedItems, Expression<Func<Order, bool>> predicate)
        {
            var ordersResult = base.GetAllPaginated(paginatedItems, predicate, o => o.CreatedDate);
            if (ordersResult.IsSucceeded && ordersResult.Data != null)
            {
                ordersResult.Data.Data = BindStatus(ordersResult.Data.Data);
            }
            return ordersResult;
        }

        //public override ProcessResult<PaginatedItems<Order>> GetAllPaginated(PaginatedItems<Order> paginatedItems)
        //{
        //    return GetAllPaginated(paginatedItems, null);
        //}


    }
}
