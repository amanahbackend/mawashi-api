﻿using System;
using System.Collections.Generic;
using System.Text;
using DispatchProduct.RepositoryModule;
using Utilites.ProcessingResult;
using System.Linq;
using Dispatching.Ordering.Models.Context;
using Dispatching.Ordering.Models.Entities;
using Dispatching.Ordering.BLL.IManagers;

namespace Dispatching.Ordering.BLL.Managers
{
    public class PickUpDatesManager : Repository<PickUpDates>, IPickUpDatesManager
    {
        public PickUpDatesManager(OrderDbContext context) : base(context)
        {
        }
    }
}
