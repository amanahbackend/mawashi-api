﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using DispatchProduct.BuildingBlocks.BaseEntities.EntityConfigurations;
using Dispatching.DriverModule.Models.Entities;

namespace Dispatching.DriverModule.Models.EntitiesConfiguration
{
    public class DriverLocationTypeConfiguration : BaseEntityTypeConfiguration<DriverLocation>, IEntityTypeConfiguration<DriverLocation>
    {
        public new void Configure(EntityTypeBuilder<DriverLocation> builder)
        {
            base.Configure(builder);

            builder.Property(l => l.Lat).IsRequired();
            builder.Property(l => l.Long).IsRequired();
            builder.Property(l => l.FK_Driver_Id).IsRequired();
        }
    }
}