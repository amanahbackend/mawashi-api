﻿using Dispatching.DriverModule.Models.Entities;
using Dispatching.DriverModule.Models.EntitiesConfiguration;
using Microsoft.EntityFrameworkCore;

namespace Dispatching.DriverModule.Models.Context
{
    public class DriverDbContext : DbContext
    {
        public DbSet<DriverLocation> DriverLocation { get; set; }
      
        public DriverDbContext(DbContextOptions<DriverDbContext> options)
            : base(options)
        {
            


        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.ApplyConfiguration(new DriverLocationTypeConfiguration());
        }
    }
}
