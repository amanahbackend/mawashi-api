﻿using DispatchProduct.BuildingBlocks.BaseEntities.Entities;
using DispatchProduct.BuildingBlocks.BaseEntities.IEntities;
using System;
using System.Collections.Generic;
using System.Text;

namespace Dispatching.DriverModule.Models.IEntities
{
    public interface IDriverLocation : IBaseEntity
    {
         string FK_Driver_Id { get; set; }
         double Lat { get; set; }
         double Long { get; set; }
    }
}
