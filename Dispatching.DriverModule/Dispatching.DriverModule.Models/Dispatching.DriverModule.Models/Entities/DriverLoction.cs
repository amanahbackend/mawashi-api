﻿using Dispatching.DriverModule.Models.IEntities;
using DispatchProduct.BuildingBlocks.BaseEntities.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace Dispatching.DriverModule.Models.Entities
{
    public class DriverLocation : BaseEntity,IDriverLocation
    {
        public string FK_Driver_Id { get; set; }
        public double Lat { get; set; }
        public double Long { get; set; }
    }
}
