﻿using DispatchProduct.RepositoryModule;
using System;
using System.Collections.Generic;
using System.Text;
using DispatchProduct.Controllers;
using AutoMapper;
using Utilites.ProcessingResult;
using Microsoft.AspNetCore.Mvc;
using DispatchProduct.Controllers.V1;
using Dispatching.DriverModule.API.ViewModels;
using Dispatching.DriverModule.Models.Entities;
using Dispatching.DriverModule.BLL.Managers;
using Dispatching.DriverModule.BLL.IManagers;

namespace Dispatching.Inventory.API.Controllers
{
    [ApiVersion("1.0")]
    [Route("v{version:apiVersion}/[controller]")]
    public class DriverLocationController : BaseControllerV1<IDriverLocationManager, DriverLocation, DriverLocationViewModel>
    {
        IProcessResultMapper processResultMapper;
        public DriverLocationController(IDriverLocationManager _manger, IMapper _mapper, IProcessResultMapper _processResultMapper, IProcessResultPaginatedMapper _processResultPaginatedMapper) : base(_manger, _mapper, _processResultMapper, _processResultPaginatedMapper)
        {
            manger = _manger;
            processResultMapper = _processResultMapper;
        }
        [HttpPost, Route("AddOrUpdateDriverLocation")]
        public ProcessResultViewModel<DriverLocationViewModel> AddOrUpdateDriverLocation([FromBody]DriverLocationViewModel model)
        {
            ProcessResultViewModel<DriverLocationViewModel> result = null;
            if (model != null)
            {
                if (model.Id == 0)
                {
                    result = Post(model);
                }
                else
                {
                    var updateRes = Put(model);
                    if (updateRes.IsSucceeded)
                    {
                        result = ProcessResultViewModelHelper.Succedded(model);
                    }
                    else
                    {
                        if (updateRes.Status != null)
                            result = ProcessResultViewModelHelper.Failed(model, updateRes.Status.Message);
                        else
                            result = ProcessResultViewModelHelper.Failed(model, "Can't Update Driver location");
                    }
                }
            }
            else
            {
                result = ProcessResultViewModelHelper.Failed<DriverLocationViewModel>(null, "Inserted Data Can't be Null");
            }
            return result;
        }
        [HttpGet,Route("GetByDriverId/{driver}")]
        public ProcessResultViewModel<DriverLocationViewModel> GetByDriverId([FromRoute]string driver)
        {
            var driverRes = manger.GetByDriverId(driver);
            return processResultMapper.Map<DriverLocation, DriverLocationViewModel>(driverRes);
        }
    }
}
