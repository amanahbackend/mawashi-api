﻿using AutoMapper;
using Dispatching.DriverModule.API.ViewModels;
using Dispatching.DriverModule.Models.Entities;
using Dispatching.DriverModule.Models;
using DispatchProduct.BuildingBlocks.BaseEntities.Entities;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Utilites.PaginatedItems;
using Utilites.PaginatedItemsViewModel;
using Utilities;
using Utilities.Utilites;
using Dispatching.DriverModule.Models.Context;
using DispatchProduct.BuildingBlocks.BaseEntities.IEntities;

namespace Dispatching.Inventory.API.AutoMapperConfig
{
    public class MappingProfile : Profile
    {
        public MappingProfile()
        {
            CreateMap<DriverLocation, DriverLocationViewModel>();
            CreateMap<DriverLocationViewModel, DriverLocation>()
                .IgnoreBaseEntityProperties();
        }
    }
}
