﻿using Dispatching.DriverModule.Models.Entities;
using DispatchProduct.RepositoryModule;
using System;
using System.Collections.Generic;
using System.Text;
using Utilites.ProcessingResult;

namespace Dispatching.DriverModule.BLL.IManagers
{
    public interface IDriverLocationManager : IRepository<DriverLocation>
    {
        ProcessResult<DriverLocation> GetByDriverId(string fk_driver_id);
    }
}
