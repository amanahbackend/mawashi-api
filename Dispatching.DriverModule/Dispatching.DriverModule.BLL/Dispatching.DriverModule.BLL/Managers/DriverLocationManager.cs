﻿using System;
using System.Collections.Generic;
using System.Text;
using DispatchProduct.RepositoryModule;
using Utilites.ProcessingResult;
using System.Linq;
using Dispatching.DriverModule.Models.Context;
using Dispatching.DriverModule.Models.Entities;
using Dispatching.DriverModule.Models.IEntities;
using Dispatching.DriverModule.BLL.IManagers;

namespace Dispatching.DriverModule.BLL.Managers
{
    public class DriverLocationManager : Repository<DriverLocation>, IDriverLocationManager
    {
        public DriverLocationManager(DriverDbContext context) : base(context)
        {
        }
        public ProcessResult<DriverLocation> GetByDriverId(string fk_driver_id)
        {
            ProcessResult<DriverLocation> result = null;
            var driver=GetAllQuerable().Data.Where(drv => drv.FK_Driver_Id == fk_driver_id).OrderBy(drv=>drv.CreatedDate).FirstOrDefault();
            if (driver != null)
            {
                result = ProcessResultHelper.Succedded(driver);
            }
            else
            {
                result = ProcessResultHelper.Failed(driver,new Exception($"there is no driver with this {fk_driver_id}"));
            }
            return result;
        }
    }
}
