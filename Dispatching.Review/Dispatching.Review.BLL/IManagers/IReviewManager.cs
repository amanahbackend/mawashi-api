﻿
using Microsoft.EntityFrameworkCore;
using System.Threading.Tasks;
using DispatchProduct.RepositoryModule;
using Dispatching.Reviewing.Models.Entities;
using Dispatching.Reviewing.Models.Context;
using Utilites.ProcessingResult;
using System.Collections.Generic;
using CommonEnums;
using Utilites.PaginatedItems;

namespace DispatchProduct.CustomerModule.BLL.Managers
{
    public interface IReviewManager : IRepository<Review>
    {
    }
}
