﻿
using Microsoft.EntityFrameworkCore;
using System.Threading.Tasks;
using DispatchProduct.RepositoryModule;
using Dispatching.Reviewing.Models.Entities;
using Dispatching.Reviewing.Models.Context;
using Utilites.ProcessingResult;
using System.Collections.Generic;

namespace DispatchProduct.CustomerModule.BLL.Managers
{
    public interface ILKP_Rating_ScaleManager : IRepository<LKP_Rating_Scale>
    {
        ProcessResult<List<LKP_Rating_Scale>> GetByItemId(int itemId);
    }
}
