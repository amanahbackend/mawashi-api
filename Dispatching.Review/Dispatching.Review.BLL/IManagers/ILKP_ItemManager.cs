﻿
using Microsoft.EntityFrameworkCore;
using System.Threading.Tasks;
using DispatchProduct.RepositoryModule;
using Dispatching.Reviewing.Models.Entities;

namespace DispatchProduct.CustomerModule.BLL.Managers
{
    public interface ILKP_ItemManager : IRepository<LKP_Item>
    {

    }
}
