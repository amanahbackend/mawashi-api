﻿
using Microsoft.EntityFrameworkCore;
using System.Threading.Tasks;
using Dispatching.Reviewing.Models.Entities;
using Dispatching.Reviewing.Models.Context;
using DispatchProduct.RepositoryModule;
namespace DispatchProduct.CustomerModule.BLL.Managers
{
    public class LKP_ItemManager : Repository<LKP_Item>, ILKP_ItemManager
    {
        public LKP_ItemManager(ReviewDbContext context)
            : base(context)
        {

        }

    }
}
