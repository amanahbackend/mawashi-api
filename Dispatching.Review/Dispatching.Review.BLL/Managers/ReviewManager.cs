﻿
using Microsoft.EntityFrameworkCore;
using System.Threading.Tasks;
using DispatchProduct.RepositoryModule;
using Dispatching.Reviewing.Models.Entities;
using Dispatching.Reviewing.Models.Context;
using System.Collections.Generic;
using Utilites.ProcessingResult;
using CommonEnums;
using System.Linq;
using Utilites.PaginatedItems;
using Utilites.PaginatedItemsViewModel;

namespace DispatchProduct.CustomerModule.BLL.Managers
{
    public class ReviewManager : Repository<Review>, IReviewManager
    {
        public ReviewManager(ReviewDbContext context)
            : base(context)
        {
        }
        
    }
}
