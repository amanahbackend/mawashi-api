﻿
using Microsoft.EntityFrameworkCore;
using System.Threading.Tasks;
using DispatchProduct.RepositoryModule;
using Dispatching.Reviewing.Models.Entities;
using Dispatching.Reviewing.Models.Context;
using Utilites.ProcessingResult;
using System.Collections.Generic;
using CommonEnums;
using System.Linq;

namespace DispatchProduct.CustomerModule.BLL.Managers
{
    public class LKP_Rating_ScaleManager : Repository<LKP_Rating_Scale>, ILKP_Rating_ScaleManager
    {
        public LKP_Rating_ScaleManager(ReviewDbContext context)
            : base(context)
        {

        }
        public ProcessResult<List<LKP_Rating_Scale>> GetByItemId(int itemId)
        {
            var entityResult = GetAllQuerable().Data.Where(scale => scale.FK_Item_Id == itemId).ToList();
            return ProcessResultHelper.Succedded(entityResult);
        }

    }
}
