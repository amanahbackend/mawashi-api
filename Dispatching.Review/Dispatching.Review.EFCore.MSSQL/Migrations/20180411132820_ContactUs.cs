﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Dispatching.Reviewing.EFCore.MSSQL.Migrations
{
    public partial class ContactUs : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "ReviewerMail",
                table: "Review",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "ReviewerName",
                table: "Review",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "ReviewerPhone",
                table: "Review",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Title",
                table: "Review",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "ReviewerMail",
                table: "Review");

            migrationBuilder.DropColumn(
                name: "ReviewerName",
                table: "Review");

            migrationBuilder.DropColumn(
                name: "ReviewerPhone",
                table: "Review");

            migrationBuilder.DropColumn(
                name: "Title",
                table: "Review");
        }
    }
}
