﻿using CommonEnums;
using DispatchProduct.BuildingBlocks.BaseEntities.Entities;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Dispatching.Reviewing.API.ServicesViewModels.Customer
{
    public class CustomerViewModel : RepoistryBaseEntity
    {
        public string UserName { get; set; }
        public string Email { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Phone1 { get; set; }
        public string Phone2 { get; set; }
        public string PicturePath { get; set; }
        public List<string> RoleNames { get; set; }
        public string Picture { get; set; }
        public string Password { get; set; }
        public string Fk_AppUser_Id { get; set; }
        public Country Fk_Country_Id { get; set; }
        public string PictureUrl { get; set; }
    }
}
