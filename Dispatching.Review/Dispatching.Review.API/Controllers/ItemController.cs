﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using AutoMapper;
using Microsoft.AspNetCore.Http;
using System.Security.Claims;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Authorization;
using DispatchProduct.CustomerModule.BLL.Managers;
using DispatchProduct.Controllers;
using Dispatching.Reviewing.Models.Entities;
using Utilites.ProcessingResult;
using Dispatching.Reviewing.API.ViewModels;
using DispatchProduct.Controllers.V1;

namespace Dispatching.Reviewing.API.Controllers
{
    [ApiVersion("1.0")]
    [Route("v{version:apiVersion}/[controller]")]
    public class ItemController : BaseControllerV1<ILKP_ItemManager, LKP_Item, LKP_ItemViewModel>
    {
        public ItemController(ILKP_ItemManager _manger, IMapper _mapper, IProcessResultMapper _processResultMapper, IProcessResultPaginatedMapper _processResultPaginatedMapper) :base(_manger, _mapper, _processResultMapper, _processResultPaginatedMapper)
        {
           
        }
    }
}