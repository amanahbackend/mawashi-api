﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using AutoMapper;
using Microsoft.AspNetCore.Http;
using System.Security.Claims;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Authorization;
using DispatchProduct.CustomerModule.BLL.Managers;
using DispatchProduct.Controllers;
using Dispatching.Reviewing.Models.Entities;
using Utilites.ProcessingResult;
using Dispatching.Reviewing.API.ViewModels;
using DispatchProduct.Controllers.V1;
using CommonEnums;
using Utilites.PaginatedItemsViewModel;
using DispatchProduct.RepositoryModule;
using Dispatching.Reviewing.API.ServicesCommunication.Customer;
using Microsoft.Extensions.DependencyInjection;
using Dispatching.Reviewing.API.ViewModel;

namespace Dispatching.Reviewing.API.Controllers
{
    [ApiVersion("1.0")]
    [Route("v{version:apiVersion}/[controller]")]
    public class ReviewController : BaseControllerV1<IReviewManager, Review, ReviewViewModel>
    {
        public new IReviewManager manger;
        public new readonly IMapper mapper;
        IProcessResultMapper processResultMapper;
        IProcessResultPaginatedMapper processResultPaginatedMapper;
        IServiceProvider serviceprovider;
        public ReviewController(IServiceProvider _serviceprovider, IReviewManager _manger, IMapper _mapper, IProcessResultMapper _processResultMapper, IProcessResultPaginatedMapper _processResultPaginatedMapper) : base(_manger, _mapper, _processResultMapper, _processResultPaginatedMapper)
        {
            manger = _manger;
            mapper = _mapper;
            processResultMapper = _processResultMapper;
            processResultPaginatedMapper = _processResultPaginatedMapper;
            serviceprovider = _serviceprovider;
        }

        [Route("SubmitApplicationReview")]
        [HttpPost]
        [MapToApiVersion("1.0")]
        public ProcessResultViewModel<ReviewViewModel> SubmitApplicationReview([FromBody]ReviewViewModel model)
        {
            ProcessResultViewModel<ReviewViewModel> result = null;

            if (model != null)
            {
                model.FK_LKP_Item_Id = (int)ReviewItems.Application;
                result = Post(model);
            }
            return result;

        }
        [Route("SubmitComplainReview")]
        [HttpPost]
        [MapToApiVersion("1.0")]
        public ProcessResultViewModel<ReviewViewModel> SubmitComplainReview([FromBody]ReviewViewModel model)
        {
            ProcessResultViewModel<ReviewViewModel> result = null;
            if (model != null)
            {
                model.FK_LKP_Item_Id = (int)ReviewItems.Complain;
                result = Post(model);
            }
            return result;
        }

        [Route("ContactUs")]
        [HttpPost]
        [MapToApiVersion("1.0")]
        public ProcessResultViewModel<ReviewViewModel> ContactUs ([FromBody]ReviewViewModel model)
        {
            ProcessResultViewModel<ReviewViewModel> result = null;
            if (model != null)
            {
                model.FK_LKP_Item_Id = (int)ReviewItems.ContactUs;
                result = Post(model);
            }
            return result;
        }
        [Route("SubmitOrderReview")]
        [HttpPost]
        [MapToApiVersion("1.0")]
        public ProcessResultViewModel<ReviewViewModel> SubmitOrderReview([FromBody]ReviewViewModel model)
        {
            ProcessResultViewModel<ReviewViewModel> result = null;

            if (model != null)
            {
                model.FK_LKP_Item_Id = (int)ReviewItems.Order;
                result = Post(model);
            }
            return result;

        }

        [Route("GetApplicationReviews")]
        [HttpPost]
        [MapToApiVersion("1.0")]
        public async Task<ProcessResultViewModel<PaginatedItemsViewModel<ReviewViewModel>>> GetApplicationReviews([FromBody]PaginatedItemsViewModel<ReviewViewModel> model = null,[FromRoute]int reviewerId = 0)
        {
            return await GetReviews(model, ReviewItems.Application, reviewerId);
        }
        [Route("GetOrderReviews")]
        [HttpPost]
        [MapToApiVersion("1.0")]
        public async Task<ProcessResultViewModel<PaginatedItemsViewModel<ReviewViewModel>>> GetOrderReviews([FromBody]PaginatedItemsViewModel<ReviewViewModel> model = null,[FromRoute]int reviewerId = 0)
        {
            return await GetReviews(model, ReviewItems.Order, reviewerId);
        }

        [HttpGet, Route("GetAppReviewStats"), MapToApiVersion("1.0")]
        public ProcessResultViewModel<List<ReviewScaleCountViewModel>> GetAppReviewStats()
        {
            return GetReviewsCount(ReviewItems.Application);
        }

        [Route("GetComplains/{reviewerId:int?}")]
        [HttpPost]
        [MapToApiVersion("1.0")]
        public async Task<ProcessResultViewModel<PaginatedItemsViewModel<ReviewViewModel>>> GetComplains([FromBody]PaginatedItemsViewModel<ReviewViewModel> model = null,[FromRoute]int reviewerId = 0)
        {
            return await GetReviews(model, ReviewItems.Complain, reviewerId);
        }

        [Route("GetContactUSMessages/{reviewerId:int?}")]
        [HttpPost]
        [MapToApiVersion("1.0")]
        public async Task<ProcessResultViewModel<PaginatedItemsViewModel<ReviewViewModel>>> GetContactUSMessages([FromBody]PaginatedItemsViewModel<ReviewViewModel> model = null,[FromRoute]int reviewerId = 0)
        {
            return await GetReviews(model, ReviewItems.ContactUs, reviewerId);
        }

        public async Task<ProcessResultViewModel<PaginatedItemsViewModel<ReviewViewModel>>> GetReviews([FromBody]PaginatedItemsViewModel<ReviewViewModel> model = null, ReviewItems review = 0, int reviewerId = 0)
        {
            ProcessResultViewModel<PaginatedItemsViewModel<ReviewViewModel>> result = null;
            if (review != 0)
            {
                var predicate = PredicateBuilder.False<Review>();
                Review r = new Review();
                var expr = PredicateBuilder.CreateEqualSingleExpression<Review>(nameof(r.FK_LKP_Item_Id), (int)review);
                if (reviewerId > 0)
                {
                    var reviewExpr = PredicateBuilder.CreateEqualSingleExpression<Review>(nameof(r.FK_Reviewer_Id), reviewerId);
                    expr = PredicateBuilder.And(expr, reviewExpr);
                }
                result = GetAllPaginatedByPredicate(model, expr);
            }
            else
            {
                result = GetAllPaginated(model);

            }
            await BindCustomerToReviews(result.Data.Data);
            BindItems_scalesToReviews(result.Data.Data);
            result.Data.Data = BindRating(result.Data.Data);
            if (result.Data != null && result.Data.Data != null)
            {
                result.Data.Data.OrderByDescending(r => r.CreatedDate);
            }
            return result;
        }
        public async Task<ProcessResultViewModel<List<ReviewViewModel>>> BindCustomerToReviews(List<ReviewViewModel> reviews)
        {
            var reviewerIds = reviews.Select(r => r.FK_Reviewer_Id).ToList();
            var reviewersRes = await CustomerService.GetByCustomerIds(reviewerIds);
            if (reviewersRes.IsSucceeded && reviewersRes.Data != null)
            {
                foreach (var item in reviews)
                {
                    item.Reviewer = reviewersRes.Data.Where(r => r.Id == item.FK_Reviewer_Id).FirstOrDefault();
                }
            }
            return ProcessResultViewModelHelper.Succedded(reviews);
        }
        public ProcessResultViewModel<List<ReviewViewModel>> BindItems_scalesToReviews(List<ReviewViewModel> reviews)
        {
            var itemIds = reviews.Select(r => r.FK_LKP_Item_Id).ToList();
            var itemsRes = ItemController.GetByIds(itemIds);
            var scaleIds = reviews.Select(r => r.FK_LKP_Item_Id).ToList();
            var scaleRes = RatingScaleController.GetByIds(scaleIds);
            if (itemsRes.IsSucceeded && itemsRes.Data != null)
            {
                foreach (var item in reviews)
                {
                    item.Item = itemsRes.Data.Where(r => r.Id == item.FK_LKP_Item_Id).FirstOrDefault();
                }
            }
            if (scaleRes.IsSucceeded && scaleRes.Data != null)
            {
                foreach (var item in reviews)
                {
                    item.RatingScale = scaleRes.Data.Where(r => r.Id == item.FK_LKP_Rating_Scale_Id).FirstOrDefault();
                }
            }
            return ProcessResultViewModelHelper.Succedded(reviews);
        }
        public ICustomerService CustomerService
        {
            get
            {
                return serviceprovider.GetService<ICustomerService>();
            }
        }
        public ItemController ItemController
        {
            get
            {
                return serviceprovider.GetService<ItemController>();
            }
        }
        public RatingScaleController RatingScaleController
        {
            get
            {
                return serviceprovider.GetService<RatingScaleController>();
            }
        }

        public List<ReviewViewModel> BindRating(List<ReviewViewModel> reviews)
        {
            foreach (var item in reviews)
            {
                var ratingRes = RatingScaleController.Get(item.FK_LKP_Rating_Scale_Id);
                if (ratingRes.IsSucceeded && ratingRes.Data != null)
                {
                    item.RatingScale = ratingRes.Data;
                }
            }
            return reviews;
        }
        public ProcessResultViewModel<List<ReviewScaleCountViewModel>> GetReviewsCount(ReviewItems review = 0)
        {
            ProcessResultViewModel<List<ReviewScaleCountViewModel>> result = null;
            List<Review> data = null;
            if (review != 0)
            {
                data = manger.GetAllQuerable().Data.Where(r => r.FK_LKP_Item_Id == (int)review).ToList();
            }
            else
            {
                data = manger.GetAll().Data;
            }
            var groupedData = data.GroupBy(r => r.FK_LKP_Rating_Scale_Id, (key, r) => new ReviewScaleCountViewModel { Key = key, Count = r.Count() }).ToList();
            var keys = groupedData.Select(r => r.Key).ToList();
            var scales = RatingScaleController.GetByIds(keys);
            if (scales.IsSucceeded)
            {
                foreach (var item in scales.Data)
                {
                    var rating = groupedData.FirstOrDefault(c => c.Key == item.Id);
                    rating.Name = item.NameEN;
                }
            }
            result = ProcessResultViewModelHelper.Succedded(groupedData);
            return result;
        }

    }
}