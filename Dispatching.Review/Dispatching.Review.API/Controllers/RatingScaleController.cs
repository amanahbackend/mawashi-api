﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using AutoMapper;
using Microsoft.AspNetCore.Http;
using System.Security.Claims;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Authorization;
using DispatchProduct.CustomerModule.BLL.Managers;
using DispatchProduct.Controllers;
using Dispatching.Reviewing.Models.Entities;
using Utilites.ProcessingResult;
using Dispatching.Reviewing.API.ViewModels;
using DispatchProduct.Controllers.V1;
using CommonEnums;

namespace Dispatching.Reviewing.API.Controllers
{
    [ApiVersion("1.0")]
    [Route("v{version:apiVersion}/[controller]")]
    public class RatingScaleController : BaseControllerV1<ILKP_Rating_ScaleManager, LKP_Rating_Scale, LKP_Rating_ScaleViewModel>
    {
        public new ILKP_Rating_ScaleManager manger;
        public new readonly IMapper mapper;
        IProcessResultMapper processResultMapper;
        IProcessResultPaginatedMapper processResultPaginatedMapper;
        IServiceProvider serviceprovider;
        public RatingScaleController(IServiceProvider _serviceprovider,ILKP_Rating_ScaleManager _manger, IMapper _mapper, IProcessResultMapper _processResultMapper, IProcessResultPaginatedMapper _processResultPaginatedMapper) : base(_manger, _mapper, _processResultMapper, _processResultPaginatedMapper)
        {
            manger = _manger;
            mapper = _mapper;
            processResultMapper = _processResultMapper;
            processResultPaginatedMapper = _processResultPaginatedMapper;
            serviceprovider = _serviceprovider;
        }
        [Route("GetByItemId")]
        [HttpGet]
        [MapToApiVersion("1.0")]
        public ProcessResultViewModel<List<LKP_Rating_ScaleViewModel>> GetByItemId([FromRoute]int itemId)
        {
            var entityResult = manger.GetByItemId(itemId);
            return processResultMapper.Map<List<LKP_Rating_Scale>, List<LKP_Rating_ScaleViewModel>>(entityResult);
        }
        [Route("GetApplicationOptions")]
        [HttpGet]
        [MapToApiVersion("1.0")]
        public ProcessResultViewModel<List<LKP_Rating_ScaleViewModel>> GetApplicationOptions()
        {
            return GetByItemId((int)ReviewItems.Application);
        }

        [Route("GetComplainOptions")]
        [HttpGet]
        [MapToApiVersion("1.0")]
        public ProcessResultViewModel<List<LKP_Rating_ScaleViewModel>> GetComplainOptions()
        {
            return GetByItemId((int)ReviewItems.Complain);
        }

        [Route("GetOrderOptions")]
        [HttpGet]
        [MapToApiVersion("1.0")]
        public ProcessResultViewModel<List<LKP_Rating_ScaleViewModel>> GetOrderOptions()
        {
            return GetByItemId((int)ReviewItems.Order);
        }
    }
}