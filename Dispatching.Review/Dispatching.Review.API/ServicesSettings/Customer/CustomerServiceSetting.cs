﻿using DispatchProduct.HttpClient;

namespace DispatchProduct.Reviewing.API.ServicesSettings
{
    public class CustomerServiceSetting : DefaultHttpClientSettings
    {
        public override string Uri
        {
            get; set;
        }

        public string GetByCustomerIdsVerb
        {
            get; set;
        }
        
    }
}
