﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Dispatching.Reviewing.API.ViewModel
{
    public class ReviewScaleCountViewModel
    {
        public int Key { get; set; }
        public string Name { get; set; }
        public int Count { get; set; }
    }
}
