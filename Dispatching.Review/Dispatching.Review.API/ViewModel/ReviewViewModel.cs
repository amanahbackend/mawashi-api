﻿using Dispatching.Reviewing.API.ServicesViewModels.Customer;
using Dispatching.Reviewing.API.ViewModels;
using DispatchProduct.BuildingBlocks.BaseEntities.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace Dispatching.Reviewing.Models.Entities
{
    public class ReviewViewModel:BaseEntity
    {
        public int FK_Reviewer_Id  { get; set; }
        public int Fk_Reviewed_Id { get; set; }
        public string Comment { get; set; }
        public int FK_LKP_Item_Id { get; set; }
        public int FK_LKP_Rating_Scale_Id { get; set; }
        public LKP_ItemViewModel Item { get; set; }
        public LKP_Rating_ScaleViewModel RatingScale { get; set; }
        public CustomerViewModel Reviewer { get; set; }
        public string ReviewerName { get; set; }
        public string ReviewerPhone { get; set; }
        public string ReviewerMail { get; set; }
        public string Title { get; set; }

        
    }
}
