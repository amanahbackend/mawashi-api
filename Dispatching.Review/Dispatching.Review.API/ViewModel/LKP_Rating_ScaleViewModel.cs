﻿using DispatchProduct.BuildingBlocks.BaseEntities.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace Dispatching.Reviewing.Models.Entities
{
    public class LKP_Rating_ScaleViewModel:BaseLKPEntity
    {
        public int Degree { get; set; }
        public int FK_Item_Id { get; set; }
    }
}
