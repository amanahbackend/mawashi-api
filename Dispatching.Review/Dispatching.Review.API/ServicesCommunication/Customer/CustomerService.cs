﻿using Dispatching.Reviewing.API.ServicesViewModels.Customer;
using DispatchProduct.HttpClient;
using DispatchProduct.Reviewing.API.ServicesSettings;
using DnsClient;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Utilites.ProcessingResult;

namespace Dispatching.Reviewing.API.ServicesCommunication.Customer
{
    public class CustomerService : DefaultHttpClientCrud<CustomerServiceSetting, CustomerViewModel, CustomerViewModel>, ICustomerService
    {
        CustomerServiceSetting settings;
        IDnsQuery dnsQuery;
        public CustomerService(IOptions<CustomerServiceSetting> _settings, IDnsQuery _dnsQuery) :base(_settings.Value, _dnsQuery)
        {
            settings = _settings.Value;
            dnsQuery = _dnsQuery;
        }
        public async Task<ProcessResultViewModel<List<CustomerViewModel>>> GetByCustomerIds(List<int> customerIds)
        {
            var requesturi = $"{await settings.GetUri(dnsQuery)}/{settings.GetByCustomerIdsVerb}";
            string baseUrl = await settings.GetUri(dnsQuery);
            while (baseUrl.EndsWith('/'))
            {
                baseUrl = baseUrl.Substring(0, baseUrl.Length - 1);
            }
            var result = await PostCustomize<List<int>, List<CustomerViewModel>>(requesturi, customerIds);
            return result;
        }
    }
}
