﻿using Dispatching.Reviewing.API.ServicesViewModels.Customer;
using DispatchProduct.HttpClient;
using DispatchProduct.Reviewing.API.ServicesSettings;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Utilites.ProcessingResult;

namespace Dispatching.Reviewing.API.ServicesCommunication.Customer
{
    public interface ICustomerService : IDefaultHttpClientCrud<CustomerServiceSetting, CustomerViewModel, CustomerViewModel>
    {
        Task<ProcessResultViewModel<List<CustomerViewModel>>> GetByCustomerIds(List<int> customerIds);
    }
}
