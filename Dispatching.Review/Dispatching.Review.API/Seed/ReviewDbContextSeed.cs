﻿
using DispatchProduct.RepositoryModule;
using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Dispatching.Reviewing.Models.Context;
using Dispatching.Reviewing.Models.Entities;
using CommonEnums;

namespace DispatchProduct.CustomerModule.API.Seed
{
    public class ReviewDbContextSeed : ContextSeed
    {
        public async Task SeedAsync(ReviewDbContext context, IHostingEnvironment env,
            ILogger<ReviewDbContextSeed> logger, int? retry = 0)
        {
            int retryForAvaiability = retry.Value;

            try
            {
                var contentRootPath = env.ContentRootPath;
                var webroot = env.WebRootPath;
                List<LKP_Item> lstLKP_Item = new List<LKP_Item>();
                List<LKP_Rating_Scale> lstLKP_Rating_Scale = new List<LKP_Rating_Scale>();
                lstLKP_Item = GetDefaultLKP_Items();
                lstLKP_Rating_Scale = GetDefaultLKP_Rating_Scale();
                using (var transaction = context.Database.BeginTransaction())
                {
                    context.Database.OpenConnection();
                    await SeedEntityAsync(context, lstLKP_Item, false);
                    context.Database.ExecuteSqlCommand("SET IDENTITY_INSERT LKP_Item ON");
                    context.SaveChanges();
                    context.Database.ExecuteSqlCommand("SET IDENTITY_INSERT LKP_Item OFF");

                    await SeedEntityAsync(context, lstLKP_Rating_Scale, false);
                    context.Database.ExecuteSqlCommand("SET IDENTITY_INSERT LKP_Rating_Scale ON");
                    context.SaveChanges();
                    context.Database.ExecuteSqlCommand("SET IDENTITY_INSERT LKP_Rating_Scale OFF");
                    transaction.Commit();
                }
            }
            catch (Exception ex)
            {
                if (retryForAvaiability < 3)
                {
                    retryForAvaiability++;

                    logger.LogError(ex.Message, $"There is an error migrating data for CustomerDbContextSeed");

                    await SeedAsync(context, env, logger, retryForAvaiability);
                }
            }
        }
        private List<LKP_Item> GetDefaultLKP_Items()
        {
            List<LKP_Item> result = new List<LKP_Item>
            {
                new LKP_Item() {Id=(int)ReviewItems.Application, NameAR=EnumManager<ReviewItems>.GetDescription(ReviewItems.Application),NameEN= EnumManager<ReviewItems>.GetName(ReviewItems.Application) },
                new LKP_Item() {Id=(int)ReviewItems.Complain, NameAR=EnumManager<ReviewItems>.GetDescription(ReviewItems.Complain),NameEN= EnumManager<ReviewItems>.GetName(ReviewItems.Complain) },
                new LKP_Item() {Id=(int)ReviewItems.Order, NameAR=EnumManager<ReviewItems>.GetDescription(ReviewItems.Order),NameEN= EnumManager<ReviewItems>.GetName(ReviewItems.Order) },
                new LKP_Item() {Id=(int)ReviewItems.ContactUs, NameAR=EnumManager<ReviewItems>.GetDescription(ReviewItems.ContactUs),NameEN= EnumManager<ReviewItems>.GetName(ReviewItems.ContactUs) },
            };
            return result;
        }

        private List<LKP_Rating_Scale> GetDefaultLKP_Rating_Scale()
        {
            List<LKP_Rating_Scale> result = new List<LKP_Rating_Scale>
            {
                new LKP_Rating_Scale() {Id=1,FK_Item_Id=(int)ReviewItems.Application, NameEN = "Happy" ,Degree=5,NameAR= "سعيد" },
                new LKP_Rating_Scale() {Id=2,FK_Item_Id=(int)ReviewItems.Application, NameEN= "Normal",Degree=7,NameAR= "عادى" },
                new LKP_Rating_Scale() {Id=3,FK_Item_Id=(int)ReviewItems.Application, NameEN= "Sad",Degree=0 ,NameAR= "حزين"},
                new LKP_Rating_Scale() {Id=4,FK_Item_Id=(int)ReviewItems.Complain, NameEN = "Delivery Boy" ,Degree=5,NameAR= "موصل الخدمة" },
                new LKP_Rating_Scale() {Id=5,FK_Item_Id=(int)ReviewItems.Complain, NameEN= "Order Components",Degree=7,NameAR= "مكونات الطلب" },

                 new LKP_Rating_Scale() {Id=4,FK_Item_Id=(int)ReviewItems.ContactUs, NameEN = "Inquiry for Orders" ,Degree=5,NameAR= "استفسار عن الطلبات" },
                new LKP_Rating_Scale() {Id=5,FK_Item_Id=(int)ReviewItems.ContactUs, NameEN= "Inquiry for Discount Schedule",Degree=7,NameAR= "استفسار عن اسعارالتخفيضات" },
            };
            return result;
        }



    }
}
