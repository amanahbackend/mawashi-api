﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Dispatching.Reviewing.Models.Context;
using Swashbuckle.AspNetCore.Swagger;
using Microsoft.EntityFrameworkCore;
using AutoMapper;
using DispatchProduct.RepositoryModule;
using Dispatching.Reviewing.Models.Entities;
using DispatchProduct.CustomerModule.BLL.Managers;
using Autofac;
using Autofac.Extensions.DependencyInjection;
using Utilites.ProcessingResult;
using System.Net;
using DnsClient;
using System.Net.Sockets;
using Newtonsoft.Json.Serialization;
using BuildingBlocks.ServiceDiscovery;
using Dispatching.Reviewing.API.ServicesCommunication.Customer;
using Dispatching.Reviewing.API.Controllers;
using Utilites.PaginatedItems;
using DispatchProduct.Reviewing.API.ServicesSettings;

namespace Dispatching.Reviewing.API
{
    public class Startup
    {
        public IHostingEnvironment _env;
        public IConfigurationRoot Configuration { get; }
        public Startup(IHostingEnvironment env)
        {
            _env = env;
            var builder = new ConfigurationBuilder()
                .SetBasePath(env.ContentRootPath)
                .AddEnvironmentVariables();
            Configuration = builder.Build();

           var country = Configuration.GetSection("DockerContainerName").Value.Split('.').Last().ToLower();
            // var country = "kw";
           builder = new ConfigurationBuilder()
                 .SetBasePath(env.ContentRootPath)
                 .AddJsonFile($"appsettings.{country}.json", optional: false, reloadOnChange: true)
                 .AddEnvironmentVariables();

            Configuration = builder.Build();
        }

        // This method gets called by the runtime. Use this method to add services to the container.
        public IServiceProvider ConfigureServices(IServiceCollection services)
        {
            services.AddCors(options =>
            {
                options.AddPolicy("AllowAll",
                    builder =>
                    {
                        builder
                        .AllowAnyOrigin()
                        .AllowAnyMethod()
                        .AllowAnyHeader()
                        .AllowCredentials();
                    });
            });

            services.AddSingleton(provider => Configuration);

            services.AddMvc().AddJsonOptions(options =>
            {
                options.SerializerSettings.ContractResolver = new CamelCasePropertyNamesContractResolver();
            });

            string consulContainerName = Configuration.GetSection("ServiceRegisteryContainerName").Value;
            int consulPort = Convert.ToInt32(Configuration.GetSection("ServiceRegisteryPort").Value);
            IPAddress ip = Dns.GetHostEntry(consulContainerName).AddressList.Where(o => o.AddressFamily == AddressFamily.InterNetwork).First();
            services.AddSingleton<IDnsQuery>(new LookupClient(ip, consulPort));

            services.AddServiceDiscovery(Configuration.GetSection("ServiceDiscovery"));


            services.Configure<CustomerServiceSetting>
               (Configuration.GetSection("ServiceCommunications:CustomerService"));
            services.AddApiVersioning(o => o.ReportApiVersions = true);
            services.AddDbContext<ReviewDbContext>(options =>
            options.UseSqlServer(Configuration["ConnectionString"],
            sqlOptions => sqlOptions.MigrationsAssembly("Dispatching.Reviewing.EFCore.MSSQL")));

            services.AddOptions();

            services.AddSwaggerGen(c => {
                c.SwaggerDoc("v1",
                    new Info()
                    {
                        Title = "Review API",
                        Description = "Review  API"
                    });
                c.AddSecurityDefinition("oauth2", new OAuth2Scheme
                {
                    Type = "oauth2",
                    Flow = "implicit",
                    AuthorizationUrl = $"{Configuration.GetValue<string>("IdentityUrlExternal")}/connect/authorize",
                    TokenUrl = $"{Configuration.GetValue<string>("IdentityUrlExternal")}/connect/token",
                    Scopes = new Dictionary<string, string>()
                    {
                        { "Review", "Review API" }
                    }
                });
            });


            services.AddMvc();
            services.AddAutoMapper(typeof(Startup));
            Mapper.AssertConfigurationIsValid();

            services.AddScoped<DbContext, ReviewDbContext>();
            services.AddScoped(typeof(IRepository<>), typeof(Repository<>));
            services.AddScoped(typeof(ILKP_Item), typeof(LKP_Item));
            services.AddScoped(typeof(ILKP_Rating_Scale), typeof(LKP_Rating_Scale));
            services.AddScoped(typeof(IReview), typeof(Review));
            services.AddScoped(typeof(ILKP_ItemManager), typeof(LKP_ItemManager));
            services.AddScoped(typeof(ILKP_Rating_ScaleManager), typeof(LKP_Rating_ScaleManager));
            services.AddScoped(typeof(IReviewManager), typeof(ReviewManager));
            services.AddScoped(typeof(ReviewController), typeof(ReviewController));
            services.AddScoped(typeof(ItemController), typeof(ItemController));
            services.AddScoped(typeof(RatingScaleController), typeof(RatingScaleController));
            services.AddScoped(typeof(IRepository<>), typeof(Repository<>));
            services.AddScoped(typeof(IProcessResultMapper), typeof(ProcessResultMapper));
            services.AddScoped(typeof(IProcessResultPaginatedMapper), typeof(ProcessResultPaginatedMapper));
            services.AddScoped(typeof(IPaginatedItems<>), typeof(PaginatedItems<>));
            services.AddScoped(typeof(IReviewManager), typeof(ReviewManager));
            services.AddScoped(typeof(IProcessResultMapper), typeof(ProcessResultMapper));
            services.AddScoped(typeof(ICustomerService), typeof(CustomerService));
            var container = new ContainerBuilder();
            container.Populate(services);
            return new AutofacServiceProvider(container.Build());
        }

        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            app.UseCors("AllowAll");
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
                //app.UseDatabaseErrorPage();
            }
            else
            {
                app.UseExceptionHandler("/Home/Error");
            }
            app.UseStaticFiles();
            app.UseSwagger();
            app.UseSwaggerUI(c =>
            {
                c.SwaggerEndpoint("/swagger/v1/swagger.json", "Reviewing API");
            });

            app.UseMvc();
            app.UseConsulRegisterService();
        }
    }
}
