﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Dispatching.Reviewing.Models.Context;
using DispatchProduct.CustomerModule.API.Seed;
using Microsoft.Extensions.DependencyInjection;
namespace Dispatching.Reviewing.API
{
    public class Program
    {
        public static void Main(string[] args)
        {
            BuildWebHost(args).MigrateDbContext<ReviewDbContext>((context, services) =>
            {
                var env = services.GetService<IHostingEnvironment>();
                var logger = services.GetService<ILogger<ReviewDbContextSeed>>();
                new ReviewDbContextSeed()
                    .SeedAsync(context, env, logger)
                    .Wait();
            }).Run();
        }

        public static IWebHost BuildWebHost(string[] args) =>
                     WebHost.CreateDefaultBuilder(args)
                         .UseKestrel()
                         .UseContentRoot(Directory.GetCurrentDirectory())
                         .UseIISIntegration()
                         .UseStartup<Startup>()
                         .ConfigureLogging((hostingContext, builder) =>
                         {
                             builder.AddConfiguration(hostingContext.Configuration.GetSection("Logging"));
                             builder.AddConsole();
                             builder.AddDebug();
                         })
                         .Build();
    }
}
