﻿using Dispatching.Reviewing.Models.Entities;
using Dispatching.Reviewing.Models.EntityConfigurations;
using Microsoft.EntityFrameworkCore;

namespace Dispatching.Reviewing.Models.Context
{
    public class ReviewDbContext : DbContext
    {

        public DbSet<LKP_Item> LKP_Item { get; set; }
        public DbSet<LKP_Rating_Scale> LKP_Rating_Scale { get; set; }
        public DbSet<Review> Review { get; set; }

        public ReviewDbContext(DbContextOptions<ReviewDbContext> options)
            : base(options)
        {
            

        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
            modelBuilder.ApplyConfiguration(new LKP_ItemEntityTypeConfiguration());
            modelBuilder.ApplyConfiguration(new LKP_Rating_ScaleEntityTypeConfiguration());
            modelBuilder.ApplyConfiguration(new ReviewEntityTypeConfiguration());

        }
    }
}
