﻿using Dispatching.Reviewing.Models.Entities;
using DispatchProduct.BuildingBlocks.BaseEntities.EntityConfigurations;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;

namespace Dispatching.Reviewing.Models.EntityConfigurations
{
    public class ReviewEntityTypeConfiguration : BaseEntityTypeConfiguration<Review>
    {
        public void Configure(EntityTypeBuilder<Review> ReviewConfiguration)
        {
            base.Configure(ReviewConfiguration);

            ReviewConfiguration.ToTable("Review");
            ReviewConfiguration.Property(o => o.Id).ValueGeneratedOnAdd();
            ReviewConfiguration.Property(o => o.FK_Reviewer_Id).IsRequired();
            ReviewConfiguration.Property(o => o.FK_LKP_Item_Id).IsRequired();
            ReviewConfiguration.Property(o => o.FK_LKP_Rating_Scale_Id).IsRequired();
            ReviewConfiguration.Property(o => o.Fk_Reviewed_Id).IsRequired(false);
            ReviewConfiguration.Property(o => o.Comment).IsRequired(false);
            ReviewConfiguration.Ignore(o => o.Item);
            ReviewConfiguration.Ignore(o => o.RatingScale);
            //ReviewConfiguration.Property(o => o.Id)
            // .ForSqlServerUseSequenceHiLo("ReviewSeq");
        }
    }
}