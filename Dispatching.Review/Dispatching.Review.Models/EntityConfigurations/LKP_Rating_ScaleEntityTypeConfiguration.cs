﻿using Dispatching.Reviewing.Models.Entities;
using DispatchProduct.BuildingBlocks.BaseEntities.EntityConfigurations;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;

namespace Dispatching.Reviewing.Models.EntityConfigurations
{
    public class LKP_Rating_ScaleEntityTypeConfiguration : BaseLKPEntityTypeConfiguration<LKP_Rating_Scale>
    {
        public void Configure(EntityTypeBuilder<LKP_Rating_Scale> LKP_Rating_ScaleConfiguration)
        {
            base.Configure(LKP_Rating_ScaleConfiguration);
            LKP_Rating_ScaleConfiguration.ToTable("LKP_Rating_Scale");
            LKP_Rating_ScaleConfiguration.Property(o => o.Id).ValueGeneratedOnAdd();
            LKP_Rating_ScaleConfiguration.Property(o => o.Degree).IsRequired(false);
            //LKP_Rating_ScaleConfiguration.Property(o => o.Id)
            // .ForSqlServerUseSequenceHiLo("RatingScale");
        }
    }
}