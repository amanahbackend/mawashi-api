﻿using DispatchProduct.BuildingBlocks.BaseEntities.IEntities;
using System;
using System.Collections.Generic;
using System.Text;

namespace Dispatching.Reviewing.Models.Entities
{
    public interface IReview : IBaseEntity
    {
        int Id { get; set; }
        int FK_Reviewer_Id { get; set; }
        int Fk_Reviewed_Id { get; set; }
        string Comment { get; set; }
        int FK_LKP_Item_Id { get; set; }
        int FK_LKP_Rating_Scale_Id { get; set; }
        LKP_Item Item { get; set; }
        LKP_Rating_Scale RatingScale { get; set; }
        string ReviewerName { get; set; }
        string ReviewerPhone { get; set; }
        string ReviewerMail { get; set; }
        string Title { get; set; }
    }
}
