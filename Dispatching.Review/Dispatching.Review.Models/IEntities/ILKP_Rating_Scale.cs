﻿using DispatchProduct.BuildingBlocks.BaseEntities.Entities;
using DispatchProduct.BuildingBlocks.BaseEntities.IEntities;
using System;
using System.Collections.Generic;
using System.Text;

namespace Dispatching.Reviewing.Models.Entities
{
    public interface ILKP_Rating_Scale:IBaseLKPEntity
    {
         int Degree { get; set; }
         int FK_Item_Id { get; set; }
    }
}
