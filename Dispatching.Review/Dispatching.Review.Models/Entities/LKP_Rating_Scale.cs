﻿using DispatchProduct.BuildingBlocks.BaseEntities.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace Dispatching.Reviewing.Models.Entities
{
    public class LKP_Rating_Scale:BaseLKPEntity, ILKP_Rating_Scale
    {
        public int Degree { get; set; }
        public int FK_Item_Id { get; set; }
    }
}
