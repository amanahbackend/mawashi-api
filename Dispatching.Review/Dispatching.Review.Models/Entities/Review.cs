﻿using DispatchProduct.BuildingBlocks.BaseEntities.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace Dispatching.Reviewing.Models.Entities
{
    public class Review:BaseEntity,IReview
    {
        public int FK_Reviewer_Id  { get; set; }
        public int Fk_Reviewed_Id { get; set; }
        public string Comment { get; set; }
        public int FK_LKP_Item_Id { get; set; }
        public int FK_LKP_Rating_Scale_Id { get; set; }
        public LKP_Item Item { get; set; }
        public LKP_Rating_Scale RatingScale { get; set; }
        public string ReviewerName { get; set; }
        public string ReviewerPhone { get; set; }
        public string ReviewerMail { get; set; }
        public string Title { get; set; }
    }
}
