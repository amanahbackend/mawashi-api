﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Threading.Tasks;
using Autofac;
using Autofac.Extensions.DependencyInjection;
using AutoMapper;
using BuildingBlocks.ServiceDiscovery;
using Dispatching.Settings.API.ServicesCommunication.Cart;
using Dispatching.Settings.API.ServicesCommunication.Inventory;
using Dispatching.Settings.API.ServicesSettings.Cart;
using Dispatching.Settings.API.ServicesSettings.Inventory;
using Dispatching.Settings.BLL.IManagers;
using Dispatching.Settings.BLL.Managers;
using Dispatching.Settings.Models.Context;
using Dispatching.Settings.Models.Entities;
using Dispatching.Settings.Models.Settings;
using DispatchProduct.RepositoryModule;
using DnsClient;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Newtonsoft.Json.Serialization;
using Swashbuckle.AspNetCore.Swagger;
using Utilites.PaginatedItems;
using Utilites.ProcessingResult;
using Utilities.Utilites;

namespace Dispatching.Settings.API
{
    public class Startup
    {
        public IConfigurationRoot Configuration { get; }
        public IHostingEnvironment _env;
        public Startup(IHostingEnvironment env)
        {
            _env = env;
            var builder = new ConfigurationBuilder()
                .SetBasePath(env.ContentRootPath)
                .AddEnvironmentVariables();
            Configuration = builder.Build();

            var country = Configuration.GetSection("DockerContainerName").Value.Split('.').Last().ToLower();
            // var country = "kw";
            builder = new ConfigurationBuilder()
               .SetBasePath(env.ContentRootPath)
               .AddJsonFile($"appsettings.{country}.json", optional: false, reloadOnChange: true)
               .AddEnvironmentVariables();

            Configuration = builder.Build();
        }



        // This method gets called by the runtime. Use this method to add services to the container.
        public IServiceProvider ConfigureServices(IServiceCollection services)
        {
            services.AddCors(options =>
            {
                options.AddPolicy("AllowAll",
                    builder =>
                    {
                        builder
                        .AllowAnyOrigin()
                        .AllowAnyMethod()
                        .AllowAnyHeader()
                        .AllowCredentials();
                    });
            });
            services.AddSingleton(provider => Configuration);
            services.Configure<AppSettings>(Configuration);
            services.AddDbContext<DataContext>(options =>
           options.UseSqlServer(Configuration["ConnectionString"],
            sqlOptions => sqlOptions.MigrationsAssembly("Dispatching.Settings.EFCore.MSSQL")));
            services.AddOptions();
            services.AddMvc().AddJsonOptions(options =>
            {
                options.SerializerSettings.ContractResolver = new CamelCasePropertyNamesContractResolver();
            });
            services.AddAutoMapper(typeof(Startup));
            Mapper.AssertConfigurationIsValid();

            services.Configure<CartServiceSetting>
               (Configuration.GetSection("ServiceCommunications:CartService"));

            services.Configure<InventoryServiceSetting>
               (Configuration.GetSection("ServiceCommunications:InventoryService"));

            services.AddScoped<ICartService, CartService>();
            services.AddScoped<IInventoryService, InventoryService>();
            #region Data & Managers
            services.AddScoped<DbContext, DataContext>();
            services.AddScoped(typeof(IRepository<>), typeof(Repository<>));
            services.AddScoped(typeof(IProcessResultMapper), typeof(ProcessResultMapper));
            services.AddScoped(typeof(IProcessResultPaginatedMapper), typeof(ProcessResultPaginatedMapper));
            services.AddScoped(typeof(IPaginatedItems<>), typeof(PaginatedItems<>));

            services.AddScoped(typeof(ISetting), typeof(Setting));
            services.AddScoped(typeof(ISettingGroup), typeof(SettingGroup));

            services.AddScoped(typeof(ISettingManager), typeof(SettingManager));
            services.AddScoped(typeof(ISettingGroupManager), typeof(SettingGroupManager));
            #endregion
            string consulContainerName = Configuration.GetSection("ServiceRegisteryContainerName").Value;
            int consulPort = Convert.ToInt32(Configuration.GetSection("ServiceRegisteryPort").Value);
            IPAddress ip = Dns.GetHostEntry(consulContainerName).AddressList.Where(o => o.AddressFamily == AddressFamily.InterNetwork).First();
            services.AddSingleton<IDnsQuery>(new LookupClient(ip, consulPort));
            services.AddServiceDiscovery(Configuration.GetSection("ServiceDiscovery"));

            // Register the Swagger generator, defining one or more Swagger documents
            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1", new Info { Title = "", Version = "v1" });
            });

            services.AddApiVersioning(o => o.ReportApiVersions = true);
            var container = new ContainerBuilder();
            container.Populate(services);
            return new AutofacServiceProvider(container.Build());
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            app.UseCors("AllowAll");

            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
                app.UseDatabaseErrorPage();
            }
            //else
            //{
            //    app.UseExceptionHandler("/Home/Error");
            //}

            // Enable middleware to serve generated Swagger as a JSON endpoint.
            app.UseStaticFiles();
            app.UseSwagger();
            // Enable middleware to serve swagger-ui (HTML, JS, CSS, etc.), specifying the Swagger JSON endpoint.
            app.UseSwaggerUI(c =>
            {
                c.SwaggerEndpoint("/swagger/v1/swagger.json", "");
            });


            app.UseMvc();

            // Autoregister using server.Features (does not work in reverse proxy mode)
            app.UseConsulRegisterService();
        }
    }
}


