﻿using System;
using System.Collections.Generic;
using System.Text;
using DispatchProduct.RepositoryModule;
using Microsoft.AspNetCore.Mvc;
using DispatchProduct.Controllers;
using AutoMapper;
using Utilites.ProcessingResult;
using DispatchProduct.Controllers.V1;
using Dispatching.Settings.BLL.Managers;
using Dispatching.Settings.Models.Entities;
using Dispatching.Settings.ViewModel;
using Dispatching.Settings.BLL.IManagers;
using Dispatching.Settings.API.ServicesCommunication.Cart;
using Dispatching.Settings.API.ServicesCommunication.Inventory;
using Dispatching.Settings.API.ViewModel;
using System.Threading.Tasks;
using Dispatching.Settings.API.ServicesViewModels.Cart;
using System.Linq;
using Dispatching.Settings.API.ServicesViewModels.Inventory;

namespace Dispatching.Settings.API.Controllers
{
    [ApiVersion("1.0")]
    [Route("v{version:apiVersion}/[controller]")]
    public class SettingController : BaseControllerV1<ISettingManager, Setting, SettingViewModel>
    {
        public ISettingManager manger;
        public readonly new IMapper mapper;
        IProcessResultMapper processResultMapper;
        IProcessResultPaginatedMapper processResultPaginatedMapper;
        ICartService cartService;
        IInventoryService inventoryService;

        public SettingController(ISettingManager _manger, IMapper _mapper, IProcessResultMapper _processResultMapper,
            IProcessResultPaginatedMapper _processResultPaginatedMapper, ICartService _cartService,
            IInventoryService _inventoryService) : base(_manger, _mapper, _processResultMapper, _processResultPaginatedMapper)
        {
            cartService = _cartService;
            inventoryService = _inventoryService;
            manger = _manger;
            mapper = _mapper;
            processResultMapper = _processResultMapper;
            processResultPaginatedMapper = _processResultPaginatedMapper;
        }

        [HttpGet, Route("GetSetting/{key}")]
        public ProcessResultViewModel<List<SettingViewModel>> GetSetting([FromRoute] string key)
        {
            var entityRes = manger.GetByGroupName(key);
            return processResultMapper.Map<List<Setting>, List<SettingViewModel>>(entityRes);
        }

        [HttpGet, Route("GetOrderDeliveryDay")]
        public async Task<ProcessResultViewModel<OrderDeliveryDay?>> GetOrderDeliveryDay([FromQuery] int cartId)
        {
            ProcessResultViewModel<OrderDeliveryDay?> result = new ProcessResultViewModel<OrderDeliveryDay?>();
            try
            {
                var cartResult = await cartService.GetCartWithItems(cartId);
                if (cartResult != null && cartResult.IsSucceeded && cartResult.Data != null)
                {
                    var categoryIds = cartResult.Data.CartItems.Select(x => x.Item.FK_Category_Id).ToList();
                    //var categoryIds = new List<int>();
                    //categoryIds.Add(61);
                    //categoryIds.Add(62);

                    var categoriesResult = await inventoryService.GetByIds(categoryIds);
                    if (categoriesResult != null && categoriesResult.IsSucceeded && categoriesResult.Data != null)
                    {
                        var allowTodayDeliveryLst = categoriesResult.Data.Select(x => x.AllowTodayDelivery).ToList();
                        bool allowTodayDelivery;
                        if (allowTodayDeliveryLst.Count > 1)
                        {
                            allowTodayDelivery = !allowTodayDeliveryLst.Contains(false);
                        }
                        else
                        {
                            allowTodayDelivery = allowTodayDeliveryLst.FirstOrDefault();
                        }

                        var settings = GetSetting("ORDER_TIME_OUT").Data;
                        var DEFAULT_TIME_OUT = Convert.ToInt32(settings.FirstOrDefault(x => x.Key.Equals("DEFAULT_TIME_OUT")).Value);
                        var LAST_TODAY_ORDER = Convert.ToInt32(settings.FirstOrDefault(x => x.Key.Equals("LAST_TODAY_ORDER")).Value);

                        if (allowTodayDelivery &&
                            DateTime.UtcNow.Hour < DEFAULT_TIME_OUT &&
                            DateTime.UtcNow.Hour < LAST_TODAY_ORDER)
                        {
                            result = ProcessResultViewModelHelper.Succedded((OrderDeliveryDay?)OrderDeliveryDay.Today);
                        }
                        else
                        if (!allowTodayDelivery &&
                            DateTime.UtcNow.Hour < DEFAULT_TIME_OUT &&
                            DateTime.UtcNow.Hour < LAST_TODAY_ORDER)
                        {
                            result = ProcessResultViewModelHelper.Succedded((OrderDeliveryDay?)OrderDeliveryDay.Tomorrow);
                        }
                        else
                        if (DateTime.UtcNow.Hour < DEFAULT_TIME_OUT &&
                            DateTime.UtcNow.Hour >= LAST_TODAY_ORDER)
                        {
                            result = ProcessResultViewModelHelper.Succedded((OrderDeliveryDay?)OrderDeliveryDay.Tomorrow);
                        }
                        else
                        if (DateTime.UtcNow.Hour >= DEFAULT_TIME_OUT)
                        {
                            result = ProcessResultViewModelHelper.Succedded((OrderDeliveryDay?)OrderDeliveryDay.AfterTomorrow);
                        }
                        else
                        {
                            result = ProcessResultViewModelHelper.Succedded((OrderDeliveryDay?)OrderDeliveryDay.AfterTomorrow);
                        }
                    }
                    else
                    {
                        result = ProcessResultViewModelHelper.MapToProcessResultViewModel<List<LKP_CategoryViewModel>, OrderDeliveryDay?>(categoriesResult);
                    }
                }
                else
                {
                    result = ProcessResultViewModelHelper.MapToProcessResultViewModel<CartViewModel, OrderDeliveryDay?>(cartResult);
                }
            }
            catch (Exception ex)
            {
                result = ProcessResultViewModelHelper.Failed<OrderDeliveryDay?>(null, ex.Message);
            }
            return result;
        }
    }
}

