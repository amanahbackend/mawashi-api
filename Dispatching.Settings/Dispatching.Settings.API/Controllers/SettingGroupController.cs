﻿using System;
using System.Collections.Generic;
using System.Text;
using DispatchProduct.RepositoryModule;
using Dispatching.Settings.Models.Entities;
using Microsoft.AspNetCore.Mvc;
using DispatchProduct.Controllers;
using AutoMapper;
using Utilites.ProcessingResult;
using DispatchProduct.Controllers.V1;
using Dispatching.Settings.BLL.Managers;
using Dispatching.Settings.ViewModel;
using Dispatching.Settings.BLL.IManagers;

namespace Dispatching.SettingGroups.API.Controllers
{
    [ApiVersion("1.0")]
    [Route("v{version:apiVersion}/[controller]")]
    public class SettingGroupController : BaseControllerV1<ISettingGroupManager, SettingGroup, SettingGroupViewModel>
    {
        public SettingGroupController(ISettingGroupManager _manger, IMapper _mapper, IProcessResultMapper _processResultMapper, IProcessResultPaginatedMapper _processResultPaginatedMapper) : base(_manger, _mapper, _processResultMapper, _processResultPaginatedMapper)
        {

        }
    }
}

