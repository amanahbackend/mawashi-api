﻿

using Dispatching.Settings.Models.Context;
using Dispatching.Settings.Models.Entities;
using Dispatching.Settings.Models.Settings;
using DispatchProduct.RepositoryModule;
using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DispatchProduct.Ordering.API.Seed
{
    public class DataContextSeed : ContextSeed
    {
        AppSettings settings = null;
        public async Task SeedAsync(IOptions<AppSettings> _settings, DataContext context, IHostingEnvironment env,
            ILogger<DataContextSeed> logger, int? retry = 0)
        {
            int retryForAvaiability = retry.Value;
            settings = _settings.Value;
            try
            {
                var contentRootPath = env.ContentRootPath;
                var webroot = env.WebRootPath;
                var lstDeaultSettings = GetDefaultSettingGroup();
                await SeedEntityAsync(context, lstDeaultSettings);

            }
            catch (Exception ex)
            {
                if (retryForAvaiability < 3)
                {
                    retryForAvaiability++;

                    logger.LogError(ex.Message, $"There is an error migrating data for CustomerDbContextSeed");

                    await SeedAsync(_settings, context, env, logger, retryForAvaiability);
                }
            }
        }
        private List<SettingGroup> GetDefaultSettingGroup()
        {
            //var seedData = settings.SeedSettingGroup;
            List<SettingGroup> result = new List<SettingGroup>()
            {
                 new SettingGroup() { NameEN = "SocialMedia",NameAR = "التواصل الاجتماعى"},
                 new SettingGroup(){ NameEN = "Emails",NameAR = "البريد الالكترونى"},
                 new SettingGroup(){NameEN = "Phones",NameAR = "ارقام التليفونات"}
            };
            //foreach (var item in seedData)
            //{
            //    var obj = new SettingGroup() { NameAR = item.NameAR, NameEN = item.NameEN };
            //    result.Add(obj);
            //}

            return result;
        }
    }
}
