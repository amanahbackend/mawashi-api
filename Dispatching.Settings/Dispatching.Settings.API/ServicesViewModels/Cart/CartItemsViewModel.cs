﻿using CommonEnums;
using Dispatching.BuisnessCommon.Enums;
using DispatchProduct.BuildingBlocks.BaseEntities.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Dispatching.Settings.API.ServicesViewModels.Cart
{
    public class CartItemsViewModel : RepoistryBaseEntity
    {
        public int Fk_Item_Id { get; set; }
        public LKP_ItemViewModel Item { get; set; }
        public object Cutting { get; set; }
        public object Quantity { get; set; }
        public double ItemFinalPrice { get; set; }
        public double ItemDiscount { get; set; }
        public double ItemNetPrice { get; set; }
        public int FK_Cutting_Id { get; set; }
        public double CuttingPrice { get; set; }
        public int FK_Quantity_Id { get; set; }
        public int FK_Cart_Id { get; set; }
        public int QuantityValue { get; set; }
        public double ItemQuantityByKG { get; set; }
        public int FK_CategoryConfig_Id { get; set; }
        public int FK_Option_Id { get; set; }
        public object CategorySubOption { get; set; }
        public int FK_Order_Id { get; set; }
        public bool AcceptMulti { get; set; }
        public double Price { get; set; }
        public double SalesPricePerItem { get; set; }
        public bool IsSalesChangePrice { get; set; }
        public OrderType OrderType { get; set; }
        public MeasurmentType MeasurmentType { get; set; }
        public EnumEntity MeasurmentUnit { get; set; }
        public double VAT { get; set; }
        public CartViewModel Cart { get; set; }
    }
}
