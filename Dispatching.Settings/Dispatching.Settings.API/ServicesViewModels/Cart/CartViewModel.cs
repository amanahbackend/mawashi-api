﻿using Dispatching.BuisnessCommon.Enums;
using DispatchProduct.BuildingBlocks.BaseEntities.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Dispatching.Settings.API.ServicesViewModels.Cart
{
    public class CartViewModel : RepoistryBaseEntity
    {
        public int FK_Customer_Id { get; set; }
        public double Price { get; set; }
        public double VAT { get; set; }
        public int FK_Order_Id { get; set; }
        public bool HasOrder { get; set; }
        public List<CartItemsViewModel> CartItems { get; set; }
        public OrderType OrderType { get; set; }
    }
}
