﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Dispatching.Settings.API.ViewModel
{
    public enum OrderDeliveryDay
    {
        Today = 0,
        Tomorrow = 1,
        AfterTomorrow = 2
    }
}
