﻿using AutoMapper;
using Dispatching.Settings.Models.Entities;
using Dispatching.Settings.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Utilites.PaginatedItems;
using Utilites.PaginatedItemsViewModel;
using Utilities.Utilites;

namespace Dispatching.Inventory.API.AutoMapperConfig
{
    public class MappingProfile : Profile
    {
        public MappingProfile()
        {
            CreateMap<Setting, SettingViewModel>();
            CreateMap<SettingViewModel, Setting>()
                .IgnoreBaseEntityProperties();

            CreateMap<SettingGroup, SettingGroupViewModel>();
            CreateMap<SettingGroupViewModel, SettingGroup>()
                .IgnoreBaseEntityProperties();

        }
    }
}
