﻿using Dispatching.Settings.API.ServicesSettings.Inventory;
using Dispatching.Settings.API.ServicesViewModels.Inventory;
using DispatchProduct.HttpClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Dispatching.Settings.API.ServicesCommunication.Inventory
{
    public interface IInventoryService : IDefaultHttpClientCrud<InventoryServiceSetting, LKP_CategoryViewModel, LKP_CategoryViewModel>
    {
    }
}
