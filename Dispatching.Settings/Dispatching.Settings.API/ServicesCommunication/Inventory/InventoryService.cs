﻿using Dispatching.Settings.API.ServicesSettings.Inventory;
using Dispatching.Settings.API.ServicesViewModels.Inventory;
using DispatchProduct.HttpClient;
using DnsClient;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Dispatching.Settings.API.ServicesCommunication.Inventory
{
    public class InventoryService: DefaultHttpClientCrud<InventoryServiceSetting, LKP_CategoryViewModel, LKP_CategoryViewModel>, IInventoryService
    {
        InventoryServiceSetting settings;
        IDnsQuery dnsQuery;
        public InventoryService(IOptions<InventoryServiceSetting> _settings, IDnsQuery _dnsQuery) : base(_settings.Value, _dnsQuery)
        {
            settings = _settings.Value;
            dnsQuery = _dnsQuery;
        }
    }
}
