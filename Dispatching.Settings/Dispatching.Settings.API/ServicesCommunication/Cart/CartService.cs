﻿using Dispatching.Settings.API.ServicesSettings.Cart;
using Dispatching.Settings.API.ServicesViewModels.Cart;
using DispatchProduct.HttpClient;
using DnsClient;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Utilites.ProcessingResult;

namespace Dispatching.Settings.API.ServicesCommunication.Cart
{
    public class CartService : DefaultHttpClientCrud<CartServiceSetting, CartViewModel, CartViewModel>, ICartService
    {
        CartServiceSetting settings;
        IDnsQuery dnsQuery;

        public CartService(IOptions<CartServiceSetting> _settings, IDnsQuery _dnsQuery) : base(_settings.Value, _dnsQuery)
        {
            settings = _settings.Value;
            dnsQuery = _dnsQuery;
        }

        public async Task<ProcessResultViewModel<CartViewModel>> GetCartWithItems(int cartId)
        {
            var requesturi = $"{await settings.GetUri(dnsQuery)}/{settings.GetCartWithItemsVerb}/{cartId}";
            string baseUrl = await settings.GetUri(dnsQuery);
            while (baseUrl.EndsWith('/'))
            {
                baseUrl = baseUrl.Substring(0, baseUrl.Length - 1);
            }
            var result = await GetByUri(requesturi);
            return result;
        }
    }
}
