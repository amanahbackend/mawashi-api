﻿using Dispatching.Settings.API.ServicesSettings.Cart;
using Dispatching.Settings.API.ServicesViewModels.Cart;
using DispatchProduct.HttpClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Utilites.ProcessingResult;

namespace Dispatching.Settings.API.ServicesCommunication.Cart
{
    public interface ICartService : IDefaultHttpClientCrud<CartServiceSetting, CartViewModel, CartViewModel>
    {
        Task<ProcessResultViewModel<CartViewModel>> GetCartWithItems(int cartId);
    }
}
