﻿using System.Collections.Generic;
using Utilities.Utilites;

namespace Dispatching.Settings.Models.Settings
{
    public class AppSettings
    {
        public List<SettingSeedObject> SeedSettingGroup { get; set; }
    }
}
