﻿using CommonEnums;
using DispatchProduct.BuildingBlocks.BaseEntities.Entities;
using DispatchProduct.BuildingBlocks.BaseEntities.IEntities;
using System;
using System.Collections.Generic;
using System.Text;

namespace Dispatching.Settings.Models.Entities
{
    public interface ISetting: IBaseEntity
    {
         string Key { get; set; }
         int FK_Group_Id { get; set; }
         string Value { get; set; }
         SettingGroup Group { get; set; }
    }
}
