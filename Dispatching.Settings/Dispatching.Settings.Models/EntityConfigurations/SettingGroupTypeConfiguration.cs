﻿using Dispatching.Settings.Models.Entities;
using DispatchProduct.BuildingBlocks.BaseEntities.EntityConfigurations;
using Microsoft.EntityFrameworkCore;

using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;

namespace Dispatching.Settings.Models.EntityConfigurations
{
    public class SettingGroupTypeConfiguration : BaseLKPEntityTypeConfiguration<SettingGroup>
    {
        public void Configure(EntityTypeBuilder<SettingGroup> builder)
        {
            base.Configure(builder);
            builder.ToTable("SettingGroup");
        }
    }
}
