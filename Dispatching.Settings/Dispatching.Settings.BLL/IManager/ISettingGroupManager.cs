﻿using Dispatching.Settings.Models.Entities;
using DispatchProduct.RepositoryModule;
using System;
using System.Collections.Generic;
using System.Text;
using Utilites.ProcessingResult;

namespace Dispatching.Settings.BLL.IManagers
{
    public interface ISettingGroupManager : IRepository<SettingGroup>
    {
        ProcessResult<SettingGroup> GetByGroupName(string grpName);
    }
}
