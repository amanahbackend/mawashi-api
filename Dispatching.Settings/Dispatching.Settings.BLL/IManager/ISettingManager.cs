﻿using Dispatching.Settings.Models.Entities;
using DispatchProduct.RepositoryModule;
using System;
using System.Collections.Generic;
using System.Text;
using Utilites.ProcessingResult;

namespace Dispatching.Settings.BLL.IManagers
{
    public interface ISettingManager : IRepository<Setting>
    {
        ProcessResult<List<Setting>> GetByGroupName(string grpName);
    }
}
