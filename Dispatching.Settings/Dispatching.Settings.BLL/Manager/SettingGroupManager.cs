﻿using System;
using System.Collections.Generic;
using System.Text;
using DispatchProduct.RepositoryModule;
using Utilites.ProcessingResult;
using System.Linq;
using Dispatching.Settings.Models.Entities;
using Dispatching.Settings.Models.Context;
using Dispatching.Settings.BLL.IManagers;

namespace Dispatching.Settings.BLL.Managers
{
    public class SettingGroupManager : Repository<SettingGroup>, ISettingGroupManager
    {
        public SettingGroupManager(DataContext context) : base(context)
        {
        }
        public ProcessResult<SettingGroup> GetByGroupName(string grpName)
        {
            ProcessResult<SettingGroup> result = null;

           var data= GetAllQuerable().Data.Where(grp => grp.NameAR == grpName || grp.NameEN == grpName).FirstOrDefault();

            if (data != null)
            {
                result= ProcessResultHelper.Succedded(data);
            }
            else
            {
                result=ProcessResultHelper.Failed(data, new Exception($"No group setting Name called {grpName} "));
            }
            return result;
        }
    }
}
