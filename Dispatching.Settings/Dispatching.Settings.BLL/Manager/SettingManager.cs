﻿using System;
using System.Collections.Generic;
using System.Text;
using DispatchProduct.RepositoryModule;
using Utilites.ProcessingResult;
using System.Linq;
using Dispatching.Settings.Models.Context;
using Dispatching.Settings.Models.Entities;
using Dispatching.Settings.BLL.IManagers;
using Microsoft.Extensions.DependencyInjection;
namespace Dispatching.Settings.BLL.Managers
{
    public class SettingManager : Repository<Setting>, ISettingManager
    {
        IServiceProvider serviceProvider;
        public SettingManager(IServiceProvider _serviceProvider, DataContext context) : base(context)
        {
            serviceProvider = _serviceProvider;
        }
        private ISettingGroupManager SettingGroupManager
        {
            get { return serviceProvider.GetService<ISettingGroupManager>(); }
        }
        public ProcessResult<List<Setting>> GetByGroupName(string grpName)
        {
            ProcessResult<List<Setting>> result = null;
            var grpRes = SettingGroupManager.GetByGroupName(grpName);
            if (grpRes.IsSucceeded && grpRes.Data != null)
            {
                var data = GetAllQuerable().Data.Where(setting => setting.FK_Group_Id == grpRes.Data.Id).ToList();
                if (data.Count>0)
                {
                    result = ProcessResultHelper.Succedded(data);
                }
                else
                {
                    result = ProcessResultHelper.Failed(data, new Exception($"No settings found for this group {grpName}"));
                }
            }
            else
            {
                result = ProcessResultHelper.MapToProcessResult<SettingGroup, List<Setting>>(grpRes);
            }
            return result;
        }
    }
}
