﻿using CommonEnums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Dispatching.Content.API.ViewModels
{
    public class AdViewModel
    {
        public string FileContent { get; set; }
        public string AdFileName { get; set; }
        public Country Country { get; set; }
    }
}
