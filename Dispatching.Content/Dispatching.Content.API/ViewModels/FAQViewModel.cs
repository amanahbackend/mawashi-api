﻿using CommonEnums;
using DispatchProduct.BuildingBlocks.BaseEntities.IEntities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Dispatching.Content.API.ViewModels
{
    public class FAQViewModel 
    {
        public int Id { get; set; }
        public string Question { get; set; }
        public string Answer { get; set; }
        public Country Fk_Country_Id { get; set; }
    }
}
