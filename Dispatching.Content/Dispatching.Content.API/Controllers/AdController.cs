﻿using CommonEnums;
using Dispatching.Content.API.ViewModels;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Utilites;
using Utilites.ProcessingResult;
using Utilities;

namespace Dispatching.Content.API.Controllers
{
    [ApiVersion("1.0")]
    [Route("v{version:apiVersion}/[controller]")]
    public class AdController : Controller
    {
        private IConfigurationRoot _configuration;
        private IHostingEnvironment _hostingEnvironment;

        public AdController(IHostingEnvironment hostingEnvironment, IConfigurationRoot configuration)
        {
            _hostingEnvironment = hostingEnvironment;
            _configuration = configuration;
        }

        public string UaeAdsDir
        {
            get
            {
                string uaeDirPath = $"{_hostingEnvironment.WebRootPath}/uae/ads";
                return uaeDirPath;
            }
        }
        public string KwAdsDir
        {
            get
            {
                string kwDirPath = $"{_hostingEnvironment.WebRootPath}/kw/ads";

                return kwDirPath;
            }
        }

        public string UaeArchivedAdsDir
        {
            get
            {
                string uaeDirPath = $"{_hostingEnvironment.WebRootPath}/uae/ads/archived";
                return uaeDirPath;
            }
        }
        public string KwArchivedAdsDir
        {
            get
            {
                string kwDirPath = $"{_hostingEnvironment.WebRootPath}/kw/ads/archived";
                return kwDirPath;
            }
        }



        [HttpPost, Route("Add"), MapToApiVersion("1.0")]
        public ProcessResultViewModel<string> Add([FromBody]AdViewModel model)
        {
            ProcessResultViewModel<string> result = null;
            try
            {
                string fileExt = StringUtilities.GetFileExtension(model.FileContent);
                string rand = Guid.NewGuid().ToString().Split('-').First();
                string fileName = $"ad_{rand}.{fileExt}";
                string filePath = "";
                if (model.Country == Country.Kuwait)
                {
                    filePath = $"{KwAdsDir}/{fileName}";
                }
                else if (model.Country == Country.UAE)
                {
                    filePath = $"{UaeAdsDir}/{fileName}";
                }
                FileUtilities.WriteFileToPath(model.FileContent, filePath);
                var adUrl = BindFileURL(filePath, _configuration);
                result = ProcessResultViewModelHelper.Succedded(adUrl);
            }
            catch (Exception ex)
            {
                result = ProcessResultViewModelHelper.Failed<string>(null, ex.Message);
            }
            return result;
        }

        [HttpGet, Route("GetActive"), MapToApiVersion("1.0")]
        public ProcessResultViewModel<List<string>> GetActive([FromQuery]Country country)
        {
            ProcessResultViewModel<List<string>> result = null;
            try
            {
                List<string> adUrls = new List<string>();
                List<string> filePaths = new List<string>();
                if (country == Country.Kuwait)
                {
                    filePaths = Directory.GetFiles(KwAdsDir).ToList();
                }
                else if (country == Country.UAE)
                {
                    filePaths = Directory.GetFiles(UaeAdsDir).ToList();
                }
                foreach (var item in filePaths)
                {
                    string adUrl = BindFileURL(item, _configuration);
                    adUrls.Add(adUrl);
                }
                result = ProcessResultViewModelHelper.Succedded(adUrls);
            }
            catch (Exception ex)
            {
                result = ProcessResultViewModelHelper.Failed<List<string>>(null, ex.Message);
            }
            return result;
        }

        [HttpGet, Route("GetArchived"), MapToApiVersion("1.0")]
        public ProcessResultViewModel<List<string>> GetArchived([FromQuery]Country country)
        {
            ProcessResultViewModel<List<string>> result = null;
            try
            {
                List<string> adUrls = new List<string>();
                List<string> filePaths = new List<string>();
                if (country == Country.Kuwait)
                {
                    filePaths = Directory.GetFiles(KwArchivedAdsDir).ToList();
                }
                else if (country == Country.UAE)
                {
                    filePaths = Directory.GetFiles(UaeArchivedAdsDir).ToList();
                }
                foreach (var item in filePaths)
                {
                    string adUrl = BindFileURL(item, _configuration);
                    adUrls.Add(adUrl);
                }
                result = ProcessResultViewModelHelper.Succedded(adUrls);
            }
            catch (Exception ex)
            {
                result = ProcessResultViewModelHelper.Failed<List<string>>(null, ex.Message);
            }
            return result;
        }

        [HttpPut, Route("Activate"), MapToApiVersion("1.0")]
        public ProcessResultViewModel<string> Activate([FromBody] AdViewModel model)
        {
            ProcessResultViewModel<string> result = null;
            try
            {
                string fileUrl = string.Empty;
                if (model.Country == Country.Kuwait)
                {
                    fileUrl = ActivateFile(KwAdsDir, KwArchivedAdsDir, model.AdFileName);
                }
                else if (model.Country == Country.UAE)
                {
                    fileUrl = ActivateFile(UaeAdsDir, UaeArchivedAdsDir, model.AdFileName);
                }
                result = String.IsNullOrEmpty(fileUrl) ?
                    ProcessResultViewModelHelper.Failed<string>(null, "Ad file not found") :
                    ProcessResultViewModelHelper.Succedded(fileUrl);
            }
            catch (Exception ex)
            {
                result = ProcessResultViewModelHelper.Failed<string>(null, ex.Message);
            }
            return result;
        }

        [HttpPut, Route("Archive"), MapToApiVersion("1.0")]
        public ProcessResultViewModel<string> Archive([FromBody] AdViewModel model)
        {
            ProcessResultViewModel<string> result = null;
            try
            {
                string fileUrl = string.Empty;
                if (model.Country == Country.Kuwait)
                {
                    fileUrl = ArchiveFile(KwAdsDir, KwArchivedAdsDir, model.AdFileName);
                }
                else if (model.Country == Country.UAE)
                {
                    fileUrl = ArchiveFile(UaeAdsDir, UaeArchivedAdsDir, model.AdFileName);
                }
                result = String.IsNullOrEmpty(fileUrl) ?
                    ProcessResultViewModelHelper.Failed<string>(null, "Ad file not found") :
                    ProcessResultViewModelHelper.Succedded(fileUrl);
            }
            catch (Exception ex)
            {
                result = ProcessResultViewModelHelper.Failed<string>(null, ex.Message);
            }
            return result;
        }




        private string ActivateFile(string activeDir, string archiveDir, string adFileName)
        {
            var files = Directory.EnumerateFiles(archiveDir).Select(s => s.Split("/").Last());
            if (files.Contains(adFileName))
            {
                var base64File = FileUtilities.GetFileBase64($"{archiveDir}/{adFileName}");
                string newPath = $"{activeDir}/{adFileName}";
                FileUtilities.WriteFileToPath(base64File, newPath);
                string fileUrl = BindFileURL(newPath, _configuration);
                FileUtilities.DeleteFileIfExist($"{archiveDir}/{adFileName}");
                return fileUrl;
            }
            return String.Empty;
        }

        private string ArchiveFile(string activeDir, string archiveDir, string adFileName)
        {
            var files = Directory.EnumerateFiles(activeDir).Select(s => s.Split("/").Last());
            if (files.Contains(adFileName))
            {
                var base64File = FileUtilities.GetFileBase64($"{activeDir}/{adFileName}");
                string newPath = $"{archiveDir}/{adFileName}";
                FileUtilities.WriteFileToPath(base64File, newPath);
                string fileUrl = BindFileURL(newPath, _configuration);
                FileUtilities.DeleteFileIfExist($"{activeDir}/{adFileName}");
                return fileUrl;
            }
            return String.Empty;
        }

        private string BindFileURL(string filePath, IConfigurationRoot configuration)
        {
            string result = "";
            var baseUrl = configuration["ApiGatewayBaseUrl"];

            var identityServiceName = configuration["ServiceDiscovery:ServiceName"];

            var basePath = $"{baseUrl}/{identityServiceName}";

            if (filePath != null)
            {
                filePath = filePath.Replace('\\', '/');
                var segments = filePath.Split('/');
                filePath = filePath.Remove(0, segments[1].Length + 1).Remove(0, segments[2].Length + 1);
                result = $"{basePath}{filePath}";
            }
            return result;
        }
    }
}
