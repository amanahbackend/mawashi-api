﻿using AutoMapper;
using CommonEnums;
using Dispatching.Content.API.ViewModels;
using Dispatching.Content.BLL.IManagers;
using Dispatching.Content.Models.Entities;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Utilites.ProcessingResult;

namespace Dispatching.Content.API.Controllers
{
    [ApiVersion("1.0")]
    [Route("v{version:apiVersion}/[controller]")]
    public class FAQController
    {
        private IConfigurationRoot _configuration;
        private IHostingEnvironment _hostingEnvironment;
        private IFAQManager _manger;
        private readonly IMapper _mapper;
        private IProcessResultMapper _processResultMapper;

        public FAQController(IHostingEnvironment hostingEnvironment,
            IConfigurationRoot configuration, IFAQManager manager, IMapper mapper,
            IProcessResultMapper processResultMapper)
        {
            _hostingEnvironment = hostingEnvironment;
            _configuration = configuration;
            _manger = manager;
            _processResultMapper = processResultMapper;
            _mapper = mapper;
        }

        [Route("Add"), HttpPost]
        public virtual ProcessResultViewModel<FAQViewModel> Post([FromBody]FAQViewModel model)
        {
            var entityModel = _mapper.Map<FAQViewModel, FAQ>(model);
            var entityResult = _manger.Add(entityModel);
            return _processResultMapper.Map<FAQ, FAQViewModel>(entityResult);
        }

        [Route("GetByCountry/{country}"), HttpGet]
        public virtual ProcessResultViewModel<List<FAQViewModel>> GetByCountry([FromRoute]Country country)
        {
            var entityResult = _manger.GetByCountry(country);
            return _processResultMapper.Map<List<FAQ>, List<FAQViewModel>>(entityResult);
        }

        [HttpGet, Route("Get/{id}")]
        public virtual ProcessResultViewModel<FAQViewModel> Get([FromRoute]int id)
        {
            var entityResult = _manger.Get(id);
            return _processResultMapper.Map<FAQ, FAQViewModel>(entityResult);
        }

        [HttpGet, Route("GetAll")]
        public virtual ProcessResultViewModel<List<FAQViewModel>> Get()
        {
            var entityResult = _manger.GetAll();
            return _processResultMapper.Map<List<FAQ>, List<FAQViewModel>>(entityResult);
        }

        [HttpPut]
        [Route("Update")]
        public virtual ProcessResultViewModel<bool> Put([FromBody]FAQViewModel model)
        {
            var entitymodel = _mapper.Map<FAQViewModel, FAQ>(model);
            var result = _manger.Update(entitymodel);
            return _processResultMapper.Map<bool, bool>(result);
        }
        [HttpDelete]
        [Route("Delete/{id}")]
        public virtual ProcessResultViewModel<bool> Delete([FromRoute]int id)
        {
            var result = _manger.DeleteById(id);
            return _processResultMapper.Map<bool, bool>(result);
        }
    }
}
