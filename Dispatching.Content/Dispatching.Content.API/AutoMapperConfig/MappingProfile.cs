﻿using AutoMapper;
using Dispatching.Content.API.ViewModels;
using Dispatching.Content.Models.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Dispatching.Content.API.AutoMapperConfig
{
    public class MappingProfile : Profile
    {
        public MappingProfile()
        {
            CreateMap<FAQ, FAQViewModel>();

            CreateMap<FAQViewModel, FAQ>()
                .ForMember(dest => dest.Answer, opt => opt.MapFrom(src => src.Answer))
                .ForMember(dest => dest.Question, opt => opt.MapFrom(src => src.Question))
                .ForMember(dest => dest.Id, opt => opt.MapFrom(src => src.Id))
                .ForMember(dest => dest.Fk_Country_Id, opt => opt.MapFrom(src => src.Fk_Country_Id))
                .ForAllOtherMembers(opt => opt.Ignore());
        }
    }
}
