﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Dispatching.Content.Models.Context;
using Microsoft.AspNetCore;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Serilog;
using Utilities.Utilites.SerilogExtensions;

namespace Dispatching.Content.API
{
    public class Program
    {
        public static void Main(string[] args)
        {
            Log.Logger = new LoggerConfiguration()
                .Enrich.With<CustomExceptionEnricher>()
                .MinimumLevel.Error()
                .Enrich.FromLogContext()
                .WriteTo.File("logs/log_.csv", rollingInterval: RollingInterval.Day,
                outputTemplate: "{Timestamp:yyyy-MM-dd HH:mm:ss.fff zzz},[{Level}],{Message},{ExceptionMessage},{ExceptionSource},{ExceptionType},{ExceptionStackTrace},{NewLine}")
                .CreateLogger();

            BuildWebHost(args)
                .MigrateDbContext<DataContext>((context, services) =>
                {
                    //var logger = services.GetService<ILogger<DataContextSeed>>();
                    //var customerManager = services.GetService<ICustomerManager>();

                    //new DataContextSeed(customerManager)
                    //    .Seed(context, logger);
                }).Run();
        }

        public static IWebHost BuildWebHost(string[] args) =>
          WebHost.CreateDefaultBuilder(args)
              .UseKestrel()
              .UseContentRoot(Directory.GetCurrentDirectory())
              .UseIISIntegration()
              .UseStartup<Startup>()
              .UseSerilog()
              .Build();
    }
}
