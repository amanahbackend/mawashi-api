﻿using CommonEnums;
using Dispatching.Content.BLL.IManagers;
using Dispatching.Content.Models.Context;
using Dispatching.Content.Models.Entities;
using DispatchProduct.RepositoryModule;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Utilites.ProcessingResult;

namespace Dispatching.Content.BLL.Managers
{
    public class FAQManager : Repository<FAQ>, IFAQManager
    {
        public FAQManager(DataContext context) : base(context)
        {
        }

        public ProcessResult<List<FAQ>> GetByCountry(Country country)
        {
            var faqs = GetAllQuerable().Data.Where(x => x.Fk_Country_Id == country).ToList();
            return ProcessResultHelper.Succedded(faqs);
        }
    }
}
