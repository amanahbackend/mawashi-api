﻿using CommonEnums;
using Dispatching.Content.Models.Entities;
using DispatchProduct.RepositoryModule;
using System;
using System.Collections.Generic;
using System.Text;
using Utilites.ProcessingResult;

namespace Dispatching.Content.BLL.IManagers
{
    public interface IFAQManager : IRepository<FAQ>
    {
        ProcessResult<List<FAQ>> GetByCountry(Country country);
    }
}
