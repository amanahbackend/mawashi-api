﻿using Dispatching.Content.Models.Entities;
using DispatchProduct.BuildingBlocks.BaseEntities.EntityConfigurations;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;

namespace Dispatching.Content.Models.EntitiesConfiguration
{
    public class FAQTypeConfiguration: BaseEntityTypeConfiguration<FAQ>, IEntityTypeConfiguration<FAQ>
    {
        public new void Configure(EntityTypeBuilder<FAQ> builder)
        {
            base.Configure(builder);
            builder.ToTable("FAQ");
            builder.Property(c => c.Question).IsRequired();
            builder.Property(c => c.Answer).IsRequired();
        }
    }
}
