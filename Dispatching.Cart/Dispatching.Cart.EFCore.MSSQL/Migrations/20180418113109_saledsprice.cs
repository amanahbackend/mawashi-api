﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Dispatching.CartModule.EFCore.MSSQL.Migrations
{
    public partial class saledsprice : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<bool>(
                name: "IsSalesChangePrice",
                table: "CartItems",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<double>(
                name: "SalesPricePerItem",
                table: "CartItems",
                nullable: false,
                defaultValue: 0.0);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "IsSalesChangePrice",
                table: "CartItems");

            migrationBuilder.DropColumn(
                name: "SalesPricePerItem",
                table: "CartItems");
        }
    }
}
