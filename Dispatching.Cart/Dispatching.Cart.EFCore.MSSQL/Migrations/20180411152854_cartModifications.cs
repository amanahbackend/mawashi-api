﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Dispatching.CartModule.EFCore.MSSQL.Migrations
{
    public partial class cartModifications : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "CuttingFinalPrice",
                table: "CartItems",
                newName: "ItemNetPrice");

            migrationBuilder.AddColumn<double>(
                name: "CuttingPrice",
                table: "CartItems",
                nullable: false,
                defaultValue: 0.0);

            migrationBuilder.AddColumn<double>(
                name: "ItemDiscount",
                table: "CartItems",
                nullable: false,
                defaultValue: 0.0);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "CuttingPrice",
                table: "CartItems");

            migrationBuilder.DropColumn(
                name: "ItemDiscount",
                table: "CartItems");

            migrationBuilder.RenameColumn(
                name: "ItemNetPrice",
                table: "CartItems",
                newName: "CuttingFinalPrice");
        }
    }
}
