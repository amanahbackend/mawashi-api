﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Dispatching.CartModule.EFCore.MSSQL.Migrations
{
    public partial class addingProdPropsToCartItems : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<double>(
                name: "ActualQuantityKg",
                table: "CartItems",
                nullable: false,
                defaultValue: 0.0);

            migrationBuilder.AddColumn<int>(
                name: "ActualQuantityPcs",
                table: "CartItems",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<bool>(
                name: "IsPacked",
                table: "CartItems",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<string>(
                name: "Location",
                table: "CartItems",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "ActualQuantityKg",
                table: "CartItems");

            migrationBuilder.DropColumn(
                name: "ActualQuantityPcs",
                table: "CartItems");

            migrationBuilder.DropColumn(
                name: "IsPacked",
                table: "CartItems");

            migrationBuilder.DropColumn(
                name: "Location",
                table: "CartItems");
        }
    }
}
