﻿using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Dispatching.CartModule.EFCore.MSSQL.Migrations
{
    public partial class init : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Cart",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    CreatedDate = table.Column<DateTime>(nullable: false),
                    DeletedDate = table.Column<DateTime>(nullable: false),
                    FK_CreatedBy_Id = table.Column<string>(nullable: true),
                    FK_Customer_Id = table.Column<int>(nullable: false),
                    FK_DeletedBy_Id = table.Column<string>(nullable: true),
                    FK_Order_Id = table.Column<int>(nullable: false),
                    FK_UpdatedBy_Id = table.Column<string>(nullable: true),
                    HasOrder = table.Column<bool>(nullable: false),
                    IsDeleted = table.Column<bool>(nullable: false),
                    Price = table.Column<double>(nullable: false),
                    UpdatedDate = table.Column<DateTime>(nullable: false),
                    VAT = table.Column<double>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Cart", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "CartItems",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    CreatedDate = table.Column<DateTime>(nullable: false),
                    CuttingFinalPrice = table.Column<double>(nullable: false),
                    DeletedDate = table.Column<DateTime>(nullable: false),
                    FK_Cart_Id = table.Column<int>(nullable: false),
                    FK_CreatedBy_Id = table.Column<string>(nullable: true),
                    FK_Cutting_Id = table.Column<int>(nullable: false),
                    FK_DeletedBy_Id = table.Column<string>(nullable: true),
                    FK_Quantity_Id = table.Column<int>(nullable: false),
                    FK_UpdatedBy_Id = table.Column<string>(nullable: true),
                    Fk_Item_Id = table.Column<int>(nullable: false),
                    IsDeleted = table.Column<bool>(nullable: false),
                    ItemFinalPrice = table.Column<double>(nullable: false),
                    Price = table.Column<double>(nullable: false),
                    UpdatedDate = table.Column<DateTime>(nullable: false),
                    VAT = table.Column<double>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_CartItems", x => x.Id);
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Cart");

            migrationBuilder.DropTable(
                name: "CartItems");
        }
    }
}
