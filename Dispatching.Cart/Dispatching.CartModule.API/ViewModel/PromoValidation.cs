﻿using System.ComponentModel.DataAnnotations.Schema;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;
using DispatchProduct.BuildingBlocks.BaseEntities.Entities;

namespace Dispatching.CartModule.API.ViewModel
{
    public class PromoValidation 
    {
        public bool IsValid { get; set; }
        public int PromoValue { get; set; }
        public double Price { get; set; }
        public double VAT { get; set; }
        public double DiscountValue { get; set; }
        public double Total { get; set; }

    }
}
