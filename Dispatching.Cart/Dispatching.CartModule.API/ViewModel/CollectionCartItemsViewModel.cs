﻿using DispatchProduct.BuildingBlocks.BaseEntities.Entities;
using DispatchProduct.BuildingBlocks.BaseEntities.IEntities;
using System;
using System.Collections.Generic;
using System.Text;

namespace Dispatching.CartModule.API.ViewModel
{
    public class CollectionCartItemsViewModel 
    {
       public List<CartItemsViewModel> Items { get; set; }
       //public int? FK_Cart_Id { get; set; }
       public CartViewModel Cart { get; set; }
    }
}
