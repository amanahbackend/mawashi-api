﻿using Autofac;
using Autofac.Extensions.DependencyInjection;
using AutoMapper;
using BuildingBlocks.ServiceDiscovery;
using CommonServicesAPI.ServicesCommunication.Identity;
using CommonServicesAPI.ServicesSettings.Identity;
using Dispatching.CartModule.API.Controllers;
using Dispatching.CartModule.API.ServicesCommunication.Inventory;
using Dispatching.CartModule.API.ServicesCommunication.Inventory.Cutting;
using Dispatching.CartModule.API.ServicesCommunication.Inventory.Quantity;
using Dispatching.CartModule.API.ServicesCommunication.Location;
using Dispatching.CartModule.API.ServicesCommunication.VAT;
using Dispatching.CartModule.BLL.IManagers;
using Dispatching.CartModule.BLL.Managers;
using Dispatching.CartModule.Models.Context;
using Dispatching.CartModule.Models.Entities;
using Dispatching.CartModule.Models.Settings;
using DispatchProduct.CartModule.API.ServicesCommunication.PromoCode;
using DispatchProduct.CartModule.API.ServicesCommunication.Settings;
using DispatchProduct.CartModule.API.ServicesCommunication.VAT;
using DispatchProduct.CartModule.API.Settings;
using DispatchProduct.Contracting.API.ServicesCommunication.Cutting;
using DispatchProduct.Contracting.API.ServicesCommunication.Item;
using DispatchProduct.Contracting.API.ServicesCommunication.Quantity;
using DispatchProduct.RepositoryModule;
using DnsClient;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Newtonsoft.Json.Serialization;
using Swashbuckle.AspNetCore.Swagger;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using Utilites.PaginatedItems;
using Utilites.ProcessingResult;

namespace Dispatching.CartModule.API
{
    public class Startup
    {
        public IHostingEnvironment _env;
        public IConfigurationRoot Configuration { get; }

        public Startup(IHostingEnvironment env)
        {
            _env = env;
            var builder = new ConfigurationBuilder()
                .SetBasePath(env.ContentRootPath)
                .AddEnvironmentVariables();
            Configuration = builder.Build();

            var country = Configuration.GetSection("DockerContainerName").Value.Split('.').Last().ToLower();
            //var country = "uae";
            builder = new ConfigurationBuilder()
                .SetBasePath(env.ContentRootPath)
                .AddJsonFile($"appsettings.{country}.json", optional: false, reloadOnChange: true)
                .AddEnvironmentVariables();

            Configuration = builder.Build();
        }


        // This method gets called by the runtime. Use this method to add services to the container.
        public IServiceProvider ConfigureServices(IServiceCollection services)
        {
            services.AddCors(options =>
            {
                options.AddPolicy("AllowAll",
                    builder =>
                    {
                        builder
                        .AllowAnyOrigin()
                        .AllowAnyMethod()
                        .AllowAnyHeader()
                        .AllowCredentials();
                    });
            });

            services.AddSingleton(provider => Configuration);

            services.AddMvc().AddJsonOptions(options =>
            {
                options.SerializerSettings.ContractResolver = new CamelCasePropertyNamesContractResolver();
            });

            string consulContainerName = Configuration.GetSection("ServiceRegisteryContainerName").Value;
            int consulPort = Convert.ToInt32(Configuration.GetSection("ServiceRegisteryPort").Value);
            IPAddress ip = Dns.GetHostEntry(consulContainerName).AddressList.Where(o => o.AddressFamily == AddressFamily.InterNetwork).First();
            services.AddSingleton<IDnsQuery>(new LookupClient(ip, consulPort));

            services.AddServiceDiscovery(Configuration.GetSection("ServiceDiscovery"));

            services.Configure<CuttingServiceSetting>
                (Configuration.GetSection("ServiceCommunications:CuttingService"));

            services.Configure<ItemServiceSetting>
               (Configuration.GetSection("ServiceCommunications:ItemService"));
            services.Configure<QuantityServiceSetting>
              (Configuration.GetSection("ServiceCommunications:QuantityService"));
            services.Configure<IdentityServiceSettings>
               (Configuration.GetSection("ServiceCommunications:IdentityService"));

            services.Configure<PromoCodeServiceSetting>
          (Configuration.GetSection("ServiceCommunications:PromoCodeService"));

            services.Configure<FeesServiceSetting>
         (Configuration.GetSection("ServiceCommunications:FeesService"));

            services.Configure<VATServiceSetting>
        (Configuration.GetSection("ServiceCommunications:VatService"));


            services.Configure<CategorySubOptionsServiceSetting>
        (Configuration.GetSection("ServiceCommunications:CategorySubOptionService"));

            services.AddApiVersioning(o => o.ReportApiVersions = true);
            services.AddDbContext<CartDbContext>(options =>
            options.UseSqlServer(Configuration["ConnectionString"],
            sqlOptions => sqlOptions.MigrationsAssembly("Dispatching.CartModule.EFCore.MSSQL")));

            services.AddOptions();
            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1",
                    new Info()
                    {
                        Title = "Calling API",
                        Description = "Calling  API"
                    });
                c.AddSecurityDefinition("oauth2", new OAuth2Scheme
                {
                    Type = "oauth2",
                    Flow = "implicit",
                    AuthorizationUrl = $"{Configuration.GetValue<string>("IdentityUrlExternal")}/connect/authorize",
                    TokenUrl = $"{Configuration.GetValue<string>("IdentityUrlExternal")}/connect/token",
                    Scopes = new Dictionary<string, string>()
                    {
                        { "calling", "calling API" }
                    }
                });
            });
            services.AddMvc();
            services.AddAutoMapper(typeof(Startup));
            Mapper.AssertConfigurationIsValid();

            services.Configure<CartAppSettings>(Configuration);
            services.AddScoped<DbContext, CartDbContext>();
            services.AddScoped(typeof(IRepository<>), typeof(Repository<>));
            services.AddScoped(typeof(ICart), typeof(Cart));
            services.AddScoped(typeof(ICartItems), typeof(CartItems));

            services.AddScoped(typeof(ICartManager), typeof(CartManager));
            services.AddScoped(typeof(ICartItemsManager), typeof(CartItemsManager));
            services.AddScoped(typeof(ICuttingService), typeof(CuttingService));
            services.AddScoped(typeof(IItemService), typeof(ItemService));
            services.AddScoped(typeof(IQuantityService), typeof(QuantityService));
            services.AddScoped(typeof(IVATService), typeof(VATService));
            services.AddScoped(typeof(IFeesService), typeof(FeesService));

            services.AddScoped(typeof(CartController), typeof(CartController));
            services.AddScoped(typeof(CartItemsController), typeof(CartItemsController));
            services.AddScoped(typeof(IIdentityUserService), typeof(IdentityUserService));
            services.AddScoped(typeof(IPromoCodeService), typeof(PromoCodeService));
            services.AddScoped(typeof(ICategorySubOptionsService), typeof(CategorySubOptionsService));
            services.AddScoped(typeof(PromoCodeValidation), typeof(PromoCodeValidation));


            services.AddScoped(typeof(IProcessResultMapper), typeof(ProcessResultMapper));
            services.AddScoped(typeof(IProcessResultPaginatedMapper), typeof(ProcessResultPaginatedMapper));
            services.AddScoped(typeof(IPaginatedItems<>), typeof(PaginatedItems<>));
            var container = new ContainerBuilder();
            container.Populate(services);
            return new AutofacServiceProvider(container.Build());
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            app.UseCors("AllowAll");
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
                app.UseDatabaseErrorPage();
            }
            else
            {
                app.UseExceptionHandler("/Home/Error");
            }
            app.UseStaticFiles();
            app.UseSwagger();
            app.UseSwaggerUI(c =>
            {
                c.SwaggerEndpoint("/swagger/v1/swagger.json", "Reviewing API");
            });

            app.UseMvc();
            app.UseConsulRegisterService();

        }

    }
}
