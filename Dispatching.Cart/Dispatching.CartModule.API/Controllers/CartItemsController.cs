﻿using AutoMapper;
using Dispatching.BuisnessCommon.Enums;
using Dispatching.CartModule.API.ServicesViewModels;
using Dispatching.CartModule.API.ViewModel;
using Dispatching.CartModule.BLL.IManagers;
using Dispatching.CartModule.Models.Entities;
using DispatchProduct.Contracting.API.ServicesCommunication.Cutting;
using DispatchProduct.Contracting.API.ServicesCommunication.Item;
using DispatchProduct.Contracting.API.ServicesCommunication.Quantity;
using DispatchProduct.Controllers.V1;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Utilites;
using Utilites.ProcessingResult;

namespace Dispatching.CartModule.API.Controllers
{
    [ApiVersion("1.0")]
    [Route("v{version:apiVersion}/[controller]")]
    public class CartItemsController : BaseControllerV1<ICartItemsManager, CartItems, CartItemsViewModel>
    {
        public ICartItemsManager manger;
        public readonly IMapper mapper;
        IProcessResultMapper processResultMapper;
        IProcessResultPaginatedMapper processResultPaginatedMapper;
        IItemService itemService;
        ICategorySubOptionsService categorySubOptionsService;
        ICuttingService cuttingService;
        IQuantityService quantityService;
        public CartItemsController(ICategorySubOptionsService _categorySubOptionsService, ICartItemsManager _manger, IMapper _mapper, IProcessResultMapper _processResultMapper, IProcessResultPaginatedMapper _processResultPaginatedMapper, IItemService _itemService, ICuttingService _cuttingService, IQuantityService _quantityService) : base(_manger, _mapper, _processResultMapper, _processResultPaginatedMapper)
        {
            this.manger = _manger;
            this.mapper = _mapper;
            processResultMapper = _processResultMapper;
            processResultPaginatedMapper = _processResultPaginatedMapper;
            itemService = _itemService;
            cuttingService = _cuttingService;
            quantityService = _quantityService;
            categorySubOptionsService = _categorySubOptionsService;
        }

        public async Task<ProcessResultViewModel<CartViewModel>> AddListItems(CartViewModel model)
        {
            try
            {
                var deleteResult = DeleteCartItemsByCartId(model.Id);
                if (deleteResult.IsSucceeded)
                {
                    var manipulateResult = await ManipulateCartItems(model);
                    if (manipulateResult.IsSucceeded)
                    {
                        model = manipulateResult.Data;
                        for (int i = 0; i < model.CartItems.Count; i++)
                        {
                            var addResult = base.Post(model.CartItems[i]);
                            if (addResult.IsSucceeded)
                            {
                                model.CartItems[i].Id = addResult.Data.Id;
                            }
                            else
                            {
                                return ProcessResultViewModelHelper.MapToProcessResultViewModel<CartItemsViewModel, CartViewModel>(addResult);
                            }
                        }
                        return ProcessResultViewModelHelper.Succedded(model);
                    }
                    else
                    {
                        return ProcessResultViewModelHelper.MapToProcessResultViewModel<CartViewModel, CartViewModel>(manipulateResult);
                    }
                }
                else
                {
                    return ProcessResultViewModelHelper.MapToProcessResultViewModel<bool, CartViewModel>(deleteResult);
                }
            }
            catch (Exception ex)
            {
                return ProcessResultViewModelHelper.Failed<CartViewModel>(null, ex.Message);
            }
        }
        private CartViewModel ResetCartValues(CartViewModel model)
        {
            foreach (var cartItem in model.CartItems)
            {
                cartItem.ItemDiscount = 0;
                cartItem.CuttingPrice = 0;
                cartItem.ItemNetPrice = 0;
                cartItem.VAT = 0;
                cartItem.Price = 0;
                cartItem.ItemFinalPrice = 0;
                cartItem.FK_Cart_Id = model.Id;
                cartItem.Fk_Item_Id = cartItem.Item.Id;
            }
            return model;
        }
        private async Task<ProcessResultViewModel<CartViewModel>> ManipulateCartItems(CartViewModel model)
        {
            ProcessResultViewModel<CartViewModel> result = null;
            var cartItemsResult = await GetCartItemDependencies(model.CartItems);
            if (cartItemsResult.IsSucceeded)
            {
                model.CartItems = cartItemsResult.Data;
                model = ResetCartValues(model);
                if (model.OrderType == OrderType.Individual)
                {
                    result = CalculateIndividualCartItems(model);
                }
                else if (model.OrderType == OrderType.Retail)
                {
                    result = CalculateSalesCartItems(model);
                }
                else
                {
                    return ProcessResultViewModelHelper.Failed(model, $"Cart Type not defined to it must be retail(sales) or individual(usual customers)");
                }
            }
            else
            {
                result = ProcessResultViewModelHelper.MapToProcessResultViewModel<List<CartItemsViewModel>, CartViewModel>(cartItemsResult);
            }
            return result;
        }
        private ProcessResultViewModel<CartViewModel> CalculateSalesCartItems(CartViewModel model)
        {
            foreach (var cartItem in model.CartItems)
            {
                if (cartItem.Item != null)
                {
                    if (cartItem.SalesPricePerItem > 0)
                    {
                        cartItem.ItemNetPrice = cartItem.SalesPricePerItem;
                        cartItem.IsSalesChangePrice = true;
                    }
                    else
                    {
                        cartItem.ItemNetPrice = cartItem.Item.Price;
                    }
                    if (cartItem.ItemQuantityByKG > 0)
                        cartItem.ItemNetPrice = cartItem.ItemNetPrice * cartItem.ItemQuantityByKG;
                    //cartItem.ItemDiscount = cartItem.Item.DiscountValue;
                    //cartItem.VAT = cartItem.Item.VATValue;
                    cartItem.ItemDiscount = 0;
                    cartItem.VAT = 0;
                    if (cartItem.Quantity != null && cartItem.QuantityValue > 0)
                    {
                        cartItem.Price = cartItem.ItemNetPrice * cartItem.Quantity.Value;

                    }
                    else
                    {
                        return ProcessResultViewModelHelper.Failed(model, $"Cart Quantity:{cartItem.QuantityValue} for item {cartItem.Fk_Item_Id} not valid to submit Cart");
                    }
                }
            }

            return ProcessResultViewModelHelper.Succedded(model);
        }
        private ProcessResultViewModel<CartViewModel> CalculateIndividualCartItems(CartViewModel model)
        {
            ProcessResultViewModel<CartViewModel> result = null;
            foreach (var cartItem in model.CartItems)
            {
                if (cartItem.Item != null)
                {
                    cartItem.ItemDiscount = cartItem.Item.DiscountValue;
                    cartItem.ItemNetPrice = cartItem.Item.Price;
                    cartItem.VAT = cartItem.Item.VATValue;
                    if (cartItem.Cutting != null)
                        cartItem.CuttingPrice = cartItem.Cutting.Price;
                    else if (cartItem.FK_Cutting_Id == 0)
                        cartItem.CuttingPrice = 0;
                    else
                        return result = ProcessResultViewModelHelper.Failed(model, $"There is no Cutting with this Id {cartItem.FK_Cutting_Id}");

                    if (cartItem.QuantityValue > 0)
                    {
                        cartItem.CuttingPrice = cartItem.CuttingPrice * cartItem.QuantityValue;
                        cartItem.ItemDiscount = cartItem.ItemDiscount * cartItem.QuantityValue;
                        cartItem.ItemNetPrice = cartItem.ItemNetPrice * cartItem.QuantityValue;
                        cartItem.Price = cartItem.ItemNetPrice + cartItem.CuttingPrice - cartItem.ItemDiscount;

                        if (cartItem.Cutting != null)
                            cartItem.VAT = (cartItem.VAT + cartItem.Cutting.VATValue) * cartItem.QuantityValue;
                        else if (cartItem.FK_Cutting_Id == 0)
                            cartItem.VAT = (cartItem.VAT) * cartItem.QuantityValue;
                        else
                            return result = ProcessResultViewModelHelper.Failed(model, $"There is no Cutting with this Id {cartItem.FK_Cutting_Id}");
                    }
                    else
                    {
                        return result = ProcessResultViewModelHelper.Failed(model, $"There is no quantity with this Id {cartItem.FK_Quantity_Id}");
                    }
                    cartItem.ItemFinalPrice = cartItem.Price;
                }
                else
                {
                    return result = ProcessResultViewModelHelper.Failed(model, $"Item with Id {cartItem.Fk_Item_Id} doesn't Exist");
                }
            }
            result = ProcessResultViewModelHelper.Succedded(model);
            return result;
        }
        public async Task<ProcessResultViewModel<List<CartItemsViewModel>>> GetCartItemDependencies(List<CartItemsViewModel> model)
        {
            ProcessResultViewModel<List<CartItemsViewModel>> result = null;
            var fk_Item_Ids = model.Select(c => c.Fk_Item_Id).ToList();
            var resultInventoryItems = await GetInventoryItems(fk_Item_Ids);
            if (resultInventoryItems.IsSucceeded)
            {
                foreach (var cartItem in model)
                {
                    cartItem.Item = resultInventoryItems.Data.Where(c => c.Id == cartItem.Fk_Item_Id).FirstOrDefault();
                }
                var optionRes = await BindOptionsToCartItems(model);
                if (optionRes.IsSucceeded && optionRes.Data != null)
                {
                    model = optionRes.Data;
                }
                result = ProcessResultViewModelHelper.Succedded(model);
            }
            else
            {
                result = ProcessResultViewModelHelper.MapToProcessResultViewModel<List<LKP_ItemViewModel>, List<CartItemsViewModel>>(resultInventoryItems);
            }
            var item = model.FirstOrDefault();
            if (item.OrderType == OrderType.Individual)
            {
                var fk_cutting_Ids = model.Where(ct => ct.FK_Cutting_Id > 0).Select(c => c.FK_Cutting_Id).ToList();
                if (fk_cutting_Ids.Count > 0)
                {
                    var resultInventoryCuttings = await GetInventoryCuttings(fk_cutting_Ids);
                    if (resultInventoryCuttings.IsSucceeded)
                    {
                        foreach (var cartItem in model)
                        {
                            cartItem.Cutting = resultInventoryCuttings.Data.Where(c => c.Id == cartItem.FK_Cutting_Id).FirstOrDefault();
                        }
                    }
                }
                var fk_quantities_Ids = model.Where(ct => ct.FK_Quantity_Id > 0).Select(c => c.FK_Quantity_Id).ToList();
                if (fk_quantities_Ids.Count > 0)
                {
                    var resultInventoryQuantities = await GetInventoryQuantities(fk_quantities_Ids);
                    if (resultInventoryQuantities.IsSucceeded && resultInventoryItems.IsSucceeded)
                    {
                        foreach (var cartItem in model)
                        {
                            cartItem.Quantity = resultInventoryQuantities.Data.Where(c => c.Id == cartItem.FK_Quantity_Id).FirstOrDefault();
                            cartItem.QuantityValue = cartItem.Quantity.Value;
                        }
                    }
                }
                result = ProcessResultViewModelHelper.Succedded(model);
            }
            return result;
        }
        public ProcessResultViewModel<bool> DeleteCartItemsByCartId(int cartId)
        {
            var entityResult = manger.DeleteByCartId(cartId);
            return processResultMapper.Map<bool, bool>(entityResult);
        }

        [HttpGet, Route("GetByCartId/{cartId}"), MapToApiVersion("1.0")]
        public async Task<ProcessResultViewModel<List<CartItemsViewModel>>> GetCartItemsByCartId([FromRoute]int cartId)
        {
            ProcessResultViewModel<List<CartItemsViewModel>> result = null;
            var entityResult = manger.GetByCartId(cartId);
            var entityResultViewModel = processResultMapper.Map<List<CartItems>, List<CartItemsViewModel>>(entityResult);
            if (entityResultViewModel.IsSucceeded)
            {
                result = await GetCartItemDependencies(entityResultViewModel.Data);
            }
            else
            {
                result = ProcessResultViewModelHelper.MapToProcessResultViewModel<List<CartItemsViewModel>, List<CartItemsViewModel>>(result);
            }
            return result;
        }
        private async Task<ProcessResultViewModel<List<CartItemsViewModel>>> BindOptionsToCartItems(List<CartItemsViewModel> model)
        {
            ProcessResultViewModel<List<CartItemsViewModel>> result = null;
            var fk_Option_Ids = model.Select(c => c.FK_Option_Id).ToList();
            if (!fk_Option_Ids.Contains(0) && fk_Option_Ids.Count() > 0)
            {
                var resultInventoryOptions = await GetOptionsItems(fk_Option_Ids);
                if (resultInventoryOptions.IsSucceeded && resultInventoryOptions.Data != null)
                {
                    foreach (var cartItem in model)
                    {
                        cartItem.CategorySubOption = resultInventoryOptions.Data.Where(c => c.Id == cartItem.FK_Option_Id).FirstOrDefault();
                    }
                    result = ProcessResultViewModelHelper.Succedded(model);
                }
                else
                {
                    result = ProcessResultViewModelHelper.MapToProcessResultViewModel<List<CategorySubOptionsViewModel>, List<CartItemsViewModel>>(resultInventoryOptions);
                }
            }
            else
            {
                result = ProcessResultViewModelHelper.Succedded(model);
            }
            return result;
        }
        private async Task<ProcessResultViewModel<List<LKP_ItemViewModel>>> GetInventoryItems(List<int> itemIds, string authHeader = null)
        {
            if (Request != null && authHeader == null)
            {
                authHeader = Helper.GetValueFromRequestHeader(Request);
            }
            ProcessResultViewModel<List<LKP_ItemViewModel>> itemResult = null;
            itemResult = await itemService.GetByIds(itemIds, authHeader);
            return itemResult;
        }
        private async Task<ProcessResultViewModel<List<CategorySubOptionsViewModel>>> GetOptionsItems(List<int> option_Ids, string authHeader = null)
        {
            if (Request != null && authHeader == null)
            {
                authHeader = Helper.GetValueFromRequestHeader(Request);
            }
            ProcessResultViewModel<List<CategorySubOptionsViewModel>> subOptionsResult = null;
            subOptionsResult = await categorySubOptionsService.GetByIds(option_Ids, authHeader);
            return subOptionsResult;
        }
        private async Task<ProcessResultViewModel<List<LKP_CuttingViewModel>>> GetInventoryCuttings(List<int> cuttingIds, string authHeader = null)
        {
            if (Request != null && authHeader == null)
            {
                authHeader = Helper.GetValueFromRequestHeader(Request);
            }
            ProcessResultViewModel<List<LKP_CuttingViewModel>> cuttingResult = null;
            cuttingResult = await cuttingService.GetByIds(cuttingIds, authHeader);
            return cuttingResult;
        }
        private async Task<ProcessResultViewModel<List<LKP_QuantityViewModel>>> GetInventoryQuantities(List<int> quantityIds, string authHeader = null)
        {
            if (Request != null && authHeader == null)
            {
                authHeader = Helper.GetValueFromRequestHeader(Request);
            }
            ProcessResultViewModel<List<LKP_QuantityViewModel>> quantityResult = null;
            quantityResult = await quantityService.GetByIds(quantityIds, authHeader);
            return quantityResult;
        }

        [HttpPut, Route("UpdateProdValues"), MapToApiVersion("1.0")]
        public ProcessResultViewModel<bool> UpdateProdValues([FromBody]CartItemsViewModel model)
        {
            try
            {
                var cartItem = mapper.Map<CartItems>(model);
                var result = manger.UpdateProdValues(cartItem);
                return processResultMapper.Map<bool, bool>(result);
            }
            catch (Exception ex)
            {
                return ProcessResultViewModelHelper.Failed(false, ex.Message);
            }
        }
    }
}

