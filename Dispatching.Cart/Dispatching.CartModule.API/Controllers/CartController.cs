﻿using AutoMapper;
using Dispatching.CartModule.API.ViewModel;
using Dispatching.CartModule.BLL.IManagers;
using Dispatching.CartModule.Models.Entities;
using DispatchProduct.Controllers;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Text;
using Utilites.ProcessingResult;
using Microsoft.Extensions.DependencyInjection;
using System.Threading.Tasks;
using DispatchProduct.Controllers.V1;
using CommonServicesAPI.ServicesViewModels.Identity;
using CommonServicesAPI.ServicesCommunication.Identity;
using DispatchProduct.Contracting.API.ServicesCommunication.Item;
using System.Linq;
using DispatchProduct.CartModule.API.ServicesCommunication.PromoCode;
using Dispatching.BuisnessCommon.Enums;
using Dispatching.CartModule.API.ServicesViewModels;
using DispatchProduct.CartModule.API.ServicesCommunication.VAT;
using Dispatching.CartModule.API.ServicesViewModels.VAT;
using DispatchProduct.Contracting.API.ServicesCommunication.Cutting;
using Dispatching.CartModule.API.ServicesViewModels.Inventory.Item;

namespace Dispatching.CartModule.API.Controllers
{
    [ApiVersion("1.0")]
    [Route("v{version:apiVersion}/[controller]")]
    public class CartController : BaseControllerV1<ICartManager, Cart, CartViewModel>
    {
        public new ICartManager manger;
        public readonly new IMapper mapper;
        IProcessResultMapper processResultMapper;
        IProcessResultPaginatedMapper processResultPaginatedMapper;
        IServiceProvider serviceProvider;
        public CartController(IServiceProvider _serviceProvider, ICartManager _manger, IMapper _mapper, IProcessResultMapper _processResultMapper, IProcessResultPaginatedMapper _processResultPaginatedMapper) : base(_manger, _mapper, _processResultMapper, _processResultPaginatedMapper)
        {
            this.manger = _manger;
            this.mapper = _mapper;
            serviceProvider = _serviceProvider;
            processResultMapper = _processResultMapper;
            processResultPaginatedMapper = _processResultPaginatedMapper;
        }
        [Route("GetByCustomerId/{customerId}")]
        [HttpGet]
        [MapToApiVersion("1.0")]
        public ProcessResultViewModel<List<CartViewModel>> GetByCustomerId([FromRoute]int customerId)
        {
            var processResult = manger.GetByCustomerId(customerId);
            return processResultMapper.Map<List<Cart>, List<CartViewModel>>(processResult);

        }
        [Route("Submit")]
        [HttpPost]
        [MapToApiVersion("1.0")]
        public async Task<ProcessResultViewModel<CartViewModel>> SubmitIndividual([FromBody]CartViewModel model)
        {
            model.OrderType = OrderType.Individual;
            foreach (var item in model.CartItems)
            {
                item.OrderType = OrderType.Individual;
            }
            return await Submit(model);
        }
        [Route("Submit/Retail")]
        [HttpPost]
        [MapToApiVersion("1.0")]
        public async Task<ProcessResultViewModel<CartViewModel>> SubmitRetail([FromBody]CartViewModel model)
        {
            model.OrderType = OrderType.Retail;
            foreach (var item in model.CartItems)
            {
                item.OrderType = OrderType.Retail;
            }
            return await Submit(model);
        }
        [Route("Submit/SalesCart")]
        [HttpPost]
        [MapToApiVersion("1.0")]
        public async Task<ProcessResultViewModel<CartViewModel>> SubmitSalesCart([FromBody]SalesCartViewModel model)
        {
            ProcessResultViewModel<CartViewModel> result = null;
            var cart = mapper.Map<SalesCartViewModel, CartViewModel>(model);
            cart.OrderType = OrderType.Retail;
            foreach (var item in cart.CartItems)
            {
                item.OrderType = OrderType.Retail;
            }
            result = await Submit(cart);
            return result;
        }
        public async Task<ProcessResultViewModel<CartViewModel>> Submit([FromBody]CartViewModel model)
        {
            ProcessResultViewModel<CartViewModel> result = new ProcessResultViewModel<CartViewModel>();
            var availabilityResult = await IsAvailableInStock(model);
            if (availabilityResult.IsSucceeded && availabilityResult.Data)
            {
                result = await CanCombineCartItems(model);
                if (result.IsSucceeded)
                {
                    var cartIdResult = manger.GetCurrentCartId(model.FK_Customer_Id, model.OrderType);
                    if (model.Id == 0 && cartIdResult.IsSucceeded && cartIdResult.Data > 0)
                    {
                        result = await GetCartWithItems(cartIdResult.Data);
                        result = ProcessResultViewModelHelper.Failed(result.Data, $"there is already not completed Cart  for this user {model.FK_Customer_Id} ");
                    }
                    else
                    {
                        result = await GetOrAddCart(model);
                        if (result.IsSucceeded)
                        {
                            if (model.CartItems != null && model.CartItems.Count > 0)
                            {
                                model.Id = result.Data.Id;
                                result.Data.CartItems = model.CartItems;
                                var listItmsResult = await CartItemsController.AddListItems(model);
                                if (listItmsResult.IsSucceeded)
                                {
                                    model = listItmsResult.Data;
                                    result.Data.Price = 0;
                                    result.Data.VAT = 0;
                                    foreach (var cartItem in model.CartItems)
                                    {
                                        // remove truncate
                                        //result.Data.Price = Math.Truncate(result.Data.Price + cartItem.Price);
                                        result.Data.Price = result.Data.Price + cartItem.Price;
                                    }
                                    result = await CalculateCartVat(result.Data);
                                    if (result.IsSucceeded)
                                    {
                                        var updateresult = Put(result.Data);
                                        if (updateresult.IsSucceeded)
                                        {
                                            result.Data.CartItems = model.CartItems;
                                            model = result.Data;
                                        }
                                        else
                                        {
                                            return ProcessResultViewModelHelper.MapToProcessResultViewModel<bool, CartViewModel>(updateresult);
                                        }
                                    }
                                    else
                                    {
                                        return result;
                                    }
                                }
                                else
                                {
                                    Delete(model.Id);
                                    return ProcessResultViewModelHelper.MapToProcessResultViewModel<CartViewModel, CartViewModel>(listItmsResult);
                                }
                            }
                            else
                            {
                                return ProcessResultViewModelHelper.Failed<CartViewModel>(model, "Can't Submit Order without CartItems");
                            }
                            result = ProcessResultViewModelHelper.Succedded(model);
                        }
                    }
                }
            }
            else
            {
                result = ProcessResultViewModelHelper.Failed<CartViewModel>(null, availabilityResult.Status.Message);
            }
            return result;
        }

        private async Task<ProcessResultViewModel<bool>> IsAvailableInStock(CartViewModel model)
        {
            var result = new ProcessResultViewModel<bool>();
            try
            {
                var models = model.CartItems.Select(x => new CheckItemAvailabilityViewModel
                {
                    ItemId = x.Fk_Item_Id,
                    QuantityId = x.FK_Quantity_Id,
                    MeasurmentType = x.MeasurmentType,
                    OrderType = model.OrderType
                }).ToList();
                var availabilityResult = await ItemService.CheckItemsAvailability(models);
                if (availabilityResult != null && availabilityResult.IsSucceeded && availabilityResult.Data != null)
                {
                    var isAvailable = !availabilityResult.Data.Select(x => x.IsAvailable).Contains(false);
                    switch (isAvailable)
                    {
                        case true:
                            result = ProcessResultViewModelHelper.Succedded(true);
                            break;
                        case false:
                            var notAvailableItems = availabilityResult.Data.Where(x => x.IsAvailable == false).ToList();
                            var errorMsg = "";
                            foreach (var item in notAvailableItems)
                            {
                                switch (item.OrderType)
                                {
                                    case OrderType.Individual:
                                        errorMsg += $"Item {item.DescriptionEN} current stock is {item.CurrentStock}. ";
                                        break;
                                    case OrderType.Retail:
                                        switch (item.MeasurmentType)
                                        {
                                            case MeasurmentType.Head:
                                                errorMsg += $"Item {item.DescriptionEN} current stock is {item.CurrentHeadStock}. ";

                                                break;
                                            case MeasurmentType.Kgs:
                                                errorMsg += $"Item {item.DescriptionEN} current stock is {item.CurrentKgsStock}. ";

                                                break;
                                            case MeasurmentType.Pieces:
                                                errorMsg += $"Item {item.DescriptionEN} current stock is {item.CurrentPiecesStock}. ";
                                                break;
                                        }
                                        break;
                                }
                            }
                            result = ProcessResultViewModelHelper.Succedded(false, errorMsg);
                            break;
                    }
                }
                else
                {
                    result = ProcessResultViewModelHelper.Failed(false, availabilityResult.Status.Message);
                }
            }
            catch (Exception ex)
            {
                result = ProcessResultViewModelHelper.Failed(false, ex.Message);
            }
            return result;
        }

        public async Task<ProcessResultViewModel<CartViewModel>> CanCombineCartItems(CartViewModel model)
        {
            ProcessResultViewModel<CartViewModel> result = null;
            var itemIds = model.CartItems.Select(itm => itm.Fk_Item_Id).ToList();
            if (itemIds.Count > 0)
            {
                var combineRes = await ItemService.CanCombineItems(itemIds);
                if (combineRes != null && combineRes.IsSucceeded)
                {
                    result = ProcessResultViewModelHelper.Succedded(model);
                }
                else
                {
                    if (combineRes != null && combineRes.Status != null && combineRes.Status.Message != null)
                        result = ProcessResultViewModelHelper.Failed(model, combineRes.Status.Message);
                    else
                        result = ProcessResultViewModelHelper.Failed(model, "Failed While Checking Combine Items");
                }
            }
            else
            {
                result = ProcessResultViewModelHelper.Failed(model, "Can't Submit Order without CartItems");
            }

            return result;
        }
        [Route("RemoveCart/{cartId}")]
        [HttpDelete]
        [MapToApiVersion("1.0")]
        public ProcessResultViewModel<bool> RemoveCart([FromRoute]int cartId)
        {
            ProcessResultViewModel<bool> result = null;
            result = CartItemsController.DeleteCartItemsByCartId(cartId);
            if (result.IsSucceeded)
            {
                result = Delete(cartId);
            }
            return result;
        }
        [Route("GetCartWithItems/{cartId}")]
        [HttpGet]
        [MapToApiVersion("1.0")]
        public async Task<ProcessResultViewModel<CartViewModel>> GetCartWithItems([FromRoute]int cartId)
        {
            ProcessResultViewModel<CartViewModel> result = null;
            if (cartId > 0)
            {
                result = Get(cartId);
                if (result.IsSucceeded && result.Data != null)
                {
                    var itemresult = await CartItemsController.GetCartItemsByCartId(cartId);
                    if (itemresult.IsSucceeded)
                    {
                        result.Data.CartItems = itemresult.Data;
                    }
                    else
                    {
                        result = ProcessResultViewModelHelper.MapToProcessResultViewModel<List<CartItemsViewModel>, CartViewModel>(itemresult);
                    }
                }
            }
            else
            {
                result = ProcessResultViewModelHelper.Failed<CartViewModel>(null, "missing argument cartId");
            }
            return result;
        }

        [Route("GetCartsWithItems")]
        [HttpPost]
        [MapToApiVersion("1.0")]
        public async Task<ProcessResultViewModel<List<CartViewModel>>> GetCartsWithItems([FromBody]List<int> cartIds)
        {
            ProcessResultViewModel<List<CartViewModel>> result = new ProcessResultViewModel<List<CartViewModel>>();
            List<CartViewModel> resultData = new List<CartViewModel>();
            foreach (var id in cartIds)
            {
                var cart = await GetCartWithItems(id);
                if (cart.IsSucceeded && cart.Data != null)
                {
                    resultData.Add(cart.Data);
                }
            }
            result = ProcessResultViewModelHelper.Succedded(resultData);
            return result;
        }

        [Route("GetCurrentCart/{customerId}")]
        [HttpGet]
        [MapToApiVersion("1.0")]
        public async Task<ProcessResultViewModel<CartViewModel>> GetCurrentCartIndividual([FromRoute]int customerId)
        {
            return await GetCurrentCart(customerId, OrderType.Individual);
        }

        [Route("GetCurrentCart/Retail/{customerId}")]
        [HttpGet]
        [MapToApiVersion("1.0")]
        public async Task<ProcessResultViewModel<CartViewModel>> GetCurrentCartRetail([FromRoute]int customerId)
        {
            return await GetCurrentCart(customerId, OrderType.Retail);
        }
        public async Task<ProcessResultViewModel<CartViewModel>> GetCurrentCart(int customerId, OrderType type)
        {
            ProcessResultViewModel<CartViewModel> result = null;
            var cartIdResult = manger.GetCurrentCartId(customerId, type);
            if (cartIdResult.IsSucceeded && cartIdResult.Data > 0)
            {
                result = await GetCartWithItems(cartIdResult.Data);
            }
            else
            {
                result = ProcessResultViewModelHelper.Succedded<CartViewModel>(null, $"there is no existing cart for this customer {customerId}");
            }
            return result;
        }

        [Route("GetUserByCartId/{cartId}")]
        [HttpGet]
        [MapToApiVersion("1.0")]
        public async Task<ProcessResultViewModel<ApplicationUserViewModel>> GetUserByCartId([FromRoute]int cartId)
        {
            ProcessResultViewModel<ApplicationUserViewModel> result = null;
            var version = HttpContext.GetRequestedApiVersion().ToString();
            if (!string.IsNullOrEmpty(version))
            {
                version = $"V{version}";
            }
            var userIdResult = manger.GetUserByCartId(cartId);
            if (version != null && userIdResult.IsSucceeded && userIdResult.Data != null)
            {
                var userResult = await UserService.GetById(version, userIdResult.Data);
                if (userResult.IsSucceeded && userResult.Data != null)
                {
                    result = ProcessResultViewModelHelper.Succedded(userResult.Data);
                }
                else
                {
                    result = ProcessResultViewModelHelper.Failed<ApplicationUserViewModel>(null, $"User with Id {userIdResult.Data} doesn't exist");
                }
            }
            else
            {
                result = ProcessResultViewModelHelper.Failed<ApplicationUserViewModel>(null, "Error While Get User byCartId");

            }
            return result;
        }
        [Route("UpdateCartWithOrderId")]
        [HttpPost]
        [MapToApiVersion("1.0")]
        public async Task<ProcessResultViewModel<bool>> UpdateCartWithOrderId([FromBody]CartOrderViewModel model)
        {
            ProcessResultViewModel<bool> result = null;
            if (model != null)
            {
                var entityResult = manger.UpdateCartOrderInfo(model.FK_Cart_Id, model.FK_Order_Id);
                var cartRes = await GetCartWithItems(model.FK_Cart_Id);
                if (cartRes.IsSucceeded && cartRes.Data != null)
                {
                    model.Cart = cartRes.Data;
                    List<UpdateItemsStockViewModel> lst = new List<UpdateItemsStockViewModel>();
                    foreach (var item in model.Cart.CartItems)
                    {
                        var itemStock = mapper.Map<CartItemsViewModel, UpdateItemsStockViewModel>(item);
                        lst.Add(itemStock);
                    }
                    var updateStockRes = await ItemService.UpdateStock(lst);
                    if (!updateStockRes.IsSucceeded && !updateStockRes.Data)
                    {
                        result = ProcessResultViewModelHelper.MapToProcessResultViewModel<bool, bool>(updateStockRes);
                    }
                }
                result = processResultMapper.Map<bool, bool>(entityResult);
            }
            else
            {
                result = ProcessResultViewModelHelper.Failed(false, "Parameter Can't be null");
            }
            return result;
        }
        private IIdentityUserService UserService
        {
            get { return serviceProvider.GetService<IIdentityUserService>(); }
        }
        private IVATService VATService
        {
            get { return serviceProvider.GetService<IVATService>(); }
        }
        private IFeesService FeesService
        {
            get { return serviceProvider.GetService<IFeesService>(); }
        }
        private IItemService ItemService
        {
            get { return serviceProvider.GetService<IItemService>(); }
        }
        private CartItemsController CartItemsController
        {
            get { return serviceProvider.GetService<CartItemsController>(); }
        }
        private async Task<ProcessResultViewModel<CartViewModel>> GetOrAddCart(CartViewModel model)
        {
            ProcessResultViewModel<CartViewModel> cartResult = null;
            try
            {
                if (model.Id > 0)
                {
                    cartResult = Get(model.Id);
                    if (cartResult.IsSucceeded)
                    {
                        if (cartResult.Data == null)
                        {
                            cartResult = Post(model);
                        }
                    }
                }
                else
                {
                    cartResult = Post(model);
                }
            }
            catch (Exception ex)
            {
                cartResult = ProcessResultViewModelHelper.Failed<CartViewModel>(null, ex.Message);
            }
            return cartResult;
        }
        private PromoCodeValidation PromoCodeValidation
        {
            get { return serviceProvider.GetService<PromoCodeValidation>(); }
        }

        private IPromoCodeService PromoCodeService
        {
            get { return serviceProvider.GetService<IPromoCodeService>(); }
        }

        [Route("IsPromoCodeValid")]
        [HttpPost]
        [MapToApiVersion("1.0")]
        public async Task<ProcessResultViewModel<PromoValidation>> IsPromoCodeValidIndividual([FromBody]PromoCodeValidation model)
        {
            return await IsPromoCodeValid(model, OrderType.Individual);
        }

        [Route("IsPromoCodeValid/Retail")]
        [HttpPost]
        [MapToApiVersion("1.0")]
        public async Task<ProcessResultViewModel<PromoValidation>> IsPromoCodeValidRetail([FromBody]PromoCodeValidation model)
        {
            return await IsPromoCodeValid(model, OrderType.Retail);
        }
        public async Task<ProcessResultViewModel<PromoValidation>> IsPromoCodeValid([FromBody]PromoCodeValidation model, OrderType type)
        {
            ProcessResultViewModel<PromoValidation> result = null;
            if (model != null)
            {
                model.Value = 0;
                model.IsUse = false;
                var cartRes = await GetCurrentCart(model.CustomerId, type);
                if (cartRes.IsSucceeded && cartRes.Data != null)
                {
                    model.Value = (int)cartRes.Data.Price;
                    var promoResult = await PromoCodeService.SubmitPromoCode(model);
                    if (promoResult.IsSucceeded)
                    {

                        var data = mapper.Map<PromoResult, PromoValidation>(promoResult.Data);
                        data = await CalculatePromowithCart(data, cartRes.Data);
                        result = ProcessResultViewModelHelper.Succedded(data);
                    }
                    else
                    {
                        result = ProcessResultViewModelHelper.MapToProcessResultViewModel<PromoResult, PromoValidation>(promoResult);
                    }
                }
                else
                {
                    result = ProcessResultViewModelHelper.Failed<PromoValidation>(null, "No Cart for this User");
                }
            }
            else
            {
                result = ProcessResultViewModelHelper.Failed<PromoValidation>(null, "Parameter Can't be null");
            }
            return result;
        }
        private async Task<PromoValidation> CalculatePromowithCart(PromoValidation promo, CartViewModel cart)
        {
            //get default delivery fees from order service 
            // assume these
            int defaultDeliveryFees = 0;
            var feesRes = await FeesService.GetFees();
            if (feesRes.IsSucceeded)
            {
                defaultDeliveryFees = (int)feesRes.Data;
            }
            var vatResult = await VATService.GetVAT();
            int vatPercentage = 0;
            if (vatResult.IsSucceeded)
            {
                vatPercentage = (int)vatResult.Data.Percentage;
                // calculations
                var cartPriceWithDelivery = cart.Price + defaultDeliveryFees;
                var discountValue = (cartPriceWithDelivery * promo.PromoValue) / 100;
                var priceAfterDiscount = cartPriceWithDelivery - discountValue;
                var VatValue = (vatPercentage * priceAfterDiscount) / 100;

                // promo object
                // remove truncate
                //promo.Price = Math.Truncate(priceAfterDiscount);
                //promo.VAT = Math.Truncate(VatValue);
                //promo.DiscountValue = Math.Truncate(discountValue);
                //promo.Total = Math.Truncate(promo.Price + promo.VAT);

                promo.Price = priceAfterDiscount;
                promo.VAT = VatValue;
                promo.DiscountValue = discountValue;
                promo.Total = promo.Price + promo.VAT;
                return promo;
            }
            return promo;
        }
        private async Task<ProcessResultViewModel<CartViewModel>> CalculateCartVat(CartViewModel model)
        {
            ProcessResultViewModel<CartViewModel> result = null;
            var vat = await VATService.GetVAT();
            if (vat != null && vat.IsSucceeded && vat.Data != null)
            {
                // assume this 
                int defaultDeliveryFees = 0;
                var feesRes = await FeesService.GetFees();
                if (feesRes.IsSucceeded)
                {
                    defaultDeliveryFees = (int)feesRes.Data;
                }
                // Make cart vat like this
                // mn el a5r add el delivery fees 3la el cart price w enta bt7sb w b3deen e7sb el vat
                var cartPriceWithDelivery = model.Price + defaultDeliveryFees;
                model.VAT = Math.Abs((vat.Data.Percentage * cartPriceWithDelivery) / 100);

                result = ProcessResultViewModelHelper.Succedded(model);
            }
            else
            {
                result = ProcessResultViewModelHelper.MapToProcessResultViewModel<LKP_VATViewModel, CartViewModel>(vat);
            }
            return result;
        }
    }

}
