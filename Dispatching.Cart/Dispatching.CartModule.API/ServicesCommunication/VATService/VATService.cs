﻿using Dispatching.CartModule.API.ServicesViewModels.VAT;
using DispatchProduct.CartModule.API.ServicesCommunication.Settings;
using DispatchProduct.CartModule.API.ServicesCommunication.VAT;
using DispatchProduct.HttpClient;
using DnsClient;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Utilites.ProcessingResult;

namespace Dispatching.CartModule.API.ServicesCommunication.VAT
{
    public class VATService : DefaultHttpClientCrud<VATServiceSetting, LKP_VATViewModel, LKP_VATViewModel>, IVATService
    {
        VATServiceSetting settings;
        IDnsQuery dnsQuery;
        public VATService(IOptions<VATServiceSetting> _settings, IDnsQuery _dnsQuery) :base(_settings.Value, _dnsQuery)
        {
            settings = _settings.Value;
            dnsQuery = _dnsQuery;
        }
        public async Task<ProcessResultViewModel<LKP_VATViewModel>> GetVAT(string vat = null)
        {
            var requesturi = $"{await settings.GetUri(dnsQuery)}/{settings.GetVatVerb}";
            if (!string.IsNullOrEmpty(settings.VatName))
            {
               requesturi = $"{requesturi}/{settings.VatName}";
            }
            string baseUrl = await settings.GetUri(dnsQuery);
            while (baseUrl.EndsWith('/'))
            {
                baseUrl = baseUrl.Substring(0, baseUrl.Length - 1);
            }
            var result = await GetByUri(requesturi);
            return result;
        }
      
    }
}
