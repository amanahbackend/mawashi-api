﻿using Dispatching.CartModule.API.ServicesViewModels.VAT;
using DispatchProduct.CartModule.API.ServicesCommunication.Settings;
using DispatchProduct.HttpClient;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Utilites.ProcessingResult;

namespace DispatchProduct.CartModule.API.ServicesCommunication.VAT
{
    public interface IVATService : IDefaultHttpClientCrud<VATServiceSetting, LKP_VATViewModel, LKP_VATViewModel>
    {
        Task<ProcessResultViewModel<LKP_VATViewModel>> GetVAT(string vat=null);
    }
}
