﻿using Dispatching.CartModule.Models.Entities;
using DispatchProduct.CartModule.API.ServicesCommunication.Settings;
using DispatchProduct.HttpClient;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Utilites.ProcessingResult;
using Utilities.Utilites.PACI;
namespace DispatchProduct.CartModule.API.ServicesCommunication.PromoCode
{
    public interface IPromoCodeService : IDefaultHttpClientCrud<PromoCodeServiceSetting, PromoCodeValidation, PromoResult>
    {
        Task<ProcessResultViewModel<PromoResult>> SubmitPromoCode(PromoCodeValidation model);
    }
}
