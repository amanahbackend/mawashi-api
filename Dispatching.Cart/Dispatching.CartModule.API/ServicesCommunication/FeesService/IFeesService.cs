﻿using Dispatching.CartModule.API.ServicesViewModels;
using DispatchProduct.CartModule.API.Settings;
using DispatchProduct.HttpClient;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Utilites.ProcessingResult;

namespace DispatchProduct.Contracting.API.ServicesCommunication.Cutting
{
    public interface IFeesService : IDefaultHttpClientCrud<FeesServiceSetting, string, double>
    {
        Task<ProcessResultViewModel<double>> GetFees(string cityName = null);
    }
}
