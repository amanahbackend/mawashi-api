﻿using Dispatching.CartModule.API.ServicesViewModels;
using DispatchProduct.CartModule.API.Settings;
using DispatchProduct.Contracting.API.ServicesCommunication.Cutting;
using DispatchProduct.HttpClient;
using DnsClient;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Utilites.ProcessingResult;

namespace Dispatching.CartModule.API.ServicesCommunication.Inventory.Cutting
{
    public class FeesService : DefaultHttpClientCrud<FeesServiceSetting, string, double>, IFeesService
    {
        FeesServiceSetting settings;
        IDnsQuery dnsQuery;
        public FeesService(IOptions<FeesServiceSetting> _settings, IDnsQuery _dnsQuery) :base(_settings.Value, _dnsQuery)
        {
            settings = _settings.Value;
            dnsQuery = _dnsQuery;
        }

        public async Task<ProcessResultViewModel<double>> GetFees(string cityName=null)
        {
            var requesturi = $"{await settings.GetUri(dnsQuery)}/{settings.GetFeesVerb}";
            if (cityName != null)
            {
                requesturi = $"{requesturi}/{cityName}";

            }
            string baseUrl = await settings.GetUri(dnsQuery);
            while (baseUrl.EndsWith('/'))
            {
                baseUrl = baseUrl.Substring(0, baseUrl.Length - 1);
            }
            var result = await GetByUri(requesturi);
            return result;
        }

    }
}
