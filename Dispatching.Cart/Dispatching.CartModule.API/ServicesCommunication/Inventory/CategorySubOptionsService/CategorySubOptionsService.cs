﻿using Dispatching.CartModule.API.ServicesViewModels;
using DispatchProduct.CartModule.API.Settings;
using DispatchProduct.Contracting.API.ServicesCommunication.Quantity;
using DispatchProduct.HttpClient;
using DnsClient;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Dispatching.CartModule.API.ServicesCommunication.Inventory.Quantity
{
    public class CategorySubOptionsService : DefaultHttpClientCrud<CategorySubOptionsServiceSetting, CategorySubOptionsViewModel, CategorySubOptionsViewModel>, ICategorySubOptionsService
    {
        CategorySubOptionsServiceSetting settings;
        public CategorySubOptionsService(IOptions<CategorySubOptionsServiceSetting> _settings, IDnsQuery _dnsQuery) :base(_settings.Value, _dnsQuery)
        {
            settings = _settings.Value;
        }

    }
}
