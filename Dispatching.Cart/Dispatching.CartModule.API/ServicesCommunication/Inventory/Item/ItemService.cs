﻿using Dispatching.CartModule.API.ServicesViewModels;
using Dispatching.CartModule.API.ServicesViewModels.Inventory.Item;
using DispatchProduct.CartModule.API.Settings;
using DispatchProduct.Contracting.API.ServicesCommunication.Item;
using DispatchProduct.HttpClient;
using DnsClient;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Utilites.ProcessingResult;

namespace Dispatching.CartModule.API.ServicesCommunication.Inventory
{
    public class ItemService : DefaultHttpClientCrud<ItemServiceSetting, LKP_ItemViewModel, LKP_ItemViewModel>, IItemService
    {
        ItemServiceSetting settings;
        IDnsQuery dnsQuery;
        public ItemService(IOptions<ItemServiceSetting> _settings, IDnsQuery _dnsQuery) : base(_settings.Value, _dnsQuery)
        {
            settings = _settings.Value;
            dnsQuery = _dnsQuery;
        }
        public async Task<ProcessResultViewModel<bool>> CanCombineItems(List<int> model)
        {
            var requesturi = $"{await settings.GetUri(dnsQuery)}/{settings.CanCombineItemsVerb}";
            string baseUrl = await settings.GetUri(dnsQuery);
            while (baseUrl.EndsWith('/'))
            {
                baseUrl = baseUrl.Substring(0, baseUrl.Length - 1);
            }
            var result = await PostCustomize<List<int>, bool>(requesturi, model);
            return result;
        }

        public async Task<ProcessResultViewModel<bool>> UpdateStock(List<UpdateItemsStockViewModel> model)
        {
            var requesturi = $"{await settings.GetUri(dnsQuery)}/{settings.UpdateStockVerb}";
            string baseUrl = await settings.GetUri(dnsQuery);
            while (baseUrl.EndsWith('/'))
            {
                baseUrl = baseUrl.Substring(0, baseUrl.Length - 1);
            }
            var result = await PostCustomize<List<UpdateItemsStockViewModel>, bool>(requesturi, model);
            return result;
        }

        public async Task<ProcessResultViewModel<List<ItemAvailabilityViewModel>>> CheckItemsAvailability(List<CheckItemAvailabilityViewModel> models)
        {
            var requesturi = $"{await settings.GetUri(dnsQuery)}/{settings.CheckItemsAvailabilityVerb}";
            string baseUrl = await settings.GetUri(dnsQuery);
            while (baseUrl.EndsWith('/'))
            {
                baseUrl = baseUrl.Substring(0, baseUrl.Length - 1);
            }
            var result = await PostCustomize<List<CheckItemAvailabilityViewModel>, List<ItemAvailabilityViewModel>>(requesturi, models);
            return result;
        }
    }
}
