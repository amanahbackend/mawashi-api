﻿using Dispatching.CartModule.API.ServicesViewModels;
using Dispatching.CartModule.API.ServicesViewModels.Inventory.Item;
using DispatchProduct.CartModule.API.Settings;
using DispatchProduct.HttpClient;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Utilites.ProcessingResult;

namespace DispatchProduct.Contracting.API.ServicesCommunication.Item
{
    public interface IItemService : IDefaultHttpClientCrud<ItemServiceSetting, LKP_ItemViewModel, LKP_ItemViewModel>
    {
        Task<ProcessResultViewModel<bool>> CanCombineItems(List<int> model);
        Task<ProcessResultViewModel<bool>> UpdateStock(List<UpdateItemsStockViewModel> model);
        Task<ProcessResultViewModel<List<ItemAvailabilityViewModel>>> CheckItemsAvailability(List<CheckItemAvailabilityViewModel> models);
    }
}
