﻿using Dispatching.CartModule.API.ServicesViewModels;
using DispatchProduct.CartModule.API.Settings;
using DispatchProduct.Contracting.API.ServicesCommunication.Cutting;
using DispatchProduct.HttpClient;
using DnsClient;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Dispatching.CartModule.API.ServicesCommunication.Inventory.Cutting
{
    public class CuttingService : DefaultHttpClientCrud<CuttingServiceSetting, LKP_CuttingViewModel, LKP_CuttingViewModel>, ICuttingService
    {
        CuttingServiceSetting settings;
        public CuttingService(IOptions<CuttingServiceSetting> _settings, IDnsQuery _dnsQuery) :base(_settings.Value, _dnsQuery)
        {
            settings = _settings.Value;
        }

    }
}
