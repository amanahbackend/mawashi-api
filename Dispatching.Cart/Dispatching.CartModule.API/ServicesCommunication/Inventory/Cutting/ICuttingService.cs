﻿using Dispatching.CartModule.API.ServicesViewModels;
using DispatchProduct.CartModule.API.Settings;
using DispatchProduct.HttpClient;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DispatchProduct.Contracting.API.ServicesCommunication.Cutting
{
    public interface ICuttingService : IDefaultHttpClientCrud<CuttingServiceSetting, LKP_CuttingViewModel, LKP_CuttingViewModel>
    {
     
    }
}
