﻿
using DispatchProduct.RepositoryModule;
using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Dispatching.CartModule.Models.Context;
using Dispatching.CartModule.Models.Entities;

namespace DispatchProduct.CartModule.API.Seed
{
    public class CartDbContextSeed : ContextSeed
    {
        public async Task SeedAsync(CartDbContext context, IHostingEnvironment env,
            ILogger<CartDbContextSeed> logger,  int? retry = 0)
        {
            int retryForAvaiability = retry.Value;

            try
            {
                var contentRootPath = env.ContentRootPath;
                var webroot = env.WebRootPath;
                List<Cart> lstCart = new List<Cart>();
                List<CartItems> lstCartItems = new List<CartItems>();
                //lstCart = GetDefaultCarts();
                //lstCartItems = GetDefaultCartItems();
              
                await SeedEntityAsync(context, lstCart);

                await SeedEntityAsync(context, lstCartItems);

            }
            catch (Exception ex)
            {
                if (retryForAvaiability < 3)
                {
                    retryForAvaiability++;

                    logger.LogError(ex.Message, $"There is an error migrating data for CustomerDbContextSeed");

                    await SeedAsync(context, env, logger, retryForAvaiability);
                }
            }
        }
    }
}
