﻿using AutoMapper;
using CommonEnums;
using Dispatching.BuisnessCommon.Enums;
using Dispatching.CartModule.API.ServicesViewModels;
using Dispatching.CartModule.API.ViewModel;
using Dispatching.CartModule.Models.Entities;
using Utilities.Utilites;

namespace Dispatching.CartModule.API.AutoMapperConfig
{
    public class MappingProfile : Profile
    {
        public MappingProfile()
        {



            CreateMap<PromoResult, PromoValidation>()
                 .ForMember(dest => dest.IsValid, opt => opt.MapFrom(src => src.Valid))
                 .ForMember(dest => dest.PromoValue, opt => opt.MapFrom(src => src.Value))
                 .ForMember(dest => dest.Price, opt => opt.Ignore())
                 .ForMember(dest => dest.VAT, opt => opt.Ignore())
                 .ForMember(dest => dest.DiscountValue, opt => opt.Ignore())
                 .ForMember(dest => dest.Total, opt => opt.Ignore());
            CreateMap<SalesCartItemsViewModel, CartItemsViewModel>()
             .ForMember(dest => dest.Quantity, opt => opt.ResolveUsing(BindQuantity))
             .ForMember(dest => dest.Cutting, opt => opt.Ignore())
             .ForMember(dest => dest.QuantityValue, opt => opt.MapFrom(src => src.Quantity))
             .ForMember(dest => dest.FK_Cutting_Id, opt => opt.Ignore())
             .ForMember(dest => dest.FK_Quantity_Id, opt => opt.Ignore())
             .ForMember(dest => dest.CuttingPrice, opt => opt.Ignore())
             .ForMember(dest => dest.CategorySubOption, opt => opt.MapFrom(src => src.CategorySubOption))
             .ForMember(dest => dest.MeasurmentUnit, opt => opt.ResolveUsing(BindMeasurment));

            CreateMap<SalesCartViewModel, CartViewModel>();

            CreateMap<CartItemsViewModel, UpdateItemsStockViewModel>()
            .ForMember(dest => dest.ItemId, opt => opt.MapFrom(src => src.Fk_Item_Id))
            .ForMember(dest => dest.Count, opt => opt.MapFrom(src => src.QuantityValue))
            .ForMember(dest => dest.OrderType, opt => opt.MapFrom(src => src.OrderType))
            .ForMember(dest => dest.MeasurmentType, opt => opt.MapFrom(src => src.MeasurmentType));

            CreateMap<Cart, CartViewModel>();
            CreateMap<CartViewModel, Cart>()
                .IgnoreBaseEntityProperties();
            CreateMap<CartItems, CartItemsViewModel>()
                .ForMember(dest => dest.Item, opt => opt.Ignore())
                .ForMember(dest => dest.Item, opt => opt.Ignore())
                .ForMember(dest => dest.Cutting, opt => opt.Ignore())
                .ForMember(dest => dest.Quantity, opt => opt.Ignore())
                .ForMember(dest => dest.Cutting, opt => opt.Ignore())
                .ForMember(dest => dest.CategorySubOption, opt => opt.Ignore())
                .ForMember(dest => dest.MeasurmentUnit, opt => opt.ResolveUsing(BindMeasurment));
            CreateMap<CartItemsViewModel, CartItems>()

                .IgnoreBaseEntityProperties();
        }
        private LKP_QuantityViewModel BindQuantity(SalesCartItemsViewModel salesModel, CartItemsViewModel cartModel)
        {
            var Quantity = new LKP_QuantityViewModel();
            Quantity.Value = salesModel.Quantity;
            return Quantity;
        }
        private EnumEntity BindMeasurment(CartItems cartItem, CartItemsViewModel cartItemViewModel)
        {
            EnumEntity result = null;
            if (cartItem.MeasurmentType > 0)
            {
                result = EnumManager<MeasurmentType>.GetEnumObject(cartItem.MeasurmentType);
            }
            return result;
        }
        private EnumEntity BindMeasurment(SalesCartItemsViewModel cartItem, CartItemsViewModel cartItemViewModel)
        {
            EnumEntity result = null;
            if (cartItem.MeasurmentType > 0)
            {
                result = EnumManager<MeasurmentType>.GetEnumObject(cartItem.MeasurmentType);
            }
            return result;
        }
    }
}
