﻿using DispatchProduct.HttpClient;

namespace DispatchProduct.CartModule.API.Settings
{
    public class FeesServiceSetting : DefaultHttpClientSettings
    {
        public override string Uri
        {
            get; set;
        }
        public string GetFeesVerb
        {
            get; set;
        }
    }
}
