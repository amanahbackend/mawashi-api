﻿using DispatchProduct.HttpClient;

namespace DispatchProduct.CartModule.API.Settings
{
    public class ItemServiceSetting : DefaultHttpClientSettings
    {
        public override string Uri
        {
            get; set;
        }
        public string CanCombineItemsVerb
        {
            get; set;
        }
        public string UpdateStockVerb
        {
            get; set;
        }
        public string CheckItemsAvailabilityVerb
        {
            get; set;
        }
    }
}
