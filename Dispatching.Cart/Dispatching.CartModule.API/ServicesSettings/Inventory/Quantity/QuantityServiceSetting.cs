﻿using DispatchProduct.HttpClient;

namespace DispatchProduct.CartModule.API.Settings
{
    public class QuantityServiceSetting : DefaultHttpClientSettings
    {
        public override string Uri
        {
            get; set;
        }
    }
}
