﻿using DispatchProduct.HttpClient;

namespace DispatchProduct.CartModule.API.Settings
{
    public class CuttingServiceSetting : DefaultHttpClientSettings
    {
        public override string Uri
        {
            get; set;
        }
    }
}
