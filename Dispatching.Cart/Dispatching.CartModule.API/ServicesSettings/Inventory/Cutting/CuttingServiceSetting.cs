﻿using DispatchProduct.HttpClient;

namespace DispatchProduct.CartModule.API.Settings
{
    public class CategorySubOptionsServiceSetting : DefaultHttpClientSettings
    {
        public override string Uri
        {
            get; set;
        }
    }
}
