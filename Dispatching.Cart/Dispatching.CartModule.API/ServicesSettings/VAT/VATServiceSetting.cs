﻿using DispatchProduct.HttpClient;

namespace DispatchProduct.CartModule.API.ServicesCommunication.Settings
{
    public class VATServiceSetting : DefaultHttpClientSettings
    {
        public override string Uri
        {
            get; set;
        }
        public string GetVatVerb
        {
            get; set;
        }
        public string VatName
        {
            get; set;
        }
        
    }
}
