﻿using DispatchProduct.HttpClient;

namespace DispatchProduct.CartModule.API.ServicesCommunication.Settings
{
    public class PromoCodeServiceSetting : DefaultHttpClientSettings
    {
        public override string Uri
        {
            get; set;
        }
        public  string CheckPromoCodeVerb
        {
            get; set;
        }
        

    }
}
