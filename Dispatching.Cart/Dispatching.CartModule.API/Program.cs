﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.DependencyInjection;
using DispatchProduct.CartModule.API.Seed;
using Dispatching.CartModule.Models.Context;
using Serilog;
using Utilities.Utilites.SerilogExtensions;

namespace Dispatching.CartModule.API
{
    public class Program
    {
        public static void Main(string[] args)
        {

            Log.Logger = new LoggerConfiguration()
               .Enrich.With<CustomExceptionEnricher>()
               .MinimumLevel.Error()
               .Enrich.FromLogContext()
               .WriteTo.File("logs/log_.csv", rollingInterval: RollingInterval.Day,
               outputTemplate: "{Timestamp:yyyy-MM-dd HH:mm:ss.fff zzz},[{Level}],{Message},{ExceptionMessage},{ExceptionSource},{ExceptionType},{ExceptionStackTrace},{NewLine}")
               .CreateLogger();

            BuildWebHost(args).MigrateDbContext<CartDbContext>((context, services) =>
            {
                var env = services.GetService<IHostingEnvironment>();
                var logger = services.GetService<ILogger<CartDbContextSeed>>();
                new CartDbContextSeed()
                    .SeedAsync(context, env, logger)
                    .Wait();
            }).Run();
        }

        public static IWebHost BuildWebHost(string[] args) =>
          WebHost.CreateDefaultBuilder(args)
              .UseKestrel()
              .UseContentRoot(Directory.GetCurrentDirectory())
              .UseIISIntegration()
              .UseStartup<Startup>()
              .UseSetting("detailedErrors", "true")
              .CaptureStartupErrors(true)
              .UseSerilog()
              .Build();
    }
}
