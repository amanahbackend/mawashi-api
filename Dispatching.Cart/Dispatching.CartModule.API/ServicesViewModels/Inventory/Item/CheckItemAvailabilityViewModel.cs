﻿using Dispatching.BuisnessCommon.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Dispatching.CartModule.API.ServicesViewModels.Inventory.Item
{
    public class CheckItemAvailabilityViewModel
    {
        public int ItemId { get; set; }
        public OrderType OrderType { get; set; }
        public MeasurmentType MeasurmentType { get; set; }
        public int QuantityId { get; set; }
    }
}
