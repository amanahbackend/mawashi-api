﻿using CommonEnums;
using Dispatching.BuisnessCommon.Enums;
using DispatchProduct.BuildingBlocks.BaseEntities.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace Dispatching.CartModule.API.ServicesViewModels
{
    public class UpdateItemsStockViewModel 
    {
        public int ItemId { get; set; }
        public int Count { get; set; }
        public OrderType OrderType { get; set; }
        public  MeasurmentType MeasurmentType { get; set; }
    }
}
