﻿using DispatchProduct.BuildingBlocks.BaseEntities.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace Dispatching.CartModule.API.ServicesViewModels
{
    public class CategorySubOptionsViewModel : BaseLKPEntityViewModel
    {
        public int FK_CategoryConfig_Id { get; set; }
    }
}
