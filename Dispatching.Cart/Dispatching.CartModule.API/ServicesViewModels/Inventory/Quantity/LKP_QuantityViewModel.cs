﻿using DispatchProduct.BuildingBlocks.BaseEntities.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace Dispatching.CartModule.API.ServicesViewModels
{
    public class LKP_QuantityViewModel : BaseLKPEntityViewModel
    {
        public int Value { get; set; }
        public string DescriptionAR { get; set; }
        public string DescriptionEN { get; set; }
    }
}
