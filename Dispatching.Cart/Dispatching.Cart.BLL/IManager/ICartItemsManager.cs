﻿using System;
using System.Collections.Generic;
using DispatchProduct.RepositoryModule;
using System.Text;
using Utilites.ProcessingResult;
using Dispatching.CartModule.Models.Entities;

namespace Dispatching.CartModule.BLL.IManagers
{
    public interface ICartItemsManager : IRepository<CartItems>
    {
        ProcessResult<bool> DeleteByCartId(int cartId);
        ProcessResult<List<CartItems>> GetByCartId(int cartId);
        ProcessResult<bool> UpdateProdValues(CartItems cartItem);
    }
}
