﻿using System;
using System.Collections.Generic;
using DispatchProduct.RepositoryModule;
using System.Text;
using Utilites.ProcessingResult;
using Dispatching.CartModule.Models.Entities;
using Dispatching.BuisnessCommon.Enums;

namespace Dispatching.CartModule.BLL.IManagers
{
    public interface ICartManager : IRepository<Cart>
    {
        ProcessResult<List<Cart>> GetByCustomerId(int customerId);
        ProcessResult<string> GetUserByCartId(int cartId);
        ProcessResult<int> GetCurrentCartId(int customerId, OrderType type);
        ProcessResult<bool> UpdateCartOrderInfo(int cartId, int orderId);
    }
}
