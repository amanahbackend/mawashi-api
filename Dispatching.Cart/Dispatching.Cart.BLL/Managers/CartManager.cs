﻿using System;
using System.Collections.Generic;
using DispatchProduct.RepositoryModule;
using System.Text;
using Utilites.ProcessingResult;
using System.Linq;
using Dispatching.CartModule.Models.Entities;
using Dispatching.CartModule.Models.Context;
using Dispatching.CartModule.BLL.IManagers;
using Dispatching.BuisnessCommon.Enums;

namespace Dispatching.CartModule.BLL.Managers
{
    public class CartManager : Repository<Cart>, ICartManager
    {
        IProcessResultMapper processResultMapper;
        public CartManager(CartDbContext context, IProcessResultMapper _processResultMapper)
            : base(context)
        {
            _processResultMapper = processResultMapper;
        }
        public ProcessResult<List<Cart>> GetByCustomerId(int customerId)
        {
            try
            {


                var result = GetAllQuerable();
                if (result.IsSucceeded)
                {
                    var data = result.Data.Where(c => c.FK_Customer_Id == customerId).ToList();
                    return ProcessResultHelper.Succedded(data);
                }
                else
                {
                    return ProcessResultHelper.MapToProcessResult<IQueryable<Cart>, List<Cart>>(result);
                }
            }
            catch (Exception ex)
            {
                return ProcessResultHelper.Failed<List<Cart>>(null, ex, ex.Message);
            }
        }

        public ProcessResult<string> GetUserByCartId(int cartId)
        {
            ProcessResult<string> result = null;
            string userId = GetAllQuerable().Data.Where(c => c.Id == cartId).Select(c => c.FK_CreatedBy_Id).FirstOrDefault();
            if (!string.IsNullOrEmpty(userId))
            {
                result = ProcessResultHelper.Succedded(userId);
            }
            else
            {
                result = ProcessResultHelper.Failed(userId, new Exception("No User Id for this Cart"));
            }
            return result;
        }

        public ProcessResult<int> GetCurrentCartId(int customerId, OrderType type)
        {
            var cartId = GetAllQuerable().Data.Where(cs => cs.FK_Customer_Id == customerId && cs.HasOrder == false&& cs.OrderType==type).Select(cus => cus.Id).FirstOrDefault();
            return ProcessResultHelper.Succedded(cartId);
        }

        public ProcessResult<bool> UpdateCartOrderInfo(int cartId, int orderId)
        {
            ProcessResult<bool> result = null;
            var cartRes = Get(cartId);
            if (cartRes.IsSucceeded && cartRes.Data != null)
            {
                cartRes.Data.FK_Order_Id = orderId;
                cartRes.Data.HasOrder = true;
                result = Update(cartRes.Data);
            }
            else
            {
                result = ProcessResultHelper.MapToProcessResult<Cart, bool>(cartRes);
            }
            return result;
        }
    }
}
