﻿using Dispatching.CartModule.BLL.IManagers;
using Dispatching.CartModule.Models.Context;
using Dispatching.CartModule.Models.Entities;
using DispatchProduct.RepositoryModule;
using System;
using System.Collections.Generic;
using System.Linq;
using Utilites.ProcessingResult;
namespace Dispatching.CartModule.BLL.Managers
{
    public class CartItemsManager : Repository<CartItems>, ICartItemsManager
    {
        IProcessResultMapper processResultMapper;
        IServiceProvider serviceProvider;
        public CartItemsManager(CartDbContext context, IProcessResultMapper _processResultMapper, IServiceProvider _serviceProvider)
            : base(context)
        {
            _processResultMapper = processResultMapper;
            _serviceProvider = serviceProvider;
        }
        public ProcessResult<bool> DeleteByCartId(int cartId)
        {
            var cartitems = GetAllQuerable().Data.Where(c => c.FK_Cart_Id == cartId).ToList();
            return Delete(cartitems);
        }
        public ProcessResult<List<CartItems>> GetByCartId(int cartId)
        {
            var cartitems = GetAllQuerable().Data.Where(c => c.FK_Cart_Id == cartId).ToList();
            return ProcessResultHelper.Succedded(cartitems);
        }

        public ProcessResult<bool> UpdateProdValues(CartItems cartItem)
        {
            try
            {
                var dbCartItem = Get(cartItem.Id)?.Data;
                if (dbCartItem != null)
                {
                    dbCartItem.ActualQuantityKg = cartItem.ActualQuantityKg;
                    dbCartItem.ActualQuantityPcs = cartItem.ActualQuantityPcs;
                    dbCartItem.Location = cartItem.Location;
                    dbCartItem.IsPacked = cartItem.IsPacked;
                    var updateResult = Update(dbCartItem);
                    return updateResult;
                }
                else
                {
                    return ProcessResultHelper.Failed(false, new Exception("Can't find cart item with the provided id."));
                }
            }
            catch (Exception ex)
            {
                return ProcessResultHelper.Failed(false, ex);
            }
        }
    }
}
