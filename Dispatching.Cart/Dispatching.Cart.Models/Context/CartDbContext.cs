﻿using Dispatching.CartModule.Models.Entities;
using Dispatching.CartModule.Models.EntityConfigurations;
using Microsoft.EntityFrameworkCore;

namespace Dispatching.CartModule.Models.Context
{
    public class CartDbContext : DbContext
    {

        public DbSet<Cart> Cart { get; set; }
        public DbSet<CartItems> CartItems { get; set; }
        public CartDbContext(DbContextOptions<CartDbContext> options)
            : base(options)
        {

        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.ApplyConfiguration(new CartEntityTypeConfiguration());
            modelBuilder.ApplyConfiguration(new CartItemsEntityTypeConfiguration());
        }
    }
}
