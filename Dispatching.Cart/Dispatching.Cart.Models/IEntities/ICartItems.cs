﻿using Dispatching.BuisnessCommon.Enums;
using DispatchProduct.BuildingBlocks.BaseEntities.IEntities;
using System;
using System.Collections.Generic;
using System.Text;

namespace Dispatching.CartModule.Models.Entities
{
    public interface ICartItems : IBaseEntity
    {
         int Fk_Item_Id { get; set; }
         double ItemFinalPrice { get; set; }
         double ItemNetPrice { get; set; }
         double ItemDiscount { get; set; }
         int FK_Cutting_Id { get; set; }
         double CuttingPrice { get; set; }
         int FK_Quantity_Id { get; set; }
         int FK_Cart_Id { get; set; }
         int QuantityValue { get; set; }
         double ItemQuantityByKG { get; set; }
         int FK_CategoryConfig_Id { get; set; }
         int FK_Option_Id { get; set; }
         double Price { get; set; }
         double SalesPricePerItem { get; set; }
         bool IsSalesChangePrice { get; set; }
         OrderType OrderType { get; set; }
         MeasurmentType MeasurmentType { get; set; }
         int FK_Order_Id { get; set; }
         double VAT { get; set; }
         Cart Cart { get; set; }
         bool AcceptMulti { get; set; }

    }
}
