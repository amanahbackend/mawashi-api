﻿using System.ComponentModel.DataAnnotations.Schema;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;
using DispatchProduct.BuildingBlocks.BaseEntities.IEntities;
using Dispatching.BuisnessCommon.Enums;

namespace Dispatching.CartModule.Models.Entities
{
    public interface ICart : IBaseEntity
    {
        int FK_Customer_Id { get; set; }
        double VAT { get; set; }
        double Price { get; set; }
        int FK_Order_Id { get; set; }
        bool HasOrder { get; set; }
        OrderType OrderType { get; set; }
    }
}
