﻿using Dispatching.CartModule.Models.Entities;
using DispatchProduct.BuildingBlocks.BaseEntities.EntityConfigurations;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Dispatching.CartModule.Models.EntityConfigurations
{
    public class CartEntityTypeConfiguration : BaseEntityTypeConfiguration<Cart>,IEntityTypeConfiguration<Cart>
    {
        public void Configure(EntityTypeBuilder<Cart> CategoryConfiguration)
        {
            base.Configure(CategoryConfiguration);
            CategoryConfiguration.Property(o => o.Price).IsRequired();
            CategoryConfiguration.Property(o => o.FK_Customer_Id).IsRequired();
            CategoryConfiguration.Property(o => o.HasOrder).IsRequired();
            CategoryConfiguration.Property(o => o.FK_Order_Id).IsRequired();
            CategoryConfiguration.Property(o => o.Id).ValueGeneratedOnAdd();
            CategoryConfiguration.ToTable("Cart");
            CategoryConfiguration.Ignore(o => o.CartItems);
        }
    }
}
