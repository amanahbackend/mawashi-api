﻿using Dispatching.CartModule.Models.Entities;
using DispatchProduct.BuildingBlocks.BaseEntities.EntityConfigurations;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Dispatching.CartModule.Models.EntityConfigurations
{
    public class CartItemsEntityTypeConfiguration : BaseEntityTypeConfiguration<CartItems>, IEntityTypeConfiguration<CartItems>
    {
        public void Configure(EntityTypeBuilder<CartItems> CategoryConfiguration)
        {
            base.Configure(CategoryConfiguration);
            CategoryConfiguration.Property(o => o.Fk_Item_Id).IsRequired();
            CategoryConfiguration.Property(o => o.Id).ValueGeneratedOnAdd();
            CategoryConfiguration.Property(o => o.ItemFinalPrice).IsRequired();
            CategoryConfiguration.Property(o => o.FK_Cutting_Id).IsRequired();
            CategoryConfiguration.Property(o => o.CuttingPrice).IsRequired();
            CategoryConfiguration.Property(o => o.FK_Quantity_Id).IsRequired();
            CategoryConfiguration.Property(o => o.FK_Cart_Id).IsRequired();
            CategoryConfiguration.Property(o => o.Price).IsRequired();

            // Production System Props
            CategoryConfiguration.Property(o => o.ActualQuantityKg).IsRequired();
            CategoryConfiguration.Property(o => o.ActualQuantityPcs).IsRequired();
            CategoryConfiguration.Property(o => o.Location).IsRequired(false);

            CategoryConfiguration.Ignore(o => o.Cart);
            CategoryConfiguration.ToTable("CartItems");
        }
    }
}
