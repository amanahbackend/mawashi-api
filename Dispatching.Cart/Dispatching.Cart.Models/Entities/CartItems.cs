﻿using Dispatching.BuisnessCommon.Enums;
using DispatchProduct.BuildingBlocks.BaseEntities.Entities;
using DispatchProduct.BuildingBlocks.BaseEntities.IEntities;
using System;
using System.Collections.Generic;
using System.Text;

namespace Dispatching.CartModule.Models.Entities
{
    public class CartItems : BaseEntity, ICartItems
    {
        public int Fk_Item_Id { get; set; }
        public double ItemFinalPrice { get; set; }
        public double ItemNetPrice { get; set; }
        public double ItemDiscount { get; set; }
        public int FK_Cutting_Id { get; set; }
        public double CuttingPrice { get; set; }
        public int FK_Quantity_Id { get; set; }
        public int FK_Cart_Id { get; set; }
        public int QuantityValue { get; set; }
        public double ItemQuantityByKG { get; set; }
        public int FK_CategoryConfig_Id { get; set; }
        public int FK_Option_Id { get; set; }
        public double Price { get; set; }
        public double SalesPricePerItem { get; set; }
        public bool IsSalesChangePrice { get; set; }
        public OrderType OrderType { get; set; }
        public MeasurmentType MeasurmentType { get; set; }
        public int FK_Order_Id { get; set; }
        public double VAT { get; set; }
        public Cart Cart { get; set; }
        public bool AcceptMulti { get; set; }

        // Production System Props
        public double ActualQuantityKg { get; set; }
        public int ActualQuantityPcs { get; set; }
        public string Location { get; set; }
        public bool IsPacked { get; set; }
    }
}
