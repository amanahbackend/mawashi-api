﻿using System.ComponentModel.DataAnnotations.Schema;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;
using DispatchProduct.BuildingBlocks.BaseEntities.Entities;
using Dispatching.BuisnessCommon.Enums;

namespace Dispatching.CartModule.Models.Entities
{
    public class Cart : BaseEntity,ICart
    {
        public int FK_Customer_Id { get; set; }
        public double VAT { get; set; }
        public double Price { get; set; }
        public int FK_Order_Id { get; set; }
        public bool HasOrder { get; set; }
        public List<CartItems> CartItems { get; set; }
        public OrderType OrderType { get; set; }
    }
}
