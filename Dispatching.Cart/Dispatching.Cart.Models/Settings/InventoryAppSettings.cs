﻿namespace Dispatching.CartModule.Models.Settings
{
    public class CartAppSettings
    {
        public string ItemImagesPath { get; set; }
        public string CategoryImagesPath { get; set; }
    }
}
